<?php

use yii\db\Migration;

/**
 * Class m191123_011308_alter_category_table
 */
class m191123_011308_alter_category_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('{{%category}}', 'slug', $this->string()->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{%category}}', 'slug');
    }

}
