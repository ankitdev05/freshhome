<?php

use yii\db\Migration;

/**
 * Class m191206_190654_alter_menu_table
 */
class m191206_190654_alter_menu_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('{{%menu}}', 'brand_id', $this->integer()->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{%menu}}', 'brand_id');
    }

}
