<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brand_description}}`.
 */
class m191111_100657_create_brand_description_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%brand_description}}', [
            'id' => $this->primaryKey(),
            'brand_id' => $this->integer(11)->defaultValue(null),
            'language_id' => $this->integer(11)->defaultValue(null),   
            'brand_name' => $this->string(100)->defaultValue(null), 
            'created_at' => $this->integer(11)->defaultValue(null), 
            'updated_at' => $this->integer(11)->defaultValue(null),   
        ]);
    }  

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%brand_description}}');
    }
}
