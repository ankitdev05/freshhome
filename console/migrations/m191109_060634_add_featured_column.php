<?php

use yii\db\Migration;

/**
 * Class m191109_060634_add_featured_column
 */
class m191109_060634_add_featured_column extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('{{%category}}', 'featured', $this->smallInteger(1)->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{%category}}', 'category');

        return false;
    }

}
