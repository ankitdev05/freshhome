<?php

use yii\db\Migration;

/**
 * Class m191203_155536_alter_menu_table
 */
class m191203_155536_alter_menu_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('{{%menu}}', 'home_category', $this->integer()->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{%menu}}', 'home_category');
    }

}
