<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brand_category}}`.
 */
class m191111_100039_create_brand_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%brand_category}}', [
            'id' => $this->primaryKey(),
            'brand_id' => $this->integer(11)->defaultValue(null),
            'category_id' => $this->integer(11)->defaultValue(null),  
            'created_at' => $this->integer(11)->defaultValue(null),
            'updated_at' => $this->integer(11)->defaultValue(null),    
        ]);
    } 

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%brand_category}}');
    }
}
