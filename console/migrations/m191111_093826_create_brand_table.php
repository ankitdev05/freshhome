<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brand}}`.
 */
class m191111_093826_create_brand_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%brand}}', [
            'brand_id' => $this->primaryKey(),
            'image' => $this->string(100)->defaultValue(null),
            'file_info' => $this->string(100)->defaultValue(null), 
            'status' => $this->integer(11)->defaultValue(null), 
            'created_at' => $this->integer(11)->defaultValue(null),
            'updated_at' => $this->integer(11)->defaultValue(null),   
        ]); 
    }



    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%brand}}');
    }
}
