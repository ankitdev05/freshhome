<?php
namespace console\controllers;
use Yii;
use yii\console\Controller;

use common\models\User;
use common\models\Order;
use common\models\Library;
use common\models\Notifications;
use common\models\DriverOrderStatus;

class CronController extends Controller {
    public $time_to_wait = 300;
    public $extra_time = 1200;
    public function actionIndex(){
        $library = new Library();
        $all_orders = Order::find()->where(['and',['is','order_accepted',null],['not IN','order_status_id',[5,2]]])->all();
        if(!empty($all_orders)){
            foreach($all_orders as $order){
                $current_time   = time();
                $d = new \DateTime($order->date_added);
                $date_added = $d->getTimestamp();
                $time = $date_added+$order->dish_prep_time+$this->extra_time;
                $time_remaining = $time-$current_time;

                if($time_remaining<=0){
                    $order_id = $order->order_id;
                    $order->order_status_id = 2;
                    $order->driver_id = null;
                    $order->order_accepted = null;
                    $order->save();


                    $order_info['user_id']        = $order->supplier_id;
                    $order_info['role']           = 2;
                    $order_info['from_user']      = $order->supplier_id;
                    $order_info['type']           = 'order';
                    $order_info['reason']         = 'Driver was not available';
                    $order_info['config_name']    = 'Your order has been cancelled';
                    $order_info['config_value']['order_id']= $order_id;
                    $order_info['config_value']['message']= 'Your order has been cancelled';
                    $library->saveNotification($order_info);

                    $order_info['user_id']        = $order->user_id;
                    $order_info['role']           = 1;
                    $order_info['from_user']      = $order->user_id;
                    $order_info['type']           = 'order';
                    $order_info['reason']         = 'Driver was not available';
                    $order_info['config_name']    = 'Your order has been cancelled';
                    $order_info['config_value']['order_id']= $order_id;
                    $order_info['config_value']['message']= 'Your order has been cancelled';
                    $library->saveNotification($order_info);
                }else{
                    $time_left = ($date_added+$order->dish_prep_time)-time();
                    if($order->type=="pre-order"){
                        $time= time();
                        $rem_time = $date_added-$time;
                        if($rem_time<=3600){ // send a notification to supplier pre order.(As a reminder).
                            $count_notify = Notifications::find()->where(['reason'=>'new_order_'.$order->order_id])->count();
                            if($count_notify==0){
                                $user_information = User::findOne($order->user_id);
                                $user_info = $user_information->userDetail;
                                $order_info['user_id']       = $order->supplier_id;
                                $order_info['role']          = 2;
                                $order_info['from_user']     = $order->supplier_id;
                                $order_info['type']          = 'order';
                                $order_info['reason']         = 'new_order_'.$order->order_id;
                                $order_info['config_name']    = 'You\'ve one pending order from '.$user_info['name'];
                                $order_info['config_value']['order_id']= $order->order_id;
                                $order_info['config_value']['message']= 'You\'ve one pending order from '.$user_info['name'];
                                $library->saveNotification($order_info);
                            }
                        }
                    }
                    if($time_left<=$this->extra_time){
                        if($order->driver_id==null){
                            $order->driver;
                        }else{
                            $order_id = $order->order_id;
                            $driver_id = $order->driver_id;

                            $time = time();
                            $driver_order_status = DriverOrderStatus::find()->where(['order_id'=>$order_id,'driver_id'=>$driver_id,'order_status'=>'Pending'])->one();

                            if(!empty($driver_order_status)){
                                $remaining_time = $time-$driver_order_status->created_at;
                                if($remaining_time>=$this->time_to_wait){
                                    $data['driver_id'] = $driver_id;
                                    $data['order_id']   = $order_id;
                                    $data['order_status'] = 'Cancelled';
                                    $library->updateDriverHistory($data);

                                    $order_info['user_id']       = $driver_id;
                                    $order_info['role']          = 4;
                                    $order_info['from_user']     = $driver_id;
                                    $order_info['type']          = 'order';
                                    $order_info['config_name']    = 'Your order has been cancelled.';
                                    $order_info['config_value']['order_id']= $this->order_id;
                                    $order_info['config_value']['message']= 'Your order has been cancelled.';
                                    $library->saveNotification($order_info);

                                    $order->driver_id = null;
                                    $order->order_accepted = null;
                                    $order->save();

                                    $order->driver;
                                    $driver_order_status = DriverOrderStatus::find()->where(['order_id'=>$order_id,'driver_id'=>$driver_id,'order_status'=>'Cancelled'])->count();
                                    if($driver_order_status>0 ){
                                        if($driver_order_status%3 == 0){
                                            $order_info['user_id']       = $order->user_id;
                                            $order_info['role']          = 1;
                                            $order_info['from_user']     = $order->user_id;
                                            $order_info['type']          = 'order';
                                            $order_info['config_name']    = 'Might be there will be a bit delay in your order.Because driver is not available right now.';
                                            $order_info['config_value']['order_id']= $this->order_id;
                                            $order_info['config_value']['message']= 'Might be there will be a bit delay in your order.Because driver is not available right now.';
                                            $library->saveNotification($order_info);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}