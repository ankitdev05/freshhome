function addToFav(menu_id,value){
    $.ajax({
        url : website_url+"add-to-fav",
        type : "POST",
        dataType : "json",
        data : "menu_id="+menu_id+"&value="+value,
        success : function(json){
            if(json["success"])
                window.location.reload();
        }
    })
}
function addToFavSupp(user_id,value){
    $.ajax({
        url : website_url+"add-to-fav-supp",
        type : "POST",
        dataType : "json",
        data : "user_id="+user_id+"&value="+value,
        success : function(json){
            if(json["success"])
                window.location.reload();
        }
    })
}
function addToCart(product_id,quantity,confirm,refresh=0,showAlert=1){
    $.ajax({
        url : website_url+"add-to-cart",
        type : "POST",
        dataType : "json",
        data : "menu_id="+product_id+"&quantity="+quantity+"&confirm="+confirm,
        success : function(json){
            var code = json['code'];
            if(code===401){
                alert(json['error']['msg']);
                return false;
            }else if(json['redirect_url']){
                window.location.href = json['redirect_url'];
            }else if(json['is_allow']===0){
                $.confirm({
                    theme: 'bootstrap', // 'material', 'bootstrap'
                    title: '',
                    content: json['confirm'],
                    buttons: {
                        confirm: function () {
                            addToCart(product_id,quantity,1)
                        },
                        cancel: function () {

                        }
                    }
                });
                return false;
            }else if(json['is_allow']===1){
                $('.cart_badge').html(json['cart_items']);
                if(json["success"]){

                    $.confirm({
                        theme: 'bootstrap', // 'material', 'bootstrap'
                        title: '',
                        content: json['success'],
                        buttons: {
                           'View cart' : function () {
                               window.location.href = website_url+"cart"
                            },
                            cancel: function () {

                            }
                        }
                    });
                }
            }
            else {
                if(showAlert==1){
                    $.confirm({
                        theme: 'bootstrap', // 'material', 'bootstrap'
                        title: '',
                        content: json['success'],
                        buttons: {
                            confirm:  {
                                text: 'OK',
                                action: function(){
                                    if(refresh==1) window.location.href = website_url+"cart";
                                }
                            }
                        }
                    });
                }else
                    window.location.href = website_url+"cart";

            }
        }
    })
}
function addCart(confirm=0) {
    $.ajax({
        url : website_url+'products/add-to-cart?confirm='+confirm,
        dataType: 'json',
        type: 'POST',
        data : $('#dd-to-cart input[type=\'text\'], #add-to-cart input[type=\'hidden\'], #add-to-cart input[type=\'radio\']:checked, #add-to-cart input[type=\'checkbox\']:checked, #add-to-cart select, #add-to-cart textarea'),
        success : function(json){
            if(json["error"]){
                warningPopup(json["error"])
                return;
            }else if(json["success"]){
                warningPopup(json["success"])
                return;
            }else if(json['is_allow']===0){
                $.confirm({
                    theme: 'bootstrap', // 'material', 'bootstrap'
                    title: '',
                    content: json['confirm'],
                    buttons: {
                        confirm: function () {
                            addCart(1)
                        },
                        cancel: function () {

                        }
                    }
                });
                return false;
            }
        }
    })
}
function updateCart(id,qty) {
    $.ajax({
        url : website_url+'cart/update-cart',
        dataType: 'json',
        type: 'POST',
        data: 'id='+id+'&qty='+qty,
        success : function(json){
            location.reload();
        }
    });
}