<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
use \yii\web\Request;
use common\models\Settings;
use common\models\Countries;
use common\models\UserAddress;
use common\models\TempProImages;
use kartik\mpdf\Pdf;

$baseUrl 	= str_replace('/frontend/web', '', (new Request)->getBaseUrl());
return [
    'name'=>'Freshhomee',
    'id' => 'app-frontend',
    'language' => 'en-US',
    'sourceLanguage' => 'en-US',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'Asia/Kolkata',
    'controllerNamespace' => 'frontend\controllers',
    'on beforeAction' => function ($event) {

        $session = Yii::$app->session;
        if (!$session->has('screen_id'))
            $session->set('screen_id', 1);

        if (!$session->has('language_id'))
            $session->set('language_id', 1);

        $country = "United Arab Emirates";
        $location = Yii::$app->geoip->lookupLocation();

        $session->set('user_address', null);
        if(!yii::$app->user->isGuest){
            $user_address = UserAddress::find()->where(['user_id' => yii::$app->user->id,'is_default' => 'yes','is_delete' => 0])->asArray()->one();
            if(!empty($user_address)){
                $session->set('user_address', $user_address);
                $session->set('long', $user_address['longitude']);
                $session->set('lat', $user_address['latitude']);
            }else{
                $session->set('long', 0);
                $session->set('lat', 0);
            }
        }else{
            if(isset($location->longitude))
                $session->set('long', $location->longitude);
            if(isset($location->latitude))
                $session->set('lat', $location->longitude);
        }
        if (!$session->has('country_name')){
            if(!empty($location))
                $country_name = $location->countryName;
            else
                $country_name = "United Arab Emirates";
            if(!empty($country_name)){
                $country_data = Countries::find()->where(['status'=>1,'name'=>$country_name])->asArray()->one();
                if(!empty($country_data))
                    $country = $country_name;
            }

            $session->set('country_name', $country);
        }else{
            $country_name = $session->get('country_name');
            $country_data = Countries::find()->where(['status'=>1,'name'=>$country_name])->asArray()->one();
            if(empty($country_data))
                $session->set('country_name', $country);
//            else
//                $session->set('country_name', $country_data['name']);
        }


        $country = Countries::find()->where(['status'=>1,'name'=>$session->get('country_name')])->asArray()->one();
        @define("config_default_currency",$country['currency_code']);
        define("country_data",$country);
        $Settings	= Settings::find()->asArray()->all();
        foreach($Settings as $Setting)
            @define($Setting['settings_key'],$Setting['settings_value']);


        define("language_id",$session->get('language_id'));
        if (!Yii::$app->user->isGuest) {
            $allow = true;
            $user_id = Yii::$app->user->id;
            $controller_name = yii::$app->controller->id;
            $action_name = yii::$app->controller->action->id;
            $images = TempProImages::find()->where(['user_id'=>$user_id])->all();
            if($controller_name=='profile' && $action_name=='index')
                $allow = false;
            elseif($controller_name=='default' && $action_name=='toolbar')
                $allow = false;
            if((new Request)->post())
                $allow = false;
            if(!empty($images) && $allow==true){
                foreach($images as $image){
                    $fullurl = Yii::$app->basePath . '/web/uploads/'.$image->file_name;
                    if(file_exists($fullurl))
                        unlink($fullurl);
                    $image->delete();
                }
            }

        }
        if(language_id==2){
            Yii::$app->language = 'ar-AE';
        }

    },
    'modules' => [
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
            // other module settings, refer detailed documentation
        ]
    ],
    'components' => [
        'geoip' => [
            'class' => 'dpodium\yii2\geoip\components\CGeoIP',
        ],
        'cache' => [
            'class' => 'yii\caching\ApcCache',
            'useApcu' => true
        ],
        'pdf' => [
            'class' => Pdf::classname(),
            'format' => Pdf::FORMAT_A3,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
        ],
        'i18n' =>[
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'main.php',
                        'app/error' => 'error.php',
                    ],
                ]
            ]
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '249356604784-pd7r7m0kco1vdfoum9efggf7c5dtrpn5.apps.googleusercontent.com',
                    'clientSecret' => 'HjCaOX4scTvGEyhvIqFgdTsJ',
                    'returnUrl' => 'https://www.freshhomee.com/web/auth?authclient=google',

                ],
            ]
        ],
        'request' => [
            'baseUrl' => $baseUrl,
            'csrfParam' => '_csrf-frontend',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'wishlist' => 'profile/wishlist',
                'fav-supplier' => 'profile/fav-supplier',
                'user/<slug>' => 'user/index',
                '<alias:index|about|signup|login|auth|add-to-fav|add-to-cart|subscribe|contact-us|logout|products|checknumber|latest-products|online-kitchen|best-sellers|switch-to-supplier|switch-to-buy|auth-user|ask-for-help|guest-add-to-cart|add-to-fav-supp|hot-deals|categories|search-items|search|sell-with-us|home-food>' => 'site/<alias>',
                'pages/<slug>' => 'pages/index',
                'category/<slug>' => 'category/index',
                'brand' => 'category/brand',
                'product/<product_id:\d+>-<slug:>' => 'products/view',
            ],
        ],

    ],
    'params' => $params,
];
