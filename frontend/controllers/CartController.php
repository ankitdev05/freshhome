<?php

namespace frontend\controllers;
use common\models\Menu;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use api\models\Cart;
use common\models\Currency;
use common\models\Library;
use common\models\Order;
use common\models\CreditCards;
use common\models\UserAddress;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Product controller
 */
class CartController extends Controller
{
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','guest-update-cart'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['new-address','index','select-address','update-cart'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'new-address' => ['post'],
                    'select-address' => ['post'],
                    'update-cart' => ['post'],
                    'guest-update-cart' => ['post'],
                ]
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex(){
        $cart_type = $_GET['cart_type'] ?? '';
        $empty_cart = 1;
        $data = [];
        $user_info = [];
        $payment_code = $_POST['Order']['payment_code'] ?? '';
        $model = new Order();

        if($cart_type=='pre-order'){
            if($payment_code=='card')
                $model->scenario = 'pre_order_with_card';
            else
                $model->scenario = 'pre_order';
        }else{
            if($payment_code=='card')
                $model->scenario = 'card_add1';
            else
                $model->scenario = 'card_add';
        }

        $pre_order_data = 0;
        $online_order_data = 0;

        if (!Yii::$app->user->isGuest){
            if(isset($_POST['default_card'])){

                $model = new CreditCards();
                $result = $model->find()->where(['id' =>$_POST['default_card'],'user_id' => yii::$app->user->id])->one();
                if(!empty($result)){
                    $result->default_card = 0;
                    $result->save();
                    CreditCards::updateAll(['default_card'=>0],'user_id='.$result->user_id);

                    $result->default_card = 1;
                    if($result->save())
                        return $this->redirect(['/cart','type' => 'card','cart_type' => $cart_type]);

                }

            }
            $library = new Library();
            $user_info = Yii::$app->user->identity;
            $cart_data = $library->json_decodeArray($user_info->cart);

            if(!empty($cart_data)){
                $empty_cart = 0;
                $cart_model	= new Cart();
                $output = $cart_model->cartData($user_info->id,$cart_type);

                if($cart_type==''){
                    if(isset($output['cart']['supplier_info']['cart_data']) && !empty($output['cart']['supplier_info']['cart_data']))
                        $online_order_data = 1;
                    $output1 = $cart_model->cartData($user_info->id,'pre-order');
                    if(isset($output1['cart']['supplier_info']['cart_data']) && !empty($output1['cart']['supplier_info']['cart_data']))
                        $pre_order_data = 1;

                }
                elseif($cart_type=='pre-order'){
                    if(isset($output['cart']['supplier_info']['cart_data']) && !empty($output['cart']['supplier_info']['cart_data']))
                        $pre_order_data = 1;
                    $output1 = $cart_model->cartData($user_info->id,'');
                    if(isset($output1['cart']['supplier_info']['cart_data']) && !empty($output1['cart']['supplier_info']['cart_data']))
                        $online_order_data = 1;
                }
                if(!empty($output)){

                    if($model->load(Yii::$app->request->post()) ) {

                        $response = $model->addToCart($user_info,Yii::$app->request->post(),$cart_type);
                        if(isset($response['error']['msg'])){
                            Yii::$app->session->setFlash('warning', $response['error']['msg']);
                        }else{
                            Yii::$app->session->setFlash('success', $response['success']['msg']);
                            return $this->redirect(['/']);
                        }
                    }
                    if(isset($output['cart']['supplier_info'])){
                        $data['supplier_info'] 	= ($output['cart']['supplier_info']);
                        $data['count'] 	= count($data['supplier_info']['cart_data']);
                        $data['cart_total'] 	= array_values($output['total_amounts']);
                    }else
                        $empty_cart = 1;



                }else $empty_cart = 1;
            }else $empty_cart = 1;

        }else{
            $session = Yii::$app->session;
            $library = new Library();
            $user_info = [];
            $cart_data = $library->json_decodeArray($session->get('guest_cart'));

            if(!empty($cart_data)){
                $empty_cart = 0;
                $cart_model	= new Cart();
                $output = $cart_model->guestCartData($cart_type);

                if($cart_type==''){
                    if(isset($output['cart']['supplier_info']['cart_data']) && !empty($output['cart']['supplier_info']['cart_data']))
                        $online_order_data = 1;
                    $output1 = $cart_model->guestCartData('pre-order');
                    if(isset($output1['cart']['supplier_info']['cart_data']) && !empty($output1['cart']['supplier_info']['cart_data']))
                        $pre_order_data = 1;

                }
                elseif($cart_type=='pre-order'){
                    if(isset($output['cart']['supplier_info']['cart_data']) && !empty($output['cart']['supplier_info']['cart_data']))
                        $pre_order_data = 1;
                    $output1 = $cart_model->guestCartData('');
                    if(isset($output1['cart']['supplier_info']['cart_data']) && !empty($output1['cart']['supplier_info']['cart_data']))
                        $online_order_data = 1;
                }
                if(!empty($output)){

                    if(isset($output['cart']['supplier_info'])){
                        $data['supplier_info'] 	= ($output['cart']['supplier_info']);
                        $data['count'] 	= count($data['supplier_info']['cart_data']);
                        $data['cart_total'] 	= array_values($output['total_amounts']);
                    }else
                        $empty_cart = 1;



                }else $empty_cart = 1;
            }else $empty_cart = 1;

            if(isset($output['cart']['supplier_info']['cart_data']) && empty($output['cart']['supplier_info']['cart_data']))
                $empty_cart = 1;

            return $this->render('guest_index', ['user_info' => $user_info,'pre_order_data'=>$pre_order_data,'online_order_data'=>$online_order_data,'data' => $data, 'empty_cart' => $empty_cart,'model' => $model]);
        }
        if(isset($output['cart']['supplier_info']['cart_data']) && empty($output['cart']['supplier_info']['cart_data']))
            $empty_cart = 1;

        $cart_type = $_GET['cart_type'] ?? null;
        if(empty($online_order_data) && !empty($pre_order_data) && empty($cart_type))
           return $this->redirect(['/cart','cart_type' => 'pre-order']);
        return $this->render('index', ['pre_order_data'=>$pre_order_data,'online_order_data'=>$online_order_data,'user_info'=>$user_info,'data' => $data, 'empty_cart' => $empty_cart,'model' => $model]);
    }

    public function actionNewAddress(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = [];
        $model = new UserAddress();
        if (Yii::$app->request->isAjax) {

            $json['html'] = $this->renderAjax('//profile/_form_address',['model'=>$model,'isAjax'=>true]);
        }else{
            $model->user_id = Yii::$app->user->id;
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if($model->save()){
                    UserAddress::updateAll(['is_default'=>'no'],'user_id='.$model->user_id);
                    $model->is_default = 'yes';
                    $model->save();

                    Yii::$app->session->setFlash('success', 'Your address has been saved successfully.');
                    return $this->redirect(['/cart']);
                }
            }else{
                $all_errors	= [];
                $errors	= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $error	= !empty($all_errors) ? $all_errors[0] : '' ;
                Yii::$app->session->setFlash('success', $error);
                return $this->redirect(Yii::$app->request->referrer ?: $this->redirect(['/cart']));
            }
        }
        return $json;
    }

    public function actionSelectAddress(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            $json = [];
            $model = new UserAddress();

            $results = $model->find()->where(['is_delete'=>0,'user_id' => yii::$app->user->id])->all();
            $json['html'] = $this->renderAjax('_select_address',['results'=>$results]);
            return $json;
        }else{
            if(isset($_POST['user_addr'])){
                $model = new UserAddress();
                $result = $model->find()->where(['address_id' =>$_POST['user_addr'],'user_id' => yii::$app->user->id])->one();
                if(!empty($result)){
                    $result->is_default = 'no';
                    $result->save();
                    UserAddress::updateAll(['is_default'=>'no'],'user_id='.$result->user_id);
                    $result->is_default = 'yes';
                    $result->save();
                }
                return $this->redirect(Yii::$app->request->referrer ?: $this->redirect(['/cart']));


            }
        }
    }
    public function actionGuestUpdateCart(){
        $session = Yii::$app->session;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = [];
        $post_data = Yii::$app->request->post();
        $error = false;
        if (Yii::$app->request->isAjax) {
            $id = $post_data['id'] ?? 0;
            $qty = $post_data['qty'] ?? 0;
            if($id==0){
                $error = true;
            }else{
                $menu_info = Menu::findOne($id);
                if(!$menu_info)
                    $error = true;
                if($error==false){

                    $library = new Library();
                    $cart = $library->json_decodeArray($session->get('guest_cart'));
                    if(isset($cart[$menu_info->supplier_id])){
                        if($qty==0){

                            unset($cart[$menu_info->supplier_id][$_POST['id']]);
                            $cart_val = $library->json_encodeArray($cart);
                        }else{

                            if( isset($cart[$menu_info->supplier_id][$id]['product_attributes'])){
                                $product_attributes = $cart[$menu_info->supplier_id][$id]['product_attributes'];
                                $post_data['Cart']['product_attributes'] = $product_attributes;
                                $post_data['Cart']['quantity'] = $qty;
                            }
                            if(isset($post_data['Cart'])){
                                $validate_cart = $menu_info->validateCart($post_data['Cart']);
                                if(!empty($validate_cart)){
                                    if(isset($validate_cart['error'])){
                                        $json['error'] = $validate_cart['error'];
                                        Yii::$app->session->setFlash('success', $json['error']);
                                        return;
                                    }
                                }
                            }

                            $cart[$menu_info->supplier_id][$id]['quantity'] = $qty;
                            $cart_val = $library->json_encodeArray($cart);
                        }

                        $session->set('guest_cart', $cart_val);
                        $json['success'] = 'Your cart has been updated successfully.';
                        Yii::$app->session->setFlash('success', $json['success']);
                    }
                }
            }
        }
        return $json;
    }
    public function actionUpdateCart(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = [];
        $post_data = Yii::$app->request->post();
        $error = false;
        if (Yii::$app->request->isAjax) {
            $id = $post_data['id'] ?? 0;
            $qty = $post_data['qty'] ?? 0;
            if($id==0){
                $error = true;
            }else{
                $menu_info = Menu::findOne($id);
                if(!$menu_info)
                    $error = true;
                if($error==false){
                    $user_info = Yii::$app->user->identity;
                    $library = new Library();
                    $cart = $library->json_decodeArray($user_info->cart);
                    if(isset($cart[$menu_info->supplier_id])){
                        if($qty==0){

                            unset($cart[$menu_info->supplier_id][$_POST['id']]);
                            $cart_val = $library->json_encodeArray($cart);
                        }else{

                            if( isset($cart[$menu_info->supplier_id][$id]['product_attributes'])){
                                $product_attributes = $cart[$menu_info->supplier_id][$id]['product_attributes'];
                                $post_data['Cart']['product_attributes'] = $product_attributes;
                                $post_data['Cart']['quantity'] = $qty;
                            }
                            if(isset($post_data['Cart'])){
                                $validate_cart = $menu_info->validateCart($post_data['Cart']);
                                if(!empty($validate_cart)){
                                    if(isset($validate_cart['error'])){
                                        $json['error'] = $validate_cart['error'];
                                        Yii::$app->session->setFlash('success', $json['error']);
                                        return;
                                    }
                                }
                            }

                            $cart[$menu_info->supplier_id][$id]['quantity'] = $qty;
                            $cart_val = $library->json_encodeArray($cart);
                        }

                        $user_info->cart = $cart_val;
                        $user_info->save();
                        $json['success'] = 'Your cart has been updated successfully.';
                        Yii::$app->session->setFlash('success', $json['success']);
                    }
                }
            }
        }
        return $json;
    }
}