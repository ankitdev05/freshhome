<?php

namespace frontend\controllers;
use common\models\Brand;
use common\models\Menu;
use Yii;
use common\models\Category;
use common\models\BrandCategory;
use common\models\MenuCategories;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
/**
 * Category controller
 */
class CategoryController extends Controller
{
    public $page_size=12;
    public function actionIndex($slug=null,$category_id=null){
        $category =  Category::find();
        $query = $category->select('*')->joinWith('categoryDescription as cd')->where(['active' => 1,'cd.language_id' => language_id]);
        if(!empty($slug))
            $query->andWhere(['tbl_category.slug' => $slug]);
        elseif(!empty($category_id))
            $query->andWhere(['tbl_category.category_id' => $category_id]);
        else
            $query->andWhere(['category_id' => 0]);
        $result = $query->one();
        if(!$result)
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));


        $query = Category::find()->joinWith('categoryDescription as cd')->where(['and',['active' => 1],['parent_id' => $result->category_id],['cd.language_id' => language_id]]);
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
        ]);

        $query = BrandCategory::find()->joinWith(['brand','brand.brandDescription as bd'])->where(['in','category_id',$result->category_id])->andWhere(['bd.language_id' => language_id]);
        $brandDataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
        ]);

        $query = MenuCategories::find()->joinWith('menu as menu')->where(['in','category_id', $result->category_id])->groupBy('menu.supplier_id');
        $supplierDataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
        ]);


        return $this->render('index',['result' => $result,'dataProvider' => $dataProvider,'brandDataProvider' => $brandDataProvider,'supplierDataProvider' => $supplierDataProvider]);
    }

    public function actionBrand($brand_id){
        $sort_order = ['id' => SORT_DESC];
        $result = Brand::find()->joinWith('brandDescription as bd')->where(['tbl_brand.brand_id' => $brand_id,'bd.language_id' => language_id,'tbl_brand.status' => 1])->one();
        if(!empty($result)){
            $query = Menu::find()->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0,'tbl_menu.brand_id'=>$brand_id],['>','subscription_end',time()]]);
            if (!Yii::$app->user->isGuest)
                $query->andWhere(['!=','supplier_id',Yii::$app->user->identity->id]);
            $dataProvider	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => $sort_order
                ],
            ]);

            return $this->render('brand',['result' => $result,'dataProvider' => $dataProvider]);
        }else
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}