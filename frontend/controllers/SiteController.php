<?php
namespace frontend\controllers;

use common\models\Order;
use common\models\UserFavMenu;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use frontend\components\AuthHandler;
use common\models\{Pages,
    User,
    SortMethod,
    Menu,
    Library,
    UserAddress,
    UserRole,
    MainCategories,
    Subscriptions,
    LoginForm,
    Apiuser,
    Brand,
    Category,
    Braintree};
use common\models\HelpRequests;
use common\models\Countries;
use common\models\UserFavSupplier;
use common\models\MenuToAttributes;
use common\models\SubscriptionTransaction;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Search;
use api\models\Cart;
use Faker;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
/**
 * Site controller
 */
class SiteController extends Controller
{
    public $screen_id;
    public $page_size=12;
    public $statusCode = 200;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['add-to-fav-supp','logout', 'add-to-fav', 'add-to-cart','switch-to-supplier','switch-to-buy'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'checknumber' => ['post'],
                    'subscribe' => ['post'],
                    'logout' => ['post'],
                    'add-to-fav' => ['post'],
                    'add-to-fav-supp' => ['post'],
                    'add-to-cart' => ['post'],
                    'guest-add-to-cart' => ['post'],
                    'search-items' => ['GET'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function beforeAction($action) {
        if($action->id == 'btree-webhook') {
            Yii::$app->request->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $lang = $_GET['lang'] ?? 0;
        $country = $_GET['country'] ?? 0;
        $session = Yii::$app->session;
        if(isset($_GET['UserAddress']['is_default']) && !yii::$app->user->isGuest){
            $user_id = Yii::$app->user->id;
            $address_id = $_GET['UserAddress']['is_default'];
            $model		= new UserAddress();
            $result = $model->find()->where(['user_id'=> $user_id,'address_id'=>$address_id,'is_delete'=>0])->one();
            if(!empty($result)){
                UserAddress::updateAll(['is_default'=>'no'],'user_id='.$user_id);
                $result->is_default = 'yes';
                $result->save();
                return $this->goBack();
            }
        }
        if(!empty($lang)){
            $lang_id  = 1;
            if($lang=='ar')
                $lang_id  = 2;
            $session->set('language_id', $lang_id);
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);

        }
        if(!empty($country)){
            $country_data = Countries::find()->where(['iso_code_2' => $country,'status' => 1])->asArray()->one();
            $session->set('country_name', $country_data['name']);
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
        if(isset($_REQUEST['menu_type'])){
            $menu_category = new MainCategories();
            $menu_category = $menu_category->find()->where(['status' => 1,'code' => $_REQUEST['menu_type']])->asArray()->one();
            if(!empty($menu_category)){
                $session = Yii::$app->session;
                $session->set('screen_id', $menu_category['id']);
                return $this->redirect(Yii::$app->request->baseUrl.'/products');
                //return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
            }
        }
        if(isset($_REQUEST['u'])){
            $user_info = User::findByUsername($_REQUEST['u']);

            if(!empty($user_info)){
                $cookies = Yii::$app->response->cookies;
                $cookies->readOnly = false;
                $cookies->add(new \yii\web\Cookie([
                    'name' => 'refer_by',
                    'value' => $user_info->id,
                    'expire' => time()+2592000,
                ]));

            }
        }

        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $session = Yii::$app->session;
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $json = [];
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($model->load(Yii::$app->request->post()) && $model->login(null)) {
                if($session->has('guest_cart')){
                    $user_info = Yii::$app->user->identity;
                    $user_info->cart = $session->get('guest_cart');
                    $user_info->save();
                }
                $json['success'] = true;
            }else{
                $all_errors	= [];
                $errors	= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']	= !empty($all_errors) ? $all_errors[0] : '' ;

            }
            return $json;
        }else{

            if ($model->load(Yii::$app->request->post()) && $model->login(null)) {

                $session->set('is_supplier', true);
                return $this->goBack();
            } else {
                $model->password = '';

                return $this->render('login', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContactUs()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(config_email)) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {

        $model = new SignupForm();
        $model->scenario = 'registration';
        $model->role = 1;
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $json = [];
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($model->load(Yii::$app->request->post()) && $model->signup()) {
                Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
                $json['success'] = true;
            }else{
                $all_errors	= [];
                $errors	= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']	= !empty($all_errors) ? $all_errors[0] : '' ;
            }
            return $json;
        }else{
            $model = new User();
            $model->scenario = 'checknumber';

            if (isset($_POST['signup-button-1']) && $model->load(Yii::$app->request->post()) && $model->validate()) {
                $phone_number = $model->phone_number;

                $model = new User();
                $model->scenario = 'checknumber1';

                $model->phone_number = $phone_number;

                return $this->render('signup/_step2',['model' => $model]);
            }elseif(isset($_POST['g-recaptcha-response'])){
                $phone_number = '';
                if(isset($_POST['User']['phone_number']))
                    $phone_number = $_POST['User']['phone_number'];
                if(isset($_POST['SignupForm']['phone_number']))
                    $phone_number = $_POST['SignupForm']['phone_number'];

                if(empty($phone_number)){
                    return $this->refresh();
                }

                $model = new SignupForm();
                $model->scenario = 'supplier_registration';
                $model->role = 2;
                $model->phone_number = $phone_number;

                if ($model->load(Yii::$app->request->post()) && $model->signUpSupplier()) {
                    Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
                    return $this->goHome();
                }
                return $this->render('signup/_step3',['model' => $model]);
            }
            return $this->render('signup/_step2',['model' => $model]);


            if ($model->load(Yii::$app->request->post()) && $model->signup()) {
                Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
                return $this->goHome();
            }

            return $this->render('signup', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {

                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            $role_info	= UserRole::findOne($user->role);
            $code		= '';
            if(!empty($role_info)){
                $umodel		= new User();
                $prefix		= strtolower($role_info->code);
                $code		= $umodel->genrateCode($prefix);
            }

            $user->share_id		        = $code;
            $user->status 	 	        = 10;
            $user->email_verify 	    = 1;
            $user->last_logged_in_as    = 1;
            $user->save();

            if (Yii::$app->user->login($user)) {
                User::deleteAll(['and',['phone_number' => $user->phone_number],['NOT in','id',$user->id]]);
                $user->loginActivity($user);
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');

                $session = Yii::$app->session;
                if($session->has('guest_cart')){
                    $user->cart = $session->get('guest_cart');
                    $user->save();
                }
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }
    public function onAuthSuccess($client){
        (new AuthHandler($client))->handle();
    }

    public function actionAddToFavSupp(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json['success'] = false;
        $post_data = Yii::$app->request->post();
        $supplier_id = $post_data['user_id'] ?? null;
        $value = $post_data['value'] ?? null;
        if(!empty($supplier_id)){
            if($value==0){
                UserFavSupplier::deleteAll(['user_id' => Yii::$app->user->identity->id, 'supplier_id'=>$supplier_id]);
                Yii::$app->session->setFlash('success', 'Successfully removed from favorite.');
            }
            if($value==1){
                $model = new UserFavSupplier();
                $model->user_id = Yii::$app->user->identity->id;
                $model->supplier_id = $supplier_id;
                $model->save();
                Yii::$app->session->setFlash('success', 'Successfully added in your favorite list.');
            }
            $json['success'] = true;

        }

        return $json;
    }

    public function actionAddToFav(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json['success'] = false;
        $post_data = Yii::$app->request->post();
        $menu_id = $post_data['menu_id'] ?? null;
        $value = $post_data['value'] ?? null;
        if(!empty($menu_id)){
            if($value==0){
                UserFavMenu::deleteAll(['user_id' => Yii::$app->user->identity->id, 'menu_id'=>$menu_id]);
                Yii::$app->session->setFlash('success', 'Product successfully removed.');
            }
            if($value==1){
                $model = new UserFavMenu();
                $model->user_id = Yii::$app->user->identity->id;
                $model->menu_id = $menu_id;
                $model->save();
                Yii::$app->session->setFlash('success', 'Successfully added in your wishlist.');
            }
            $json['success'] = true;

        }

        return $json;
    }
    public function actionGuestAddToCart(){
        $session = Yii::$app->session;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $confirm = 0;
        $model = new Cart();
        $library = new Library();
        $post_data['Cart']	= Yii::$app->request->post();
        if(isset($post_data['Cart']['confirm']) && $post_data['Cart']['confirm']==1)
            $confirm = 1;
        $cart	= $library->json_decodeArray($session->get('guest_cart'));
        if($model->load($post_data) && $model->validate()) {
            $menu_id 	= $model->menu_id;
            $quantity 	= (int)$model->quantity;
            $result		= Menu::find()->where(['id'=>$menu_id])->one();
            if(!empty($result)){
                if($quantity==0){
                    $json['success']		= 'Your product has been removed successfully.';
                    unset($cart[$result->supplier_id][$menu_id]);
                    if(empty($cart[$result->supplier_id]))
                        unset($cart[$result->supplier_id]);
                    if(!empty($cart))
                        $cart_val = $library->json_encodeArray($cart);
                    else
                        $cart_val = null;
                    $session->set('guest_cart', $cart_val);
                }else{
                    if(!empty($cart)){
                        if(!isset($cart[$result->supplier_id])){
                            $supplier_info = user::findOne($result->supplier_id);
                            if($confirm==0){
                                $json['is_allow'] 		= 0;
                                $json['confirm']		= 'Your cart contains items from '.$supplier_info->name.' - '.$supplier_info->location.'. Do you wish to clear your cart and start a new order here?';
                                $json['code']	= $this->statusCode;

                                if($this->statusCode==200){
                                    $cart_items = 0;
                                    $cart_total = (object)[];
                                    $cart = new Cart();
                                    $total_items = $cart->getGuestTotalCartItems();
                                    if(isset($total_items['total_items'])){
                                        $cart_items = $total_items['total_items'];
                                        $cart_total = $total_items['total_price'];
                                    }
                                    $json['cart_items'] = $cart_items;
                                    $json['cart_total'] = $cart_total;
                                }
                                return $json;
                            }
                            if($confirm==1)
                                $cart = [];

                        }
                    }
                    $menu_to_attr_count = MenuToAttributes::find()->where(['menu_id' => (int)$menu_id])->count();
                    if($menu_to_attr_count>0){
                        $json['redirect_url'] = $result->proUrl;
                    }else{
                        $cart[$result->supplier_id][$menu_id]	= ['menu_id'=>(int)$menu_id,'quantity'=>(int)$quantity];
                        $cart_val = $library->json_encodeArray($cart);
                        $session->set('guest_cart', $cart_val);
                        $json['is_allow'] 		= 1;

                        $json['success'] = 'Your cart has been updated successfully.';
                        //Yii::$app->session->setFlash('success', $json['success']);
                    }

                }

            }else{
                $json['error']['msg']	= 'Sorry this dish does not exist or currently not active.';
                $this->statusCode		= 401;
            }
        }else{
            $all_errors				= [];
            $errors					= $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode		= 401;
        }
        $json['code']	= $this->statusCode;
        if($this->statusCode==200){
            $cart_items = 0;
            $cart_total = (object)[];
            $cart = new Cart();
            $total_items = $cart->getGuestTotalCartItems();
            if(isset($total_items['total_items'])){
                $cart_items = $total_items['total_items'];
                $cart_total = $total_items['total_price'];
            }
            $json['cart_items'] = $cart_items;
            $json['cart_total'] = $cart_total;
        }
        return $json;

    }
    public function actionAddToCart(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $confirm			= 0;
        $model				= new Cart();
        $library			= new Library();

        $user_info			= Yii::$app->user->identity;
        $user_id			= $user_info->id;
        $post_data['Cart']	= Yii::$app->request->post();

        if(isset($post_data['Cart']['confirm']) && $post_data['Cart']['confirm']==1)
            $confirm = 1;

        $cart	= $library->json_decodeArray($user_info->cart);
        if($model->load($post_data) && $model->validate()) {
            $menu_id 	= $model->menu_id;
            $quantity 	= (int)$model->quantity;
            $result		= Menu::find()->where(['id'=>$menu_id])->one();
            if(!empty($result)){
                if($result->product_type!=1 ){
                    if(($result->status=='inactive' || $result->dist_qty==0) )
                        $result = [];
                }

            }

            if(!empty($result)){
                if($quantity==0){
                    $json['success']		= 'Your product has been removed successfully.';
                    unset($cart[$result->supplier_id][$menu_id]);
                    if(empty($cart[$result->supplier_id]))
                        unset($cart[$result->supplier_id]);
                    if(!empty($cart))
                        $cart_val = $library->json_encodeArray($cart);
                    else
                        $cart_val = null;
                    $user_info->cart = $cart_val;
                    $user_info->save();
                }else{
                    if(!empty($cart)){
                        if(!isset($cart[$result->supplier_id])){
                            $supplier_info = user::findOne($result->supplier_id);
                            if($confirm==0){
                                $json['is_allow'] 		= 0;
                                $json['confirm']		= 'Your cart contains items from '.$supplier_info->name.' - '.$supplier_info->location.'. Do you wish to clear your cart and start a new order here?';
                                $json['code']	= $this->statusCode;

                                if($this->statusCode==200){
                                    $cart_items = 0;
                                    $cart_total = (object)[];
                                    $cart = new Cart();
                                    $total_items = $cart->getTotalCartItems($user_info->id);
                                    if(isset($total_items['total_items'])){
                                        $cart_items = $total_items['total_items'];
                                        $cart_total = $total_items['total_price'];
                                    }
                                    $json['cart_items'] = $cart_items;
                                    $json['cart_total'] = $cart_total;
                                }
                                return $json;
                            }
                            if($confirm==1)
                                $cart = [];

                        }
                    }
                    $menu_to_attr_count = MenuToAttributes::find()->where(['menu_id' => (int)$menu_id])->count();
                    if($menu_to_attr_count>0){
                        $json['redirect_url'] = $result->proUrl;
                    }else{
                        $cart[$result->supplier_id][$menu_id]	= ['menu_id'=>(int)$menu_id,'quantity'=>(int)$quantity];
                        $cart_val = $library->json_encodeArray($cart);
                        $user_info->cart = $cart_val;
                        $user_info->save();
                        $json['is_allow'] 		= 1;

                        $json['success'] = 'Your cart has been updated successfully.';
                        //Yii::$app->session->setFlash('success', $json['success']);
                    }

                }

            }else{
                $json['error']['msg']	= 'Sorry this product does not exist or currently not active.';
                $this->statusCode		= 401;
            }


        }else{
            $all_errors				= [];
            $errors					= $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode		= 401;
        }
        $json['code']	= $this->statusCode;
        if($this->statusCode==200){
            $cart_items = 0;
            $cart_total = (object)[];
            $cart = new Cart();
            $total_items = $cart->getTotalCartItems($user_info->id);
            if(isset($total_items['total_items'])){
                $cart_items = $total_items['total_items'];
                $cart_total = $total_items['total_price'];
            }
            $json['cart_items'] = $cart_items;
            $json['cart_total'] = $cart_total;
        }
        return $json;
    }

    public function actionSubscribe(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Subscriptions();
        $model->ip_address = Yii::$app->getRequest()->getUserIP();
        $model->user_agent = Yii::$app->getRequest()->getUserAgent();
        $model->status = 1;
        if ($model->load(yii::$app->request->post()) && $model->validate()) {
            $result = $model->findOne(['email'=>$_POST['Subscriptions']['email']]);
            if(!empty($result)){
                $result->status  = 1;
                $result->save();
                Yii::$app->session->setFlash('success', 'Thanks for your subscription.');
            }else{
                Yii::$app->session->setFlash('success', 'Thanks for your subscription.');
                $model->save();
            }
            $json['success'] = true;
        }else{
            $all_errors				= [];
            $errors					= $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']= !empty($all_errors) ? $all_errors[0] : '' ;
        }
        return $json;
    }

    public function actionChecknumber(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new User();
        $model->scenario = 'checknumber';
        if ($model->load(yii::$app->request->post()) && $model->validate()) {
            $json['success'] = true;
            $json['message'] = 'Please Enter The OTP code sent to '.$model->phone_number;
        }else{
            $all_errors				= [];
            $errors					= $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']= !empty($all_errors) ? $all_errors[0] : '' ;
        }
        return $json;
    }

    public function actionProducts(){
        $session = Yii::$app->session;
        $screen_id = $session->get('screen_id', 1);
        $sort = $_GET['Filters']['sort'] ?? null;
        $screen = $_GET['Filters']['screen_id'] ?? null;
        $city = $_GET['Filters']['city'] ?? null;
        $title = $_GET['Filters']['title'] ?? null;
        $categories = $_GET['Filters']['categories'] ?? null;

        $main_category = MainCategories::findOne($screen_id);
        if(!empty($screen)){
            $category = MainCategories::findOne($screen);
            if(!empty($category)){
                $screen_id = $screen;
                $main_category = $category;
            }
        }


        $query = Menu::find()->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0,'tbl_menu.product_type'=>$screen_id],['>','subscription_end',time()]]);
        if (!Yii::$app->user->isGuest)
            $query->andWhere(['!=','supplier_id',Yii::$app->user->identity->id]);
        if(!empty($city))
            $query->andWhere(['city_id'=>$city]);
        if(!empty($title)){
            $query->andWhere(['like','dish_name',$title]);
            //$query->joinWith(['menuCategories.category']);
            //$query->andWhere(['or',['like','dish_name',$title],['like','tbl_category.name',$title]]);
        }
        if(!empty($categories)){
            $query->joinWith(['menuCategories as m2c']);
            $query->andWhere(['m2c.category_id'=>$categories]);
        }
        $sort_order = ['dish_name' => SORT_ASC];
        if(!empty($sort)){
            $sort_method = Sortmethod::findOne($sort);
            if(!empty($sort_method)){
                switch ($sort) {
                    case 6:
                        $sort_order = ['dish_price' => SORT_ASC];
                        break;
                    case 7:
                        $sort_order = ['dish_price' => SORT_DESC];
                        break;
                    case 8:
                        $sort_order = ['id' => SORT_DESC];
                        break;
                }
            }

        }
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => $sort_order
            ],
        ]);
        return $this->render('products',['main_category' => $main_category, 'dataProvider' => $dataProvider]);
    }
    public function actionLatestProducts(){
        $session = Yii::$app->session;
        $screen_id = $session->get('screen_id', 1);
        $long = $session->get('long');
        $lat = $session->get('lat');
        $sort_order = ['id' => SORT_DESC];
        if(empty($long) && empty($lat)){
            $query = Menu::find()->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0,'tbl_menu.product_type'=>$screen_id],['>','subscription_end',time()]]);
        }else{
            $query = Menu::find()->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance  "))->joinWith('supplierInfo')->where(['and',
                ['tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0, 'tbl_menu.product_type' => $screen_id],
                ['>', 'subscription_end', time()]
            ])->orderBy('distance');

        }

        if (!Yii::$app->user->isGuest)
            $query->andWhere(['!=','supplier_id',Yii::$app->user->identity->id]);

        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => $sort_order
            ],
        ]);
        return $this->render('latest_products',['dataProvider' => $dataProvider]);
    }
    public function actionBestSellers(){
        $session = Yii::$app->session;
        $screen_id = $session->get('screen_id', 1);
        $long = $session->get('long');
        $lat = $session->get('lat');

        if(empty($long) && empty($lat)){
            $query = Menu::find()->joinWith('supplierInfo')->select(new Expression('tbl_menu.id,dish_name,slug,dish_description,dish_image,dish_price,dish_weight,dish_since,SUM(op.quantity) AS TotalQuantity,op.created_at as cdate'))->joinWith('orderProducts as op')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0,'tbl_menu.product_type'=>$screen_id],['>','subscription_end',time()]])->groupBy('tbl_menu.id')->orderBy('TotalQuantity desc')->limit(10);
        }else{
            $query = Menu::find()->joinWith('supplierInfo')->select(new Expression("(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance,tbl_menu.id,dish_name,slug,dish_description,dish_image,dish_price,dish_weight,dish_since,SUM(op.quantity) AS TotalQuantity,op.created_at as cdate"))->joinWith('orderProducts as op')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0,'tbl_menu.product_type'=>$screen_id],['>','subscription_end',time()]])->groupBy('tbl_menu.id')->orderBy('TotalQuantity desc')->limit(10);
        }

        if (!Yii::$app->user->isGuest)
            $query->andWhere(['!=','supplier_id',Yii::$app->user->identity->id]);


        $sort_order = ['id' => SORT_DESC];

        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => $sort_order
            ],
        ]);

        return $this->render('best_sellers',['dataProvider' => $dataProvider]);
    }
    public function actionOnlineKitchen(){
        $session = Yii::$app->session;
        $long = $session->get('long');
        $lat = $session->get('lat');

        $screen_id = $session->get('screen_id', 1);
        $sort_order = ['id' => SORT_DESC];
        if(empty($long) && empty($lat)){
            $query = User::find()->where(['availability'=>'online','status'=>10,'is_test_user'=>0,'is_supplier'=>'yes','supplier_type'=>$screen_id])->andWhere(['>','subscription_end',time()]);
        }else{
            $query = User::find()->select(new Expression("tbl_user.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance  "))->where(['availability'=>'online','status'=>10,'is_test_user'=>0,'is_supplier'=>'yes','supplier_type'=>$screen_id])->andWhere(['>','subscription_end',time()])->orderBy('distance');

        }

        if (!Yii::$app->user->isGuest)
            $query->andWhere(['!=','id',Yii::$app->user->identity->id]);

        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => $sort_order
            ],
        ]);
        return $this->render('online_kitchen',['dataProvider' => $dataProvider]);
    }

    public function actionSwitchToSupplier(){
        $user_info = Yii::$app->user->identity;
        if($user_info->is_supplier=="no"){
            $model = User::findOne($user_info->id);
            $model->scenario = 'switch_to_supplier';
            $model->is_supplier = 'yes';
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $session = Yii::$app->session;
                $session->set('is_supplier', true);
                Yii::$app->session->setFlash('success', 'Your data has been saved.');
                return $this->redirect(Yii::$app->request->baseUrl.'/supplier');
            }
            return $this->render('switch_to_supplier',['model' => $model]);
        }else{
            $session = Yii::$app->session;
            $session->set('is_supplier', true);
            return $this->redirect(Yii::$app->request->baseUrl.'/supplier');
        }
    }
    public function actionSwitchToBuy(){
        $session = Yii::$app->session;
        $session->set('is_supplier', null);
        return $this->goHome();
    }

    public function actionAuthUser($token,$product_id){
        Yii::$app->user->logout();
        $allow = false;
        $user = User::find()->where(['status' => 10,'auth_key' => $token])->one();
        if(!empty($user)){
            $product = Menu::find()->where(['id' => $product_id,'supplier_id' => $user->id])->one();
            if(!empty($product))
                $allow = true;
        }
        if($allow==true){
            Yii::$app->user->login($user, 3600 * 24 * 30);
            $session = Yii::$app->session;
            $session->set('is_supplier', true);
            if($product->product_type==1)
                return $this->redirect(['/supplier/updatemenu?id='.$product_id]);
            else  return $this->redirect(['/supplier?type=updateProduct&id='.$product_id]);

        }else return $this->goHome();

    }

    public function actionFakeDrivers(){
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 5; $i++) {
            $name = $faker->name;
            $model = new Apiuser();
            $model->scenario = 'driver_register';
            $post_data['Apiuser']['document'] = 'abc.jpeg';
            $post_data['Apiuser']['status'] = 10;
            $post_data['Apiuser']['role'] = 4;
            $post_data['Apiuser']['dob'] = '1988-07-11';
            $post_data['Apiuser']['name'] = $name;
            $post_data['Apiuser']['username'] = $faker->userName;
            $post_data['Apiuser']['phone_number'] = $faker->phoneNumber;
            $post_data['Apiuser']['email'] = $faker->freeEmail;
            $post_data['Apiuser']['password'] = $post_data['Apiuser']['username'];
            $post_data['Apiuser']['latitude'] = $faker->latitude($min = -90, $max = 90);
            $post_data['Apiuser']['longitude'] = $faker->latitude($min = -180, $max = 180);
            $post_data['Apiuser']['location'] = $faker->address;
            $post_data['Apiuser']['city'] = 1;
            $model->setPassword($post_data['Apiuser']['password']);
            $model->email_verify = 1;

            $model->availability = 'offline';
            $model->is_user		= 'yes';
            $model->is_supplier	= 'no';


                $model->is_user = 'yes';

                $model->is_driver = 'yes';

            $model->ip_address		= Yii::$app->getRequest()->getUserIP();
            $model->last_login		= time();
            $model->last_login_ip	= Yii::$app->getRequest()->getUserIP();



            $model->generateAuthKey();
            if($model->load($post_data) && $model->save()){

            }else{
                echo '<pre>';
                print_r($model->getErrors());
                echo '</pre>';
            }

            echo '<pre>';
                print_r($post_data);
            echo '</pre>';
        }

    }

    public function actionFakeSuppliers(){
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 5; $i++) {
            $name = $faker->name;
            $model = new Apiuser();
            $model->scenario = 'driver_register';
            $post_data['Apiuser']['document'] = 'abc.jpeg';
            $post_data['Apiuser']['status'] = 10;
            $post_data['Apiuser']['role'] = 2;
            $post_data['Apiuser']['supplier_type'] = 2;
            $post_data['Apiuser']['dob'] = '1988-07-11';
            $post_data['Apiuser']['name'] = $name;
            $post_data['Apiuser']['username'] = $faker->userName;
            $post_data['Apiuser']['phone_number'] = $faker->phoneNumber;
            $post_data['Apiuser']['email'] = $faker->freeEmail;
            $post_data['Apiuser']['password'] = $post_data['Apiuser']['username'];
            $post_data['Apiuser']['latitude'] = $faker->latitude($min = -90, $max = 90);
            $post_data['Apiuser']['longitude'] = $faker->latitude($min = -180, $max = 180);
            $post_data['Apiuser']['location'] = $faker->address;
            $post_data['Apiuser']['city'] = 1;
            $model->setPassword($post_data['Apiuser']['password']);
            $model->email_verify = 1;

            $model->availability = 'online';
            $model->is_user		= 'yes';
            $model->is_supplier	= 'yes';


            $model->ip_address		= Yii::$app->getRequest()->getUserIP();
            $model->last_login		= time();
            $model->last_login_ip	= Yii::$app->getRequest()->getUserIP();



            $model->generateAuthKey();
            if($model->load($post_data) && $model->save()){

            }else{
                echo '<pre>';
                print_r($model->getErrors());
                echo '</pre>';
            }

            echo '<pre>';
            print_r($post_data);
            echo '</pre>';
        }

    }

    public function actionAddProducts(){
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 5; $i++) {
          // $faker->seed(rand(0,5));
            $post_data['Menu']['supplier_id'] = 119;
            $post_data['Menu']['image_approval'] = 1;
            $post_data['Menu']['dish_name'] = $faker->firstNameFemale;
            $post_data['Menu']['dist_qty'] = $faker->randomNumber(3);
            $post_data['Menu']['dish_price'] = $faker->randomNumber(2);
            $post_data['Menu']['collected_amount'] = $post_data['Menu']['dish_price'];
            $post_data['Menu']['type_of_product'] = 'hand';
            $post_data['Menu']['dish_image'] = 'menu/05b417ab845b7dc78c0c57166a75e5b0.png';
            $post_data['Menu']['dish_description'] = $faker->realText($maxNbChars = 200, $indexSize = 2);
            $model = new Menu();
            if ($model->load($post_data) && $model->save()) {

            }else{
                echo '<pre>';
                print_r($model->getErrors());
                echo '</pre>';
            }
            echo '<pre>';
            print_r($post_data);
            echo '</pre>';
        }
    }

    public function actionBtreeWebhook(){
        $this->enableCsrfValidation = false;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = [];
        if (isset($_POST["bt_signature"]) && isset($_POST["bt_payload"])) {
            $model= new Braintree();
            $response = $model->webHookResponse($_POST["bt_signature"],$_POST["bt_payload"]);
            $kind = $response['response']['kind'];

//            $message ='<pre>';
//            $message .= print_r($response,true);
//            $message .='</pre>';
//            Yii::$app->mailer->compose()
//                ->setHtmlBody($message)
//                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
//                ->setTo('oberai.rahul6676@gmail.com')
//                ->setSubject('Webhook '.$response['response']['subject']['subscription']['id'])
//                ->send();
            if($kind=='subscription_charged_successfully'){
                if(isset($response['response']['subject']['subscription']['transactions'][0]['customer']['id'])){
                    $user_id = str_replace('fresh_', '', $response['response']['subject']['subscription']['transactions'][0]['customer']['id']);
                    $user_info = User::find()->where(['id' => $user_id])->one();
                }else{
                    $subscription_id = $response['response']['subject']['subscription']['id'];
                    $user_info = User::find()->where(['subscription_id' => $subscription_id])->one();
                }
                if(!empty($user_info)){

                    $subscription_id = $response['response']['subject']['subscription']['id'];
                    $payment_token = $response['response']['subject']['subscription']['transactions'][0]['id'];
                    $transaction_id = $response['response']['subject']['subscription']['transactions'][0]['id'];
                    $transaction_status = $response['response']['subject']['subscription']['transactions'][0]['status'];
                    $transaction_currency = $response['response']['subject']['subscription']['transactions'][0]['currencyIsoCode'];
                    $transaction_amount = $response['response']['subject']['subscription']['transactions'][0]['amount'];

                    $full_response = serialize($response);

                    $model = new SubscriptionTransaction();
                    $model->user_id = $user_info->id;
                    $model->payment_token = $payment_token;
                    $model->subscription_id = $subscription_id;
                    $model->transaction_id = $transaction_id;
                    $model->transaction_status = $transaction_status;
                    $model->transaction_currency = $transaction_currency;
                    $model->transaction_amount = $transaction_amount;
                    $model->type = Yii::$app->params['btree_environment'];

                    $model->full_response = $full_response;
                    $model->save();

                }

            }elseif($kind=='subscription_canceled'){
                if(isset($response['response']['subject']['subscription']['transactions'][0]['customer']['id'])){
                    $user_id = str_replace('fresh_', '', $response['response']['subject']['subscription']['transactions'][0]['customer']['id']);
                }
                 else{
                    $subscription_id = $response['response']['subject']['subscription']['id'];
                    $user_info = User::find()->where(['subscription_id' => $subscription_id])->one();
                    if(!empty($user_info))
                        $user_id = $user_info->id;
                }
                $user_info = User::find()->where(['id' => $user_id])->one();

                if(!empty($user_info)){
                    $user_info->subscription_status = 'Canceled';
                    $user_info->save();
                }
            }
            elseif($kind=='subscription_went_active'){
                $user_id = str_replace('fresh_', '', $response['response']['subject']['subscription']['transactions'][0]['customer']['id']);
                $user_info = User::find()->where(['id' => $user_id])->one();
                if(!empty($user_info)){
                    $transaction_id = $response['response']['subject']['subscription']['id'];
                    $subscription_status = $response['response']['subject']['subscription']['status'];
                    $subscription_end = strtotime($response['response']['subject']['subscription']['billingPeriodEndDate']['date']);

                    $user_info->transaction_id = $transaction_id;
                    $user_info->subscription_status = $subscription_status;
                    $user_info->subscription_end = $subscription_end;
                    $user_info->subscription_type = Yii::$app->params['btree_environment'];
                    $user_info->save();

                }
            }
        }
    }
    public function actionAskForHelp(){
        $model = new HelpRequests();
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $results = $model->getSalesperson();
            if(!empty($results)){
                $model->source_type = 'website';
                $model->request_status = 1;
                if($model->save()){
                    $library = new Library();
                    foreach($results as $result){
                        $order_info['user_id']        = $result->id;
                        $order_info['role']           = 5;
                        $order_info['from_user']      = 1;
                        $order_info['type']           = 'help_request';
                        $order_info['request_id']     = $model->request_id;
                        $order_info['request_status']  = 'pending';
                        $order_info['request_type']  = $model->request_type;
                        $order_info['config_name']    = $model->name.' is looking for a sales representative';
                        $order_info['config_value']['request_id']= $model->request_id;
                        $order_info['config_value']['message']= $model->name.' is looking for a sales representative';
                        $order_info['config_value']['name'] = $model->name;
                        $order_info['config_value']['location'] = $model->location;
                        $order_info['config_value']['phonenumber']= $model->phonenumber;
                        $order_info['config_value']['lat']= $model->lat;
                        $order_info['config_value']['lng']= $model->lng;
                        $order_info['config_value']['device_token']= $model->device_token;
                        $library->saveNotification($order_info);

                    }
                    Yii::$app->session->setFlash('success', 'Your request has been sent.');

                }
            }else{
                Yii::$app->session->setFlash('success', 'Sorry no sales person found in your location.');
            }
            return $this->refresh();
        }
        return $this->render('ask_for_help',['model' => $model]);
    }

    public function actionCartInfo(){
        $phone_number = $_POST['phone_number'] ?? '';
        $notes = $_POST['notes'] ?? '';
        $date = $_POST['date'] ?? '';

        $session = Yii::$app->session;
        $session->set('phone_number', $phone_number);
        $session->set('notes', $notes);
        $session->set('date', $date);
    }

    public function actionOrderCat(){
        $orders = Order::find()->all();
        foreach ($orders as $order){
            $order_category = $order->supplier->supplier_type;
            $order->order_category = $order_category;
            $order->save();
        }
    }

    public function actionHotDeals(){
        $sort = $_GET['sort_id'] ?? null;

        $slug = 'hot-deals';
        $model		= Pages::find()->select('*')->joinWith('pageDescription as pd')->where(['slug'=>$slug,'status'=>1,'pd.language_id' => language_id,'page_type' => 1 ])->one();
        if(empty($model))
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));

        $query = Menu::find()->joinWith('supplierInfo');
        $query->where(['and',
            ['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],
            ['>','subscription_end',time()],
            ['!=', 'discount' , 0.00]
        ]);
        $sort_order = ['dish_name' => SORT_ASC];
        $sort_order_name = 'Featured';
        if(!empty($sort)){
            $sort_method = Sortmethod::findOne($sort);
            if(!empty($sort_method)){
                $sort_order_name = $sort_method->sort_name;
                switch ($sort) {
                    case 6:
                        $sort_order = ['dish_price' => SORT_ASC];
                        break;
                    case 7:
                        $sort_order = ['dish_price' => SORT_DESC];
                        break;
                    case 8:
                        $sort_order = ['id' => SORT_DESC];
                        break;
                }
            }

        }
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => $sort_order
            ]
        ]);

        return $this->render('deals',['model' => $model,'dataProvider' => $dataProvider,'sort_order_name' =>$sort_order_name]);
    }

    public function actionCategories(){
        return $this->render('categories');
    }

    public function actionSearchItems($term){
        $q = trim(Html::encode($term));
        $json = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $all_brands = Brand::find()->joinWith('brandDescription as bd')->where(['and',['bd.language_id' => language_id],['like','brand_name',$q],['status' => 1]])->limit(10)->all();

        $all_categories = Category::find()->select('*,cd.category_name as cname')->joinWith('categoryDescription as cd')->where(['and',['active' => 1,'cd.language_id' => language_id],['like','cd.category_name',$q],['NOT IN','cat_id',1]])->limit(10)->all();

        $all_suppliers = Menu::find()->select('tbl_menu.*')->joinWith('supplierInfo')->where(['and',['tbl_menu.status'=>'active','tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0]])->andWhere(['like','tbl_user.name',$q])->groupBy('tbl_user.id')->limit(10)->all();

        $all_menus = Menu::find()->where(['and',['tbl_menu.status'=>'active','tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0]])->andWhere(['like','tbl_menu.dish_name',$q])->limit(10)->all();

        if(!empty($all_brands)){
            foreach ($all_brands as $all_brand)
                $json[] = ['label' => $all_brand->brandDescription->brand_name,'value' => $all_brand->brandDescription->brand_name,'type' => 'brands'];
        }

        if(!empty($all_categories)){
            foreach ($all_categories as $category)
                $json[] = ['label' => $category->categoryDescription->category_name,'value' => $category->categoryDescription->category_name,'type' => 'category'];
        }

        if(!empty($all_suppliers)){
            foreach ($all_suppliers as $supplier)
                $json[] = ['label' => $supplier->supplierInfo->name,'value' =>$supplier->supplierInfo->name,'type' => 'supplier'];
        }

        if(!empty($all_menus)){
            foreach ($all_menus as $menu)
                $json[] = ['label' => $menu->dish_name,'value' =>$menu->dish_name,'type' => 'product'];
        }
        return $json;
    }

    public function actionSearch(){
        $search_model = new Search();
        $title = '';
        $sort_order = ['id' => SORT_DESC];
        $booking_ids = [];
        $query = Menu::find()->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],['>','subscription_end',time()]]);
        if($search_model->load(yii::$app->request->get())){
            $title = trim(Html::encode($search_model->name));
            if(!empty($title)){
                $booking_ids[] = 0;
                $all_brands = Brand::find()->select('tbl_brand.brand_id')->joinWith('brandDescription as bd')->where(['and',['bd.language_id' => language_id],['like','brand_name',$title],['status' => 1]]);
                $brand_menus = Menu::find()->select('tbl_menu.*')->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],['>','subscription_end',time()],['IN','brand_id',$all_brands]])->asArray()->all();
              if(!empty($brand_menus)){
                  foreach ($brand_menus as $brand_menu)
                      $booking_ids[] = $brand_menu['id'];
              }

              $all_categories = Category::find()->select(' tbl_category.category_id')->joinWith('categoryDescription as cd')->where(['and',['active' => 1,'cd.language_id' => language_id],['like','cd.category_name',$title],['NOT IN','cat_id',1]]);
              $category_menus = Menu::find()->select('tbl_menu.*')->joinWith(['supplierInfo','menuCategories as mc'])->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],['>','subscription_end',time()],['IN','mc.category_id',$all_categories]])->asArray()->all();
                if(!empty($category_menus)){
                    foreach ($category_menus as $category_menu)
                        $booking_ids[] = $category_menu['id'];
                }

                $all_suppliers = User::find()->select('id')->where(['and',['status' => 10],['>','subscription_end',time()],['like','name',$title],['is_supplier' => 'yes']]);
                $supplier_menus = Menu::find()->select('tbl_menu.*')->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],['>','subscription_end',time()],['IN','supplier_id',$all_suppliers]])->asArray()->all();
                if(!empty($supplier_menus)){
                    foreach ($supplier_menus as $supplier_menu)
                        $booking_ids[] = $supplier_menu['id'];
                }

                $all_menus = Menu::find()->select('tbl_menu.*')->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],['>','subscription_end',time()],['like','dish_name',$title]])->asArray()->all();
                if(!empty($all_menus)){
                    foreach ($all_menus as $all_menu)
                        $booking_ids[] = $all_menu['id'];
                }
                if(!empty($booking_ids)){
                    $query->andWhere(['IN','tbl_menu.id',$booking_ids]);
                }

            }
        }
        if (!Yii::$app->user->isGuest)
            $query->andWhere(['!=','supplier_id',Yii::$app->user->identity->id]);

        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => $sort_order
            ],
        ]);
        return $this->render('search',['title' => $title,'dataProvider' => $dataProvider]);
    }

    public function actionSellWithUs(){
        if(yii::$app->user->isGuest)
            return $this->redirect(['/signup']);
        return $this->redirect(['/switch-to-supplier']);
    }

    public function actionHomeFood(){
        $sort_order = ['id' => SORT_DESC];
        $slug = 'home-food';
        $model = Pages::find()->select('*')->joinWith('pageDescription as pd')->where(['slug'=>$slug,'status'=>1,'pd.language_id' => language_id,'page_type' => 1 ])->one();
        if(empty($model))
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));

        $query = Menu::find()->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0,'tbl_menu.product_type'=> 1],['>','subscription_end',time()]]);
        if (!Yii::$app->user->isGuest)
            $query->andWhere(['!=','supplier_id',Yii::$app->user->identity->id]);
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => $sort_order
            ],
        ]);
        return $this->render('home-food',['model' => $model,'dataProvider' => $dataProvider]);
    }
}
