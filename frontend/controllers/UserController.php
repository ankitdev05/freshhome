<?php
namespace frontend\controllers;
use Yii;
use yii\db\Expression;
use common\models\Menu;
use common\models\User;
use common\models\OrderRatings;
use common\models\MenuSchedules;
use common\models\MenuScheduleItems;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;

class UserController extends \yii\web\Controller
{
    public $page_size =20;
    public function actionIndex($slug,$type=null){
       // $model = User::find()->where(['status' => 10,'is_supplier' =>'yes','username' =>$slug])->andWhere(['>','subscription_end',time()])->one();
        $model = User::find()->where(['status' => 10,'is_supplier' =>'yes','username' =>$slug])->one();
        if(!$model)
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        $dataProvider = [];
        $result = [];
        if($type=='reviews'){
            $query  = OrderRatings::find()->where(['supplier_id'=>$model->id,'is_active'=>1])->orderBy("id desc");
            $dataProvider	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_DESC,
                    ]
                ],
            ]);
        }elseif($type=='products'){
            $query = Menu::find()->where(['and',['image_approval'=>1,'is_delete'=>0,'test_data'=>0,'product_type'=>$model->supplier_type,'supplier_id'=>$model->id]]);
            $dataProvider	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_DESC,
                    ]
                ],
            ]);
        }elseif($type=='schedule'){
            $query = MenuSchedules::find()->where(['and',['choices' => 'active'],['supplier_id' => $model->id],['>=','end_timestamp',time() ]]);

            $dataProvider	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'start_timestamp' => SORT_ASC,
                    ]
                ],
            ]);
        }elseif($type=='schedule_items'){
            $id = $_GET['id'] ?? 0;
            $result = MenuSchedules::find()->where(['and',['choices' => 'active'],['supplier_id' => $model->id],['id'=>$id ]])->one();
            if(!$result)
                throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
            $subQuery = MenuScheduleItems::find()->select('menu_id')->where(['menuschedule_id' => $id]);
            $query = Menu::find()->where(['and',['in','id',$subQuery],['image_approval'=>1,'is_delete'=>0,'test_data'=>0,'product_type'=>$model->supplier_type,'supplier_id'=>$model->id]]);
            $dataProvider	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_DESC,
                    ]
                ],
            ]);
        }
        return $this->render('index',['result'=>$result,'model' =>$model,'dataProvider'=>$dataProvider,'type'=>$type]);
    }

}
