<?php
namespace frontend\controllers;
use Yii;
use common\models\Pages;
use yii\web\NotFoundHttpException;
class UserController extends \yii\web\Controller
{
    public function actionIndex($slug)
    {
        $model		= Pages::find()->where(['slug'=>$slug,'status'=>1])->one();
        if(empty($model))
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        return $this->render('index',['model'=>$model]);
    }

}
