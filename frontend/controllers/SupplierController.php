<?php
namespace frontend\controllers;
use common\models\Category;
use common\models\OrderOptions;
use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use GuzzleHttp;

use common\models\User;
use common\models\Menu;
use common\models\Order;
use common\models\Library;
use common\models\Braintree;
use common\models\OrderSearch;
use common\models\MenuMeals;
use common\models\MenuSearch;
use common\models\OrderRatings;
use common\models\MenuCuisines;
use common\models\MenuCategories;
use common\models\MenuSchedules;
use common\models\TempProImages;
use common\models\MenuToImages;
use common\models\MenuScheduleItems;
use common\models\OrderHistory;
use common\models\OrderCancel;
use common\models\CreditCards;
use common\models\MenuToAttributes;
use common\models\MenuToAttributeOptions;
use common\models\Filters;
use common\models\Plans;
use common\models\AttributeGroup;
use common\models\HomeCategory;

use himiklab\thumbnail\EasyThumbnailImage;

class SupplierController extends Controller{
    public $page_size = 20;
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','my-sch','addmenu','updatemenu','upload','pdel_timage','pupload','del-timage','deletemenuimage','product-attribute','deletemenu','accept-order','reject-order','subscription','product-attributes','get-categories'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'my-sch' => ['post'],
                    'pupload' => ['post'],
                    'pdel_timage' => ['post'],
                    'del-timage' => ['post'],
                    'product-attribute' => ['post'],
                    'deletemenu' => ['post'],
                    'accept-order' => ['post'],
                    'reject-order' => ['post'],
                    'product-attributes' => ['post'],

                ]
            ]
        ];
    }

    public function actionIndex(){
        $session = Yii::$app->session;
        $is_supplier = $session->has('is_supplier');
        if(!$is_supplier)
            return $this->redirect(Yii::$app->homeUrl);

        $action	= 'index';
        if(isset($_GET['type']) && !empty($_GET['type']))
            $action	= $_GET['type'];

        if (method_exists($this,$action))
            return $this->{$action}();

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    protected function index(){


        $user_model =  User::findOne(Yii::$app->user->identity->id);
        $user_model->scenario = 'change_email';
        if (isset($_POST['update_email']) && $user_model->load(Yii::$app->request->post()) && $user_model->validate()) {

            $user_model->email = $user_model->email;
            $user_model->status = 9;
            $user_model->email_verify = 0;
            $user_model->generateEmailVerificationToken();
            if($user_model->save()){
                $user_model->sendEmailUpdate($user_model);
                Yii::$app->session->setFlash('success', 'Please check your inbox for verification email.');
                return $this->refresh();
            }
        }
        // Start profile pic update
        if ($user_model->load(Yii::$app->request->post())) {
            $user_model->scenario = 'supp_update_image';
            $profile_pic = UploadedFile::getInstance($user_model, 'profile_pic');
            if(!empty($profile_pic)){
                $library = new Library();
                $user_model->profile_pic  = $library->saveFile($profile_pic,'users');
                if($user_model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        // End profile pic update

        // Start name update
        if(isset($_POST['update_name'])){
            $user_model->scenario = 'supp_update_name';
            if ($user_model->load(Yii::$app->request->post()) && $user_model->validate()) {
                if($user_model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        // End name update

        // Start dob update
        if(isset($_POST['update_dob'])){
            $user_model->scenario = 'supp_update_dob';
            if ($user_model->load(Yii::$app->request->post()) && $user_model->validate()) {
                if($user_model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        // End dob update

        // Start about update
        if(isset($_POST['update_about'])){
            $user_model->scenario = 'supp_update_description';
            if ($user_model->load(Yii::$app->request->post()) && $user_model->validate()) {
                if($user_model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        // End about update

        // Start nationality update
        if(isset($_POST['update_nationality'])){
            $user_model->scenario = 'supp_update_nationality';
            if ($user_model->load(Yii::$app->request->post()) && $user_model->validate()) {
                if($user_model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        // End nationality update

        // Start phone number update
        if(isset($_POST['User']['phone_number'])){
            $user_model->scenario = 'supp_update_phone_number';
            if ($user_model->load(Yii::$app->request->post())) {
                if($user_model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        // End phone number update

        // Start city update
        if(isset($_POST['update_city'])){
            $user_model->scenario = 'supp_update_city';
            if ($user_model->load(Yii::$app->request->post())) {
                if($user_model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        // End city update

        // Start Building update
        if(isset($_POST['update_building'])){
            $user_model->scenario = 'supp_update_building';
            if ($user_model->load(Yii::$app->request->post())) {
                if($user_model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        // End Building update

        // Start Floor update
        if(isset($_POST['update_floor'])){
            $user_model->scenario = 'supp_update_floor';
            if ($user_model->load(Yii::$app->request->post())) {
                if($user_model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        // End Floor update

        // Start Flat update
        if(isset($_POST['update_flat'])){
            $user_model->scenario = 'supp_update_flat';
            if ($user_model->load(Yii::$app->request->post())) {
                if($user_model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        // End Flat update

        // Start Location update
        if(isset($_POST['update_location'])){
            $user_model->scenario = 'supp_update_location';
            if ($user_model->load(Yii::$app->request->post())) {
                if($user_model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        // End Location update

        return $this->render('profile',['model' => $user_model]);

    }

    protected function mySchedule(){
        $this->checkSubscription();
        $this->checkFoodSupplier();
        $user_info = Yii::$app->user->identity;
        $menu_items = Menu::find()->where(['supplier_id' => $user_info->id,'is_delete' => 0,'image_approval' => 1])->all();
        $model = new MenuSchedules();
        $model->supplier_id = $user_info->id;
        $model->choices = 'active';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $menu_schedule = new MenuSchedules();
            $result_sch = $menu_schedule->find()->where(['supplier_id'=>$user_info->id,'schedule_date'=>$model->schedule_date])->one();

            $date = $model->schedule_date;
            $start_time = date("H:i:s", strtotime($model->start_time));
            $end_time = date("H:i:s", strtotime($model->end_time));
            if(!empty($result_sch)){
                $model = $result_sch;
            }
            $model->schedule_date      = $date;
            $model->start_time         = $start_time;
            $model->end_time           = $end_time;
            $model->choices            = 'active';

            if($model->save()){
                MenuScheduleItems::deleteAll(['menuschedule_id'=>$model->id]);
                if(isset($_POST['Menu']['product_id']) && !empty($_POST['Menu']['product_id'])){
                    foreach($_POST['Menu']['product_id'] as $pid=>$value){
                        $menu_sch_items = new MenuScheduleItems();
                        $menu_sch_items->menuschedule_id    = $model->id;
                        $menu_sch_items->choices            = 'active';
                        $menu_sch_items->quantity           = $_POST['Menu']['quantity'][$pid];
                        $menu_sch_items->menu_id           = $pid;
                        $menu_sch_items->save();
                    }
                }
                Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
                return $this->refresh();
            }else{
                print_r($menu_schedule->getErrors());
            }

        }
        return $this->render('my_schedule',['model' => $model,'menu_items' => $menu_items,'user_info' => $user_info]);
    }

    public function actionMySch($date){
        $json = [];
        $menu_schedule = New MenuSchedules();
        $user_info = Yii::$app->user->identity;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result_sch = $menu_schedule->find()->where(['supplier_id'=>$user_info->id,'schedule_date'=>$date])->one();
        if(!empty($result_sch)){
            $json['to'] = date("g:i a", strtotime($result_sch->start_time));
            $json['from'] = date("g:i a", strtotime($result_sch->end_time));
            $json['schedule_date'] = $result_sch->schedule_date;
            $json['schedule_date_format'] = date('d F Y',strtotime($result_sch->schedule_date));
            $menu_sch_items = new MenuScheduleItems();
            $items = $menu_sch_items->find()->where(['menuschedule_id' => $result_sch->id])->all();
            if(!empty($items)){
                foreach($items as $item){
                    $json['items'][] = ['menu_id' => $item->menu_id,'quantity' => $item->quantity];
                }
            }
        }

        return $json;
    }

    /* Add new menu item (ffd) */
    public function actionAddmenu(){
        $this->checkSubscription();
        $this->checkFoodSupplier();
        $user_info = Yii::$app->user->identity;
        $user_id	= $user_info->id;
        $model = new Menu();
        $model->scenario = 'food_menu';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $post_data		= Yii::$app->request->post();
            $dish_image = UploadedFile::getInstance($model, 'dish_image');
            if(!empty($dish_image)){
                $library   			 	= new Library();
                $model->dish_image  	= $library->saveFile($dish_image,'menu');
                $model->image_approval 	= 0;
                $more_text	= ' Please wait till image approval from Admin.';
            }
            if(!empty($model->dish_since)){
                $explode = explode(':',$model->dish_since);
                $time = $explode[0]*60;
                if(isset($explode[1]))
                    $time += $explode[1];
                $model->dish_prep_time = $time;
            }
            $model->supplier_id =$user_id;
            $model->city_id = $user_info->city;
            $model->product_type = $user_info->supplier_type;
            if($model->save()) {
                if (isset($post_data['Menu']['category']) && !empty($post_data['Menu']['category'])) {
                    $categories = $post_data['Menu']['category'];
                    foreach ($categories as $category) {
                        $cat_model = new MenuCategories();
                        $cat_model->menu_id = $model->id;
                        $cat_model->category_id = $category;
                        $cat_model->save();
                    }
                }

                if (isset($post_data['Menu']['meal']) && !empty($post_data['Menu']['meal'])) {
                    $menu_model = new MenuMeals();
                    $menu_model->menu_id = $model->id;
                    $menu_model->meal_id = $post_data['Menu']['meal'];
                    $menu_model->save();
                }

                if (isset($post_data['Menu']['cuisine']) && !empty($post_data['Menu']['cuisine'])) {
                    $cuisines = $post_data['Menu']['cuisine'];
                    foreach ($cuisines as $cuisine) {
                        $cus_model = new MenuCuisines();
                        $cus_model->menu_id = $model->id;
                        $cus_model->cuisine_id = $cuisine;
                        $cus_model->save();
                    }
                }
            }
            Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
            return $this->redirect(['/supplier?type=listmenu']);
        }
        return $this->render("//menu/create",['model'=>$model]);
    }
    /* Listing menu items (ffd) */
    protected function listmenu(){
        $this->checkFoodSupplier();
        $user_info = Yii::$app->user->identity;
        $user_id	= $user_info->id;
        $searchModel = new MenuSearch();
        $searchModel->supplier_id = $user_id;
        $searchModel->is_delete = 0;
        $searchModel->product_type = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('//menu/listing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user_info' => $user_info,
        ]);
    }

    public function invite(){
        $model = Yii::$app->user->identity;

        if(isset($_GET['download'])){
            $root=Yii::getAlias('@webroot').'/uploads/qrcodes/'.$model->id.'.png';
            return Yii::$app->response->sendFile($root, 'qr-code.png');
        }

        return $this->render('//profile/invite',['model'=>$model]);
    }

    /* Update menu item (ffd) */
    public function actionUpdatemenu($id){
        $this->checkSubscription();
        $this->checkFoodSupplier();
        $user_info = Yii::$app->user->identity;
        $user_id	= $user_info->id;
        $model = Menu::find()->where(['id'=>$id,'supplier_id'=>$user_id])->one();
        if(!$model)
            throw new ForbiddenHttpException('You\'re not authorized to view this page.');

        $old_image = $model->dish_image;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $post_data		= Yii::$app->request->post();
            $dish_image = UploadedFile::getInstance($model, 'dish_image');
            if(!empty($dish_image)){
                $library   			 	= new Library();
                $model->dish_image  	= $library->saveFile($dish_image,'menu');
                $model->image_approval 	= 0;
                $more_text	= ' Please wait till image approval from Admin.';
            }else{
                $model->dish_image = $old_image;
            }
            if(!empty($model->dish_since)){
                $explode = explode(':',$model->dish_since);
                $time = $explode[0]*60;
                if(isset($explode[1]))
                    $time += $explode[1];
                $model->dish_prep_time = $time;
            }

            if($model->save()) {
                if (isset($post_data['Menu']['category']) && !empty($post_data['Menu']['category'])) {
                    $categories = $post_data['Menu']['category'];
                    foreach ($categories as $category) {
                        $cat_model = new MenuCategories();
                        $cat_model->menu_id = $model->id;
                        $cat_model->category_id = $category;
                        $cat_model->save();
                    }
                }

                if (isset($post_data['Menu']['meal']) && !empty($post_data['Menu']['meal'])) {
                    $menu_model = new MenuMeals();
                    $menu_model->menu_id = $model->id;
                    $menu_model->meal_id = $post_data['Menu']['meal'];
                    $menu_model->save();
                }

                if (isset($post_data['Menu']['cuisine']) && !empty($post_data['Menu']['cuisine'])) {
                    $cuisines = $post_data['Menu']['cuisine'];
                    foreach ($cuisines as $cuisine) {
                        $cus_model = new MenuCuisines();
                        $cus_model->menu_id = $model->id;
                        $cus_model->cuisine_id = $cuisine;
                        $cus_model->save();
                    }
                }
            }
            Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
            return $this->redirect(['/supplier?type=listmenu']);
        }
        return $this->render("//menu/update",['model'=>$model]);
    }

    public function salesreport(){
        $this->checkSupplier();
        $model = new Filters();
        if (!$model->load(Yii::$app->request->get()) ) {
            $model->start_date = date('1/'.date('m').'/'.date('Y'));
            $model->end_date = date(date('d').'/'.date('m').'/'.date('Y'));
            $model->report_type = null;
        }
        $user_info = Yii::$app->user->identity;
        $user_id = $user_info->id;
        if($user_info->is_supplier=='no')
            throw new ForbiddenHttpException('You\'re not authorized to view this page.');

        $headers = [
            'Authorization' => 'Bearer ' . $user_info->auth_key,
            'Accept'        => 'application/json',
        ];
        if(empty( $model->start_date))
            $model->start_date  = date('1/'.date('m').'/'.date('Y'));

        if(empty( $model->end_date))
            $model->end_date  = date(date('d').'/'.date('m').'/'.date('Y'));
        if(empty($model->report_type))
            $model->report_type = null;
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', Url::to(['/api/report','start_date'=>$model->start_date,'end_date'=>$model->end_date,'type'=>$model->report_type],true), [
            'headers' => $headers
        ]);
        $response = GuzzleHttp\json_decode($res->getBody()->getContents(),true);

        return $this->render('salesreport',['response' => $response,'model' => $model]);
    }


    /*
        User's  products listing
    */
    protected function myProducts(){
        $this->checkOtherSupplier();
        $user_info = Yii::$app->user->identity;
        $user_id	= $user_info->id;
        $searchModel = new MenuSearch();
        $searchModel->supplier_id = $user_id;
        $searchModel->is_delete = 0;
        $searchModel->product_type = $user_info->supplier_type;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('products/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user_info' => $user_info,
        ]);
    }

    /*
        Add product
    */
    protected function addProduct(){
        $this->checkSubscription();
        $this->checkOtherSupplier();
        $model = new Menu();
        $model->scenario = 'other_items';

        if ($model->load(Yii::$app->request->get())) {

        }
        if ($model->load(Yii::$app->request->post())) {
            $post_data = $_POST['Menu'];
            $model->dish_name = $post_data['product_name'] ?? '';
            $model->dish_description = $post_data['product_description'] ?? '';
            $model->dish_price = $post_data['product_price'] ?? 0;
            $model->dist_qty = $post_data['product_qty'] ?? 0;
            $dish_image = UploadedFile::getInstance($model, 'dish_image');

            if(!empty($dish_image)){
                $library   			 	= new Library();
                $model->dish_image  	= $library->saveFile($dish_image,'menu');
                $model->image_approval = 0;
            }
            if($model->validate() && $model->save()){

                if(isset($post_data['options'])){
                    foreach($post_data['options'] as $k=>$options){
                        $menu_to_attr = new MenuToAttributes();
                        $menu_to_attr->menu_id = $model->id;
                        $menu_to_attr->option_id = $k;
                        $menu_to_attr->is_required = $options['is_required'];
                        if($menu_to_attr->save()){
                            if(isset($post_data['product_attributes'][$k]) && !empty($post_data['product_attributes'][$k])){
                                foreach($post_data['product_attributes'][$k] as $pro_options){
                                    if(!empty($pro_options['name'])){
                                        $menu_to_pro_opt = new MenuToAttributeOptions();
                                        $menu_to_pro_opt->menu_id = $model->id;
                                        $menu_to_pro_opt->menu_attr = $menu_to_attr->menu_attr;
                                        $menu_to_pro_opt->name = $pro_options['name'];
                                        $menu_to_pro_opt->quantity = $pro_options['quantity'];
                                        $menu_to_pro_opt->subtract = $pro_options['subtract'];
                                        $menu_to_pro_opt->price = $pro_options['price'];
                                        $menu_to_pro_opt->save();
                                        print_r($menu_to_pro_opt->getErrors());
                                    }

                                }
                            }
                        }
                    }
                }

                $categories = $model->category;
                if(!empty($categories)){
                    $categories = explode(',',$categories);
                    foreach ($categories as $category) {
                        $cat_model = new MenuCategories();
                        $cat_model->menu_id = $model->id;
                        $cat_model->category_id = $category;
                        $cat_model->save();
                    }
                }
                $user_id = yii::$app->user->id;
                $temp_images  = TempProImages::find()->where(['user_id'=>$user_id]);
                $images = $temp_images->all();
                if(!empty($images)){
                    foreach($images as $image){
                        $pro_images = new MenuToImages();
                        $pro_images->menu_id  = $model->id;
                        $pro_images->user_id  = $user_id;
                        $pro_images->image_name = $image->file_name;
                        $pro_images->file_info = $image->file_info;
                        $pro_images->save();
                        $image->delete();
                    }

                }
                Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
                if($model->image_approval==0)
                    Yii::$app->session->setFlash('success', 'Please wait till image approval from Admin.');
                return $this->redirect(['/supplier','type' => 'myProducts']);
            }
        }
        return $this->render('products/add',['model' => $model]);
    }

    public function actionUpload(){
        $json = [];
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Menu();
        $temp_products = new TempProImages();
        $imageFile = UploadedFile::getInstance($model, 'product_images');
        if(!empty($imageFile)){
            $library = new Library();
            $file_name = $library->saveFile($imageFile,'menu');
            if(!empty($file_name)){
                $image_info = (array)$imageFile;
                $temp_products->user_id     = Yii::$app->user->identity->id;
                $temp_products->file_name   = $file_name;
                $temp_products->file_info   = $library->json_encodeArray($image_info);
                if($temp_products->save()){
                    $thumb=EasyThumbnailImage::thumbnailFileUrl(
                        "../../frontend/web/uploads/".$temp_products->file_name,100,100,EasyThumbnailImage::THUMBNAIL_INSET
                    );
                    $json['files']  = [
                        [
                            'name'=>$imageFile->name,
                            'size'=>$imageFile->size,
                            'url'=>$thumb,
                            'thumbnailUrl'=>$thumb,
                            'deleteUrl'=>Yii::$app->request->baseUrl.'/supplier/del-timage?id='.$temp_products->id,
                            'deleteType'=>'POST'
                        ]
                    ];
                }
            }
        }

        return $json;
    }

    public function actionPupload($id){
        $json = [];
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Menu();

        $imageFile = UploadedFile::getInstance($model, 'product_images');
        $allow = false;
        $user_id = Yii::$app->user->identity->id;
        if(!empty($imageFile)){
            $pro_info		= $model->find()->where(['id'=>$id,'supplier_id'=>$user_id])->one();
            if(!empty($pro_info))
                $allow = true;
        }
        if($allow===true){
            $library    = new Library();
            $file_name  = $library->saveFile($imageFile,'menu');
            if(!empty($file_name)){
                $image_info = (array)$imageFile;
                $temp_products = new MenuToImages();
                $temp_products->menu_id = $id;
                $temp_products->user_id = $user_id;
                $temp_products->image_name = $file_name;
                $temp_products->file_info  = $library->json_encodeArray($image_info);
                if($temp_products->save()){
                    $thumb=EasyThumbnailImage::thumbnailFileUrl(
                        "../../frontend/web/uploads/".$temp_products->image_name,100,100,EasyThumbnailImage::THUMBNAIL_INSET
                    );
                    $json['files']  = [
                        [
                            'name'=>$image_info['name'],
                            'size'=>$image_info['size'],
                            'url'=>$thumb,
                            'thumbnailUrl'=>$thumb,
                            'deleteUrl'=>Yii::$app->request->baseUrl.'/supplier/pdel_timage?id='.$temp_products->image_id,
                            'deleteType'=>'POST'
                        ]
                    ];
                }
            }
        }
        return $json;
    }

    public function actionPdel_timage($id){
        $json		        = [];
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $imageModel      =  MenuToImages::find()->where(['image_id'=>$id])->one();
        $allow = false;
        if(!empty($imageModel)){
            $pro_info		= Menu::find()->where(['id'=>$imageModel->menu_id,'supplier_id'=>Yii::$app->user->identity->id])->one();
            if(!empty($pro_info))
                $allow = true;
        }
        if($allow===true){
            $fullurl 			= Yii::$app->basePath . '/web/uploads/'.$imageModel->image_name;
            unlink($fullurl);
            $imageModel->delete();
        }
        return $json;
    }

    public function updateProduct(){

        $id = $_GET['id'] ?? 0;
        $this->checkSubscription();
        $this->checkOtherSupplier();
        $user_info = yii::$app->user->identity;

        $model =  Menu::find()->where(['id' => $id ,'is_delete' => 0,'supplier_id' => $user_info->id,'product_type' => $user_info->supplier_type])->one();
        if(!$model) throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        $model->scenario = 'other_items';
        $model->product_name = $model->dish_name;
        $model->product_description = $model->dish_description;
        $model->product_price = $model->dish_price;
        $model->product_qty = $model->dist_qty;
        $old_image = $model->dish_image;
        if ($model->load(Yii::$app->request->post())) {
            $post_data = $_POST['Menu'];

            $model->dish_name = $post_data['product_name'] ?? '';
            $model->dish_description = $post_data['product_description'] ?? '';
            $model->dish_price = $post_data['product_price'] ?? 0;
            $model->dist_qty = $post_data['product_qty'] ?? 0;
            $dish_image = UploadedFile::getInstance($model, 'dish_image');
            if(!empty($dish_image)){
                $library   			 	= new Library();
                $model->dish_image  	= $library->saveFile($dish_image,'menu');
                $model->image_approval 	= 0;
            }else{
                $model->dish_image = $old_image;
            }
            if($model->validate() && $model->save()){
                MenuCategories::deleteAll(['menu_id' => $id]);
                MenuToAttributes::deleteAll(['menu_id' => $id]);
                MenuToAttributeOptions::deleteAll(['menu_id' => $id]);
                if(isset($post_data['options'])){
                    foreach($post_data['options'] as $k=>$options){
                        $menu_to_attr = new MenuToAttributes();
                        $menu_to_attr->menu_id = $model->id;
                        $menu_to_attr->option_id = $k;
                        $menu_to_attr->is_required = $options['is_required'];
                        if($menu_to_attr->save()){
                            if(isset($post_data['product_attributes'][$k]) && !empty($post_data['product_attributes'][$k])){
                                foreach($post_data['product_attributes'][$k] as $pro_options){
                                    if(!empty($pro_options['name'])){
                                        $menu_to_pro_opt = new MenuToAttributeOptions();
                                        $menu_to_pro_opt->menu_id = $model->id;
                                        $menu_to_pro_opt->menu_attr = $menu_to_attr->menu_attr;
                                        $menu_to_pro_opt->name = $pro_options['name'];
                                        $menu_to_pro_opt->quantity = $pro_options['quantity'];
                                        $menu_to_pro_opt->subtract = $pro_options['subtract'];
                                        $menu_to_pro_opt->price = $pro_options['price'];
                                        $menu_to_pro_opt->save();

                                    }

                                }
                            }
                        }
                    }
                }

                $categories = $model->category;
                if(!empty($categories)){
                    $categories = explode(',',$categories);
                    foreach ($categories as $category) {
                        $cat_model = new MenuCategories();
                        $cat_model->menu_id = $model->id;
                        $cat_model->category_id = $category;
                        $cat_model->save();
                    }
                }

                Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
                if($model->image_approval==0)
                    Yii::$app->session->setFlash('success', 'Please wait till image approval from Admin.');
                return $this->redirect(['/supplier','type' => 'myProducts']);
            }
        }
        return $this->render('products/update',['model' => $model]);
    }

    /* Delete menu item (ffd) */
    public function actionDeletemenu($id){
        $user_info = Yii::$app->user->identity;
        $user_id	= $user_info->id;
        $model = Menu::find()->where(['id'=>$id,'supplier_id'=>$user_id])->one();
        if(!$model)
            throw new ForbiddenHttpException('You\'re not authorized to view this page.');
        $model->is_delete = 1;
        $model->save();
        Yii::$app->session->setFlash('success', 'Your record has been deleted successfully.');
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);

    }

    public function actionDeletemenuimage($id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_info = Yii::$app->user->identity;
        $user_id	= $user_info->id;
        $model = Menu::find()->where(['id'=>$id,'supplier_id'=>$user_id])->one();
        if(!$model)
            throw new ForbiddenHttpException('You\'re not authorized to view this page.');

        $absolute_url 	= Yii::getAlias('@frontend').'/web/uploads/';
        unlink($absolute_url.$model->dish_image);
        $model->dish_image = '';
        if($model->save()){
            $json['result']= 'done';
            return $json;
        }
    }

    public function actionDelTimage($id){
        $json = [];
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $imageModel =  TempProImages::findOne($id);
        if(!empty($imageModel)){
            $fullurl  = Yii::$app->basePath . '/web/uploads/'.$imageModel->file_name;
            unlink($fullurl);
            $imageModel->delete();
        }
        return $json;
    }

    public function actionProductAttribute(){
        $json = [];
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $attr_id = $_POST['attr_id'] ?? 0;
        $menu_id = $_POST['menu_id'] ?? 0;
        if($attr_id!=0)
            $json['html'] = $this->renderAjax('_attribute_html',['attr_id' => $attr_id,'menu_id' => $menu_id]);
        else
            $json['html'] = '';
        return $json;
    }

    protected function mykitchen(){
        $this->checkFoodSupplier();
        $model = new Order();
        $model->scenario = 'reject_order';

        $user_info		= Yii::$app->user->identity;

        $p_query = $user_info->getFoodSupplierPendingOrders();

        $c_query = $user_info->getFoodSupplierCompleteOrder();

        $pre_query = $user_info->getFoodSupplierPreOrder();

        $p_dataProvider	=  new ActiveDataProvider([
            'query' => $p_query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC,
                ]
            ],
        ]);
        $c_dataProvider	=  new ActiveDataProvider([
            'query' => $c_query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC,
                ]
            ],
        ]);
        $pre_dataProvider	=  new ActiveDataProvider([
            'query' => $pre_query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC,
                ]
            ],
        ]);
        $p_dataProvider->pagination->pageParam = 'p_page';
        $c_dataProvider->pagination->pageParam = 'c_page';
        $pre_dataProvider->pagination->pageParam = 'pre_page';
        $searchModel = new OrderSearch();
        return $this->render('//menu/mykitchen',[
            'p_dataProvider'=>$p_dataProvider,
            'c_dataProvider'=>$c_dataProvider,
            'pre_dataProvider'=>$pre_dataProvider,
            'searchModel'=>$searchModel,
            'model'=>$model,
        ]);
    }
    protected function myorders(){
        $this->checkOtherSupplier();
        $model = new Order();
        $model->scenario = 'reject_order';

        $user_info		= Yii::$app->user->identity;
        $supplier_id	= $user_info->id;
        $p_query		= Order::find()->where(['and',
            ['supplier_id'=>$supplier_id],
            ['!=','order_type','pre-order'],
            ['NOT IN','order_status_id',[2,5]]
        ]);

        $c_query		= Order::find()->where(['supplier_id'=>$supplier_id,'order_status_id'=>5])->andWhere(['!=','order_type','pre-order']);


        $p_dataProvider	=  new ActiveDataProvider([
            'query' => $p_query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC,
                ]
            ],
        ]);
        $c_dataProvider	=  new ActiveDataProvider([
            'query' => $c_query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC,
                ]
            ],
        ]);

        $p_dataProvider->pagination->pageParam = 'p_page';
        $c_dataProvider->pagination->pageParam = 'c_page';

        $searchModel = new OrderSearch();
        return $this->render('//menu/myorders',[
            'p_dataProvider'=>$p_dataProvider,
            'c_dataProvider'=>$c_dataProvider,
            'searchModel'=>$searchModel,
            'model'=>$model,
        ]);
    }
    public function actionAcceptOrder($order_id){
        $user_info = Yii::$app->user->identity;
        $supplier_id = $user_info->id;
        $order_info = Order::find()->where(['supplier_id'=>$supplier_id,'order_id'=>$order_id])->orWhere(['order_status_id'=>6,'order_status_id'=>8])->one();
        if(!empty($order_info)) {
            $order_info->order_status_id = 11;
            if($order_info->save()){
                $json['success']	= 'Your order has been accepted successfully.';

                $user_detail        = User::findOne($supplier_id);
                $user_detail        = $user_detail->userInfo;

                $library = new Library();
                $order_info1['user_id']        = $order_info->user_id;
                $order_info1['role']            = 1;
                $order_info1['from_user']      = $order_info->supplier_id;
                $order_info1['type']           = 'order_accept';
                $order_info1['config_name']    = $user_detail['name'].' has accepted your order.';
                $order_info1['config_value']['order_id']= $order_info->order_id;
                $order_info1['config_value']['message']= $user_detail['name'].' has accepted your order.';
                $library->saveNotification($order_info1);

                $order_history	= new OrderHistory();
                $order_history->order_id			= $order_info->order_id;
                $order_history->order_status_id		= $order_info->order_status_id;
                $order_history->notify				= 1;
                $order_history->comment				= 'Your order has been accepted successfully.';
                $order_history->save();
                Yii::$app->session->setFlash('success', $json['success']);
            }else{
                $all_errors				= [];
                $errors					= $order_info->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']	= !empty($all_errors) ? $all_errors[0] : '' ;
                Yii::$app->session->setFlash('warning', $json['error']);

            }
        }else{
            Yii::$app->session->setFlash('warning', 'You don\'t have permission to perform this action');
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);

    }

    public function actionRejectOrder(){
        $model= new Order();
        $supplier_id    = Yii::$app->user->identity->id;
        $model->scenario = 'reject_order';
        if ($model->load(Yii::$app->request->post())) {
            $order_info     = Order::find()->where(['supplier_id'=>$supplier_id,'order_id'=>$model->order_id])->one();
            if(!empty($order_info)){
                $order_info->scenario = 'reject_order';
                $order_info->order_status_id= 5;
                $order_info->reject_type = $model->reject_type;
                $order_info->reject_message = $model->reject_message;
                if($order_info->validate() && $order_info->save()){
                    $reject_message = $model->reject_message;
                    $reject_type = $model->reject_type;

                    $canModel = new OrderCancel();
                    $canModel->cancelled_by = $supplier_id;
                    $canModel->by_supplier = 'yes';
                    $canModel->by_user = 'no';
                    $canModel->reason = $reject_message;
                    $canModel->save();

                    $json['success'] = 'Your order has been rejected successfully.';
                    $user_detail  = User::findOne($supplier_id);
                    $user_detail = $user_detail->userInfo;

                    $library = new Library();
                    $order_info1['user_id']        = $order_info->user_id;
                    $order_info1['role']            = 1;
                    $order_info1['from_user']      = $order_info->supplier_id;
                    $order_info1['type']           = 'order_reject';
                    $order_info1['config_name']    = $user_detail['name'].' has rejected your order.';
                    $order_info1['config_value']['order_id']= $order_info->order_id;
                    $order_info1['config_value']['message']= $reject_message;
                    $order_info1['config_value']['reject_type']= $reject_type;
                    $library->saveNotification($order_info1);

                    $order_history	= new OrderHistory();
                    $order_history->order_id			= $order_info->order_id;
                    $order_history->order_status_id		= $order_info->order_status_id;
                    $order_history->notify				= 1;
                    $order_history->comment				= 'Your order has been rejected successfully.';
                    $order_history->save();

                    Yii::$app->session->setFlash('warning', $json['success']);
                }else{
                    $json['error']	= 'Order does not exist';
                    Yii::$app->session->setFlash('warning', $json['error']);

                }
            }
        }else{
            $all_errors	 = [];
            $errors	 = $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']	= !empty($all_errors) ? $all_errors[0] : '' ;
            Yii::$app->session->setFlash('warning', $json['error']);
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function vieworder(){
        $order_id = $_GET['order_id'] ?? 0;
        $user_info = yii::$app->user->identity;
        $supplier_id = $user_info->id;
        $order_info = Order::find()->where(['order_id' => $order_id,'supplier_id' => $supplier_id])->one();
        if(!$order_info)
            throw new ForbiddenHttpException('You\'re not authorized to view this page.');

        $ratingModel = new OrderRatings();
        $ratingModel->user_id = $supplier_id;
        $ratingModel->order_id = $order_id;
        $ratingModel->supplier_id = $order_info->supplier_id;
        if ($ratingModel->load(Yii::$app->request->post()) && $ratingModel->validate()) {
            if($ratingModel->save()){
                Yii::$app->session->setFlash('success', 'Your data has been changed.');
                return $this->refresh();
            }
        }
        return $this->render('orders/vieworder',['model' => $order_info,'ratingModel' => $ratingModel]);
    }

    public function processing(){
        $order_id = $_GET['order_id'] ?? 0;
        $user_info = yii::$app->user->identity;
        $supplier_id = $user_info->id;
        $order_info = Order::find()->where(['order_id' => $order_id,'supplier_id' => $supplier_id])->one();
        if(!$order_info)
            throw new ForbiddenHttpException('You\'re not authorized to view this page.');
        $order_info->order_status_id = 4;
        if($order_info->save()){
            Yii::$app->session->setFlash('success', 'Your order has been processed.');

            $user_detail        = User::findOne($supplier_id);
            $user_detail        = $user_detail->userInfo;

            $library = new Library();
            $order_info1['user_id']        = $order_info->user_id;
            $order_info1['role']            = 1;
            $order_info1['from_user']      = $order_info->supplier_id;
            $order_info1['type']           = 'order_accept';
            $order_info1['config_name']    = $user_detail['name'].' has accepted your order.';
            $order_info1['config_value']['order_id']= $order_info->order_id;
            $order_info1['config_value']['message']= 'Your order #'.$order_info->order_id.' has been processed';
            $library->saveNotification($order_info1);

            $order_history	= new OrderHistory();
            $order_history->order_id			= $order_info->order_id;
            $order_history->order_status_id		= $order_info->order_status_id;
            $order_history->notify				= 1;
            $order_history->comment				= 'Your order #'.$order_info->order_id.' has been processed';
            $order_history->save();
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionSubscription(){
        $cancel = $_GET['cancel'] ?? false;
        $user_info = Yii::$app->user->identity;
        $model = New Plans();
        $model->scenario = 'subscription';
        $credit_card_model = new CreditCards();
        $credit_card_model->scenario = 'step_1';
        $plans = $model->find()->where(['type' => 'supplier'])->all();
        $cards = $credit_card_model->find()->where(['user_id'=>$user_info->id,'type'=>Yii::$app->params['btree_environment']])->asArray()->all();
        if($cancel==true){
            $braintree_model = new Braintree();
            $braintree_model->subscriptionCancel($user_info->subscription_id);
            Yii::$app->session->setFlash('warning','Your subscription has been cancelled.' );
            return $this->redirect(['/supplier']);
        }
        if ($model->load(Yii::$app->request->post())) {
            $select_plan = $_POST['Plans']['select_plan'] ?? 0;
            $select_card = $_POST['Plans']['select_card'] ?? 0;

            $plan_info = $model->findOne($select_plan);
            $cc_info = $credit_card_model->find()->where(['id' => $select_card,'user_id' => $user_info->id])->one();
            if(!$plan_info){
                $json['error'] = 'Please select valid plan.';
                Yii::$app->session->setFlash('warning',$json['error'] );
                return $this->refresh();
            }
            if(!$cc_info){
                $json['error'] = 'Please select valid card.';
                Yii::$app->session->setFlash('warning',$json['error'] );
                return $this->refresh();
            }
            $braintree_model = new Braintree();
            $time_stamp  = time();
            if($user_info->subscription_end>$time_stamp)
                $time_stamp = $user_info->subscription_end;
            $response = $braintree_model->subscriptionCreate($cc_info->token,$plan_info->btree_plan_id,$time_stamp,$user_info->id,$plan_info);
            if($response['error']==false)
                Yii::$app->session->setFlash('success','Thanks for your subscription.' );
            else
                Yii::$app->session->setFlash('warning',$response['message'] );
            return $this->refresh();

        }
        $subscription_status = $user_info->subscriptionData;
        $status = $subscription_status['subscription_status'] ?? '';
        if($status=='Active' || $status=='Pending'){
            return $this->render('subscription_view',['subscription_status' => $subscription_status]);
        }else
            return $this->render('subscription',['plans' => $plans,'model' => $model,'credit_card_model' =>$credit_card_model,'cards' => $cards,'user_info' => $user_info,'subscription_status'=>$subscription_status]);

    }

    protected function checkSupplier(){
        $session = Yii::$app->session;
        $is_supplier = $session->has('is_supplier');
        if(!$is_supplier)
            return $this->goHome();

        $user_info = Yii::$app->user->identity;
        if($user_info->supplier_type==null)
            throw new ForbiddenHttpException('You\'re not authorized to view this page.');
    }

    protected function checkOtherSupplier(){
        $session = Yii::$app->session;
        $is_supplier = $session->has('is_supplier');
        if(!$is_supplier)
            return $this->goHome();

        $user_info = Yii::$app->user->identity;
        if($user_info->supplier_type==1 || $user_info->supplier_type==null)
            throw new ForbiddenHttpException('You\'re not authorized to view this page.');
    }

    protected function checkSubscription(){
        $user_info = yii::$app->user->identity;
        $subscription_status = $user_info->subscriptionData;
        if($subscription_status['status']=='inactive'){
            Yii::$app->session->setFlash('success', 'Please subscribe our plans.');
            return $this->redirect(['supplier/subscription?type=subscription']);
        }
    }
    protected function checkFoodSupplier(){
        $session = Yii::$app->session;
        $is_supplier = $session->has('is_supplier');
        if(!$is_supplier)
            return $this->goHome();

        $user_info = Yii::$app->user->identity;
        if($user_info->supplier_type==null || $user_info->supplier_type!=1)
            throw new ForbiddenHttpException('You\'re not authorized to view this page.');
    }

    public function actionProductAttributes(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $value = $_POST['value'] ?? '';
        $category = explode(',',$value);
        $results = AttributeGroup::find()->joinWith('categories as cat')->where(['IN','cat.category_id',$category])->groupBy('attribute_id')->asArray()->all();
        return $results;
    }

    public function actionGetCategories($home_category,$category_id=null,$other_cats=null,$category_idx=null){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $cat_model = new Category();
        $json['html'] = '';
        $json['cat_name'] = '';
        $other_cats = array_reverse($cat_model->getParentCategories($category_id));

        if(!empty($other_cats)){
            $other_cats_new = [];
            foreach ($other_cats as $other_cat){
                $other_cats_new[] = $other_cat;
                if($other_cat==$category_id)
                    break;
            }
            $other_cats = $other_cats_new;
        }
        $home_category_res = HomeCategory::find()->select('*')->joinWith('homeCategoryDescription as hd')->where(['status' => 1,'hd.language_id' => language_id])->andWhere(['tbl_home_category.home_category_id'=> $home_category])->asArray()->one();
        if(!empty($home_category_res)){
            $query = Category::find()->select('*')->joinWith('categoryDescription as cd')->where(['active' => 1,'cd.language_id' => language_id]);
            $query->andWhere(['home_category_id' => $home_category]);

            if($category_id===null)
                $query->andWhere(['is','parent_id',$category_id]);
            elseif($category_idx!=''){
                $cat = Category::findOne($category_idx);
                $query->andWhere(['parent_id'=> $cat->parent_id]);
            }

            else
                $query->andWhere(['parent_id'=> $category_id]);
            $results = $query->asArray()->all();
            $html = '';
            if(!empty($results)){
                foreach ($results as $result){
                    $html .= '<tr onclick="return selectCategory('.$result['category_id'].')" class="sub_cats home_cat'.$result['category_id'].'" data-attr="'.$result['category_id'].'" title="'.$result['category_name'].'"><td><a href="javascript:void(0)">'.$result['category_name'].'</a></td></tr>';
                }
            }
            $json['html'] = $html;

            $breadcrumbs = '<li onclick="return homeCategories('.$home_category_res['home_category_id'].')" class="breadcrumb-item main_brd home_category1" aria-current="page"><a href="javascript:void(0)">'.$home_category_res['home_category_name'].'</a></li>';

            if($category_id!==null){
                $category_name = Category::find()->select('*')->joinWith('categoryDescription as cd')->where(['active' => 1,'cd.language_id' => language_id,'tbl_category.category_id' => $category_id])->asArray()->one();

                if(!empty($other_cats)){
                    foreach ($other_cats as $other_cat){
                        if(!empty($other_cat)){
                            $category_name1 = Category::find()->select('*')->joinWith('categoryDescription as cd')->where(['active' => 1,'cd.language_id' => language_id,'tbl_category.category_id' => $other_cat])->asArray()->one();
                            if(!empty($category_name1)){
                                $breadcrumbs .= '<li id="parent_id_'.$category_name1['parent_id'].'" onclick="return selectCategory('.$category_name1['category_id'].')" class="breadcrumb-item main_brd home_category1" aria-current="page"><a href="javascript:void(0)">'.$category_name1['category_name'].'</a></li>';
                            }
                        }
                    }
                }

                if(!empty($category_name) && !in_array($category_name['category_id'],$other_cats)){
                    $breadcrumbs .= '<li id="parent_id_'.$category_name['parent_id'].'" onclick="return selectCategory('.$category_name['category_id'].')" class="breadcrumb-item main_brd home_category1" aria-current="page"><a href="javascript:void(0)">'.$category_name['category_name'].'</a></li>';
                }


                $other_cats[] = $category_id;
                $other_cats = array_unique($other_cats);
                if(!empty($category_name))
                    $json['cat_name'] = $category_name['category_name'];
            }

            $json['breadcrumbs'] = $breadcrumbs;
            $json['categories'] = $other_cats;
        }

        return $json;
    }
}