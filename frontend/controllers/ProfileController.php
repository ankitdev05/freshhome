<?php
namespace frontend\controllers;
use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

use common\models\User;
use common\models\Menu;
use common\models\Order;
use common\models\Library;
use common\models\UserData;
use common\models\Braintree;
use common\models\Wallet;
use common\models\CreditCards;
use common\models\UserAddress;
use common\models\OrderRatings;
use common\models\UserBankAccount;
use common\models\UserFavSupplier;
use common\models\UserAddressSearch;

use himiklab\thumbnail\EasyThumbnailImage;

class ProfileController extends Controller{
    public $page_size = 20;
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','del-profile-pic','delete_card','address','delete_card','supplier','del-supplier-pic','change-password','change-email','wishlist','profile-pic','mywallet','fav-supplier','invoice','update-national-id'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [

                    'delete_card' => ['post'],
                    'product-attribute' => ['post'],
                ]
            ]
        ];
    }

    public function actionIndex(){
        $action	= 'index';
        if(isset($_GET['type']) && !empty($_GET['type']))
            $action	= $_GET['type'];

        $session = Yii::$app->session;
        $is_supplier = $session->has('is_supplier');
        if($is_supplier && $action!='creditCard')
            return $this->redirect(Yii::$app->homeUrl);



        if (method_exists($this,$action))
            return $this->{$action}();

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    /*
       Change user's password
       @Data method GET
       @Required parameter $password
       @Required parameter $new_password
       @Required parameter $confirm_password
       @Response mixed
   */
    protected function changepassword(){

        $model = User::findOne(Yii::$app->user->identity->id);
        $model->scenario = 'change_password';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setPassword($model->password);
            if($model->save()){
                Yii::$app->session->setFlash('success', 'Your password has been changed.');
                return $this->refresh();
            }
        }
        return $this->render('changepassword',['model'=>$model]);
    }


    /*
        Listing Users's address
        Required parameters $title,$location,$city,$building_name,$flat_no,$floor_no,$landmark,$latitude,$longitude
    */
    protected function address(){
        $action	= '';
        $id	= '';
        if(isset($_GET['action']) && !empty($_GET['action']))
            $action	= $_GET['action'];
        if(isset($_GET['id']) && !empty($_GET['id']))
            $id	= $_GET['id'];
        if (!empty($action)){
            if(method_exists($this,$action)){
                if(!empty($id))
                    return $this->{$action}($id);
                return $this->{$action}();
            }

            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $user_id = Yii::$app->user->id;
        $model		= new UserAddress();
        $searchModel = new UserAddressSearch();
        $searchModel->user_id = $user_id;
        $searchModel->is_delete = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('address',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /*
    Mark as default user address
    */
    protected function default_addr($id=null){
        $user_id = Yii::$app->user->id;
        $model		= new UserAddress();
        $result = $model->find()->where(['user_id'=>$user_id,'address_id'=>$id,'is_delete'=>0])->one();
        if(!empty($result)){
            UserAddress::updateAll(['is_default'=>'no'],'user_id='.$user_id);
            $result->is_default = 'yes';
            $result->save();
            Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
            return $this->redirect(['/profile','type'=>'address']);
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    /*
    Delete user address
    */
    protected function delete_address($id=null){
        $user_id = Yii::$app->user->id;
        $model		= new UserAddress();
        $result = $model->find()->where(['user_id'=>$user_id,'address_id'=>$id,'is_delete'=>0])->one();
        if(!empty($result)){
            $result->is_delete = 1;
            if($result->save()){
                Yii::$app->session->setFlash('success', 'Your record has been deleted successfully.');
            }

            return $this->redirect(['/profile','type'=>'address']);
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    /*
    View user address
    */
    protected function view_address($id=null){
        $user_id = Yii::$app->user->id;
        $model		= new UserAddress();
        $result = $model->find()->where(['user_id'=>$user_id,'address_id'=>$id,'is_delete'=>0])->one();
        if(!empty($result)){
            return $this->render("address/view_address",['model'=>$result]);
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    /*
    Update user address
    Required parameters $title,$location,$city,$building_name,$flat_no,$floor_no,$landmark,$latitude,$longitude
    */
    protected function edit_address($id){
        $user_id = Yii::$app->user->id;
        $model		= new UserAddress();
        $result = $model->find()->where(['user_id'=>$user_id,'address_id'=>$id,'is_delete'=>0])->one();
        if(!empty($result)){
            if ($result->load(Yii::$app->request->post()) && $result->validate()) {

                if($result->save()){
                    Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
                    return $this->redirect(['/profile','type'=>'address']);
                }
            }
            return $this->render("address/edit_address",['model'=>$result]);
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    /*
    Create user address
    Required parameters $title,$location,$city,$building_name,$flat_no,$floor_no,$landmark,$latitude,$longitude
    */
    protected function create_account(){
        $user_id = Yii::$app->user->id;
        $model		= new UserAddress();
        $model->user_id = $user_id;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if($model->save()){
                Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
                return $this->redirect(['/profile','type'=>'address']);
            }
        }
        return $this->render('create_account',['model'=>$model]);
    }

    protected function index(){


        $user_model =  User::findOne(Yii::$app->user->identity->id);
        $user_model->scenario = 'change_email';
        if (isset($_POST['update_email']) && $user_model->load(Yii::$app->request->post())) {
            if($user_model->validate()){
                $user_model->email = $user_model->email;
                $user_model->status = 9;
                $user_model->email_verify = 0;
                $user_model->generateEmailVerificationToken();
                if($user_model->save()){
                    $user_model->sendEmailUpdate($user_model);
                    Yii::$app->session->setFlash('success', 'Please check your inbox for verification email.');
                    return $this->refresh();
                }
            }
            else{
                $all_errors	= [];
                $errors	= $user_model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $error	= !empty($all_errors) ? $all_errors[0] : '' ;
                Yii::$app->session->setFlash('success', $error);
                return $this->refresh();
            }
        }
        $user_id = $user_model->id;
        $model = new UserData();
        $model = $model->find()->where(['user_id'=>$user_id])->one();
        if(empty($model)){
            $model = new UserData();
            $model->user_id = $user_id;
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $profile_pic = UploadedFile::getInstance($model, 'profile_pic');
            if(!empty($profile_pic)){
                $library = new Library();
                $model->profile_pic  = $library->saveFile($profile_pic,'users');
                if($model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }
        }
        if(isset($_POST['update_name']) || isset($_POST['update_dob']) || isset($_POST['update_gender']) || isset($_POST['update_about']) || isset($_POST['update_mobile']) || isset($_POST['update_occupation']) || isset($_POST['update_nationality'])|| isset($_POST['UserData']['phone_number'])){
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if($model->save()){
                    Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                    return $this->refresh();
                }
            }else{
                $all_errors	= [];
                $errors	= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $error	= !empty($all_errors) ? $all_errors[0] : '' ;
                Yii::$app->session->setFlash('success', $error);
                return $this->refresh();

            }
        }
        return $this->render('profile',['model' => $model,'user_model' => $user_model]);


    }

    /*
        Update user profile
        Required parameters $name,$email,$gender,$dob,$image_approval,$occupation_id,$description,$nationality
    */
    protected function updateprofile(){
        $user_id = Yii::$app->user->id;
        $model = new UserData();

        $result = $model->find()->where(['user_id'=>$user_id])->one();
        $pro_pic = '';
        if(!empty($result)){
            $model = $result;
            $pro_pic = $model->profile_pic;
        }

        $model->scenario = 'update_profile';
        $model->user_id = $user_id;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $profile_pic = UploadedFile::getInstance($model, 'profile_pic');
            if(!empty($profile_pic)){
                $library   			 = new Library();
                $model->profile_pic  = $library->saveFile($profile_pic,'users');
            }else{
                $model->profile_pic =  $pro_pic;
            }
            if($model->save()){
                Yii::$app->session->setFlash('success', 'Your profile has been updated.');
                return $this->refresh();
            }
        }
        return $this->render('index',['model'=>$model]);
    }
    /*
      Delete User's profile picture
    */
    public function actionDelProfilePic(){
        $json		= [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_id = Yii::$app->user->id;
        $model = new UserData();
        $result = $model->find()->where(['user_id'=>$user_id])->one();
        if(!empty($result)){
            $full_url = Yii::getAlias('@frontend').'/web/uploads/'.$result->profile_pic;
            unlink($full_url);
            $result->profile_pic = '';
            $result->save();
            $json['result']= 'done';
        }
        return $json;
    }
    /*
    Delete User's profile picture
  */
    public function actionDelSupplierPic(){
        $json		= [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_id = Yii::$app->user->id;
        $model = new User();
        $result = $model->findOne($user_id);
        if(!empty($result)){
            $full_url = Yii::getAlias('@frontend').'/web/uploads/'.$result->profile_pic;
            unlink($full_url);
            $result->profile_pic = '';
            $result->save();
            $json['result']= 'done';
        }
        return $json;
    }

    /* Credit card information */
    protected function creditCard(){

        $user_info  	= Yii::$app->user->identity;
        $user_id    	= $user_info->id;

        $model 				= new CreditCards();
        $model->scenario	= 'step_1';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $btree_model	= new Braintree();
            $btre_user		= $btree_model->createAccount($user_info); // check or create user braintree vault

            if(empty($btre_user)){
                Yii::$app->session->setFlash('warning', 'Invalid request. Please try again');
                return $this->refresh();
            }else{
                $cc_data					= [];
                $cc_data['cardholderName']	= $model->card_name;
                $cc_data['number']			= $model->card_number;
                $cc_data['cvv']				= $model->cvv;
                $cc_data['expirationDate']	= $model->exp_month.'/'.$model->exp_year;
                $cc_add						= $btree_model->addCreditCard($btre_user->id,$cc_data);// Add new credit card.
                $redirect_to	= $model->redirect_to;
                if(isset($cc_add['error'])){
                    Yii::$app->session->setFlash('warning', $cc_add['error']);
                    return $this->redirect($model->redirect_to);
                }else{
                    CreditCards::updateAll(['default_card'=>0],'user_id='.$user_id);
                    $model					= new CreditCards();
                    $model->scenario		= 'step_2';
                    $model->name			= $cc_add['creditCard']['cardholderName'];
                    $model->user_id			= $user_id;
                    $model->token			= $cc_add['creditCard']['token'];
                    $model->identifier		= $cc_add['creditCard']['uniqueNumberIdentifier'];
                    $model->card_type		= $cc_add['creditCard']['cardType'];
                    $model->expired			= (int)$cc_add['creditCard']['expired'];
                    $model->image_url		= $cc_add['creditCard']['imageUrl'];
                    $model->exp_date		= $cc_add['creditCard']['expirationDate'];
                    $model->masked_num		= $cc_add['creditCard']['maskedNumber'];
                    $model->cc_verification	= $cc_add['creditCard']['verifications'][0]['status'];
                    $model->type			= Yii::$app->params['btree_environment'];
                    $model->default_card	= 1;
                    if($model->save()){
                        Yii::$app->session->setFlash('success', 'Your card has been added successfully.');
                        if(!empty($redirect_to))
                            return $this->redirect($redirect_to);
                        else
                            return $this->refresh();
                    }
                }
            }

        }
        return $this->render('card/index',['model'=>$model]);
    }
    /* Delete card information */
    public function actionDelete_card($id){
        $user_id	= Yii::$app->user->identity->id;
        $model		= new CreditCards();
        $result 	= $model->find()->where(['id'=>$id,'user_id'=>$user_id])->one();
        if(!$result)
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        $btree_model	= new Braintree();
        $response 		= $btree_model->deleteCard($result->token);
        if(isset($response->success)){
            Yii::$app->session->setFlash('success', 'Your card has been deleted successfully.');
            $result->delete();
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    /*
        User's Favorite Suppliers
    */

    public function actionFavSupplier(){
        $user_id = yii::$app->user->id;
        $query = UserFavSupplier::find()->select('supplier_id')->where(['user_id' => $user_id]);
        $query = User::find()->where(['in','id',$query]);
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
        ]);
        return $this->render('fav_supplier',['dataProvider' => $dataProvider]);
    }

    /*
        User's wishlist products
    */
    public function actionWishlist(){
        $user_id = yii::$app->user->id;
        $query = Menu::find()->joinWith('favMenu as fav')->where(['fav.user_id'=>$user_id]);
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
        ]);

        return $this->render('wishlist',['dataProvider'=>$dataProvider]);
    }

    public function actionProfilePic(){
        $json = [];
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_model =  User::findOne(Yii::$app->user->identity->id);
        $user_id = $user_model->id;
        $model = new UserData();
        $model = $model->find()->where(['user_id'=>$user_id])->one();
        if(empty($model)){
            $model = new UserData();
            $model->user_id = $user_id;
        }

        $profile_pic = UploadedFile::getInstance($model, 'profile_pic');
        if(!empty($profile_pic)){
            $library   			 = new Library();
            $model->profile_pic  = $library->saveFile($profile_pic,'users');
            if($model->save()){
                $thumb=EasyThumbnailImage::thumbnailFileUrl(
                    "../../frontend/web/uploads/".$model->profile_pic,200,200,EasyThumbnailImage::THUMBNAIL_INSET
                );
                $config = ['url' => Url::to(['/profile/del-profile-pic'])];
                $json = ['initialPreview' => Html::img($thumb, ['class'=>'file-preview-image', 'alt'=>'Profile Picture', 'title'=>'Profile Picture']), 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
            }
        }
        return $json;
    }

    public function invite(){
        $model = Yii::$app->user->identity;

        if(isset($_GET['download'])){
            $root=Yii::getAlias('@webroot').'/uploads/qrcodes/'.$model->id.'.png';
            return Yii::$app->response->sendFile($root, 'qr-code.png');
        }

        return $this->render('invite',['model'=>$model]);
    }

    public function myorders(){
        $user_info = Yii::$app->user->identity;
        $user_id = $user_info->id;
        $query = Order::find()->where(['user_id'=>$user_id])->andWhere(['date'=>date('Y-m-d')])->orWhere(['and',['NOT IN','order_status_id',[2,5]],['is','date',null],['user_id'=>$user_id],['=',new Expression("FROM_UNIXTIME(created_at,'%Y-%m-%d')"),new Expression('CURDATE()')]])->orWhere(['and',['NOT IN','order_status_id',[2,5]],['user_id'=>$user_id]]);
        $query		= Order::find()->joinWith('address as addr')->where(['and',['tbl_order.user_id'=>$user_id],['NOT in','order_status_id',[5,2]]]);
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC,
                ]
            ],
        ]);

        return $this->render('orders/myorders',['dataProvider'=>$dataProvider]);
    }

    public function completedorders(){
        $user_info = Yii::$app->user->identity;
        $user_id = $user_info->id;
        $query = Order::find()->where(['AND',['user_id'=>$user_id],['IN','order_status_id',[5,2]]]);
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC,
                ]
            ],
        ]);
        return $this->render('orders/completedorders',['dataProvider' => $dataProvider]);
    }

    public function vieworder(){
        $order_id = $_GET['order_id'] ?? 0;
        $user_info = yii::$app->user->identity;
        $user_id = $user_info->id;
        $order_info = Order::find()->where(['order_id' => $order_id,'user_id' => $user_id])->one();
        if(!$order_info)
            throw new ForbiddenHttpException('You\'re not authorized to view this page.');

        $ratingModel = new OrderRatings();
        $ratingModel->user_id = $user_id;
        $ratingModel->order_id = $order_id;
        $ratingModel->supplier_id = $order_info->supplier_id;
        if ($ratingModel->load(Yii::$app->request->post()) && $ratingModel->validate()) {
            if($ratingModel->save()){
                Yii::$app->session->setFlash('success', 'Your data has been changed.');
                return $this->refresh();
            }
        }
        return $this->render('orders/vieworder',['model' => $order_info,'ratingModel' => $ratingModel]);
    }

    public function track_order(){
        $order_id = $_GET['order_id'] ?? 0;
        $user_info = yii::$app->user->identity;
        $user_id = $user_info->id;
        $order_info = Order::find()->where(['order_id' => $order_id,'user_id' => $user_id])->one();
        if(!$order_info)
            throw new ForbiddenHttpException('You\'re not authorized to view this page.');

        return $this->render('orders/track_order',['model' => $order_info]);

    }

    public function actionMywallet(){

        $user_info = Yii::$app->user->identity;
        $check_national_id = $user_info->checkNationalId;
        if(empty($check_national_id))
            return $this->redirect(['/profile/update-national-id?type=mywallet']);
        $model = new Wallet();
        $query = $model->find()->where(['user_id' => $user_info->id])->orderBy('created_at desc');
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
        ]);

        $model->user_id = $user_info->id;
        $model->currency = config_default_currency;
        $model->type = 'added';
        $model->user_role = 1;
        $model->message = 'Added to wallet';
        $model->ip_address = Yii::$app->getRequest()->getUserIP();
        $model->scenario = 'add_money';
        if (isset($_POST['add-wallet']) && $model->load(Yii::$app->request->post()) ) {

            if($model->validate() && $model->save()){
                $token = '';
                $braintree_model = new Braintree();
                $creditcard = CreditCards::find()->where(['id' => $model->card_id,'user_id' => $user_info->id ])->one();
                if(!empty($creditcard))
                    $token = $creditcard->token;
                //$model->amount = Yii::$app->formatter->format($model->amount,['decimal',2]);
                $model->amount = number_format($model->amount, 2, '.', '');
                $response = $braintree_model->submitForSettlement($model->amount,$token,'Wallet '.$model->wallet_id);
                if($response['error']==1){
                    Wallet::findOne($model->wallet_id)->delete();
                    Yii::$app->session->setFlash('success', $response['message']);
                    return $this->refresh();
                }else{
                    $model->orderTransaction($response['response']);
                    Yii::$app->session->setFlash('success', 'Money has been added to your wallet.');
                    return $this->refresh();
                }
            }else{
                $all_errors	= [];
                $errors	= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $error	= !empty($all_errors) ? $all_errors[0] : '' ;
                Yii::$app->session->setFlash('success', $error);
                return $this->refresh();
            }
        }
        $credit_card_model = new CreditCards();
        $cards = $credit_card_model->find()->where(['user_id'=>$user_info->id,'type'=>Yii::$app->params['btree_environment']])->asArray()->all();

        $user_account = new UserBankAccount();
        $result = $user_account->find()->where(['user_id' => $user_info->id])->one();
        if(!empty($result))
            $user_account = $result;
        if (isset($_POST['new-card']) && $user_account->load(Yii::$app->request->post()) ) {
            $user_account->user_id = $user_info->id;
            if($user_account->validate() && $user_account->save()){
                Yii::$app->session->setFlash('success', 'Your data has been saved.');
                return $this->refresh();
            }else{
                $all_errors	= [];
                $errors	= $user_account->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $error	= !empty($all_errors) ? $all_errors[0] : '' ;
                Yii::$app->session->setFlash('success', $error);
                return $this->refresh();
            }
        }
        return $this->render('mywallet',['user_info' => $user_info,'model' => $model,'credit_card_model' => $credit_card_model,'cards' => $cards,'dataProvider' => $dataProvider,'user_account' => $user_account]);
    }
    public function actionInvoice($order_id){
        yii::$app->language = 'en-US';
        $pdf = Yii::$app->pdf;
        $user_id = yii::$app->user->id;
        $model = Order::find()->where(['AND',['user_id'=>$user_id],['order_id' => $order_id]])->one();

         $htmlContent = $this->renderPartial('_invoice',['model' => $model]);
        $pdf->content = $htmlContent;

        return $pdf->render();

    }
    public function actionUpdateNationalId(){
        $model = Yii::$app->user->identity;
        $check_national_id = $model->nationalId;
        if ($model->load(Yii::$app->request->post())) {
            $national_pic = UploadedFile::getInstance($model, 'national_pic');
            if(!empty($national_pic)){
                $library = new Library();
                $model->national_pic_approval = 1;
                $model->national_pic  = $library->saveFile($national_pic,'nationalID');
                if($model->save()){
                    Yii::$app->session->setFlash('success', 'Your file has been submitted. Please wait until approved by our team. ');
                    return $this->refresh();
                }
            }else $model->addError('national_pic','Please add national ID');
        }
        return $this->render('update_national_id',['model' => $model,'check_national_id' => $check_national_id]);
    }
}