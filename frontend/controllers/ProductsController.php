<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Menu;
use common\models\User;
use common\models\Library;
use common\models\MenuToAttributeOptions;
use api\models\Cart;

/**
 * Product controller
 */
class ProductsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /**
     * Product View
     * @param integer $product_id
     * @param string $slug
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionView($product_id,$slug){
        $model = Menu::find()->joinWith('supplierInfo')->where(['tbl_menu.id'=>$product_id, 'tbl_menu.slug' => $slug])->andWhere(['>','subscription_end',time()])->one();
        if(!$model) throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        $model->updateCounters(['view' => 1]);
        $cart_model = New Cart();
        $cart_model->menu_id = $product_id;
        if ($cart_model->load(Yii::$app->request->post()) && $cart_model->validate()) {
            if (Yii::$app->user->isGuest)
                return $this->redirect(['/login']);
        }
        $products_attributes = $model->productAttributes;

        return $this->render('view',['model' => $model, 'cart_model' => $cart_model,'products_attributes' => $products_attributes]);
    }
    public function actionAttributes(){
        $json = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post_data = Yii::$app->request->post()['Cart'];
        $menu_id = $post_data['menu_id'] ?? 0;
        $menu = Menu::find()->where(['status'=>'active','id' => $menu_id])->one();
        $library = new Library();
        if(!empty($menu)){
            $price = $menu->productPrice['price']['value'];
            $totals = [];
            if(isset($post_data['product_attributes'])){
                foreach($post_data['product_attributes'] as $k=>$option){
                    if(!empty($option)){
                        if(is_array($option)){
                            foreach($option as $val){
                                $menu_to_options = MenuToAttributeOptions::find()->where(['and',['name' => $val],['menu_id' => $menu_id],['!=','price','0.00']])->asArray()->one();
                                if(!empty($menu_to_options)){
                                    $totals[] = ['subtract' => $menu_to_options['subtract'],'price' =>$menu_to_options['price']];
                                }
                            }
                        }else{
                            $menu_to_options = MenuToAttributeOptions::find()->where(['and',['name' => $option],['menu_id' => $menu_id],['!=','price','0.00']])->asArray()->one();

                            if(!empty($menu_to_options)){
                                $totals[] = ['subtract' => $menu_to_options['subtract'],'price' =>$menu_to_options['price']];
                            }
                        }
                    }
                }
            }
            $option_prices = 0;
            if(!empty($totals)){
                foreach($totals as $total){
                    if($total['subtract'] == 0)
                        $option_prices += $total['price'];
                    if($total['subtract'] == 1)
                        $option_prices -= $total['price'];
                }
            }
        }
        $total = $price+$option_prices;
        $json['total']['amount'] = $library->currencyFormat($total,config_default_currency);
        $json['total']['value'] = $total;
        return $json;
    }
    public function actionAddToCart($confirm=0){

        $json = [];
        $is_error = false;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->user->isGuest){
            $is_error = true;
            $json['error'] = 'Please Login first.';
        }
        if($is_error==false){
            $post_data = Yii::$app->request->post();
            $menu_id = $post_data['Cart']['menu_id'] ?? 0;
            $menu = Menu::find()->where(['and',['!=','product_type',1],['status'=>'active'],['id' => $menu_id]])->one();
            $menu = Menu::findOne($menu_id);

            if(!empty($menu)){
                if($menu->product_type!=1 ){
                    if(($menu->status=='inactive' || $menu->dist_qty==0) )
                    $menu = [];
                }

            }

            if(!empty($menu)){
                $validate_cart = $menu->validateCart($post_data['Cart']);
                if(!empty($validate_cart)){
                    $json['error'] = $validate_cart['error'];
                    $is_error = true;
                }
            }else{
                $is_error = true;
                $json['error'] = 'Sorry this product does not exist or currently not active.';
            }
        }

        if($is_error==false){

            $model = new Cart();
            $library = new Library();
            $user_info = Yii::$app->user->identity;
            $post_data	= Yii::$app->request->post();

            $cart	= $library->json_decodeArray($user_info->cart);
            if($model->load($post_data) && $model->validate()) {
                $menu_id 	= $model->menu_id;
                $quantity 	= (int)$model->quantity;
                $result		= Menu::find()->where(['id'=>$menu_id])->one();
                if(!empty($result)){
                    if($quantity==0){
                        $json['success'] = 'Your dish has been removed successfully.';
                        unset($cart[$result->supplier_id][$menu_id]);
                        if(empty($cart[$result->supplier_id]))
                            unset($cart[$result->supplier_id]);
                        if(!empty($cart))
                            $cart_val = $library->json_encodeArray($cart);
                        else
                            $cart_val = null;
                        $user_info->cart = $cart_val;
                        $user_info->save();
                    }else{
                        if(!empty($cart)){
                            if(!isset($cart[$result->supplier_id])){
                                $supplier_info = user::findOne($result->supplier_id);
                                if($confirm==0){
                                    $json['is_allow'] 		= 0;
                                    $json['confirm']		= 'Your cart contains items from '.$supplier_info->name.' - '.$supplier_info->location.'. Do you wish to clear your cart and start a new order here?';
                                    $is_error = true;

                                    return $json;
                                }
                                if($confirm==1)
                                    $cart = [];
                            }
                        }
                        $product_attributes = $post_data['Cart']['product_attributes'] ?? [];
                        $cart[$result->supplier_id][$menu_id]	= ['menu_id'=>(int)$menu_id,'quantity'=>(int)$quantity,'product_attributes' => $product_attributes];

                        $cart_val = $library->json_encodeArray($cart);
                        $user_info->cart = $cart_val;
                        $user_info->save();
                        $json['is_allow'] 		= 1;

                        $json['success'] = 'Your cart has been updated successfully.';
                        Yii::$app->session->setFlash('success', $json['success']);
                    }
                }else{
                    $json['error']	= 'Sorry this product does not exist or currently not active.';
                    $is_error = true;
                }

            }else{
                $all_errors				= [];
                $errors					= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']= !empty($all_errors) ? $all_errors[0] : '' ;
                $is_error = true;
            }
        }
        return $json;
    }

    public function actionGuestAddToCart($confirm=0){
        $session = Yii::$app->session;
        $json = [];
        $is_error = false;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if($is_error==false){
            $post_data = Yii::$app->request->post();
            $menu_id = $post_data['Cart']['menu_id'] ?? 0;
            $menu = Menu::find()->where(['status'=>'active','id' => $menu_id])->one();

            if(!empty($menu)){
                $validate_cart = $menu->validateCart($post_data['Cart']);
                if(!empty($validate_cart)){
                    $json['error'] = $validate_cart['error'];
                    $is_error = true;
                }
            }else{
                $is_error = true;
                $json['error'] = 'Sorry this product does not exist or currently not active.';
            }
        }

        if($is_error==false){

            $model = new Cart();
            $library = new Library();
            $post_data	= Yii::$app->request->post();

            $cart	= $library->json_decodeArray($session->get('guest_cart'));
            if($model->load($post_data) && $model->validate()) {
                $menu_id 	= $model->menu_id;
                $quantity 	= (int)$model->quantity;
                $result		= Menu::find()->where(['id'=>$menu_id])->one();
                if(!empty($result)){
                    if($quantity==0){
                        $json['success'] = 'Your dish has been removed successfully.';
                        unset($cart[$result->supplier_id][$menu_id]);
                        if(empty($cart[$result->supplier_id]))
                            unset($cart[$result->supplier_id]);
                        if(!empty($cart))
                            $cart_val = $library->json_encodeArray($cart);
                        else
                            $cart_val = null;
                        $session->set('guest_cart', $cart_val);
                    }else{
                        if(!empty($cart)){
                            if(!isset($cart[$result->supplier_id])){
                                $supplier_info = user::findOne($result->supplier_id);
                                if($confirm==0){
                                    $json['is_allow'] 		= 0;
                                    $json['confirm']		= 'Your cart contains items from '.$supplier_info->name.' - '.$supplier_info->location.'. Do you wish to clear your cart and start a new order here?';
                                    $is_error = true;

                                    return $json;
                                }
                                if($confirm==1)
                                    $cart = [];
                            }
                        }
                        $product_attributes = $post_data['Cart']['product_attributes'] ?? [];
                        $cart[$result->supplier_id][$menu_id]	= ['menu_id'=>(int)$menu_id,'quantity'=>(int)$quantity,'product_attributes' => $product_attributes];

                        $cart_val = $library->json_encodeArray($cart);
                        $session->set('guest_cart', $cart_val);
                        $json['is_allow'] 		= 1;

                        $json['success'] = 'Your cart has been updated successfully.';
                        Yii::$app->session->setFlash('success', $json['success']);
                    }
                }else{
                    $json['error']	= 'Sorry this product does not exist or currently not active.';
                    $is_error = true;
                }

            }else{
                $all_errors				= [];
                $errors					= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']= !empty($all_errors) ? $all_errors[0] : '' ;
                $is_error = true;
            }
        }
        return $json;
    }
}