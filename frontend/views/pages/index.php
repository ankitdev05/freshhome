<?php
$this->title = empty($model->meta_title) ? config_title : $model->meta_title;
\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => empty($model->meta_description) ? config_meta_description : $model->meta_description
]);
\Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' =>  empty($model->meta_keyword) ? config_meta_keywords : $model->meta_keyword
]);
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="bg-sec">
    <div class="container">
        <div class="heading">
            <h3><?= $model->title ?></h3>
            <div class="divider"></div>
        </div>
    </div>
</div>
<section class="contact_info about-page">
    <div class="container">
        <div class="row">
            <div class="about-div">
                <?= $model->description ?>
            </div>
        </div>
    </div>
</section>

