<?php
use yii\helpers\Url;
use common\models\Menu;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

$relatedCategory = $model->menuCategories;
if(!empty($relatedCategory)) {

    foreach ($relatedCategory as $rCategory)
        $category[] = $rCategory->category_id;

    $session = Yii::$app->session;
    $screen_id = $session->get('screen_id') ?? 1;

    $website_url = Url::to(Yii::$app->request->baseUrl . '/', true);
    $query = Menu::find()->joinWith(['menuCategories as m2c'])->where(['and', ['image_approval' => 1, 'is_delete' => 0, 'test_data' => 0, 'product_type' => $model->product_type]])->andWhere(['and', ['in', 'm2c.category_id', $category], ['NOT IN', 'tbl_menu.id', $model->id]])->limit(10);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => false,
        'sort' => [
            'defaultOrder' => [
                'id' => SORT_DESC,
            ]
        ],
    ]);

    $count = $dataProvider->getTotalCount();
    if ($count > 0) {
        ?>
        <section class="latest-products">
            <div class="container">
                <div class="inner-product">
                    <h4 class="latestprohead">RELATED PRODUCTS</h4>
                    <div class="product-slider">

                        <?= ListView::widget([
                            'layout' => '{items}',
                            'itemView' => '//products/_slider_product',
                            'dataProvider' => $dataProvider,
                            'options' => [
                                'tag' => 'div',
                                'class' => 'autoplay',
                                'id' => 'list-wrapper',
                            ],
                            'itemOptions' => [
                                'tag' => false
                            ],
                        ]); ?>

                    </div>
                </div>
            </div>
        </section>
        <?php
        $this->registerJs("
    $('.autoplay').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        margin:30,
        autoplaySpeed: 2000,
        nextArrow: '<i class=\"fa fa-arrow-right\"></i>',
        prevArrow: '<i class=\"fa fa-arrow-left\"></i>',
        responsive: [
              {
                 breakpoint: 768,
                 settings: 
                  {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 3
                  }
               },
			   {
                breakpoint: 667,
                settings:
                {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '0px',
                  slidesToShow: 2
                }
              },
              {
                breakpoint: 480,
                settings:
                {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '0px',
                  slidesToShow: 2
                }
              }
        ]
    });
");
    }
}