<?php
use yii\helpers\Url;
use yii\helpers\Html;

$dish_info = $model->dishData;
$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);

$fav_dish = $model->favDish;
$class = 'far fa-heart';
$fav_val = 1;
if($fav_dish==1){
    $class = 'fas fa-heart';
    $fav_val = 0;
}
$session = Yii::$app->session;
$supplier_id = $session->has('is_supplier');
?>
<div class="item">
    <?= Html::img($model->mainImage,['title'=>$model->dish_name,'alt'=>$model->dish_name,'class' => 'product-grid']) ?>
    <div class="product-content">
        <div class="title">
            <?= Html::a($model->dish_name,$model->proUrl,['title'=>$model->dish_name]) ?>
        </div>
		<p class="mb-0 line-through saleprice">AED 20.00</p>
        <div class="price"><span class="price-tag"><b> <?= $dish_info['currency'] ?> <?= $dish_info['dish_price'] ?></b></span>
            <span class="rating-rate"><i class="fas fa-star"></i>(<?= $dish_info['total_review'] ?>)</span>
        </div>
        <?php if(!$supplier_id){ ?>
        <ul class="bottom-icon">
            <?php if (!Yii::$app->user->isGuest){ ?>
                <li>
                    <?= Html::a(Html::img($website_url.'img/cart.png',['class'=>'img-resposive','alt'=>'Add to cart','title'=>'Add to cart']),'javascript:void(0)',['onClick'=>'return addToCart('.$model->id.','.($model->myCart+1).',0)']) ?>
                </li>
                <li class="wishlist">
                    <?= Html::a(Html::tag('i','',['class' => $class]),'javascript:void(0)',['onClick' => 'return addToFav('.$model->id.','.$fav_val.')']) ?>
                </li>
            <?php }else{ ?>
                <li>
                    <?= Html::a(Html::img($website_url.'img/cart.png',['class'=>'img-resposive','alt'=>'Add to cart','title'=>'Add to cart']),'javascript:void(0)',['onClick'=>'return guestAddToCart('.$model->id.',1,0)']) ?>
                </li>
                <li class="wishlist">
                    <?= Html::a('<i class="far fa-heart"></i>','javascript:void(0)',['onClick'=>'return warningPopup("Please login first.")']) ?>
                </li>
            <?php } ?>

        </ul>
        <?php } ?>
    </div>
</div>