<?php

use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use common\models\User;
use common\models\MenuRating;

$this->title = $model->dish_name;
if($model->product_type==1)
    $product_info = $model->dishInfo;
else{
    $product_info = $model->productInfo;
    $product_info['dish_price'] = $product_info['product_price'];
    $product_info['dist_qty'] = $product_info['quantity'];
}

$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);
$cart_model->quantity = 1;

$session = Yii::$app->session;
$supplier_id = $session->has('is_supplier');

$supplier_info = User::findOne($product_info['supplier_id']);
$ratings = MenuRating::find()->where(['menu_id' => $model->id])->orderBy('id desc')->all();
$pro_images = $model->images;
$more_images = [];
if(!empty($pro_images)){
    foreach( $pro_images as $pro_image){
        $more_images[] = ['large' => $model->getImage($pro_image->image_name,400,400),'small' => $model->getImage($pro_image->image_name,115,115)];
    }

}
$final_price = $model->finalPrice;
?>

<section class="product p-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-12 mb-3">
                <div class="img-product">
                    <div class="wrapper">
                        <div class="portfolio-item-slider">
                            <div class="slick-slider-item">
                                <?= Html::img($model->getMainImage(400,400),['title'=>$model->dish_name,'alt'=>$model->dish_name,'class' => 'img-fluid']) ?>
                            </div>
                            <?php if(!empty($more_images)){
                                foreach($more_images as $more_image){
                                    ?>
                                    <div class="slick-slider-item">
                                        <?= Html::img($more_image['large'],['title'=>$model->dish_name,'alt'=>$model->dish_name,'class' => 'img-fluid']) ?>
                                    </div>
                                    <?php
                                }
                            } ?>
                        </div>
                        <div class="portfolio-thumb-slider">
                            <div class="slick-slider-item">
                                <?= Html::img($model->getMainImage(115,115),['title'=>$model->dish_name,'alt'=>$model->dish_name,'class' => 'img-thumbnail']) ?>
                            </div>
                            <?php if(!empty($more_images)){
                                foreach($more_images as $more_image){
                                    ?>
                                    <div class="slick-slider-item">
                                        <?= Html::img($more_image['small'],['title'=>$model->dish_name,'alt'=>$model->dish_name,'class' => 'img-thumbnail']) ?>
                                    </div>
                                    <?php
                                }
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="item-about">
                    <h2><?= $model->dish_name ?></h2>
                    <?php if($model->product_type==1 && isset($product_info['meals'][0])){ ?>
                    <p><?= $product_info['meals'][0]['meal_detail']['meal_name'] ?> for <?= $product_info['dish_serve'] ?> people</p>
                    <?php } ?>
                    <ul class="rating">
                        <li>
                            <a href="javascript:void(0)">
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
					<span class="reviews-star"><a href="javascript:void(0)">(<?= $model->avgReviews ?> <?= Yii::t('app','reviews') ?>)</a></span>
                    <span class="price-item">
                        <?php if($final_price['discount'] > 0){ ?>
                            <del><?= $final_price['product_price_label'] ?></del> <?= $final_price['discount'] ?>% <?= Yii::t('app','off') ?><br/>
                        <?php } ?>
                        <?= $final_price['real_price_label'] ?>
                    </span>
                    <?php if(!$supplier_id){ ?>
                    <?php $form = ActiveForm::begin(['id' => 'add-to-cart']); ?>
                    <?= $form->field($cart_model, 'quantity')->hiddenInput()->label(false) ?>
                    <?= $form->field($cart_model, 'menu_id')->hiddenInput()->label(false) ?>
                    <div class="qnty mb-3">
                        <div class="quantity">

                            <div class="QTY">
                                <label><?= Yii::t('app','Qty') ?> :</label>
                                <input type="hidden" value="1" id="cart-quantity1" name="Cart[quantity]"/>
                                <ul>
                                    <li>
                                        <a href="javascript:void(0)" id="sub_"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="qno">1</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" id="add_"><i class="fas fa-plus"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?= $this->render('_product_attribute',['products_attributes' => $products_attributes,'model' => $cart_model,'form' => $form,'product_info'=>$product_info]) ?>
                    <div class="add-cart">
                        <ul>

                                <?php if(yii::$app->user->isGuest){
                                    echo '<li>';
                                    echo Html::button('<i class="fa fa-cart-plus" aria-hidden="true"></i> '.Yii::t('app','ADD TO CART'), ['class' => 'btn custom-btn mb-2 btncartproduct', 'onClick' => 'return guestAddCart()']);
                                    echo '</li>';

                                    echo Html::button('<i class="far fa-heart"></i> '.Yii::t('app','ADD TO WISHLIST'), ['class' => 'btn custom-btn green btnwishlist mb-2', 'onClick' => 'return warningPopup("Please login first.")']);

                                }else{
                                    echo '<li>';
                                        echo Html::button('<i class="fa fa-cart-plus" aria-hidden="true"></i> '.Yii::t('app','ADD TO CART'), ['class' => 'btn custom-btn mb-2 btncartproduct', 'onClick' => 'return addCart()']);
                                    echo '</li>';
                                    $fav_dish = $model->favDish;
                                        $class = 'far fa-heart';
                                        $text = Yii::t('app','ADD TO WISHLIST');
                                        $fav_val = 1;
                                        if($fav_dish==1){
                                            $class = 'fas fa-heart';
                                            $fav_val = 0;
                                            $text = 'Remove from wishlist';
                                        }
                                    echo Html::button(Html::tag('i','',['class' => $class]).' '.$text,['class' => 'btn custom-btn green','onClick' => 'return addToFav('.$model->id.','.$fav_val.')']);
                                } ?>
                        </ul>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <?php } ?>
                    <div class="user-view">
                        <ul>
                            <li>
                                <span class="no-view"><?= $product_info['user_view'] ?></span> 
                                <span class="user-color"><?= Yii::t('app','User Views') ?></span>
                            </li>
                            <li>
                                <span class="no-view"><?= $product_info['dist_qty'] ?></span>
                                <span class="user-color"><?= Yii::t('app','Available Items') ?></span>
                            </li>
                            <li>
                                <span class="no-view"><?= $product_info['delivered_order'] ?></span>
                                <span class="user-color"><?= Yii::t('app','Delivered Items') ?></span>
                            </li>
                        </ul>

                    </div>
                    <div class="additon-info">
                        <p>
                            <img src="https://freshhomee.com/web/assets/thumbnails/48/48e499bfbc43060eda7ef3c2f63dda49.png" width="50" class="imground img-fluid"> &nbsp; <b><?= Yii::t('app','Made By') ?>:</b> <a href="<?= $supplier_info->profileUrl ?>"><?= $supplier_info->name ?></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="description">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="product-tabs">
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><?= Yii::t('app','Description') ?></a>

							<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"><?= Yii::t('app','Reviews') ?></a>
						</div>
					</nav>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
							<p>
								<?= $model->dish_description ?>
							</p>
						</div>

						<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
							<h3 class="cmt-profile">
                                <?= Yii::t('app','Recent Comments') ?>
								<?php if(!empty($ratings)){ ?>
									<span class="view-p"><a href="javascript:void(0);">View All</a></span>
								<?php } ?>
							</h3>
								<?= $this->render('_product_review',['model'=>$model,'ratings' => $ratings]) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
<?= $this->render('related_products',['model' => $model]) ?>
<style>
    .QTY ul li a i{font-weight:inherit;}
</style>
<?php
$this->registerJs("
    $('.view-p').on('click',function(){
        $(this).hide();
        $('.Comments-user').show();
    })
    $('#add_').on('click',function(){
        var qty = $('#cart-quantity1').val();
        var val = parseInt(qty)+parseInt(1);
        $('#cart-quantity1').val(val);
        $('.qno').html(val);
    })
      $('#sub_').on('click',function(){
        var qty = $('#cart-quantity1').val();
        var val = parseInt(qty)-parseInt(1);
        if(val>=1){
            $('#cart-quantity1').val(val);
            $('.qno').html(val);
        }
        
    })
    $('.portfolio-thumb-slider').slick({
        slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.portfolio-item-slider',
            dots: false,
            arrows: false,
            focusOnSelect: true,
            infinite: false
	});
	$('.portfolio-item-slider').slick({
		slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.portfolio-thumb-slider',
        infinite: false
	});
");