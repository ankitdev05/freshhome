<?php
use yii\helpers\Url;
use yii\helpers\Html;

$fav_dish = $model->favDish;
$class = 'far fa-heart';
$fav_val = 1;
if($fav_dish==1){
    $class = 'fas fa-heart';
    $fav_val = 0;
}
$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);
$session = Yii::$app->session;
$supplier_id = $session->has('is_supplier');
?>
<div class="col-md-3 col-6 product-grids">
    <div class="agile-products">
        <div class="item">
            <?= Html::a(Html::img($model->mainImage,['title'=>$model->dish_name,'alt'=>$model->dish_name,'class' => 'product-grid']),$model->proUrl) ?>
            <div class="product-content">
                <div class="title"> 
                    <p class="producttitle"><a href="<?= $model->proUrl ?>"><?= $model->dish_name ?></a></p>
                </div>
                <div class="price">
                    <span class="price-tag"><b> <?= config_default_currency.' '.$model->dish_price ?></b></span> <span class="rating-rate"><i class="fas fa-star"></i> (<?= $model->avgReviews ?>)</span>
                </div>
                <?php if(!$supplier_id){ ?>
                <ul class="bottom-icon">
                        <?php if (!Yii::$app->user->isGuest){ ?>
                            <li>
                                <?= Html::a('<i class="fas fa-cart-plus primary-color"></i>','javascript:void(0)',['onClick'=>'return addToCart('.$model->id.','.($model->myCart+1).',0)']) ?>
								 <?php Html::a(Html::img($website_url.'img/cart.png',['class'=>'img-resposive','alt'=>'Add to cart','title'=>'Add to cart']),'javascript:void(0)',['onClick'=>'return addToCart('.$model->id.','.($model->myCart+1).',0)']) ?>
                            </li>
                            <li class="wishlist">
                                <?= Html::a(Html::tag('i','',['class' => $class]),'javascript:void(0)',['onClick' => 'return addToFav('.$model->id.','.$fav_val.')']) ?>
                            </li>
                        <?php }else{ ?> 
                            <li>
								<?= Html::a('<i class="fas fa-cart-plus primary-color"></i>','javascript:void(0)',['onClick'=>'return guestAddToCart('.$model->id.',1,0)']) ?>
                                 <?php Html::a(Html::img($website_url.'img/cart.png',['class'=>'img-resposive','alt'=>'Add to cart','title'=>'Add to cart']),'javascript:void(0)',['onClick'=>'return guestAddToCart('.$model->id.',1,0)']); ?>
                            </li>
                            <li class="wishlist">
                                <?= Html::a(Html::tag('i','',['class' => $class]),'javascript:void(0)',['onClick' => 'return warningPopup("Please login first.")']) ?>
                            </li>
                        <?php } ?>

                </ul>
                <?php } ?>
            </div>

        </div>
    </div>
</div>

<section class="product-filter-section productpage pb-5 d-none">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 mt-3">
				<div class="product-filter mt-5">
					<p class="mb-1">Filter By:</p>
					<h5>Category</h5>
					<ul class="cat-filter">
						<li>Baby Clothes &amp; Shoes</li>
						<li>Diet and Nutrition</li>
						<li>Cameras and Accessories</li>
						<li>Fashion</li>
						<li>Baby Clothes &amp; Shoes</li>
						<li>Diet and Nutrition</li>
						<li>Fashion</li>
						<li>Cameras and Accessories</li>
						<li>Diet and Nutrition</li>
						<li>Fashion</li>
					</ul>
					<h5>Cusinies</h5>
					<ul class="cat-filter">
						<li>Arabic</li>
						<li>Fusion</li>
						<li>Indian and Punjabi</li>
						<li>Thai</li>
					</ul>
					<h5>Meal Type</h5>
					<ul class="cat-filter mealtype">
						<li>Breakfast</li>
						<li>Dessert</li>
						<li>Indian and Punjabi</li>
						<li>Thai</li>
					</ul>
				</div>
			</div>
			<div class="col-md-10">
				<div class="productlist mt-4">
					<div class="row">
						<div class="col-md-6">
							<h5 class="mt-1">Hot Deals</h5>
						</div>
						<div class="col-md-6">
							<div class="sortfeature text-right">
								<div class="dropdown show">
									<a class="btn dropdown-toggle btnfeatured" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span>Sort By :</span>&nbsp;Featured
									</a>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<a class="dropdown-item" href="#">Action</a>
									<a class="dropdown-item" href="#">Another action</a>
									<a class="dropdown-item" href="#">Something else here</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="latest-products best-selling-product allproductlisting mt-3">
					<div class="row">
						<div class="col-md-3">
							<div class="productslide1 mb-3">
								<span class="product-category innerprocat">Chinese</span>
								<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
								<div class="product-info">
									<h4>The quick brown jumps over a lazy dog</h4>
									<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
									<div class="row">
										<div class="col-md-6 col-7 pr-0">
											<h5 class="mb-0 mt-1">AED 23.56</h5>
										</div>
										<div class="col-md-6 col-5 pl-0">
											<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-6">
											<span><i class="fas fa-cart-plus primary-color"></i></span>
										</div>
										<div class="col-md-6 col-6 text-right">
											<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="productslide1 mb-3">
								<span class="product-category innerprocat">Chinese</span>
								<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
								<div class="product-info">
									<h4>The quick brown jumps over a lazy dog</h4>
									<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
									<div class="row">
										<div class="col-md-6 col-7 pr-0">
											<h5 class="mb-0 mt-1">AED 23.56</h5>
										</div>
										<div class="col-md-6 col-5 pl-0">
											<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-6">
											<span><i class="fas fa-cart-plus primary-color"></i></span>
										</div>
										<div class="col-md-6 col-6 text-right">
											<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="productslide1 mb-3">
								<span class="product-category innerprocat">Chinese</span>
								<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
								<div class="product-info">
									<h4>The quick brown jumps over a lazy dog</h4>
									<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
									<div class="row">
										<div class="col-md-6 col-7 pr-0">
											<h5 class="mb-0 mt-1">AED 23.56</h5>
										</div>
										<div class="col-md-6 col-5 pl-0">
											<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-6">
											<span><i class="fas fa-cart-plus primary-color"></i></span>
										</div>
										<div class="col-md-6 col-6 text-right">
											<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="productslide1 mb-3">
								<span class="product-category innerprocat">Chinese</span>
								<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
								<div class="product-info">
									<h4>The quick brown jumps over a lazy dog</h4>
									<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
									<div class="row">
										<div class="col-md-6 col-7 pr-0">
											<h5 class="mb-0 mt-1">AED 23.56</h5>
										</div>
										<div class="col-md-6 col-5 pl-0">
											<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-6">
											<span><i class="fas fa-cart-plus primary-color"></i></span>
										</div>
										<div class="col-md-6 col-6 text-right">
											<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="productslide1 mb-3">
								<span class="product-category innerprocat">Chinese</span>
								<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
								<div class="product-info">
									<h4>The quick brown jumps over a lazy dog</h4>
									<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
									<div class="row">
										<div class="col-md-6 col-7 pr-0">
											<h5 class="mb-0 mt-1">AED 23.56</h5>
										</div>
										<div class="col-md-6 col-5 pl-0">
											<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-6">
											<span><i class="fas fa-cart-plus primary-color"></i></span>
										</div>
										<div class="col-md-6 col-6 text-right">
											<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="productslide1 mb-3">
								<span class="product-category innerprocat">Chinese</span>
								<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
								<div class="product-info">
									<h4>The quick brown jumps over a lazy dog</h4>
									<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
									<div class="row">
										<div class="col-md-6 col-7 pr-0">
											<h5 class="mb-0 mt-1">AED 23.56</h5>
										</div>
										<div class="col-md-6 col-5 pl-0">
											<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-6">
											<span><i class="fas fa-cart-plus primary-color"></i></span>
										</div>
										<div class="col-md-6 col-6 text-right">
											<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="productslide1 mb-3">
								<span class="product-category innerprocat">Chinese</span>
								<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
								<div class="product-info">
									<h4>The quick brown jumps over a lazy dog</h4>
									<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
									<div class="row">
										<div class="col-md-6 col-7 pr-0">
											<h5 class="mb-0 mt-1">AED 23.56</h5>
										</div>
										<div class="col-md-6 col-5 pl-0">
											<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-6">
											<span><i class="fas fa-cart-plus primary-color"></i></span>
										</div>
										<div class="col-md-6 col-6 text-right">
											<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="productslide1 mb-3">
								<span class="product-category innerprocat">Chinese</span>
								<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
								<div class="product-info">
									<h4>The quick brown jumps over a lazy dog</h4>
									<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
									<div class="row">
										<div class="col-md-6 col-7 pr-0">
											<h5 class="mb-0 mt-1">AED 23.56</h5>
										</div>
										<div class="col-md-6 col-5 pl-0">
											<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-6">
											<span><i class="fas fa-cart-plus primary-color"></i></span>
										</div>
										<div class="col-md-6 col-6 text-right">
											<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>