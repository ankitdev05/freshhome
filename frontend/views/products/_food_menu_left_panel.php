<?php

use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use common\models\Meal;
use common\models\Cuisine;
use common\models\Filters;
use common\models\Product;
use yii\helpers\ArrayHelper;

$session = Yii::$app->session;
$screen_id = $session->get('screen_id');

$categories = ArrayHelper::map(Product::find()->select('*')->joinWith('categoryName as cn')->where(['status'=>'active','cn.language_id'=>language_id,'cat_id'=>$screen_id])->orderBy('cn.category_name asc')->asArray()->all(),'category_id','category_name');

$meals = ArrayHelper::map(Meal::find()->select('*')->joinWith('mealName as mn')->where(['status'=>'active','mn.language_id'=>language_id])->orderBy('mn.meal_name asc')->asArray()->all(),'meal_id','meal_name');

$cuisines = ArrayHelper::map(Cuisine::find()->select('*')->joinWith('cuisineName as cn')->where(['status'=>'active','cn.language_id'=>language_id])->orderBy('cn.cuisine_name asc')->asArray()->all(),'cuisine_id','cuisine_name');

$model = new Filters();
$model->load(Yii::$app->request->get());
?>
<?php Pjax::begin(); ?>
<?php $form = ActiveForm::begin(['id' => 'filter-products','method'=>'GET','action' => Url::to(Yii::$app->request->baseUrl.'/',true)]); ?>
<div class="bs-example">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <div class="accordion" id="accordionExample">
        <div class="card">
            <div class="card-header" id="meals_for">
                <h2 class="mb-0">
                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"><i class="fa fa-plus"></i> Meals For</button>
                </h2>
            </div>

            <div id="collapseOne" class="collapse" aria-labelledby="meals_for" data-parent="#accordionExample">
                <div class="card-body">
                    <?= $form->field($model, 'meals')->inline(true)->checkboxList($meals)->label(false) ?>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="categories">
                <h2 class="mb-0">
                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo"><i class="fa fa-plus"></i> Select categories</button>
                </h2>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="categories" data-parent="#accordionExample">
                <div class="card-body">
                    <?= $form->field($model, 'categories')->inline(true)->checkboxList($categories)->label(false) ?>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="cuisines">
                <h2 class="mb-0">
                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree"><i class="fa fa-plus"></i> Select cuisines</button>
                </h2>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="cuisines" data-parent="#accordionExample">
                <div class="card-body">
                    <?= $form->field($model, 'cuisines')->inline(true)->checkboxList($cuisines)->label(false) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group mt-3">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-danger']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>
<?php
$this->registerJs('
    $(".collapse.show").each(function(){
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });
    
    $(".collapse").on(\'show.bs.collapse\', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on(\'hide.bs.collapse\', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
');