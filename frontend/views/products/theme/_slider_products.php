<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\BrandDescription;
$dish_info = $model->dishData;
$fav_dish = $model->favDish;
$class = 'far fa-heart';
$fav_val = 1;
if($fav_dish==1){
    $class = 'fas fa-heart';
    $fav_val = 0;
}
$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);

$session = Yii::$app->session;
$supplier_id = $session->has('is_supplier');
$final_price = $model->finalPrice;
$brand_name = '';
if(!empty( $model->brand_id)){
    $brand_description = BrandDescription::find()->where(['brand_id' => $model->brand_id,'language_id' => language_id])->one();
    if(!empty($brand_description))
        $brand_name = $brand_description->brand_name;
}

?>
<div class="item productslide1">
    <?php if(isset($show_cat) && $show_cat ==true){  ?>
        <?php if(!empty($brand_name)) echo Html::tag('span',$brand_name,['class' => 'product-category innerprocat']) ?>
    <?php } ?>
    <?= Html::img($model->mainImage,['title'=>$model->dish_name,'alt'=>$model->dish_name,'class' => 'img-fluid']) ?>
    <div class="product-info">
        <h4><?= Html::a($model->dish_name,$model->proUrl,['title'=>$model->dish_name]) ?></h4>
		
		<div class="productoffprice">
        <?php if($final_price['discount'] > 0){ ?>
            <span class="price-off"><?= $final_price['product_price_label'] ?></span>&nbsp;&nbsp;<cite class="sale-off"><?= $final_price['discount'] ?>% OFF</cite>
        <?php } ?>
		</div>

        <div class="form-row">
            <div class="col-md-7 col-6">
                <h5 class="mb-0 mt-1"><?= $final_price['real_price_label'] ?></h5>
            </div>
            <div class="col-md-5 col-6">
                <p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span><?= $dish_info['total_review'] ?></span></p>
            </div>
        </div>

        <?php if(!$supplier_id){ ?>
        <div class="row">
            <?php if (!Yii::$app->user->isGuest){ ?>
            <div class="col-md-6 col-6">
                <span>
                    <?= Html::a('<i class="fas fa-cart-plus primary-color"></i>','javascript:void(0)',['onClick'=>'return addToCart('.$model->id.','.($model->myCart+1).',0)']) ?>

                </span>
            </div>
            <div class="col-md-6 col-6 text-right">
                <span>
                    <?= Html::a(Html::tag('i','',['class' => $class]),'javascript:void(0)',['onClick' => 'return addToFav('.$model->id.','.$fav_val.')']) ?>
                </span>
            </div>
            <?php }else{ ?>
                <div class="col-md-6 col-6">
                    <span>
                         <?= Html::a('<i class="fas fa-cart-plus primary-color"></i>','javascript:void(0)',['onClick'=>'return guestAddToCart('.$model->id.',1,0)']) ?>
                        </span>
                </div>
                <div class="col-md-6 col-6 text-right">
                    <span>
                        <?= Html::a('<i class="far fa-heart"></i>','javascript:void(0)',['onClick'=>'return warningPopup("Please login first.")']) ?>
                    </span>
                </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
</div>