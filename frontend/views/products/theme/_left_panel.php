<?php
use common\models\HomeCategory;
use common\models\Meal;
use common\models\Cuisine;

$all_cuisines = Cuisine::find()->select('*')->joinWith('cuisineName as cn')->where(['status' => 'active','cn.language_id' => language_id])->orderBy('cn.cuisine_name ASC')->asArray()->all();

$all_meals = Meal::find()->select('*')->joinWith('mealDescription as md')->where(['status' => 'active','md.language_id' => language_id])->orderBy('md.meal_name ASC')->asArray()->all();

$all_categories = HomeCategory::find()->select('*')->joinWith('homeCategoryDescription as hd')->where(['status' => 1,'hd.language_id' => language_id])->orderBy('hd.home_category_name ASC')->asArray()->all();

?>
<p class="mb-1"><?= Yii::t('app', 'Filter By') ?>:</p>
<h5><?= Yii::t('app', 'Category') ?></h5>
<ul class="cat-filter">
    <?php if(!empty($all_categories)){ ?>
        <?php foreach ($all_categories as $all_category){ ?>
            <li><?= $all_category['home_category_name'] ?></li>
        <?php } ?>
    <?php } ?>
</ul>
<h5><?= Yii::t('app', 'Cuisines') ?></h5>
<?php if(!empty($all_cuisines)){ ?>
<ul class="cat-filter">
    <?php foreach ($all_cuisines as $all_cuisine){ ?>
        <li><?= $all_cuisine['cuisine_name'] ?></li>
    <?php } ?>
</ul>
<?php } ?>

<h5><?= Yii::t('app', 'Meal Type') ?></h5>
<?php if(!empty($all_meals)){ ?>
<ul class="cat-filter mealtype">
    <?php foreach ($all_meals as $all_meal){ ?>
        <li><?= $all_meal['meal_name'] ?></li>
    <?php } ?>
</ul>
<?php } ?>