<?php
use kartik\select2\Select2;
use common\models\Library;

$library = new Library();
if(!empty($products_attributes)){
    echo '<div class="w-100">&nbsp;</div><div class="row w-100">';
    foreach($products_attributes as $products_attribute){
        $data =[];
        foreach($products_attribute['option_data'] as $value){
            if($value['price']!='0.00'){
                $price = $library->currencyFormat($value['price'],$product_info['currency']);
                $data[$value['name']] = $value['name'].'('.$value['price_prefix'].' '.$price.')';
            }else
                $data[$value['name']] = $value['name'];

        }

?>
	<div class="col-md-12">	
		<div class="productattr">
			<div class="row">
				<div class="col-sm-12"><h5><?= $products_attribute['name'] ?></h5></div>
				<?php if($products_attribute['type']=='select'){ ?>
				<div class="col-md-6">
				<?= $form->field($model, 'product_attributes['.$products_attribute['option_id'].']')->widget(Select2::classname(), [
				   'data' => $data,
				   'options' => [
					   'prompt' => 'Please select...' ,
					   'id' => 'product_attributes_'.$products_attribute['option_id'],
					   'class' => 'product_attributes'
				   ],
				])->label(false)
				?>
				</div>

					<?php }elseif($products_attribute['type']=='radio'){ ?>
                    <div class="col-md-12">
					    <?= $form->field($model, 'product_attributes['.$products_attribute['option_id'].']')->radioList($data)->label(false)
					?>
				    </div>

					<?php }elseif($products_attribute['type']=='checkbox'){ ?>
                    <div class="col-md-12">
					<?= $form->field($model, 'product_attributes['.$products_attribute['option_id'].']')->checkboxList($data)->inline(true)->label(false)
					?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
    <div class="w-100"><hr/></div>
<?php
    }
    echo '</div>';
}
$this->registerJs("
$('#add-to-cart input, #add-to-cart select').on('change',function(){
    $.ajax({
        url : website_url+'products/attributes',
        dataType: 'json',
        type: 'POST',
        data : $('#dd-to-cart input[type=\'text\'], #add-to-cart input[type=\'hidden\'], #add-to-cart input[type=\'radio\']:checked, #add-to-cart input[type=\'checkbox\']:checked, #add-to-cart select, #add-to-cart textarea'),
        success : function(json){
            if(json['total']['amount'])
                $('.price-item').html(json['total']['amount']);
        }
    })
})
");