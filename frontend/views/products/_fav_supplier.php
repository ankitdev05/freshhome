<?php
use yii\helpers\Html;

$class = 'fas fa-heart';
$fav_val = 0;
?>
<div class="col-md-3 col-6 product-grids">
    <div class="agile-products">
        <div class="item">
            <span class=" wishlisticon"><?= Html::a(Html::tag('i','',['class' => $class,'style'=>'color:#2B6707']),'javascript:void(0)',['onClick' => 'return addToFavSupp('.$model->id.','.$fav_val.')']) ?></span>
            <?php if(empty($model->profile_pic)) $model->profile_pic = 'no_image.png';
            echo Html::a(Html::img($model->getSupplierImage($model->profile_pic,300,300),['alt'=>$model->name,'title'=>$model->name,'class' => 'product-grid']),$model->profileUrl) ?>

            <div class="product-content">
                <div class="title">
                    <p class="producttitle"><a href="<?= $model->profileUrl ?>"><?= $model->name ?></a></p>
                </div>

            </div>
        </div>
    </div>
</div>
