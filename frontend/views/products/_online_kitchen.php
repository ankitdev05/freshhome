<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\User;
?>
<div class="col-md-3 col-6 product-grids">
    <div class="agile-products">
        <div class="item">
            <?php if(empty($model->profile_pic)) $model->profile_pic = 'no_image.png';
            echo Html::img($model->getSupplierImage($model->profile_pic,150,150),['alt'=>$model->name,'title'=>$model->name]) ?>

            <div class="product-content">
				<div class="form-row">
					<div class="col-md-10">
						<div class="title">
							<?= Html::a($model->name,$model->profileUrl) ?>
						</div>
					</div>
					<div class="col-md-2 text-center">
						<cite class="wishlistbadge"><a href="javascript:void(0)"><i class="far fa-heart"></i></a></cite>
					</div>
				</div>
            </div>

        </div>
    </div>
</div>