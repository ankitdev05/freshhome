<?php
if(!empty($ratings)){
    foreach($ratings as $k=>$rating){
        $info = $rating->user->userDetail;
        $style= '';
        if($k>0)
            $style = 'display:none;';
?>
<div class="Comments-user pb-3 mt-2" style="<?= $style ?>">
        <div class="form-row">
            <div class="col-lg-1 col-md-1 col-sm-2 col-3">
                <div class="user-pic">
                    <img src="<?= $info['profile_pic'] ?>" alt="<?= $info['name'] ?>" title="<?= $info['name'] ?>" class="img-fluid img-responsive">
                </div>
            </div>
            <div class="col-lg-11 col-md-11 col-sm-10 col-9">

                <h5 class="user-name"><?= $info['name'] ?> <span class="date-cmt">(<?= Yii::$app->formatter->format($model->created_at, 'relativeTime'); ?>) </span></h5>
                <p>
                    <q><?= $rating['reviews'] ?></q>
                </p>
            </div>
        </div>
</div>
<?php
    }
}else{
    echo '<p>'.Yii::t('app','No review found').'.</p>';
}
?>
