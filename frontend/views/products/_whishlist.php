<?php
use yii\helpers\Html;

$fav_dish = $model->favDish;
$class = 'far fa-heart';
$fav_val = 1;
if($fav_dish==1){
    $class = 'fas fa-heart';
    $fav_val = 0;
}
$final_price = $model->finalPrice;
?>
<div class="col-md-3 col-6 product-grids">
    <div class="agile-products">
        <div class="item">
            <span class=" wishlisticon"><?= Html::a(Html::tag('i','',['class' => $class,'style'=>'color:#021540']),'javascript:void(0)',['onClick' => 'return addToFav('.$model->id.','.$fav_val.')']) ?></span>
            <?= Html::a(Html::img($model->mainImage,['title'=>$model->dish_name,'alt'=>$model->dish_name,'class' => 'product-grid']),$model->proUrl) ?>
            <div class="product-content">
                <div class="title">
                     <p class="producttitle"><a href="<?= $model->proUrl ?>"><?= $model->dish_name ?></a></p>
                </div>
                <div class="price">
					<span class="price-tag saleofftag">
                    <?php if($final_price['discount'] > 0){ ?>
                        <b> <del><?= $final_price['product_price_label'] ?></del> (<?= $final_price['discount'] ?>% OFF)</b>
                    <?php } ?>
					</span><br/>
                    <span class="price-tag"><b><?= $final_price['real_price_label'] ?></b></span>
                    <span class="rating-rate"><i class="fas fa-star"></i> &nbsp;(<?= $model->avgReviews ?>)</span>
                </div>
            </div>
        </div>
    </div>
</div>