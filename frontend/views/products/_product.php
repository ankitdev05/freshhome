<?php

use yii\helpers\Html;

$dish_info = $model->dishInfo;

?>
<div class="col-sm-4 mb-3">
    <div class="card">
        <div class="card-body">  
            <h5 class="card-title"><?= $model->dish_name ?></h5>
            <p><?= Html::img($model->mainImage,['title'=>$model->dish_name,'alt'=>$model->dish_name,'class' => 'img-responsive']) ?></p>
            <div class="row">
                <p class="col-sm-7"><p class="mb-1 line-through">AED 20.00</p><?= $dish_info['currency'].' '.$dish_info['dish_price'] ?></p>
                <p class="col-sm-5 text-right"><?= Html::tag('i','',['class' => 'fas fa-star text-warning']) ?> <?= $dish_info['rate_point'] ?></p>
                <?php if (!Yii::$app->user->isGuest){
                        $fav_dish = $model->favDish;
                        $class = 'far fa-heart';
                        $fav_val = 1;
                        if($fav_dish==1){
                            $class = 'fas fa-heart';
                            $fav_val = 0;
                        }

                ?>
                <div class="col-sm-12">
                    <?= Html::a(Html::tag('i','',['class' => $class]),'javascript:void(0)',['onClick' => 'return addToFav('.$model->id.','.$fav_val.')']) ?>
                </div>
                <?php } ?>
            </div>
            <?= Html::a('View Product',$model->proUrl,['title'=>$model->dish_name,'alt'=>$model->dish_name,'data-pjax'=>0,'class'=>'btn btn-primary']) ?>
        </div>
    </div>
</div>