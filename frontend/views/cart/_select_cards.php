<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use common\models\CreditCards;
$model = new CreditCards();

$results = $model->find()->where(['user_id'=>$user_info->id,'type'=>Yii::$app->params['btree_environment']])->asArray()->all();
$form = ActiveForm::begin();
if(!empty($results)){
    foreach ($results as $result){
        $selected = '';
        if($result['default_card']==1)
            $selected = 'checked';
    ?>
<div class="innerlist-blcok">
	<div class="custom-control custom-radio custom-control-inline">
		<input <?= $selected ?> type="radio" class="custom-control-input" id="customCard<?= $result['id'] ?>" name="default_card" value="<?= $result['id']  ?>" value="customEx">
		<label class="custom-control-label" for="customCard<?= $result['id'] ?>">
		<img src="<?= $result['image_url'] ?>" class="img-fluid" width="30" />
		<?= $result['masked_num'] ?>
		</label>
	</div>
</div>
<?php
    }
}
?>
<div class="form-group" >
    <?= Html::submitButton('Use this Card',['class' => 'btn btn-sm custom-btn mt-3','style' => 'background-color:#2B6707;']) ?>
</div>
<?php  ActiveForm::end(); ?>
