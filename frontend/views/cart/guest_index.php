<?php
use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use common\models\UserAddress;
use common\models\CreditCards;
use yii\bootstrap4\Tabs;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use borales\extensions\phoneInput\PhoneInput;
use yii\web\JsExpression;
$credit_card = new CreditCards();
$credit_card->scenario = 'step_1';

$session = Yii::$app->session;

$this->title = 'Cart | '.config_name;
$default_address = '';
if(!empty($user_info)){
    $addresses = $user_info->addresses;
    $user_addr = UserAddress::find()->where(['user_id' => $user_info->id,'is_default' => 'yes'])->asArray()->one();
    if(!empty($user_addr)){
        $default_address .= $user_addr['title'];
        $default_address .= '<br/>'.$user_addr['location'];
    }
}
$type= $_GET['type'] ?? '';
if($type=='card')
    $model->payment_code = $type;
$cart_type = $_GET['cart_type'] ?? 'online';
$model->order_type = $cart_type;

$on_html = '';
$pre_html = '';
if($pre_order_data > 0)
    $pre_html = '<span id="order_ava"></span>';
if($online_order_data > 0)
    $on_html = '<span id="order_ava"></span>';
if($session->has('phone_number'))
    $model->phone_number = $session->get('phone_number');
if($session->has('notes'))
    $model->notes = $session->get('notes');
if($session->has('date'))
    $model->date = $session->get('date');

if($cart_type=='pre-order')
    $model->payment_options_values = ['card' => 'Pay via card'];;
?>
    <input type="hidden" value="<?= $type ?>" id="pcode" />
    <div class=" common-color">
        <section class="address-list">
            <div class="container">
                <div class="common-dashbaord">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php
                            if($empty_cart==0){
                                $cart_total = end($data['cart_total']);
                                ?>
                                <div class="my-cart">
                                    <p>My Cart(<?= $data['count'] ?> <?= $data['count'] >0 ? 'items' : 'item' ?>) <span class="cart-price"><?= $cart_total['currency'] ?> <?= $cart_total['total'] ?></span></p>
                                </div>
                            <?php } ?>
                            <div class="cartproduct">
                                <div class="my-cart-body">
                                    <?=  Tabs::widget([
                                        'encodeLabels' => false,
                                        'items' => [
                                            [
                                                'label' =>yii::t('app','Online Orders').$on_html,
                                                'url' =>Url::to(['/cart'],true),
                                                'active' => ($cart_type == '' || $cart_type == 'online') ? true : false
                                            ],
                                            [
                                                'label' =>yii::t('app','Pre Orders').$pre_html,
                                                'url' =>Url::to(['/cart','cart_type' => 'pre-order'],true),
                                                'active' => $cart_type == 'pre-order' ? true : false
                                            ]
                                        ]
                                    ]);
                                    echo '<br/>';
                                    if($empty_cart==0){
                                        foreach($data['supplier_info']['cart_data'] as $product){?>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                    <div class="cart-img">
                                                        <img src="<?= $product['main_image'] ?>" alt="<?= $product['dish_name'] ?>" class="img-fluid" />
                                                    </div>
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-xs-10">
                                                    <div class="cart-img-body">
                                                <span class="cart-title">
                                                    <p class="title-cart">
                                                        <?= $product['dish_name'] ?>
                                                        <span class="seller-by">By: <?= $data['supplier_info']['name'] ?></span>
                                                    </p>
                                                    <?php if(isset($product['options']) && !empty($product['options'])){
                                                        foreach ($product['options'] as $options){
                                                            $productOptions[$options['name']][] = $options['value'];
                                                        }
                                                        echo '<div class="row w-100">';
                                                        foreach ($productOptions as $k=>$productOption){
                                                            echo '<p class="col-sm-12 mb-0"><b>'.$k.':</b> '.implode(',',$productOption).'</p>';
                                                        }
                                                        echo '</div>';
                                                    } ?>

                                                    <p class="price-crt"><?= $product['currency'] ?> <?= $product['total_price'] ?></p>
                                                    <div class="ul-cart-size">
                                                        <ul>
                                                            <li>
                                                                <div class="QTY">
                                                                    <label>Qty :</label>
                                                                    <input type="hidden" value="<?= $product['quantity'] ?>" id="qty_<?= $product['menu_id'] ?>" />
                                                                    <ul>
                                                                        <li>
                                                                            <a href="javascript:void(0)" onClick="return subQty(<?= $product['menu_id'] ?>)"><i class="fas fa-minus"></i></a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:void(0)" class="qno"><?= $product['quantity'] ?></a>
                                                                        </li>
                                                                        <li>
                                                                          <a href="javascript:void(0)" onClick="return addQty(<?= $product['menu_id'] ?>)"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                                                       </li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <div class="cart-close">
                                                            <span><a onClick="return guestAddToCart(<?= $product['menu_id'] ?>,0,0,1)" href="javascript:void(0)"><i class="fa fa-times-circle" aria-hidden="true"></i></a></span>
                                                        </div>
                                                    </div>
                                                </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }else{ ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?= Yii::t('app','Your cart is empty.') ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="add-morecart">
                                <a href="javascript:void(0)" onclick="return $('#header_login_btn_id').click()" class="btn custom-btn">Checkout</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
<?php
$rurl = Url::to(['/cart','type'=> 'card',$type]);
if($empty_cart==0){
    $this->registerJs("
        $('#select-address').on('click',function(){
            selectAddress();
        })
        $('#add-new-address').on('click',function(){
            addNewAddress();
        })
    ");
    ?>
    <div class="modal fade pr-2" id="addCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Enter card info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= $this->render('//profile/card/_new_card',['model'=>$credit_card,'rurl' => $rurl]) ?>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
    <style>
        .modal { overflow: auto !important; }
        .cart-close span i{font-size:22px;}
    </style>
<?php
$this->registerCss('
.QTY ul li a i{font-weight:inherit}
.cart-img img{
    width:auto !important;height:auto !important;
}
');

$this->registerJs('
    function addQty(id){
        var qty = parseInt($("#qty_"+id).val())+parseInt(1);
        guestUpdateCart(id,qty)
    }
    function subQty(id){
        var qty = parseInt($("#qty_"+id).val())-parseInt(1);
        guestUpdateCart(id,qty);
    }

', yii\web\View::POS_HEAD);
?>