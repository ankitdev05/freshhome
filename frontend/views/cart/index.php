<?php
use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use common\models\UserAddress;
use common\models\CreditCards;
use yii\bootstrap4\Tabs;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use borales\extensions\phoneInput\PhoneInput;
use yii\web\JsExpression;
$credit_card = new CreditCards();
$credit_card->scenario = 'step_1';

$session = Yii::$app->session;

$this->title = 'Cart | '.config_name;
$default_address = '';
if(!empty($user_info)){
    $addresses = $user_info->addresses;
    $user_addr = UserAddress::find()->where(['user_id' => $user_info->id,'is_default' => 'yes'])->asArray()->one();
    if(!empty($user_addr)){
        $default_address .= $user_addr['title'];
        $default_address .= '<br/>'.$user_addr['location'];
    }
}
$type= $_GET['type'] ?? '';
if($type=='card')
    $model->payment_code = $type;
$cart_type = $_GET['cart_type'] ?? 'online';
$model->order_type = $cart_type;
$cart_total = isset($data['cart_total']) ? end($data['cart_total']) : [];

$on_html = '';
$pre_html = '';
if($pre_order_data > 0)
    $pre_html = '<span id="order_ava"></span>';
if($online_order_data > 0)
    $on_html = '<span id="order_ava"></span>';
if($session->has('phone_number'))
    $model->phone_number = $session->get('phone_number');
if($session->has('notes'))
    $model->notes = $session->get('notes');
if($session->has('date'))
    $model->date = $session->get('date');

if($cart_type=='pre-order')
$model->payment_options_values = ['card' => 'Pay via card'];

if(empty($cart_total['total_value'])){
    $model->payment_code = 'wallet';
    $model->payment_options_values = ['wallet' => 'Wallet'];
    $this->registerJs('
        $(".field-order-payment_code").hide();
    ');
}
else unset($model->payment_options_values['wallet']);

?>
<input type="hidden" value="<?= $type ?>" id="pcode" />
<div class=" common-color">
    <section class="address-list">
        <div class="container">
            <div class="common-dashbaord">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <?php
                            if($empty_cart==0){
                                $cart_total = end($data['cart_total']);
                        ?>
                        <div class="my-cart">
                            <p><?= yii::t('app','My Cart') ?>(<?= $data['count'] ?> <?= $data['count'] >0 ? 'items' : 'item' ?>) <span class="cart-price"><?= $cart_total['currency'] ?> <?= $cart_total['total'] ?></span></p>
                        </div>
                        <?php } ?>
                        <div class="cartproduct">
                            <div class="my-cart-body">
                                <?=  Tabs::widget([
                                    'encodeLabels' => false,
                                    'items' => [
                                        [
                                            'label' =>yii::t('app','Online Orders').$on_html,
                                            'url' =>Url::to(['/cart'],true),
                                            'active' => ($cart_type == '' || $cart_type == 'online') ? true : false
                                        ],
                                        [
                                            'label' =>yii::t('app','Pre Orders').$pre_html,
                                            'url' =>Url::to(['/cart','cart_type' => 'pre-order'],true),
                                            'active' => $cart_type == 'pre-order' ? true : false
                                        ]
                                    ]
                                ]);
                                echo '<br/>';
                                    if($empty_cart==0){
                                        foreach($data['supplier_info']['cart_data'] as $product){?>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <div class="cart-img">
                                                <img src="<?= $product['main_image'] ?>" alt="<?= $product['dish_name'] ?>" class="img-fluid" />
                                            </div>
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-xs-10">
                                            <div class="cart-img-body">
                                                <div class="cart-title">
													<div class="form-row">
														<div class="col-md-6">
															<p class="title-cart">
																<?= $product['dish_name'] ?>
																<span class="seller-by">By: <?= $data['supplier_info']['name'] ?></span>
															</p>
															<?php if(isset($product['options']) && !empty($product['options'])){
																foreach ($product['options'] as $options){
																	$productOptions[$options['name']][] = $options['value'];
																}
																echo '<div class="row w-100">';
																foreach ($productOptions as $k=>$productOption){
																		echo '<p class="col-sm-12 mb-0"><b>'.$k.':</b> '.implode(',',$productOption).'</p>';
																	}
																echo '</div>';
															} ?>
														</div>
														<div class="col-md-3">
															<p class="price-crt"><?= $product['currency'] ?> <?= $product['total_price'] ?></p>
														</div>
														<div class="col-md-3">
															<div class="ul-cart-size">
																<ul>
																	<li>
																		<div class="QTY">
																			<label class="d-none">Qty :</label>
																			<input type="hidden" value="<?= $product['quantity'] ?>" id="qty_<?= $product['menu_id'] ?>" />
																			<ul>
																				<li>
																					<a href="javascript:void(0)" onClick="return subQty(<?= $product['menu_id'] ?>)"><i class="fas fa-minus"></i></a>
																				</li>
																				<li>
																					<a href="javascript:void(0)" class="qno"><?= $product['quantity'] ?></a>
																				</li>
																				<li>
																				  <a href="javascript:void(0)" onClick="return addQty(<?= $product['menu_id'] ?>)"><i class="fa fa-plus" aria-hidden="true"></i></a>
																			   </li>
																			</ul>
																		</div>
																	</li>
																</ul>
																<div class="cart-close mt-1">
																	<span><a onClick="return addToCart(<?= $product['menu_id'] ?>,0,0,1)" href="javascript:void(0)"><i class="fa fa-times-circle" aria-hidden="true"></i></a></span>
																</div>
															</div>
														</div>
													</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        }
                                    }
                                if($empty_cart==1){ ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5><?= Yii::t('app','Your cart is empty.') ?></h5>
                                        </div>
                                    </div>
                                <?php }
                                    if(isset($data['supplier_info']['inactive_orders']) && !empty($data['supplier_info']['inactive_orders'])){
                                        echo '<hr/><h4>Unavailable Products</h4>';
                                        foreach($data['supplier_info']['inactive_orders'] as $product){
                                    ?>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                    <div class="cart-img">
                                                        <img src="<?= $product['main_image'] ?>" alt="<?= $product['dish_name'] ?>" class="img-fluid" />
                                                    </div>
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-xs-10">
                                                    <div class="cart-img-body">
                                                        <div class="cart-title">
                                                            <div class="form-row">
                                                                <div class="col-md-6">
                                                                    <p class="title-cart">
                                                                        <?= $product['dish_name'] ?>
                                                                        <span class="seller-by">By: <?= $data['supplier_info']['name'] ?></span><br/>
                                                                        <?= $product['dist_qty'] ?> quantity left

                                                                    </p>
                                                                    <?php if(isset($product['options']) && !empty($product['options'])){
                                                                        foreach ($product['options'] as $options){
                                                                            $productOptions[$options['name']][] = $options['value'];
                                                                        }
                                                                        echo '<div class="row w-100">';
                                                                        foreach ($productOptions as $k=>$productOption){
                                                                            echo '<p class="col-sm-12 mb-0"><b>'.$k.':</b> '.implode(',',$productOption).'</p>';
                                                                        }
                                                                        echo '</div>';
                                                                    } ?>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <p class="price-crt"><?= $product['currency'] ?> <?= $product['total_price'] ?></p>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="ul-cart-size">
                                                                        <ul>
                                                                            <li>
                                                                                <div class="QTY">
                                                                                    <label class="d-none">Qty :</label>
                                                                                    <input type="hidden" value="<?= $product['quantity'] ?>" id="qty_<?= $product['menu_id'] ?>" />
                                                                                    <ul>
                                                                                        <li>
                                                                                            <a href="javascript:void(0)">&nbsp;</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="javascript:void(0)" class="qno"><?= $product['quantity'] ?></a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="javascript:void(0)">&nbsp;</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="cart-close mt-1">
                                                                            <span><a onClick="return addToCart(<?= $product['menu_id'] ?>,0,0,1)" href="javascript:void(0)"><i class="fa fa-times-circle" aria-hidden="true"></i></a></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php
                                        }
                                    }
                                     ?>
                            </div>
                        </div>
                        <div class="add-morecart">
                            <a href="<?= Yii::$app->request->baseUrl ?>/wishlist"><?= yii::t('app','Add more from wishlist') ?></a>
                        </div>
                    </div>
                    <?php if($empty_cart==0){ ?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <?php $form = ActiveForm::begin(['id' => 'add-to-cart-form']); ?>
                        <div class="my-cart">
                            <p><?= yii::t('app','Delivery Address') ?>
                            <?php if(!empty($addresses)){ ?>
                                <span class="cart-price"><a href="javascript:void(0)" onClick="selectAddress()" style="color: #DF8317;">Change</a></span>
                            <?php } ?>
                            </p>
                        </div>
                        <div class="profile-side cart-right">
                            <div class="right-cart">
                                <div class="address">
                                    <?php if(empty($addresses)){ ?>
                                        <p><?= yii::t('app','No address found') ?>.</p>
                                        <?= Html::button(yii::t('app','Add new Address'),['class'=>'btn btn custom-btn','id' =>'add-new-address']) ?>
                                        <hr/>
                                    <?php }else{
                                            if(!empty($default_address)){
                                                echo $default_address;
                                                $model->address_id = $user_addr['address_id'];
                                               echo $form->field($model, 'address_id')->hiddenInput()->label(false);
                                            }
                                            else{
                                               echo Html::button(yii::t('app','Select Address'),['class'=>'btn btn custom-btn','id' =>'select-address']);
                                            }
                                        ?>

                                    <?php } ?>
                                    <p class="mt-2">
                                        Phone: <?= $user_info->phone_number ?>
                                    </p>
                                </div>
                            </div>
                        <div class="add-info-cart">
                            <p><?= yii::t('app','Additional Requirements') ?></p>
                        </div>
                        <div class="right-cart">
                            <?php if($model->order_type=='pre-order'){ ?>
                                <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                                    'options' => ['name' => 'Order[collection_date]','placeholder' => 'Select Delivery date ...','readonly'=>true],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'd/m/yyyy',
                                        'startDate'=>'+0d',
                                    ]
                                ]); ?>
                                <?= $form->field($model, 'time')->widget(TimePicker::classname(), [
                                    'options' => ['name' => 'Order[collection_time]'],
                                ]); ?>
                            <?php } ?>
                            <div class="address">
                                <?= $form->field($model, 'payment_code')->radioList($model->payment_options_values) ?>
                            </div>
                            <div class="address" id="user_cards" style="display: none;">
                                <?= $this->render('_cards',['user_info' => $user_info,'model' => $model,'form' => $form]) ?>
                            </div>
                            <div class="address">
                                <?= $form->field($model, 'notes')->textArea(['rows' => 3])->label('Your Message') ?>
                                <?= $form->field($model, 'phone_number')->widget(PhoneInput::className(),[

                                    'jsOptions'=>['nationalMode'=>false,'autoHideDialCode'=>false,'initialCountry'=>'auto','geoIpLookup'=>new JsExpression('function(callback) { 
				$.get(\'https://ipinfo.io\', function() {}, "jsonp").always(function(resp) {
					  var countryCode = (resp && resp.country) ? resp.country : "";
					  callback(countryCode);
					});
				}')]
                                ])->label('Additional Phone No') ?>
                            </div>
                        </div>
                        <div class="add-info-cart">
                            <p><?= yii::t('app','Price Details') ?></p>
                        </div>
                        <div class="right-cart">
                            <div class="address total-last">
                                <?php foreach($data['cart_total'] as $k=>$total){ ?>
                                    <p <?= ($k+1)==count($data['cart_total']) ? "class='total-price'" : "" ?>><?= $total['label'] ?> <span class="price-total"><?= $total['currency'] ?> <?= $total['total'] ?></span></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="place-order">
                            <?= $form->field($model, 'order_type')->hiddenInput()->label(false) ?>
                            <?= Html::submitButton('Place Order',['class'=>'btn btn custom-btn','id' =>'add-to-cart']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    </div>

                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
$rurl = Url::to(['/cart','type'=> 'card',$type]);
if($empty_cart==0){
    $this->registerJs("
        $('#select-address').on('click',function(){
            selectAddress();
        })
        $('#add-new-address').on('click',function(){
            addNewAddress();
        })
    ");
    ?>
    <div class="modal fade pr-2" id="addCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Enter card info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= $this->render('//profile/card/_new_card',['model'=>$credit_card,'rurl' => $rurl]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade pr-2" id="selectCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Select card</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= $this->render('_select_cards',['user_info' => $user_info]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade pr-2" id="selectAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body address-modal">
                    <h5>Select Delivery Address</h5>
                    <div class="profile-body">
                        <div class="address-list-block" id="addressList"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade pr-2" id="addAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Address</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="txtBody"></div>
                <div class="modal-footer">
                    <button type="button" class="btn custom-btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<style>
    .modal { overflow: auto !important; }
    .cart-close span i{font-size:22px;}
</style>
<?php
$this->registerCss('
.QTY ul li a i{font-weight:inherit}
.cart-img img{
    width:auto !important;height:auto !important;
}
');
$this->registerJs('
$("#order-notes,#order-phone_number,#order-date").on("keyup",function(){
    var notes = $("#order-notes").val();
    var phone_number = $("#order-phone_number").val();
    var date = $("#order-date").val();
    $.ajax({
        url : website_url+"site/cart-info",
        type : "POST",
        data : "notes="+notes+"&phone_number="+phone_number+"&date="+date,
    })
})

$("#order-date").on("change",function(){
    var notes = $("#order-notes").val();
    var phone_number = $("#order-phone_number").val();
    var date = $("#order-date").val();
    $.ajax({
        url : website_url+"site/cart-info",
        type : "POST",
        data : "notes="+notes+"&phone_number="+phone_number+"&date="+date,
    })
})
     $("#addCard").appendTo("body");
     $("#order-payment_code input[type=\'radio\']").click(function(){
     $("#user_cards").hide();
        var value = $(this).val();
        if(value=="card")
        $("#user_cards").show();
     })
     var value = $("#pcode").val();

     if(value=="card")
        $("#user_cards").show();
');
$this->registerJs('
    function addQty(id){
        var qty = parseInt($("#qty_"+id).val())+parseInt(1);
        updateCart(id,qty)
    }
    function subQty(id){
        var qty = parseInt($("#qty_"+id).val())-parseInt(1);
        updateCart(id,qty);
    }
    function selectAddress(){
        $.ajax({
            url : website_url+"cart/select-address",
            type : "POST",
            cache : false,
            async :true,
            dataType : "json",
            success : function(json){
                if(json[\'html\']){
                    jQuery(\'#addressList\').html(json[\'html\']);
                    jQuery(\'#selectAddress\').modal(\'show\');
                    $(\'#selectAddress\').modal(\'handleUpdate\')
                }
            }
        })
    }
    function addNewAddress(){
    jQuery(\'.modal\').modal(\'hide\');
        $.ajax({
            url: website_url+\'cart/new-address\',
            type: \'POST\',
            cache : false,
            async :true,
            dataType : \'json\',
            success : function(json){
                if(json[\'html\']){
                    jQuery(\'#txtBody\').html(json[\'html\']);
                    jQuery(\'#addAddress\').modal(\'show\');
                    $(\'#addAddress\').modal(\'handleUpdate\')
                }
            }
        });
    }
', yii\web\View::POS_HEAD);
?>