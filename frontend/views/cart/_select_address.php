<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
$form = ActiveForm::begin();
    if(!empty($results)){
        foreach($results as $result){
            $selected = '';
            if($result['is_default']=='yes')
                $selected = 'checked';
            ?>
            <div class="innerlist-blcok">
                <p>
                    <span class="edit-list">
                        <div class="custom-control custom-radio custom-control-inline" style="float: right;">
                             <input <?= $selected ?> type="radio" class="custom-control-input" id="customRadio<?= $result['address_id'] ?>" name="user_addr" value="<?= $result['address_id']  ?>" value="customEx">
                             <label class="custom-control-label" for="customRadio<?= $result['address_id'] ?>"></label>
                           </div>
                    </span>
                    <?= $result['title'] ?><br/>
                    <?= $result['location'] ?>
                </p>
            </div>
<?php }
    }

?>
<div class="form-group" >
    <button onClick="return addNewAddress()" type="button" class="btn btn custom-btn " style="margin-top:20px; float: left;">Add New Address</button>

    <?= Html::submitButton('Use this Address',['class' => 'btn btn custom-btn','style' => 'margin-top:20px; background-color:#2B6707;']) ?>
</div>
<?php  ActiveForm::end(); ?>