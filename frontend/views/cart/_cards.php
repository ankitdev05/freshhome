<?php
use yii\helpers\Html;
use common\models\CreditCards;
$cc_model = new CreditCards();

$count = $cc_model->find()->where(['user_id'=>$user_info->id,'type'=>Yii::$app->params['btree_environment']])->count();
$default_card = $cc_model->find()->where(['user_id'=>$user_info->id,'type'=>Yii::$app->params['btree_environment'],'default_card' => 1])->asArray()->one();
if(!empty($default_card))
    $model->card_id = $default_card['id'];
if($count>0){
    echo '<hr/>';
    if(!empty($default_card))
        echo '<div class="custom-control custom-radio custom-control-inline pt-2"><input type="radio" id="def_result" class="custom-control-input" checked /><label class="custom-control-label" for="def_result"></div><img src="'.$default_card['image_url'].'" title="'.$default_card['card_type'].'" alt="'.$default_card['card_type'].'" /> '.$default_card['masked_num'] .' ('.$default_card['exp_date'].')';
    echo $form->field($model, 'card_id')->hiddenInput()->label(false);
    echo Html::a('Select Card','javascript:void(0)',['class' => 'btn btn-sm btn-dark','data-toggle' => 'modal','data-target' => '#selectCard']);
    echo ' OR ';

}
echo Html::a('New Card','javascript:void(0)',['class' => 'btn btn-sm btn-dark','data-toggle' => 'modal','data-target' => '#addCard']);

echo '<hr/>';
?>
