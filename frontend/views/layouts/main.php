<?php

use yii\helpers\Url;
use yii\bootstrap4\Html;
use frontend\assets\AppAsset;
use common\models\Pages;
use common\models\Subscriptions;

AppAsset::register($this);
rmrevin\yii\fontawesome\AssetBundle::register($this);

$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);
$footer_pages = Pages::find()->select('*')->joinWith('pageDescription as pd')->where(['status' => 1, 'pd.language_id' => language_id,'show_on_footer' => 1])->orderBy('page_order asc')->all();
$header_pages = Pages::find()->select('*')->joinWith('pageDescription as pd')->where(['status' => 1, 'pd.language_id' => language_id,'show_on_header' => 1])->orderBy('page_order asc')->all();

$this->beginPage();
$subs_model = new Subscriptions();

$controller =  $this->context->id;
$action     = $this->context->action->id;
$is_homepage = false;
if($controller=='site' && $action=='index') 
    $is_homepage = true;
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= $website_url ?>images/logo.ico" />
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php
    if(language_id==2){
        $this->registerCssFile(Yii::$app->request->baseUrl.'/css/arabic.css',[
            'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
        ]);
        $this->registerCssFile(Yii::$app->request->baseUrl.'/css/arabic-responsive.css',[
            'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
        ]);
		$this->registerCssFile(Yii::$app->request->baseUrl.'/css/stylesheet/arabic.css',[
            'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
        ]);
		$this->registerCssFile(Yii::$app->request->baseUrl.'/css/stylesheet/arabic-responsive.css',[
            'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
        ]);
    }

    $this->registerCssFile(Yii::$app->request->baseUrl.'/css/responsive-style.css',[
        'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
    ]);

    $this->registerCssFile('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',[
        'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
    ]);

    $this->registerCssFile(Yii::$app->request->baseUrl.'/css/jquery-confirm.min.css',[
        'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
    ]);

	$this->registerCssFile(Yii::$app->request->baseUrl.'/css/stylesheet/owl.carousel.min.css',[
        'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
    ]);
    $this->registerCssFile(Yii::$app->request->baseUrl.'/css/stylesheet/owl.theme.default.min.css',[
        'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
    ]);

	$this->registerCssFile(Yii::$app->request->baseUrl.'/css/jquery.mCustomScrollbar.min.css',[
        'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
    ]);
	$this->registerCssFile(Yii::$app->request->baseUrl.'/css/stylesheet/style.css',[
        'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
    ]);
	$this->registerCssFile(Yii::$app->request->baseUrl.'/css/stylesheet/responsive.css',[
        'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
    ]);
	
	$this->registerJsFile(Yii::$app->request->baseUrl.'/js/javascript/owl.carousel.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
	
    $this->registerJsFile(Yii::$app->request->baseUrl.'/js/slick.js',['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::$app->request->baseUrl.'/js/jquery-confirm.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); 
    $this->registerJsFile(Yii::$app->request->baseUrl.'/js/jquery.mCustomScrollbar.concat.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);

    $this->registerJsFile(Yii::$app->request->baseUrl.'/js/site.js',['depends' => [\yii\web\JqueryAsset::className()]]);
    if (!Yii::$app->user->isGuest){
        $this->registerJsFile(Yii::$app->request->baseUrl.'/js/user.js',['depends' => [\yii\web\JqueryAsset::className()]]);
    }

    ?>
    <script>var website_url = "<?= $website_url ?>"</script>
</head>
<body>
<?php $this->beginBody() ?>
<?= $this->render('//site/home_header',['is_homepage'=>$is_homepage,'header_pages' => $header_pages]) ?>

<?php if(Yii::$app->user->isGuest) echo $this->render('//user/login')  ?>
<div class="page-wrap mt-0">
    <?= \yii2mod\notify\BootstrapNotify::widget() ?>
    <div class="contact-side">
        <div class="icon-bar">
            <?= Html::a(Yii::t('app', 'Contact Us'),['/contact-us']) ?>
        </div>
    </div>
    <?= $content ?>
</div>


<footer class="footer-section mt-5">
	<div class="container pt-5 pb-2">
		<div class="row">
			<div class="col-md-3">
				<h5 class="text-white"><?= Yii::t('app', 'About Us') ?></h5>
				<p class="text-white">We light opportunities by connecting the world in feasibility, accountability and trust.</p>
			</div>
			<div class="col-md-3">
				<h5 class="text-white"><?= Yii::t('app', 'Customer Service') ?></h5>
				<ul>
                    <li>
                        <?= Html::a(Yii::t('app', 'My Account'),$website_url.'profile') ?>
                    </li>
                    <li>
                        <?= Html::a(Yii::t('app', 'Login'),$website_url.'login') ?>
                    </li>
                    <li>
                        <?= Html::a(Yii::t('app', 'My Cart'),$website_url.'cart') ?>
                    </li>
                    <li>
                        <?= Html::a(Yii::t('app', 'Wishlist'),$website_url.'profile/whishlist') ?>
                    </li>
                    <li>
                        <?= Html::a(Yii::t('app', 'Checkout'),$website_url.'cart/checkout') ?>
                    </li>
				</ul>
			</div>
			<div class="col-md-3">
				<h5 class="text-white"><?= Yii::t('app','Corporation') ?></h5>
				<ul>
                    <?php if(!empty($footer_pages)){
                        foreach($footer_pages as $footer_page){
                            ?><li><?= Html::a($footer_page->title,$footer_page->pageUrl) ?></li>
                            <?php
                        }
                    } ?>
				</ul>
			</div>
			<div class="col-md-3">
				<h5 class="text-white"><?= Yii::t('app','Contact Us') ?></h5>
				<p class="text-white"><?= config_email ?></p>
			</div>
			<div class="col-md-12 mt-3">
				<p class="copyright text-center text-white"><?= str_replace('[date]',date('Y'),config_footer) ?></p>
			</div>
		</div>
	</div>
</footer>
<?php
$this->registerJs('
$(".allow-dropdown").addClass("w-100");
');
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
