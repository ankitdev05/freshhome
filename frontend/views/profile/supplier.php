<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\City;
use common\models\Country;
use common\models\MainCategories;
use himiklab\thumbnail\EasyThumbnailImage;

$this->title = 'Supplier | '.config_name;
$this->params['breadcrumbs'][] = 'Supplier';
$imageSrc = [];
$del_image = [['url'=>yii::$app->request->baseUrl.'/profile/del-supplier-pic']];
$all_nationality = ArrayHelper::map(Country::find()->asArray()->all(),'code','nationality');
$language_id = 1;
$all_cities = ArrayHelper::map(City::find()->joinWith('cityName as cn')->where(['cn.language_id'=>$language_id])->asArray()->all(), 'id', 'cityName.city_name');
if( !empty($model->profile_pic)){
    $image=EasyThumbnailImage::thumbnailFileUrl(
        "../../frontend/web/uploads/".$model->profile_pic,200,200,EasyThumbnailImage::THUMBNAIL_INSET
    );
    $imageSrc	= [Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Profile Picture', 'title'=>'Profile Picture'])];
}

$all_cats = ArrayHelper::map(MainCategories::find()->asArray()->all(),'id','name');
?>
<div class="supplier">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' => 'supplier-info']); ?>
    <div class="row">
        <?php if(empty($model->supplier_type)){ ?>
        <div class="col-sm-12">
            <?= $form->field($model, 'supplier_type')->inline()->radioList($all_cats)->label(false) ?>

        </div>
        <?php } ?>
        <div class="col-sm-12">
            <?=
            $form->field($model, 'profile_pic')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' =>[
                    'initialPreview'=>$imageSrc,
                    'initialPreviewConfig'=>$del_image
                ]
            ])->label(false);
            ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'dob')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter birth date ...','readonly'=>true],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'nationality')->widget(Select2::classname(), [
                'data' =>$all_nationality,
                'options' => ['placeholder' => 'Please select ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
        <div class="w-100"><hr/></div>
        <div class="col-sm-12">
            <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'building_no')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'flat_no')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'floor_no')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'landmark')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'city')->widget(Select2::classname(), [
                'data' =>$all_cities,
                'options' => ['placeholder' => 'Please select ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'availability')->widget(Select2::classname(), [
                'data' =>['online'=>'Online','offline'=>'Offline'],
                'options' => ['placeholder' => 'Please select ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
        <div class="form-group col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>
</div>
