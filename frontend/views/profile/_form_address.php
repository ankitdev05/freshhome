<?php
use yii\bootstrap4\Url;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use common\models\City;

$language_id = language_id;
$all_cities = ArrayHelper::map(City::find()->joinWith('cityName as cn')->where(['cn.language_id'=>$language_id])->all(), 'id', 'cityName.city_name');

?>
<div class="address-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-12 mb-2">
            <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
            <div class='map' id='map_canvas' style="height:300px;"></div>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'city')->widget(Select2::classname(), [
                'data' =>$all_cities,
                'options' => ['placeholder' => 'Please select ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
        <div class="w-100"></div>
        <div class="col-sm-4">
            <?= $form->field($model, 'building_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'flat_no')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'floor_no')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'landmark')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'latitude')->hiddenInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'longitude')->hiddenInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="form-group col-sm-12 mt-3">
            <?php if(!isset($isAjax)){ ?>
                <?= Html::a('Cancel',['/profile','type' => 'address'],['class' => 'can btn btncancel']) ?>
            <?php } ?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn custom-btn btnupdate']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php

echo $this->render('//site/_location',['model'=>$model]);