<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
?>

<div class="bank-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-12">
            <?= $form->field($model, 'email_address')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'account_number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'iban')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="w-100"></div>
        <div class="col-sm-6">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn custom-btn','name'=>'new-card']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>