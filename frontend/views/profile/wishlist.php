<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);
$this->title = 'My wishlist | '.config_name;
?>
<section class="product p-detail supplier-sec new-wishlist">
    <div class="container">
        <div class="form-row wish-list">
            <div class="col-md-9">
				<h4 class="mywish-list"><?= Yii::t('app','My Wishlist') ?><span class="wishlist-no">(<?= $dataProvider->getTotalCount() ?> Items)</span></h4>
			</div>
			<div class="col-md-3">
				<div class="form-row mt-2 pt-1">
					<div class="col-md-6 col-6">
						<a href="<?= yii::$app->request->baseUrl.'/fav-supplier' ?>" class="btn btnfavosupplier w-100 text-white"><?= Yii::t('app','Favo Suppliers') ?></a>
					</div>
					<div class="col-md-6 col-6">
						<a href="<?= yii::$app->request->baseUrl.'/wishlist' ?>" class="btn btnfavosupplier w-100 text-white"><?= Yii::t('app','Favo Dishes') ?></a>
					</div>
				</div>
			</div>
            <?=  ListView::widget([
                    'summary' => false,
                    'dataProvider' => $dataProvider,
                    'itemView' => '//products/_whishlist',
                    'options' => [
                        'tag' => false,
                        'class' => 'row',
                        'id' => 'list-wrapper',
                    ],
                    'itemOptions' => [
                        'tag' => false
                    ],
                    'pager' => [
                        'pageCssClass' => 'page-item',
                        'disabledPageCssClass' => 'page-link disabled',
                        'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
                        'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
                        'linkOptions' => ['class' => 'page-link'],
                    ]
            ]); ?>

        </div>
    </div>
</section>