<?php
use yii\helpers\Url;

$session = Yii::$app->session;
$is_supplier = $session->has('is_supplier');
$this->title	= 'Invite your friends | '.config_title;
$type	= '';
if(isset($_GET['type']) && !empty($_GET['type']))
    $type	= $_GET['type'];
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/clipboard.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<section class="address-list">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <div class="profile-side">
                        <?php
                            if($is_supplier)
                                echo $this->render('//supplier/_left_panel',['type'=>$type]);
                            else
                                echo $this->render('//profile/_left_panel',['type'=>$type]);
                        ?>

                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <div class="profile-body">
                        <div class="address-list-block">
                            <h4 class="Invite-title">Invite your friends to enjoy wonderful<br>food and products</h4>
                            <div class="row">
								<div class="col-sm-7 mx-auto">
                                <div class="form-group">
                                    <input type="text" id="ctext" class="form-control txtfield"  value="<?= Url::to(['/user/'.$model->username,'u'=>$model->username],true) ?>">
                                    <p class="copylink text-center"><a href="javascript:void(0);" id="copylink" data-clipboard-action="copy" data-clipboard-target="#ctext"> Copy Link to clipboard</a> </p>
                                </div>
								</div>
    <script type='text/javascript' src="https://platform-api.sharethis.com/js/sharethis.js#property=5d1e4aaa55a8b200110ce012&product='inline-share-buttons'" async='async'>
    </script>
                                <div class="col-sm-12">
                                    <div data-url="<?= Url::to(['/','u'=>$model->username],true) ?>" class="sharethis-inline-share-buttons"></div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <span class="qr-dload">
                                    <img src="<?= $model->qrcode ?>" alt="QR code" title="QR code" width="250" />
                                </span>
                                <span class="Download-btn mt-3">
                                    <a class="btn btn custom-btn" href="<?= yii::$app->request->baseUrl ?>/profile?type=invite&download=true" target="_blank">Download</a>
								</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<?php
$this->registerJs("
var clipboard = new ClipboardJS('#copylink');
clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
");