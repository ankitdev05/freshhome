<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Change email | profile';
$breadcrumbs[] = ['href'=>false, 'label' => 'Change Email'];
$model->email = '';
$user_info = Yii::$app->user->identity;
?>
<div class="bg-sec">
    <div class="heading">
        <h3>Change Email</h3>
        <div class="divider"></div>
    </div>
</div>
<?= $this->render('//site/_breadcrumbs',['breadcrumbs'=>$breadcrumbs]) ?>
<section class="prfile-dasboard">
    <div class="container">
        <div class="row">
            <?= $this->render('profile_pic',['user_info' => $user_info]) ?>
            <div class="w-100"><hr/></div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <div class="profile-side">
                    <?= $this->render('//profile/_left_panel') ?>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                <div class="profile-body">
                    <div class="inner-profile">

                        <h4>Change Email</h4>
                        <div class="row mob-no">
                            <div class="col-md-12">
                                <div class="form-group " >
                                    <img alt="email" src="<?= yii::$app->request->baseUrl ?>/img/email100.jpg" />
                                </div>
                            </div>
                            <?php $form = ActiveForm::begin(); ?>
                            <div class="col-md-12">
                                <?= $form->field($model, 'old_email_address')->textInput() ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'email')->textInput()->label('New email address') ?>
                            </div>
                            <div class="form-group col-sm-12">
                                <?= Html::submitButton( Yii::t('app', 'Update'), ['class' => 'btn btn custom-btn']) ?>
                                <?= Html::resetButton('Cancel',['class' => 'btn btn custom-btn Cancel']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
