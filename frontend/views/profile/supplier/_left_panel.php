<?php
use yii\helpers\Url;
use yii\bootstrap4\Html;

$type	= '';
if(isset($_GET['type']) && !empty($_GET['type']))
    $type	= $_GET['type'];
$action     = $this->context->action->id;
if($action=='updatemenu' || $action=='addmenu')
    $type = 'listmenu';
if($type=='addProduct' || $type=='updateProduct')
    $type = 'myProducts';
$user_info = yii::$app->user->identity;
$supplier_type = $user_info->supplier_type;
?>
<ul>
    <li>
        <h5>Account</h5>
        <?= Html::tag('p',Html::a('Profile',Url::to(['/profile'],true),['class'=>$type=='' ? 'active' : ''])) ?>
        <?php if($supplier_type==1){ ?>
            <?= Html::tag('p',Html::a('My Menu',Url::to(['/profile','type' => 'listmenu'],true),['class'=>$type=='listmenu' ? 'active' : ''])) ?>
            <?= Html::tag('p',Html::a('My Schedule',Url::to(['/profile','type' => 'mySchedule'],true),['class'=>$type=='mySchedule' ? 'active' : ''])) ?>
            <?= Html::tag('p',Html::a('Invite Friends',Url::to(['/profile','type' => 'invite'],true),['class'=>$type=='invite' ? 'active' : ''])) ?>
        <?php }else{ ?>
            <?= Html::tag('p',Html::a('My Products',Url::to(['/profile','type' => 'myProducts'],true),['class'=>$type=='myProducts' ? 'active' : ''])) ?>
        <?php } ?>
        <?= Html::tag('p',Html::a('Sales Report',Url::to(['/profile','type' => 'salesreport'],true),['class'=>$type=='salesreport' ? 'active' : ''])) ?>
    </li>
</ul>
