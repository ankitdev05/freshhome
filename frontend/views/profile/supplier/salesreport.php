<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap4\ActiveForm;
use miloschuman\highcharts\Highcharts;

$this->title = 'Sales report | '.config_name;
$format = Yii::$app->formatter;


$xLabels =[];
$yLabels = [];
$total_data = [];

if(isset($response['results'])){
    $total_data = $response['total_data'];
    foreach($response['results'] as $label)   {
        $xLabels[] = $label['title'];
        $yLabels[] = round($label['total'] ?? 0,2);
    }
}


?>
<section class="address-list common-color">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <div class="profile-side">
                        <?= $this->render('_left_panel') ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <div class="profile-body">
                        <?php $form = ActiveForm::begin(['id' => 'filter-products','method'=>'GET','action' => Url::to(Yii::$app->request->baseUrl.'/profile?type=salesreport',true)]); ?>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Start Date','readonly' => true],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'endDate'=>'+0d',
                                        'format' => 'dd/mm/yyyy'
                                    ]
                                ])->label(false) ?>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <?= $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'End Date','readonly' => true],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'endDate'=>'+0d',
                                        'format' => 'dd/mm/yyyy'
                                    ]
                                ])->label(false) ?>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $form->field($model,'report_type',['options'=>['tag'=>false]])->dropDownList(['month' =>'Months','weeks' =>'Weeks','days' => 'Days'],['class' => 'custom-select','prompt' => 'Filter By'])->label(false) ?>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= Html::submitButton('Search',['class' => 'btn custom-btn']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                        <div class="address-list-block card-credit">
                        <?=
                             Highcharts::widget([
                                 'scripts' => [
                                    'modules/exporting',
                                    'themes/grid-light',
                                 ],
                                 'options' => [
                                     'credits'=>false,
                                     'title' => ['text' => 'My Sales report'],
                                     'xAxis' => [
                                             'categories' => $xLabels,
                                     ],
                                     'yAxis' => [
                                             'title' => [
                                                     'text' => 'Price'
                                             ]
                                     ],
                                     'series' => [
                                         [
                                             'type' => 'column',
                                             'name' => '',
                                             'colorByPoint' => true,
                                             'data' => $yLabels,
                                             'showInLegend' => false,
                                             'center' => [100, 80],
                                             'dataLabels' => [
                                                  'enabled' => false,
                                             ]
                                         ],
                                     ],
                                 ]
                             ]);
                        ?>
                            <div class="order-table sale-report m-t-25 ">
                                <div class="col-md-12 p-0">
                                   <?php if(!empty($total_data)){ ?>
                                    <p class="orderpending">Total Revenue<span class="get-invoice"><?= $total_data['default_currency'] ?> <?= $format->format($total_data['total_revenue'] ?? 0,['decimal',2]) ?></span></p>

                                    <p class="Deliver-info">My Plan Fee<span class="orderdate"><?= $total_data['default_currency'] ?> <?= $format->format($total_data['my_plan_fee'] ?? 0,['decimal',2]) ?></span></p>

                                    <p class="orderpending">My Kitchen Revenu<span class="get-invoice"><?= $total_data['default_currency'] ?> <?= $format->format($total_data['my_kitchen_revenue'] ?? 0,['decimal',2]) ?> </span></p>

                                    <p class="orderpending">Total Number of meal sold<span class="get-invoice"><?= $total_data['total_meals_sold'] ?? 0 ?> </span></p>

                                    <p class="orderpending">Best Selling Day<span class="get-invoice"><?= $total_data['best_selling_day'] ?></span></p>

                                    <p class="orderpending">Best meal sold<span class="get-invoice"><?= $total_data['best_selling_product'] ?></span></p>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$this->registerCss("
.was-validated .form-control:valid, .form-control.is-valid,.was-validated .custom-select:valid, .custom-select.is-valid {
  background-image: none;
}
");