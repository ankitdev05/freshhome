<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
$this->title = 'Add money to wallet | '.config_name;

?><?php $form = ActiveForm::begin(['id' => 'add-money-form']); ?>
<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'amount')->textInput() ?>
    </div>

    <?php if(!empty($cards)){
        echo '<div class="col-sm-12"><div class="form-group field-wallet-amount required"><label>Select Card</label><br/>';
        foreach($cards as $result){ ?>
            <div class="custom-control custom-radio custom-control-inline mb-2">
                <input type="radio" class="custom-control-input" id="customCard<?= $result['id'] ?>" name="Wallet[card_id]" value="<?= $result['id']  ?>" value="customEx">
                <label class="custom-control-label" for="customCard<?= $result['id'] ?>">
                    <img src="<?= $result['image_url'] ?>" class="img-fluid" width="30" />
                    <?= $result['masked_num'] ?>
                </label>
            </div>
            <?php
        }
        echo '</div></div>';
    }
    ?>
    <div class="col-sm-12 text-center">OR</div>
    <div class="col-sm-12 text-center">
        <?= Html::a('New Card','javascript:void(0)',['class' => 'btn btn-sm btn-light','data-toggle' => 'modal','data-target' => '#addCard','onClick' => '$("#selectPayment").modal("hide")']); ?>
        <hr/>
    </div>
    <div class="col-sm-12">
        <?= Html::submitButton( Yii::t('app', 'Save'), ['name'=>'add-wallet','class' => 'btn custom-btn']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
