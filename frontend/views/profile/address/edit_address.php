<?php
use yii\widgets\DetailView;
$this->title = $model->title.' - Address | '.config_name;
$this->params['breadcrumbs'][] = ['label' => 'Addresses', 'url' => ['/profile', 'type' => 'address']];
$this->params['breadcrumbs'][] = $model->title;
?>
<section class="address-list">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <div class="profile-side">
                        <?= $this->render('//profile/_left_panel') ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <div class="profile-body">
                        <div class="address-list-block">
                            <?= $this->render('//profile/_form_address', [
                                'model' => $model,
                            ]) ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
