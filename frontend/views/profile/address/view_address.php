<?php
use yii\widgets\DetailView;
$this->title = $model->title.' - Address | '.config_name;
$this->params['breadcrumbs'][] = ['label' => 'Addresses', 'url' => ['/profile', 'type' => 'address']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="row">
    <div class="col-sm-4">
        <?= $this->render('//profile/_left_panel') ?>
    </div>
    <div class="col-sm-8">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'title',
                'location',
                'building_name',
                'flat_no',
                'floor_no',
                'landmark',
                'is_default',

            ]
        ]) ?>
    </div>
</div>