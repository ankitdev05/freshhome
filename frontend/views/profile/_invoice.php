<?php
use yii\helpers\Url;
use common\models\UserData;

$config_logo    = config_logo;
$image = Url::to(Yii::$app->params['website_path'].'/images/store/'.$config_logo,true);
$supplier = $model->supplier;

$city_info = $supplier->cityInfo;
$address = $model->address;
$user_info = UserData::findOne(['user_id'=>$model->user_id]);
$order_products = $model->orderProducts;
$order_total = $model->orderTotals;

?>
<body style="margin:0;padding:0;">
<div style="width:80%;margin:0 auto;">
    <div style="float:left;width:50%;">
        <img src="<?= $image ?>" width="250">
    </div>
    <div style="float:left;width:50%">
        <h4 style="font-size: 20px;text-align:right;margin-bottom: 0;">Tax Invoice/Bill of Supply/Cash Memo</h4>
        <p style="font-size:20px;text-align:right;margin-top:5px;">(Original for Recipient)</p>
    </div>
    <div style="width:100%;margin-top:15px;float:left;">
        <div style="float:left;width:50%;">
            <h5 style="font-size:18px;margin-bottom:0;">Sold By :</h5>
            <p style="margin:0;"><?= ucwords($supplier->name) ?></p>
            <p style="margin:0;padding-right:75px;"><?= ucwords($supplier->location) ?></p>
            <p style="margin:0;padding-right:75px;">Building No.<?= ucwords($supplier->building_no) ?>, Flat No. <?= ucwords($supplier->flat_no) ?>, Floor No. <?= ucwords($supplier->floor_no) ?></p>
            <p style="margin:0;"><?= ucwords(!empty($city_info) ? $city_info->city_name : '') ?></p>
        </div>
        <div style="float:left;width:50%;text-align:right;">
            <h5 style="font-size:18px;margin-bottom:0;">Billing Address :</h5>
            <p style="margin:0;"><?= $user_info->name ?></p>
            <p style="margin:0;padding-left:75px;"><?= $address->location ?></p>
            <p style="margin:0;">Building No.<?= ucwords($address->building_name) ?>, Flat No. <?= ucwords($address->flat_no) ?>, Floor No. <?= ucwords($address->floor_no) ?></p>

        </div>
    </div>
    <div style="width:100%;float:left;">
        <div style="float:left;width:50%;">
            <h5 style="font-size:18px;margin-bottom:0;margin-top:10px;">Supplier Code :</h5>
            <span style="margin:0;"><?= $supplier->share_id ?></span>
        </div>
        <div style="float:left;width:50%;text-align:right;">
            <h5 style="font-size:18px;margin-bottom:0;margin-top:10px;">Shipping Address :</h5>
            <p style="margin:0;"><?= $user_info->name ?></p>
            <p style="margin:0;padding-left:75px;"><?= $address->location ?></p>
            <p style="margin:0;">Building No.<?= ucwords($address->building_name) ?>, Flat No. <?= ucwords($address->flat_no) ?>, Floor No. <?= ucwords($address->floor_no) ?></p>
        </div>
    </div>
    <div style="width:100%;float:left;">
        <div style="float:left;width:50%;">
            <h5 style="font-size:16px;margin-bottom:0;margin-top:0;">Order Number : <span style="font-weight:normal;"><?= $model->order_id ?></span></h5>
            <h5 style="font-size:16px;margin-bottom:0;margin-top:0;">Order Date : <span style="font-weight:normal;"><?= date('d.m.Y',$model->created_at) ?></span></h5>
        </div>
        <div style="float:left;width:50%;text-align:right;">
            <h5 style="font-size:16px;margin-bottom:0;margin-top:0;">Invoice Number : <span style="font-weight:normal;">IN-<?= $model->order_id ?></span></h5>
            <h5 style="font-size:16px;margin-bottom:0;margin-top:0;">Invoice Details : <span style="font-weight:normal;">XXXXXXXX</span></h5>
            <h5 style="font-size:16px;margin-bottom:0;margin-top:0;">Invoice Date : <span style="font-weight:normal;"><?= date('d.m.Y',$model->created_at) ?></span></h5>
        </div>
    </div>
    <div style="width:100%;float:left;margin-top:20px;margin-bottom:50px;">
        <table class="table table-bordered" style="text-align:left;width:100%;border-collapse:collapse;">
            <thead>
            <tr>
                <th style="border-collapse:collapse;border:1px solid #ddd;padding:10px;">S No.</th>
                <th style="border-collapse:collapse;border:1px solid #ddd;padding:10px;">Description</th>
                <th style="border-collapse:collapse;border:1px solid #ddd;padding:10px;">Unit Price</th>
                <th style="border-collapse:collapse;border:1px solid #ddd;padding:10px;">Qty</th>
                <th style="border-collapse:collapse;border:1px solid #ddd;padding:10px;">Net Amount</th>
                <th style="border-collapse:collapse;border:1px solid #ddd;padding:10px;">Total Amount</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($order_products as $k=>$order_product){ ?>
            <tr>
                <td style="border-collapse:collapse;border:1px solid #ddd;padding:10px;"><?= $k+1 ?></td>
                <td style="border-collapse:collapse;border:1px solid #ddd;padding:10px;">
                    <?= $order_product->name ?>
                </td>
                <td style="border-collapse:collapse;border:1px solid #ddd;padding:10px;">
                    <?= $model->currency_code ?> <?= $order_product->price ?>
                </td>
                <td style="border-collapse:collapse;border:1px solid #ddd;padding:10px;">
                    <?= $order_product->quantity ?>
                </td>
                <td style="border-collapse:collapse;border:1px solid #ddd;padding:10px;">
                    <?= $model->currency_code ?> <?= $order_product->price ?>
                </td>
                <td style="border-collapse:collapse;border:1px solid #ddd;padding:10px;">
                    <?= $model->currency_code ?> <?= $order_product->total ?>
                </td>
            </tr>
            <?php } ?>
            </tbody>
            <tfoot>

            <?php foreach($order_total as $total){ ?>
            <tr>
                <td style="border-collapse: collapse;border:1px solid #ddd;padding:10px;" colspan="5"><?= $total->title ?></td>
                <td style="border-collapse: collapse;border:1px solid #ddd;padding:10px;">
                    <?= $model->currency_code ?> <?= $total->value ?>
                </td>
            </tr>
            <?php } ?>

            </tfoot>
        </table>
    </div>
</div>
</body>