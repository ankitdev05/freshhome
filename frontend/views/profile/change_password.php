<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Change Password | profile';
?>
<div class="change-password">
    <h3>Change password</h3>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'old_password')->passwordInput() ?>
        </div>
        <div class="w-100"></div>

        <div class="col-sm-4">
            <?= $form->field($model, 'password')->passwordInput() ?>
        </div>
        <div class="w-100"></div>

        <div class="col-sm-4">
            <?= $form->field($model, 'password_repeat')->passwordInput() ?>
        </div>
        <div class="form-group col-sm-12">
            <?= Html::submitButton( Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
