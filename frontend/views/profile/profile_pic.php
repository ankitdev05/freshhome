<?php
use yii\helpers\Html;

$image = $user_info->getSupplierImage($user_info->profile_pic,100,100);
?>
<div class="proile-account">
    <?php
        if(!empty($image))
            echo Html::img($image,['class'=>'img-thumbnail']);
        else
            echo Html::tag('i','',['class' => 'far fa-user fa-5x']);
    ?>

    <span><?= $user_info->name ?></span>
    <span class="acc-name">Account</span>
</div>