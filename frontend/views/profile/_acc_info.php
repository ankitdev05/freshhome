<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control txtfield" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="<?= $user_info->email ?>" readonly>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control txtfield" id="exampleInputPassword1" placeholder="******" readonly>
        </div>
    </div>
</div>