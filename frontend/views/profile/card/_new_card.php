<?php
use yii\bootstrap4\Html;

use yii\bootstrap4\ActiveForm;

use common\models\Library;
use kartik\select2\Select2;

$library	= new Library();
$years		= $library->getCCyears();
$months		= $library->getCCmonths();
$model->redirect_to	= isset($rurl) ? $rurl : '';
$form = ActiveForm::begin([
		'action'=>['/profile?type=creditCard'],
		'options' => ['enctype' => 'multipart/form-data','id'=>'new_card']
	]); ?>
    <div class="row">
        <div class="col-sm-12 mt-3">
            <?= $form->field($model, 'card_name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-12">
            <?= $form->field($model, 'card_number')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'exp_month')->widget(Select2::classname(), [
                'data' => $months,
                'options' => ['placeholder' => 'Please select ...',],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'exp_year')->widget(Select2::classname(), [
                'data' => $years,
                'options' => ['placeholder' => 'Please select ...',],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'cvv')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'redirect_to')->hiddenInput()->label(false) ?>
        </div>
    </div>
<?= Html::submitButton('Save', ['class' => 'btn btn custom-btn', 'name' => 'credit-card']) ?>
<?php ActiveForm::end();
$this->registerCss('
.select2-container--krajee .select2-selection--single{
	height:34px;
	margin-top:0;
	padding:6px 24px 6px 12px;
}
.select2-container--krajee .select2-selection--single .select2-selection__arrow{
	height:32px;
}
.select2-container--krajee .select2-selection__clear{
	top:0.4rem;
}
');