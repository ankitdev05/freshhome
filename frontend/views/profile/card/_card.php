<?php
use yii\helpers\Html;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FAR;
?>
<div class="addedcardinfo">
    <div class="form-row">
        <div class="col-md-5 col-12">
            <label>Card Name</label>
            <p>
                <img class="mr-3" width="40" src="<?= $model->image_url ?>"> <?= $model->masked_num ?>
            </p>
        </div>
        <div class="col-md-3 col-5">
            <label>Name</label>
            <p><?= $model->name ?></p>
        </div>
        <div class="col-md-2 col-4">
            <label>Exp. Date</label>
            <p><?= $model->exp_date ?></p>
        </div>
        <div class="col-md-2 col-3">
            <label>Action</label>
            <p><?= Html::a(FAR::icon('trash-alt'),Url::to(['/profile/delete_card','id'=>$model->id]),['data-confirm'=>'Are you sure want to delete this card?','data-method'=>'post']) ?></p>
        </div>
    </div>
</div>