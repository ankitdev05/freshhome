<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use common\models\CreditCards;

$user_id 	= Yii::$app->user->id;
$query		= CreditCards::find()->where(['user_id'=>$user_id,'type'=>Yii::$app->params['btree_environment']]);

$dataProvider = new ActiveDataProvider([
	'query' => $query,
	'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
]);
$count		= $dataProvider->getTotalCount();
if($count>0){
echo ListView::widget( [
	'dataProvider' => $dataProvider,
    'options' => [
        'tag' => false,
    ],
	'itemOptions' => [
		'tag' => false,
	],
	'summary' => false,
	'itemView' => '_card',
]);

}