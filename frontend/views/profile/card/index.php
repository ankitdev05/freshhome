<?php
use rmrevin\yii\fontawesome\FAS;

$type	= '';
if(isset($_GET['type']) && !empty($_GET['type']))
	$type	= $_GET['type'];
$this->title	= 'Credit Cards | '.config_title;
$this->params['breadcrumbs'][] = 'Credit Card';
$breadcrumbs[] = ['href'=>false, 'label' => 'Change Password'];
?>
<section class="address-list">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                    <div class="profile-side">
                        <?= $this->render('//profile/_left_panel',['type'=>$type]) ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9">
                    <div class="profile-body">
                        <div class="address-list-block">
                            <?= $this->render('_user_cards',['type'=>$type]) ?>
							
                            <div class="well" style="cursor:pointer;" onclick="$('#new_card').fadeIn(400)">
                                <div class="col-md-12">
									<div class="row">
										<div class="col-xs-12">
											<h5 class="mb-0 mt-2">Add a new credit card</h5>
										</div>
										<hr/>

										<?= $this->render('_new_card',['model'=>$model]) ?>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>