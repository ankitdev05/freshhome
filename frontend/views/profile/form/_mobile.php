<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;
use yii\web\JsExpression;
?>
<div class="profile_mobile form_edit" style="display:none">

    <?php $form = ActiveForm::begin(['enableClientScript'=>false,'enableClientValidation'=>false,'options' => ['enctype' => 'multipart/form-data'],'id' => 'sign-in-form']); ?>
    <div class="row">
        <div class="col-sm-9">

            <?= $form->field($model, 'phone_number')->widget(PhoneInput::className(),[
                'options' => ['id' => 'phone-number'],
                'jsOptions'=>['nationalMode'=>false,'autoHideDialCode'=>false,'initialCountry'=>'auto','geoIpLookup'=>new JsExpression('function(callback) { 
				            $.get(\'https://ipinfo.io\', function() {}, "jsonp").always(function(resp) {
					        var countryCode = (resp && resp.country) ? resp.country : "";
					        callback(countryCode);
					       
					        
					});
				}')]
            ]) ?>
        </div>
        <div class="col-sm-3">
			<div class="pt-2 mb-3 m-profile">
				<?= Html::button(Yii::t('app', 'Cancel'),['class' => 'btn btndark btnabout btn-sm float-right','style'=>'margin-top:25px','onClick'=>"$('.profile_mobile_').show('slow'); $('.profile_mobile').hide();"]) ?>
				<?= Html::button($model->isNewRecord ? 'Add Phone number' : 'Update', ['class' => 'btn-default btn btn-sm btnabout mt-4', 'name' => 'profile-phone','id' => 'sign-in-button','style' =>'background-color:#DF8317;color:#fff;']) ?>&nbsp;
			</div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <?php $form = ActiveForm::begin(['enableClientScript'=>false,'enableClientValidation'=>false,'options' => ['enctype' => 'multipart/form-data'],'id' => 'verification-code-form']); ?>
    <div class="row">
        <div class="col-sm-8 paddl0">
            <?= $form->field($model, 'verification_code')->textInput(['id' => 'verification-code']) ?>
            <div class="clearfix"></div>

        </div>
        <div class="col-sm-3 col-12">
			<div class="mb-3 mt-3 pt-2 m-profile">
				<?= Html::button('Cancel',['class' => 'btn btndark btn-sm float-right btnabout mt-2','onClick'=>"$('.profile_mobile_').show('slow'); $('.profile_mobile').hide();"]) ?>
				<?= Html::submitButton('Submit', ['id' => 'verify-code-button','class' => 'btn-default btn btn-sm btnabout', 'name' => 'verify-phone','style' =>'background-color:#DF8317;color:#fff;']) ?>&nbsp;
			</div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<div class="row profile_mobile_ form_view">
    <div class="col-md-3 col-12"><label><?= Yii::t('app', 'Mobile') ?></label></div>
    <div class="col-md-7 col-9"><p><strong><?= ($model->phone_number) ?></strong></p></div>
    <div class="col-md-2 col-3">
        <h5 class="text-right edit-icon">
    <a href="javascript:void(0)" onClick="$('.form_view').show();$('.form_edit').hide();$('.profile_mobile').show('slow');$('.profile_mobile_').hide();"><?= Yii::t('app', 'Edit') ?></a>
        </h5>
    </div>
</div>
<?php
$this->registerJsFile('https://www.gstatic.com/firebasejs/6.2.0/firebase-app.js', [
    'position' => \yii\web\View::POS_HEAD
]);
$this->registerJsFile('https://www.gstatic.com/firebasejs/6.2.0/firebase-auth.js',[
    'position' => \yii\web\View::POS_HEAD
]);
$this->registerJsFile('https://www.gstatic.com/firebasejs/6.2.0/firebase-firestore.js',[
    'position' => \yii\web\View::POS_HEAD
]);


$this->registerJs('
  var firebaseConfig = {
    apiKey: "AIzaSyBI2zYYmUyDTh9nwX8czmiOsoJa_dyw_LI",
    authDomain: "freshhome-eb8df.firebaseapp.com",
    databaseURL: "https://freshhome-eb8df.firebaseio.com",
    projectId: "freshhome-eb8df",
    storageBucket: "freshhome-eb8df.appspot.com",
    messagingSenderId: "350237825472",
    appId: "1:350237825472:web:0229272c446eeac8"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
	window.onload = function() {
		onSignOutClick();
		firebase.auth().onAuthStateChanged(function(user) {
			
			if (user) {
				var uid = user.uid;
				var email = user.email;
				var photoURL = user.photoURL;
				var phoneNumber = user.phoneNumber;
				var isAnonymous = user.isAnonymous;
				var displayName = user.displayName;
				var providerData = user.providerData;
				var emailVerified = user.emailVerified;
				//console.log(user);
			}
			updateSignInButtonUI();
			  updateSignInFormUI();
			  updateSignOutButtonUI();
			  updateSignedInUserStatusUI();
			  updateVerificationCodeFormUI();
		});
		
		document.getElementById(\'phone-number\').addEventListener(\'keyup\', updateSignInButtonUI);
		document.getElementById(\'phone-number\').addEventListener(\'change\', updateSignInButtonUI);
		document.getElementById(\'verification-code\').addEventListener(\'keyup\', updateVerifyCodeButtonUI);
		document.getElementById(\'verification-code\').addEventListener(\'change\', updateVerifyCodeButtonUI);
		document.getElementById(\'verification-code-form\').addEventListener(\'submit\', onVerifyCodeSubmit);
		
		window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(\'sign-in-button\', {
		  \'size\': \'invisible\',
		  \'callback\': function(response) {
			// reCAPTCHA solved, allow signInWithPhoneNumber.
			onSignInSubmit();
		  }
		});
		recaptchaVerifier.render().then(function(widgetId) {
		  window.recaptchaWidgetId = widgetId;
		  updateSignInButtonUI();
		});
	};
	
	function updateSignInButtonUI() {
		
		document.getElementById(\'sign-in-button\').disabled =
        !isPhoneNumberValid()
        || !!window.signingIn;
	}
	function updateVerifyCodeButtonUI() {
		document.getElementById(\'verify-code-button\').disabled =
			!!window.verifyingCode
			|| !getCodeFromUserInput();
	}
	function updateVerificationCodeFormUI() {
    if (!firebase.auth().currentUser && window.confirmationResult) {
      document.getElementById(\'verification-code-form\').style.display = \'block\';
    } else {
      document.getElementById(\'verification-code-form\').style.display = \'none\';
    }
  }
	function updateSignOutButtonUI() {
		
	}
	function updateSignedInUserStatusUI() {
		var user = firebase.auth().currentUser;
		if (user) {
			console.log(JSON.stringify(user, null, \'  \'));
            $("#sign-in-form").submit();
		}
	}
	function onSignOutClick() {
		firebase.auth().signOut();
	  }
	function onSignInSubmit() {
		if (isPhoneNumberValid()) {
		  window.signingIn = true;
		  updateSignInButtonUI();
		  var phoneNumber = getPhoneNumberFromUserInput();
		  var appVerifier = window.recaptchaVerifier;
		  firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
			  .then(function (confirmationResult) {
				// SMS sent. Prompt user to type the code from the message, then sign the
				// user in with confirmationResult.confirm(code).
				window.confirmationResult = confirmationResult;
				window.signingIn = false;
				updateSignInButtonUI();
				updateVerificationCodeFormUI();
				updateVerifyCodeButtonUI();
				updateSignInFormUI();
			  }).catch(function (error) {
				// Error; SMS not sent
				//console.error(\'Error during signInWithPhoneNumber\', error);
				window.alert(\'Error during signInWithPhoneNumber:\n\n\'
					+ error.code + \'\n\n\' + error.message);
				window.signingIn = false;
				updateSignInFormUI();
				updateSignInButtonUI();
			  });
		}
	  }
	function updateSignInFormUI() {
		if (firebase.auth().currentUser || window.confirmationResult) {
		  document.getElementById(\'sign-in-form\').style.display = \'none\';
		} else {
		  resetReCaptcha();
		  document.getElementById(\'sign-in-form\').style.display = \'block\';
		}
	  }
	  function getCodeFromUserInput() {
		return document.getElementById(\'verification-code\').value;
	  }
	  
	  function getPhoneNumberFromUserInput() {
		return document.getElementById(\'phone-number\').value;
	  }
	  function isPhoneNumberValid() {
		var pattern = /^\+[0-9\s\-\(\)]+$/;
		var phoneNumber = getPhoneNumberFromUserInput();
		if(phoneNumber.length > 5)
			return phoneNumber.search(pattern) !== -1;
	  }
	  
	function resetReCaptcha() {
    if (typeof grecaptcha !== \'undefined\'
        && typeof window.recaptchaWidgetId !== \'undefined\') {
      grecaptcha.reset(window.recaptchaWidgetId);
    }
  }
	function onVerifyCodeSubmit(e) {
    e.preventDefault();
    if (!!getCodeFromUserInput()) {
      window.verifyingCode = true;
      updateVerifyCodeButtonUI();
      var code = getCodeFromUserInput();
      confirmationResult.confirm(code).then(function (result) {
        // User signed in successfully.
        var user = result.user;
        window.verifyingCode = false;
        window.confirmationResult = null;
        updateVerificationCodeFormUI();
      }).catch(function (error) {
        // User couldn\'t sign in (bad verification code?)
        console.error(\'Error while checking the verification code\', error);
        window.alert(\'Error while checking the verification code:\n\n\'
            + error.code + \'\n\n\' + error.message);
        window.verifyingCode = false;
        updateSignInButtonUI();
        updateVerifyCodeButtonUI();
      });
    }
  }  
', yii\web\View::POS_HEAD);