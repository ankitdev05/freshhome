<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use himiklab\thumbnail\EasyThumbnailImage;

$imageSrc =  [];

$del_image = [['url'=>yii::$app->request->baseUrl.'/profile/del-profile-pic']];
if(empty($model->profile_pic))
    $model->profile_pic = 'no_image.png';
if( !empty($model->profile_pic)){
    $image=EasyThumbnailImage::thumbnailFileUrl(
        "../../frontend/web/uploads/".$model->profile_pic,200,200,EasyThumbnailImage::THUMBNAIL_INSET
    );
    $imageSrc	= [Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Profile Picture', 'title'=>'Profile Picture'])];
}
$class = strtolower(\yii\helpers\StringHelper::basename(get_class($model)));

?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' => 'profile-image']); ?>
    <img src="<?= $image ?>" alt="Profile picture" class="img-pc img-fluid">
<?= $form->field($model, 'profile_pic')->fileInput(['class' => 'd-none'])->label(false) ?>
    <span><a href="javascript:void(0)" id="changePic"><?= Yii::t('app', 'Change Image') ?></a> </span>
<?php ActiveForm::end(); ?>
<?php
$this->registerJs('
    $("#changePic").on("click",function(){
        $("#'.$class.'-profile_pic").click();
    });
    $("#'.$class.'-profile_pic").on("change",function(){
        $("#profile-image").submit();
    })
    
');