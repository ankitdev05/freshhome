<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

?>
<div class="profile_location form_edit" style="display:none">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' => 'profile-location']); ?>
        <div class="row">
            <div class="col-sm-9">
                <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'latitude')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'longitude')->hiddenInput()->label(false) ?>
                <div class='map' id='map_canvas' style="height:300px;"></div>
            </div>
            <div class="col-md-3 col-12">
				<div class="mb-3 mt-3 pt-2 m-profile">
					<?= Html::a(Yii::t('app', 'Cancel'),'javascript:void(0)',['class' => 'btn btndark btn-sm float-right btnabout mt-2','onClick'=>"$('.profile_location_').show('slow'); $('.profile_location').hide();"]) ?>
					<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-default btn-sm btnabout mt-2','name'=>'update_location','style' =>'background-color:#DF8317;color:#fff;']) ?>&nbsp;
				</div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

</div>
<div class="row profile_location_ form_view">
    <div class="col-md-3 col-12"><label><?= Yii::t('app', 'Location') ?></label></div>
    <div class="col-md-7 col-9"><p><strong><?= ucwords($model->location) ?></strong></p></div>
    <div class="col-md-2 col-3">
        <h5 class="text-right edit-icon"><a href="javascript:void(0)" onClick="$('.form_view').show();$('.form_edit').hide();$('.profile_location').show('slow');$('.profile_location_').hide();"><?= Yii::t('app', 'Edit') ?></a> </h5></div>
</div>
<?php
echo $this->render('//site/_location',['model'=>$model]);