<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
function obfuscate_email($email)
{
    $em   = explode("@",$email);
    $name = implode(array_slice($em, 0, count($em)-1), '@');
    $len  = floor(strlen($name)/2);

    return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($em);
}
$email = $model->email;
$model->email = '';
?>
<div class="profile_email form_edit" style="display:none">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' => 'profile-email']); ?>
    <div class="row">
        <div class="col-sm-9">
            <?= $form->field($model, 'old_email_address')->textInput() ?>
            <?= $form->field($model, 'email')->textInput()->label('New email address') ?>
        </div>
        <div class="col-md-3 col-12">
			<div class="mb-3 mt-3 pt-2 m-profile">
				<?= Html::a(Yii::t('app', 'Cancel'),'javascript:void(0)',['class' => 'btn btndark btn-sm float-right btnabout mt-2','onClick'=>"$('.profile_email_').show('slow'); $('.profile_email').hide();"]) ?>
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-default btn-sm btnabout mt-2','name'=>'update_email','style' =>'background-color:#DF8317;color:#fff;']) ?>&nbsp;
			</div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<div class="row profile_email_ form_view">
    <div class="col-md-3 col-12"><label><?= Yii::t('app', 'Email') ?></label></div>
    <div class="col-md-7 col-9"><p><strong><?= obfuscate_email($email) ?></strong></p></div>
    <div class="col-md-2 col-3">
        <h5 class="text-right edit-icon">
        <a href="javascript:void(0)" onClick="$('.form_view').show();$('.form_edit').hide();$('.profile_email').show('slow');$('.profile_email_').hide();"><?= Yii::t('app', 'Edit') ?></a>
        </h5>
    </div>
</div>
