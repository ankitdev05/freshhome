<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
?>
<div class="profile_gender form_edit" style="display:none">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' => 'profile-gender']); ?>
    <div class="row">
        <div class="col-sm-9">
            <?= $form->field($model, 'gender')->widget(Select2::classname(), [
                'data' =>['male'=>'Male','female'=>'Female'],
                'options' => ['placeholder' => 'Please select ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
        <div class="col-md-3 col-12">
			<div class="mb-3 mt-3 pt-2 m-profile">
				<?= Html::a(Yii::t('app', 'Cancel'),'javascript:void(0)',['class' => 'btn btndark btnabout btn-sm float-right mt-2','onClick'=>"$('.profile_gender_').show('slow'); $('.profile_gender').hide();"]) ?>
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-default btn-sm btnabout mt-2','name'=>'update_gender','style' =>'background-color:#DF8317;color:#fff;']) ?>&nbsp;
			</div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<div class="row profile_gender_ form_view">
    <div class="col-md-3 col-12"><label><?= Yii::t('app', 'Gender') ?></label></div>
    <div class="col-md-7 col-9"><p><strong><?= ucwords($model->gender) ?></strong></p></div>
    <div class="col-md-2 col-3">
        <h5 class="text-right edit-icon">
            <a href="javascript:void(0)" onClick="$('.form_view').show();$('.form_edit').hide();$('.profile_gender').show('slow');$('.profile_gender_').hide();"><?= Yii::t('app', 'Edit') ?></a>
        </h5>
    </div>
</div>
