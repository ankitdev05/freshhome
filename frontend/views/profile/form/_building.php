<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

?>
<div class="profile_building form_edit" style="display:none">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' => 'profile-building']); ?>
    <div class="row">
        <div class="col-sm-9">
            <?= $form->field($model, 'building_no')->textInput(['maxlength'=>true]) ?>
        </div>
        <div class="col-md-3 col-12">
			<div class="mb-3 mt-3 pt-2 m-profile">
				<?= Html::a(Yii::t('app', 'Cancel'),'javascript:void(0)',['class' => 'btn btndark btn-sm float-right mt-2 btnabout','onClick'=>"$('.profile_building_').show('slow'); $('.profile_building').hide();"]) ?>
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-default btn-sm mt-2 btnabout','name'=>'update_building','style' =>'background-color:#DF8317;color:#fff;']) ?>&nbsp;
			</div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<div class="row profile_building_ form_view">
    <div class="col-md-3 col-12"><label><?= Yii::t('app', 'Building name or No.') ?></label></div>
    <div class="col-md-7 col-9"><p><strong><?= ucwords($model->building_no) ?></strong></p></div>
    <div class="col-md-2 col-3">
        <h5 class="text-right edit-icon"><a href="javascript:void(0)" onClick="$('.form_view').show();$('.form_edit').hide();$('.profile_building').show('slow');$('.profile_building_').hide();"><?= Yii::t('app', 'Edit') ?></a>
        </h5>
    </div>
</div>
