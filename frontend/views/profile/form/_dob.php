<?php
use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap4\ActiveForm;

?>
<div class="profile_dob form_edit" style="display:none">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' => 'profile-dob']); ?>
    <div class="row">
        <div class="col-sm-9">
            <?= $form->field($model, 'dob')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter birth date ...','readonly'=>true],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label('Date of Birth'); ?>
        </div>
        <div class="col-md-3 col-12">
			<div class="mb-3 mt-3 pt-2 m-profile">
				<?= Html::a(Yii::t('app', 'Cancel'),'javascript:void(0)',['class' => 'btn btndark btnabout btn-sm float-right mt-2','onClick'=>"$('.profile_dob_').show('slow'); $('.profile_dob').hide();"]) ?>
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-default btnabout btn-sm mt-2','name'=>'update_dob','style' =>'background-color:#DF8317;color:#fff;']) ?>&nbsp;
			</div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<div class="row profile_dob_ form_view">
    <div class="col-md-3 col-12"><label><?= Yii::t('app', 'Date Of Birth') ?></label></div>
    <div class="col-md-7 col-9"><p><strong><?= ($model->dob) ?></strong></p></div>
    <div class="col-md-2 col-3">
        <h5 class="text-right edit-icon">
        <a href="javascript:void(0)" onClick="$('.form_view').show();$('.form_edit').hide();$('.profile_dob').show('slow');$('.profile_dob_').hide();"><?= Yii::t('app', 'Edit') ?></a>
        </h5>
    </div>
</div>
