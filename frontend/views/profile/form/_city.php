<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use common\models\City;
use yii\helpers\ArrayHelper;

$language_id = language_id;
$all_cities = ArrayHelper::map(City::find()->joinWith('cityName as cn')->where(['cn.language_id'=>$language_id])->asArray()->all(), 'id', 'cityName.city_name');

?>

<div class="profile_city form_edit" style="display:none">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' => 'profile-city']); ?>
    <div class="row">
        <div class="col-sm-9">

            <?= $form->field($model, 'city')->widget(Select2::classname(), [
                'data' =>$all_cities,
                'options' => ['placeholder' => 'Please select ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
        <div class="col-md-3 col-12">
			<div class="mb-3 mt-3 pt-2 m-profile">
				<?= Html::a(Yii::t('app', 'Cancel'),'javascript:void(0)',['class' => 'btn btndark btn-sm float-right btnabout mt-2','onClick'=>"$('.profile_city_').show('slow'); $('.profile_city').hide();"]) ?>
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-default btn-sm btnabout mt-2','name'=>'update_city','style' =>'background-color:#DF8317;color:#fff;']) ?>&nbsp;
			</div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<div class="row profile_city_ form_view">
    <div class="col-md-3 col-12"><label><?= Yii::t('app', 'City') ?></label></div>
    <div class="col-md-7 col-9"><p><strong><?= !empty($model->cityInfo) ?  ucwords($model->cityInfo->city_name) : '' ?></strong></p></div>

    <div class="col-md-2 col-3">
        <h5 class="text-right edit-icon">
    <a href="javascript:void(0)" onClick="$('.form_view').show();$('.form_edit').hide();$('.profile_city').show('slow');$('.profile_city_').hide();"><?= Yii::t('app', 'Edit') ?></a>
        </h5>
    </div>
</div>
