<?php
$this->title = 'Profile | '.config_name;
$user_info = Yii::$app->user->identity;
$breadcrumbs[] = ['href'=>false, 'label' => 'Profile'];
$user_detail = $user_info->userDetail;
?>

<section class="address-list common-color">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                    <div class="profile-side">
                        <?= $this->render('//profile/_left_panel') ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <div class="profile-body">
                        <div class="address-list-block">
                            <div class="row">
                                <div class="proile-account">
                                    <?= $this->render('form/_image',['model' => $model]) ?>
                                </div>
								<div class="col-md-12">
									<div class="userprofileaccount">
                                        <?= $this->render('form/_name',['model' => $model]) ?>
									</div>
									<div class="userprofileaccount">
                                        <?= $this->render('form/_dob',['model' => $model]) ?>
									</div>
									<div class="userprofileaccount">
                                        <?= $this->render('form/_gender',['model' => $model]) ?>
									</div>
									<div class="userprofileaccount">
                                        <?= $this->render('form/_about',['model' => $model]) ?>

									</div>
									<div class="userprofileaccount">
                                        <?= $this->render('form/_mobile',['model' => $model]) ?>
										
									</div>
									<div class="userprofileaccount">
                                        <?= $this->render('form/_email',['model' => $user_model]) ?>
									</div>
									<div class="userprofileaccount">
                                        <?= $this->render('form/_occupation',['model' => $model]) ?>
									</div>
									<div class="userprofileaccount">
                                        <?= $this->render('form/_nationality',['model' => $model]) ?>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$this->registerCss('
.table{color:#6a7889}');
