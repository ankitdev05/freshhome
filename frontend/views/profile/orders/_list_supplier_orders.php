<?php
use yii\helpers\Url;

$address= $model->address;
?>
<div class="order-table orderlisting">
    <a href="<?= Url::to(['/profile','type'=>'vieworder','order_id'=>$model->order_id],true) ?>" title="View Order">
        <p>
            <span>Order No.</span>
            <span class="no">&nbsp;<?= $model->order_id ?></span>
            <span class="right-list orderno"><?= date('d F,Y',$model->created_at) ?></span> 
        </p>
        <div class="right-arrow">
            <p>
                <span>Total Items</span>
                <span class="no">&nbsp;<?= $model->countOrderProducts ?></span>
                <span class="right-list totalprice">Total Price <b> <?= $model->currency_code ?> <?= $model->total ?></b></span>
            </p>
        </div>
        <p>
            <span>Address</span>
            <span class="no">&nbsp;<?= $address->location ?> (<?= $address->title ?>)</span>
        </p>
    </a>
</div>