<?php
use yii\helpers\Url;

$address= $model->address;
$order_status = $model->order_status_id;

$btn = 'btn-warning';
if($order_status==1)
    $btn = 'btn-danger';
if($order_status==11)
    $btn = 'btn-success';
if($order_status==13)
    $btn = 'btn-light';
if($order_status==5)
    $btn = 'btn-dark';
?>
<div class="order-table orderlisting">
    <a href="<?= Url::to(['/profile','type'=>'vieworder','order_id'=>$model->order_id],true) ?>" title="View Order">
        <p>
            <span style="width:300px;">Order No.</span>
            <span class="no">&nbsp;<?= $model->order_id ?></span>
            <span class="right-list ml-2"><button class="btnaccepted text-white btn btn-sm <?= $btn ?>"><?= $model->orderStatus->name ?></button></span>
            <span class="right-list profileorderdate"><?= date('d F,Y',$model->created_at) ?></span>
        </p>
        <div class="right-arrow pt-1">
            <p><span style="width:300px;">Total Items</span><span class="no">&nbsp;<?= $model->countOrderProducts ?></span><span class="right-list totalprice">Total Price <b> <?= $model->currency_code ?> <?= $model->total ?></b></span></p>
        </div>
        <p><span style="width:300px;">Address</span> <span class="no">&nbsp;<?= $address->location ?> (<?= $address->title ?>)</span></p>
    </a>
</div>