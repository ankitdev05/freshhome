<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$type = $_GET['type'] ?? '';
$this->title = 'Pending orders | '.config_name;
?>
<section class="address-list">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                    <div class="profile-side">
                        <?= $this->render('//profile/_left_panel',['type'=>$type]) ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <div class="profile-body">
                        <div class="address-list-block card-credit">
                            <p class="orderno">
                                <span class="trckorder">
                                    <?= Html::a('Previous Orders',\yii\helpers\Url::to(['/profile','type'=>'completedorders'],true),['style' => 'color:#DF8317 !important;']) ?>
                                </span>
                            </p>
                            <p class="status-color" style="font-weight: normal; color: #333;font-size: 15px;">Status: In Progress</p>
                            <?php Pjax::begin(); ?>
                            <?=  ListView::widget([
                                'layout' => "{items}\n<div class='col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
                                'dataProvider' => $dataProvider,
                                'itemView' => '_list_order',
                                'options' => [
                                    'tag' => false,
                                    'class' => 'row',
                                    'id' => 'list-wrapper',
                                ],
                                'itemOptions' => [
                                    'tag' => false
                                ],
                                'pager' => [
                                    'options' => ['class'=>'pagination justify-content-end'],
                                    'prevPageLabel' =>'Previous',
                                    'nextPageLabel' =>'Next',
                                    'pageCssClass' => 'page-item',
                                    'disabledPageCssClass' => 'page-link disabled',
                                    'linkOptions' => ['class' => 'page-link'],
                                ]
                            ]); ?>
                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>