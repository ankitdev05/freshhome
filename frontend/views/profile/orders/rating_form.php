<?php
use yii\bootstrap4\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\rating\StarRating;

?>
<?php $form = ActiveForm::begin(); ?>
<hr/>
<div class="feedback-star">
	<div class="form-row">
		<div class="col-md-12"><h5>Rate this order</h5></div>
		<div class="feedbackittle col-sm-6 col-6">Taste Rating</div>
		<div class="col-sm-6 float-right text-right  col-6">
			<?= $form->field($model, 'taste_rating')->widget(StarRating::classname(), [
				'pluginOptions' => ['size'=>'xs','showCaption'=>false,'step' => 1]
				])->label(false);
			?>
		</div>
		<div class="feedbackittle col-sm-6  col-6">Presentation</div>
		<div class="col-sm-6 float-right text-right col-6">
			<?= $form->field($model, 'presentation_rating')->widget(StarRating::classname(), [
				'pluginOptions' => ['size'=>'xs','showCaption'=>false,'step' => 1]
			])->label(false);
			?>
		</div>
		<div class="feedbackittle col-sm-6 col-6">Packing</div>
		<div class="col-sm-6 float-right text-right col-6">
			<?= $form->field($model, 'packing_rating')->widget(StarRating::classname(), [
				'pluginOptions' => ['size'=>'xs','showCaption'=>false,'step' => 1]
			])->label(false);
			?>
		</div>
		<div class="feedbackittle col-sm-6 col-6">Overall Experience</div>
		<div class="col-sm-6 float-right text-right col-6">
			<?= $form->field($model, 'overall_rating')->widget(StarRating::classname(), [
				'pluginOptions' => ['size'=>'xs','showCaption'=>false,'step' => 1]
			])->label(false);
			?>
		</div>
		<div class="col-md-12">
			<div class="comt-here">
				<div class="form-group">
					<?= $form->field($model, 'review')->textarea(['class' => 'form-control txtfield text-left','rows' => 5,'placeholder' => 'Write Your Comments Here..'])->label(false) ?>
				</div>
			</div>
		</div>
		<div class="form-group col-sm-12 mt-3">
			<?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn custom-btn']) ?>
		</div>
	</div>
</div>
<?php ActiveForm::end(); ?>