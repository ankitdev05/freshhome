<?php
use common\models\Library;

$this->title = 'View order - '.$model->order_id.' | '.config_name;

$user = $model->user;
$userAddr = $model->address;
$orderTotal = $model->orderTotals;
$format = Yii::$app->formatter;
$orderProducts = $model->orderProducts;
$countProducts = $model->countOrderProducts;
$order_rating = $model->orderRatings;
$order_options = $model->orderOptions;
$options = [];
$library = new Library();
if(!empty($order_options)){
    foreach ($order_options as $order_option){
        $add_text = '';
        if($order_option->price!='0.00'){
            $price = $library->currencyFormat($order_option->price,$model->currency_code);
            if($order_option->subtract==1)
                $add_text = '(- '.$price.')';
            elseif($order_option->subtract==0)
                $add_text = '(+'.$price.')';
        }
        $options[$order_option->name][] = $order_option->value.$add_text;
    }
}
?>
<section class="address-list">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                    <div class="profile-side">
                        <?= $this->render('//profile/_left_panel') ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <div class="profile-body">
                        <div class="address-list-block card-credit">
                            <p class="orderno">Order no. <?= $model->order_id ?>
                                <span class="trckorder"><a href="<?= Yii::$app->request->baseUrl.'/profile?type=track_order&order_id='.$model->order_id ?>"> Track Order</a></span>
                            </p>
                            <p class="orderpending">Status: <?= $model->orderStatus->name ?><span class="get-invoice"><a href="<?= Yii::$app->request->baseUrl.'/profile/invoice?order_id='.$model->order_id ?>" target="_blank">Get Invoice</a> </span></p>

                            <div class="order-table">
                                <p class="Deliver-info">Delivery Information<span class="orderdate"><?= date('d',$model->created_at) ?><sup><?= date('S',$model->created_at) ?></sup> <?= date('F Y',$model->created_at) ?> </span></p>
                                <table class="table table-hover profile-tble">
                                    <tbody>
                                        <tr class="border-td">
                                            <td  width="100px" class="table-name" style="">Phone</td>
                                            <td><span><?= $user->phone_number ?></span></td>
                                        </tr>
                                        <tr>
                                            <td  width="100px" class="table-name">Email-ID</td>
                                            <td><span><?= $user->email ?></span></td>
                                        </tr>
                                        <tr>
                                            <td  width="100px" class="table-name">Address</td>
                                            <td><span><?= $userAddr->title ?> - <?= $userAddr->location ?></span></td>
                                        </tr>
                                    <?php if(!empty($model->notes)){ ?>
                                        <tr>
                                            <td  width="250px" class="table-name">Additional Message</td>
                                            <td><span><?= $model->notes ?></span></td>
                                        </tr>
                                    <?php } ?>
                                        <?php if(!empty($model->phone_number)){ ?>
                                            <tr>
                                                <td  width="250px" class="table-name">Additional Phone number</td>
                                                <td><span><?= $model->phone_number ?></span></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <p class="Deliver-info">Payment Info<span class="orderpayment"><?=  $model->payment_method ?></span></p>
                                <table class="table table-hover profile-tble">
                                    <tbody>
                                    <?php foreach($orderTotal as $total){ ?>
                                        <tr class="border-td">
                                            <td  width="160px" class="table-name"><?= $total->title ?></td>
                                            <td><span class="float-right"><?= $model->currency_code ?> <?= $format->format($total->value,['decimal',2]) ?></span></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                <p class="Deliver-info">Products in this order<span class="orderdate"><?= $countProducts.' ' ?><?= $countProducts==1 ? 'item' : 'items' ?></span></p>
                                <div class="col-md-12 order-img">
                                <?php foreach($orderProducts as $orderProduct){ ?>
                                    <div class="row">
                                        <div class="col-md-2 col-3 p-0 ">
                                            <div class="img-dish-table"> <img alt="<?= $orderProduct->name  ?>" title="<?= $orderProduct->name  ?>" src="<?= $orderProduct->product->mainImage ?>" class="img-thumbnail rounded-circle"></div>
                                        </div>
                                        <div class="col-md-10 col-9">
                                            <div class="cake-item mt-2">
                                                <p class="my-1"><?= $orderProduct->name  ?></p>
                                                <p class="my-1"><?= $model->currency_code ?> <?= $format->format($orderProduct->price,['decimal',2]) ?></p>
                                                <p class="my-1">Qty <?= $orderProduct->quantity ?></p>
                                            </div>
                                            <?php if(!empty($options)){
                                                foreach ($options as $k=>$option) {
                                            ?>
                                                    <span><b><?= $k ?>:</b> <?= implode(', ', $option) ?></span><br/>
                                            <?php
                                                }
                                            }?>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>
                            </div>
                            <?php
                                if(empty($order_rating) && $model->order_status_id==5)
                                    echo $this->render('rating_form',['model' => $ratingModel]);
                                elseif(!empty($order_rating) && $model->order_status_id==5)
                                    echo $this->render('rating_view',['model' => $order_rating]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
