<?php
use yii\bootstrap4\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\rating\StarRating;

?>
<?php $form = ActiveForm::begin(); ?>
    <hr/>
    <div class="feedback-star">
        <h5>Rate this order</h5>
        <div class="feedbackittle col-sm-6">Taste Rating</div>
        <div class="col-sm-6 float-right text-right" style="margin-top: -10px;">
            <?= $form->field($model, 'taste_rating')->widget(StarRating::classname(), [
                'pluginOptions' => ['size'=>'xs','showCaption'=>false,'step' => 1,'disabled'=>true]
            ])->label(false);
            ?>
        </div>
        <div class="w-100">&nbsp;</div>

        <div class="feedbackittle col-sm-6">Presentation</div>
        <div class="col-sm-6 float-right text-right" style="margin-top: -10px;">
            <?= $form->field($model, 'presentation_rating')->widget(StarRating::classname(), [
                'pluginOptions' => ['size'=>'xs','showCaption'=>false,'step' => 1,'disabled'=>true]
            ])->label(false);
            ?>
        </div>
        <div class="w-100">&nbsp;</div>

        <div class="feedbackittle col-sm-6">Packing</div>
        <div class="col-sm-6 float-right text-right" style="margin-top: -10px;">
            <?= $form->field($model, 'packing_rating')->widget(StarRating::classname(), [
                'pluginOptions' => ['size'=>'xs','showCaption'=>false,'step' => 1,'disabled'=>true]
            ])->label(false);
            ?>
        </div>
        <div class="w-100">&nbsp;</div>

        <div class="feedbackittle col-sm-6">Overall Experience</div>
        <div class="col-sm-6 float-right text-right" style="margin-top: -10px;">
            <?= $form->field($model, 'overall_rating')->widget(StarRating::classname(), [
                'pluginOptions' => ['size'=>'xs','showCaption'=>false,'step' => 1,'disabled'=>true]
            ])->label(false);
            ?>
        </div>
        <div class="w-100">&nbsp;</div>

        <div class="feedbackittle col-sm-6">Comments</div>
        <div class="col-sm-6 float-right text-right" style="margin-top: -10px;">
            <?= $model->review  ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>