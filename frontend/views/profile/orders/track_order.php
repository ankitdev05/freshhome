<?php
use common\models\Library;

$this->title = 'Track order - '.$model->order_id.' | '.config_name;
$time_stamp = $model->orderTime;
if(empty($time_stamp))
    $time_stamp = $model->created_at*(86400*5);

$width = 0;
$step1 = $step2 = $step3 = $step4 = $step5 = '';
if($model->order_status_id==11){
    $step1 = $step2 = 'process1';
}
elseif($model->order_status_id==4)
    $step1 = $step2 = $step3 = 'process1';
elseif($model->order_accepted==1 && !empty($model->driver_id) && $model->order_status_id!=5)
    $step1 = $step2 = $step3 = $step4 = 'process1';
elseif($model->order_status_id==5)
    $step1 = $step2 = $step3 = $step4 = $step5= 'process1';

?>
<section class="address-list">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                    <div class="profile-side">
                        <?= $this->render('//profile/_left_panel') ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
					<div class="content">
						<div class="ordertracking">
							<h2>Order No :  #<?= $model->order_id ?></h2>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6 col-12">
									<div class="content2-header1">
										<p>Status : <span><?= $model->orderStatus->name ?></span></p>
									</div>
								</div>
								<div class="col-md-6 col-12">
									<div class="content2-header1">
										<p>Expected Date : <span><?= date('d F Y',$time_stamp) ?></span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="content3">
							<div class="shipment">
								<div class="confirm">
									<div class="imgcircle processing process1">
										<img src="./img/confirm.png" class="img-fluid" alt="confirm order">
									</div>
									<span class="line processing process1"></span>
									<p>Order Place</p>
								</div>
								<div class="process">
									<div class="imgcircle processing process2 <?= $step2 ?>">
										<img src="./img/quality.png" class="img-fluid" alt="process order">
									</div>
									<span class="line processing process2 <?= $step3 ?>"></span>
									<p>Confirmed</p>
								</div>
								<div class="quality">
									<div class="imgcircle processing process3 <?= $step3 ?>">
										<img src="./img/process.png" class="img-fluid" alt="quality check">
									</div>
									<span class="line processing process3 <?= $step4 ?>"></span>
									<p>Processing</p>
								</div>
								<div class="dispatch">
									<div class="imgcircle processing process4 <?= $step4 ?>">
										<img src="./img/dispatch.png" class="img-fluid" alt="dispatch product">
									</div>
									<span class="line processing process4 <?= $step5 ?>"></span>
									<p>Out For Delivery</p>
								</div>
								<div class="delivery">
									<div class="imgcircle processing process5 <?= $step5 ?>">
										<img src="./img/delivery.png" class="img-fluid" alt="delivery">
									</div>
									<p>Delivery</p>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>
</section>
