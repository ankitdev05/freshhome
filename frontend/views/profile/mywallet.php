<?php
use yii\helpers\Html;
use common\models\Library;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = 'My wallet | '.config_name;
$session = Yii::$app->session;
$is_supplier = $session->has('is_supplier');
$Library = new Library();
?>
<section class="address-list common-color">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                    <div class="profile-side">
                        <?php
                        if($is_supplier)
                            echo $this->render('//supplier/_left_panel');
                        else
                            echo $this->render('//profile/_left_panel');
                        ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-9">
                    <div class="profile-body">
                        <div class="address-list-block">
                            <div class="aed">
                                <h2><?= $Library->currencyFormat($user_info->balance,config_default_currency) ?></h2>
                                <p><?= Yii::t('app','Your Current Balance') ?></p>
                            </div>
                            <div class="row">

                                <div class="col-sm-12">
                                    <p class="transaction-history-list text-left mb-1"><?= Yii::t('app','Transaction History') ?></p>
                                </div>
                                
                                <div class="col-md-12 w-100"><hr/></div>
                            </div>
                            <?php Pjax::begin(); ?>
                            <?=  ListView::widget([
                                'layout' => "<div class='col-sm-12'>{summary}</div>\n {items}\n<div class='row col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
                                'dataProvider' => $dataProvider,
                                'itemView' => '_wallet',
                                'summary' => false,
                                'options' => [
                                    'tag' => 'div',
                                    'class' => 'wallettranscation float-left w-100',
                                    'id' => 'list-wrapper',
                                ],
                                'itemOptions' => [
                                    'tag' => false
                                ],
                                'pager' => [
                                    'options' => ['class'=>'pagination justify-content-end'],
                                    'prevPageLabel' =>'Previous',
                                    'nextPageLabel' =>'Next',
                                    'pageCssClass' => 'page-item',
                                    'disabledPageCssClass' => 'page-link disabled',
                                    'linkOptions' => ['class' => 'page-link'],
                                ]
                            ]); ?>
                            <?php Pjax::end(); ?>
                            <div class="Historybtn text-center">
                                <button type="button" class="btn btn custom-btn" data-toggle="modal" data-target="#bankAccount"><?= Yii::t('app','Link your bank account') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$rurl = Yii::$app->request->baseUrl.'/profile/mywallet?type=mywallet';

?>
<div class="modal fade pr-2" id="bankAccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Enter bank details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->render('//profile/_new_bank',['model'=>$user_account]) ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade pr-2" id="selectPayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Enter card info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->render('//profile/addwallet',['credit_card_model'=>$credit_card_model,'model'=>$model,'rurl' => $rurl,'cards'=>$cards]) ?>
            </div>
        </div>
    </div>
</div>
<?php
$rurl = Yii::$app->request->baseUrl.'/profile?type=addwallet';

?>
<div class="modal fade pr-2" id="addCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Enter card info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->render('//profile/card/_new_card',['model'=>$credit_card_model,'rurl' => $rurl]) ?>
            </div>
        </div>
    </div>
</div>