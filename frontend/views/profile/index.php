<?php
use yii\bootstrap4\Html;
use kartik\file\FileInput;
use common\models\Country;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\Occupation;
use yii\bootstrap4\ActiveForm;
use himiklab\thumbnail\EasyThumbnailImage;

$this->title = 'Profile | '.config_name;
$this->params['breadcrumbs'][] = 'Profile';

$language_id = 1;
$all_occupations = ArrayHelper::map(Occupation::find()->joinWith('occupationName as cn')->where(['cn.language_id'=>$language_id])->asArray()->all(), 'id', 'occupationName.occupation_name');
$all_nationality = ArrayHelper::map(Country::find()->asArray()->all(),'code','nationality');

$imageSrc =  [];
$del_image = [['url'=>yii::$app->request->baseUrl.'/profile/del-profile-pic']];
if( !empty($model->profile_pic)){
    $image=EasyThumbnailImage::thumbnailFileUrl(
        "../../frontend/web/uploads/".$model->profile_pic,200,200,EasyThumbnailImage::THUMBNAIL_INSET
    );
    $imageSrc	= [Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Profile Picture', 'title'=>'Profile Picture'])];
}
$type	= '';
if(isset($_GET['type']) && !empty($_GET['type']))
    $type	= $_GET['type'];
?>
<div class="row">
    <div class="col-sm-4">
        <?= $this->render('//profile/_left_panel',['type'=>$type]) ?>
    </div>

    <div class="col-sm-8">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' => 'profile-info']); ?>
        <div class="row">
            <div class="col-sm-12">
                <?=
                $form->field($model, 'profile_pic')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' =>[
                        'initialPreview'=>$imageSrc,
                        'initialPreviewConfig'=>$del_image
                    ]
                ])->label(false);
                ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'gender')->widget(Select2::classname(), [
                    'data' =>['male'=>'Male','female'=>'Female'],
                    'options' => ['placeholder' => 'Please select ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'dob')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Enter birth date ...','readonly'=>true],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'occupation_id')->widget(Select2::classname(), [
                    'data' =>$all_occupations,
                    'options' => ['placeholder' => 'Please select ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'nationality')->widget(Select2::classname(), [
                    'data' =>$all_nationality,
                    'options' => ['placeholder' => 'Please select ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]) ?>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'description')->textArea(['rows'=>3]) ?>
            </div>
            <div class="form-group col-sm-12">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>