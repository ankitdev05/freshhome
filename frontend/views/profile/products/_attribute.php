<?php
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\AttributeGroup;

$attr_groups = ArrayHelper::map(AttributeGroup::find()->where(['status' => 1])->asArray()->all(),'attribute_id','name');
?>
<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'attribute_id')->widget(Select2::classname(), [
            'data' => $attr_groups,
            'options' => [
                'prompt' => 'Select Attribute'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-sm-12 attr_html"></div>
</div>
<?php
if(!empty($model->id)){
    $this->registerJs("
    var attr_id = $('#menu-attribute_id').val();
    $.ajax({
        url : website_url+'profile/product-attribute',
        type : 'POST',
        data : 'attr_id='+attr_id+'&menu_id='+$model->id,
        dataType : 'json',
        success : function(json){
            $('.attr_html').html(json['html']); 
        }
    });");
}
$this->registerJs("
    $('#menu-attribute_id').on('change',function(){
        $('.attr_html').html('');
        var attr_id = $(this).val();
        $.ajax({
            url : website_url+'profile/product-attribute',
            type : 'POST',
            data : 'attr_id='+attr_id,
            dataType : 'json',
            success : function(json){
                $('.attr_html').html(json['html']); 
            }
        });
    })
");