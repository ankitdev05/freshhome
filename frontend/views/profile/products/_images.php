<?php
use dosamigos\fileupload\FileUploadUI;
use himiklab\thumbnail\EasyThumbnailImage;
use common\models\Library;

$files= [];
$upload_url	= Yii::$app->request->baseUrl.'/profile/upload';
if(!empty($model->id)){
    $library = new Library();
    $upload_url	= Yii::$app->request->baseUrl.'/profile/pupload?id='.$model->id;
    $product_images = $model->images;
    if(!empty($product_images)){
        foreach($product_images as $product_image){
            $image_info = $library->json_decodeArray($product_image->file_info);
            $thumb=EasyThumbnailImage::thumbnailFileUrl(
                "../../frontend/web/uploads/".$product_image->image_name,100,100,EasyThumbnailImage::THUMBNAIL_INSET
            );

            $files[]   = [
                'name'=>$image_info['name'],
                'size'=>$image_info['size'],
                'url'=>$thumb,
                'thumbnailUrl'=>$thumb,
                'deleteUrl'=>Yii::$app->request->baseUrl.'/profile/pdel_timage?id='.$product_image->image_id,
                'deleteType'=>'POST'
            ];
        }
    }
}
?>
<div class="row">
    <div class="col-sm-12">
        <?= FileUploadUI::widget([
            'model' => $model,
            'attribute' => 'product_images',
            'fieldOptions' => [
                'accept' => 'image/*'
            ],
            'clientOptions' => [
                'autoUpload' => true,
            ],
            'url' => $upload_url,
        ]); ?>
    </div>
</div>
<?php

if(!empty($files)){
    $this->registerJs('
    var $form = $(\'#menu-product_images-fileupload\');
    $form.fileupload(\'option\', \'done\').call($form, $.Event(\'done\'), {result: {files: '.json_encode($files).'}});
');
}