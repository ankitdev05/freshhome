<?php
use yii\bootstrap4\Html;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\tree\TreeViewInput;
use himiklab\thumbnail\EasyThumbnailImage;

$imageSrc = $delUrl = '';

if(!empty($model->dish_image)){
    $image=EasyThumbnailImage::thumbnailFileUrl(
        "../../frontend/web/uploads/".$model->dish_image,200,200,EasyThumbnailImage::THUMBNAIL_INSET
    );
    $imageSrc = [Html::img($image, ['class'=>'file-preview-image', 'alt'=>$model->dish_name, 'title'=>$model->dish_name])];
    $delUrl=[['caption'=>$model->dish_name,'url'=>yii::$app->request->baseUrl.'/profile/deletemenuimage?id='.$model->id]];
}

$categories = $model->menuCategories;
if(!empty($categories)){
    $category = [];
    if(!empty($categories)){
        foreach($categories  as $cat)
            $category[]= $cat->category_id;
        $model->category = implode(',',$category);
    }
}
?>
<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, 'product_price')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($model, 'dish_image')->widget(FileInput::classname(),
            [
                'options'=>['accept'=>'image/*'],
                'pluginOptions'=>
                    [
                        'allowedFileExtensions'=>['jpg','gif','png'],
                        'overwriteInitial'=>true,
                        'initialPreview'=>$imageSrc,
                        'initialPreviewConfig'=>$delUrl
                    ],
            ]) ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($model, 'product_description')->textArea(['rows' => 3]) ?>
    </div>
    <div class="col-sm-12">
        <?=
        $form->field($model, 'category')->widget(TreeViewInput::classname(),[
            'query' => \common\models\Category::find()->where(['active' => 1,'cat_id' => $user_info->supplier_type])->addOrderBy('root, lft'),
            'headingOptions' => ['label' => 'Categories'],
            'rootOptions' => ['label'=>'<i class="fas fa-tree text-success"></i>'],
            'fontAwesome' => true,
            'asDropdown' => true,
            'multiple' => true,
            'model' =>$model,
            'attribute' =>'category',
            'options' => ['disabled' => false]
        ]);

        ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'status')->widget(Select2::classname(), [
            'data' => ['active'=>'Active','inactive'=>'In active'],
            //'options' => ['placeholder' => 'Please select ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'product_qty')->textInput() ?>
    </div>
</div>
