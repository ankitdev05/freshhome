<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Tabs;
use yii\bootstrap4\ActiveForm;

$user_info = Yii::$app->user->identity;
?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<?php
echo Tabs::widget([
        'items' => [
            [
              'label' => 'Product Information',
              'content' => '<br/>'.$this->render('_general',['form' => $form,'model' => $model,'user_info' => $user_info]),
              'active' => true
            ],
            [
              'label' => 'Images',
              'content' => '<br/>'.$this->render('_images',['form' => $form,'model' => $model,'user_info' => $user_info]),
            ],
            [
                'label' => 'Product Attributes',
                'content' => '<br/>'.$this->render('_attribute',['form' => $form,'model' => $model,'user_info' => $user_info]),
            ],
        ],
]);
?>
<div class="row">
    <div class="form-group col-sm-12">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Save') : Yii::t('yii', 'Save'), ['class' => $model->isNewRecord ? 'btn custom-btn' : 'btn custom-btn']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php
$this->registerCss('.fade.in {opacity: 1}');

