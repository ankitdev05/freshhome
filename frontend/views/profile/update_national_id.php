<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use common\models\Library;
use kartik\file\FileInput;

$this->title = 'Update national ID | '.config_name;
$session = Yii::$app->session;
$is_supplier = $session->has('is_supplier');
$Library = new Library();
?>
<section class="address-list common-color">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                    <div class="profile-side">
                        <?php
                        if($is_supplier)
                            echo $this->render('//supplier/_left_panel');
                        else
                            echo $this->render('//profile/_left_panel');
                        ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-9">
                    <div class="profile-body">
                        <div class="address-list-block">
                            <?php if(!empty($check_national_id) && $model->national_pic_approval==1): ?>
                                <h5>Your national ID verification is under process.</h5>
                            <?php else: ?>

                            <p>To activate your wallet please provide national ID</p>
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $form->field($model, 'national_pic')->widget(FileInput::classname(),
                                        [
                                            'options'=>['accept'=>'image/*'],
                                            'pluginOptions'=>
                                                [
                                                    'allowedFileExtensions'=>['jpg','gif','png'],
                                                    'showPreview' => false,
                                                    'showCaption' => true,
                                                    'showRemove' => true,
                                                    'showUpload' => false
                                                ],
                                        ])->label('National ID') ?>
                                </div>
                                <div class="col-sm-1">
                                    <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Save') : Yii::t('yii', 'Save'), ['class' => $model->isNewRecord ? 'btn custom-btn' : 'btn custom-btn']) ?>
                                </div>
                            </div>

                            <?php ActiveForm::end(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
