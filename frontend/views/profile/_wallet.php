<?php
use common\models\Library;
$Library = new Library();
?>
<div class="list-history-aed">
	<div class="row">
		<div class="col-md-10 col-8">
			<h4><?= $Library->currencyFormat($model->amount,$model->currency) ?> <?= $model->message ?></h4>
			<p class="mb-1"><?php echo Yii::$app->formatter->format($model->created_at, 'relativeTime') ?>
			</p>
		</div>
		<div class="col-md-2 col-4 text-center">
            <?php if($model->amount>0){ ?>
			    <p class="credithead"><?= Yii::t('app','Cash In') ?></p>
			    <i class="fas fa-long-arrow-alt-up creditarrow"></i>
            <?php }else{ ?>
			    <i class="fas fa-long-arrow-alt-down debitarrow"></i>
			    <p class="debithead"><?= Yii::t('app','Cash Out') ?></p>
            <?php } ?>
		</div>
	</div>
</div>