<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FAR;
use rmrevin\yii\fontawesome\FAS;

$type	= '';
if(isset($_GET['type']) && !empty($_GET['type']))
    $type	= $_GET['type'];
if($type=='vieworder' || $type=='completedorders')
    $type = 'myorders';
$user_info = Yii::$app->user->identity;
?>
<ul>
    <li>
        <h5><?= Yii::t('app', 'Account') ?></h5>
        <?= Html::tag('p',Html::a(Yii::t('app', 'Profile'),Url::to(['/profile'],true),['class'=>$type=='' ? 'active' : ''])) ?>
        <?= Html::tag('p',Html::a(Yii::t('app', 'Your Address'),Url::to(['/profile','type'=>'address'],true),['class'=>$type=='address' ? 'active' : ''])) ?>
        <?= Html::tag('p',Html::a(Yii::t('app', 'Invite Friends'),Url::to(['/profile','type'=>'invite'],true),['class'=>$type=='invite' ? 'active' : ''])) ?>
    </li>
    <li>
        <h5><?= Yii::t('app', 'Credits') ?></h5>
        <?php Html::tag('p',Html::a(Yii::t('app', 'Change Email'),Url::to(['/profile','type'=>'changeemail'],true),['class'=>$type=='changeemail' ? 'active' : ''])) ?>
        <?php Html::tag('p',Html::a('Change Mobile number',Url::to(['/profile','type'=>'changemobile'],true),['class'=>$type=='changemobile' ? 'active' : ''])) ?>
        <?php Html::tag('p',Html::a('Change Password',Url::to(['/profile','type'=>'changepassword'],true),['class'=>$type=='changepassword' ? 'active' : ''])) ?>
        <?= Html::tag('p',Html::a(Yii::t('app', 'My Wallet'),Url::to(['/profile/mywallet','type'=>'mywallet'],true),['class'=>$type=='mywallet' ? 'active' : ''])) ?>
    </li>
    <li>
        <h5><?= Yii::t('app', 'Order Details') ?></h5>
        <?= Html::tag('p',Html::a(Yii::t('app', 'Order & Returns'),Url::to(['/profile','type'=>'myorders'],true),['class'=>$type=='myorders' ? 'active' : ''])) ?>
    <?php if($user_info->supplier_type==1){ ?>

        <?php Html::tag('p',Html::a('My menu',Url::to(['/profile','type'=>'listmenu'],true),['class'=>$type=='listmenu' ? 'active' : ''])) ?>
        <?php Html::a(Html::tag('li',FAS::icon('utensils').' My Kitchen',['class'=>$type=='mykitchen' ? 'list-group-item active' : 'list-group-item','style'=>'margin-bottom:7px;']),Url::to(['/profile','type'=>'mykitchen'],true)) ?>

    <?php }elseif(!empty($user_info->supplier_type)){ ?>
        <?php Html::tag('p',Html::a('My products',Url::to(['/profile','type'=>'myProducts'],true),['class'=>($type=='myProducts' || $type=='addProduct') ? 'active' : ''])) ?>
    <?php } ?>
    </li>
</ul>
