<?php
use yii\widgets\ListView;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;

$this->title = 'Address | '.config_name;
$this->params['breadcrumbs'][] = 'Address';

$type	= '';
if(isset($_GET['type']) && !empty($_GET['type']))
    $type	= $_GET['type'];
?>
<section class="address-list">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <div class="profile-side">
                        <?= $this->render('//profile/_left_panel',['type'=>$type]) ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <div class="profile-body">
                        <div class="address-list-block">
                            <?php Pjax::begin(); ?>
                            <?=
                                ListView::widget([
                                    'dataProvider' => $dataProvider,
                                    'layout' => "{summary} \n {items}\n<div class='col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
                                    'itemView' => '_address',
                                    'options' => [
                                        'tag' => 'div',
                                        'class' => '',
                                        'id' => 'list-wrapper',
                                    ],
                                    'itemOptions' => [
                                        'tag' => false
                                    ],
                                    'pager' => [
                                        'options' => ['class'=>'pagination justify-content-end'],
                                        'prevPageLabel' =>'Previous',
                                        'nextPageLabel' =>'Next',
                                        'pageCssClass' => 'page-item',
                                        'disabledPageCssClass' => 'page-link disabled',
                                        'linkOptions' => ['class' => 'page-link'],
                                    ]
                                ]);
                            ?>
                            <?php Pjax::end(); ?>
                            <div class="form-group" >

                                <?= Html::a('Add New Address',['/profile','type'=>'address','action'=>'create_account'],['class'=>'btn btn custom-btn']) ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
