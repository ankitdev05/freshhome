<?php
use yii\helpers\Url;
use yii\helpers\Html;

$title = 'Mark as Featured address';
$icon   = 'far';
$update = 1;
if($model->is_default=="yes") {
    $update = 0;
    $icon   = 'fas text-danger';
    $title = 'Remove from featured list';
}
?>
<div class="innerlist-blcok">
    <p>
        <span class="edit-list">
            <?=
                Html::a(Html::tag('i','',['class'=>'far fa-edit']),Url::to(['/profile','type'=>'address','action'=>'edit_address','id'=>$model->address_id],true), ['data-pjax' => 0,'alt'=>'Edit','title'=>'Edit','class'=>'btn btn-light']);
            ?>
            <?=
             Html::a(Html::tag('i','',['class'=>$icon.' fa-star']),Url::to(['/profile','type'=>'address','action'=>'default_addr','id'=>$model->address_id]), ['data-pjax' => 0,'alt'=>$title,'title'=>$title,'class'=>'btn btn-light']);
            ?>
            <?= Html::a(Html::tag('i','',['class'=>'far fa-trash-alt']),Url::to(['/profile','type'=>'address','action'=>'delete_address','id'=>$model->address_id],true), ['data-pjax' => 0,'alt'=>'Delete','title'=>'Delete','class'=>'btn btn-light','data-confirm'=>'Are you sure want to delete this record?']); ?>
        </span>
        <?= ucwords($model->title) ?><br/> <?= $model->location ?>
    </p>
</div>