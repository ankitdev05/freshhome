<?php
use yii\widgets\Pjax;
use yii\widgets\ListView;
$this->title = $result->brandDescription->brand_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bg-sec">
    <div class="container">
        <div class="heading">
            <h3><?= $this->title ?></h3>
            <div class="divider"></div>
        </div>
    </div>
</div>
<section class="product">
    <div class="container">
        <?php Pjax::begin(); ?>
        <?=  ListView::widget([
            'layout' => "<div class='col-sm-12'>{summary}</div>\n {items}\n<div class='col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
            'dataProvider' => $dataProvider,
            'itemView' => '//products/_view_product',
            'options' => [
                'tag' => 'div',
                'class' => 'row',
                'id' => 'list-wrapper',
            ],
            'itemOptions' => [
                'tag' => false
            ],
            'pager' => [
                'options' => ['class'=>'pagination justify-content-end'],
                'prevPageLabel' =>'Previous',
                'nextPageLabel' =>'Next',
                'pageCssClass' => 'page-item',
                'disabledPageCssClass' => 'page-link disabled',
                'linkOptions' => ['class' => 'page-link'],
            ]
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</section>
