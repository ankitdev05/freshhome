<div class="col-md-3 col-6">
    <a href="<?= $model->categoryUrl ?>">
    <div class="categorylisting mb-3">
        <img src="<?= $model->getProductImage(251,200) ?>" class="img-fluid">
        <div class="category-name">
            <h5><?= $model->categoryDescription->category_name ?></h5>
        </div>
    </div>
    </a>
</div>