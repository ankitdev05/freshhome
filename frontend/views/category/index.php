<?php
use yii\widgets\ListView;
use common\models\Category;
use common\models\HomeCategory;

$this->title = $result->categoryDescription->category_name;
$home_categories = HomeCategory::find()->select('*')->joinWith('homeCategoryDescription as hd')->where(['status' => 1,'hd.language_id' => language_id])->andWhere(['not IN','tbl_home_category.home_category_id',[29,30]])->asArray()->all();
?>
<section class="product-filter-section categorypage pb-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 mt-3">
                <div class="product-filter mt-3">
                    <p class="mb-1"><?= Yii::t('app','Filter By') ?>:</p>
                    <?php
                        if(!empty($home_categories)):
                            foreach ($home_categories as $home_category):
                                $categories = Category::find()->select('*')->joinWith('categoryDescription as cd')->where(['and',['home_category_id' => $home_category['home_category_id']],['IS','parent_id',NULL],['active' => 1],['cd.language_id' => language_id]])->all();
                    ?>
                        <h5><?= $home_category['home_category_name'] ?></h5>
                            <?php if(!empty($categories)): ?>
                                <ul class="cat-filter">
                                    <?php foreach ($categories as $category): ?>
                                        <li><a href="<?= $category->categoryUrl ?>"><?= $category->categoryDescription->category_name ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif;
                    endforeach;
                        endif;
                    ?>

                </div>
            </div>
            <div class="col-md-10">
                <div class="productlist categorylist mt-4">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="mt-1"><?= $result->categoryDescription->category_name; ?></h5>
                        </div>
                        <div class="col-md-6 col-8">
                            <p><?= Yii::t('app','Shop By Category') ?></p>
                        </div>
                        <div class="col-md-6 col-4">

                        </div>
                    </div>
                </div>
                <div class="latest-products best-selling-product allproductlisting mt-3">
                    <?=  ListView::widget([
                        'layout' => "<div class='col-sm-12'>{summary}</div>\n {items}\n<div class='col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
                        'summary' => false,
                        'itemView' => '_category',
                        'dataProvider' => $dataProvider,
                        'options' => [
                            'tag' => 'div',
                            'class' => 'row',
                            'id' => '_category',
                        ],
                        'itemOptions' => [
                            'tag' => false
                        ],
                        'pager' => [
                            'options' => ['class'=>'pagination justify-content-center'],
                            'prevPageLabel' =>'<i class="fas fa-angle-double-left"></i>',
                            'nextPageLabel' =>'<i class="fas fa-angle-double-right"></i>',
                            'pageCssClass' => 'page-item',
                            'disabledPageCssClass' => 'page-link disabled',
                            'linkOptions' => ['class' => 'page-link'],
                        ]
                    ]); ?>
                </div>

                <div class="productlist categorylist mt-2">
                    <div class="row">
                        <div class="col-md-6 col-8">
                            <p><?= Yii::t('app','Shop By Supplier') ?></p>
                        </div>
                        <div class="col-md-6 col-4">
                            <p class="text-right"></p>
                        </div>
                    </div>
                </div>
                <div class="latest-products best-selling-product allproductlisting mt-3">
                    <?=  ListView::widget([
                        'layout' => "<div class='col-sm-12'>{summary}</div>\n {items}\n<div class='col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
                        'summary' => false,
                        'itemView' => '_supplier',
                        'dataProvider' => $supplierDataProvider,
                        'options' => [
                            'tag' => 'div',
                            'class' => 'row',
                            'id' => '_category',
                        ],
                        'itemOptions' => [
                            'tag' => false
                        ],
                        'pager' => [
                            'options' => ['class'=>'pagination justify-content-center'],
                            'prevPageLabel' =>'<i class="fas fa-angle-double-left"></i>',
                            'nextPageLabel' =>'<i class="fas fa-angle-double-right"></i>',
                            'pageCssClass' => 'page-item',
                            'disabledPageCssClass' => 'page-link disabled',
                            'linkOptions' => ['class' => 'page-link'],
                        ]
                    ]); ?>
                </div>

                <div class="productlist categorylist mt-2">
                    <div class="row">
                        <div class="col-md-6 col-8">
                            <p><?= Yii::t('app','Shop By Brands') ?></p>
                        </div>
                        <div class="col-md-6 col-4">
                            <p class="text-right"></p>
                        </div>
                    </div>
                </div>
                <div class="latest-products best-selling-product allproductlisting mt-3">
                    <?=  ListView::widget([
                        'layout' => "<div class='col-sm-12'>{summary}</div>\n {items}\n<div class='col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
                        'summary' => false,
                        'itemView' => '_brand',
                        'dataProvider' => $brandDataProvider,
                        'options' => [
                            'tag' => 'div',
                            'class' => 'row',
                            'id' => '_category',
                        ],
                        'itemOptions' => [
                            'tag' => false
                        ],
                        'pager' => [
                            'options' => ['class'=>'pagination justify-content-center'],
                            'prevPageLabel' =>'<i class="fas fa-angle-double-left"></i>',
                            'nextPageLabel' =>'<i class="fas fa-angle-double-right"></i>',
                            'pageCssClass' => 'page-item',
                            'disabledPageCssClass' => 'page-link disabled',
                            'linkOptions' => ['class' => 'page-link'],
                        ]
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>
