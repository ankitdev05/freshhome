<div class="col-md-3 col-6 mb-3">
    <div class="brandlisting mb-3">
        <a href="<?= Yii::$app->request->baseUrl.'/brand?brand_id='.$model->brand_id ?>">
            <img src="<?= $model->brand->getBrandImage(251,200) ?>" class="img-fluid">
            <div class="category-name">
                <h5><?= $model->brand->brandDescription->brand_name ?></h5>
            </div>
        </a>
    </div>
</div>