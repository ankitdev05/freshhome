<?php
$supplier_info = $model->menu->supplierInfo;

?>
<div class="col-md-2 col-6">
    <a href="<?= $supplier_info->profileUrl ?>">
    <div class="supplierlisting mb-3">
        <img src="<?= $supplier_info->getSuppImage(251,200) ?>" class="img-fluid">
        <h5><?= $supplier_info->name ?></h5>
    </div>
    </a>
</div>