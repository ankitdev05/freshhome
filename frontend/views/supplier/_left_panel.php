<?php
use yii\helpers\Url;
use yii\bootstrap4\Html;

$type	= '';
if(isset($_GET['type']) && !empty($_GET['type']))
    $type	= $_GET['type'];
$action     = $this->context->action->id;
if($action=='updatemenu' || $action=='addmenu')
    $type = 'listmenu';
if($type=='addProduct' || $type=='updateProduct')
    $type = 'myProducts';
$user_info = yii::$app->user->identity;
$supplier_type = $user_info->supplier_type;
?>
<ul>
    <li>
        <h5>Account</h5>
        <?= Html::tag('p',Html::a(Yii::t('app', 'Profile'),Url::to(['/supplier'],true),['class'=>$type=='' ? 'active' : ''])) ?>
        <?php if($supplier_type==1){ ?>
            <?= Html::tag('p',Html::a(Yii::t('app', 'My Menu'),Url::to(['/supplier','type' => 'listmenu'],true),['class'=>$type=='listmenu' ? 'active' : ''])) ?>
            
            <?= Html::tag('p',Html::a(Yii::t('app', 'My Kitchen'),Url::to(['/supplier','type' => 'mykitchen'],true),['class'=>$type=='mykitchen' ? 'active' : ''])) ?>

        <?php }else{ ?>
            <?= Html::tag('p',Html::a(Yii::t('app', 'My Products'),Url::to(['/supplier','type' => 'myProducts'],true),['class'=>$type=='myProducts' ? 'active' : ''])) ?>
            <?= Html::tag('p',Html::a(Yii::t('app', 'My Orders'),Url::to(['/supplier','type' => 'myorders'],true),['class'=>$type=='myorders' ? 'active' : ''])) ?>
        <?php } ?>
        <?= Html::tag('p',Html::a(Yii::t('app', 'My Balance'),Url::to(['/profile/mywallet','type' => 'mywallet'],true),['class'=>$type=='mywallet' ? 'active' : ''])) ?>
        <?= Html::tag('p',Html::a(Yii::t('app', 'My Plan'),Url::to(['/supplier/subscription','type' => 'subscription'],true),['class'=>$type=='subscription' ? 'active' : ''])) ?>
        <?= Html::tag('p',Html::a(Yii::t('app', 'Invite Friends'),Url::to(['/supplier','type' => 'invite'],true),['class'=>$type=='invite' ? 'active' : ''])) ?>
        <?= Html::tag('p',Html::a(Yii::t('app', 'My Sales Report'),Url::to(['/supplier','type' => 'salesreport'],true),['class'=>$type=='salesreport' ? 'active' : ''])) ?>
    </li>
</ul>
