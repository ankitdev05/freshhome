<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
$this->title = 'Subscription | '.config_name;
$user_plans = [];
$my_cards = [];
foreach ($plans as $plan){
    $user_plans[$plan->plan_id] = $plan->plan_name.' for '.$plan->months.' months at '.config_default_currency.' '.$plan->price;
}
if(!empty($cards)){
    foreach($cards as $card)
        $my_cards[$card['id']] = $card['masked_num'] .'('.$card['card_type'].')';
}

?>
<section class="address-list common-color">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                    <div class="profile-side">
                        <?= $this->render('//supplier/_left_panel') ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                    <div class="profile-body">
                        <div class="address-list-block">
                            <?php if(isset($subscription_status['status']) && $subscription_status['status']=='active'){ ?>
                            <p>Your Subscription will be expired on <?= date('d F Y',$subscription_status['subscription_end']) ?>.</p>
                            <?php } ?>
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
							<div class="col-md-6 mx-auto mb-4">
								<div class="selectplan">
									<h5 class="text-center">Select Plan</h5>
                                    <div class="col-sm-12">
                                        <?= $form->field($model, 'select_plan')->radioList($user_plans)->label(false) ?>
                                    </div>
								</div>
							</div>

							
                            <div class="col-sm-12">
                                <?= Html::submitButton( Yii::t('app', 'Save'), ['name'=>'add-wallet','class' => 'btn custom-btn']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<?php
$rurl = Yii::$app->request->baseUrl.'/supplier/subscription?type=subscription';
$this->registerCss('
.invalid-feedback{
    display:block !important;
}
');
?>
<div class="modal fade" id="addCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Enter card info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->render('//profile/card/_new_card',['model'=>$credit_card_model,'rurl' => $rurl]) ?>
            </div>
        </div>
    </div>
</div>