<?php
$this->title = 'Profile | '.config_name;
?>
<section class="address-list common-color">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                    <div class="profile-side">
                        <?= $this->render('_left_panel') ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                    <div class="profile-body">
                        <div class="address-list-block">
                            <div class="row">
                                <div class="proile-account">
                                    <?php $model->scenario = 'supp_update_image'; ?>
                                    <?= $this->render('//profile/form/_image',['model' => $model]) ?>
                                </div>
                                <div class="col-md-12">
                                    <div class="userprofileaccount">
                                        <?php $model->scenario = 'supp_update_name'; ?>
                                        <?= $this->render('//profile/form/_name',['model' => $model]) ?>
                                    </div>
                                    <div class="userprofileaccount">
                                        <?php $model->scenario = 'supp_update_dob'; ?>
                                        <?= $this->render('//profile/form/_dob',['model' => $model]) ?>
                                    </div>

                                    <div class="userprofileaccount">
                                        <?php $model->scenario = 'supp_update_description'; ?>
                                        <?= $this->render('//profile/form/_about',['model' => $model]) ?>
                                    </div>

                                    <div class="userprofileaccount">
                                        <?php $model->scenario = 'supp_update_phone_number'; ?>
                                        <?= $this->render('//profile/form/_mobile',['model' => $model]) ?>
                                    </div>
                                    <div class="userprofileaccount">
                                        <?php $model->scenario = 'supp_update_email'; ?>
                                        <?= $this->render('//profile/form/_email',['model' => $model]) ?>
                                    </div>
                                    <div class="userprofileaccount">
                                        <?php $model->scenario = 'supp_update_nationality'; ?>
                                        <?= $this->render('//profile/form/_nationality',['model' => $model]) ?>
                                    </div>

                                    <div class="userprofileaccount">
                                        <?php $model->scenario = 'supp_update_city'; ?>
                                        <?= $this->render('//profile/form/_city',['model' => $model]) ?>
                                    </div>

                                    <div class="userprofileaccount">
                                        <?php $model->scenario = 'supp_update_building'; ?>
                                        <?= $this->render('//profile/form/_building',['model' => $model]) ?>
                                    </div>

                                    <div class="userprofileaccount">
                                        <?php $model->scenario = 'supp_update_floor'; ?>
                                        <?= $this->render('//profile/form/_floor',['model' => $model]) ?>
                                    </div>

                                    <div class="userprofileaccount">
                                        <?php $model->scenario = 'supp_update_flat'; ?>
                                        <?= $this->render('//profile/form/_flat',['model' => $model]) ?>
                                    </div>
                                    <div class="userprofileaccount">
                                        <?php $model->scenario = 'supp_update_location'; ?>
                                        <?= $this->render('//profile/form/_location',['model' => $model]) ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
