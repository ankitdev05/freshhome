<?php
use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use common\models\HomeCategory;

$url = Yii::$app->urlManager->createUrl(['supplier','type' => 'addProduct']);
if(isset($_GET['id']) && !empty($_GET['id'])){
   $url = Yii::$app->urlManager->createUrl(['supplier','type' => 'updateProduct','id' => $_GET['id']]);
}
$form = ActiveForm::begin(['action' => $url,'method' => 'GET','options' => ['enctype' => 'multipart/form-data']]);
echo $form->field($model,'home_category')->hiddenInput()->label(false);
echo $form->field($model,'category')->hiddenInput()->label(false);
$this->registerCss('
.scrollbar {
    float: left;
    height: 400px;
    width: 400px;
    background: #F5F5F5;
    overflow-y: scroll;
}
.active-tab{
    background: #f9f9f9;
}
');

$this->registerJs('
    function selectCategory(id,category_id=""){
        $(".sub_cats").each(function(){
            $(this).removeClass("active-tab")
        });
        
        $.ajax({
            url: "'.Yii::$app->request->baseUrl.'/supplier/get-categories",
            method: "GET",
            data : "home_category="+$("#menu-home_category").val()+"&category_id="+id+"&other_cats="+$("#menu-category").val()+"&category_idx="+category_id,
            dataType: "json",
            success : function(data){
                if(data["html"]!=""){
                    $("#table2").html(data["html"]);
                }
                $(".home_category1").remove();
                $("#cats_brdcrumbs").after(data["breadcrumbs"]);
                $("#menu-category").val(data["categories"]);
                $("#sForm").show();
                $("#selected_cat").html("Select "+data["cat_name"]);
                
                $("#home_category_id_"+$("#menu-home_category").val()).addClass("active-tab");
                 $(".home_cat"+id).addClass("active-tab");
            }
        });
    }
    
    function homeCategories(id){
        $("#sForm").hide();
        $(".home_category").each(function(){
            $(this).removeClass("active-tab")
        });
        $("#selected_cat").html("");
        
        $("#home_category_id_"+id).addClass("active-tab");
        $("#menu-home_category").val(id);
        $(".main_brd").remove();
        $.ajax({
            url: "'.Yii::$app->request->baseUrl.'/supplier/get-categories",
            method: "GET",
            data : "home_category="+id,
            dataType: "json",
            success : function(data){
                $("#table2").html(data["html"]);
                $("#cats_brdcrumbs").after(data["breadcrumbs"]);
                $("#menu-category").val(data["categories"]);
            }
        });
    }
',yii\web\View::POS_HEAD);
$home_categories = HomeCategory::find()->select('*')->joinWith('homeCategoryDescription as hd')->where(['status' => 1,'hd.language_id' => language_id])->andWhere(['not IN','tbl_home_category.home_category_id',[29,30]])->asArray()->all();


if(isset($_GET['Menu']['category'])){

    $categories = $_GET['Menu']['category'];
    if(!empty($categories)){
        $categories = explode(',',$categories);
        $this->registerJs('
            selectCategory('.end($categories).','.end($categories).');
        ');
    }
}
?>
<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" >
                <li class="breadcrumb-item" aria-current="page" id="cats_brdcrumbs">All Products Categories</li>
            </ol>
        </nav>
    </div>
    <div class="w-100"></div>
    <div class="col-sm-4 scrollbar">
        <div class="panel-body table-responsive">
            <table class="table" id ="table1">
                <?php foreach ($home_categories as $home_category): ?>
                <tr class="home_category" id="home_category_id_<?= $home_category['home_category_id'] ?>" onclick="return homeCategories(<?= $home_category['home_category_id'] ?>)">
                    <td><a href="javascript:void(0)"><?= $home_category['home_category_name'] ?></a></td>
                </tr>
                <?php endforeach; ?>

            </table>
        </div>
    </div>
    <div class="col-sm-4 scrollbar">
        <table class="table" id ="table2">

        </table>
    </div>
    <div class="col-sm-4 scrollbar">
        <div class="form-group col-sm-12" style="margin-top:50%;display: none;" id="sForm">
            <p id="selected_cat"></p>
            <?= Html::submitButton( Yii::t('yii', 'Select') , ['id'=>'cat_selection','class' => 'btn custom-btn']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>