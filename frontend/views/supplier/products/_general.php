<?php
use yii\bootstrap4\Html;
use kartik\file\FileInput;
use kartik\select2\Select2;
use himiklab\thumbnail\EasyThumbnailImage;
use common\models\Brand;
use common\models\BrandCategory;
use yii\db\Expression;

$imageSrc = $delUrl = '';

if(!empty($model->dish_image)){
    $image=EasyThumbnailImage::thumbnailFileUrl(
        "../../frontend/web/uploads/".$model->dish_image,200,200,EasyThumbnailImage::THUMBNAIL_INSET
    );
    $imageSrc = [Html::img($image, ['class'=>'file-preview-image', 'alt'=>$model->dish_name, 'title'=>$model->dish_name])];
    $delUrl=[['caption'=>$model->dish_name,'url'=>yii::$app->request->baseUrl.'/supplier/deletemenuimage?id='.$model->id]];
}


if(!empty($model->discount))
    $model->discount_product = 1;



echo $form->field($model,'category')->hiddenInput()->label(false);
$brands = [];
if(!empty($model->category)){
    $categories = explode(',',$model->category);
    $sub_query = BrandCategory::find()->select('brand_id')->where(['in','category_id',$categories]);
    $all_brands = Brand::find()->select('*')->joinWith('brandDescription as bd')->where(['in','tbl_brand.brand_id',($sub_query)])->andWhere(['bd.language_id' => language_id])->asArray()->all();
    if(!empty($all_brands)){
        foreach ($all_brands as $brand)
            $brands[$brand['brand_id']] = $brand['brand_name'];
    }
}
if(isset($_GET['Menu']['home_category'])){
    $model->home_category = $_GET['Menu']['home_category'];
}
echo $form->field($model,'home_category')->hiddenInput()->label(false);
?>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="w-100"></div>
    <div class="col-sm-6">
        <?= $form->field($model, 'product_price')->textInput(['type' => 'number', 'maxlength' => true,'step' => '0.01' ,'min' => 0])->hint("Transaction charges ".config_transaction_fee.'% & Freshhomee fee '.config_freshhommee_fee.'% & VAT '.config_vat.'%') ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'collected_amount')->textInput(['type' => 'number', 'maxlength' => true,'step' => '0.01' ,'min' => 0,'readonly' => true]) ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($model, 'discount_product')->checkbox() ?>
    </div>
    <div class="col-sm-6 discount" style="display:none;">
        <?= $form->field($model, 'discount')->textInput(['type' => 'number', 'maxlength' => true,'step' => '0.01' ,'min' => 0,'max' => 100]) ?>
    </div>
    <div class="w-100"></div>

    <div class="col-sm-12">
        <?= $form->field($model, 'dish_image')->widget(FileInput::classname(),
            [
                'options'=>['accept'=>'image/*'],
                'pluginOptions'=>
                    [
                        'allowedFileExtensions'=>['jpg','gif','png'],
                        'overwriteInitial'=>true,
                        'initialPreview'=>$imageSrc,
                        'initialPreviewConfig'=>$delUrl
                    ],
            ]) ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($model, 'product_description')->textArea(['rows' => 3]) ?>
    </div>


    <div class="col-sm-6">
        <?= $form->field($model, 'type_of_product')->inline(true)->radioList($model->products_type) ?>
    </div>
    <div class="w-100"></div>
    <div class="col-sm-12">
        <?= $form->field($model,'brand_id')->widget(Select2::classname(),[
            'data' => $brands,
            'options' => ['placeholder' => 'Please select ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    </div>
    <div class="w-100"></div>
    <div class="col-sm-4">
        <?= $form->field($model, 'status')->widget(Select2::classname(), [
            'data' => ['active'=>'Active','inactive'=>'In active'],
            //'options' => ['placeholder' => 'Please select ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'product_qty')->textInput(['type' => 'number','step' => 1,'min'=>0])->hint('Dish can be active once you added some quantity.') ?>
    </div>
</div>
<?php

$percentage = config_transaction_fee+config_freshhommee_fee+config_vat;
$this->registerJs('
    var qty = $("#menu-product_qty").val();
    if(qty < 1){
        $("#menu-status").val("inactive").trigger("change");
    }
    $("#menu-status").on("change",function(){
    var qty = $("#menu-product_qty").val();
    var status = $("#menu-status").val();
        if(qty < 1 && status=="active"){
            $("#menu-status").val("inactive").trigger("change");
        }
    })
    $("#menu-product_qty").on("change",function(){
    var qty = $("#menu-product_qty").val();
     var status = $("#menu-status").val();
        if(qty < 1 && status=="active"){
            $("#menu-status").val("inactive").trigger("change");
        }
    })
    if($("#menu-discount_product").is(":checked")){
        $(".discount").show();
    }
    $("#menu-discount_product").on("change onchange keyup",function(){
        if($(this).is(":checked")){
            $(".discount").show();
        }else{
            $(".discount").hide();
            $("#menu-discount").val("");
            calculatePrice();
        }
             
    })
    
    $("#menu-product_price").on("keyup onchange",function(){
        calculatePrice();
    })
    $("#menu-discount").on("keyup onchange",function(){
        calculatePrice();
    })
');
$this->registerJs('
    function calculatePrice(){
        var price = $("#menu-product_price").val();
        var discount = $("#menu-discount").val();
        if(discount!=0){
            price = parseFloat(price-( price * discount / 100 )).toFixed(2);        
        }
        var total_percent = '.$percentage.';
        price = parseFloat(price-( price * total_percent / 100 )).toFixed(2);
       
        $("#menu-collected_amount").val(price)            

    }
',yii\web\View::POS_HEAD);