<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\Tabs;
use yii\bootstrap4\ActiveForm;

$user_info = Yii::$app->user->identity;
$categories = $model->menuCategories;
if(!empty($categories) && !empty($model->id)){
    $category = [];
    foreach ($categories as $cats)
        $category[] = $cats->category_id;
    $model->category = implode(',',$category);
    if(isset($_GET['Menu']['category']))
        $model->category = $_GET['Menu']['category'];
}

$link = '';
$show_cat = $_GET['show_cat'] ?? '';
if(empty($model->id)){
    $link = Yii::$app->request->baseUrl.'/supplier?type=addProduct&Menu[home_category]='.$model->home_category.'&Menu[category]='.$model->category.'&show_cat='.true;
}else{
    $link = Yii::$app->request->baseUrl.'/supplier?type=addProduct&Menu[home_category]='.$model->home_category.'&Menu[category]='.$model->category.'&show_cat='.true.'&id='.$model->id;
}

if(empty($model->category) || $show_cat==true)
    echo $this->render('_add_product_categories',['model' => $model]);
else {
    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
    echo Tabs::widget([
        'items' => [
            [
                'label' => 'Select Categories',
                'url' => $link

            ],
            [
                'label' => 'Product Information',
                'content' => '<br/>' . $this->render('_general', ['form' => $form, 'model' => $model, 'user_info' => $user_info]),
                'active' => true
            ],
            [
                'label' => 'Images',
                'content' => '<br/>' . $this->render('_images', ['form' => $form, 'model' => $model, 'user_info' => $user_info]),
            ],
            [
                'label' => 'Product Characteristics',
                'content' => '<br/>' . $this->render('_attribute', ['form' => $form, 'model' => $model, 'user_info' => $user_info]),
            ],
        ],
    ]);
    ?>
    <div class="form-group">
        <div class="row">

            <div class="col-sm-3">
                <a class="btn custom-btn previous" href="javascript:void(0);">Previous</a>
                <a class="btn custom-btn next"  href="javascript:void(0);">Next</a>
            </div>
            <div class="col-sm-1">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Save') : Yii::t('yii', 'Save'), ['class' => $model->isNewRecord ? 'btn custom-btn' : 'btn custom-btn']) ?>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <?php
    $this->registerCss('.fade.in {opacity: 1}');
    $this->registerJs('
    $(\'.next\').click(function () {
        
        $(\'.nav-tabs > .nav-item > .active\').parent().next(\'li\').find(\'a\').trigger(\'click\');
        if($(\'.nav-tabs > .nav-item > .active\').parent().next(\'li\').find(\'a\').length==0)
        $(".next").hide();
       
    });

    $(\'.previous\').click(function () {
    var href = ($(\'.nav-tabs > .nav-item > .active\').parent().prev(\'li\').find(\'a\').attr("href"))
        if(href.indexOf("type=addProduct"))
            window.location.href  = href;
        $(".next").show();
        $(\'.nav-tabs > .nav-item > .active\').parent().prev(\'li\').find(\'a\').trigger(\'click\');
        
        
    });
    ');
}

