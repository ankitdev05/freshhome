<?php
use yii\widgets\ListView;

$this->title = 'My Products';

?>
<div class="common-color">
    <section class="address-list">
        <div class="container">
            <div class="common-dashbaord">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                        <div class="profile-side">
                            <?= $this->render('//supplier/_left_panel') ?>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-12">

                        <div class="profile-body suppliermenu">
                            <div class="form-group" >
                                <a class="btn custom-btn" href="<?= Yii::$app->request->baseUrl.'/supplier?type=addProduct' ?>">Add product</a>
                            </div>
                            <?=  ListView::widget([
                                'layout' => "<div class='col-sm-12'>{summary}</div>\n {items}\n<div class='col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
                                'dataProvider' => $dataProvider,
                                'itemView' => '//menu/_list_product',
                                'options' => [
                                    'tag' => 'div',
                                    'class' => 'form-row my-menu',
                                    'id' => 'list-wrapper',
                                ],
                                'itemOptions' => [
                                    'tag' => false
                                ],
                                'pager' => [
                                    'options' => ['class'=>'pagination justify-content-end'],
                                    'prevPageLabel' =>'Previous',
                                    'nextPageLabel' =>'Next',
                                    'pageCssClass' => 'page-item',
                                    'disabledPageCssClass' => 'page-link disabled',
                                    'linkOptions' => ['class' => 'page-link'],
                                ]
                            ]); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
