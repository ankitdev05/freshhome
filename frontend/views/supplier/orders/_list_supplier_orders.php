<?php
use yii\helpers\Url;

$address= $model->address;
$order_status = $model->order_status_id;

$btn = 'btn-warning';
if($order_status==1)
    $btn = 'btn-danger';
if($order_status==11)
    $btn = 'btn-success';
if($order_status==13)
    $btn = 'btn-light';
if($order_status==5)
    $btn = 'btn-dark';

$time_stamp = $model->orderTime;
?>
<div class="order-table orderlisting mt-3">
    <a href="<?= Url::to(['/supplier','type'=>'vieworder','order_id'=>$model->order_id],true) ?>" title="View Order">
        <p>
            <span>Order No.</span>
            <span class="no">&nbsp;<?= $model->order_id ?></span>
            <span class="right-list ml-2"><button class="btn btnadjust btn-sm <?= $btn ?>"><?= $model->orderStatus->name ?></button></span>
            <span class="right-list supplierorderdate"><?= date('d F,Y',$model->created_at) ?></span>
        </p>
        <div class="right-arrow">
            <p>
                <span>Total Items</span>
                <span class="no">&nbsp;<?= $model->countOrderProducts ?></span>
                <span class="right-list suppliertotalprice">Total Price <b> <?= $model->currency_code ?> <?= $model->total ?></b></span>
            </p>
        </div>
        <p>
            <span>Address</span>
            <span class="no">&nbsp;<?= $address->location ?> (<?= $address->title ?>)</span>

        </p>
        <?php if(!empty($model->date)){ ?>
            <p style="color:#000000 !important;"><span>Delivery Time</span> : <b><?= date('d F Y',strtotime(str_replace('/', '-',$model->date))).' '.$model->time ?></b></p>
        <?php } ?>

    </a>
    <?php if(!empty($time_stamp)){
        $remaining = $time_stamp - time();

        if($remaining<=7200 && $remaining>0){
        ?>
        <div class="pb-3">
            <button class="btn btn-sm btn-light" style="background: #e69e49;color: #ffffff"><span id="hours_<?= $model->order_id ?>"></span> Hrs</button> :
            <button class="btn btn-sm btn-light" style="background: #e69e49;color: #ffffff"><span id="minutes_<?= $model->order_id ?>"></span> Min</button> :
            <button class="btn btn-sm btn-light" style="background: #e69e49;color: #ffffff"><span id="seconds_<?= $model->order_id ?>"></span> Sec</button>
        </div>
        <?php

            $this->registerJs("
                $('#hours_".$model->order_id."').countdown('".date('Y/m/d H:i:s',$time_stamp)."')
                .on('update.countdown', function(event) {
               
                    var hours_format = '%H';
                    var minute_format = '%M';
                    var seconds_format = '%S';
                    $(this).html(event.strftime(hours_format));
                    $('#minutes_".$model->order_id."').html(event.strftime(minute_format));
                    $('#seconds_".$model->order_id."').html(event.strftime(seconds_format));
              
                }).on('finish.countdown', function(event) {
                  $(this).html('00');
                  $('#minutes_".$model->order_id."').html('00');
                  $('#seconds_".$model->order_id."').html('00');
                
                });
            ");

        ?>

    <?php } } ?>
    <?php if($order_status==1){ ?>
        <div class="acceptrejectbtns mb-2">
            <a data-method="POST" data-confirm="Are you sure want to confirm this order?" class="btn btn-sm acceptbtn" href="<?= Yii::$app->request->baseUrl.'/supplier/accept-order?order_id='.$model->order_id ?>">Accept</a>
            <a class="btn btn-sm btn-danger rejectOrder"  data-attr="<?= $model->order_id ?>" href="javascript:void(0);">Reject</a>
		</div>
    <?php } ?>
</div>

