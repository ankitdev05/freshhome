<?php
use common\models\Library;

$this->title = 'View order - '.$model->order_id.' | '.config_name;

$user = $model->user;
$userAddr = $model->address;
$orderTotal = $model->supplierOrderTotals;
$format = Yii::$app->formatter;
$orderProducts = $model->orderProducts;
$countProducts = $model->countOrderProducts;
$order_rating = $model->orderRatings;
$order_options = $model->orderOptions;
$options = [];
$library = new Library();
if(!empty($order_options)){
    foreach ($order_options as $order_option){
        $add_text = '';
        if($order_option->price!='0.00'){
            $price = $library->currencyFormat($order_option->price,$model->currency_code);
            if($order_option->subtract==1)
                $add_text = '(- '.$price.')';
            elseif($order_option->subtract==0)
                $add_text = '(+'.$price.')';
        }
        $options[$order_option->name][] = $order_option->value.$add_text;
    }
}
$orderTime = $model->orderTime;
?>
<section class="address-list">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                    <div class="profile-side">
                        <?= $this->render('//supplier/_left_panel') ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                    <div class="profile-body">
                        <div class="address-list-block card-credit order-track">
                            <div class="col-md-12">
								<div class="row">
									<p class="orderno">Order no.<?= $model->order_id ?>
										<span class="trckorder" style="color: #006200;">Order By <?= $user->name ?></span>
									</p>
									<p class="orderno">Status: <?= $model->orderStatus->name ?></p>
									<?php if(!empty($model->notes)){ ?>
										<p class="orderno">
											Additional Message: <?= $model->notes ?>
										</p>
									<?php } ?>
									<?php if(!empty($model->phone_number)){ ?>
										<p class="orderno">
											Additional Phone number: <?= $model->phone_number ?>
										</p>
									<?php } ?>
								<?php
								if(!empty($orderTime)){
									$this->registerJsFile(Yii::$app->request->baseUrl.'/js/jquery.countdown.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
									$remaining = $orderTime - time();
									?>
									<p class="orderno">Delivery Date & Time<span class="trckorder" style="color: #006200;"><?= date('d,F Y h:iA',$orderTime) ?></span></p>


										<?php if($remaining<=7200 && $remaining>0){ ?>
											<div>
												<a class="btn btn-sm btn-light"><span id="hours_<?= $model->order_id ?>"></span> Hrs</a> :
												<a class="btn btn-sm btn-light"><span id="minutes_<?= $model->order_id ?>"></span> Min</a> :
												<a class="btn btn-sm btn-light"><span id="seconds_<?= $model->order_id ?>"></span> Sec</a>
											</div>
											<?php
											$this->registerJs("
												$('#hours_".$model->order_id."').countdown('".date('Y/m/d H:i:s',$orderTime)."')
												.on('update.countdown', function(event) {
											   
													var hours_format = '%H';
													var minute_format = '%M';
													var seconds_format = '%S';
													$(this).html(event.strftime(hours_format));
													$('#minutes_".$model->order_id."').html(event.strftime(minute_format));
													$('#seconds_".$model->order_id."').html(event.strftime(seconds_format));
											  
												}).on('finish.countdown', function(event) {
												  $(this).html('00');
												  $('#minutes_".$model->order_id."').html('00');
												  $('#seconds_".$model->order_id."').html('00');
												
												});
											");
										}
									}
									if($model->order_status_id==11){
									?>
									<p class="tym">
										<span class="order-ready-btn float-right"><a href="<?= Yii::$app->request->baseUrl.'/supplier?type=processing&order_id='.$model->order_id ?>" data-confirm="Are you sure want to process this order?" class="btn btn custom-btn">Order Ready</a></span>
									</p>
									<?php } ?>

								</div>
							</div>
							<div class="col-md-12">
                            <div class="order-table">
                                <div class="row">
                                    <div class="w-100"><hr/></div>
                                    <div class="col-md-7 col-sm-7 col-xs-12 p-0">
                                        <p class="orderno">Payment Method<span class="trckorder1" style="color: #006200;"><?= $model->payment_method ?></span></p>
                                        <?php
                                            foreach($orderTotal as $total){
                                                if($total['code']!='delivery_charges'){
                                         ?>
                                        <p class="orderno"><?= $total->title ?><span class="trckorder1" style="color: #006200;"><?= $model->currency_code ?> <?= $format->format($total->value,['decimal',2]) ?></span></p>
                                        <?php } } ?>
                                    </div>
                                </div>
                                <div class="row m-t-20">
                                    <div class="col-md-12 p-0">
                                        <p class="orderno">Items in this order</p>
                                    </div>
									<div class="col-sm-12 p-0">
									<div class="form-row">
										<?php foreach($orderProducts as $orderProduct){ ?>
										<div class="col-md-6">
											<div class="supplierordersumary mb-3">
												<div class="form-row">
													<div class="col-md-3 col-4 pl-3">
														<div class="img-dish-table">
															<img alt="<?= $orderProduct->name  ?>" title="<?= $orderProduct->name  ?>" src="<?= $orderProduct->product->mainImage ?>" class="img-fluid">
														</div>
													</div>
													<div class="col-md-9 col-8">
														<div class="cake-item">
															<p><?= $orderProduct->name  ?></p>
															<p><?= $model->currency_code ?> <?= $format->format($orderProduct->price,['decimal',2]) ?></p>
															<p>Qty <?= $orderProduct->quantity ?></p>
															<?php if(!empty($options)){
																foreach ($options as $k=>$option) {
																	?>
																	<p><b><?= $k ?>:</b> <?= implode(', ', $option) ?></p>
																	<?php
																}
															}?>
														</div>
													</div>
												</div>
											</div>
										</div>
										<?php } ?>
									</div>
									</div>
                                </div>
                            </div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
