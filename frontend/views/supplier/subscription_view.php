<?php

$this->title = 'Subscription | '.config_name;

?>
<section class="address-list common-color">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 mb-3">
                    <div class="profile-side">
                        <?= $this->render('//supplier/_left_panel') ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                    <div class="profile-body">
                        <div class="address-list-block">
                            <p>Your Subscription will be expired on <?= date('d F Y',$subscription_status['subscription_end']) ?>.</p>
                            <p>Subscription Status: <?= $subscription_status['subscription_status'] ?></p>
                            <p><a href="<?= Yii::$app->request->baseUrl.'/supplier/subscription?type=subscription&cancel=true' ?>" data-confirm="Are you sure want to cancel this subscription?" class="btn btn-sm custom-btn">Cancel Subscription</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
