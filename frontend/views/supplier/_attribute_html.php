<?php
use yii\bootstrap4\Html;
use common\models\Menu;
use common\models\Option;
use common\models\OptionValue;
use common\models\MenuToAttributes;
use common\models\MenuToAttributeOptions;
use unclead\multipleinput\MultipleInput;

$language_id = language_id;
$all_options = Option::find()->joinWith('optionDescription as od')->where(['status' => 1,'od.language_id' => $language_id,'tbl_option.attribute_id' => $attr_id])->asArray()->all();
if(!empty($all_options)){
    foreach($all_options as $all_option){
        $default_val = '';
        $model = new Menu();
        $items = [];
        $optionValues = OptionValue::find()->where(['option_id' => $all_option['option_id']])->asArray()->all();
        if(!empty($optionValues)){
            foreach($optionValues as $optionValue)
                $items[] = ['name' => $optionValue['name'],'quantity' => '','subtract' => 0,'price' => '0.00','attribute_id' => $all_option['option_id']];
        }

        if(!empty($menu_id)){
            $menu2attr = MenuToAttributes::find()->where(['menu_id' => $menu_id,'option_id' => $all_option['option_id']])->asArray()->one();
            if(!empty($menu2attr)){
                $default_val = $menu2attr['is_required'];
                $items = [];
                $menu_to_options = MenuToAttributeOptions::find()->where(['menu_id' => $menu_id,'menu_attr' => $menu2attr['menu_attr']])->asArray()->all();
                if(!empty($menu_to_options)){
                    foreach($menu_to_options as $menu_to_option){
                        $items[] = ['name' => $menu_to_option['name'],'quantity' => $menu_to_option['quantity'],'subtract' => $menu_to_option['subtract'],'price' => $menu_to_option['price'],'attribute_id' => $all_option['option_id']];
                    }
                }
            }
        }
        ?>
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <h5 class="col-sm-10">
						<span class="pb-3"><?= $all_option['optionDescription']['name'] ?></span>
						<p class="mb-0 text-dark">Mandatory for user's to choose</p>
					</h5>
                    <div class="col-sm-2 mb-3">
                    <?=
                        Html::dropDownList( 'Menu[options]['.$all_option['option_id'].'][is_required]',$default_val,['yes'=>'Yes','no'=>'No'],['class' =>'form-control']
                        )
                    ?>
                    </div>
                </div>
                <?=
                MultipleInput::widget([
                    'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
                    'id' => 'product_attributes_'.$all_option['option_id'],
                    'model' => $model,
                    'min' => 0,
                    'data' => $items,
                    'addButtonOptions' => [
                        'class' => 'btn btn-success',
                    ],
                    'attribute' => 'product_attributes['.$all_option['option_id'].']',
                    'columns' => [
                        [
                            'name' => 'name',
                            'title' => 'Name',
                        ],
                        [
                            'name' => 'quantity',
                            'title' => 'Quantity',
                            'defaultValue' => 0,
                            'options' => [
                                'type' => 'number',
                                'class' => 'input-priority',
                                'step' => '1',
                                'min' => 0,
                            ]
                        ],
                        [
                            'name' => 'subtract',
                            'title' => 'Price Prefix',
                            'type'  => 'dropDownList',
                            'items' => [
                                  0 => '+',
                                  1 => '-'
                            ]
                        ],
                        [
                            'name' => 'price',
                            'title' => 'Price(e.g. +4.05,-8.10)',
                            'defaultValue' => '0.00',
                            'options' => [
                                'type' => 'number',
                                'class' => 'input-priority',
                                'step' => '0.01',
                                'min' => 0,
                            ]
                        ],
                        [
                            'name' => 'attribute_id',
                            'title' => 'attribute_id',
                            'options' => [
                                'class' => 'd-none',
                                'tags' =>false
                            ],
                            'headerOptions' => [
                                'style' => 'width: 0px;display:none;',

                            ]

                        ]
                    ]
                ])
                ?>
            </div>
        </div>
        <div class="w-100"><hr/></div>
        <?php
    }
}
$this->registerCss('
.was-validated .form-control:valid, .form-control.is-valid{background:transparent !important;padding-right:0 !important}
.list-cell__attribute_id{display:none;}
.list-cell__subtract select{
    padding:2px 6px;
}
');