<?php
use yii\bootstrap4\Html;
use common\models\Library;
use yii\bootstrap4\ActiveForm;
use kartik\time\TimePicker;

$this->title = 'Menu Schedule | '.config_name;
$library = new Library();
$mt = $_GET['mt'] ?? '';
$current_month_time_stamp = mktime(0,0,0,date('m'),1,date('Y'));
if($mt=='next'){
    $current_month_time_stamp = strtotime('+1 month', $current_month_time_stamp);
}

$month = date('m',$current_month_time_stamp);
$year = date('Y',$current_month_time_stamp);

$weeks = $library->weeks($month, $year);
$start_date =  mktime(0,0,0,$month,1,date('Y'));
$end_date =  mktime(0,0,0,$month,31,date('Y'));

$periods = new \DatePeriod(
    new \DateTime(date('Y-m-d',$start_date)),
    new \DateInterval('P1W'),
    new \DateTime(date('Y-m-d',$end_date))
);
foreach($periods as $k=>$period){
    $week_name  =  $period->format("W");
    $days = $library->getStartAndEndDate($week_name,date("Y"));
    $final_results[] = [ 'days' => $days,'title'=>'Week '.$week_name];
}
$current_time_stamp= mktime(0,0,0,date("m"),date("d"),date("Y"));

?>
<section class="address-list">
    <div class="container">
        <div class="common-dashbaord">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <?php if(empty($mt)){ ?>
                        <p class="mb-1" style="color: #ddd;font-weight: bold;">Previous Month <span class="next-m"><a href="<?= Yii::$app->request->baseUrl.'/supplier?type=mySchedule&mt=next' ?>">Next Month</a></span></p>
                    <?php }else{ ?>
                        <p class="mb-1" style="color:#333;font-weight: bold;"><a href="<?= Yii::$app->request->baseUrl.'/supplier?type=mySchedule' ?>">Previous Month</a> <span class="next-m" style="color: #ddd;font-weight: bold;">Next Month</span></p>
                    <?php } ?>
                <?php foreach($final_results as $k=>$final_result){
                    $week_end = $final_result['days']['week_end'];
                    $week_start = $final_result['days']['week_start'];

                    $week_end_explode = explode('-',$week_end);
                    $week_start_explode = explode('-',$week_start);

                    $start = end($week_start_explode);
                    $end = end($week_end_explode);
                    if($k==0)
                        $start = 1;
                    if($k==count($final_results)-1){
                        $date = new \DateTime(date('y-m-d',$start_date));
                        $date->modify('last day of this month');
                        $end =  $date->format('d');
                    }

                    ?>
                    <div class="profile-body2 mt-4">
                        <div class="address-list-block card-credit order-track">
                            <div class="week week<?= ($k+1) ?>">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <div class="week-circle-big">
                                            <span class="week-name"><?= $final_result['title'] ?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-xs-12">
                                        <div class="week-circle-small">
                                            <span class="week-date"><?=  str_pad($start,2,0,STR_PAD_LEFT  ) ?> - <?= str_pad($end,2,0,STR_PAD_LEFT  ) ?> <?= $library->convertMonthNumberToName($month) ?></span>
                                            <ul>
                                                <?php

                                                for($i=$start;$i<=$end;$i++){
                                                    $time_stamp = mktime(0,0,0,$month,$i,$year);
                                                    $class= '';
                                                    if($current_time_stamp>$time_stamp)
                                                        $class = 'opactity';
                                                    $week_active = '';

                                                    if($i==date('d'))
                                                        $week_active = 'week-active';

                                                    ?>
                                                    <li class="<?= $class ?>">
                                                        <?php if(empty($class)){
                                                        $_GET['MenuSchedulesSearch']['schedule_date'] = date('Y-m-d',$time_stamp);
                                                        $menu_sch = $user_info->userMenuSchedules;
                                                        $active_class = '';
                                                        if(!empty($menu_sch))
                                                            $active_class = ' week-active1';
                                                            ?>
                                                        <a schedule_date="<?= date('Y-m-d',$time_stamp) ?>" href="javascript:void(0);" timestamp="<?= $time_stamp ?>" data-attr="<?= date('d F Y',$time_stamp) ?>" class="week-circle-big small-circle <?= $week_active.$active_class ?> day-data">
                                                            <span class="week-name">
                                                                <?= str_pad($i,2,0,STR_PAD_LEFT) ?>
                                                            </span>
                                                        </a>
                                                    <?php }else{ ?>
                                                            <span class="week-circle-big small-circle">
                                                                <span class="week-name">
                                                                    <?= str_pad($i,2,0,STR_PAD_LEFT) ?>
                                                                </span>
                                                            </span>
                                                    <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
					<div class="text-center mb-4 pb-1 d-none">
						<a href="javascript:void(0)" class="btn btnviewalldays">view all scheduled days</a>
					</div>
                    <div class="profile-side p-15">
                        <div class="timing-side">
                            <?php if(empty($menu_items)){ ?>
                                <h5 class="text-center">No product found.</h5>
                            <?php }else{ ?>
                            <p class="m-b-5"><b>Timings</b></p>
                            <h5 id="event_date"></h5>
                            <?php $form = ActiveForm::begin(); ?>
                                <?= $form->field($model, 'schedule_date')->hiddenInput()->label(false) ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?=  $form->field($model, 'start_time')->widget(TimePicker::classname())->label('To');
                                        ?>
                                    </div>

                                    <div class="col-md-6">
                                        <?=  $form->field($model, 'end_time')->widget(TimePicker::classname())->label('From');
                                        ?>
                                    </div>
                                </div>
                                <div class="cake-add-item">
                                    <p><b>Select Menu</b></p>
									<div class="schedulemenulist">
									<?php foreach($menu_items as $menu_item){ ?>
                                       <div class="row">
										   <div class="dis-pic col-sm-4">
											   <div class="img-dish-table"> <img src="<?= $menu_item->mainImage ?>" class="img-circle"></div>
										   </div>

										   <div class="cake-add col-sm-8">
											   <div class="cake-item">
												   <p><?= $menu_item->dish_name ?></p>
												   <p>Price <?= config_default_currency ?> <?= $menu_item->dish_price ?></p>
												   <span class="cus-box" style="top:auto">
														<div class="custom-control custom-checkbox">
														   <input id="men_id_<?= $menu_item->id ?>" type="checkbox" class="custom-control-input" name="Menu[product_id][<?= $menu_item->id ?>]">
														   <label class="custom-control-label" for="men_id_<?= $menu_item->id ?>"></label>
														</div>
												   </span>
												   <p>Qty <input id="men_qty_<?= $menu_item->id ?>" type="number" min="1" step="1" name="Menu[quantity][<?= $menu_item->id ?>]" class="qty-input" /></p>
											   </div>
										   </div>
                                           <div class="w-100"><hr/></div>
                                       </div>
									<?php } ?>
								   </div>
                                    <div class="add-cake-btn">
                                        <?= Html::submitButton('Add Schedule', ['class' => 'btn btn custom-btn']) ?>

                                    </div>
                                </div>
                            <?php ActiveForm::end(); ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$this->registerCss('
.opactity{
    opacity : 0.3;
    cursor :
}
.was-validated .form-control:valid, .form-control.is-valid,.was-validated .custom-select:valid, .custom-select.is-valid {
  background-image: none;
}
#menuschedules-start_time,#menuschedules-end_time{
    padding: 8px;
}
');
$this->registerJs('
    $(".day-data").on("click",function(){
        $(".qty-input").each(function(){
            $(this).val("");
        })
         $(".custom-control-input").each(function(){
            $(this).removeAttr("checked");
        })
        $(".week-circle-big").each(function(){
            $(this).removeClass("week-active");
        });
        $(this).addClass("week-active");
        var date =$(this).attr("data-attr");
        $("#event_date").html(date);
        $("html, body").animate({ scrollTop: 0 }, "slow");
        
        var sch_date = $(this).attr("schedule_date");
        $("#menuschedules-schedule_date").val(sch_date);
        
        $.ajax({
            url : website_url+"/supplier/my-sch?date="+sch_date,
            type : "POST",
            dataType: "json",
            success : function(json){
                if(json["to"])
                    $("#menuschedules-start_time").val(json["to"])
                if(json["from"])
                    $("#menuschedules-end_time").val(json["from"])
                if(json["schedule_date"])
                    $("#menuschedules-schedule_date").val(json["schedule_date"])
                if(json["schedule_date_format"])
                    $("#event_date").html(json["schedule_date_format"])
                 if(json["items"]){
                    var items = json["items"];
                    for ( var i = 0; i < items.length; i++ ) {
                        menu_id = items[i].menu_id;
                        quantity = items[i].quantity;
                        $("#men_qty_"+menu_id).val(quantity);
                        $("#men_id_"+menu_id).attr("checked",true);
                    }
                 }   
            }
        })
    })
');