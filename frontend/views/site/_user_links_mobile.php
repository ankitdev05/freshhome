<?php
use yii\helpers\Html;

if(Yii::$app->user->isGuest){
?>
<li>
    <a href="javascript:void(0);" onClick='$("#login_id").click();' data-target=".bs-example-modal-lg" data-toggle="modal">
        <?= Yii::t('app', 'Login') ?>
    </a>
</li>
<li><?= Html::a(Yii::t('app', 'Wishlist'),$website_url.'wishlist',['title'=>'Wishlist']) ?></li>
<?php }else{ ?>
    <li>
        <?= Html::a(Yii::t('app', 'Wishlist'),$website_url.'wishlist',['title'=>'Wishlist']) ?>
    </li>
    <li class="dropdown">
        <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?= Yii::t('app', 'Account') ?>
        </a>
        <div class="dropdown-menu mdropdowmmenu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="<?= $website_url.'profile' ?>">
                <?= Yii::t('app', 'Profile') ?>
            </a>
            <a class="dropdown-item" href="<?= $website_url.'profile?type=myorders' ?>">
                <?= Yii::t('app', 'My Orders') ?>
            </a>
            <a class="dropdown-item" href="<?= $website_url.'profile/mywallet?type=mywallet' ?>">
                <?= Yii::t('app', 'My Wallet') ?>
            </a>
            <a class="dropdown-item" href="<?= $website_url.'profile?type=invite' ?>">
                <?= Yii::t('app', 'Invite Friends') ?>
            </a>
            <a class="dropdown-item" href="<?= $website_url.'switch-to-supplier' ?>">
                <?= Yii::t('app', 'Switch to Sell') ?>
            </a>
        </div>
    </li>

<?php } ?>
