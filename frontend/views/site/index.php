<?php
use yii\helpers\Url;
use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this yii\web\View */
$config_logo    = config_logo;
$image	= EasyThumbnailImage::thumbnailFileUrl(
    "../../frontend/web/images/store/".$config_logo,200,200,EasyThumbnailImage::THUMBNAIL_INSET
);


$this->title = config_title;
\Yii::$app->view->registerMetaTag([
    'property' => 'fb:app_id',
    'content' => '744473809305794'
]);
\Yii::$app->view->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website'
]);
\Yii::$app->view->registerMetaTag([
    'property' => 'og:locale',
    'content' => 'en_US'
]);
\Yii::$app->view->registerMetaTag([
    'property' => 'og:title',
    'content' => $this->title
]);
\Yii::$app->view->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::to(['/'],true)
]);
\Yii::$app->view->registerMetaTag([
    'property' => 'og:image',
    'content' => Url::to($image,true)
]);
if(!empty(config_meta_description)){
    \Yii::$app->view->registerMetaTag([
        'name' => 'description',
        'content' => config_meta_description
    ]);
    \Yii::$app->view->registerMetaTag([
        'property' => 'og:description',
        'content' => config_meta_description
    ]);
}
if(!empty(config_meta_keywords)){
    \Yii::$app->view->registerMetaTag([
        'name' => 'keywords',
        'content' => config_meta_keywords
    ]);
}
echo $this->render('//site/home_sliders');
//echo $this->render('//site/_filters');
//secho $this->render('//site/_all_main_cats');
echo $this->render('//site/_latest_products');
echo $this->render('//site/_best_seller_products');
echo $this->render('//site/_online_kitchen');
echo $this->render('//site/_order_now'); // ORDER NOW
//echo $this->render('//site/_best_sellers');
?>
 