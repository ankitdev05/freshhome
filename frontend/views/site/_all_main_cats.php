<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\MainCategories;

$session = Yii::$app->session;
$screen_id = $session->get('screen_id') ?? 1;
$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);

$all_cats = MainCategories::find()->select('*')->joinWith('categoryDescription as cd')->where(['status' => 1 ,'cd.language_id' => language_id])->asArray()->all();

?>
<section class="service-sec d-none">
    <div class="container">
        <div class="row inner-service">
            <?php foreach($all_cats as $all_cat){ ?>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="service-box">
                    <a href="<?= yii::$app->request->baseUrl.'?menu_type='.$all_cat['code'] ?>">
                        <span class="img-circle-div">
                            <?= Html::img($website_url.$all_cat['image'],['class' => 'img-resposive','alt' => $all_cat['name'], 'title'=> $all_cat['name']]) ?>
                        </span>
                        <h4><?= $all_cat['name'] ?></h4>
                        <p>
                            <?= $all_cat['description'] ?>
                        </p>
                    </a>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section> 