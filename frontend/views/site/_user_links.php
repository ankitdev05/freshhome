<?php
use yii\helpers\Html;

if(Yii::$app->user->isGuest){ ?>
<ul class="nav navbar-nav ml-auto cus-navbar">
    <li class="diff-li cart-icon"><?= Html::a('<i class="fas fa-user-alt"></i> <span>'.Yii::t('app', 'Login').'</span>','#login',['class' => 'modal-tab', 'data-toggle' => 'modal', 'data-target' => '.bs-example-modal-lg', 'id' => 'header_login_btn_id','onClick'=>'return $("#login_id").click()']) ?></li>

    <?php }else{ ?>
    <ul class="nav navbar-nav ml-auto cus-navbar dshbrd-nvbar">
        <li class=" cart-icon dropdown-li">
            <a href="#" class="dropdown-user">
                <i class="fas fa-user"></i>
                <?= Yii::t('app', 'Account') ?>
            </a>
            <ul class="m-0 p-0 dshbrd-subnvbar" type="none">
                <li>
                    <a href="<?= $website_url.'profile' ?>"><?= ucwords(Yii::$app->user->identity->name) ?><span class="qr"><i class="fas fa-qrcode"></i></span></a>
                </li>
                <li>
                    <a href="<?= $website_url.'profile?type=myorders' ?>">
                        <?= Yii::t('app', 'My Orders') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $website_url.'profile/mywallet?type=mywallet' ?>">
                        <?= Yii::t('app', 'My Wallet') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $website_url ?>profile?type=invite">
                        <?= Yii::t('app', 'Invite Friends') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $website_url.'switch-to-supplier' ?>">
                        <?= Yii::t('app', 'Switch to Sell') ?>
                    </a>
                </li>
                <li>
                    <?= Html::a(Yii::t('app', 'Logout'),$website_url.'logout',['data-method' => 'post']) ?>
                </li>
            </ul>
        </li>
        <?php } ?>
        <li class="diff-li cart-icon"><?= Html::a('<i class="far fa-heart"></i><span>'.Yii::t('app', 'Wishlist').'</span>',$website_url.'wishlist',['title'=>'Wishlist']) ?></li>
        <li class="diff-li cart-icon position-relative"><?= Html::a('<i class="fas fa-cart-plus"></i><span>'.Yii::t('app', 'Cart').'</span>',$website_url.'cart') ?><cite class="cartbadge cart_badge"><?= $count ?></cite></li>
    </ul>