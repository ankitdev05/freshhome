<?php
use yii\helpers\Html;
$user_info = Yii::$app->user->identity;
$supplier_type = $user_info->supplier_type;
?>
<?php if($supplier_type==1){ ?>

    <li>
        <a href="<?= $website_url.'supplier?type=listmenu' ?>">
            <?= Yii::t('app', 'Menu') ?>
        </a>
    </li>
<?php } ?>
<?php if($supplier_type>1){ ?>
    <li>
        <a href="<?= $website_url.'supplier?type=myorders' ?>">
            <?= Yii::t('app', 'Orders') ?>
        </a>
    </li>
    <li>
        <a href="<?= $website_url.'supplier?type=myProducts' ?>">
            <?= Yii::t('app', 'Products') ?>
        </a>
    </li>
<?php } ?>
<li class="dropdown">
    <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?= Yii::t('app', 'Account') ?>
    </a>
    <div class="dropdown-menu mdropdowmmenu" aria-labelledby="dropdownMenuButton">
        <?php if($supplier_type==1){ ?>
            <a class="dropdown-item" href="<?= $website_url.'supplier?type=listmenu' ?>">
                <?= Yii::t('app', 'My menu') ?>
            </a>
            
            <a class="dropdown-item" href="<?= $website_url.'supplier?type=mykitchen' ?>">
                <?= Yii::t('app', 'My Kitchen') ?>
                <span class="badge" style="position: relative;">
                    <?= $count_orders ?>
                </span></a>
        <?php } ?>
        <?php if($supplier_type>1){ ?>
            <a class="dropdown-item" href="<?= $website_url.'supplier?type=myProducts' ?>">
                <?= Yii::t('app', 'My Products') ?>
            </a>
        <?php } ?>
        <a class="dropdown-item" href="<?= $website_url.'profile/mywallet?type=mywallet' ?>">
            <?= Yii::t('app', 'My Wallet') ?>
        </a>
        <a class="dropdown-item" href="<?= $website_url.'supplier/subscription?type=subscription' ?>">
            <?= Yii::t('app', 'My Plan') ?>
        </a>
        <a class="dropdown-item" href="<?= $website_url.'supplier?type=salesreport' ?>">
            <?= Yii::t('app', 'My Sales Report') ?>
        </a>
        <a class="dropdown-item" href="<?= $website_url ?>supplier?type=invite">
            <?= Yii::t('app', 'Invite Friends') ?>
        </a>
        <a class="dropdown-item" href="<?= $website_url.'supplier' ?>">
            <?= Yii::t('app', 'Ask For Help') ?>
        </a>
        <a class="dropdown-item" href="<?= $website_url.'switch-to-buy' ?>">
            <?= Yii::t('app', 'Switch to Buy') ?>
        </a>

    </div>
</li>
