<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = Yii::t('app','Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="signup-sec">
    <div class="container">
        <div class="signup-content">
            <div class="signup-body">
                <h3 class="text-uppercase text-center m-b-30"><?= Yii::t('app','Forgot Password') ?></h3>
                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                <div class="row">
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
                    </div>
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 submit-bt">
                        <?= Html::submitButton(Yii::t('app','Send'), ['class' => 'btn custom-btn']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>

