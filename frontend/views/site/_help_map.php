<?php
$class_name 	=  strtolower(\yii\helpers\StringHelper::basename(get_class($model)));
$lat 			= 34.06073314355871;
$long 			= -118.24884984130858;
$allow			= 1;
if(!empty($model->lat) && !empty($model->lng) ){
    $lat 		= $model->lat;
    $long 		= $model->lng;
    $allow		= 0;
}
$this->registerJs('
function initMap() {
    infoWindow = new google.maps.InfoWindow;
    if (navigator.geolocation) {
		var lat	 = '.$lat.';
		var long = '.$long.';
		var allow = '.$allow.';
		showmap(lat,long);
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
			  lat: position.coords.latitude,
			  lng: position.coords.longitude
			};
			var lat	 = position.coords.latitude;
			var long = position.coords.longitude;
			if(allow==1)
				showmap(lat,long);
	   }, function(e) {console.log(e)},{timeout:1000});
	}
		


}
function showmap(lat,long) {
		
		
		var map = new google.maps.Map(document.getElementById("map_canvas"), {
		  center: {lat: lat, lng: long},
		  draggable:true,
		  zoom: 12,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		});
	
		
		var markerPosition = new google.maps.LatLng(lat, long)
		var marker = new google.maps.Marker({
				map: map,
				draggable: true,
				animation: google.maps.Animation.DROP,
				position: markerPosition
		})
		var geocoder = new google.maps.Geocoder;
		var infowindow = new google.maps.InfoWindow();
		//getAddress(lat,long);
		$(\'#'.$class_name.'-lat\').val(lat);
		$(\'#'.$class_name.'-lng\').val(long);
		var autocomplete = new google.maps.places.Autocomplete($("#'.$class_name.'-location")[0], {});
		google.maps.event.addListener(autocomplete, "place_changed", function() {
			var place = autocomplete.getPlace();
			var lat = place.geometry.location.lat();
			var lng = place.geometry.location.lng();
			map.setCenter({lat:parseFloat(lat),lng: parseFloat(lng)})
			marker.setPosition({lat:parseFloat(lat),lng: parseFloat(lng)});

			  geocoder.geocode({
					latLng: marker.getPosition()
				},function(results, status) {
					
					if (status == google.maps.GeocoderStatus.OK) {
						if (results[0]) {
							lat	= marker.getPosition().lat();
							long = marker.getPosition().lng();
							$(\'#'.$class_name.'-lat\').val(lat);
							$(\'#'.$class_name.'-lng\').val(long);
							var address = results[0].formatted_address;
							$(\'#'.$class_name.'-location\').val(address);
							getAddress(marker.getPosition().lat(),marker.getPosition().lng());
						}
					}
				})
		});
		
		google.maps.event.addListener(marker, "dragend", function() {
			geocoder.geocode({
				latLng: marker.getPosition()
			}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						lat	= marker.getPosition().lat();
						long = marker.getPosition().lng();
						$(\'#'.$class_name.'-lat\').val(lat);
						$(\'#'.$class_name.'-lng\').val(long);
						var address = results[0].formatted_address;
						$(\'#'.$class_name.'-location\').val(address);
						getAddress(marker.getPosition().lat(),marker.getPosition().lng());
					}
				}
			})
		})
		map.addListener("click", function(e) {
		  marker.setPosition(e.latLng);
		  geocoder.geocode({
				latLng: marker.getPosition()
			},function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						lat	= marker.getPosition().lat();
						long = marker.getPosition().lng();
						$(\'#'.$class_name.'-lat\').val(lat);
						$(\'#'.$class_name.'-lng\').val(long);
						var address = results[0].formatted_address;
						$(\'#'.$class_name.'-location\').val(address);
						getAddress(marker.getPosition().lat(),marker.getPosition().lng());
					}
				}
			})
		});
}

function getAddress(lat,long){

	var country, city,postal_code,state;
	var address = "";
	latlng 		= new google.maps.LatLng(lat, long),
	geocoder 	= new google.maps.Geocoder();
	geocoder.geocode({"latLng": latlng}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			storableLocation = {};
			for (var ac = 0; ac < results[0].address_components.length; ac++) {
				
			   var component = results[0].address_components[ac];
				
			   if(component.types.includes("sublocality") || component.types.includes("locality")) {
					storableLocation.city = component.long_name;
			   }
			   else if (component.types.includes("administrative_area_level_1")) {
					storableLocation.state = component.long_name;
			   }
			   else if (component.types.includes("country")) {
					storableLocation.country = component.long_name;
					storableLocation.registered_country_iso_code = component.short_name;
			   }
			   else if (component.types.includes("postal_code")) {
					storableLocation.postal_code = component.long_name;
					
			   }

			}

			if(storableLocation.postal_code)
				$("#'.$class_name.'-zip_code").val(storableLocation.postal_code);
			if(storableLocation.city!="")
				$("#'.$class_name.'-city").val(storableLocation.city);
			if(storableLocation.country!="")
				$("#'.$class_name.'-country").val(storableLocation.country);
			if(storableLocation.state!="")
				$("#'.$class_name.'-state").val(storableLocation.state);
			
	}
});
	
}
', \yii\web\View::POS_HEAD);
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key='.Yii::$app->params['google_api_key'].'&callback=initMap&libraries=places', ['depends' => [\yii\web\JqueryAsset::className()]]);
