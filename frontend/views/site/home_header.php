<?php

use yii\helpers\Url;
use yii\bootstrap4\Html;
use himiklab\thumbnail\EasyThumbnailImage;
use common\models\MainCategories;
use common\models\Category;
use common\models\Countries;
use common\models\HomeCategory;
use frontend\models\Search;
use api\models\Cart;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;

$user_info = Yii::$app->user->identity;
$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);


$config_logo    = config_logo;
$image	= EasyThumbnailImage::thumbnailFileUrl(
    "../../frontend/web/images/store/".$config_logo,370,112,EasyThumbnailImage::THUMBNAIL_INSET
);
$language_id = language_id;

$all_cats = MainCategories::find()->select('*')->joinWith('categoryDescription as cd')->where(['status' => 1 ,'cd.language_id' => $language_id])->asArray()->all();

$session = Yii::$app->session;
$screen_id = $session->get('screen_id');
$is_supplier = $session->has('is_supplier');

$count_items = 0;
$cart = new Cart();
if(!Yii::$app->user->isGuest){
    $items = $cart->getTotalCartItems(yii::$app->user->id);
    if(!empty($items))
        $count_items = $items['total_items'];
}else{
    $items = $cart->getGuestTotalCartItems();
    if(!empty($items))
        $count_items = $items['total_items'];
}
$language = 'Arabic';
$link = Yii::$app->request->baseUrl.'/?lang=ar';
if($language_id > 1){
    $language = 'English';
    $link = Yii::$app->request->baseUrl.'/?lang=en';
}

$count_orders = 0;
if(!empty($user_info) && $user_info->supplier_type==1){
    $p_orders_count = $user_info->getFoodSupplierPendingOrders();
    $pre_orders_count = $user_info->getFoodSupplierPreOrder();
    $count_orders = $p_orders_count->count()+$pre_orders_count->count();
}
$all_countries = Countries::find()->where(['status'=>1])->asArray()->all();
$home_categories = HomeCategory::find()->select('*')->joinWith('homeCategoryDescription as hd')->where(['and',['status' => 1,'hd.language_id' => language_id]])->asArray()->all();

$all_categories = Category::find()->select('*')->joinWith('categoryDescription as cd')->where(['and',['active' => 1,],['cd.language_id' => language_id],['is','parent_id',null],['>','cat_id' ,1]])->all();

$search_model = new Search();
$categories = [];
if(!empty($home_categories)){
    foreach ($home_categories as $home_category)
        $categories[$home_category['home_category_id']] = $home_category['home_category_name'];
}
$search_model->load(yii::$app->request->get());
?>
<header>
		<div class="container-fluid">
			<div class="form-row">
				<div class="col-2 p-0 d-block d-md-none d-sm-none d-xs-block">
					<button type="button" id="sidebarCollapse" class="btn btnsidemenu mt-2">
						<i class="fas fa-align-left"></i>
					</button>
				</div>
				<div class="col-md-2 col-6">
                    <?= Html::a(Html::img($image,['width' => 200,'class' => 'img-fluid sitelogo', 'alt' => 'Logo', 'title' => 'Logo']),$website_url) ?>
				</div>
				<div class="col-4 d-block d-md-none d-sm-none d-xs-block">
					<div class="procart text-right mt-3">
                        <a href="<?= $website_url.'cart' ?>">
                            <span><i class="fas fa-cart-plus text-white"></i></span>
                        </a>
					</div>
				</div>
				<div class="col-md-8 col-sm-10 col-12">
                    <?php
                        $form = ActiveForm::begin(['id' => 'search-home-form',
                            'action' => ['search'],
                            'method' => 'GET',
                            'fieldConfig' => [
                                'options' => [
                                    'tag' => false,
                                ],
                            ],
                        ]); ?>
					<div class="input-group mt-2 pt-1">
						<div class="input-group-prepend d-none d-md-block d-sm-block d-xs-none">
                            <div class="dropdowncategory">
                                <?php
                                    if(!empty($home_categories))
                                        echo $form->field($search_model, 'category')->dropDownList($categories,['prompt'=>'All','class' => 'droplistcat'])->label(false); ?>
                            </div>

						</div>
                        <?php
                        echo $form->field($search_model,'type')->hiddenInput()->label(false);
                        echo $form->field($search_model, 'name')->widget(AutoComplete::className(),[
                            'name' => 's',
                            'id' => 'header-search',
                            'options' => [
                                'class' => 'form-control searchinput'
                            ],
                            'clientOptions' => [
                                'source' =>  Url::to(['search-items']),
                                'minLength'=>'2',
                                'autoFill'=>true,
                                'select' => new JsExpression("function( event, ui ) {
                                        $('#search-type').val(ui.item.type);
                                        $('#search-home-form').submit();
                                    }")],
                        ])->label(false);

                        ?>
						<div class="input-group-append">
							<button class="input-group-text btnsearch px-4" id="basic-addon2" onclick="$('#search-home-form').submit()"><i class="fas fa-search"></i></button>
						</div>
					</div>
                    <?php ActiveForm::end(); ?>
				</div>
				<div class="col-md-2 d-none d-md-block d-sm-block d-xs-none">
					<div class="arabictext text-right mt-2 pt-2">
						<a href="<?= $link ?>"><p><?= $language ?></p></a>
					</div>
				</div>
				<div class="w-100 mt-2"></div>
				<div class="col-md-2 d-none d-md-block d-sm-block d-xs-none">
                <?= $this->render('_address') ?>
				</div>
				<div class="col-md-7 d-none d-md-block d-sm-block d-xs-none" >
					<div class="form-row">
						<div class="col-md-2">
							<div class="shopbycategory mt-2">
								<cite class="storeicon"><i class="fas fa-store text-white"></i></cite><a href="<?= $website_url.'categories' ?>"><span><?= yii::t('app','Shop By') ?><br><?= yii::t('app','Category') ?></span></a>
							</div>
							<div class="shopbycategory mt-2 d-none">
                            <?php if(!empty($all_categories)){ ?>
                            <select class="form-control shoplistcat">
                                <option value="shopbycategory">Shop by Category</option>
                                <?php foreach ($all_categories as $category){ ?>
                                    <option value="<?= $category->category_id ?>"><?= $category->categoryDescription->category_name ?></option>
                                <?php } ?>

                            </select>
                            <?php } ?>
							</div>
							
						</div>
						<div class="col-md-10">
							<div class="smallmenus mt-3 pt-1">
								<ul>
                                    <?php if(!empty($header_pages)){ ?>
                                        <?php foreach ($header_pages as $header_page): ?>
									        <li><?= Html::a($header_page->title,$header_page->pageUrl) ?></li>
                                        <?php endforeach; ?>
                                    <?php } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 d-none d-md-block d-sm-block d-xs-none">
					<div class="signinaccount mt-2">
						<ul class="rightsidecart">
							<li>
                                <?php
                                    if(Yii::$app->user->isGuest) echo $this->render('theme/login_links');
                                    elseif($is_supplier && !Yii::$app->user->isGuest) echo $this->render('theme/_supplier_links',['website_url'=>$website_url,'count_orders' => $count_orders,'user_info' => $user_info]);
                                    elseif(!Yii::$app->user->isGuest) echo $this->render('theme/_user_links',['website_url'=>$website_url,'count' => $count_items]);
                                ?>
							</li>
							<li>
								<div class="wishlist text-center">
                                    <a href="<?= $website_url.'wishlist' ?>">
									<span class="herdersmarticon">
                                        <i class="far fa-heart text-white"></i>
                                    </span>
									<p class="mb-0 text-white spad">
                                        <?= Yii::t('app', 'Wishlist') ?>
                                    </p>
                                    </a>
								</div>
							</li>
							<li>
								<div class="procart text-center">
                                    <a href="<?= $website_url.'cart' ?>">
									    <span class="herdersmarticon"><i class="fas fa-cart-plus text-white"></i></span>
									    <p class="mb-0 text-white spad"><?= Yii::t('app', 'Cart') ?></p>
                                    </a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>
<?= $this->render('theme/_mobile_menus',['all_countries'=>$all_countries,'website_url' => $website_url,'is_supplier' => $is_supplier,'count_orders' => $count_orders]) ?>

<?php
$this->registerCss('#navbarNav .active,.maincategory .active{background-color:#df8317 !important}');