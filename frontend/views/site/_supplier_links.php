<?php
use yii\helpers\Html;

$supplier_type = $user_info->supplier_type;

?>
<ul class="nav navbar-nav ml-auto cus-navbar dshbrd-nvbar">
    <li class=" cart-icon dropdown-li">
        <a href="#" class="dropdown-user">
            <i class="fas fa-user"></i>
            <?= Yii::t('app', 'Account') ?>
        </a>
        <ul class="m-0 p-0 dshbrd-subnvbar" type="none">
            <li>
                <a href="<?= $website_url.'supplier' ?>"><?= ucwords($user_info->name) ?><span class="qr"><i class="fas fa-qrcode"></i></span></a>
            </li>
        <?php if($supplier_type==1){ ?>
            <li>
                <a href="<?= $website_url.'supplier?type=listmenu' ?>">
                    <?= Yii::t('app', 'My menu') ?>
                </a>
            </li>

            <li>
                <a href="<?= $website_url.'supplier?type=mykitchen' ?>">
                    <?= Yii::t('app', 'My Kitchen') ?> </a>
                <span class="badge2"><?= $count_orders ?></span>
            </li>
        <?php } ?>
        <?php if($supplier_type>1){ ?>
            <li>
                <a href="<?= $website_url.'supplier?type=myProducts' ?>">
                    <?= Yii::t('app', 'My Products') ?>
                </a>
            </li>
        <?php } ?>
            <li>
                <a href="<?= $website_url.'profile/mywallet?type=mywallet' ?>">
                    <?= Yii::t('app', 'My Wallet') ?>
                </a>
            </li>
            <li>
                <a href="<?= $website_url.'supplier/subscription?type=subscription' ?>">
                    <?= Yii::t('app', 'My Plan') ?>
                </a>
            </li>
            <li>
                <a href="<?= $website_url.'supplier?type=salesreport' ?>">
                    <?= Yii::t('app', 'My Sales Report') ?>
                </a>
            </li>
            <li>
                <a href="<?= $website_url ?>supplier?type=invite">
                    <?= Yii::t('app', 'Invite Friends') ?>
                </a>
            </li>
            <li>
                <a href="<?= $website_url.'supplier' ?>">
                    <?= Yii::t('app', 'Ask For Help') ?>
                </a>
            </li>
            <li>
                <a href="<?= $website_url.'switch-to-buy' ?>">
                    <?= Yii::t('app', 'Switch to Buy') ?>
                </a>
            </li>
            <li><?= Html::a(Yii::t('app', 'Logout'),$website_url.'logout',['data-method' => 'post']) ?></li>
        </ul>
    </li>
    <?php if($supplier_type==1){ ?>
        <li class="diff-li cart-icon">
            <a href="<?= $website_url.'supplier?type=mySchedule' ?>"><i class="fas fa-calendar-alt" aria-hidden="true"></i>
                <span><?= Yii::t('app', 'Schedule') ?></span>
            </a>
        </li>
        <li class="diff-li cart-icon">
            <a href="<?= $website_url.'supplier?type=listmenu' ?>"><i class="fas fa-utensils" aria-hidden="true"></i>
                <span><?= Yii::t('app', 'Menu') ?></span>
            </a>
        </li>
    <?php } ?>
<?php if($supplier_type>1){ ?>
    <li class="diff-li cart-icon">
        <a href="<?= $website_url.'supplier?type=myorders' ?>"><i class="fas fa-clipboard-list"></i>
            <span><?= Yii::t('app', 'Orders') ?></span>
        </a>
    </li>
    <li class="diff-li cart-icon">
        <a href="<?= $website_url.'supplier?type=myProducts' ?>"><i class="far fa-list-alt"></i>
            <span><?= Yii::t('app', 'Products') ?></span>
        </a>
    </li>
    <?php } ?>

</ul>
<?php
$this->registerCss('.header-sec .cus-navbar>li>a{margin:0px 2px;}');