<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\MainCategories;
use kartik\select2\Select2;

$this->title = 'Ask for help | '.config_title;
$all_cats = ArrayHelper::map(MainCategories::find()->where(['status' => 1])->asArray()->all(),'id','name');
?>
<div class="bg-sec">
    <div class="container">
        <div class="heading">
            <h3>Ask for Help</h3>
            <div class="divider"></div>
        </div>
    </div>
</div>
<section class="contact_info help">
    <div class="container">
        <div class="row ">
            <div class="col-lg-3 pr-0">
                <div class="contact_info_item">
                    <h6 class="contact-h6">How it Works?</h6>
                    <p class="">One of our salesperson call,then visit you to teach you how to use this app .</p>
                </div>
                <div class="contact_info_item">
                    <h6 class="contact-h6">Roles and Responsibilities of a salesperson</h6>
                    <?= config_ask_for_help_text ?>
                </div>
            </div>
            <div class="col-lg-8 offset-lg-1">
                <div class="contact_form">
                    <?php $form = ActiveForm::begin(['id' => 'help-form']); ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'name')->textInput() ?>
                        </div>
                        <div class="col-sm-12">
                            <?= $form->field($model, 'email')->textInput() ?>
                        </div>
                        <div class="col-sm-12">
                            <?= $form->field($model, 'phonenumber')->textInput() ?>
                        </div>
                        <div class="col-sm-12">
                            <?= $form->field($model, 'location')->textInput() ?>
                            <div class='map' id='map_canvas' style="height:300px;"></div>

                            <?= $form->field($model, 'lat')->hiddenInput(['maxlength' => true])->label(false) ?>
                            <?= $form->field($model, 'lng')->hiddenInput(['maxlength' => true])->label(false) ?>
                        </div>
                        <div class="col-sm-6 mt-3">
                            <?=
                            $form->field($model, 'request_type')->widget(Select2::classname(), [
                                'data' => $all_cats,
                                'options' => ['placeholder' => 'Please select ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label('What you want to sell with Freshhomee?');
                            ?>
                        </div>
                        <div class="col-sm-12 mt-3">
                            <?= Html::submitButton('Submit', ['class' => 'btn custom-btn', 'name' => 'contact-button']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
echo $this->render('//site/_help_map',['model'=>$model]);
