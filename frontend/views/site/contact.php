<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\captcha\Captcha;

$this->title = Yii::t('app','Contact Us');
?>
<div class="bg-sec">
    <div class="container">
        <div class="heading">
            <h3><?= Yii::t('app','Contact Us'); ?></h3>
            <div class="divider"></div>
        </div>
    </div>
</div>

<section class="contact_info">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 pr-0">
                <div class="contact_info_item">
                    <h6 class="contact-h6"><?= Yii::t('app','Office Address'); ?></h6>
                    <p><?= config_address ?></p>
                </div>
                <div class="contact_info_item">
                    <h6 class="contact-h6"><?= Yii::t('app','Contact Info'); ?></h6>

                    <p class="contact-p"><span class=""><?= Yii::t('app','Email'); ?>:</span> <a href="mailto:<?= config_email ?>"><?= config_email ?></a></p>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="contact_form">
                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group text_box">
                                <?= $form->field($model, 'name')->textInput() ?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group text_box">
                                <?= $form->field($model, 'email')->textInput() ?>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group text_box">
                                <?= $form->field($model, 'subject')->textInput() ?>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group text_box">
                                <?= $form->field($model, 'body')->textarea(['rows' => 3]) ?>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group text_box">
                                <?= $form->field($model, 'verifyCode')->widget(Captcha::className()) ?>
                            </div>
                        </div>
                    </div>
                    <?= Html::submitButton('Send Message', ['class' => 'btn custom-btn', 'name' => 'contact-button']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="map-section">
    <div class="map">
        <div class="mapouter">
            <div class="gmap_canvas">
                <iframe  id="gmap_canvas" src="https://maps.google.com/maps?q=dubai&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            </div>
        </div>
    </div>
</section>