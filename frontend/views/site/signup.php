<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\City;
use common\models\Occupation;
use kartik\select2\Select2;
use borales\extensions\phoneInput\PhoneInput;
use yii\web\JsExpression;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
$language_id = 1;
$all_cities = ArrayHelper::map(City::find()->joinWith('cityName as cn')->where(['cn.language_id'=>$language_id])->asArray()->all(), 'id', 'cityName.city_name');
$all_occupations = ArrayHelper::map(Occupation::find()->joinWith('occupationName as cn')->where(['cn.language_id'=>$language_id])->asArray()->all(), 'id', 'occupationName.occupation_name');
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app','Please fill out the following fields to signup') ?>:</p>

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <div class="row">
                <div class="col-sm-4">
                    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-4">

                    <?= $form->field($model, 'phone_number')->widget(PhoneInput::className(),[
                        'jsOptions'=>['initialCountry'=>'auto','geoIpLookup'=>new JsExpression('function(callback) { 
				$.get(\'https://ipinfo.io\', function() {}, "jsonp").always(function(resp) {
					  var countryCode = (resp && resp.country) ? resp.country : "";
					  callback(countryCode);
					});
				}')]
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'email') ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'occupation_id')->widget(Select2::classname(), [
                        'data' =>$all_occupations,
                        'options' => ['placeholder' => 'Please select ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'dob')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Enter birth date ...','readonly'=>true],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'endDate'=>'+0d',
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>
                </div>
                <div class="w-100 col-sm-12"></div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'password')->passwordInput() ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'password_repeat')->passwordInput() ?>
                </div>
            </div>
                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
            <?= yii\authclient\widgets\AuthChoice::widget([
                'baseAuthUrl' => ['site/auth'],
                'popupMode' => false,
            ]) ?>
        </div>
    </div>
</div>
