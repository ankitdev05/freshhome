<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\User;
use yii\db\Expression;
$session = Yii::$app->session;
$screen_id = $session->get('screen_id') ?? 1;
$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);
$long = $session->get('long');
$lat = $session->get('lat');
if (!Yii::$app->user->isGuest)
    if(empty($long) && empty($lat)){
        $online_suppliers = User::find()->where(['availability'=>'online','status'=>10,'is_test_user'=>0,'is_supplier'=>'yes','supplier_type'=>$screen_id])->andWhere(['and',['NOT IN','id',yii::$app->user->id],['>','subscription_end',time()]])->limit(5)->all();
    }else{
        $online_suppliers = User::find()->select(new Expression("tbl_user.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance  "))->where(['availability'=>'online','status'=>10,'is_test_user'=>0,'is_supplier'=>'yes','supplier_type'=>$screen_id])->andWhere(['and',/*['NOT IN','id',yii::$app->user->id],*/['>','subscription_end',time()]])->orderBy('distance')->limit(5)->all();
    }

else
    $online_suppliers = User::find()->where(['availability'=>'online','status'=>10,'is_test_user'=>0,'is_supplier'=>'yes','supplier_type'=>$screen_id])->andWhere(['>','subscription_end',time()])->limit(5)->all();

?>
<section class="order-home-food mt-4">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 p-0">
                <div class="middle-banner" style="background-image:url(https://freshhomee.com/web/frontend/web/img/images/banner1.jpg);">
                    <div class="container pt-5 bannerarabic">
                        <div class="row pt-2">
                            <div class="col-md-10">
                                <h3 class="mb-0">Order Home Cooked Food</h3>
                                <span>Awesome Quality at affordable price</span>
                                <p class="mt-3">The quick brown fox jumps over the lazy dog. DJ's Flock by When MTV as quiz prog. Junk MTV quiz graced by fox helps.</p>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btnshopow">Shop Now</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if(!empty($online_suppliers)){ ?>
<section class="latest-products order-from-kitchens mt-4 p-0">
    <div class="container-fluid">
        <div class="row justify-content-md-center">
            <div class="col-md-11">
                <div class="latest-pro-slider">
                    <div class="form-row">
                        <div class="col-md-6 col-7">
                            <h5 class="latest-product"><?= Yii::t('app', 'Order Now From Kitchens') ?></h5>
                        </div>
                        <div class="col-md-6 col-5">
                            <p class="text-right mb-2 viewallpro">
                                <?= Html::a(Yii::t('app', 'View All'),$website_url.'online-kitchen') ?>
                            </p>
                        </div>
                    </div>
                    <hr class="speratorline mt-0"/>

                    <div id="demos">
                        <div class="large-12 columns">
                            <div class="owl-carousel owl-theme">
                                <?php foreach($online_suppliers as $online_supplier){ ?>
                                    <div class="item productslide1 kitchenssliders text-center">
                                        <?php if(empty($online_supplier->profile_pic)) $online_supplier->profile_pic = 'no_image.png';
                                        echo Html::img($online_supplier->getSupplierImage($online_supplier->profile_pic,150,150),['alt'=>$online_supplier->name,'title'=>$online_supplier->name,'class' => 'img-fluid mx-auto d-block active']) ?>
                                        <div class="product-info kitchensinfo">
                                            <h5 class="mb-0 mt-2"><?= Html::a($online_supplier->name,$online_supplier->profileUrl) ?></h5>
                                        </div>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>