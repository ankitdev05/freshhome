<?php
use yii\helpers\Html;
use himiklab\thumbnail\EasyThumbnailImage;

$title = 'Guest';
$user_link = 'javascript:void(0)';
if (!Yii::$app->user->isGuest){
    $session = Yii::$app->session;
    $is_supplier = $session->has('is_supplier');
    $user_link = Yii::$app->request->baseUrl.'/profile';
    if($is_supplier)
        $user_link = Yii::$app->request->baseUrl.'/supplier';
    $title = ucwords(Yii::$app->user->identity->name);
}
$language = 'Arabic';
$link = Yii::$app->request->baseUrl.'/?lang=ar';
if(language_id > 1){
    $language = 'English';
    $link = Yii::$app->request->baseUrl.'/?lang=en';
}
?>
<div class="mobilemenus d-md-none d-lg-none d-xl-none">
    <div class="msidenavbar">
        <ul class="mlanguageicon">
            <?php
            foreach($all_countries as $country){
                $cimage = EasyThumbnailImage::thumbnailFileUrl(
                    '../../frontend/web/images/language/'.$country['flag'],42,22,EasyThumbnailImage::THUMBNAIL_INSET
                );
                $clink = Yii::$app->request->baseUrl.'/?country='.$country['iso_code_2'];
                ?>
                <li><?= Html::a(Html::img($cimage,['alt' => $country['name'], 'title' => $country['name']]),$clink) ?></li>
            <?php } ?>
            <li class="langaugeeng"><a href="<?= $link ?>"><?= $language ?></a></li>
        </ul>
        <div class="useraccountlogin">
            <a href="<?= $user_link ?>"><img src="<?= Yii::$app->request->baseUrl ?>/img/user.svg" class="img-fluid" width="40" alt="User"></a>
            <span>Hi,  <a href="<?= $user_link ?>" class="text-white"><?= $title ?></a></span>
            <cite class="closenavbar float-right mt-2"><a href="javascript:void(0)"><i class="fas fa-times-circle text-white"></i></a></cite>
        </div>
    </div>
    <div class="m-menus">
        <ul class="m-accountmenus">
            <li><a href="<?= $website_url ?>"><?= Yii::t('app', 'Home') ?></a></li>
            <?php
            if($is_supplier && !Yii::$app->user->isGuest)
                echo $this->render('_supplier_links_mobile',['website_url'=>$website_url,'count_orders' => $count_orders]);
            else
                echo $this->render('_user_links_mobile',['website_url'=>$website_url])
            ?>
        </ul>
    </div>
    <?php if (!Yii::$app->user->isGuest){ ?>
        <div class="logoutaccount">
            <?= Html::a('<p>'.Yii::t('app', 'Logout').'&nbsp;<i class="fas fa-sign-out-alt"></i></p>',$website_url.'logout',['data-method' => 'post']) ?>

        </div>
    <?php } ?>
</div>