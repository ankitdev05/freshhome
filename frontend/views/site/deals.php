<?php
use yii\widgets\ListView;
use common\models\SortMethod;

$this->title = empty($model->meta_title) ? config_title : $model->meta_title;
\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => empty($model->meta_description) ? config_meta_description : $model->meta_description
]);
\Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' =>  empty($model->meta_keyword) ? config_meta_keywords : $model->meta_keyword
]);
$this->params['breadcrumbs'][] = $model->title;

$sort_method = SortMethod::find()->where(['type' => 2])->all();
$url = Yii::$app->request->baseUrl.'/hot-deals?sort_id=';
?>
<section class="product-filter-section productpage pb-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 mt-3">
                <div class="product-filter mt-5">
                <?= $this->render('//products/theme/_left_panel')  ?>
                </div>
            </div>
            <div class="col-md-10">
                <div class="productlist mt-4">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="mt-1"><?= $model->title ?></h5>
                        </div>
                        <div class="col-md-6">
                            <?php if(!empty($sort_method)): ?>
                            <div class="sortfeature text-right">
                                <div class="dropdown show">
                                    <a class="btn dropdown-toggle btnfeatured" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span>Sort By :</span>&nbsp;<?= $sort_order_name ?>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <?php foreach ($sort_method as $method): ?>
                                            <a class="dropdown-item" href="<?= $url.$method->id ?>"><?= $method->sort_name ?></a>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="latest-products best-selling-product allproductlisting mt-3">
                    <div class="row">
                        <?=  ListView::widget([
                            'layout' => '{items}',
                            'itemView' => '//products/theme/_product',
                            'dataProvider' => $dataProvider,
                            'options' => [
                                'tag' => 'div',
                                'class' => 'row',
                                'id' => 'holt-deals',
                            ],
                            'itemOptions' => [
                                'tag' => false
                            ],
                        ]); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
