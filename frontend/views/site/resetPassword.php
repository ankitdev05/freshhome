<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app','Reset password');

?>
<div class="bg-sec">
    <div class="container">
        <div class="heading">
            <h3><?= Html::encode($this->title) ?></h3>
            <div class="divider"></div>
        </div>
    </div>
</div>
<section class="contact_info help">
    <div class="container">
        <p><?= Yii::t('app','Please choose your new password:') ?></p>
        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
        <div class="row ">
            <div class="col-sm-6">
                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
            </div>
            <div class="col-sm-12">
                <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn custom-btn']) ?>
            </div>

        </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>
