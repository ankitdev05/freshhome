<?php
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use common\models\City;
use common\models\Country;
use common\models\MainCategories;
use kartik\select2\Select2;
use kartik\date\DatePicker;

$language_id = language_id;
$all_cities = ArrayHelper::map(City::find()->joinWith('cityName as cn')->where(['cn.language_id'=>$language_id])->asArray()->all(), 'id', 'cityName.city_name');
$main_cats = ArrayHelper::map(MainCategories::find()->where(['status' =>1])->asArray()->all(),'id','name');
$nationality = ArrayHelper::map(Country::find()->orderBy('countryname asc')->asArray()->all(),'code','nationality');

$this->title = 'Switch to seller | '.config_title;
$model->name = '';
?>
<section class="signup-sec pattern">
    <div class="container">
        <div class="signup-content">
            <div class="signup-body">
                <div class="signup-heading">
                    <h3 class="text-uppercase text-center m-b-30"> Sign Up as supplier </h3>
                </div>
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'supplier_type')->widget(Select2::classname(), [
                            'data' =>$main_cats,
                            'options' => ['placeholder' => 'Please select ...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ])->label('What kind of products you want to sell ?') ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label('Business name') ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'city')->widget(Select2::classname(), [
                            'data' =>$all_cities,
                            'options' => ['placeholder' => 'Please select ...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'nationality')->widget(Select2::classname(), [
                            'data' =>$nationality,
                            'options' => ['placeholder' => 'Please select ...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'dob')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter birth date ...','readonly'=>true],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'endDate'=>'+0d',
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]); ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'latitude')->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'longitude')->hiddenInput()->label(false) ?>
                        <div class='map' id='map_canvas' style="height:300px;"></div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'building_no')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'flat_no')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'floor_no')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'landmark')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-md-12 form-group col-sm-12 col-xs-12 submit-bt">
                        <?= Html::submitButton('Signup', ['class' => 'btn custom-btn', 'name' => 'signup-button']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>
<?php
echo $this->render('//site/_location',['model'=>$model]);