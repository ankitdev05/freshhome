<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\City;
use common\models\Occupation;
use common\models\MainCategories;
use kartik\select2\Select2;

$this->title =  Yii::t('app','Signup');
$this->params['breadcrumbs'][] = $this->title;
$language_id = language_id;
$all_cities = ArrayHelper::map(City::find()->joinWith('cityName as cn')->where(['cn.language_id'=>$language_id])->asArray()->all(), 'id', 'cityName.city_name');
$all_occupations = ArrayHelper::map(Occupation::find()->joinWith('occupationName as cn')->where(['cn.language_id'=>$language_id])->asArray()->all(), 'id', 'occupationName.occupation_name');
$main_cats = ArrayHelper::map(MainCategories::find()->where(['status' =>1])->asArray()->all(),'id','name');
?>

<section class="signup-sec pattern">
    <div class="container">
        <div class="signup-content">
            <div class="signup-body">
                <div class="signup-heading">
                    <h3 class="text-uppercase text-center m-b-30"><?= Yii::t('app','Signup') ?> </h3>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'form-signup-final']); ?>
                <input type="hidden" name="g-recaptcha-response" value="1" />
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'supplier_type')->widget(Select2::classname(), [
                            'data' =>$main_cats,
                            'options' => ['placeholder' => 'Please select ...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ])->label('What kind of products you want to sell ?') ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true,'readonly' =>true]) ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'username')->textInput(['maxlength' => true])->label('User Name(must be unique)') ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'email') ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'dob')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter birth date ...','readonly'=>true],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'endDate'=>'+0d',
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]); ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'city')->widget(Select2::classname(), [
                            'data' =>$all_cities,
                            'options' => ['placeholder' => 'Please select ...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'latitude')->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'longitude')->hiddenInput()->label(false) ?>
                        <div class='map' id='map_canvas' style="height:300px;"></div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'building_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'flat_no')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'floor_no')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'landmark')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'password')->passwordInput() ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'password_repeat')->passwordInput() ?>
                    </div>
                    <div class="col-md-12 form-group col-sm-12 col-xs-12 submit-bt">
                        <?= Html::submitButton('Signup', ['class' => 'btn custom-btn', 'name' => 'signup-button']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="line-div"></div>
                    <p class="text-center"> Already have an account with us ? <a class="color" href="<?= yii::$app->request->baseUrl.'/login' ?>"> Sign In </a> </p>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
echo $this->render('//site/_location',['model'=>$model]);