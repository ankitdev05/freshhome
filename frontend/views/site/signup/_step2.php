<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;
use yii\web\JsExpression;
$this->title = Yii::t('app','Signup');
?>
<section class="signup-sec pattern">
    <div class="container">
        <div class="signup-content">
            <div class="signup-body">
                <h3 class="text-uppercase text-center m-b-30"><?= Yii::t('app','Sign Up Step 1'); ?></h3>

                <?php
//                ,'onsubmit' => '  event.preventDefault();'
                $form = ActiveForm::begin(['enableClientScript'=>false,'enableClientValidation'=>false,'options' => ['enctype' => 'multipart/form-data'],'id' => 'sign-in-form','options' => ['style'=>'display:none;','onsubmit' => 'return false']]); ?>
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <?= $form->field($model, 'phone_number')->widget(PhoneInput::className(),[

                        'jsOptions'=>['nationalMode'=>false,'autoHideDialCode'=>false,'initialCountry'=>'auto','geoIpLookup'=>new JsExpression('function(callback) { 
				            $.get(\'https://ipinfo.io\', function() {}, "jsonp").always(function(resp) {
					        var countryCode = (resp && resp.country) ? resp.country : "";
					        callback(countryCode);
					       
					        
					});
				}')]
                    ]) ?>
                </div>
                <div class="col-md-12 col-sm-12 form-group col-xs-12 submit-bt">
                    <?= Html::button('Submit', ['class' => 'custom-btn btn', 'id' => 'sign-in-button-1']) ?>
                    <?= Html::button('Confirm your number', ['class' => 'custom-btn btn', 'name' => 'profile-phone','id' => 'sign-in-button','style' =>'display:none;']) ?>
                </div>
                <?php ActiveForm::end(); ?>
                <?php $form = ActiveForm::begin(['enableClientScript'=>false,'enableClientValidation'=>false,'options' => ['enctype' => 'multipart/form-data'],'id' => 'verification-code-form','options' => ['style'=>'display:none;']]); ?>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'verification_code')->textInput(['id' => 'verification-code']) ?>
                        <div class="clearfix"></div>

                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                        <a class="color" href="javascript:void(0);" onClick="$('#verification-code').val('');$('#sign-in-button').click();"><?= Yii::t('app','Resend Code?') ?>  </a>
                    </div>
                    <div class="col-md-12 form-group  col-sm-12 col-xs-12 submit-bt">
                        <?= Html::submitButton(Yii::t('app','Submit'), ['id' => 'verify-code-button','class' => 'btn custom-btn', 'name' => 'verify-phone']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="line-div"></div>
                    <p class="text-center"><?= Yii::t('app','Already have an account with us?'); ?> <a class="color" href="<?= yii::$app->request->baseUrl.'/login' ?>"><?= Yii::t('app','Sign In'); ?></a> </p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$this->registerJs('
    $("#sign-in-button-1").on("click",function(){
        $.ajax({
             url : website_url+"checknumber",
             method : "POST",
             data : $("#sign-in-form").serialize(),
             dataType: "json",
             success : function(json){
                if(json["success"]){
                    $("#message").html(json["message"]);
                    $("#sign-in-button").click();
                }
                else{
                    warningPopup(json["error"]);
                    return false;
                }
            }
        })
    });
');

$this->registerJsFile('https://www.gstatic.com/firebasejs/6.4.2/firebase-app.js', [
    'position' => \yii\web\View::POS_HEAD
]);
$this->registerJsFile('https://www.gstatic.com/firebasejs/6.4.2/firebase-auth.js',[
    'position' => \yii\web\View::POS_HEAD
]);
$this->registerJsFile('https://www.gstatic.com/firebasejs/6.4.2/firebase-firestore.js',[
    'position' => \yii\web\View::POS_HEAD
]);

$this->registerJs('
var eventhandler = function(e) {
   e.preventDefault();      
}
var firebaseConfig = {
    apiKey: "AIzaSyBI2zYYmUyDTh9nwX8czmiOsoJa_dyw_LI",
    authDomain: "freshhome-eb8df.firebaseapp.com",
    databaseURL: "https://freshhome-eb8df.firebaseio.com",
    projectId: "freshhome-eb8df",
    storageBucket: "freshhome-eb8df.appspot.com",
    messagingSenderId: "350237825472",
    appId: "1:350237825472:web:0229272c446eeac8"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

	window.onload = function() {
		onSignOutClick();
		firebase.auth().onAuthStateChanged(function(user) {
			
			if (user) {
				var uid = user.uid;
				var email = user.email;
				var photoURL = user.photoURL;
				var phoneNumber = user.phoneNumber;
				var isAnonymous = user.isAnonymous;
				var displayName = user.displayName;
				var providerData = user.providerData;
				var emailVerified = user.emailVerified;
				//console.log(user);
			}
			updateSignInButtonUI();
			  updateSignInFormUI();
			  updateSignOutButtonUI();
			  updateSignedInUserStatusUI();
			  updateVerificationCodeFormUI();
		});
		
		document.getElementById(\'user-phone_number\').addEventListener(\'keyup\', updateSignInButtonUI);
		document.getElementById(\'user-phone_number\').addEventListener(\'change\', updateSignInButtonUI);
		document.getElementById(\'verification-code\').addEventListener(\'keyup\', updateVerifyCodeButtonUI);
		document.getElementById(\'verification-code\').addEventListener(\'change\', updateVerifyCodeButtonUI);
		document.getElementById(\'verification-code-form\').addEventListener(\'submit\', onVerifyCodeSubmit);
		
		window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(\'sign-in-button\', {
		  \'size\': \'invisible\',
		  \'callback\': function(response) {
			// reCAPTCHA solved, allow signInWithPhoneNumber.
			onSignInSubmit();
			
		  }
		});
		recaptchaVerifier.render().then(function(widgetId) {
		  window.recaptchaWidgetId = widgetId;
		  updateSignInButtonUI(true);
		 
		});
		
	};
	
	function updateSignInButtonUI(allow=false) {
		
		document.getElementById(\'sign-in-button\').disabled =
        !isPhoneNumberValid()
        || !!window.signingIn;
        
        
	}
	function updateVerifyCodeButtonUI() {
		document.getElementById(\'verify-code-button\').disabled =
			!!window.verifyingCode
			|| !getCodeFromUserInput();
	}
	function updateVerificationCodeFormUI() {
    if (!firebase.auth().currentUser && window.confirmationResult) {
      document.getElementById(\'verification-code-form\').style.display = \'block\';
    } else {
      document.getElementById(\'verification-code-form\').style.display = \'none\';
    }
  }
	function updateSignOutButtonUI() {
		
	}
	function updateSignedInUserStatusUI() {
		var user = firebase.auth().currentUser;
		if (user) {
		console.log(JSON.stringify(user, null, \'  \'));
            $("#sign-in-form").removeAttr(\'onsubmit\');
			 $("#sign-in-form").submit();

            
		}
	}
	function onSignOutClick() {
		firebase.auth().signOut();
	  }
	function onSignInSubmit() {
		if (isPhoneNumberValid()) {
		  window.signingIn = true;
		  updateSignInButtonUI();
		  var phoneNumber = getPhoneNumberFromUserInput();
		  var appVerifier = window.recaptchaVerifier;
		  firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
			  .then(function (confirmationResult) {
				// SMS sent. Prompt user to type the code from the message, then sign the
				// user in with confirmationResult.confirm(code).
				window.confirmationResult = confirmationResult;
				window.signingIn = false;
				updateSignInButtonUI();
				updateVerificationCodeFormUI();
				updateVerifyCodeButtonUI();
				updateSignInFormUI();
			  }).catch(function (error) {
				// Error; SMS not sent
				//console.error(\'Error during signInWithPhoneNumber\', error);
				window.alert(\'Error during signInWithPhoneNumber:\n\n\'
					+ error.code + \'\n\n\' + error.message);
				window.signingIn = false;
				updateSignInFormUI();
				updateSignInButtonUI();
			  });
		}
	  }
	function updateSignInFormUI() {
		if (firebase.auth().currentUser || window.confirmationResult) {
		  document.getElementById(\'sign-in-form\').style.display = \'none\';
		} else {
		  resetReCaptcha();
		  document.getElementById(\'sign-in-form\').style.display = \'block\';
		}
	  }
	  function getCodeFromUserInput() {
		return document.getElementById(\'verification-code\').value;
	  }
	  
	  function getPhoneNumberFromUserInput() {
		return document.getElementById(\'user-phone_number\').value;
	  }
	  function isPhoneNumberValid() {
		var pattern = /^\+[0-9\s\-\(\)]+$/;
		var phoneNumber = getPhoneNumberFromUserInput();
		if(phoneNumber.length > 5)
			return phoneNumber.search(pattern) !== -1;
	  }
	  
	function resetReCaptcha() {
    if (typeof grecaptcha !== \'undefined\'
        && typeof window.recaptchaWidgetId !== \'undefined\') {
      grecaptcha.reset(window.recaptchaWidgetId);
    }
  }
	function onVerifyCodeSubmit(e) {
    e.preventDefault();
    if (!!getCodeFromUserInput()) {
      window.verifyingCode = true;
      updateVerifyCodeButtonUI();
      var code = getCodeFromUserInput();
      confirmationResult.confirm(code).then(function (result) {
        // User signed in successfully.
        var user = result.user;
        window.verifyingCode = false;
        window.confirmationResult = null;
        updateVerificationCodeFormUI();
      }).catch(function (error) {
        // User couldn\'t sign in (bad verification code?)
        console.error(\'Error while checking the verification code\', error);
        window.alert(\'Error while checking the verification code:\n\n\'
            + error.code + \'\n\n\' + error.message);
        window.verifyingCode = false;
        updateSignInButtonUI();
        updateVerifyCodeButtonUI();
      });
    }
  }  
', yii\web\View::POS_HEAD);