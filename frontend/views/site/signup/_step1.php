<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;
use yii\web\JsExpression;

$this->title = Yii::t('app','Signup');
?>
<section class="signup-sec pattern">
    <div class="container">
        <div class="signup-content">
            <div class="signup-body">
                <h3 class="text-uppercase text-center m-b-30"><?= Yii::t('app','Sign Up Step 1'); ?></h3>
                <?php $form = ActiveForm::begin(['id' => 'form-signup-1']); ?>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-12">
                        <?= $form->field($model, 'phone_number')->widget(PhoneInput::className(),[
                            'jsOptions'=>['initialCountry'=>'auto','geoIpLookup'=>new JsExpression('function(callback) { 
				$.get(\'https://ipinfo.io\', function() {}, "jsonp").always(function(resp) {
					  var countryCode = (resp && resp.country) ? resp.country : "";
					  callback(countryCode);
					});
				}')]
                        ]) ?>
                    </div>
                    <div class="col-md-12 form-group col-sm-12 col-xs-12 submit-bt">
                        <?= Html::submitButton('Signup', ['class' => 'btn custom-btn', 'name' => 'signup-button-1']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="line-div"></div>
                    <p class="text-center"><?= Yii::t('app','Already have an account with us?'); ?> <a class="color" href="<?= yii::$app->request->baseUrl.'/login' ?>"><?= Yii::t('app','Sign In'); ?></a> </p>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
.invalid-feedback {
    display: block;
}
</style>