<?php

use yii\helpers\Url;
use yii\bootstrap4\Html;

$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);

$this->registerJsFile($website_url.'js/slick.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<section class="banner-section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 p-0">
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img class="d-block w-100" src="https://freshhomee.com/web/frontend/web/img/images/banner1.jpg" alt="First slide">
						</div>
						<div class="carousel-item">
							<img class="d-block w-100" src="https://freshhomee.com/web/frontend/web/img/images/banner1.jpg" alt="Second slide">
						</div>
						<div class="carousel-item">
							<img class="d-block w-100" src="https://freshhomee.com/web/frontend/web/img/images/banner1.jpg" alt="Third slide"> 
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="banner-sec d-none"> 
    <div id="bannerslider">
        <div class="item">
            <div class="img-div" style="background-image: url('<?= $website_url ?>img/slider-3.jpg');"></div>
        </div>
        <div class="item">
            <div class="img-div" style="background-image: url('<?= $website_url ?>img/slider-1.jpg');"></div>
        </div>
        <div class="item">
            <div class="img-div" style="background-image: url('<?= $website_url ?>img/2.jpg');"></div>
        </div>
    </div>
</section>
<?php
$this->registerJs('
    $("#bannerslider").slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        fade: true,
        cssEase: "linear",
    });
	$("")
'); 