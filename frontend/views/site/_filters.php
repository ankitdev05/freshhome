<?php

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;
use common\models\City;
use common\models\Filters;
use common\models\Category;
use common\models\SortMethod;
use common\models\MainCategories;
use yii\bootstrap4\ActiveForm;
$country = country_data;

$language_id = language_id;
$model = new Filters();
$model->load(Yii::$app->request->get());
$sortMethods = ArrayHelper::map(SortMethod::find()->where(['status'=>'active', 'type' => 2])->asArray()->all(),'id','sort_name');
$menuCategories = ArrayHelper::map(MainCategories::find()->asArray()->all(),'id','name');

$cities = ArrayHelper::map(City::find()->select('*')->joinWith('cityName as cn')->where(['status'=>'active','cn.language_id'=>$language_id,'country_id' => $country['country_id']])->orderBy('cn.city_name asc')->asArray()->all(),'city_id','city_name');

$session = Yii::$app->session;
$screen_id = $session->get('screen_id');
$categories = Category::getAllCategories($screen_id,['c1.active' => 1]);

$cats = [];
if(!empty($categories)){
    foreach ($categories as $category)
        $cats[$category['category_id']]  = strip_tags(html_entity_decode($category['name'], ENT_QUOTES, 'UTF-8'));
}

?>
<?php Pjax::begin(); ?>
<?php $form = ActiveForm::begin(['id' => 'filter-products','method'=>'GET','action' => Url::to(Yii::$app->request->baseUrl.'/products',true)]); ?>
<section class="search-sec d-none">
    <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="search-box">
                        <div class="input-group">
                            <?= $form->field($model,'sort',['options'=>['tag'=>false]])->dropDownList($sortMethods,['class' => 'custom-select','prompt' => Yii::t('app', 'Filter By')])->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="search-box">
                        <div class="input-group">
                            <?= $form->field($model, 'title',['options' => ['tag' =>false]])->textInput(['class'=>'form-control cus-input','placeholder' => Yii::t('app', 'Search products')])->label(false) ?>
                            <?= $form->field($model,'categories',['options'=>['tag'=>false]])->dropDownList($cats,['class' => 'custom-select','prompt' =>  Yii::t('app', 'Category')])->label(false) ?>
                            <div class="input-group-append">
                                <?= Html::submitButton(Yii::t('app', '<i class="fa fa-search"></i>'), ['class' => 'btn btn-outline-secondary']) ?>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="search-box">
                        <div class="input-group">
                            <?= $form->field($model,'city',['options'=>['tag'=>false]])->dropDownList($cities,['class' => 'custom-select','prompt' =>  Yii::t('app', 'Select City')])->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</section>
<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>
<?php
$this->registerCss("
.was-validated .form-control:valid, .form-control.is-valid,.was-validated .custom-select:valid, .custom-select.is-valid {
  background-image: none;
}
");
$this->registerJs("
    $('.search-sec select').on('change',function(){
        $('#filter-products').submit();
    })
"); 