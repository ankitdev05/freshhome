<?php
use common\models\Category;
use common\models\HomeCategory;

$this->title = Yii::t('app','Shop by category');
$home_categories = HomeCategory::find()->select('*')->joinWith('homeCategoryDescription as hd')->where(['status' => 1,'hd.language_id' => language_id])->andWhere(['not IN','tbl_home_category.home_category_id',[29,30]])->asArray()->all();
?>
<section class="mt-5 mb-5">
    <div class="container-fluid">
        <div class="row">

            <?php
            if(!empty($home_categories)):
                foreach ($home_categories as $home_category):
                    $categories = Category::find()->select('*')->joinWith('categoryDescription as cd')->where(['and',['home_category_id' => $home_category['home_category_id']],['IS','parent_id',NULL],['active' => 1],['cd.language_id' => language_id]])->all();


                    ?>
                    <div class="col-md-3">
                        <div class="listingcategories mb-4">
                            <h5><?= $home_category['home_category_name'] ?></h5>
                            <?php if(!empty($categories)): ?>
                                <ul>
                                    <?php foreach ($categories as $category): ?>
                                        <li><a href="<?= $category->categoryUrl ?>"><?= $category->categoryDescription->category_name ?></a></li>
                                    <?php endforeach; ?>
                                </ul>

                            <?php endif; ?>
                        </div>
                    </div>
                <?php
                endforeach;
            endif;
            ?>
        </div>
    </div>
</section>