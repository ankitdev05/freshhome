<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Menu;
use common\models\MainCategories;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

$session = Yii::$app->session;
$screen_id = $session->get('screen_id') ?? 1;
$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);

$all_categories = MainCategories::find()->where(['status'=>1])->all();
$dataProviders = [];
$long = $session->get('long');
$lat = $session->get('lat');

foreach($all_categories as $k=>$all_category){
    if (!Yii::$app->user->isGuest){
        if(empty($long) && empty($lat)){
            $query = Menu::find()->joinWith('supplierInfo')->select('tbl_menu.*,SUM(op.quantity) AS TotalQuantity')->joinWith('orderProducts as op')->where(['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0,'tbl_menu.product_type'=>$all_category->id])->andWhere(['NOT IN','tbl_menu.supplier_id',yii::$app->user->id])->andWhere(['>','subscription_end',time()])->groupBy('tbl_menu.id')->orderBy('TotalQuantity desc')->limit(5);
        }else{
            $query = Menu::find()->joinWith('supplierInfo')->
            select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance ,SUM(op.quantity) AS TotalQuantity "))->
            joinWith('orderProducts as op')->
            where(['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0,'tbl_menu.product_type'=>$all_category->id])->
            andWhere(['NOT IN','tbl_menu.supplier_id',yii::$app->user->id])->andWhere(['>','subscription_end',time()])
                ->groupBy('tbl_menu.id')->
            orderBy('distance,TotalQuantity desc')->limit(5);
        }

    }
    else
        $query = Menu::find()->joinWith('supplierInfo')->select('tbl_menu.*,SUM(op.quantity) AS TotalQuantity,tbl_menu.id as id')->joinWith('orderProducts as op')->where(['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0,'tbl_menu.product_type'=>$all_category->id])->andWhere(['>','subscription_end',time()])->groupBy('tbl_menu.id')->orderBy('TotalQuantity desc')->limit(5);

    $dataProvider =  new ActiveDataProvider([
        'query' => $query,
        'pagination' => false,
        
    ]);
    $count = $dataProvider->getTotalCount();
    if($count>0){
        $dataProviders[$k]['dataProvider'] = $dataProvider;
        $dataProviders[$k]['category'] = $all_category;
    }

}
if(!empty($dataProviders)) {
    ?>
	

	
    <section class="latest-products d-none">
        <div class="container">
            <div class="inner-product">
                <h4 class="latestprohead"><?= Yii::t('app', 'Best Sellers') ?> <span class="view-product"><?= Html::a(Yii::t('app', 'View All'), $website_url . 'best-sellers') ?></span></h4>
                <div class="product-slider">
                    <div class="responsive">
                        <?php foreach($dataProviders as $dataProvider){ ?>
                            <?= ListView::widget([
                                'layout' => '{items}',
                                'itemView' => '//products/_bs_products',
                                'viewParams' => ['category' => $dataProvider['category']],
                                'dataProvider' => $dataProvider['dataProvider'],
                                'options' => [
                                    'tag' => false,
                                    'class' => 'responsive',
                                    'id' => 'list-wrapper',
                                ],
                                'itemOptions' => [
                                    'tag' => false
                                ],
                            ]); ?>
                           <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
	

	
	<section class="latest-products order-now-products mt-5">
		<div class="container-fluid">
			<div class="row justify-content-md-center">
				<div class="col-md-11">
					<div class="latest-pro-slider">
						<div class="row">
							<div class="col-md-6 col-8">
								<h5 class="latest-product">Order Now</h5>
							</div>
							<div class="col-md-6 col-4">
								<p class="text-right mb-2 viewallpro"><a href="">View All</a></p>
							</div>
						</div>
						<hr class="speratorline mt-0"/>
						<div id="demos">
							<div class="large-12 columns">
								<div class="owl-carousel owl-theme">
									<div class="item productslide1 order-active">
										<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
										<div class="product-info">
											<h4>The quick brown jumps over a lazy dog</h4>
											<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
											<div class="row">
												<div class="col-md-6 col-7 pr-0">
													<h5 class="mb-0 mt-1">AED 23.56</h5>
												</div>
												<div class="col-md-6 col-5 pl-0">
													<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-6">
													<span><i class="fas fa-cart-plus primary-color"></i></span>
												</div>
												<div class="col-md-6 col-6 text-right">
													<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="item productslide1">
										<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
										<div class="product-info">
											<h4>The quick brown jumps over a lazy dog</h4>
											<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
											<div class="row">
												<div class="col-md-6 col-7 pr-0">
													<h5 class="mb-0 mt-1">AED 23.56</h5>
												</div>
												<div class="col-md-6 col-5 pl-0">
													<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-6">
													<span><i class="fas fa-cart-plus primary-color"></i></span>
												</div>
												<div class="col-md-6 col-6 text-right">
													<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="item productslide1">
										<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
										<div class="product-info">
											<h4>The quick brown jumps over a lazy dog</h4>
											<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
											<div class="row">
												<div class="col-md-6 col-7 pr-0">
													<h5 class="mb-0 mt-1">AED 23.56</h5>
												</div>
												<div class="col-md-6 col-5 pl-0">
													<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-6">
													<span><i class="fas fa-cart-plus primary-color"></i></span>
												</div>
												<div class="col-md-6 col-6 text-right">
													<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="item productslide1">
										<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
										<div class="product-info">
											<h4>The quick brown jumps over a lazy dog</h4>
											<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
											<div class="row">
												<div class="col-md-6 col-7 pr-0">
													<h5 class="mb-0 mt-1">AED 23.56</h5>
												</div>
												<div class="col-md-6 col-5 pl-0">
													<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-6">
													<span><i class="fas fa-cart-plus primary-color"></i></span>
												</div>
												<div class="col-md-6 col-6 text-right">
													<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="item productslide1 order-active">
										<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
										<div class="product-info">
											<h4>The quick brown jumps over a lazy dog</h4>
											<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
											<div class="row">
												<div class="col-md-6 col-7 pr-0">
													<h5 class="mb-0 mt-1">AED 23.56</h5>
												</div>
												<div class="col-md-6 col-5 pl-0">
													<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-6">
													<span><i class="fas fa-cart-plus primary-color"></i></span>
												</div>
												<div class="col-md-6 col-6 text-right">
													<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="item productslide1">
										<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
										<div class="product-info">
											<h4>The quick brown jumps over a lazy dog</h4>
											<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
											<div class="row">
												<div class="col-md-6 col-7 pr-0">
													<h5 class="mb-0 mt-1">AED 23.56</h5>
												</div>
												<div class="col-md-6 col-5 pl-0">
													<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-6">
													<span><i class="fas fa-cart-plus primary-color"></i></span>
												</div>
												<div class="col-md-6 col-6 text-right">
													<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="item productslide1 order-active">
										<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
										<div class="product-info">
											<h4>The quick brown jumps over a lazy dog</h4>
											<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
											<div class="row">
												<div class="col-md-6 col-7 pr-0">
													<h5 class="mb-0 mt-1">AED 23.56</h5>
												</div>
												<div class="col-md-6 col-5 pl-0">
													<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-6">
													<span><i class="fas fa-cart-plus primary-color"></i></span>
												</div>
												<div class="col-md-6 col-6 text-right">
													<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="item productslide1">
										<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
										<div class="product-info">
											<h4>The quick brown jumps over a lazy dog</h4>
											<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
											<div class="row">
												<div class="col-md-6 col-7 pr-0">
													<h5 class="mb-0 mt-1">AED 23.56</h5>
												</div>
												<div class="col-md-6 col-5 pl-0">
													<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-6">
													<span><i class="fas fa-cart-plus primary-color"></i></span>
												</div>
												<div class="col-md-6 col-6 text-right">
													<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="item productslide1">
										<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
										<div class="product-info">
											<h4>The quick brown jumps over a lazy dog</h4>
											<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
											<div class="row">
												<div class="col-md-6 col-7 pr-0">
													<h5 class="mb-0 mt-1">AED 23.56</h5>
												</div>
												<div class="col-md-6 col-5 pl-0">
													<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-6">
													<span><i class="fas fa-cart-plus primary-color"></i></span>
												</div>
												<div class="col-md-6 col-6 text-right">
													<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="item productslide1">
										<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
										<div class="product-info">
											<h4>The quick brown jumps over a lazy dog</h4>
											<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
											<div class="row">
												<div class="col-md-6 col-7 pr-0">
													<h5 class="mb-0 mt-1">AED 23.56</h5>
												</div>
												<div class="col-md-6 col-5 pl-0">
													<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-6">
													<span><i class="fas fa-cart-plus primary-color"></i></span>
												</div>
												<div class="col-md-6 col-6 text-right">
													<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="item productslide1">
										<img src="https://freshhomee.com/web/frontend/web/img/images/food.jpg" class="img-fluid" alt=""/>
										<div class="product-info">
											<h4>The quick brown jumps over a lazy dog</h4>
											<span class="price-off">AED 50</span>&nbsp;&nbsp;<cite class="sale-off">20% OFF</cite>
											<div class="row">
												<div class="col-md-6 col-7 pr-0">
													<h5 class="mb-0 mt-1">AED 23.56</h5>
												</div>
												<div class="col-md-6 col-5 pl-0">
													<p class="text-right productreview mb-0"><i class="fas fa-star"></i>&nbsp;<span>3.4</span></p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-6">
													<span><i class="fas fa-cart-plus primary-color"></i></span>
												</div>
												<div class="col-md-6 col-6 text-right">
													<span><i class="far fa-heart primary-color"></i></span><span class="d-none"><i class="fas fa-heart primary-color"></i></span>
												</div>
											</div>
										</div>
									</div>
								</div>    
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
    <?php
    $this->registerJs("
    $('.responsive').slick({
        autoplay: true,
        margin: 30,
        autoplaySpeed: 2000,
        slidesToShow: 5,
        slidesToScroll: 5,
        nextArrow: '<i class=\"fa fa-arrow-right\"></i>',
        prevArrow: '<i class=\"fa fa-arrow-left\"></i>',
        responsive: [
              {
                 breakpoint: 1048,
                 settings: 
                  {
                    arrows: false,
                    centerMode: false,
                    slidesToShow: 4,
					slidesToScroll: 1
                  }
               },
			  {
                 breakpoint: 900,
                 settings: 
                  {
                    arrows: false,
                    centerMode: false,
                    slidesToShow: 3,
					slidesToScroll: 1
                  }
               },
			  {
                 breakpoint: 756,
                 settings: 
                  {
                    arrows: false,
                    centerMode: false,
                    slidesToShow: 2,
					slidesToScroll: 1
                  }
               },
              {
                breakpoint: 480,
                settings:
                {
                  arrows: false,
                  centerMode: false,
				  slidesToShow: 2,
				  slidesToScroll: 1
                }
              }
        ]
    });
");
} 