<?php

use common\models\Menu;
use yii\db\Expression;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

$query = Menu::find()->joinWith('supplierInfo');

$session = Yii::$app->session;
$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);
$long = $session->get('long');
$lat = $session->get('lat');

if(!empty($lat) && !empty($long) ){
    $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance ,SUM(op.quantity) AS TotalQuantity "))->orderBy('distance');
}else
    $query->select('tbl_menu.*');
$query->joinWith('orderProducts as op')
    ->where(['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0])->andWhere(['IN','tbl_menu.product_type',1])
    ->andWhere(['>','subscription_end',time()]);
//if (!Yii::$app->user->isGuest)
//    $query->andWhere(['NOT IN','tbl_menu.supplier_id',Yii::$app->user->id]);
$query->groupBy('tbl_menu.id');
$results = $query->orderBy(new Expression('tbl_menu.id DESC,CASE WHEN  tbl_menu.status = "online" THEN 0 WHEN tbl_menu.status = "offline" THEN 1 ELSE 2
		END '))->limit(10);

$dataProvider	=  new ActiveDataProvider([
    'query' => $query,
    'pagination' => false,

]);

?>
<section class="latest-products order-now-products mt-4">
    <div class="container-fluid">
        <div class="row justify-content-md-center">
            <div class="col-md-11">
                <div class="latest-pro-slider">
                    <div class="form-row">
                        <div class="col-md-6 col-7">
                            <h5 class="latest-product"><?= Yii::t('app', 'Order Now') ?></h5>
                        </div>
                        <div class="col-md-6 col-5">
                            <p class="text-right mb-2 viewallpro"><a href=""><?= Yii::t('app', 'View All') ?></a></p>
                        </div>
                    </div>
                    <hr class="speratorline mt-0"/>
                    <div id="demos">
                        <div class="large-12 columns">
                            <?=  ListView::widget([
                                'layout' => '{items}',
                                'itemView' => '//products/theme/_slider_products',
                                'dataProvider' => $dataProvider,
                                'options' => [
                                    'tag' => 'div',
                                    'class' => 'owl-carousel owl-theme',
                                    'id' => 'list-wrapper',
                                ],
                                'itemOptions' => [
                                    'tag' => false
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>