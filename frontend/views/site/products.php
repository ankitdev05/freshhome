<?php
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = 'Latest Products | '.config_name;

?>
<div class="bg-sec">
    <div class="container">
        <div class="heading">
            <h3><?= $main_category->name ?></h3>
            <div class="divider"></div>
        </div>
    </div>
</div>

<?= $this->render('//site/_filters') ?>
<?php if(isset($_GET['Filters'])){ ?>
    <div class="container">
		<div class="float-right mt-2">
			<div class="col-md-12 p-0">
				<a class="float-right" href="<?= yii::$app->request->baseUrl.'/products' ?>">Clear filter</a>
			</div>
		</div>
    </div>
<?php } ?>
<section class="product">

    <div class="container">

        <?php Pjax::begin(); ?>
        <?=  ListView::widget([
            'layout' => "<div class='col-sm-12'>{summary}</div>\n {items}\n<div class='col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
            'dataProvider' => $dataProvider,
            'itemView' => '//products/_view_product',
            'options' => [
                'tag' => 'div',
                'class' => 'form-row',
                'id' => 'list-wrapper',
            ],
            'itemOptions' => [
                'tag' => false
            ],
            'pager' => [
                'options' => ['class'=>'pagination justify-content-center'],
                'prevPageLabel' =>'<i class="fas fa-angle-double-left"></i>',
                'nextPageLabel' =>'<i class="fas fa-angle-double-right"></i>',
                'pageCssClass' => 'page-item',
                'disabledPageCssClass' => 'page-link disabled',
                'linkOptions' => ['class' => 'page-link'],
            ]
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</section>
