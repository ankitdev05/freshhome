<?php
use yii\helpers\Html;

?>
<div class="dropdown show">
    <a class="btn dropdown-toggle btnaccount px-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?= Yii::t('app','Hello, Sign in') ?><br/> <?= Yii::t('app','Accounts') ?>
    </a>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <?= Html::a(Yii::t('app', 'Login'),'#login',['class' => 'modal-tab dropdown-item', 'data-toggle' => 'modal', 'data-target' => '.bs-example-modal-lg', 'id' => 'header_login_btn_id','onClick'=>'return $("#login_id").click()']) ?>
        <?= Html::a(Yii::t('app', 'Sign Up'),'#signup',['class' => 'modal-tab dropdown-item', 'data-toggle' => 'modal', 'data-target' => '.bs-example-modal-lg', 'id' => 'header_login_btn_id','onClick'=>'return $("#signUp_id").click()']) ?>
    </div>
</div>