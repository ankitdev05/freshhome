<?php
use yii\helpers\Html;
use himiklab\thumbnail\EasyThumbnailImage;
$title = 'Guest';
$user_link = 'javascript:void(0)';
if (!Yii::$app->user->isGuest){
    $session = Yii::$app->session;
    $is_supplier = $session->has('is_supplier');
    $user_link = Yii::$app->request->baseUrl.'/profile';
    if($is_supplier)
        $user_link = Yii::$app->request->baseUrl.'/supplier';
    $title = ucwords(Yii::$app->user->identity->name);
}
$language = 'Arabic';
$link = Yii::$app->request->baseUrl.'/?lang=ar';
if(language_id > 1){
    $language = 'English';
    $link = Yii::$app->request->baseUrl.'/?lang=en';
}
?>
<div class="wrapper">
    <nav id="sidebar">
        <div class="site-language">
            <ul>
                <?php
                foreach($all_countries as $country){
                    $cimage = EasyThumbnailImage::thumbnailFileUrl(
                        '../../frontend/web/images/language/'.$country['flag'],42,22,EasyThumbnailImage::THUMBNAIL_INSET
                    );
                    $clink = Yii::$app->request->baseUrl.'/?country='.$country['iso_code_2'];
                    ?>
                    <li><?= Html::a(Html::img($cimage,['class' => 'img-fluid','alt' => $country['name'], 'title' => $country['name']]),$clink) ?></li>
                <?php } ?>
                <li><div class="m-arabictext"><a href="<?= $link ?>"><p class="mb-0"><?= $language ?></p></a></div></li>
            </ul>
        </div>

        <div class="sidebar-accountinfo w-100 float-left">
            <div id="dismiss" class="mt-5">
                <i class="fas fa-arrow-left"></i>
            </div>
            <div class="sidebar-header">
                <cite><i class="fas fa-user"></i></cite>&nbsp;<span>Hi, <a href="<?= $user_link ?>" class="text-white"><?= $title ?></a></span>
            </div>
        </div>
        <div class="float-left w-100">
            <ul class="list-unstyled components">
                <li><a href="<?= $website_url ?>"><?= Yii::t('app', 'Home') ?></a></li>
                <?php
                if($is_supplier && !Yii::$app->user->isGuest)
                    echo $this->render('_supplier_links_mobile',['website_url'=>$website_url,'count_orders' => $count_orders]);
                else
                    echo $this->render('_user_links_mobile',['website_url'=>$website_url]);
                ?>
            </ul>
        </div>
        <?php if (!Yii::$app->user->isGuest){ ?>
            <div class="logout-site">
                <p><?= Html::a(''.Yii::t('app', 'Logout').'&nbsp;<i class="fas fa-sign-out-alt"></i>',$website_url.'logout',['data-method' => 'post']) ?></p>
            </div>
        <?php } ?>
    </nav>
</div>
<?php
$this->registerJs("
    $('#sidebarCollapse').on('click', function () {
        // open sidebar
        $('#sidebar').addClass('active');
        // fade in the overlay
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    $('#dismiss, .overlay').on('click', function () {
        // hide sidebar
        $('#sidebar').removeClass('active');
        // hide overlay
        $('.overlay').removeClass('active');
    });
");
