<?php
use yii\helpers\Html;
?>
<div class="dropdown show">
    <a class="btn dropdown-toggle btnaccount px-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?= Yii::t('app', 'Hello') ?>, <?= ucwords(Yii::$app->user->identity->name) ?><br> <?= Yii::t('app', 'Accounts') ?>
    </a>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <a class="dropdown-item" href="<?= $website_url.'profile' ?>"><?= Yii::t('app','My Profile') ?></a>
        <a href="<?= $website_url.'profile?type=myorders' ?>" class="dropdown-item">
            <?= Yii::t('app', 'My Orders') ?>
        </a>
        <a href="<?= $website_url.'profile/mywallet?type=mywallet' ?>" class="dropdown-item">
            <?= Yii::t('app', 'My Wallet') ?>
        </a>
        <a href="<?= $website_url ?>profile?type=invite" class="dropdown-item">
            <?= Yii::t('app', 'Invite Friends') ?>
        </a>
        <a href="<?= $website_url.'switch-to-supplier' ?>" class="dropdown-item">
            <?= Yii::t('app', 'Switch to Sell') ?>
        </a>
        <?= Html::a(Yii::t('app', 'Logout'),$website_url.'logout',['data-method' => 'post','class' => 'dropdown-item']) ?>
    </div>
</div>
