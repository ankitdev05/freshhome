<?php
use yii\helpers\Html;
$supplier_type = $user_info->supplier_type;
?>
<div class="dropdown show">
    <a class="btn dropdown-toggle btnaccount px-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?= Yii::t('app', 'Hello') ?>, <?= ucwords(Yii::$app->user->identity->name) ?><br> <?= Yii::t('app', 'Accounts') ?>
    </a>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <a class="dropdown-item" href="<?= $website_url.'supplier' ?>">
            <?= Yii::t('app', 'My Profile') ?>
        </a>
        <?php if($supplier_type==1){ ?>
            <a class="dropdown-item" href="<?= $website_url.'supplier?type=listmenu' ?>">
                <?= Yii::t('app', 'My menu') ?>
            </a>
            
            <a class="dropdown-item" href="<?= $website_url.'supplier?type=mykitchen' ?>">
                <?= Yii::t('app', 'My Kitchen') ?> </a> <span class="badge2"><?= $count_orders ?></span>
        <?php } ?>

        <?php if($supplier_type>1){ ?>
            <a class="dropdown-item"  href="<?= $website_url.'supplier?type=myProducts' ?>">
                <?= Yii::t('app', 'My Products') ?>
            </a>
        <?php } ?>

        <a class="dropdown-item" href="<?= $website_url.'profile/mywallet?type=mywallet' ?>">
            <?= Yii::t('app', 'My Wallet') ?>
        </a>
        <a class="dropdown-item" href="<?= $website_url.'supplier/subscription?type=subscription' ?>">
            <?= Yii::t('app', 'My Plan') ?>
        </a>
        <a class="dropdown-item" href="<?= $website_url.'supplier?type=salesreport' ?>">
            <?= Yii::t('app', 'My Sales Report') ?>
        </a>
        <a class="dropdown-item" href="<?= $website_url ?>supplier?type=invite">
            <?= Yii::t('app', 'Invite Friends') ?>
        </a>
        <a class="dropdown-item" href="<?= $website_url.'supplier' ?>">
            <?= Yii::t('app', 'Ask For Help') ?>
        </a>
        <a class="dropdown-item" href="<?= $website_url.'switch-to-buy' ?>">
            <?= Yii::t('app', 'Switch to Buy') ?>
        </a>
        <?= Html::a(Yii::t('app', 'Logout'),$website_url.'logout',['data-method' => 'post','class' => 'dropdown-item']) ?>
    </div>
</div>
