<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="signup-sec pattern">
    <div class="container">
        <div class="signup-content">
            <div class="signup-body">
                <h3 class="text-uppercase text-center m-b-30"> <?= Yii::t('app','Sign In') ?> </h3>
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <div class="row">
                    <div class="form-group col-md-12 col-sm-12 col-12">
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                    </div>
                    <div class="form-group col-md-12 col-sm-12 col-12">
                        <?= $form->field($model, 'password')->passwordInput() ?>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="checkbox">
                            <?= $form->field($model, 'rememberMe')->checkbox() ?>
                        </div>
                    </div>
                    <div class="form-group col-md-6 col-sm-12 col-12 text-right forgotpwd">
                        <?= Html::a(Yii::t('app','Forgot Password?'), ['site/request-password-reset'],['class'=>'color']) ?>
                    </div>
                    <div class="form-group col-md-12 col-sm-12 col-12 submit-bt">
                        <?= Html::submitButton(Yii::t('app','Login'), ['class' => 'btn custom-btn', 'name' => 'login-button']) ?>
                    </div>
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <div class="line-div"></div>
                        <p class="text-center"> <?= Yii::t('app','Don\'t have an account?') ?> <?= Html::a(Yii::t('app','Sign Up'),['/signup'],['class'=>'color']) ?></p>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>