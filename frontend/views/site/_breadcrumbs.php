<?php
use yii\helpers\Url;
use yii\helpers\Html;
$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);
?>
<section class="using-border py-3">
    <div class="container">
        <div class="row">
            <div class="inner_breadcrumb  ml-4">
                <ul class="short_ls">
                    <li>
                        <?= Html::a('Home',$website_url) ?>
                        <span>/</span>
                    </li>
                    <?php if(isset($breadcrumbs) && !empty($breadcrumbs)){
                        foreach($breadcrumbs as $breadcrumb){
                            if(!empty($breadcrumb['href'])) echo '<li>'.Html::a($breadcrumb['label'],$breadcrumb['label']).'</li>';
                            else echo '<li>'.$breadcrumb['label'].'</li>';
                        }
                    } ?>
                </ul>
            </div>
        </div>
    </div>
</section>