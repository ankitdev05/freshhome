<?php

use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Menu;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

$session = Yii::$app->session;
$screen_id = $session->get('screen_id') ?? 1;

$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);
$long = $session->get('long');
$lat = $session->get('lat');
if (!Yii::$app->user->isGuest) {
    if(empty($long) && empty($lat)){
        $query = Menu::find()->joinWith('supplierInfo')->where(['and',
           // ['NOT IN','tbl_menu.supplier_id',yii::$app->user->id],
            ['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],
            ['>','subscription_end',time()]
        ])->andWhere(['NOT IN','tbl_menu.product_type',1])->limit(10);
    }else{
        $query = Menu::find()->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance  "))->joinWith('supplierInfo')->where(['and',
           // ['NOT IN', 'tbl_menu.supplier_id', yii::$app->user->id],
            ['tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0],
            ['>', 'subscription_end', time()]
        ])->andWhere(['NOT IN','tbl_menu.product_type',1])->orderBy('distance')->limit(10);
    }
}else
    $query = Menu::find()->joinWith('supplierInfo')->where(['and',
        ['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],
        ['>','subscription_end',time()]
    ])->andWhere(['NOT IN','tbl_menu.product_type',1])->limit(10);
$dataProvider	=  new ActiveDataProvider([
    'query' => $query,
    'pagination' => false,

]);

?>
<section class="latest-products mt-5 p-0">
	<div class="container-fluid">
		<div class="row justify-content-md-center">
			<div class="col-md-11">
				<div class="latest-pro-slider">
					<div class="form-row">
						<div class="col-md-6 col-7">
							<h5 class="latest-product"><?= Yii::t('app', 'Latest Products') ?></h5>
						</div>
						<div class="col-md-6 col-5">
							<p class="text-right mb-2 viewallpro"><?= Html::a(Yii::t('app', 'View All'),$website_url.'latest-products',['title'=>'View All Latest Products']) ?></p>
						</div>
					</div>
					<hr class="speratorline mt-0"/>
					<div id="demos">
						<div class="large-12 columns">
                            <?=  ListView::widget([
                                'layout' => '{items}',
                                'itemView' => '//products/theme/_slider_products',
                                'dataProvider' => $dataProvider,
                                'options' => [
                                    'tag' => 'div',
                                    'class' => 'owl-carousel owl-theme',
                                    'id' => 'list-wrapper',
                                ],
                                'itemOptions' => [
                                    'tag' => false
                                ],
                            ]); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
$this->registerJs("
	$(document).ready(function() {
	  $('.owl-carousel').owlCarousel({
		loop: true,
		margin: 5,
		responsiveClass: true,
		responsive: {
		  0: {
			items: 1,
			nav: true
		  },
		  375: {
			items: 2,
			nav: true
		  },
		  600: {
			items: 3,
			nav: false
		  },
		  1000: {
			items: 4,
			nav: true,
			loop: false
		  },
		  1248: {
			items: 5,
			nav: true,
			loop: false
		  }
		}
	  })
	});
"); 