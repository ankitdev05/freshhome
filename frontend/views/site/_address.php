<?php
use common\models\UserAddress;
use yii\helpers\BaseStringHelper;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$session = Yii::$app->session;
$user_address = '';
$user_model = new UserAddress();

if ($session->has('user_address')){
    $user_address = $session->get('user_address');
    $user_model->is_default = $user_address['address_id'];
}


$title = 'Select your Address';
$all_address = [];
if(!yii::$app->user->isGuest){
    $all_address = ArrayHelper::map(UserAddress::find()->where(['user_id' => yii::$app->user->id,'is_delete' => 0])->asArray()->all(),'address_id','location');
}

?>
<div class="selectaddress">
    <div class="form-row">
        <div class="col-md-2 pr-0">
            <div class="mt-3 addressicon"><i class="fas fa-map-marker-alt text-white"></i></div>
        </div>
        <div class="col-md-9">
            <p class="custaddress text-white mt-2 pt-1 mb-0">
                <?php if(yii::$app->user->isGuest){ ?>
                    <a href="#login" data-toggle="modal" data-target=".bs-example-modal-lg" onclick='return $("#login_id").click()'><?= yii::t('app','Select your Address') ?></a>
                <?php } else {
                    if(!empty($user_address))
                        $title = BaseStringHelper::truncateWords($user_address['location'], 5);
                    ?>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#selectAddress"><?= $title ?></a>

                <?php } ?>
            </p>
        </div>
    </div>
</div>
<div class="modal fade" id="selectAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= Yii::t('app','Choose your location') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php if(!empty($all_address)){ ?>
                    <?php $form = ActiveForm::begin(['method' => 'GET','action' => Url::to(yii::$app->request->baseUrl.'/',true)]); ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($user_model,'is_default')->radioList($all_address)->label(false) ?>
                        </div>
                        <div class="col-sm-12">
                            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn custom-btn btnupdate']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                <?php }else{ ?>
                    <h4>No address found.</h4>
                <?php } ?>
                <hr/>
                <a class="btn btn custom-btn btnupdate" href="<?= Yii::$app->request->baseUrl.'/profile?type=address' ?>">Manage address</a>
            </div>
        </div>
    </div>
</div>