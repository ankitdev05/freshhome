<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;

use common\models\Meal;
use common\models\Cuisine;
use common\models\Filters;
use common\models\City;
use common\models\Category;

$model = new Filters();
$categories = ArrayHelper::map(Category::find()->where(['active'=>'1','cat_id'=>$screen_id])->orderBy('name asc')->asArray()->all(),'id','name');

$meals = ArrayHelper::map(Meal::find()->select('*')->joinWith('mealName as mn')->where(['status'=>'active','mn.language_id'=>language_id])->orderBy('mn.meal_name asc')->asArray()->all(),'meal_id','meal_name');

$cuisines = ArrayHelper::map(Cuisine::find()->select('*')->joinWith('cuisineName as cn')->where(['status'=>'active','cn.language_id'=>language_id])->orderBy('cn.cuisine_name asc')->asArray()->all(),'cuisine_id','cuisine_name');

$cities = ArrayHelper::map(City::find()->select('*')->joinWith('cityName as cn')->where(['status'=>'active','cn.language_id'=>language_id])->orderBy('cn.city_name asc')->asArray()->all(), 'city_id', 'city_name');


?>
<?php $form = ActiveForm::begin(['id' => 'filter-products','method'=>'GET','action' => Url::to(Yii::$app->request->baseUrl.'/search',true)]); ?>
<section class="search-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-12">
                <div class="search-box">
                    <div class="input-group">
                     <?= $form->field($model, 'categories',['options'=>['class'=>'col-sm-12']])->dropDownList($categories,['prompt'=>'Select Product','class'=>'custom-select'])->label(false) ?>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-12">
                <div class="search-box">
                    <div class="input-group">
                        <?= $form->field($model, 'title',['template'=>'{input}','options'=>['class'=>'']])->textInput(['style'=>'width:300px','placeholder'=>'Search','maxlength' => true,'class'=>'form-control cus-input'])->label(false) ?>
                        <?= $form->field($model, 'meals',['template'=>'{input}','options'=>['class'=>'']])->dropDownList($meals,['prompt'=>'Select meals','class'=>'custom-select cus-select'])->label(false) ?>
                        
                        <div class="input-group-append">
                            <?= Html::submitButton(Yii::t('app', '<i class="fas fa-search"></i>'), ['class' => 'btn btn-outline-secondary']) ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-12">
                <div class="search-box">
                    <div class="input-group">
                        <?= $form->field($model, 'city',['options'=>['class'=>'col-sm-12']])->dropDownList($cities,['prompt'=>'Select City','class'=>'custom-select'])->label(false) ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php ActiveForm::end(); ?>

