<?php
use yii\widgets\ListView;

$title = 'Chef\'s Menu';

if($model->supplier_type > 1)
    $title = 'Products';
?>
<section class="product chefmenu">
    <div class="container">
        <div class="form-row">
            <h4 class="productchefmenuhead"><?= $title ?> <span class="view-product"></h4>
            <?=  ListView::widget([
                'layout' => " {items}\n<div class='col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
                'dataProvider' => $dataProvider,
                'itemView' => '//products/_view_product',
                'options' => [
                    'tag' => false,
                    'class' => '',
                    'id' => 'list-wrapper',
                ],
                'itemOptions' => [
                    'tag' => false
                ],
                'pager' => [
                    'options' => ['class'=>'pagination justify-content-end'],
                    'prevPageLabel' =>'Previous',
                    'nextPageLabel' =>'Next',
                    'pageCssClass' => 'page-item',
                    'disabledPageCssClass' => 'page-link disabled',
                    'linkOptions' => ['class' => 'page-link'],
                ]
            ]); ?>

        </div>
    </div>
</section>