<?php
use yii\widgets\ListView;

?>
<section class="product chefmenu">
    <div class="container">
        <div class="row">
            <h4>Reviews <span class="view-product"><span class="recentcmt">Recent Comments</span></h4>

            <?=  ListView::widget([
                'layout' => "<div class='col-sm-12'>{summary}</div>\n {items}\n <div class='col-sm-12 mt-2 float-left'><div class='pagination-sec'><div aria-label='Page navigation example'>{pager}</div></div></div>",
                'dataProvider' => $dataProvider,
                'itemView' => '_review',
                'options' => [
                    'tag' => 'div',
                    'class' => 'review-all',
                    'id' => 'list-wrapper',
                ],
                'itemOptions' => [
                    'tag' => false
                ],
                'pager' => [
                    'options' => ['class'=>'pagination justify-content-end'],
                    'prevPageLabel' =>'Previous',
                    'nextPageLabel' =>'Next',
                    'pageCssClass' => 'page-item',
                    'disabledPageCssClass' => 'page-link disabled',
                    'linkOptions' => ['class' => 'page-link'],
                ]
            ]); ?>

        </div>
    </div>
</section>