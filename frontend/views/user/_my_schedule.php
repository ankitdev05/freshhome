<?php
use yii\helpers\Html;
use common\models\MenuSchedules;

$results = MenuSchedules::find()->where(['and',['choices' => 'active'],['supplier_id' => $model->id],['>=','end_timestamp',time() ]])->orderBy('start_timestamp')->limit(8)->all();
if(!empty($results)) {
    ?>
    <section class="latest-products relate-p">
        <div class="container">
            <div class="inner-product">
                <h4 class="latestprohead">Chef's Schedule<span class="view-product"><?= Html::a('View All',$website_url.'user/'.$model->username.'?type=schedule',['title'=>'View All']) ?></span></h4>
                <div class="product-slider ">
                    <div class="Chef-schedule autoplay1">
                        <?php foreach($results as $result){ ?>
                            <div class="item ">
                                <div class="schedule-time">
                                    <p><?= date('d-F-Y',strtotime($result->schedule_date)) ?></p>
                                    <p>Opening Hours<span class="time"><?= date('h:i A',strtotime($result->start_time)) ?> To <?= date('h:i A',strtotime($result->end_time)) ?></span></p>
                                    <p><span class="availitem">Available Items</span><span class="time chefscheduleavailitem">(<?= $result->countMenuScheduleItems ?>)</span></p>
                                    <p class="scheduleviewmenu"><a href="<?= yii::$app->request->baseUrl.'/user/'.$model->username.'?type=schedule_items&id='.$result->id ?>">View Menu</a></p>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    $this->registerJs("
    $('.autoplay1').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        margin:30,
        autoplaySpeed: 2000,
        nextArrow: '<i class=\"fa fa-arrow-right\"></i>',
        prevArrow: '<i class=\"fa fa-arrow-left\"></i>',
        responsive: [
              {
                 breakpoint: 768,
                 settings: 
                  {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 3
                  }
               },
              {
                breakpoint: 480,
                settings:
                {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '0px',
                  slidesToShow: 1
                }
              }
        ]
    });
");
}