<?php
use yii\widgets\ListView;
use yii\helpers\Html;
?>
<section class="product chefmenu latest-products">
    <div class="container">
        <div class="form-row inner-product"> 
            <h4 class="scheduledatetime"><?= date('d-F-Y',strtotime($result->schedule_date)) ?> Schedule<span class="view-product"><?= Html::a('View All Schedule',Yii::$app->request->baseUrl.'/user/'.$model->username.'?type=schedule') ?></span></h4>
            <?=  ListView::widget([
                'layout' => " {items}\n<div class='col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
                'dataProvider' => $dataProvider,
                'itemView' => '//products/_view_product',
                'options' => [
                    'tag' => false,
                    'class' => '',
                    'id' => 'list-wrapper',
                ],
                'itemOptions' => [
                    'tag' => false
                ],
                'pager' => [
                    'options' => ['class'=>'pagination justify-content-end'],
                    'prevPageLabel' =>'Previous',
                    'nextPageLabel' =>'Next',
                    'pageCssClass' => 'page-item',
                    'disabledPageCssClass' => 'page-link disabled',
                    'linkOptions' => ['class' => 'page-link'],
                ]
            ]); ?>

        </div>
    </div>
</section>