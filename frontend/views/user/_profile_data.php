<?php

use yii\helpers\Html;
use kartik\rating\StarRating;

$total_reviews = $model->totalreviews;
$total_items = $model->totalAvailableItems;
$total_d_items = $model->totalDeliveredItems;

$class = 'far fa-heart';
$fav_supp = $model->favSupp;
$fav_val = 1;
if($fav_supp==1){
    $class = 'fas fa-heart';
    $fav_val = 0;
}
?>
<section class="product p-detail supplier-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="supplier-img">
                    <?php if(empty($model->profile_pic)) $model->profile_pic = 'no_image.png';
                    echo Html::img($model->getSupplierImage($model->profile_pic,150,150),['alt'=>$model->name,'title'=>$model->name]) ?>
                    <span class="<?= $model->availability ?>"></span>
                </div>
            </div>
            <div class="col-md-10 col-sm-9 col-xs-12">
                <div class="supplier-info">
                    <h2><?= ucwords($model->name) ?>
                        <?php if (Yii::$app->user->isGuest){ ?>
                            <a href="javascript:void(0)" onclick="return warningPopup('Please login first.')"><span class="wishlistbadge"><i class="far fa-heart"></i></span></a>
                        <?php }else{ ?>
                            <a href="javascript:void(0)" onclick="return addToFavSupp(<?= $model->id ?>,<?= $fav_val ?>)"><span class="wishlistbadge"><i class="<?= $class ?>"></i></span></a>
                        <?php } ?>
                    </h2>
                    <ul class="rating">
                        <?= StarRating::widget([
                            'name' => 'rating_1',
                            'value' => $model->avgOverall,
                            'pluginOptions' => ['disabled'=>true, 'showClear'=>false,'size' =>'xs','showCaption'=>false]
                        ]); ?>
                        <span class="reviews-star"><a href="#">(<?= $total_reviews==1 ? $total_reviews.' review' : $total_reviews.' reviews' ?>)</a></span>
                    </ul>

                    <p>Kitchen's Address <span class="add-rss"><?= $model->location ?></span></p>
                    <div class="user-view">
                        <ul>
                            <li>
                                <span class="no-view"><?= $model->views ?></span>
                                <span class="user-color">User <?= $model->views==1 ? 'view' : 'views' ?></span>
                            </li>
                            <li>
                                <span class="no-view"><?= $total_items ?></span>
                                <span class="user-color">Available <?= $total_items==1 ? 'Item' : 'Items' ?></span>
                            </li>
                            <li>
                                <span class="no-view"><?= $total_d_items ?></span>
                                <span class="user-color">Delivered <?= $total_d_items==1 ? 'Item' : 'Items' ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>