<?php
$info = $model->user->userDetail;

?>
<div class="Comments-user pb-3 mt-2">
    <div class="form-row">
        <div class="col-lg-1 col-md-1 col-sm-2 col-3"> 
            <div class="user-pic">
                <img src="<?= $info['profile_pic'] ?>" alt="<?= $info['name'] ?>" title="<?= $info['name'] ?>" class="img-fluid img-responsive">
            </div>
        </div>
        <div class="col-lg-11 col-md-11 col-sm-10 col-9">
            <h5 class="user-name"><?= $info['name'] ?> <span class="date-cmt">(<?= Yii::$app->formatter->format($model->created_at, 'relativeTime'); ?>) </span></h5>
            <p>
                <q>
                    <?= $model->review ?> 
                </q>
            </p>
        </div>
    </div>
</div>
