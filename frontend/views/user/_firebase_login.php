<?php
use common\models\User;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;
use yii\web\JsExpression;

$model = new User();
$model->scenario = 'checknumber';
?>
<div class="row mob-no">
    <div class="col-md-12">
        <div class="form-group " >
            <img src="<?= yii::$app->request->baseUrl ?>/img/smartphone100.jpg" width="50px;" alt="phone">
            <?php $form = ActiveForm::begin(['enableClientScript'=>false,'enableClientValidation'=>false,'options' => ['enctype' => 'multipart/form-data'],'id' => 'sign-in-form-firebase']); ?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    
                    <?= $form->field($model, 'phone_number')->widget(PhoneInput::className(),[
                        'options' => ['id' => 'phone-number-firebase'],
                        'jsOptions'=>['nationalMode'=>false,'autoHideDialCode'=>false,'initialCountry'=>'auto','geoIpLookup'=>new JsExpression('function(callback) { 
				            $.get(\'https://ipinfo.io\', function() {}, "jsonp").always(function(resp) {
					        var countryCode = (resp && resp.country) ? resp.country : "";
					        callback(countryCode);
					});
				}')]
                    ]) ?>
                </div>
                <div class="row  mob-no">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= Html::button('Send', ['class' => 'custom-btn btn', 'name' => 'profile-phone','id' => 'sign-in-button-firebase-1']) ?>
                            <?= Html::button('Send', ['class' => 'custom-btn btn', 'name' => 'profile-phone','id' => 'sign-in-button-firebase-2','style'=>'display:none;']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <?php $form = ActiveForm::begin(['enableClientScript'=>false,'enableClientValidation'=>false,'options' => ['enctype' => 'multipart/form-data'],'id' => 'verification-code-form-firebase','options' => ['style'=>'display:block;']]); ?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <p id="message"></p>
                    <?= $form->field($model, 'verification_code')->textInput(['id' => 'verification-code-firebase']) ?>
                    <div class="clearfix"></div>

                </div>

                <div class="row otp-show">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= Html::submitButton('Submit', ['id' => 'verify-code-button-firebase','class' => 'btn btn custom-btn', 'name' => 'verify-phone']) ?>
                            <?= Html::button('Resend', ['id' => 'verify-code-resend-firebase','class' => 'btn btn custom-btn Cancel', 'onClick' => "$('#verification-code-firebase').val('');$('#sign-in-button-firebase-2').click();"]) ?>

                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
$this->registerJs('
    $("#sign-in-button-firebase-1").on("click",function(){
        $.ajax({
             url : website_url+"checknumber",
             method : "POST",
             data : $("#sign-in-form-firebase").serialize(),
             dataType: "json",
             success : function(json){
                if(json["success"]){
                    $("#message").html(json["message"]);
                    $("#sign-in-button-firebase-2").click();
                }
                else{
                    warningPopup(json["error"]);
                    return false;
                }
            }
        })
    });
');
$this->registerJsFile('https://www.gstatic.com/firebasejs/6.2.0/firebase-app.js', [
    'position' => \yii\web\View::POS_HEAD
]);
$this->registerJsFile('https://www.gstatic.com/firebasejs/6.2.0/firebase-auth.js',[
    'position' => \yii\web\View::POS_HEAD
]);
$this->registerJsFile('https://www.gstatic.com/firebasejs/6.2.0/firebase-firestore.js',[
    'position' => \yii\web\View::POS_HEAD
]);



$this->registerJs('
  	var firebaseConfig = {
    	apiKey: "AIzaSyCTqdefmF2aQ9rDNu2kOuCrPd7qhjIZdJY",
	  	authDomain: "freshhome-eb8df.firebaseapp.com",
	  	databaseURL: "https://freshhome-eb8df.firebaseio.com",
	  	projectId: "freshhome-eb8df",
	  	storageBucket: "freshhome-eb8df.appspot.com",
	  	messagingSenderId: "350237825472",
	  	appId: "1:350237825472:web:0229272c446eeac8"
  	};

  	// Initialize Firebase
  	firebase.initializeApp(firebaseConfig);

	window.onload = function() {
		onSignOutClick();
		firebase.auth().onAuthStateChanged(function(user) {
			
			if (user) {
				var uid = user.uid;
				var email = user.email;
				var photoURL = user.photoURL;
				var phoneNumber = user.phoneNumber;
				var isAnonymous = user.isAnonymous;
				var displayName = user.displayName;
				var providerData = user.providerData;
				var emailVerified = user.emailVerified;
				//console.log(user);
			}
			updateSignInButtonUI();
			  updateSignInFormUI();
			  updateSignOutButtonUI();
			  updateSignedInUserStatusUI();
			  updateVerificationCodeFormUI();
		});
		
		document.getElementById(\'phone-number-firebase\').addEventListener(\'keyup\', updateSignInButtonUI);
		document.getElementById(\'phone-number-firebase\').addEventListener(\'change\', updateSignInButtonUI);
		document.getElementById(\'verification-code-firebase\').addEventListener(\'keyup\', updateVerifyCodeButtonUI);
		document.getElementById(\'verification-code-firebase\').addEventListener(\'change\', updateVerifyCodeButtonUI);
		document.getElementById(\'verification-code-form-firebase\').addEventListener(\'submit\', onVerifyCodeSubmit);
		
		window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(\'sign-in-button-firebase-2\', {
		  \'size\': \'invisible\',
		  \'callback\': function(response) {
			// reCAPTCHA solved, allow signInWithPhoneNumber.
			onSignInSubmit();
			
		  }
		});
		recaptchaVerifier.render().then(function(widgetId) {
		  window.recaptchaWidgetId = widgetId;
		  updateSignInButtonUI(true);
		 
		});
		
	};
	
	function updateSignInButtonUI(allow=false) {
		
		document.getElementById(\'sign-in-button-firebase-2\').disabled =
        !isPhoneNumberValid()
        || !!window.signingIn;
        
        
	}
	function updateVerifyCodeButtonUI() {
		document.getElementById(\'verify-code-button-firebase\').disabled =
			!!window.verifyingCode
			|| !getCodeFromUserInput();
	}
	function updateVerificationCodeFormUI() {
    if (!firebase.auth().currentUser && window.confirmationResult) {
      document.getElementById(\'verification-code-form-firebase\').style.display = \'block\';
    } else {
      document.getElementById(\'verification-code-form-firebase\').style.display = \'none\';
    }
  }
	function updateSignOutButtonUI() {
		
	}
	function updateSignedInUserStatusUI() {
		var user = firebase.auth().currentUser;
		if (user) {
			console.log(JSON.stringify(user, null, \'  \'));
            $(".mob-no").hide();
            $("#signup-form-popup").show();
            $("#sign_up-phone_number").val(user.phoneNumber);
		}
	}
	function onSignOutClick() {
		firebase.auth().signOut();
	  }
	function onSignInSubmit() {
		if (isPhoneNumberValid()) {
		  	window.signingIn = true;
		  	updateSignInButtonUI();
		  	var phoneNumber = getPhoneNumberFromUserInput();

		  	var appVerifier = window.recaptchaVerifier;
		  
		  	firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
			  	.then(function (confirmationResult) {
				// SMS sent. Prompt user to type the code from the message, then sign the
				// user in with confirmationResult.confirm(code).
				window.confirmationResult = confirmationResult;
				window.signingIn = false;
				updateSignInButtonUI();
				updateVerificationCodeFormUI();
				updateVerifyCodeButtonUI();
				updateSignInFormUI();
			}).catch(function (error) {
				
				// Error; SMS not sent
				//console.error(\'Error during signInWithPhoneNumber\', error);
				
				window.alert(\'Error during signInWithPhoneNumber:\n\n\'
					+ error.code + \'\n\n\' + error.message);

				window.signingIn = false;
				updateSignInFormUI();
				updateSignInButtonUI();
			});
		}
	}
	function updateSignInFormUI() {
		if (firebase.auth().currentUser || window.confirmationResult) {
		  document.getElementById(\'sign-in-form-firebase\').style.display = \'none\';
		} else {
		  resetReCaptcha();
		  document.getElementById(\'sign-in-form-firebase\').style.display = \'block\';
		}
	  }
	  function getCodeFromUserInput() {
		return document.getElementById(\'verification-code-firebase\').value;
	  }
	  
	  function getPhoneNumberFromUserInput() {
		return document.getElementById(\'phone-number-firebase\').value;
	  }
	  function isPhoneNumberValid() {
		var pattern = /^\+[0-9\s\-\(\)]+$/;
		var phoneNumber = getPhoneNumberFromUserInput();
		if(phoneNumber.length > 5)
			return phoneNumber.search(pattern) !== -1;
	  }
	  
	function resetReCaptcha() {
    if (typeof grecaptcha !== \'undefined\'
        && typeof window.recaptchaWidgetId !== \'undefined\') {
      grecaptcha.reset(window.recaptchaWidgetId);
    }
  }
	function onVerifyCodeSubmit(e) {
    e.preventDefault();
    if (!!getCodeFromUserInput()) {
      window.verifyingCode = true;
      updateVerifyCodeButtonUI();
      var code = getCodeFromUserInput();
      confirmationResult.confirm(code).then(function (result) {
        // User signed in successfully.
        var user = result.user;
        window.verifyingCode = false;
        window.confirmationResult = null;
        updateVerificationCodeFormUI();
      }).catch(function (error) {
        // User couldn\'t sign in (bad verification code?)
        console.error(\'Error while checking the verification code\', error);
        window.alert(\'Error while checking the verification code:\n\n\'
            + error.code + \'\n\n\' + error.message);
        window.verifyingCode = false;
        updateSignInButtonUI();
        updateVerifyCodeButtonUI();
      });
    }
  }  
', yii\web\View::POS_HEAD);