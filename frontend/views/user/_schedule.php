<div class="item ">
    <div class="schedule-time">
        <p><?= date('d-F-Y',strtotime($model->schedule_date)) ?></p>
        <p>Opening Hours<span class="time"><?= date('h:i A',strtotime($model->start_time)) ?> To <?= date('h:i A',strtotime($model->end_time)) ?></span></p>
        <p>Available Items<span class="time">(<?= $model->countMenuScheduleItems ?>)</span></p>
        <p><a href="<?= Yii::$app->request->baseUrl.'/user/'.$supplier->username.'?type=schedule_items&id='.$model->id ?>">View Menu</a></p>
    </div>
</div>