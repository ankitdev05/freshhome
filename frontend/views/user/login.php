<?php
use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use common\models\LoginForm;
use common\models\Occupation;
use frontend\models\SignupForm;
use himiklab\thumbnail\EasyThumbnailImage;

$all_occupations = ArrayHelper::map(Occupation::find()->joinWith('occupationName as cn')->where(['cn.language_id'=>language_id])->asArray()->all(), 'id', 'occupationName.occupation_name');

$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);
$model = new LoginForm();
$sign_up = new SignupForm();
$sign_up->scenario = 'registration';

$config_logo    = config_logo;
$image	= EasyThumbnailImage::thumbnailFileUrl(
    "../../frontend/web/images/store/".$config_logo,370,112,EasyThumbnailImage::THUMBNAIL_INSET
);
$action = $this->context->action->id;
?>
<div class="modal fade bs-example-modal-lg sign_up_login_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content popup-modal">
			<button type="button" class="close modalclose text-right" data-dismiss="modal" aria-label="Close">
				<?= Html::a(Html::img($website_url.'img/close.png',['alt'=>'Close','title'=>'Close','class'=>'img-fluid']),'javascript:void(0);') ?>
			</button>
            <div class="modal-body">
				<div class="login-popup form-row">
					<div class="left-part col-md-6">
						<div class="login-img">
							<?= Html::img($image,['class'=>'img-fluid login-logo','alt'=>'Banner','title'=>'Banner']) ?>
							<p><?= Yii::t('app', 'Want to sell your own product and earn some money from home.') ?></p>
							<h5><?= Yii::t('app', 'Join Us') ?></h5>
							<p class="mb-1"><a href="<?= yii::$app->request->baseUrl.'/ask-for-help' ?>"><?= Yii::t('app', 'Ask for Help?') ?></a></p>
							<?= Html::a(Yii::t('app', 'Signup as Supplier'),['/signup'],['class' => 'btn custom-btn']) ?>
							<span class="exit-user">
							  <a href="<?= Yii::$app->request->baseUrl.'/login' ?>"><?= Yii::t('app', 'Existed User?') ?></a>
							</span>
						</div>
					</div>
					<div class="right-part col-md-6">
						<div class="login-signup">
							<ul class="nav nav-pills cus-tabs">
								<li class="nav-item">
								<?= Html::a(Yii::t('app', 'Sign Up'),'#signup',['class'=>'nav-link','aria-controls'=>'signup','data-toggle'=>'pill','id'=>'signUp_id']) ?>
								</li>
								<li class="nav-item">
									<?= Html::a(Yii::t('app', 'Login'),'#login',['class'=>'nav-link','aria-controls'=>'login','data-toggle'=>'pill','id'=>'login_id']) ?>
								</li>
							</ul>
							<div class="tab-content">
								<div id="signup" class="container tab-pane"><br/>
									<?php if($action!='signup'){ ?>
										<?= $this->render('_firebase_login'); ?>
									<?php } ?>
									<?php $form = ActiveForm::begin(['id' => 'signup-form-popup','options' => ['style' => 'display:none;']]); ?>
									<div class="row">
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= $form->field($sign_up, 'name')->textInput(['id'=>'sign_up-name']) ?>
										</div>
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= $form->field($sign_up, 'username')->textInput(['id'=>'sign_up-username']) ?>
										</div>
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= $form->field($sign_up, 'phone_number')->textInput(['id'=>'sign_up-phone_number','readonly'=>true]) ?>
										</div>
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= $form->field($sign_up, 'email')->textInput(['id'=>'sign_up-email']) ?>
										</div>
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= $form->field($sign_up, 'occupation_id')->widget(Select2::classname(), [
												'data' =>$all_occupations,
												'options' => ['placeholder' => 'Please select ...','id'=>'sign_up-occupation_id'],
												'pluginOptions' => [
													'allowClear' => true,
												],
											]) ?>
										</div>
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= $form->field($sign_up, 'dob')->widget(DatePicker::classname(), [
												'options' => ['placeholder' => 'Enter birth date ...','readonly'=>true,'id'=>'sign_up-dob'],
												'pluginOptions' => [
													'autoclose'=>true,
													'endDate'=>'+0d',
													'format' => 'yyyy-mm-dd'
												]
											]); ?>
										</div>
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= $form->field($sign_up, 'password')->passwordInput(['id'=>'sign_up-password']) ?>
										</div>
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= $form->field($sign_up, 'password_repeat')->passwordInput(['id'=>'sign_up-password_repeat']) ?>
										</div>
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= Html::button('Sign Up', ['class' => 'btn custom-btn', 'name' => 'signUp-button','id'=>'signUpPopupButton']) ?>
										</div>
									</div>
									<?php ActiveForm::end(); ?>
								</div>

								<div id="login" class="container tab-pane fade"><br/>
									<?php $form = ActiveForm::begin(['id' => 'login-form-popup']); ?>
									<div class="row">
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= $form->field($model, 'username')->textInput(['id'=>'popupUsername']) ?>
										</div>
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= $form->field($model, 'password')->passwordInput(['id'=>'popupPassword']) ?>
										</div>
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= $form->field($model, 'rememberMe')->checkbox(['id'=>'popupRememberMe']) ?>
										</div>
										<div class="col-md-12 col-sm-12 col-12 px-0">
											<?= Html::button(Yii::t('app', 'Login'), ['class' => 'btn custom-btn', 'name' => 'login-button','id'=>'LoginPopupButton']) ?>
											<ul class="social_ul float-right" type="none">
												<li>
													<a href="<?= yii::$app->request->baseUrl ?>/auth?authclient=google" class="gp"><i class="fab fa-google"></i></a>
												</li>
											</ul>
										</div>
									</div>
									<div class="row">
										<div class="mt-3 col-md-12 col-sm-12 col-12 p-0">
											<?= Html::a(Yii::t('app', 'Forgot Password?'), ['site/request-password-reset']) ?>
										</div>

									</div>
									<?php ActiveForm::end(); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs('
    $("#signUpPopupButton").on("click",function(){
        $.ajax({
            url     : website_url+"signup",
             method : "POST",
             data    : $("#signup-form-popup").serialize(),
             dataType: "json",
             success : function(json){
                if(json["success"]){
                    window.location.reload();
                }else{
                    warningPopup(json["error"]);
                    return false;
                }
             }
        });
    })
    $("#LoginPopupButton").on("click",function(){
        $.ajax({
            url     : website_url+"login",
             method : "POST",
             data    : $("#login-form-popup").serialize(),
             dataType: "json",
             success : function(json){
                if(json["success"]){
                    window.location.reload();
                }else{
                    warningPopup(json["error"]);
                    return false;
                }
             }
        });
    });
');
