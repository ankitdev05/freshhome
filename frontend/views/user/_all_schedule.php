<?php
use yii\widgets\ListView;

?>
<section class="product chefmenu">
    <div class="container">
        <div class="row">
            <h4 class="latestprohead">Chef's Schedule <span class="view-product"></h4>
            <div class="product-slider">
                <div class="Chef-schedule">
                    <?=  ListView::widget([
                        'layout' => " {items}\n<div class='col-sm-12 mt-3'><div class='pagination-sec'><nav aria-label='Page navigation example'>{pager}</nav></div>",
                        'dataProvider' => $dataProvider,
                        'itemView' => '_schedule',
                        'viewParams' => ['supplier' => $model],
                        'options' => [
                            'tag' => false,
                            'class' => '',
                            'id' => 'list-wrapper',
                        ],
                        'itemOptions' => [
                            'tag' => false
                        ],
                        'pager' => [
                            'options' => ['class'=>'pagination justify-content-end'],
                            'prevPageLabel' =>'Previous',
                            'nextPageLabel' =>'Next',
                            'pageCssClass' => 'page-item',
                            'disabledPageCssClass' => 'page-link disabled',
                            'linkOptions' => ['class' => 'page-link'],
                        ]
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>
