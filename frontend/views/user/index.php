<?php
use yii\helpers\Url;

$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);
$this->title = ucwords($model->name) .' | '.config_name;

?>
<?= $this->render('_profile_data',['model' => $model])  ?>
<?php if($type==null){ ?>
    <?= $this->render('_about_us',['model' => $model,'website_url' => $website_url]) ?>
    <?= $this->render('_my_menu',['model' => $model,'website_url' => $website_url]) ?>
    <?= $this->render('_my_schedule',['model' => $model,'website_url' => $website_url]) ?>

<?php }elseif($type=='reviews'){ ?>
    <?= $this->render('_all_reviews',['model' => $model,'dataProvider' => $dataProvider]) ?>
<?php }elseif($type=='products'){ ?>
    <?= $this->render('_all_products',['model' => $model,'dataProvider' => $dataProvider]) ?>
<?php }elseif($type=='schedule'){ ?>
    <?= $this->render('_all_schedule',['model' => $model,'dataProvider' => $dataProvider]) ?>
<?php }elseif($type=='schedule_items'){ ?>
    <?= $this->render('_all_sch_products',['result' => $result,'model' => $model,'dataProvider' => $dataProvider]) ?>
<?php } ?>
<?= $this->render('_related_suppliers',['model' => $model,'website_url' => $website_url]) ?>
<?php
$this->registerCss('
.rating-container{float:left;}
');