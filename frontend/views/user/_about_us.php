<?php
$supplier_review = $model->supplierReview;
?>
<section class="description">
    <div class="container">
        <div class="row">
            <div class="product-tabs">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">About</a>

                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Reviews</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <p><?= $model->description ?></p>
                    </div>

                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <h3 class="cmt-profile">Recent Comments <span class="view-p"><a href="<?= $website_url.'user/'.$model->username.'?type=reviews'?>">View All</a></span></h3>
                        <?php if(!empty($supplier_review)){ ?>
                            <div class="Comments-user">
                                <div class="row">
                                    <div class="col-lg-1 col-md-1 col-sm-2 col-3 p-0">
                                        <div class="user-pic">
                                            <img src="<?= $supplier_review['user_info']['profile_pic'] ?>" alt="<?= $model->name ?>" title="<?= $model->name ?>" class="img-fluid img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-lg-11 col-md-11 col-sm-10 col-9 p-0">
                                        <h5 class="user-name"><?= $supplier_review['user_info']['name'] ?><span class="date-cmt">(<?= $supplier_review['time'] ?>) </span></h5>
                                        <p><q><?= $supplier_review['review'] ?></q></p>
                                    </div>
                                </div>
                            </div>
                        <?php }else{ ?>
                            <p>No review found.</p>
                        <?php } ?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>