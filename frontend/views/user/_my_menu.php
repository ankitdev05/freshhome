<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Menu;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

$session = Yii::$app->session;
$screen_id = $session->get('screen_id') ?? 1;


$query = Menu::find()->where(['and',['image_approval'=>1],['is_delete'=>0],['test_data'=>0],['supplier_id'=>$model->id]])->limit(10);
$dataProvider	=  new ActiveDataProvider([
    'query' => $query,
    'pagination' => false,
    'sort' => [
        'defaultOrder' => [
            'id' => SORT_DESC,
        ]
    ],
]);
$title = 'Chef\'s Menu';

if($model->supplier_type > 1)
    $title = 'Products';
?>
    <section class="latest-products relate-p">
        <div class="container">
            <div class="inner-product">
                <h4 class="latestprohead"><?= $title ?> <span class="view-product"><?= Html::a('View All',$website_url.'user/'.$model->username.'?type=products',['title'=>'View All Latest Products']) ?></span></h4>
                <div class="product-slider">

                    <?=  ListView::widget([
                        'layout' => '{items}',
                        'itemView' => '//products/_slider_product',
                        'dataProvider' => $dataProvider,
                        'options' => [
                            'tag' => 'div',
                            'class' => 'autoplay',
                            'id' => 'list-wrapper',
                        ],
                        'itemOptions' => [
                            'tag' => false
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </section>
