<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\User;

$session = Yii::$app->session;
$screen_id = $session->get('screen_id') ?? 1;
$website_url = Url::to(Yii::$app->request->baseUrl.'/',true);
$online_suppliers = User::find()->where(['availability'=>'online','status'=>10,'is_test_user'=>0,'is_supplier'=>'yes','supplier_type'=>$screen_id])->andWhere(['!=','id',$model->id])->andWhere(['>','subscription_end',time()])->limit(5)->all();
if(!empty($online_suppliers)){
    ?>
    <section class="latest-products relate-p">
        <div class="container">
            <div class="inner-product">
                <h4 class="latestprohead">RELATED SUPPLIERS <span class="view-product"><?= Html::a('View All',$website_url.'online-kitchen') ?></span></h4>
                <div class="product-slider">
                    <div class="autoplay kitchen">
                        <?php foreach($online_suppliers as $online_supplier){

                            $class = 'far fa-heart';
                            $fav_supp = $online_supplier->favSupp;
                            $fav_val = 1;
                            if($fav_supp==1){
                                $class = 'fas fa-heart';
                                $fav_val = 0;
                            }
                            ?>
                            <div class="item">
                                <?php if(empty($online_supplier->profile_pic)) $online_supplier->profile_pic = 'no_image.png';
                                echo Html::img($online_supplier->getSupplierImage($online_supplier->profile_pic,150,150),['alt'=>$online_supplier->name,'title'=>$online_supplier->name]) ?>

                                <div class="product-content">
									<div class="form-row">
										<div class="col-md-10">
											<div class="title">
												<?= Html::a($online_supplier->name,$online_supplier->profileUrl) ?>
											</div>
										</div>
										<div class="col-md-2 text-center">
											<span class="wishlistbadge">
                                                 <?php if (Yii::$app->user->isGuest){ ?>
                                                     <a href="javascript:void(0)" onclick="return warningPopup('Please login first.')"><span class="wishlistbadge"><i class="far fa-heart"></i></span></a>
                                                 <?php }else{ ?>
                                                     <a href="javascript:void(0)" onclick="return addToFavSupp(<?= $model->id ?>,<?= $fav_val ?>)"><span class="wishlistbadge"><i class="<?= $class ?>"></i></span></a>
                                                 <?php } ?>
                                            </span>
										</div>
									</div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    $this->registerJs("
    $('.autoplay').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        margin:30,
        autoplaySpeed: 2000,
        nextArrow: '<i class=\"fa fa-arrow-right\"></i>',
        prevArrow: '<i class=\"fa fa-arrow-left\"></i>',
        responsive: [
               {
                 breakpoint: 1048,
                 settings: 
                  {
                    arrows: false,
                    centerMode: false,
                    slidesToShow: 4,
					slidesToScroll: 1
                  }
               },
			  {
                 breakpoint: 900,
                 settings: 
                  {
                    arrows: false,
                    centerMode: false,
                    slidesToShow: 3,
					slidesToScroll: 1
                  }
               },
			  {
                 breakpoint: 756,
                 settings: 
                  {
                    arrows: false,
                    centerMode: false,
                    slidesToShow: 2,
					slidesToScroll: 1
                  }
               },
              {
                breakpoint: 480,
                settings:
                {
                  arrows: false,
                  centerMode: false,
				  slidesToShow: 2,
				  slidesToScroll: 1
                }
              }
        ]
    });
");
 } ?>