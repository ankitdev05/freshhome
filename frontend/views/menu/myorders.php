<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\Tabs;
use yii\bootstrap4\ActiveForm;

$this->title= 'My Kitchen';
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/jquery.countdown.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$p_count = $p_dataProvider->getTotalCount();
?>
    <div class="common-color">
        <section class="address-list">
            <div class="container">
                <div class="common-dashbaord">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <div class="profile-side">
                                <?= $this->render('//supplier/_left_panel') ?>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-9">
                            <div class="profile-body suppliermenu">
                                <?=  Tabs::widget([
                                    'encodeLabels' => false,
                                    'items' => [
                                        [
                                            'label' =>yii::t('yii','Pending <span class="badge" style="position: relative">'.$p_count.'</span>'),
                                            'content' =>'<br/>'.$this->render("//menu/kitchen_orders",['dataProvider'=>$p_dataProvider,'searchModel'=>$searchModel])
                                        ],
                                        [
                                            'label' =>yii::t('yii','Delivered'),
                                            'content' =>'<br/>'.$this->render("//menu/kitchen_orders",['dataProvider'=>$c_dataProvider,'searchModel'=>$searchModel])
                                        ]
                                    ]
                                ]);?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="rejectOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cancel Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin(['action' =>Url::to(['/supplier/reject-order']) ,'options' => ['enctype' => 'multipart/form-data']]); ?>
                    <?= $form->field($model, 'order_id')->hiddenInput()->label(false); ?>
                    <?= $form->field($model, 'reject_type')->dropDownList(['Item not available' => 'Item not available','Time slot not available'=>'Time slot not available'],['prompt'=>'Select Reason...']); ?>
                    <?= $form->field($model, 'reject_message')->textarea(['rows' => 3]) ?>
                    <?= Html::submitButton(Yii::t('yii', 'Save'), ['class' => 'btn custom-btn']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJs('
    $(".rejectOrder").on("click",function(){
        var order_id = $(this).attr("data-attr");
        $("#order-order_id").val(order_id);
        $("#rejectOrder").modal();
    })
');