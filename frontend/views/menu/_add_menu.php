<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use himiklab\thumbnail\EasyThumbnailImage;
use kartik\file\FileInput;
use common\models\Cuisine;
use common\models\Category;
use common\models\Meal;
use kartik\time\TimePicker;

$screen_id= 1;
$imageSrc = $delUrl = '';
if(!empty($model->dish_image)){
    $image=EasyThumbnailImage::thumbnailFileUrl(
        "../../frontend/web/uploads/".$model->dish_image,200,200,EasyThumbnailImage::THUMBNAIL_INSET
    );
    $imageSrc	= [Html::img($image, ['class'=>'file-preview-image', 'alt'=>$model->dish_name, 'title'=>$model->dish_name])];
    $delUrl=[['caption'=>$model->dish_name,'url'=>yii::$app->request->baseUrl.'/supplier/deletemenuimage?id='.$model->id]];
}

if(empty($model->dist_qty))
    $model->dist_qty = 0;

$language_id	= language_id;

$cuisines = Cuisine::find()->select('*')->joinWith('cuisineName as cn')->where(['status'=>'active','cn.language_id'=>$language_id])->orderBy('cn.cuisine_name asc')->asArray()->all();
foreach($cuisines as $cuisine)
    $cuisinesList[$cuisine['cuisine_id']] = $cuisine['cuisine_name'];

$categories = Category::find()->select('*')->joinWith('categoryDescription as cd')->where(['cat_id' => $screen_id,'active' => 1,'cd.language_id' => language_id])->andWhere(['is','parent_id',null])->asArray()->orderBy('cd.category_name')->all();

foreach($categories as $category)
    $categoryList[$category['category_id']] = $category['category_name'];

$meals			= Meal::find()->select('*')->joinWith('mealName as mn')->where(['status'=>'active','mn.language_id'=>$language_id])->orderBy('mn.meal_name asc')->asArray()->all();
foreach($meals as $meal)
    $mealList[$meal['meal_id']] = $meal['meal_name'];

if(empty($model->dish_since))
    $model->dish_since = "00:00";
if(!empty($model->id)){
    $all_cats = $model->menuCategories;
    $all_cuisines = $model->menuCuisines;
    $all_meals = $model->menuMeals;

    if(!empty($all_cats)){
       foreach ($all_cats as $all_cat)
           $cats[] =$all_cat->category_id;
       $model->category = $cats;
    }
    if(!empty($all_cuisines)){
        foreach ($all_cuisines as $all_cuisine)
            $cus[] = $all_cuisine->cuisine_id;
        $model->cuisine = $cus;
    }
    if(!empty($all_meals)){
        foreach ($all_meals as $all_meal)
            $meal[] = $all_meal->meal_id;
        $model->meal = $meal;
    }
}
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'dish_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-12">
    <?= $form->field($model, 'dish_image')->widget(FileInput::classname(),
        [
            'options'=>['accept'=>'image/*'],
            'pluginOptions'=>
                [
                    'allowedFileExtensions'=>['jpg','gif','png'],
                    'overwriteInitial'=>true,
                    'initialPreview'=>$imageSrc,
                    'initialPreviewConfig'=>$delUrl
                ],
        ]) ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($model, 'dish_description')->textArea(['rows' => 3]) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'dish_price')->textInput(['step'=>"0.05",'pattern'=>"^\d+(?:\.\d{1,2})?$",'type'=>'number','maxlength' => true])->hint("Transaction charges ".config_transaction_fee.'% & Freshhomee fee '.config_freshhommee_fee.'% & VAT '.config_vat.'%') ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, 'collected_amount')->textInput(['type' => 'number', 'maxlength' => true,'step' => '0.01' ,'min' => 0,'readonly' => true]) ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($model, 'discount_product')->checkbox() ?>
    </div>
    <div class="col-sm-6 discount" style="display:none;">
        <?= $form->field($model, 'discount')->textInput(['type' => 'number', 'maxlength' => true,'step' => '0.01' ,'min' => 0,'max' => 100]) ?>
    </div>
    <div class="w-100"></div>

    <?= $form->field($model, 'dish_since')->hiddenInput(['value'=> ''])->label(false); ?>
    <div class="col-sm-6">
        <?= $form->field($model, 'dist_qty')->textInput(['type'=>'number','maxlength' => true]) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'dish_serve')->widget(Select2::classname(), [
            'data' => [1=>1,2=>2,3=>3],
            'options' => ['placeholder' => 'Please select ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]) ?>
    </div>

    <?= $form->field($model, 'cuisine')->hiddenInput(['value'=> ''])->label(false); ?>
    
    <div class="col-sm-6">
        <?= $form->field($model, 'category')->widget(Select2::classname(), [
            'data' => $categoryList,
            'options' => ['placeholder' => 'Please select ...','multiple' => true],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <?= $form->field($model, 'meal')->hiddenInput(['value'=> ''])->label(false); ?>
    
    <div class="col-sm-6">
        <?= $form->field($model, 'status')->widget(Select2::classname(), [
            'data' => ['active'=>'Active','inactive'=>'In active'],
            //'options' => ['placeholder' => 'Please select ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="form-group col-sm-12">
        <?= Html::submitButton(Yii::t('yii', 'Save'), ['class' => 'btn custom-btn']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php

$percentage = config_transaction_fee+config_freshhommee_fee+config_vat;
$this->registerJs('

    if($("#menu-discount_product").is(":checked")){
        $(".discount").show();
    }
    $("#menu-discount_product").on("change onchange keyup",function(){
        if($(this).is(":checked")){
            $(".discount").show();
        }else{
            $(".discount").hide();
            $("#menu-discount").val("");
            calculatePrice();
        }
             
    })
    
    $("#menu-dish_price").on("keyup onchange",function(){
        calculatePrice();
    })
    $("#menu-discount").on("keyup onchange",function(){
        calculatePrice();
    })
');
$this->registerJs('
    function calculatePrice(){
        var price = $("#menu-dish_price").val();
        var discount = $("#menu-discount").val();
        if(discount!=0){
            price = parseFloat(price-( price * discount / 100 )).toFixed(2);        
        }
        var total_percent = '.$percentage.';
        price = parseFloat(price-( price * total_percent / 100 )).toFixed(2);
       
        $("#menu-collected_amount").val(price)            

    }
',yii\web\View::POS_HEAD);