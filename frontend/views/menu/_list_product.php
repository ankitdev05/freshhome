<?php
use yii\helpers\Html;
if($model->product_type==1)
    $edit_link =  Html::a('Edit', ['supplier/updatemenu','id' => $model->id], ['data-pjax' => 0,'class'=>'btn custom-btn mt-3']);
else
    $edit_link =  Html::a('Edit', ['/supplier','type'=>'updateProduct','id' => $model->id], ['data-pjax' => 0,'class'=>'btn custom-btn mt-3']);
?>
<div class="col-md-3 col-6 product-grids overley-supplier mb-3">
    <div class="agile-products ">
        <div class="item ">
            <img src="<?= $model->mainImage ?>" alt="<?= $model->dish_name ?>" class="product-grid">
            <div class="product-content">
                <div class="title">
                    <p class="producttitle"><a href="javascript:void(0)"><?= $model->dish_name ?></a></p>
                </div>
                <div class="price float-left w-100 py-2"><span class="price-tag"><b> AED <?= $model->dish_price ?></b></span> <span class="rating-rate"><i class="fa fa-star"></i> &nbsp;(<?= $model->avgReviews ?>)</span></div>

            </div>
        </div>
        <div class="overlay-order">
            <div class="inner-overley">
                <p>Pending Order</p>
                <p><?= $model->pendingOrders ?></p>
                <p>User Views</p>
                <p><?= $model->view ?></p>
                <p>Quantity Available</p>
                <p><?= $model->dist_qty ?></p>
                <p><?= $edit_link ?></p>
            </div>
        </div>
    </div>
</div>