<?php

$this->title = 'Add menu';
?>
<div class="common-color">
    <section class="address-list">
        <div class="container">
            <div class="common-dashbaord">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-12 mb-3">
                        <div class="profile-side">
                            <?= $this->render('//supplier/_left_panel') ?>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                        <div class="profile-body suppliermenu">
                            <div class="address-list-block">
                                <?= $this->render("//menu/_add_menu",['model'=>$model]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

