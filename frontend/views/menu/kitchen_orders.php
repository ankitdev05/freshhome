<?php
use yii\widgets\Pjax;
use yii\widgets\ListView;
?>
<?=  ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '//supplier/orders/_list_supplier_orders',
    'options' => [
        'tag' => false,
        'class' => 'form-row',
        'id' => 'list-wrapper',
    ],
    'itemOptions' => [
        'tag' => false
    ],
    'pager' => [
        'options' => ['class'=>'pagination justify-content-end'],
        'prevPageLabel' =>'Previous',
        'nextPageLabel' =>'Next',
        'pageCssClass' => 'page-item',
        'disabledPageCssClass' => 'page-link disabled',
        'linkOptions' => ['class' => 'page-link'],
    ]
]); ?>
