<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class Search extends Model
{
    public $category;
    public $name;
    public $type;

    public function rules()
    {
        return [
            [['category', 'name', 'type'], 'trim'],
        ];
    }
}