<?php
namespace frontend\models;

use common\models\Apiuser;
use common\models\UserToRoles;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $name;
    public $dob;
    public $email;
    public $password;
    public $username;
    public $latitude;
    public $longitude;
    public $location;
    public $building_no;
    public $flat_no;
    public $floor_no;
    public $landmark;
    public $city;
    public $role;
    public $password_repeat;
    public $phone_number;
    public $occupation_id;
    public $supplier_type;
    public $building_name;



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username','email','dob','name','latitude','longitude','location','building_no','flat_no','floor_no','landmark','city','role','phone_number'], 'trim'],
            [['name','role','phone_number','password_repeat','occupation_id','dob'], 'required','on'=>'registration'],
            [['name','role','phone_number','password_repeat','dob','supplier_type','location','building_name','flat_no','floor_no','landmark','city'], 'required','on'=>'supplier_registration'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['phone_number', 'checkPhone'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match", 'on' => 'registration' ],
        ];
    }
    public function checkPhone($attribute, $params){
        $checkPhone = User::find()->where(['phone_number'=>$this->phone_number,'status'=>10])->count();
        if($checkPhone > 0){
            $this->addError($attribute, Yii::t('app', 'This phone number has already been taken.'));
        }
    }
    public function attributeLabels()
    {
        return [
            'dob' => Yii::t('app','Date of birth'),
            'occupation_id' => Yii::t('app','Occupation'),
            'supplier_type' =>  Yii::t('app','Product Type'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->dob = $this->dob;
        $user->name = $this->name;
        $user->latitude = $this->latitude;
        $user->longitude = $this->longitude;
        $user->location = $this->location;
        $user->building_no = $this->building_no;
        $user->flat_no = $this->flat_no;
        $user->floor_no = $this->floor_no;
        $user->landmark = $this->landmark;
        $user->city = $this->city;
        $user->role = $this->role;
        $user->phone_number = $this->phone_number;
        $user->occupation_id = $this->occupation_id;

        $user->availability = 'offline';
        $user->is_user		= 'yes';
        $user->is_supplier	= 'no';
        if($this->role==1)
            $user->is_user = 'yes';
        elseif($this->role==2)
            $user->is_supplier = 'yes';
        $user->ip_address		= Yii::$app->getRequest()->getUserIP();
        $user->last_login		= time();
        $user->last_login_ip	= Yii::$app->getRequest()->getUserIP();


        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        if($user->save()){
            $api_model = Apiuser::findOne($user->id);
            if($user->is_user == 'yes')
                $api_model->updateUserData;
            if($user->is_supplier == 'yes')
                $api_model->updateSuppData;

            $user_to_role	= new UserToRoles();
            $user_to_role->user_id	= $user->id;
            $user_to_role->role_id	= $user->role;
            $user_to_role->save();

            return $this->sendEmail($user);
        }
    }

    public function signUpSupplier()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->dob = $this->dob;
        $user->name = $this->name;
        $user->latitude = $this->latitude;
        $user->longitude = $this->longitude;
        $user->location = $this->location;
        $user->building_no = $this->building_no;
        $user->flat_no = $this->flat_no;
        $user->floor_no = $this->floor_no;
        $user->landmark = $this->landmark;
        $user->city = $this->city;
        $user->role = $this->role;
        $user->phone_number = $this->phone_number;
        $user->occupation_id = $this->occupation_id;

        $user->availability = 'offline';
        $user->is_user		= 'yes';
        $user->is_supplier	= 'yes';
        if($this->role==1)
            $user->is_user = 'yes';
        elseif($this->role==2)
            $user->is_supplier = 'yes';
        $user->ip_address		= Yii::$app->getRequest()->getUserIP();
        $user->last_login		= time();
        $user->last_login_ip	= Yii::$app->getRequest()->getUserIP();


        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        if($user->save()){
            $api_model = Apiuser::findOne($user->id);
            if($user->is_user == 'yes')
                $api_model->updateUserData;

            $user_to_role	= new UserToRoles();
            $user_to_role->user_id	= $user->id;
            $user_to_role->role_id	= $user->role;
            $user_to_role->save();

            return $this->sendEmail($user);
        }
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }


}
