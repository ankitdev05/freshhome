<?php

namespace backend\controllers;

use Yii;
use common\models\HomeCategory;
use common\models\HomeCategorySearch;
use yii\web\UploadedFile;
use common\models\Library;

use common\models\MainCategories;
use common\models\HomeCategoryDescription;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl; 
/**
 * MainCategoriesController implements the CRUD actions for MainCategories model.
 */
class HomeCategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','update','view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MainCategories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HomeCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
     
        return $this->render('index', [ 
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


       public function actionCreate()
    {

        $model = new HomeCategory();

        if ($model->load(Yii::$app->request->post())) {

            $modelvalue = Yii::$app->request->post('HomeCategory');
                   
              $image = UploadedFile::getInstance($model, 'image');
            if(!empty($image)){
                $library             = new Library();
                $model->image  = $library->saveFile($image,'homecategory');
                $model->file_info = $library->json_encodeArray($image);
                
            }

               $model->save(); 

            
            HomeCategoryDescription::deleteAll(['home_category_id'=>$model->home_category_id]);

                foreach($modelvalue['home_category_names'] as $language_id=>$name){
               
                    $bdesc = new HomeCategoryDescription();
                    $bdesc->home_category_id     = $model->home_category_id;
                    $bdesc->language_id    = $language_id;
                    $bdesc->home_category_name   = $name;
                    $bdesc->save(false);  
                }

              
            


           return $this->redirect(['index']);
           // return $this->redirect(['view', 'id' => $model->brand_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    } 




        public function actionUpdate($id)
    {


        $model = $this->findModel($id);
        $old_image = $model->image;
     
        $descriptions = HomeCategoryDescription::findAll(['home_category_id'=>$id]);
        
        if(!empty($descriptions)){
        $brandname = $descriptions[0]['home_category_name'];
        }else{
          $brandname = "(not set)" ; 
        } 
        
        if ($model->load(Yii::$app->request->post())) {

         
               $image = UploadedFile::getInstance($model, 'image');
            if(!empty($image)){
                $library = new Library();
                $model->image = $library->saveFile($image,'homecategory');
                $model->file_info  = $library->json_encodeArray($image);

            }else 
                $model->image = $old_image;



         $modelvalue = Yii::$app->request->post('HomeCategory');

         $model->save();

              HomeCategoryDescription::deleteAll(['home_category_id'=>$model->home_category_id]);

                foreach($modelvalue['home_category_names'] as $language_id=>$name){
               
                    $bdesc = new HomeCategoryDescription();
                    $bdesc->home_category_id     = $model->home_category_id;
                    $bdesc->language_id    = $language_id;
                    $bdesc->home_category_name   = $name;
                    $bdesc->save(false);  
                } 

      

         return $this->redirect(['view', 'id' => $model->home_category_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'brandname' => $brandname, 
        ]);
    }
 

       public function actionView($id)
    {
  
   
   $descriptions = HomeCategoryDescription::findAll(['home_category_id'=>$id]);

    if(!empty($descriptions)){
        $brandname = $descriptions[0]['home_category_name'];

        }else{
          $brandname = "(not set)" ; 
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'brandname' => $brandname, 
        ]);
    }

 
    /**
     * Finds the MainCategories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MainCategories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HomeCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
