<?php

namespace backend\controllers;

use Yii;
use common\models\OrderRatings;
use common\models\OrderRatingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * OrderreviewController implements the CRUD actions for OrderRatings model.
 */
class OrderreviewController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','acceptreview'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [

                ],
            ],
        ];
    }

    /**
     * Lists all OrderRatings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderRatingsSearch();
        $searchModel->is_active = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'url_format' => Yii::$app->request->baseUrl.'/orderreview/acceptreview',
        ]);
    }
    public function actionAcceptreview($id){
        $model = $this->findModel($id);
        $model->is_active = 1;
        $model->save();
        Yii::$app->session->setFlash('success', 'Your reviews has been accepted successfully.');
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    /**
     * Finds the OrderRatings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderRatings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderRatings::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
