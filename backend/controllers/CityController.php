<?php

namespace backend\controllers;

use Yii;
use common\models\City;
use common\models\CitySearch;
use common\models\CityDescription;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','update'],
						'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    
                ],
            ],
        ];
    }

    /**
     * Lists all City models.
     * @return mixed
     */
    public function actionIndex()
    {
		
        $searchModel = new CitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionCreate(){
        $model		= new City();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $city_names	= $model->city_name;
            if($model->save()){
                CityDescription::deleteAll(['city_id'=>$model->id]);
                foreach($city_names as $language_id=>$name){
                    $c_desc					= new CityDescription();
                    $c_desc->city_id		= $model->id;
                    $c_desc->language_id	= $language_id;
                    $c_desc->city_name		= $name;
                    $c_desc->save();
                }
                Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
                return $this->redirect(['index']);
            }
        }
        return $this->render('create',['model'=>$model]);
    }

    public function actionUpdate($id){
        $model 		= $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $city_names	= $model->city_name;
            if($model->save()){
                CityDescription::deleteAll(['city_id'=>$model->id]);
                foreach($city_names as $language_id=>$name){
                    $c_desc					= new CityDescription();
                    $c_desc->city_id		= $model->id;
                    $c_desc->language_id	= $language_id;
                    $c_desc->city_name		= $name;
                    $c_desc->save();
                }
                    Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
                return $this->redirect(['index']);
            }
        }
        return $this->render('update',['model'=>$model]);
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return City the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = City::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
