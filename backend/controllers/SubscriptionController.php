<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use common\models\UserSearch;

/**
 * Subscription controller
 */
class SubscriptionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['suppliers', 'drivers','block','unblock'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'block' => ['POST'],
                    'unblock' => ['POST'],
                ],
            ],
        ];
    }

    public function actionSuppliers(){
        $searchModel = new UserSearch();
        $searchModel->is_supplier = 'yes';
        $dataProvider = $searchModel->subscriberSearch(Yii::$app->request->queryParams);

        return $this->render('suppliers',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDrivers(){
        $searchModel = new UserSearch();
        $searchModel->is_driver = 'yes';
        $dataProvider = $searchModel->subscriberSearch(Yii::$app->request->queryParams);

        return $this->render('drivers',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBlock($id){
        $model = $this->findModel($id);
        $model->status = 9;
        if($model->save())
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);

    }
    public function actionUnblock($id){
        $model = $this->findModel($id);
        $model->status = 10;
        if($model->save())
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);

    }
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}