<?php

namespace backend\controllers;

use Yii;
use common\models\Library;
use common\models\Category;
use common\models\CategorySearch;
use common\models\MainCategories;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use common\models\HomeCategory;
use common\models\CategoryDescription;





/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
            [
             'actions' => ['index','create','update','view','delete-image','saveform'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex($cat_id)
    {
        $main_cat = $this->findMainCat($cat_id);

        $searchModel = new CategorySearch();
        $searchModel->cat_id = $cat_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'main_cat' => $main_cat,
            'cat_id' => $cat_id,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @param integer $cat_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id,$cat_id)
    {
        $main_cat = $this->findMainCat($cat_id);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'main_cat' => $main_cat,
            'cat_id' => $cat_id,
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @param integer $cat_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCreate($cat_id)
    {
        $main_cat = $this->findMainCat($cat_id);
        $model = new Category();
        $model->cat_id = $cat_id;
       


        $all_home_category = [];

         $all_home_categorysql ="SELECT tbl_home_category.home_category_id,tbl_home_category_description.home_category_name FROM `tbl_home_category` LEFT JOIN tbl_home_category_description on tbl_home_category.home_category_id = tbl_home_category_description.home_category_id WHERE tbl_home_category_description.language_id=1"; 
        $all_home_category_data = Yii::$app->db->createCommand($all_home_categorysql)->queryAll();
      
      foreach ($all_home_category_data as $value) {
        $all_home_category[$value['home_category_id']]=$value['home_category_name'];

      };
        

        if ($model->load(Yii::$app->request->post())) {
       
            $image = UploadedFile::getInstance($model, 'image');

            if(!empty($image)){
                $library = new Library();
                $model->image  = $library->saveFile($image,'category');
            }
            $model->saveCategoryPath();

            $model->save();

              $category_name = Yii::$app->request->post('Category');
              CategoryDescription::deleteAll(['category_id'=>$model->category_id]);
                foreach($category_name['name'] as $language_id=>$name){
                
                    $bdesc = new CategoryDescription();
                    $bdesc->category_id     = $model['category_id'];
                    $bdesc->language_id    = $language_id;
                    $bdesc->category_name   = $name;
                    $bdesc->save(false); 
                } 
 

   $parent_id = $model['category_id']; 

   $this->redirect(array('category/update', 'id' => $parent_id, 'cat_id' => $cat_id));
 
     
            // return $this->redirect(['index','cat_id' => $cat_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'cat_id' => $cat_id,
            'main_cat' => $main_cat,
            'all_home_category' => $all_home_category,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $cat_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id,$cat_id)
    {
        $main_cat = $this->findMainCat($cat_id);
        $model = $this->findModel($id);
        $model->cat_id = $cat_id;
        $old_image = $model->image;
        
         $all_home_category = [];
         $all_home_categorysql ="SELECT tbl_home_category.home_category_id,tbl_home_category_description.home_category_name FROM `tbl_home_category` LEFT JOIN tbl_home_category_description on tbl_home_category.home_category_id = tbl_home_category_description.home_category_id WHERE tbl_home_category_description.language_id=1"; 
        $all_home_category_data = Yii::$app->db->createCommand($all_home_categorysql)->queryAll();
      

      foreach ($all_home_category_data as $value) {
        $all_home_category[$value['home_category_id']]=$value['home_category_name'];

      };

    
      
      


        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if(!empty($image)){
                $library   			 = new Library();
                $model->image  = $library->saveFile($image,'category');
            }else{
            $model->image = $old_image;
            }

            $model->home_category_id = $model['home_category_id'];
            $model->updateCategoryPath();
            $model->save();

 
               $category_name = Yii::$app->request->post('Category');
              CategoryDescription::deleteAll(['category_id'=>$model->category_id]);
                foreach($category_name['name'] as $language_id=>$name){
                
                    $bdesc = new CategoryDescription();
                    $bdesc->category_id     = $model['category_id'];
                    $bdesc->language_id    = $language_id;
                    $bdesc->category_name   = $name;
                    $bdesc->save(false); 
                } 


        } 

        return $this->render('update', [
            'model' => $model,
            'cat_id' => $cat_id,
            'main_cat' => $main_cat,
            'all_home_category' => $all_home_category,
         
        ]);
    }

    public function actionSaveform(){
        $model = new Category();
       
        if ($model->load(Yii::$app->request->post())) {


           $image = UploadedFile::getInstance($model, 'image1');

            if(!empty($image)){
                $library = new Library();
                $model->image  = $library->saveFile($image,'category');
            }
            $model->saveCategoryPath(); 
          
         
 
          $parent_id = Yii::$app->request->post('parentid');
          $sub_parent_id = Yii::$app->request->post('sub_parentid');
          $home_category_id = Yii::$app->request->post('home_category_id');
 
          
          $cat_id = Yii::$app->request->post('cat_id');

          $model->parent_id = $sub_parent_id; 
          $model->home_category_id = $home_category_id;  
          $model->cat_id = $cat_id;
          // $model->name = $model['name1'];
          $model->active = $model['active1'];
          $model->save(false); 

           

      
 
            $category_name = Yii::$app->request->post('Category');
           
             

              CategoryDescription::deleteAll(['category_id'=>$model['category_id']]);
         
                foreach($category_name['name1'] as $language_id=>$name){
                
                    $bdesc = new CategoryDescription();
                    $bdesc->category_id     = $model['category_id'];
                    $bdesc->language_id    = $language_id;
                    $bdesc->category_name   = $name;
                    $bdesc->save(false); 
              
                }


       
        
   $this->redirect(array('category/update', 'id' => $parent_id, 'cat_id' => $cat_id));
   
      }
       

    }


    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findMainCat($id)
    {
        if (($model = MainCategories::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDeleteImage($id){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $fullUrl = Yii::getAlias('@frontend').'/web/uploads/';
        unlink($fullUrl.$model->image);
        $model->image = '';
        $model->save();
        $json['result']= 'done';
        return $json;
    }
}
