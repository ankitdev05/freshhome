<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use common\models\LoginForm;
use common\models\UserSearch;
use common\models\MenuSchedulesSearch;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','schedules','viewschedule'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login(3)) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
	public function actionSchedules(){
		$searchModel = new UserSearch();
		$searchModel->is_supplier = 'yes';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('schedules',[
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
		]);
	}
	public function actionViewschedule($id){
		
		$user_info	= User::findOne($id);
		if(empty($user_info))
			throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
		if($user_info->is_supplier!='yes')
			throw new ForbiddenHttpException('You\'re not authorized to view this page.');
		
		$searchModel = new MenuSchedulesSearch();
		$searchModel->supplier_id = $id;
        $dataProvider = $searchModel->searchSch(Yii::$app->request->queryParams);
		
		return $this->render('viewschedule',['user_info'=>$user_info,'dataProvider'=>$dataProvider,'searchModel'=>$searchModel]);
	}
}
