<?php

namespace backend\controllers;

use Yii;


use common\models\Menu;
use common\models\User;
use common\models\ContactMessage;
use common\models\Library;
use common\models\MenuMeals;
use common\models\MenuSearch;
use common\models\MenuCuisines;
use common\models\MenuCategories;

use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','view','update','deleteimage','supplierimages','acceptimages','rejectimage','getproductdetails'],
						'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex($id)
    {
		$user_info	= User::findOne($id);
		if(empty($user_info))
			throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
		if($user_info->is_supplier!='yes')
			 throw new ForbiddenHttpException('You\'re not authorized to view this page.');
		 
        $searchModel = new MenuSearch();
		$searchModel->supplier_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user_info' => $user_info,
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($s_id)
    {
		$user_info	= User::findOne($s_id);
		if(empty($user_info))
			throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
		if($user_info->is_supplier!='yes')
			 throw new ForbiddenHttpException('You\'re not authorized to view this page.');
		 
        $model = new Menu();

        if ($model->load(Yii::$app->request->post())) {
			
			$model->supplier_id = $s_id;
			$dish_image 		= UploadedFile::getInstance($model, 'dish_image');
			if(!empty($dish_image)){
				$library    = new Library();
				$model->dish_image  = $library->saveFile($dish_image,'menu');
				$model->image_approval = 1;
			}
			if($model->save()){
				if(isset($_POST['Menu']['category']) && !empty($_POST['Menu']['category'])){
					$cats = $_POST['Menu']['category'];
					foreach($cats as $cat){
						$cat_model				= new MenuCategories();
						$cat_model->menu_id		= $model->id;
						$cat_model->category_id	= $cat;
						$cat_model->save();
					}
					
				}
				if(isset($_POST['Menu']['cuisine']) && !empty($_POST['Menu']['cuisine'])){
					$cuisines = $_POST['Menu']['cuisine'];
					foreach($cuisines as $cuisine){
						$cat_model				= new MenuCuisines();
						$cat_model->menu_id		= $model->id;
						$cat_model->cuisine_id	= $cuisine;
						$cat_model->save();
					}
					
				}
				if(isset($_POST['Menu']['meal']) && !empty($_POST['Menu']['meal'])){
					$meals = $_POST['Menu']['meal'];
					foreach($meals as $meal){
						$cat_model				= new MenuMeals();
						$cat_model->menu_id		= $model->id;
						$cat_model->meal_id		= $meal;
						$cat_model->save();
					}
					
				}
				Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
				return $this->redirect(['index', 'id' => $model->supplier_id]);
			}
        }

        return $this->render('create', [
            'model' => $model,
            'user_info' => $user_info,
        ]);
    }
	 /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id,$s_id)
    {
		$user_info	= User::findOne($s_id);
		if(empty($user_info))
			throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
		if($user_info->is_supplier!='yes')
			throw new ForbiddenHttpException('You\'re not authorized to view this page.');
		 
        return $this->render('view', [
            'model' => $this->findModel($id,$s_id),
            'user_info' => $user_info,
        ]);
    }
    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id,$s_id)
    {
		$user_info	= User::findOne($s_id);
		if(empty($user_info))
			throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
		if($user_info->is_supplier!='yes')
			throw new ForbiddenHttpException('You\'re not authorized to view this page.');
		 
        $model = $this->findModel($id,$s_id);
		$old_dish	= $model->dish_image;
        if ($model->load(Yii::$app->request->post())) {
			
			$dish_image 		= UploadedFile::getInstance($model, 'dish_image');
			if(!empty($dish_image)){
				$library    	= new Library();
				$model->dish_image  = $library->saveFile($dish_image,'menu');
				$model->image_approval = 1;
			}else
				$model->dish_image = $old_dish;
			
            if($model->save()){
				MenuCategories::deleteAll(['menu_id'=>$model->id]);
				MenuCuisines::deleteAll(['menu_id'=>$model->id]);
				MenuMeals::deleteAll(['menu_id'=>$model->id]);
				if(isset($_POST['Menu']['category']) && !empty($_POST['Menu']['category'])){
					$cats = $_POST['Menu']['category'];
					foreach($cats as $cat){
						$cat_model				= new MenuCategories();
						$cat_model->menu_id		= $model->id;
						$cat_model->category_id	= $cat;
						$cat_model->save();
					}
					
				}
				if(isset($_POST['Menu']['cuisine']) && !empty($_POST['Menu']['cuisine'])){
					$cuisines = $_POST['Menu']['cuisine'];
					foreach($cuisines as $cuisine){
						$cat_model				= new MenuCuisines();
						$cat_model->menu_id		= $model->id;
						$cat_model->cuisine_id	= $cuisine;
						$cat_model->save();
					}
					
				}
				if(isset($_POST['Menu']['meal']) && !empty($_POST['Menu']['meal'])){
					$meals = $_POST['Menu']['meal'];
					foreach($meals as $meal){
						$cat_model				= new MenuMeals();
						$cat_model->menu_id		= $model->id;
						$cat_model->meal_id		= $meal;
						$cat_model->save();
					}
					
				}
				Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
				return $this->redirect(['index', 'id' => $model->supplier_id]);
			}
				
        }

        return $this->render('update', [
            'model' => $model,
            'user_info' => $user_info,
        ]);
    }


    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id,$s_id)
    {
        if (($model = Menu::findOne(['id'=>$id,'supplier_id'=>$s_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
	public function actionDeleteimage($id,$s_id){
		$fullurl 			= Yii::$app->basePath . '/frontend/web/uploads/';
		$absolute_url 		= str_replace('backend/','',$fullurl);
		$model 				= $this->findModel($id,$s_id);
		unlink($absolute_url.$model->dish_image);
		$model->dish_image	='';
		$model->image_approval = 0;
		$model->save();
		$json['result']= 'done';
		echo json_encode($json['result']);
	}
	public function actionSupplierimages(){
        $contact_model      = new ContactMessage();
        if ($contact_model->load(Yii::$app->request->post()) && $contact_model->validate()) {
            $user_name	= User::findOne($contact_model->user_id);
            if(!empty($user_name))
                $contact_model->message	= str_replace("[user_name]",$user_name->name,$contact_model->message);

            Yii::$app->mailer->compose()
                ->setHtmlBody(nl2br($contact_model->message))
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($contact_model->email)
                ->setSubject($contact_model->subject)
                ->send();

            Yii::$app->session->setFlash('success', 'Your message has been sent successfully.');
            return $this->redirect(['rejectimage','id'=>$contact_model->send_message]);

        }
		$searchModel 		= new MenuSearch();
        $dataProvider = $searchModel->imageSearch(Yii::$app->request->queryParams);
		
		return $this->render('_images', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' =>'Dish Item Image Approval',
			'url_format' =>Yii::$app->request->baseUrl.'/menu/acceptimages',
			'reject_url' =>Yii::$app->request->baseUrl.'/menu/rejectimage',
			'role' =>2,
			'contact_model' =>$contact_model,
        ]);
	}

	//start get product details by catotech 

	  public function actionGetproductdetails()
    { 
    	$menudetails =[];
        $id  = Yii::$app->request->post('id');
 
          $menudetails['details'] = Menu::find()->select('*')->where(['id' => $id])->asArray()->one();
      
              $path1 = Url::base();  
                $path = str_replace("administrator","",$path1);
                $src = $path."frontend/web/uploads/"; 
    
  $categorylistsql ="SELECT tbl_main_categories.* FROM `tbl_menu` 
  LEFT JOIN tbl_user on tbl_menu.supplier_id = tbl_user.id  
  LEFT JOIN tbl_main_categories on tbl_main_categories.id = tbl_user.supplier_type  
  WHERE tbl_menu.supplier_id =".$menudetails['details']['supplier_id'];
 

        $categorylist = Yii::$app->db->createCommand($categorylistsql)->queryOne();
            
         $menudetails['categoryname']=$categorylist['name'];
         $menudetails['imgurl'] = $src;
     
              return json_encode($menudetails);
      
    }

	//end get product details by catotech
	/**
     * Accept Suppliers/Users pending images.
     */
	public function actionAcceptimages($id){
		if(!empty(trim($id))){
			if($id=="all"){
				Menu::updateAll([
					'image_approval' => 1,
				], ['and',['!=','dish_image','']]);
			}else{
				Menu::updateAll([
				'image_approval' => 1,
				], ['and',['IN','id',explode(',',$id)],['!=','dish_image','']]);
			}
			
			Yii::$app->session->setFlash('success', 'Your records has been updated successfully.');
		}
		
		return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
	}
    public function actionRejectimage($id){
        $model = Menu::findOne(['id'=>$id]);
        if(!empty($model)){
            $fullurl 			= Yii::$app->basePath . '/frontend/web/uploads/';
            $absolute_url 		= str_replace('backend/','',$fullurl);
            unlink($absolute_url.$model->dish_image);
            $model->dish_image	='';
            $model->image_approval = 0;
            $model->save();
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }
}
