<?php

namespace backend\controllers;

use Yii;
use common\models\Settings;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class SettingsController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'error'],
						'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
					[
                        'actions' => ['update', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
					[
                        'actions' => ['view', 'id'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],[
                        'actions' => ['deleteimage', 'key'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
					[
                        'actions' => ['delete', 'id'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    /**
     * Creates a new Settings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model 				= new Settings();
		$results			= $model->find()->all();
		$ConfigData			= array();
		$fullurl 			= Yii::$app->basePath . '/frontend/web/images/store/';
		$absolute_url 		= str_replace('backend/','',$fullurl);
		foreach($results as $result)
		{
			$model->{$result->settings_key} = $result->settings_value;
			$ConfigData[$result->settings_key]=$result->settings_value;
		}
		if(isset($_POST['Settings']))
		{
			$image 		= UploadedFile::getInstance($model, 'config_logo');
			$fileName	= $ConfigData['config_logo'];
			$model->deleteAll();
			foreach($_POST['Settings'] as $key=>$value)
			{
				if($key!='config_logo'){
					$model_new = new Settings();
					$model_new->settings_key = $key;
					$model_new->settings_value = $value;
					$model_new->validate();
					$model_new->save();
				}
			}
			if(!empty($image)){
				$fileName	= 'store-'.time().'-'.str_replace(' ','',strtolower($image->name));
				$image->saveAs($absolute_url.$fileName);
			}
			$model_new = new Settings();
			$model_new->settings_key = 'config_logo';
			$model_new->settings_value = $fileName;
			$model_new->validate();
			$model_new->save();
			$this->refresh();
		}
		
		 return $this->render('create', [
            'model' => $model,
			'ConfigData'=>$ConfigData
        ]);
		
    }
	public function actionDeleteimage($key){
		$fullurl 			= Yii::$app->basePath . '/frontend/web/images/store/';
		$absolute_url 		= str_replace('backend/','',$fullurl);
		$model				= new Settings();
		$result				= $model->find()->where(['settings_key'=>'config_logo'])->one();
		unlink($absolute_url.$result->settings_value);
		$result->settings_value='';
		$result->save();
		$json['result']= 'done';
		echo json_encode($json['result']);
	}

    /**
     * Finds the Settings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Settings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Settings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
