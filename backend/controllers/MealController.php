<?php

namespace backend\controllers;

use Yii;
use common\models\Meal;
use common\models\MealSearch;
use common\models\MealDescription;
use common\models\Library;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
/**
 * MealController implements the CRUD actions for Meal model.
 */
class MealController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','update','delete-image'],
						'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    
                ],
            ],
        ];
    }

    /**
     * Lists all Meal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MealSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }
    /**
     * Finds the Meal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Meal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Meal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionCreate(){
        $model = new Meal();
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            $meal_names	= $model->meal_name;
            if($model->save()){
                $image = UploadedFile::getInstance($model, 'image');
                if(!empty($image)){
                    $library = new Library();
                    $model->image  = $library->saveFile($image,'meal');
                    $model->save();
                }

                MealDescription::deleteAll(['meal_id'=>$model->id]);
                foreach($meal_names as $language_id=>$name){
                    $c_desc					= new MealDescription();
                    $c_desc->meal_id		= $model->id;
                    $c_desc->language_id	= $language_id;
                    $c_desc->meal_name		= $name;
                    $c_desc->save();
                }
                Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_image = $model->image;
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            $meal_names	= $model->meal_name;
            if($model->save()){
                $image = UploadedFile::getInstance($model, 'image');
                if(!empty($image)){
                    $library = new Library();
                    $model->image  = $library->saveFile($image,'meal');

                }else  $model->image = $old_image;
                $model->save();
                MealDescription::deleteAll(['meal_id'=>$model->id]);
                foreach($meal_names as $language_id=>$name){
                    $c_desc					= new MealDescription();
                    $c_desc->meal_id		= $model->id;
                    $c_desc->language_id	= $language_id;
                    $c_desc->meal_name		= $name;
                    $c_desc->save();
                }
                Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    public function actionDeleteImage($id){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $fullUrl = Yii::getAlias('@frontend').'/web/uploads/';
        unlink($fullUrl.$model->image);
        $model->image = null;
        $model->meal_name = 'yes';
        $model->save();
        $json['result']= 'done';
        return $json;
    }
}
