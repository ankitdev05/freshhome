<?php

namespace backend\controllers;

use common\models\User;
use Yii;
use common\models\Order;
use common\models\OrderSearch;
use common\models\DriverCompanyNumbers;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','driver'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [

                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    public function actionDriver($id){
        $driver_info = DriverCompanyNumbers::findOne($id);
        $user_info = User::find()->where(['driver_id'=>$id])->one();
        $searchModel = new OrderSearch();


        $searchModel->driver_id = $user_info->id;
        $searchModel->order_accepted = 1;
        $cdataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel = new OrderSearch();
        $searchModel->driver_id = $user_info->id;
        $searchModel->order_accepted = 0;
        $pdataProvider = $searchModel->search(Yii::$app->request->queryParams);



        return $this->render('driver', [
            'searchModel' => $searchModel,
            'cdataProvider' => $cdataProvider,
            'pdataProvider' => $pdataProvider,
            'driver_info' => $driver_info,
        ]);
    }
}
