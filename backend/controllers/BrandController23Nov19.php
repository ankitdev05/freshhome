<?php

namespace backend\controllers;

use Yii;
use common\models\Brand;
use common\models\BrandSearch;
use yii\web\UploadedFile;
use common\models\Library;
use yii\filters\AccessControl;
    
// use backend\models\Brand;
// use backend\models\BrandSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Category;
use common\models\BrandCategory; 
use common\models\BrandDescription;



/**
 * BrandController implements the CRUD actions for Brand model.
 */
class BrandController extends Controller
{
    /**
     * {@inheritdoc}
     */

       public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','update','getcategory','view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Brand models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Brand model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
  

   $descriptions = BrandDescription::findAll(['brand_id'=>$id]);
   $brandname = $descriptions[0]['brand_name'];
       
  $brandid=$id; 
  $categorylistsql ="SELECT tbl_brand_category.category_id,tbl_category.name FROM `tbl_brand_category` LEFT JOIN tbl_category on tbl_brand_category.category_id = tbl_category.category_id WHERE tbl_brand_category.brand_id=".$brandid;
        $categorylist = Yii::$app->db->createCommand($categorylistsql)->queryAll();


        return $this->render('view', [
            'model' => $this->findModel($id),
            'categorylist' => $categorylist,
            'brandname' => $brandname, 
        ]);
    }

    /**
     * Creates a new Brand model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Brand();

        if ($model->load(Yii::$app->request->post())) {

            $modelvalue = Yii::$app->request->post('Brand');
            $model->maincategoryid = $modelvalue['maincategoryid'];
            $image = UploadedFile::getInstance($model, 'image');
            if(!empty($image)){
                $library             = new Library();
                $model->image  = $library->saveFile($image,'brand');
                $model->file_info = $library->json_encodeArray($image);
                
            }
               $model->save(); 

         
           $subcateryid = Yii::$app->request->post('subcategory');
           if(!empty($subcateryid)){
            foreach ($subcateryid as $key => $value) {
           $brandcategory = new BrandCategory();
           $brandcategory->brand_id = $model->brand_id;
           $brandcategory->category_id = $value;
           $brandcategory->save(); 
            } } 

  

             $brand_names = $modelvalue['brand_names']; 
              BrandDescription::deleteAll(['brand_id'=>$model->brand_id]);

                foreach($brand_names as $language_id=>$name){
               
                    $bdesc = new BrandDescription();
                    $bdesc->brand_id     = $model->brand_id;
                    $bdesc->language_id    = $language_id;
                    $bdesc->brand_name   = $name;
                    $bdesc->save(); 
                }

           return $this->redirect(['index']);
           // return $this->redirect(['view', 'id' => $model->brand_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    } 

    /**
     * Updates an existing Brand model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_image = $model->image;
        
        $descriptions = BrandDescription::findAll(['brand_id'=>$id]);
        $brandname = $descriptions[0]['brand_name'];

  $brandid=$id; 
  $categorylistsql ="SELECT tbl_brand_category.category_id,tbl_category.name FROM `tbl_brand_category` LEFT JOIN tbl_category on tbl_brand_category.category_id = tbl_category.category_id WHERE tbl_brand_category.brand_id=".$brandid;
        $categorylist = Yii::$app->db->createCommand($categorylistsql)->queryAll();

        if ($model->load(Yii::$app->request->post())) {


               $image = UploadedFile::getInstance($model, 'image');
            if(!empty($image)){
                $library = new Library();
                $model->image = $library->saveFile($image,'brand');
                $model->file_info  = $library->json_encodeArray($image);

            }else
                $model->image = $old_image;



         $modelvalue = Yii::$app->request->post('Brand');
         $model->save();


         $brandcategory = BrandCategory::deleteAll(['brand_id'=>$model->brand_id]);
         $subcateryid = Yii::$app->request->post('subcategory');
        
          if(!empty($subcateryid)){

          foreach ($subcateryid as $key => $value) {
           $brandcategory = new BrandCategory();
           $brandcategory->brand_id = $model->brand_id;
           $brandcategory->category_id = $value;
           $brandcategory->save(); 
            } 

        }
            

             $brand_names = $modelvalue['brand_names']; 
          
              BrandDescription::deleteAll(['brand_id'=>$model->brand_id]); 

                foreach($brand_names as $language_id=>$name){

                    $b_desc = new BrandDescription();
                    $b_desc->brand_id     = $model->brand_id;
                    $b_desc->language_id    = $language_id;
                    $b_desc->brand_name   = $name;
                    $b_desc->save(false); 
                }

      

         return $this->redirect(['view', 'id' => $model->brand_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'categorylist' => $categorylist,
            'brandname' => $brandname, 
        ]);
    }

    /**
     * Deletes an existing Brand model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Brand model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Brand the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Brand::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }  


      public function actionGetcategory()
    {
        $id  = Yii::$app->request->post('id');

          $course = Category::find()->select('category_id,name')->where(['cat_id' => $id])->asArray()->all();
     
        return json_encode($course);
      
    }
}
