<?php

namespace backend\controllers;

use Yii;

use common\models\User;
use common\models\Library;
use common\models\UserRole;
use common\models\UserData;
use common\models\UserSearch;
use common\models\ContactMessage;
use common\models\DriverCompanyNumbers;
use common\models\DriverCompanyNumbersSearch;
use common\models\UserBankAccount;
use common\models\Wallet;
use common\models\Order;
use common\models\OrderSearch;

use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\DriverDocuments;
use yii\data\ActiveDataProvider;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','code','update','deleteimage','user','supplier','userimages','supplierimages','acceptimages','admin','admincreate','adminupdate','usercreate','userupdate','drivers','driverupdate','sales-person','saleview','driver','driverview','bank-details','driverimages','delete','nationalid','nationalpicaccept'],
						'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all Driver models.
     * @return mixed
     */
    public function actionDriver()
    {
        $accept_doc = $_GET['accept_doc'] ?? 0;
        if($accept_doc==true){
            DriverDocuments::updateAll(['status' => 1]);
            Yii::$app->session->setFlash('success', 'Documents has been approved successfully.');
            return $this->redirect(['/user/driver']);
        }
        $searchModel = new UserSearch();
        $searchModel->is_driver = 'yes';
        //$searchModel->is_delete = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('driver', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
		$model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
			$pass_hash = $model->password;
			$model->setPassword($pass_hash);
			$model->generateAuthKey();
			$model->email_verify = 1;
			$model->role =2;
			$profile_pic 		= UploadedFile::getInstance($model, 'profile_pic');
			if(!empty($profile_pic)){
				$library    = new Library();
				$model->profile_pic  = $library->saveFile($profile_pic,'users');
				$model->image_approval = 1;
			}
			if($model->save()){
				$url = 'index';
				$role = $model->userRole->name;
				if($role!='Admin')
					$url = strtolower($role);
				Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
				return $this->redirect([$url]);
			}
			else{
				$model->password_hash = $pass_hash;
			}
				
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$pp		= $model->profile_pic;
        if ($model->load(Yii::$app->request->post())) {
			if(!empty($model->password)){
				$pass_hash = $model->password;
				$model->setPassword($pass_hash);
				$model->removePasswordResetToken();
			}
			
			$profile_pic 		= UploadedFile::getInstance($model, 'profile_pic');
			if(!empty($profile_pic)){
				$library    = new Library();
				$model->profile_pic  = $library->saveFile($profile_pic,'users');
				$model->image_approval = 1;
			}else
				$model->profile_pic  = $pp;
			
			if($model->save()){
				$url = 'index';
				$role = $model->userRole->name;
				if($role!='Admin')
					$url = strtolower($role);
				Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
				return $this->redirect([$url]);
			}
				
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
	public function actionCode($role){
		$json		= [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$role_info	= UserRole::findOne($role);
		if(!empty($role_info)){
			$model		= new User();
			$prefix		= strtolower($role_info->name[0]);
			$code		= $model->genrateCode($prefix);
			$json['code'] = $code;
		}
		return $json;
	}
	public function actionDeleteimage($id){
		$fullurl 			= Yii::$app->basePath . '/frontend/web/uploads/';
		$absolute_url 		= str_replace('backend/','',$fullurl);
		$model 				= $this->findModel($id);
		unlink($absolute_url.$model->profile_pic);
		$model->profile_pic	='';
		$model->image_approval = 0;
		$model->save();
		$json['result']= 'done';
		echo json_encode($json['result']);
	}
	
	/**
     * Lists all Users accounts.
     * @return mixed
     */
    public function actionUser()
    {
        $searchModel = new UserSearch();
		$searchModel->is_user = 'yes';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('user/_users', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	/**
     * Lists all Suppliers accounts.
     * @return mixed
     */
    public function actionSupplier()
    {
        $searchModel = new UserSearch();
		$searchModel->is_supplier = 'yes';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('_suppliers', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Your record has been deleted successfully.');
        return $this->redirect(Yii::$app->request->referrer ?: $this->redirect(['index']));

    }
	
	/**
     * Lists all Users pending images approval.
     * @return mixed
     */
    public function actionUserimages()
    {
        $searchModel = new UserSearch();
		$searchModel->is_user = 'yes';
		
        $dataProvider = $searchModel->imageSearch(Yii::$app->request->queryParams);
		$c_model		= new ContactMessage();
		if ($c_model->load(Yii::$app->request->post()) && $c_model->validate()) {
			if($c_model->send_message==1){
				$user_name	= User::findOne($c_model->user_id);
				if(!empty($user_name))
					$c_model->message	= str_replace("[user_name]",$user_name->name,$c_model->message);
				Yii::$app->mailer->compose()
				->setHtmlBody(nl2br($c_model->message))
				->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
				->setTo($c_model->email)
				->setSubject($c_model->subject)
				->send();
				Yii::$app->session->setFlash('success', 'Your message has been sent successfully.');
			}else
				Yii::$app->session->setFlash('success', 'Image has been deleted successfully.');
			$fullurl 			= Yii::$app->basePath . '/frontend/web/uploads/';
			$absolute_url 		= str_replace('backend/','',$fullurl);
			
			unlink($absolute_url.$user_name->profile_pic);
			$user_name->image_approval = 0;
			$user_name->profile_pic = '';
			$user_name->save();
			return $this->refresh();
		}
        return $this->render('_images', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'c_model' => $c_model,
            'title' =>'User Profile Image Approval ',
            'url_format' =>Yii::$app->request->baseUrl.'/user/acceptimages',
			'role' =>1,
        ]);
    }
	
	/**
     * Lists all Suppliers pending images approval.
     * @return mixed
     */
    public function actionSupplierimages()
    {
        $searchModel = new UserSearch();
		$searchModel->is_supplier = 'yes';    
		
        $dataProvider = $searchModel->imageSearch(Yii::$app->request->queryParams);
		$c_model		= new ContactMessage();
		if ($c_model->load(Yii::$app->request->post()) && $c_model->validate()) {  
			if($c_model->send_message==1){
				$user_name	= User::findOne($c_model->user_id);
				if(!empty($user_name))
					$c_model->message	= str_replace("[user_name]",$user_name->name,$c_model->message);
				Yii::$app->mailer->compose()
				->setHtmlBody(nl2br($c_model->message))
				->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
				->setTo($c_model->email)
				->setSubject($c_model->subject)
				->send();
				Yii::$app->session->setFlash('success', 'Your message has been sent successfully.');
			}else
				Yii::$app->session->setFlash('success', 'Image has been deleted successfully.');
			$fullurl 			= Yii::$app->basePath . '/frontend/web/uploads/';
			$absolute_url 		= str_replace('backend/','',$fullurl);
			
			unlink($absolute_url.$user_name->profile_pic);
			$user_name->image_approval = 0;
			$user_name->profile_pic = '';
			$user_name->save();
			return $this->refresh();
		}     
        return $this->render('_images', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' =>'Supplier Profile Image Approval ',
			'url_format' =>Yii::$app->request->baseUrl.'/user/acceptimages',
			'role' =>2,
			'c_model' =>$c_model,
        ]);
    }
  
    public function actionNationalid(){

     

        $searchModel = new UserSearch();
        // $searchModel->national_pic_approval = '1';     
           
        // $dataProvider = $searchModel->imageSearch(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->nationPic(Yii::$app->request->queryParams);
                 
        // echo "<pre>";
        // print_r($dataProvider);die; 
        $c_model        = new ContactMessage();

        if ($c_model->load(Yii::$app->request->post()) && $c_model->validate()) {
            // if($c_model->send_message==1){
            //     $user_name  = User::findOne($c_model->user_id);
            //     if(!empty($user_name))
            //         $c_model->message   = str_replace("[user_name]",$user_name->name,$c_model->message);
            //     Yii::$app->mailer->compose()
            //     ->setHtmlBody(nl2br($c_model->message))
            //     ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            //     ->setTo($c_model->email)
            //     ->setSubject($c_model->subject)
            //     ->send();
            //     Yii::$app->session->setFlash('success', 'Your message has been sent successfully.');
            // }else
            //     Yii::$app->session->setFlash('success', 'Image has been deleted successfully.');
            // $fullurl            = Yii::$app->basePath . '/frontend/web/uploads/';
            // $absolute_url       = str_replace('backend/','',$fullurl);
            
            // unlink($absolute_url.$user_name->national_pic);
            // $user_name->national_pic_approval = 0;
            // $user_name->national_pic = '';
            // $user_name->save();
            // return $this->refresh();
        }    
        
        return $this->render('national_id', [   
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,  
            'title' =>'National-Id Image Approval ',
            'url_format' =>Yii::$app->request->baseUrl.'/user/nationalpicaccept',
            'role' =>2,
            'c_model' =>$c_model,
        ]);
  

    }

    public function actionDriverimages()
    {
        $searchModel = new UserSearch();
        $searchModel->is_driver = 'yes';

        $dataProvider = $searchModel->imageSearch(Yii::$app->request->queryParams);
        $c_model		= new ContactMessage();
        if ($c_model->load(Yii::$app->request->post()) && $c_model->validate()) {
            if($c_model->send_message==1){
                $user_name	= User::findOne($c_model->user_id);
                if(!empty($user_name))
                    $c_model->message	= str_replace("[user_name]",$user_name->name,$c_model->message);
                Yii::$app->mailer->compose()
                    ->setHtmlBody(nl2br($c_model->message))
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                    ->setTo($c_model->email)
                    ->setSubject($c_model->subject)
                    ->send();
                Yii::$app->session->setFlash('success', 'Your message has been sent successfully.');
            }else
                Yii::$app->session->setFlash('success', 'Image has been deleted successfully.');
            $fullurl 			= Yii::$app->basePath . '/frontend/web/uploads/';
            $absolute_url 		= str_replace('backend/','',$fullurl);

            unlink($absolute_url.$user_name->profile_pic);
            $user_name->image_approval = 0;
            $user_name->profile_pic = '';
            $user_name->save();
            return $this->refresh();
        }
        return $this->render('_d_images', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' =>'Driver Profile Image Approval ',
            'url_format' =>Yii::$app->request->baseUrl.'/user/acceptimages',
            'role' =>3,
            'c_model' =>$c_model,

        ]);
    }
	/**
     * Accept Suppliers/Users pending images.
     */
	public function actionAcceptimages($id,$role){
		if($role==1){
			if(!empty($id)){
				if($id=="all"){
					User::updateAll([
						'image_approval' => 1,
					], ['and',['is_user'=>'yes'],['!=','profile_pic','']]);
				}else{
					User::updateAll([
					'image_approval' => 1,
					], ['and',['is_user'=>'yes'],['IN','id',explode(',',$id)],['!=','profile_pic','']]);
				}
				
				Yii::$app->session->setFlash('success', 'Your records has been updated successfully.');
			}
		}elseif($role==2){
			if(!empty($id)){
				if($id=="all"){
					User::updateAll([
						'image_approval' => 1,
					], ['and',['is_supplier'=>'yes'],['!=','profile_pic','']]);
				}else{
					User::updateAll([
					'image_approval' => 1,
					], ['and',['is_supplier'=>'yes'],['IN','id',explode(',',$id)],['!=','profile_pic','']]);
				}
				
				Yii::$app->session->setFlash('success', 'Your records has been updated successfully.');
			}
		}elseif($role==3){
            if(!empty($id)){
                if($id=="all"){
                    User::updateAll([
                        'image_approval' => 1,
                    ], ['and',['is_driver'=>'yes'],['!=','profile_pic','']]);
                }else{
                    User::updateAll([
                        'image_approval' => 1,
                    ], ['and',['is_driver'=>'yes'],['IN','id',explode(',',$id)],['!=','profile_pic','']]);
                }

                Yii::$app->session->setFlash('success', 'Your records has been updated successfully.');
            }
        }
		
		
		return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
	}

    public function actionAdmin()
    {
        $searchModel = new UserSearch();
        $searchModel->role = 3;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin/_admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	public function actionAdmincreate(){
        $model = new User();
        $model->scenario = 'admin_create';
        if ($model->load(Yii::$app->request->post())) {
            $pass_hash = $model->password;
            $model->setPassword($pass_hash);
            $model->generateAuthKey();
            $model->email_verify = 1;
            $model->availability = 'online';
            $model->role = 3;
            if($model->save()){
                $url = 'index';
                Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
                return $this->redirect(['admin']);
            }
        }

        return $this->render('admin/create', [
            'model' => $model,
        ]);
    }

    public function actionAdminupdate($id){
        $model = $this->findModel($id);
        $model->scenario = 'admin_update';
        if ($model->load(Yii::$app->request->post())) {
            if(!empty($model->password)){
                $pass_hash = $model->password;
                $model->setPassword($pass_hash);
            }
            if($model->save()){
                $url = 'index';
                Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
                return $this->redirect(['admin']);
            }
        }

        return $this->render('admin/update', [
            'model' => $model,
        ]);
    }

    public function actionUsercreate(){
        $model = new User();
        $user_data = new UserData();
        $model->scenario = 'user_create';
        $user_data->scenario = 'user_create';
        if ($model->load(Yii::$app->request->post()) && $user_data->load(Yii::$app->request->post())) {
            $pass_hash = $model->password;
            $model->setPassword($pass_hash);
            $model->generateAuthKey();
            $model->email_verify = 1;
            $model->availability = 'offline';
            $model->is_user = 'yes';

            if($model->save()){
                $user_data->user_id = $model->id;
                $profile_pic 		= UploadedFile::getInstance($user_data, 'profile_pic');
                if(!empty($profile_pic)){
                    $library    = new Library();
                    $user_data->profile_pic  = $library->saveFile($profile_pic,'users');
                    $user_data->image_approval = 1;
                }
                $user_data->save();
                Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
                return $this->redirect(['/user/user']);
            }
        }

        return $this->render('user/create', [
            'model' => $model,
            'user_data' => $user_data,
        ]);
    }
    public function actionUserupdate($id){
        $model = $this->findModel($id);
        $user_data =  UserData::find()->where(['user_id'=>$id])->one();
        $pp= '';
        if(empty($user_data))
            $user_data = new UserData();
        else
            $pp		= $user_data->profile_pic;
        $model->scenario = 'user_update';
        $user_data->scenario = 'user_create';
        if ($model->load(Yii::$app->request->post()) && $user_data->load(Yii::$app->request->post())) {
            $pass_hash = $model->password;
            $model->setPassword($pass_hash);
            $model->generateAuthKey();
            $model->email_verify = 1;
            $model->availability = 'offline';
            $model->is_user = 'yes';

            if($model->save()){
                $user_data->user_id = $model->id;
                $profile_pic 		= UploadedFile::getInstance($user_data, 'profile_pic');
                if(!empty($profile_pic)){
                    $library    = new Library();
                    $user_data->profile_pic  = $library->saveFile($profile_pic,'users');
                    $user_data->image_approval = 1;
                }else
                    $user_data->profile_pic = $pp;
                $user_data->save();
                Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
                return $this->redirect(['/user/user']);
            }
        }

        return $this->render('user/create', [
            'model' => $model,
            'user_data' => $user_data,
        ]);
    }
    public function actionDrivers(){
        $searchModel = new DriverCompanyNumbersSearch();

        $dataProvider = $searchModel->drivers(Yii::$app->request->queryParams);

        return $this->render('//user/drivers/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDriverupdate($id){
        if (($model = DriverCompanyNumbers::findOne($id)) !== null) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
                return $this->redirect(Yii::$app->request->baseUrl.'/user/drivers');
            }
            return $this->render("driver_update",['model'=>$model]);
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));

    }
    public function actionSalesPerson(){
        $searchModel = new UserSearch();

        $dataProvider = $searchModel->supplierSearch(Yii::$app->request->queryParams);

        return $this->render('user/_sales_persons', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionSaleview($id){
        $model = $this->findModel($id);
        return $this->render('user/saleview',['model'=>$model]);
    }

    public function actionDriverview($id){
	    if(isset($_GET['doc_id']) && !empty($_GET['doc_id'])){
            $doc_model = new DriverDocuments();
            $result = $doc_model->find()->where(['id' => $_GET['doc_id'],'user_id' => $id])->one();
            if(!empty($result)){
                $result->status = $_GET['status'];
                if($result->save()){
                    $result->sendApproveEmail;
                    Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
                }

            }
            return $this->redirect(['user/driverview','id'=>$id]);
        }

        $model = $this->findModel($id);
        $wallet_model = new Wallet();
        $query = $wallet_model->find()->where(['user_id' => $id])->orderBy('created_at desc');
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
        ]);

        $searchModel = new OrderSearch();
        $searchModel->driver_id = $id;
        $order_dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('driverview',['searchModel' =>$searchModel,'order_dataProvider'=>$order_dataProvider,'dataProvider' => $dataProvider,'model'=>$model]);
    }
    public function actionBankDetails(){
        $json		= [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_id = $_POST['id'] ?? 0;
        $model = UserBankAccount::find()->where(['user_id' => $user_id])->one();
        $json['html'] = $this->renderAjax('_bank_detail',['model' => $model]);
        return $json;

    }

    // start accept national image

        public function actionNationalpicaccept($id,$role){
           
            
         
                if($id=="all"){
                    User::updateAll([
                        'national_pic_approval' => 2, 
                    ]);  
                }else{
                    User::updateAll([
                    'national_pic_approval' => 2,  
                    ], ['and',['IN','id',explode(',',$id)]]);
                }  
                  
                Yii::$app->session->setFlash('success', 'Your records has been updated successfully.');
          
       
        
        
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    // end accept national image 





}
