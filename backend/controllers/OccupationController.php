<?php

namespace backend\controllers;

use Yii;
use common\models\Occupation;
use common\models\OccupationSearch;
use common\models\OccupationDescription;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * OccupationController implements the CRUD actions for Occupation model.
 */
class OccupationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
						'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    
                ],
            ],
        ];
    }

    /**
     * Lists all Occupation models.
     * @return mixed
     */
    public function actionIndex($id=null)
    {
		if(!empty($id))
			$model 		= $this->findModel($id);
		else
			$model		= new Occupation();
		
        $searchModel = new OccupationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$occupation_names	= $model->occupation_name;
           if($model->save()){
				OccupationDescription::deleteAll(['occupation_id'=>$model->id]);
				foreach($occupation_names as $language_id=>$name){
					$c_desc					= new OccupationDescription();
					$c_desc->occupation_id	= $model->id;
					$c_desc->language_id	= $language_id;
					$c_desc->occupation_name		= $name;
					$c_desc->save();
				}
				if($id===null)
					Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
				else
					Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
                return $this->redirect(['index']);
			}
        }
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model' => $model,
        ]);
    }
    /**
     * Finds the Occupation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Occupation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Occupation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
