<?php

namespace backend\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Order;
use common\models\OrderSearch;
use yii\data\ActiveDataProvider;
class ReportsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [

                ],
            ],
        ];
    }
    public function actionIndex($type=null)
    {
        if($type==null)
            $type = 'customer_order';
        if (!method_exists($this,$type))
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
            $html =  $this->{$type}();


        return $this->render('index',['html'=>$html]);
    }
    protected function customer_order(){
        $searchModel = new OrderSearch();
        $query = $searchModel->find()->groupBy(['user_id']);
        $dataProvider 	= new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['order_id'=>SORT_DESC]]
        ]);
        return $this->renderAjax('customer_order', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    protected function sales_report(){

        return $this->renderAjax('sales_report');
    }
}