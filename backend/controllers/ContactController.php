<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\ContactMessage;
use common\models\ContactMessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * ContactController implements the CRUD actions for ContactMessage model.
 */
class ContactController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
						'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ContactMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
		$model		= new ContactMessage();
        $searchModel = new ContactMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model->user_id = 1;
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$user_name	= User::findOne($model->user_id);
			if(!empty($user_name))
				$model->message	= str_replace("[user_name]",$user_name->name,$model->message);
			
			Yii::$app->mailer->compose()
			->setHtmlBody(nl2br($model->message))
			->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
			->setTo($model->email)
			->setSubject($model->subject)
			->send();
			
			Yii::$app->session->setFlash('success', 'Your message has been sent successfully.');
			return $this->refresh();
		}
		
		$model->message = "Dear [user_name],\n\n\nNote: Don't reply on this email. To contact us fill out the form on website or on mobile applications.\nThanks,\nTeam ".config_name;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }


    /**
     * Finds the ContactMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContactMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContactMessage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
