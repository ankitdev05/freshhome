<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

use common\models\Library;
use common\models\Cuisine;
use common\models\CuisineSearch;
use common\models\CuisineDescription;
use yii\web\UploadedFile;

class CuisineController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','update','delete-image'],
						'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cuisine models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model		= new Cuisine();
        $searchModel = new CuisineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    public function actionCreate(){
        $model = new Cuisine();
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            $cuisine_names	= $model->cuisine_name;
            if($model->save()){
                $image = UploadedFile::getInstance($model, 'image');
                if(!empty($image)){
                    $library = new Library();
                    $model->image  = $library->saveFile($image,'cuisine');
                    $model->save();
                }

                CuisineDescription::deleteAll(['cuisine_id'=>$model->id]);
                foreach($cuisine_names as $language_id=>$name){
                    $c_desc					= new CuisineDescription();
                    $c_desc->cuisine_id		= $model->id;
                    $c_desc->language_id	= $language_id;
                    $c_desc->cuisine_name	= $name;
                    $c_desc->save();
                }
                Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_image = $model->image;
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            $cuisine_names	= $model->cuisine_name;
            if($model->save()){
                $image = UploadedFile::getInstance($model, 'image');
                if(!empty($image)){
                    $library = new Library();
                    $model->image  = $library->saveFile($image,'cuisine');

                }else  $model->image = $old_image;
                $model->save();
                CuisineDescription::deleteAll(['cuisine_id'=>$model->id]);
                foreach($cuisine_names as $language_id=>$name){
                    $c_desc					= new CuisineDescription();
                    $c_desc->cuisine_id		= $model->id;
                    $c_desc->language_id	= $language_id;
                    $c_desc->cuisine_name	= $name;
                    $c_desc->save();
                }
                Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }
    /**
     * Finds the Cuisine model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cuisine the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cuisine::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDeleteImage($id){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $fullUrl = Yii::getAlias('@frontend').'/web/uploads/';
        unlink($fullUrl.$model->image);
        $model->image = null;
        $model->cuisine_name = 'yes';
        $model->save();
        $json['result']= 'done';
        return $json;
    }
}
