<?php

namespace backend\controllers;

use Yii;
use common\models\Option;
use common\models\OptionValue;
use common\models\OptionSearch;
use common\models\AttributeGroup;
use common\models\OptionValueSearch;
use common\models\OptionDescription;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * OptionController implements the CRUD actions for Option model.
 */
class OptionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','update','view','create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'=>['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all Option models.
     * @return mixed
     * @$attr_id Attribyte ID
     */
    public function actionIndex($attr_id)
    {
        $attribute = $this->findModel1($attr_id);
        $searchModel = new OptionSearch();
        $searchModel->attribute_id = $attr_id;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'attr_id' => $attr_id,
            'attribute' => $attribute,
        ]);
    }

    /**
     * Displays a single Option model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Option model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($attr_id)
    {
        $attribute = $this->findModel1($attr_id);
        $model = new Option();
        $model->attribute_id = $attr_id;
        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
                $option_names = $model->option_name;
                $option_values = $model->schedule;
                foreach($option_names as $language_id=>$option_name){
                    $option_desc = new OptionDescription();
                    $option_desc->option_id = $model->option_id;
                    $option_desc->language_id = $language_id;
                    $option_desc->name = $option_name;
                    $option_desc->save();
                }
                $allow = false;
                if($model->type=='select' || $model->type=='checkbox' || $model->type=='radio')
                    $allow = true;
                if(!empty($option_values) && $allow==true){
                    foreach($option_values as $data){
                        $option_value = new OptionValue();
                        $option_value->option_id = $model->option_id;
                        $option_value->name = $data['option_value'];
                        $option_value->sort_order = (int)$data['sort_order'];
                        $option_value->save();
                    }
                }
                Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
                return $this->redirect(['index','attr_id' => $attr_id]);
            }
        }
        $searchModel = new OptionValueSearch();
        $searchModel->option_value_id = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('create', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'attr_id' => $attr_id,
            'attribute' => $attribute,
        ]);
    }

    /**
     * Updates an existing Option model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id,$attr_id)
    {
        $attribute = $this->findModel1($attr_id);
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
                OptionDescription::deleteAll(['option_id'=>$model->option_id]);
               // OptionValue::deleteAll(['option_id'=>$model->option_id]);

                $option_names = $model->option_name;
                $option_values = $model->schedule;
                foreach($option_names as $language_id=>$option_name){
                    $option_desc = new OptionDescription();
                    $option_desc->option_id = $model->option_id;
                    $option_desc->language_id = $language_id;
                    $option_desc->name = $option_name;
                    $option_desc->save();
                }
                if($model->type=='select' || $model->type=='checkbox' || $model->type=='radio')
                    $allow = true;
                if(!empty($option_values) && $allow==true){
                    foreach($option_values as $data){
                        $option_value = new OptionValue();
                        $option_value->option_id = $model->option_id;
                        $option_value->name = $data['option_value'];
                        $option_value->sort_order = (int)$data['sort_order'];
                        $option_value->save();
                    }
                }
                Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
                return $this->redirect(['index','attr_id' => $attr_id]);
            }
        }
        $searchModel = new OptionValueSearch();
        $searchModel->option_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('update', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'attribute' => $attribute,
            'attr_id' => $attr_id,
        ]);
    }

    /**
     * Deletes an existing Option model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Option model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Option the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Option::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findModel1($id)
    {
        if (($model = AttributeGroup::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
