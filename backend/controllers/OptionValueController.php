<?php

namespace backend\controllers;

use Yii;
use common\models\Option;
use common\models\OptionValue;
use common\models\AttributeGroup;
use common\models\OptionValueSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * OptionValueController implements the CRUD actions for OptionValue model.
 */
class OptionValueController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','update','view','create','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'=>['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all OptionValue models.
     * @param integer $option_id
     * @return mixed
     */
    public function actionIndex($option_id,$attr_id)
    {
        $attribute = $this->findModel2($attr_id);
        $option_model = $this->findModel1($option_id);
        $searchModel = new OptionValueSearch();
        $searchModel->option_id = $option_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'option_model' => $option_model,
            'dataProvider' => $dataProvider,
            'attribute' => $attribute,
            'attr_id' => $attr_id,
        ]);
    }

    /**
     * Displays a single OptionValue model.
     * @param integer $id
     * @param integer $option_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id,$option_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OptionValue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $option_id
     * @return mixed
     */
    public function actionCreate($option_id,$attr_id)
    {
        $attribute = $this->findModel2($attr_id);
        $option_model = $this->findModel1($option_id);
        $model = new OptionValue();
        $model->option_id = $option_id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Your record has been added successfully.');
            return $this->redirect(['index', 'option_id' => $option_id,'attr_id'=>$attr_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'option_model' => $option_model,
            'attribute' => $attribute,
            'attr_id' => $attr_id,
        ]);
    }

    /**
     * Updates an existing OptionValue model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $option_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id,$option_id,$attr_id)
    {
        $attribute = $this->findModel2($attr_id);
        $option_model = $this->findModel1($option_id);
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
            return $this->redirect(['index', 'option_id' => $option_id,'attr_id'=>$attr_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'option_model' => $option_model,
            'attr_id' => $attr_id,
            'attribute' => $attribute,
        ]);
    }

    /**
     * Deletes an existing OptionValue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $attr_id
     * @param integer $option_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id,$option_id,$attr_id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'option_id' => $option_id,'attr_id'=>$attr_id]);
    }

    /**
     * Finds the OptionValue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OptionValue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OptionValue::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findModel1($option_id)
    {
        if (($model = Option::findOne($option_id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    protected function findModel2($id)
    {
        if (($model = AttributeGroup::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
