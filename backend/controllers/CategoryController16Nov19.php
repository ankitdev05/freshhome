<?php

namespace backend\controllers;

use Yii;
use common\models\Library;
use common\models\Category;
use common\models\CategorySearch;
use common\models\MainCategories;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','update','view','delete-image'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex($cat_id)
    {
        $main_cat = $this->findMainCat($cat_id);
        if(isset($_GET['update']) && isset($_GET['id'])){
            $model = $this->findModel($_GET['id']);
            $model->featured = $_GET['update'];
            if($model->save()){
                Yii::$app->session->setFlash('success', 'Your record has been updated successfully.');
                return $this->redirect(['index','cat_id' => $cat_id]);
            }

        }

        $searchModel = new CategorySearch();
        $searchModel->cat_id = $cat_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'main_cat' => $main_cat,
            'cat_id' => $cat_id,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @param integer $cat_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id,$cat_id)
    {
        $main_cat = $this->findMainCat($cat_id);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'main_cat' => $main_cat,
            'cat_id' => $cat_id,
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @param integer $cat_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCreate($cat_id)
    {
        $main_cat = $this->findMainCat($cat_id);
        $model = new Category();
        $model->cat_id = $cat_id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $image = UploadedFile::getInstance($model, 'image');
            if(!empty($image)){
                $library = new Library();
                $model->image  = $library->saveFile($image,'category');
                $model->save();
            }
            $model->saveCategoryPath();
            return $this->redirect(['index','cat_id' => $cat_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'cat_id' => $cat_id,
            'main_cat' => $main_cat
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $cat_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id,$cat_id)
    {
        $main_cat = $this->findMainCat($cat_id);
        $model = $this->findModel($id);
        $model->cat_id = $cat_id;
        $old_image = $model->image;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $image = UploadedFile::getInstance($model, 'image');
            if(!empty($image)){
                $library   			 = new Library();
                $model->image  = $library->saveFile($image,'category');
            }else $model->image = $old_image;

            $model->save();
            $model->updateCategoryPath();
            return $this->redirect(['index', 'cat_id' => $cat_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'cat_id' => $cat_id,
            'main_cat' => $main_cat,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findMainCat($id)
    {
        if (($model = MainCategories::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDeleteImage($id){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $fullUrl = Yii::getAlias('@frontend').'/web/uploads/';
        unlink($fullUrl.$model->image);
        $model->image = '';
        $model->save();
        $json['result']= 'done';
        return $json;
    }
}
