<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Modules;
use common\models\UserRole;
use common\models\UserSearch;
use common\models\User;   
use common\models\MenuSearch;
use common\models\DriverDocuments;
use yii\bootstrap4\Breadcrumbs;
use himiklab\thumbnail\EasyThumbnailImage;

AppAsset::register($this);
$config_logo				= config_logo;
$logo						= config_name;
if(!empty($config_logo)){
	$image		=EasyThumbnailImage::thumbnailFileUrl(
		"../../frontend/web/images/store/".$config_logo,300,55,EasyThumbnailImage::THUMBNAIL_INSET
	);
	$logo	= Html::img($image, ['alt'=>config_name, 'title'=>config_name,'width'=>'150','class'=>'img-fluid']);
}
$this->registerCss('
.fade:not(.show){
	opacity:1;
}
');

$menus	= Modules::find()->where(['and',['IS','parent_id',null],['status'=>1]])->orderBy('sort_order ASC')->all();

$permissions = [];
if(!Yii::$app->user->isGuest){
    $group_id   = Yii::$app->user->identity->role;
    $group_permissions  = unserialize(UserRole::findOne($group_id)->permission);

    foreach($group_permissions as $group_permission){
        $permissions[]= $group_permission;
    }
    $permissions = array_unique($permissions);
}
$controller =  $this->context->id;
$action     = $this->context->action->id;
$nav_id = '0';
if($controller=='plans'){
    if($action=='view' || $action=='update'){
        $nav_id = 7;
    }
}elseif($controller=='countries'){
    $nav_id = 26;
}
elseif($controller=='state'){
    $nav_id = 27;
}
elseif($controller=='city'){
    $nav_id = 28;
}
elseif($controller=='driver-company' || $controller=='driver-company-numbers'){
    $nav_id = 6;
}
elseif($controller=='banners'){
    $nav_id = 20;
}
elseif($controller=='pages'){
    $nav_id = 21;
}
elseif($controller=='occupation'){
    $nav_id = 23;
}
elseif($controller=='videos'){
    $nav_id = 24;
}
elseif($controller=='category'){
    $nav_id = 16;
}
elseif($controller=='attribute-group'){
    $nav_id = 17;
}
elseif($controller=='cuisine'){
    $nav_id = 18;
}
elseif($controller=='meal'){
    $nav_id = 22;
}
elseif($controller=='schedules'){
    $nav_id = 19;
}
elseif($controller=='order'){
    $nav_id = 15;
}
elseif($controller=='supplierimages'){
    $nav_id = 13;
}
elseif($controller=='menu'){
    if($action=='supplierimages')
    $nav_id = 14;
    if($action=='index')
        $nav_id = 10;
}
elseif($controller=='site'){
    if($action=='viewschedule')
        $nav_id = 19;
}
elseif($controller=='user'){
    if($action=='user' || $action=='userupdate' || $action=='usercreate')
        $nav_id = 9;
    if($action=='supplier' || $action=='create' || $action=='update')
        $nav_id = 10;
    if($action=='driver' || $action=='driverview')
        $nav_id = 11;
    if($action=='sales-person' || $action=='saleview')
        $nav_id = 12;
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
<?php 
$class = 'app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show';
	rmrevin\yii\fontawesome\AssetBundle::register($this);
	
	$this->registerCssFile(Yii::$app->request->baseUrl.'/vendors/@coreui/icons/css/coreui-icons.min.css',[
		'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
	]);
	
	$this->registerCssFile(Yii::$app->request->baseUrl.'/vendors/flag-icon-css/css/flag-icon.min.css',[
		'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
	]);
	
	$this->registerCssFile(Yii::$app->request->baseUrl.'/vendors/simple-line-icons/css/simple-line-icons.css',[
		'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
	]);
	
	$this->registerCssFile(Yii::$app->request->baseUrl.'/css/style.css',[
		'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
	]);
	
	$this->registerCssFile(Yii::$app->request->baseUrl.'/vendors/pace-progress/css/pace.min.css',[
		'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
	]);
	$this->registerCssFile(Yii::$app->request->baseUrl.'/css/bootfont.css',[
		'depends' => [\yii\bootstrap4\BootstrapAsset::className()]
	]);
	
	$this->registerJsFile(Yii::$app->request->baseUrl.'/vendors/popper.js/js/popper.min.js');
	$this->registerJsFile(Yii::$app->request->baseUrl.'/vendors/pace-progress/js/pace.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
	$this->registerJsFile(Yii::$app->request->baseUrl.'/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
	$this->registerJsFile(Yii::$app->request->baseUrl.'/vendors/@coreui/coreui-pro/js/coreui.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
	$this->registerJsFile(Yii::$app->request->baseUrl.'/js/jquery.ui.touch-punch.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$searchModel = new UserSearch();
$searchModel->is_supplier = 'yes';

$dataProvider = $searchModel->imageSearch(Yii::$app->request->queryParams);
$count = $dataProvider->getTotalCount();

$searchModel = new UserSearch();
$searchModel->is_driver = 'yes';

$dataProvider = $searchModel->imageSearch(Yii::$app->request->queryParams);
$driver_count = $dataProvider->getTotalCount();

$searchModel = new MenuSearch();
$dataProvider = $searchModel->imageSearch(Yii::$app->request->queryParams);
$count1 = $dataProvider->getTotalCount();

$doc_cnt = DriverDocuments::find()->where(['status' => 0])->groupBy('user_id')->count();


$national_id = User::find()->where(['national_pic_approval'=>1])->count();


$this->registerJs("
    $('#supplier_imgs').html(".$count.")
    $('#mnu_imgs').html(".$count1.")
    $('#mnu_imgs').html(".$count1.")
    $('#drivers_cnt').html(".$doc_cnt.")
    $('#national_id').html(".$national_id.")      
    $('#national_cnt').html(".$national_id.")      
    $('#driver_imgs').html(".$driver_count.")
");
?>	
	<meta name="viewport" content="width=device-width, user-scalable=no" />
</head>
<body class="<?= $class ?>">
<?php
$this->beginBody();
?>
<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="<?= Yii::$app->request->baseUrl ?>/">
      <?= $logo ?>
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
      <span class="navbar-toggler-icon"></span>
    </button>
	<?php if(!Yii::$app->user->isGuest){ ?>
		<ul class="nav navbar-nav d-md-down-none">
		  <li class="nav-item px-3">
			<a class="nav-link" href="<?= Yii::$app->request->baseUrl ?>/"><i class="nav-icon icon-home"></i> Dashboard</a>
		  </li>
		  <li class="nav-item px-3">
			<a class="nav-link" href="<?= Yii::$app->request->baseUrl ?>/user/admin"><i class="nav-icon icon-user"></i> Users</a>
		  </li>
		  <li class="nav-item px-3">
			<a class="nav-link" href="<?= Yii::$app->request->baseUrl ?>/settings/create"><i class="nav-icon icon-settings"></i> Settings</a>
		  </li>
		</ul>
		<ul class="nav navbar-nav ml-auto"></ul>
    <?php } ?>
</header>
<div class="app-body">
	<div class="sidebar">
		<nav class="sidebar-nav">
			<ul class="nav">
				<li class="nav-item">
					<a class="nav-link" href="<?= Yii::$app->request->baseUrl ?>/">
						<i class="nav-icon icon-home"></i> Dashboard
					</a>
				</li>
                <?php if(!Yii::$app->user->isGuest){
                    if(!empty($menus)){
                        
                        foreach($menus as $menu){
                            $submenus = $menu->modules;
                            
                            if(in_array($menu->id,$permissions)) {
                ?>
                <li id="<?= !empty($submenus) ? 'nav-sub_dropdown_'.$menu->id : '' ?>" class="nav-item <?= !empty($submenus) ? 'nav-dropdown' : '' ?>">
                    
                    <?= Html::a(Html::tag('i', '', ['class' => $menu->class_name]) . ' ' . $menu->config_name.' '.$menu->badge_class, Url::to(Yii::$app->request->baseUrl.'/'.$menu->config_value, true), ['id'=>'nav-'.$menu->id,'class' => 'nav-link' . (!empty($submenus) ? ' nav-dropdown-toggle' : '')]) ?>

                    <?php if (!empty($submenus)) {
                        echo '<ul class="nav-dropdown-items" id="sub_nav_'.$menu->id.'">';
                        
                        foreach ($submenus as $submenu) {
                            if($submenu->id == 18 || $submenu->id == 19 || $submenu->id == 22) {

                            } else {
                                echo Html::a(Html::tag('i', '', ['class' => $submenu->class_name]) . ' ' . $submenu->config_name.' '.$submenu->badge_class, Url::to(Yii::$app->request->baseUrl.'/'.$submenu->config_value, true), ['class' => 'nav-link','id'=>'nav-'.$submenu->id,'data-attr' => $menu->id]);
                            }

                        }
                        echo '</ul>';
                    } ?>

                </li>
                <?php
                      }
                        }
                    }
                } ?>
                  
 
                <?php if(!Yii::$app->user->isGuest){ ?>
                    <li class="nav-item"> 
                        <?= Html::a('<i class="nav-icon icon-logout"></i> HomeCategory',Url::to(['home-category/index']),['class'=>'nav-link','data-method' => 'post']) ?>
                    </li>
                <?php } ?>

                
                
                <?php if(!Yii::$app->user->isGuest){ ?>
                    <li class="nav-item">
                        <?= Html::a('<i class="nav-icon icon-logout"></i> Logout',Url::to(['site/logout']),['class'=>'nav-link','data-method' => 'post']) ?>
                    </li>
                <?php } ?>
			</ul>
		</nav>
		 <button class="sidebar-minimizer brand-minimizer" type="button"></button>
	</div>
</div>

<main class="main">
    <div class="container-fluid">
		<div id="ui-view">
			<?= Breadcrumbs::widget([
				'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]) ?>
			<?= \yii2mod\notify\BootstrapNotify::widget() ?>
			<?= $content ?>
		</div>
    </div>
<footer class="footer">
    <div class="container">
        <p class="pull-left"><?= Html::encode(str_replace('[date]',date("Y"),config_footer)) ?></p>
    </div>
</footer>
    <?php

    $this->registerJs('
     $("#nav-'.$nav_id.'").addClass("active");
     var data_attr = $("#nav-'.$nav_id.'").attr("data-attr");
    if (typeof data_attr !== "undefined") {
        $("#nav-sub_dropdown_"+data_attr).addClass("open");
    }
');

    ?>
    <style>
        .sidebar .nav-link.active{
            background:#20a8d8;
        }
        .sidebar .nav-link.active i{
            color:#fff !important;
        }
    </style>
</main>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
