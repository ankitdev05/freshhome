<?php

use yii\helpers\Html;
use yii\bootstrap4\Tabs;

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drivers'), 'url' => ['user/drivers']];
$this->params['breadcrumbs'][] = ['label' => $driver_info->name];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="order-index table-responsive">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    echo Tabs::widget([
        'items' => [
            [
                'label' =>yii::t('yii','Pending Orders'),
                'content' =>'<br/>'.$this->render("//order/views",['dataProvider'=>$pdataProvider,'searchModel'=>$searchModel])
            ],
            [
                'label' =>yii::t('yii','Completed Orders'),
                'content' =>'<br/>'.$this->render("//order/views",['dataProvider'=>$cdataProvider,'searchModel'=>$searchModel])
            ]
        ]
    ]);
    ?>
</div>
<div class="w-100"></div>
<p></p>
