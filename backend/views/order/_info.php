<?php
use yii\widgets\DetailView;
?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'order_id',
        [
            'attribute'=>'user_id',
            'filter'=>false,
            'value'=>function($data){
                if(!empty($data->user->userDetail) && !empty($data->user->userDetail['name']))
                    return $data->user->userDetail['name'];
                return $data->user->name;

            }
        ],
        [
            'attribute'=>'supplier_id',
            'filter'=>false,
            'value'=>function($data){
                return $data->supplier->name;
            }
        ],
        [
            'attribute'=>'total',
            'value'=>function($data){
                return $data->currency_code.' '.Yii::$app->formatter->format($data->total,['decimal',2]);
            }
        ],
        'ip',
        [
            'attribute'=>'total',
            'value'=>function($data){
                return $data->currency_code.' '.Yii::$app->formatter->format($data->total,['decimal',2]);
            }
        ],
        [
            'attribute'=>'order_status_id',
            'value'=>function($data){
                return $data->orderStatus->name;
            }
        ],
        'payment_method',
        'created_at:datetime',
    ],
]) ?>