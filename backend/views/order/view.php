<?php

use yii\helpers\Html;
use yii\bootstrap4\Tabs;

$this->title = 'Order #'.$model->order_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>
<?=
Tabs::widget([
    'items' => [
        [
            'label' =>yii::t('yii','Order Info'),
            'content' =>'<br/>'.$this->render('_info',['model'=>$model])
        ],
        [
            'label' =>yii::t('yii','Order Products'),
            'content' =>'<br/>'.$this->render('_order_products',['model'=>$model])
        ],
        [
            'label' =>yii::t('yii','Order Total'),
            'content' =>'<br/>'.$this->render('_order_total',['model'=>$model])
        ]
    ]
]);
?>


</div>
