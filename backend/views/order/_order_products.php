<?php
$order_products = $model->orderProducts;
?>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($order_products as $order_product){ ?>
            <tr>
                <td><?= $order_product->name ?></td>
                <td><?= $order_product->quantity ?></td>
                <td><?= $model->currency_code.' '.Yii::$app->formatter->format($order_product->price,['decimal',2]) ?></td>
                <td><?= $model->currency_code.' '.Yii::$app->formatter->format($order_product->price*$order_product->quantity,['decimal',2]) ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
