<?php
$order_totals = $model->orderTotals;
?>
<div class="row">
    <?php foreach($order_totals as $order_total){ ?>
        <div class="col-sm-2">
            <strong><?= $order_total->title ?></strong>
        </div>
        <div class="col-sm-6">
            <?=  $model->currency_code.' '.Yii::$app->formatter->format($order_total->value,['decimal',2]) ?>
        </div>
        <div class="w-100"></div>
    <?php } ?>
</div>
