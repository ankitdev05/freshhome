<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="order-index table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render("//order/views",['dataProvider'=>$dataProvider,'searchModel'=>$searchModel]) ?>
</div>
