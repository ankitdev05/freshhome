<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContactMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contact Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-message-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php Pjax::begin(); ?>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           
			[
				'attribute'=>'user_id',
				'value'=>function($data){
					return !empty($data->user_id) ? $data->user->name : '';
				},
				'filter'=>false
			],
            'subject',
            
            'email:email',
			'message:ntext',
			[
				'attribute'=>'role',
				'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'role',
                    'data' => ['User'=>'User','Supplier'=>'Supplier'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
				
			],
			[
				'attribute'=>'created_at',
				'format'=>'datetime',
				'filter'=>false
			],
            
            //'updated_at',

            [
				'class' => 'yii\grid\FaActionColumn',
				'template'=>'{send_email}',
				'buttons'=>[
					'send_email'=>function($url, $model, $key){
						return Html::a(Html::tag('i','',['class'=>'far fa-envelope']),'javascript:void(0)', ['data-pjax' => 0,'alt'=>'Reply','title'=>'Reply','class'=>'btn btn-danger send_reply','data-toggle'=>'modal','data-target'=>'#reply-user','u-email'=>$model->email,'u-subject'=>$model->subject,'u-user_id'=>$model->user_id]);
					}
				]
			],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>
</div>	
    <?php Pjax::end(); ?>
</div>
<div id="reply-user" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h3>Send message to <span id="email-reply"></span></h3>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<?php $form = ActiveForm::begin(); ?>
					<?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>
					<?= $form->field($model, 'role')->hiddenInput()->label(false) ?>
					<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
					<?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
					<?= $form->field($model, 'message')->textArea(['rows' => 4])->hint('[user_name] will auto populate user\'s name.') ?>
					<?= Html::submitButton(Yii::t('app', 'Send') , ['class' => 'btn btn-warning']) ?>
				<?php ActiveForm::end(); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php
$this->registerJs('
	$(".send_reply").on("click",function(){
		var email = $(this).attr("u-email");
		var subject = $(this).attr("u-subject");
		var user_id = $(this).attr("u-user_id");
		$("#email-reply").html(email);
		$("#contactmessage-email").val(email);
		$("#contactmessage-role").val("admin");
		$("#contactmessage-user_id").val(user_id);
		$("#contactmessage-subject").val("Re: "+subject);
		
	})
');