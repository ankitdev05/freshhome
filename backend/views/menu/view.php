<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use himiklab\thumbnail\EasyThumbnailImage;
/* @var $this yii\web\View */
/* @var $model common\models\Menu */

$this->title = $model->dish_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $user_info->name.'\'s Menus'), 'url' => ['index','id'=>$model->supplier_id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id,'s_id'=>$model->supplier_id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dish_name',
            'dish_description:ntext',
			[
				'attribute'=>'dish_image',
				'format'=>'raw',
				'value'=>function($model){
					if(empty($model->dish_image))
						return null;
					
					$image = EasyThumbnailImage::thumbnailFileUrl(
						"../../frontend/web/uploads/".$model->dish_image,100,100,EasyThumbnailImage::THUMBNAIL_INSET
					);
					
					return Html::img($image, ['class'=>'file-preview-image', 'alt'=>$model->dish_name, 'title'=>$model->dish_name]);
				}
			],
            'dish_price',
            'dish_weight',
            'dish_since',
			[
				'attribute'=>'city_id',
				'value'=>function($model){
					return !empty($model->city) ? $model->city->city_name : null; 
				}
			],
            'status',
            'dist_qty',
            'dish_serve',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
