<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */

$this->title = Yii::t('app', 'Update Menu: {name}', [
    'name' => $model->dish_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $user_info->name.'\'s Menus'), 'url' => ['index','id'=>$user_info->id]];
$this->params['breadcrumbs'][] = ['label' => $model->dish_name, 'url' => ['view', 'id' => $model->id,'s_id'=>$model->supplier_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="menu-update">
    <?= $this->render('_form', [
        'model' => $model,
        'user_info' => $user_info,
    ]) ?>

</div>
