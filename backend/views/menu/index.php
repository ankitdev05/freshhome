<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use himiklab\thumbnail\EasyThumbnailImage;
use kartik\select2\Select2;

$this->title = Yii::t('app', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    <h5>Supplier - <?= Html::encode($user_info->name) ?> (<?= $user_info->share_id ?>)</h5>
	<hr/>
	<p>
        <?= Html::a(Yii::t('app', 'Create Menu'), ['create','s_id'=>$user_info->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
               'attribute' => 'id',
               'label' => 'Product ID',
               'value' => function($data){
                    return 'P-'.$data->id;
               }
            ],
            'dish_name',
			[
				'attribute'=>'dish_image',
				'filter'=>false,
				'format'=>'raw',
				'value'=>function($model){
					if(empty($model->dish_image))
						return null;
					$image = EasyThumbnailImage::thumbnailFileUrl(
						"../../frontend/web/uploads/".$model->dish_image,100,100,EasyThumbnailImage::THUMBNAIL_INSET
					);
					
					return Html::img($image, ['class'=>'file-preview-image', 'alt'=>$model->dish_name, 'title'=>$model->dish_name]);
				}
			],
            //'image_approval',
            'dish_price',
			'dist_qty',
            //'dish_weight',
            //'dish_since',
            //'city_id',
            [
				'attribute'=>'status',
				'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => ['active'=>'Active','inactive'=>'In active'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
					'pluginOptions' => [
                        'allowClear' => true
                    ],
                    
                ])
			],
            
            //'dish_serve',
            //'created_at',
            //'updated_at',

            [
				'class' => 'yii\grid\FaActionColumn',
				'template'=>'{view} {update}',
				'buttons'=>[
					'update'=>function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'far fa-edit']), ['menu/update','id' => $model->id,'s_id'=>$model->supplier_id], ['data-pjax' => 0,'class'=>'btn btn-secondary']);
                    },
					'view'=>function($url, $model, $key){
						return Html::a(Html::tag('i','',['class'=>'far fa-eye']), ['menu/view','id' => $model->id,'s_id'=>$model->supplier_id], ['data-pjax' => 0,'class'=>'btn btn-secondary']);
					}
				]
			],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>
</div>	
    <?php Pjax::end(); ?>
</div>
