<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use himiklab\thumbnail\EasyThumbnailImage;
$this->title = Yii::t('app', $title);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">
	<div class="container">
		<div class="row">
			<?= Html::a(Html::tag('i',' Accept All pending images',['class'=>'fas fa-check']),$url_format.'?id=all',['class'=>'btn btn-warning btn-sm float-right mr-3','data-confirm'=>'Are you sure want to accept all pending images?']) ?>
			<?= Html::a(Html::tag('i',' Accept selected images',['class'=>'fas fa-check']),'javascript:void(0)',['class'=>'btn btn-primary btn-sm float-right acc_sel']) ?>
			
		</div>
	</div>
	<p class="w-100"></p>	
	<hr/>
	<?php Pjax::begin(); ?>
<div class="table-responsive">	
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\CheckboxColumn'],
            ['class' => 'yii\grid\SerialColumn'],
			'dish_name',
			'supplier_id',
			[
				'attribute'=>'dish_image',
				'filter'=>false,
				'format'=>'raw',
				'value'=>function($model){
					$image = EasyThumbnailImage::thumbnailFileUrl(
						"../../frontend/web/uploads/".$model->dish_image,100,100,EasyThumbnailImage::THUMBNAIL_INSET
					);
					$Limage = EasyThumbnailImage::thumbnailFileUrl(
						"../../frontend/web/uploads/".$model->dish_image,400,400,EasyThumbnailImage::THUMBNAIL_INSET
					);
					return Html::a(Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Profile Picture', 'title'=>'Profile Picture']),'javascript:void(0)',['class'=>'popImg','data-src'=>$Limage]);
				}
			],
			[
				'class' => 'yii\grid\FaActionColumn',
				'template'=>'{approve} {reject}',
				'buttons'=>[
					'approve'=>function($url, $model, $key) use ($url_format){
						return Html::a(Html::tag('button',Html::tag('i',' Accept ',['class'=>'fas fa-check']),['class'=>'btn btn-sm btn-primary']), $url_format.'?id='.$model->id, ['data-pjax' => 0,'data-confirm'=>'Are you sure want to accept this image?']);
					},
                    'reject'=>function($url, $model, $key) use ($reject_url){
	                    $supplier_info = $model->supplierInfo;
                        return Html::a(Html::tag('button',Html::tag('i',' Reject ',['class'=>'fas fa-times']),['class'=>'btn btn-sm btn-danger']), 'javascript:void(0)', ['class'=>'send_reply','data-pjax' => 0,'data-toggle'=>'modal','data-target'=>'#reply-user','u-email'=>$supplier_info->email,'u-subject'=>'Your image has been rejected | '.config_name,'u-mid'=>$model->id,'u-user_id'=>$supplier_info->id,"u-msg"=>"Dear [user_name],\nYour image has been rejected for your menu item '".$model->dish_name."' \n\nThanks,\nTeam ".config_name]);
                    }
				]	
			],
		],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
	
	]); ?>
</div>	
	<?php Pjax::end(); ?>
</div>
    <div id="reply-user" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Send message to <span id="email-reply"></span></h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($contact_model, 'send_message')->hiddenInput()->label(false) ?>
                    <?= $form->field($contact_model, 'user_id')->hiddenInput()->label(false) ?>
                    <?= $form->field($contact_model, 'role')->hiddenInput()->label(false) ?>
                    <?= $form->field($contact_model, 'email')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($contact_model, 'subject')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($contact_model, 'message')->textArea(['rows' => 4])->hint('[user_name] will auto populate user\'s name.') ?>
                    <?= Html::submitButton(Yii::t('app', 'Send') , ['class' => 'btn btn-warning']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-body text-center">
        <img src="" id="imagepreview" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php
$this->registerJs('
	$(\'.popImg\').on(\'click\',function(){
		$(\'#imagepreview\').attr(\'src\', $(this).attr(\'data-src\')); 
		 $(\'#imagemodal\').modal(\'show\');
	});
	$(\'.acc_sel\').on(\'click\',function(){
		var sel_val = [];
		$.each($("input[name=\'selection[]\']:checked"), function(){
			sel_val.push($(this).val());
		});
		var ids = sel_val.join(",");
		if(ids==""){
			alert("Please select the images");
			return false;
		}
		if(ids!="" && confirm("Are you sure want to accept selected images?"))
			window.location.href = "'.$url_format.'?id="+ids;
	})
');
$this->registerJs('
	$(".send_reply").on("click",function(){
		var email = $(this).attr("u-email");
		var subject = $(this).attr("u-subject");
		var user_id = $(this).attr("u-user_id");
		var mid = $(this).attr("u-mid");
		var msg = $(this).attr("u-msg");
		$("#email-reply").html(email);
		$("#contactmessage-email").val(email);
		$("#contactmessage-role").val("admin");
		$("#contactmessage-user_id").val(user_id);
		$("#contactmessage-subject").val(subject);
		$("#contactmessage-message").val(msg);
		$("#contactmessage-send_message").val(mid);
		
	})
');