<?php

use yii\helpers\Html;
use yii\bootstrap4\Tabs;
use yii\bootstrap4\ActiveForm;

?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	<?=
		Tabs::widget([
			'items' => [
				[
					'label'=>yii::t('yii','General'),
					'content' =>'<br/>'.$this->render("_general",['form'=>$form,'model'=>$model,'user_info'=>$user_info])
				],
				[
					'label'=>yii::t('yii','Other information'),
					'content' =>'<br/>'.$this->render("_other",['form'=>$form,'model'=>$model,'user_info'=>$user_info])
				]
			]
		]);
	?>
	

    <div class="form-group mt-3">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
