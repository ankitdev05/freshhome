<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use himiklab\thumbnail\EasyThumbnailImage;
$this->title = Yii::t('app', $title);
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
  img{
    margin-left: 5px; 
  }
</style>
<div class="menu-index">
	<div class="container">
		<div class="row">
			<?= Html::a(Html::tag('i',' Accept All pending images',['class'=>'fas fa-check']),$url_format.'?id=all',['class'=>'btn btn-warning btn-sm float-right mr-3','data-confirm'=>'Are you sure want to accept all pending images?']) ?>
			<?= Html::a(Html::tag('i',' Accept selected images',['class'=>'fas fa-check']),'javascript:void(0)',['class'=>'btn btn-primary btn-sm float-right acc_sel']) ?>
			
		</div>
	</div>
	<p class="w-100"></p>	
	<hr/>
	<?php Pjax::begin(); ?>
<div class="table-responsive">	
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\CheckboxColumn'],
            ['class' => 'yii\grid\SerialColumn'],
			'dish_name',
			'supplier_id',
			[
				'attribute'=>'dish_image',
				'filter'=>false,
				'format'=>'raw',
				'value'=>function($model){
                 // return $model->dish_image;   
					$image = EasyThumbnailImage::thumbnailFileUrl(
						"../../frontend/web/uploads/".$model->dish_image,100,100,EasyThumbnailImage::THUMBNAIL_INSET
					);
					$Limage = EasyThumbnailImage::thumbnailFileUrl(
						"../../frontend/web/uploads/".$model->dish_image,400,400,EasyThumbnailImage::THUMBNAIL_INSET
					);
					return Html::a(Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Profile Picture', 'title'=>'Profile Picture']),'javascript:void(0)',['class'=>'popImg','data-src'=>$Limage]);
			 }
			],
			[ 
				'class' => 'yii\grid\ActionColumn',
				// 'class' => 'yii\grid\FaActionColumn',
				'template'=>'{approve} {reject} {viewalldetails}',
				'buttons'=>[
					'approve'=>function($url, $model, $key) use ($url_format){
						return Html::a(Html::tag('button',Html::tag('i',' Accept ',['class'=>'fas fa-check']),['class'=>'btn btn-sm btn-primary']), $url_format.'?id='.$model->id, ['data-pjax' => 0,'data-confirm'=>'Are you sure want to accept this image?']);
					},
                    'reject'=>function($url, $model, $key) use ($reject_url){
	                    $supplier_info = $model->supplierInfo;
                        return Html::a(Html::tag('button',Html::tag('i',' Reject ',['class'=>'fas fa-times']),['class'=>'btn btn-sm btn-danger']), 'javascript:void(0)', ['class'=>'send_reply','data-pjax' => 0,'data-toggle'=>'modal','data-target'=>'#reply-user','u-email'=>$supplier_info->email,'u-subject'=>'Your image has been rejected | '.config_name,'u-mid'=>$model->id,'u-user_id'=>$supplier_info->id,"u-msg"=>"Dear [user_name],\nYour image has been rejected for your menu item '".$model->dish_name."' \n\nThanks,\nTeam ".config_name]);
                    },   
                    'viewalldetails'=>function($url, $model, $key) use ($reject_url){
                    	// echo "<pre>";
                    	// print_r($model);die;
	                    $supplier_info = $model->supplierInfo;    
                        return Html::a(Html::tag('button',Html::tag('i',' View All Details ',['class'=>'fas fa-eye']),['class'=>'btn btn-sm btn-success','onclick'=>"openmode('".$model->id."')"]), 'javascript:void(0)', ['class'=>'send_reply','data-pjax' => 0,'data-toggle'=>'modal']);
                    }    
				]	
			],
		],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
	
	]); ?>
</div>	
	<?php Pjax::end(); ?>
</div>
<script type="text/javascript">
	function openmode(id){
	
	      $.ajax({ 
            type: "POST",
            url: "<?= Yii::$app->urlManager->createUrl('menu/getproductdetails'); ?>",
            data: {id: id}, 
            dataType: 'html',
            cache: false,
            success: function (data) {    
            	 var viewalldetails = JSON.parse(data);    
               console.log(viewalldetails,'viewalldetails ');
               $("#placehere").text('');   
               $("#placehere1").text(''); 
               $("#categoryname").text('');     
               $("#dish_name").text(viewalldetails['details'].dish_name);
               $("#dish_price").text(viewalldetails['details'].dish_price);
               $("#collected_amount").text(viewalldetails['details'].collected_amount);
               $("#discount").text(viewalldetails['details'].discount);
               $("#dish_description").text(viewalldetails['details'].dish_description);
               $("#homecategoryname").text(viewalldetails.home_category_name);
  
               $("#attributename").text(viewalldetails.atributname);
  
              var i;
               for (i = 0; i < viewalldetails['category_names'].length; i++) {
                console.log(viewalldetails['category_names'][i].category_name,'categoryname');
               $("#categoryname").append(viewalldetails['category_names'][i].category_name+' / ');

                }  

              
                          
                   if(viewalldetails['details'].type_of_product=='hand'){
               $("#type_of_product").text('Hand made');
              }else{  
               $("#type_of_product").text('Factory type');
              }
    
            
               $("#dist_qty").text(viewalldetails['details'].dist_qty);
               $("#status").text(viewalldetails['details'].status);

               var imageurl = viewalldetails['imgurl']+'/'+viewalldetails['details'].dish_image;
            	console.log(imageurl,'imageurl');  

            	var elem = document.createElement("img");
elem.setAttribute("src", imageurl );
elem.setAttribute("height", "100");
elem.setAttribute("width", "100");
elem.setAttribute("alt", "Flower");
document.getElementById("placehere").appendChild(elem);


          for (i = 0; i < viewalldetails['images'].length; i++) {
   console.log(viewalldetails['images'][i].image_name);  

    var imageurl = viewalldetails['imgurl']+'/'+viewalldetails['images'][i].image_name;
              console.log(imageurl,'imageurl');  

              var elem = document.createElement("img");
elem.setAttribute("src", imageurl );
elem.setAttribute("height", "100");
elem.setAttribute("width", "100"); 
elem.setAttribute("margin-left", "5");    
elem.setAttribute("alt", "Flower");
document.getElementById("placehere1").appendChild(elem);

}


            	  // $("#subcategorylistview").html(subcategorylist); 
            },
        });
$('#viewalldetails').modal('show');
	}
</script>
    <div id="reply-user" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Send message to <span id="email-reply"></span></h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($contact_model, 'send_message')->hiddenInput()->label(false) ?>
                    <?= $form->field($contact_model, 'user_id')->hiddenInput()->label(false) ?>
                    <?= $form->field($contact_model, 'role')->hiddenInput()->label(false) ?>
                    <?= $form->field($contact_model, 'email')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($contact_model, 'subject')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($contact_model, 'message')->textArea(['rows' => 4])->hint('[user_name] will auto populate user\'s name.') ?>
                    <?= Html::submitButton(Yii::t('app', 'Send') , ['class' => 'btn btn-warning']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-body text-center">
        <img src="" id="imagepreview" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>





   <div id="viewalldetails" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Product Details <span id="email-reply"></span></h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <div class="col-lg-12">
<div class="row" >
	 <div class="col-lg-6" style="float: left;" >
<p>Product Name</p>
</div>  
 <div class="col-lg-6">
<span id="dish_name"></span>      
</div> 

</div>


<div class="row" >
   <div class="col-lg-6" style="float: left;" >
<p>Home Category Name</p>
</div>    
 <div class="col-lg-6">
<span id="homecategoryname" ></span>
</div>
</div>  

   
<div class="row" >
	 <div class="col-lg-6" style="float: left;" >
<p>Category Name</p>
</div>    
 <div class="col-lg-6">
<span id="categoryname" ></span>
</div>
</div>  



<div class="row" >
   <div class="col-lg-6" style="float: left;" >
<p>Attribute Name</p>
</div>       
 <div class="col-lg-6">
<span id="attributename" ></span>
</div>
</div>  


<div class="row" >
   <div class="col-lg-6" style="float: left;" >
<p>Product Price</p>
</div>  
 <div class="col-lg-6">
<span id="dish_price" ></span>
</div>

</div> 


<div class="row" >
	 <div class="col-lg-6" style="float: left;" >
<p>Collected Amount </p>
</div>  
 <div class="col-lg-6">
 <span id="collected_amount" ></span>
</div>

</div>   

<div class="row" >
	 <div class="col-lg-6" style="float: left;" >
<p>Discount</p>
</div>  
 <div class="col-lg-6">
 	<span id="discount" ></span>
</div>

</div>

<div class="row" >
<div class="col-lg-6" style="float: left;" >
<p>Description</p>
</div>  
<div class="col-lg-6">
<span id="dish_description" ></span>
</div>
</div>

<div class="row" >
<div class="col-lg-6" style="float: left;" >
<p>Type of Product</p>
</div>  
<div class="col-lg-6">
<span id="type_of_product" ></span>
</div>
</div> 

<div class="row" >
<div class="col-lg-6" style="float: left;" >
<p>Qty</p>
</div>  
<div class="col-lg-6">
<span id="dist_qty" ></span>
</div>
</div> 

<div class="row" >
<div class="col-lg-6" style="float: left;" >
<p>Status</p>
</div>  
<div class="col-lg-6">
<span id="status" ></span>
</div>
</div>

<div class="row" >
	 <div class="col-lg-6" style="float: left;" >
<p>Image</p>
</div>  
 <div class="col-lg-6" id="placehere" > 
</div>
 </div> 

 <div class="row" style="margin-top: 5px;" >
   <div class="col-lg-6" style="float: left;" >
<p></p>
</div>     
 <div class="col-lg-6"   >
 <span id="placehere1"></span> 
</div>
 </div> 


</div>  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>



<?php
$this->registerJs('
	$(\'.popImg\').on(\'click\',function(){
		$(\'#imagepreview\').attr(\'src\', $(this).attr(\'data-src\')); 
		 $(\'#imagemodal\').modal(\'show\');
	});
	$(\'.acc_sel\').on(\'click\',function(){
		var sel_val = [];
		$.each($("input[name=\'selection[]\']:checked"), function(){
			sel_val.push($(this).val());
		});
		var ids = sel_val.join(",");
		if(ids==""){
			alert("Please select the images");
			return false;
		}
		if(ids!="" && confirm("Are you sure want to accept selected images?"))
			window.location.href = "'.$url_format.'?id="+ids;
	})
');
$this->registerJs('
	$(".send_reply").on("click",function(){
		var email = $(this).attr("u-email");
		var subject = $(this).attr("u-subject");
		var user_id = $(this).attr("u-user_id");
		var dish_image = $(this).attr("u-dish_image");
		var mid = $(this).attr("u-mid");  
		var msg = $(this).attr("u-msg");
		$("#email-reply").html(email);
		$("#contactmessage-email").val(email);
		$("#contactmessage-role").val("admin");
		$("#contactmessage-user_id").val(user_id);
		$("#contactmessage-subject").val(subject);
		$("#contactmessage-message").val(msg);
		$("#contactmessage-send_message").val(mid);
		
	})
');




