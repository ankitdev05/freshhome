<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use kartik\select2\Select2;
use himiklab\thumbnail\EasyThumbnailImage;

use common\models\City;

$language_id = 1;
$all_cities = ArrayHelper::map(City::find()->joinWith('cityName as cn')->where(['cn.language_id'=>$language_id])->all(), 'id', 'cityName.city_name');

$imageSrc = $delUrl = '';
if(!empty($model->dish_image)){
	$image=EasyThumbnailImage::thumbnailFileUrl(
		"../../frontend/web/uploads/".$model->dish_image,200,200,EasyThumbnailImage::THUMBNAIL_INSET
	);
	$imageSrc	= [Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Dish Image', 'title'=>'Dish Image'])];
	$delUrl=[['caption'=>'Dish Image','url'=>yii::$app->request->baseUrl.'/menu/deleteimage?id='.$model->id.'&s_id='.$user_info->id]];
}
?>
<div class="row">
	<div class="col-sm-3">
		<?= $form->field($model, 'dish_name')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-2">
		<?= $form->field($model, 'dish_price')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-2">
		<?= $form->field($model, 'dish_weight')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-2">
		<?= $form->field($model, 'dist_qty')->textInput() ?>
	</div>
	<div class="col-sm-2">
		<?= $form->field($model, 'dish_serve')->textInput() ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'dish_description')->textarea(['rows' => 6]) ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'dish_image')->widget(FileInput::classname(), 
		[
			'options'=>['accept'=>'image/*'],
			'pluginOptions'=>
			[
				'allowedFileExtensions'=>['jpg','gif','png'],
				'overwriteInitial'=>true,
				'initialPreview'=>$imageSrc,
				'initialPreviewConfig'=>$delUrl
			],
		]) ?>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'dish_since')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'city_id')->textInput()->widget(Select2::classname(), [
			'data' => $all_cities,
			'options' => [
				'placeholder' => 'Please select ...',
			],
			'pluginOptions' => [
				'allowClear' => true
			],
	]) ?>
	</div>
	<div class="col-sm-3">
	<?= $form->field($model, 'status')->textInput()->widget(Select2::classname(), [
		'data' => [ 'active' => 'Active', 'inactive' => 'Inactive'],
	]) ?>
	</div>
</div>