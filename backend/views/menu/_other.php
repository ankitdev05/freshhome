<?php
use common\models\Meal;
use common\models\Cuisine;
use common\models\Product;
use common\models\Category;

use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
$language_id = 1;
$all_categories 	= ArrayHelper::map(Category::find()->joinWith('categoryDescription as cn')->where(['cn.language_id'=>$language_id])->orderBy('cn.category_name asc')->all(), 'category_id', 'categoryDescription.category_name');
$all_meals		 	= ArrayHelper::map(Meal::find()->all(), 'id', 'meal_name');
$all_cuisines		= ArrayHelper::map(Cuisine::find()->joinWith('cuisineName as cn')->where(['cn.language_id'=>$language_id])->orderBy('cn.cuisine_name asc')->all(), 'id', 'cuisineName.cuisine_name');

if(!empty($model->menuCategories)){
	$cats = [];

	foreach($model->menuCategories as $menCats)
		$cats[] = $menCats->category_id;
	$model->category = $cats;	
}
if(!empty($model->menuCuisines)){
	$cats = [];

	foreach($model->menuCuisines as $menCats)
		$cats[] = $menCats->cuisine_id;
	$model->cuisine = $cats;	
}
if(!empty($model->menuMeals)){
	$cats = [];

	foreach($model->menuMeals as $menCats)
		$cats[] = $menCats->meal_id;
	$model->meal = $cats;	
}
?>
<div class="row">
	<div class="col-sm-4">
		<?= $form->field($model, 'category')->widget(Select2::classname(), [
			'data' => $all_categories,
			'options' => [
				'placeholder' => 'Please select ...',
			],
			'pluginOptions' => [
				'allowClear' => true,
				'multiple'=>true
			],
	]) ?>
	</div>
	<div class="col-sm-4">
		<?= $form->field($model, 'cuisine')->widget(Select2::classname(), [
			'data' => $all_cuisines,
			'options' => [
				'placeholder' => 'Please select ...',
			],
			'pluginOptions' => [
				'allowClear' => true,
				'multiple'=>true
			],
	]) ?>
	</div>
	<div class="col-sm-4">
		<?= $form->field($model, 'meal')->widget(Select2::classname(), [
			'data' => $all_meals,
			'options' => [
				'placeholder' => 'Please select ...',
			],
			'pluginOptions' => [
				'allowClear' => true,
				'multiple'=>true
			],
	]) ?>
	</div>
</div>