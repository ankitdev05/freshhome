<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\SubscriptionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Subscriptions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriptions-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'email:email',
                'ip_address',
                ['attribute'=>'status',
                    'filter' => [1=>'Active',0=>'Inactive'],
                    'value' => function($data){
                        return $data->status ==1 ? 'Active' : 'Inactive';
                    }
                ],
                'created_at:datetime',
                [
                    'class' => 'yii\grid\FaActionColumn',
                    'template'=>'{status_update}',
                    'buttons'=>[
                        'status_update'=>function($url, $model, $key){
                            $title = 'Subscribe';
                            $icon   = 'check';
                            $update = 1;
                            if($model->status==1) {
                                $update = 0;
                                $title = 'Unsubscribe';
                                $icon   = 'times ';
                            }
                            return Html::a(Html::tag('i','',['class'=>'fas fa-'.$icon]),Url::to(['index','update'=>$update,'id'=>$model->id]), ['data-pjax' => 0,'alt'=>$title,'title'=>$title,'class'=>'btn btn-secondary','data-confirm'=>'Are you sure want to '.strtolower($title).' this email?']);
                        }
                    ]
                ]
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>

</div>
