<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderRatingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Supplier Reviews');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-ratings-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <div class="table-responsive">
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'user_id',
                'filter'=>false,
                'value'=>function($data){
                    $user_info = $data->user->userDetail;
                    return $user_info['name'];
                }
            ],
            [
                'attribute'=>'supplier_id',
                'filter'=>false,
                'value'=>function($data){
                    $user_info = $data->supplier->userInfo;
                    return $user_info['name'];
                }
            ],
            //'taste_rating',
            //'presentation_rating',
            //'packing_rating',
            //'overall_rating',
            'review:ntext',
            //'is_active',
            'created_at:datetime',
            //'updated_at',

            [
               'class' => 'yii\grid\ActionColumn','template'=>'{approve}',
                'buttons'=>[
                    'approve'=>function($url, $model, $key) use ($url_format){
                        return Html::a(Html::tag('button',Html::tag('i',' Accept ',['class'=>'fas fa-check']),['class'=>'btn btn-sm btn-primary']), $url_format.'?id='.$model->id, ['data-pjax' => 0,'data-confirm'=>'Are you sure want to accept this review?']);
                    },
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
</div>
