<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderRatings */

$this->title = Yii::t('app', 'Create Order Ratings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Order Ratings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-ratings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
