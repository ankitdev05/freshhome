<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MainCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-categories-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'class' => 'yii\grid\FaActionColumn',
                'template'=>' {view_cats}',
                'buttons'=>[
                    'view_cats'=>function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'fas fa-list']), ['/category','cat_id' => $model->id], ['data-pjax' => 0,'class'=>'btn btn-secondary','title'=>'Categories']);
                    },
                ]
            ],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>

    <?php Pjax::end(); ?>

</div>
