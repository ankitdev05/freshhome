<?php

use yii\helpers\Html;
use common\models\OptionDescription;
/* @var $this yii\web\View */
/* @var $model common\models\OptionValue */

$option_desc = OptionDescription::find()->where(['option_id'=>$option_model->option_id,'language_id'=>1])->one();
$this->title = Yii::t('app', 'Create Option');

$this->params['breadcrumbs'][] = ['label' => 'Attribute Group','url' => ['/attribute-group']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Attributes'), 'url' => ['/option/index','attr_id' => $attr_id]];
$this->params['breadcrumbs'][] = ['label'=>$option_desc->name.' Options','url'=>['/option-value','attr_id' => $attr_id,'option_id' =>$option_model->option_id]];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="option-value-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
