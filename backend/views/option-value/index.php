<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\OptionDescription;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OptionValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$option_desc = OptionDescription::find()->where(['option_id'=>$option_model->option_id,'language_id'=>1])->one();
$this->title = Yii::t('app', 'Options');

$this->params['breadcrumbs'][] = ['label' => 'Attribute Group','url' => ['/attribute-group']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Attributes'), 'url' => ['/option/index','attr_id' => $attr_id]];
$this->params['breadcrumbs'][] = ['label'=>$option_desc->name,'url'=>['/option/update','id'=>$option_model->option_id,'attr_id'=>$attr_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="option-value-index">
    <p>
        <?= Html::a(Yii::t('app', 'Create Option'), ['create','option_id'=>$option_model->option_id,'attr_id' => $attr_id], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'image',
            'name',
            'sort_order',
            [
                'class' => 'yii\grid\FaActionColumn','template' => '{update} {delete}',
                'buttons' => [
                   'update' => function($url, $model, $key) use ($attr_id){
                       return Html::a(Html::tag('i','',['class'=>'fas fa-edit']), ['update','id'=>$model->option_value_id,'option_id' => $model->option_id,'attr_id'=>$attr_id], ['title'=>'Options Values','data-pjax' => 0,'class'=>'btn btn-secondary']);
                   },
                    'delete' => function($url, $model, $key) use ($attr_id){
                        return Html::a(Html::tag('i','',['class'=>'fas fa-trash']), ['delete','id'=>$model->option_value_id,'option_id' => $model->option_id,'attr_id'=>$attr_id], ['title'=>'Delete','data-pjax' => 0,'class'=>'btn btn-secondary','data-confirm' =>'Are you sure want to delete this item?','data-method' =>'POST']);
                    }
                ]
            ],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>

    <?php Pjax::end(); ?>

</div>
