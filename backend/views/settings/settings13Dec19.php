<?php
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Tabs;
use kartik\file\FileInput;
use himiklab\thumbnail\EasyThumbnailImage;
use kartik\select2\Select2;
use common\models\Currency;
use dosamigos\tinymce\TinyMce;
$all_currency = ArrayHelper::map(Currency::find()->orderBy('name')->all(), 'code', 'name');
?>

<div class="settings-form">
<?php
$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
$imageSrc= '';
$delUrl= '';
if(!empty($ConfigData)){
		$model->config_address 			= $ConfigData['config_address'];
		$model->config_footer 			= $ConfigData['config_footer'];
		$model->config_meta_keywords	= $ConfigData['config_meta_keywords'];
		$model->config_meta_description = $ConfigData['config_meta_description'];
		$model->config_analytic			= $ConfigData['config_analytic'];
		if(!empty($ConfigData['config_logo'])){
			$image=EasyThumbnailImage::thumbnailFileUrl(
					"../../frontend/web/images/store/".$ConfigData['config_logo'],200,200,EasyThumbnailImage::THUMBNAIL_INSET
				);
			$imageSrc	= [Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Website logo', 'title'=>'Website logo'])];
			$delUrl=[['caption'=>'Store Logo','url'=>yii::$app->request->baseUrl.'/settings/deleteimage?key=confog_logo']];
		}
}
echo Tabs::widget([
    'items' => [
        [
            'label' =>yii::t('yii','General'),
            'content' =>'<br/><div class="row"><div class="col-sm">'.
			$form->field($model, 'config_last_update')->hiddenInput(['maxlength' => 255,'value'=>$ConfigData['config_last_update']])->label(false).
			$form->field($model, 'config_website_url')->textInput(['maxlength' => 255,'value'=>$ConfigData['config_website_url']]).'</div><div class="col-sm">'.
			$form->field($model, 'config_name')->textInput(['maxlength' => 255,'value'=>$ConfigData['config_name']]).'</div></div>'.
			$form->field($model, 'config_owner')->textInput(['maxlength' => 255,'value'=>$ConfigData['config_owner']]).'<div class="row"><div class="col-sm">'.
			$form->field($model, 'config_address')->textarea(['rows' => 6,'value'=>$ConfigData['config_address']]).'</div><div class="col-sm">'.
			$form->field($model, 'config_footer')->textarea(['rows' => 6,'value'=>$ConfigData['config_footer']]).'</div></div><div class="row"><div class="col-sm">'.
			$form->field($model, 'config_email')->textInput(['maxlength' => 255,'value'=>$ConfigData['config_email']]).'</div><div class="col-sm">'.
			$form->field($model, 'config_telephone')->textInput(['maxlength' => 255,'value'=>$ConfigData['config_telephone']]).'</div></div><div class="row"><div class="col-sm">'.
			$form->field($model, 'config_logo')->widget(FileInput::classname(), 
			[
				'options'=>['accept'=>'image/*'],
				'pluginOptions'=>
				[
					'allowedFileExtensions'=>['jpg','gif','png'],
					'overwriteInitial'=>true,
					'initialPreview'=>$imageSrc,
					'initialPreviewConfig'=>$delUrl
				],
			]).'</div><div class="col-sm">'.
			$form->field($model, 'config_analytic')->textarea(['rows'=>6, 'cols'=>50,'value'=>$ConfigData['config_analytic']]).'</div></div>',
            'active' => true
        ],		
		[
            'label' => yii::t('yii','Meta Information'),
            'content' =>'<br/>'.
			$form->field($model, 'config_title')->textInput(['maxlength' => 255,'value'=>$ConfigData['config_title']]).
			$form->field($model, 'config_meta_description')->textarea(['rows' => 6,'value'=>$ConfigData['config_meta_description']]).
			$form->field($model, 'config_meta_keywords')->textarea(['rows' => 6,'value'=>$ConfigData['config_meta_keywords']]),
        ],
		[
            'label' => yii::t('yii','Store Options'),
            'content' =>'<br/><div class="row"><div class="col-sm-4">'.
			$form->field($model, 'config_vat')->textInput(['type' => 'number','min'=>0,'step'=>0.1]).'</div>'.
                '<div class="col-sm-4">'.
			$form->field($model, 'config_freshhommee_fee')->textInput(['type' => 'number','min'=>0,'step'=>0.1]).'</div>'.
                '<div class="col-sm-4">'.
                $form->field($model, 'config_transaction_fee')->textInput(['type' => 'number','min'=>0,'step'=>0.01]).'</div>'.
                '<div class="col-sm-12">'.
                $form->field($model, 'config_ask_for_help_text')->widget(TinyMce::className(), [
                    'options' => ['rows' => 12],
                    'language' => 'en',
                    'clientOptions' => [
                        'plugins' => [
                            "advlist autolink lists link charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table contextmenu paste"
                        ],
                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                    ]
                ]) .'</div>'.
			'<div class="col-sm-6">'.$form->field($model, 'config_default_currency')->widget(Select2::classname(), [
				'data' => $all_currency,
			]).'</div></div>',
        ]
    ],
]);
?>

<div class="clearfix"></div><br/>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Save') : Yii::t('yii', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
