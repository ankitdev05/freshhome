<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\HomeCategoryDescription;
use kartik\select2\Select2;

use yii\helpers\ArrayHelper;
// use common\models\Category;
// use common\models\BrandCategory;
use kartik\file\FileInput;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Url;
use common\models\Language;
// use common\models\BrandDescription;

$languages      = Language::find()->where(['status'=>1])->all();
 
if(!empty($model->home_category_id)){
  $descriptions = HomeCategoryDescription::find()->where(['home_category_id'=>$model->home_category_id])->all(); 

  foreach($descriptions as $description){
    $model->home_category_names[$description->language_id] = $description->home_category_name;
  }  
} 


/* @var $this yii\web\View */
/* @var $model backend\models\Brand */ 
/* @var $form yii\widgets\ActiveForm */

$imageSrc = $delUrl = '';
if(!empty($model->image)){ 
       $path1 = Url::base();  
                $path = str_replace("administrator","",$path1);
                $image = $path."frontend/web/uploads/". $model->image ; 
    $imageSrc   = [Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Profile Picture', 'title'=>'Profile Picture'])];
    $delUrl=[['caption'=>'Profile Picture','url'=>yii::$app->request->baseUrl.'/user/deleteimage?id='.$model->home_category_id]];
}


?>
<style type="text/css">
  .file-preview-image{
    max-width: 200px;
    max-height: 200px;
  }



.scrollbar
{
  /*margin-left: 30px;*/
  float: left;
  height: 200px;
  width: 400px;
  background: #F5F5F5;
  overflow-y: scroll;
  /*margin-bottom: 25px;*/
}

.force-overflow
{
  min-height: 200px;
}

#wrapper
{
  text-align: center;
  width: 500px;
  margin: auto;
}




#style-2::-webkit-scrollbar-track
{
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
  background-color: #F5F5F5;
}

#style-2::-webkit-scrollbar
{
  width: 10px;
  background-color: #F5F5F5;
}

#style-2::-webkit-scrollbar-thumb
{
  background-color: #000000;
  border: 2px solid #555555;
}

 
  
table{
border:none;
}
</style> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">  
<div class="brand-form">

    <?php $form = ActiveForm::begin(); ?>
  <div class="col-lg-12">
    <div class="row" >
        <?php foreach($languages as $language){ ?>
    <div class="col-sm-4">
      <?= $form->field($model, "home_category_names[{$language->language_id}]")->textInput(['maxlength' => true])->label(Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' (Home Category Name)') ?>
    </div>
  <?php } ?>
   
   <div class="col-sm-4">
      <?= $form->field($model, 'status')->widget(Select2::classname(), [
          'data' => [1 => 'Active', 0 => 'Inactive'],
          
        ])
      ?>
    </div>

    </div>
           
                              
 
<div class="row"  >
        <div class="col-lg-12 col-md-12 "  >
        <?= $form->field($model, 'image')->widget(FileInput::classname(),
        [
            'options'=>['accept'=>'image/*'],
            'pluginOptions'=>
            [
                'allowedFileExtensions'=>['jpg','gif','png'],
                'overwriteInitial'=>true,
                'initialPreview'=>$imageSrc,
                'initialPreviewConfig'=>$delUrl
            ],
        ]) ?>
    </div>
       
</div>







     <div class="w-100"></div>
     <div class="row" >
    <div class="col-sm-4" style="margin-top:28px;margin-bottom: 20px;">
      <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>
    </div>
</div>
    <?php ActiveForm::end(); ?>
</div>
</div>
<script type="text/javascript">
	 


 
</script>