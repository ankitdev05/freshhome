<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\HomeCategoryDescription;

/* @var $this yii\web\View */
/* @var $model common\models\MainCategories */

$this->title = $brandname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Home Category'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
// die;
?>
<div class="main-categories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->home_category_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->home_category_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'home_category_id',
           [
                'attribute'=>'home_category_names',
                'format'=>'raw',
                'value'=>function($model){
                   $descriptions = HomeCategoryDescription::findAll(['home_category_id'=>$model->home_category_id]);
           
                    $html = '';
                    foreach($descriptions as $description){
                        $language   = $description->language;
                        $html .= '<span class="lang_'.$language->language_id.'">'.Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' '.$description->home_category_name.'</span><br/>';
                    }
                    return $html; 
                } 
            ],
            // 'status',
             [
                'attribute' => 'status',
                'value' => function($model){
                    return $model->status==1 ? 'Active' : 'Inactive';
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
