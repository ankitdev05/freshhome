<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MainCategories */

$this->title = 'Update Home Category: ' . $brandname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Home Category'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $brandname, 'url' => ['view', 'id' => $model->home_category_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="home-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        // 'brandname' => $brandname,
    ]) ?> 

</div>
