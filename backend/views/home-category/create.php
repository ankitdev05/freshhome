<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MainCategories */

$this->title = Yii::t('app', 'Create Home Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Home Category'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
