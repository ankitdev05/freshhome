<?php

use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\grid\GridView;
use himiklab\thumbnail\EasyThumbnailImage;
use common\models\HomeCategoryDescription;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MainCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Home Category');
$this->params['breadcrumbs'][] = $this->title; 
?>
<div class="main-categories-index">

    <h1><?= Html::encode($this->title) ?></h1> 
    
    <p>
        <?= Html::a('Create Home Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    

      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'brand_id',
            [
                'attribute'=>'home_category_names',
                'format'=>'raw',
                'value'=>function($model){
                   $descriptions = HomeCategoryDescription::findAll(['home_category_id'=>$model->home_category_id]);
           
                    $html = '';
                    foreach($descriptions as $description){
                        $language   = $description->language;
                        $html .= '<span class="lang_'.$language->language_id.'">'.Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' '.$description->home_category_name.'</span><br/>';
                    }
                    return $html; 
                } 
            ], 

           
             [
                'attribute' => 'image',
                'format' => 'image',
                'value' => function ($model){
                    if(!empty($model->image)){ 

                        return EasyThumbnailImage::thumbnailFileUrl(
                            "../../frontend/web/uploads/".$model->image,100,100,EasyThumbnailImage::THUMBNAIL_INSET
                        );
                    }
                }
            ],

         


            // 'maincategoryid',

            // ['class' => 'yii\grid\ActionColumn'],
            [
                // 'class' => 'yii\grid\FaActionColumn',
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view} {update}',
                'buttons'=>[
                    'phone_numbers'=>function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'fas fa-phone']), ['/driver-company-numbers','id' => $model->home_category_id], ['data-pjax' => 0,'class'=>'btn btn-secondary','title'=>'Phone Numbers']);
                    },
                ]
            ],
        ],
    ]); ?>


    <?php Pjax::end(); ?>

</div>
