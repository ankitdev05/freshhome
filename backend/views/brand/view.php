<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use common\models\CategoryDescription;

/* @var $this yii\web\View */
/* @var $model backend\models\Brand */ 
$this->title = $brandname;
$this->params['breadcrumbs'][] = ['label' => 'Brands', 'url' => ['index']];
$this->params['breadcrumbs'][] = ucfirst($this->title);
?>
<div class="brand-view">

    <h1><?= Html::encode(ucfirst($this->title)) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->brand_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->brand_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<div class="row">
 
     <div class="col-lg-12">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'name',
        ],
    ]) ?>

</div></div>
 


<div class="row">
  
 <div class="col-lg-6">
   <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">S no.</th>
      <th scope="col">Category Name</th>

    </tr>
  </thead>
  <tbody> 

   <?php if(!empty($categorylist)){ 
                    foreach ($categorylist as  $key=>$value) { 
                        $catid = $value['category_id'];

                           $categoryname = CategoryDescription::find()->where(['category_id'=>$catid ])->andWhere(['language_id'=>1])->one();




                        ?>
       <tr>
          <th scope="row"><?= $key+1; ?></th>
          <td><?= ucfirst($categoryname['category_name']); ?></td>
      </tr>                           
                           
                     <?php  } }else{ ?>
        <tr  >
          <th scope="col" colspan="2" style="text-align: center;color: red;" >No Record Found</th>
      </tr>  
      <?php } ?>   
   
  </tbody>
</table>


</div>

  <div class="col-lg-6">
    <?php 
   
                
            
                $src='';
                if(!empty($model->image)){
                   
                         $path1 = Url::base();  
                $path = str_replace("administrator","",$path1);
                $src = $path."frontend/web/uploads/". $model->image ; 
             
                 ?> 

               <img id="imgupload" src="<?= $src; ?>" alt="your image "width="120" height="120" name="vender[banner]" class="img-rounded">
    
        
                                    
          <?php   }   ?>  
                                                            

    </div>

</div>
</div>
