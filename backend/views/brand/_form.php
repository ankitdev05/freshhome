<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MainCategories;
use yii\helpers\ArrayHelper;
use common\models\Category;
use common\models\BrandCategory;
use kartik\file\FileInput;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Url;
use common\models\Language;
use common\models\BrandDescription;

$languages      = Language::find()->where(['status'=>1])->all();

if(!empty($model->brand_id)){
  $descriptions = BrandDescription::find()->where(['brand_id'=>$model->brand_id])->all(); 

  foreach($descriptions as $description){
    $model->brand_names[$description->language_id] = $description->brand_name;
  }  
} 

/* @var $this yii\web\View */
/* @var $model backend\models\Brand */ 
/* @var $form yii\widgets\ActiveForm */
$subcategory=Category::find()->all();

$imageSrc = $delUrl = '';
if(!empty($model->image)){ 
       $path1 = Url::base();  
                $path = str_replace("administrator","",$path1);
                $image = $path."frontend/web/uploads/". $model->image ; 
    $imageSrc   = [Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Profile Picture', 'title'=>'Profile Picture'])];
    $delUrl=[['caption'=>'Profile Picture','url'=>yii::$app->request->baseUrl.'/user/deleteimage?id='.$model->brand_id]];
}


?>
<style type="text/css">
  .file-preview-image{
    max-width: 200px;
    max-height: 200px;
  }



.scrollbar
{
  /*margin-left: 30px;*/
  float: left;
  height: 200px;
  width: 400px;
  background: #F5F5F5;
  overflow-y: scroll;
  /*margin-bottom: 25px;*/
}

.force-overflow
{
  min-height: 200px;
}

#wrapper
{
  text-align: center;
  width: 500px;
  margin: auto;
}




#style-2::-webkit-scrollbar-track
{
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
  background-color: #F5F5F5;
}

#style-2::-webkit-scrollbar
{
  width: 10px;
  background-color: #F5F5F5;
}

#style-2::-webkit-scrollbar-thumb
{
  background-color: #000000;
  border: 2px solid #555555;
}

 
  
table{
border:none;
}
</style> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">  
<div class="brand-form">

    <?php $form = ActiveForm::begin(); ?>
  <div class="col-lg-12">
    <div class="row" >
        <?php foreach($languages as $language){ ?>
    <div class="col-sm-4">
      <?= $form->field($model, "brand_names[{$language->language_id}]")->textInput(['maxlength' => true])->label(Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' (Brand Name)') ?>
    </div>
  <?php } ?>
    </div>
           
                              
 
<div class="row"  >
        <div class="col-lg-12 col-md-12 "  >
        <?= $form->field($model, 'image')->widget(FileInput::classname(),
        [
            'options'=>['accept'=>'image/*'],
            'pluginOptions'=>
            [
                'allowedFileExtensions'=>['jpg','gif','png'],
                'overwriteInitial'=>true,
                'initialPreview'=>$imageSrc,
                'initialPreviewConfig'=>$delUrl
            ],
        ]) ?>
    </div>
       
</div>


 <div class="row">

      <div class="col-lg-6 col-md-6 ">
    <?= $form->field($model,'maincategoryid')->dropDownList(ArrayHelper::map(MainCategories::find()->all(),'id','name'),['prompt'=>'Select Category', "onchange"=>'getcate(this.value)'])->label('Category'); ?>
     </div>

      <div class="col-lg-6 col-md-6 ">
    </div>

 </div>


    <div class="row">
      <div class="col-lg-6"  >
     <!-- 
         <caption><span style="margin-bottom: 10px;"> Sub Category </span></caption>
      -->    
             <div id="wrapper">
     <div class="scrollbar" id="style-2">
      <div class="force-overflow">

           <div class="panel-body table-responsive" id="tablepanel">
                                     <input type="hidden" name="sid" id = "sid">
                                    
                                    <table border="1" class="table"  id ="table1">
                                      
                                        <tbody id ="subcategorylistview">
                                           
                                        </tbody>
                                    </table>

                                    </div>
                                  </div></div></div>
                                     <br>
                                </div>
       <div class="col-lg-6">
          
 <!--  <caption><span style="margin-bottom: 10px;"> Selected Category </span></caption>
  -->               
     <div id="wrapper">
     <div class="scrollbar" id="style-2">
      <div class="force-overflow">
                                 <div class="panel-body table-responsive"  id="tablepanel">
                                     <input type="hidden" name="sid" id = "sid">
                                    
                                    <table border="1" class="table" id ="table1">
                                      
                                        <tbody id ="selectedsubcate">
                     <?php if(!empty($categorylist)){ 
                    foreach ($categorylist as  $value) { 
                        $catid = $value['category_id'];
                        ?>
                                           
<tr class="active" id = selectedcategory<?= $catid ?> > <td style="text-align:left;padding-left:25px;border-right:none;" ><?= $value['category_name']; ?> </td><td    style="text-align:right;padding-left:25px;border-left:none;" ><a onclick='removeitem(<?= $catid ?>)' class='fas fa-times-circle' style="color: red;
    font-size: 22px;" > </a><input type=hidden name='subcategory[]' value='<?= $catid ?>'  > </td> </tr>                              
                           
                     <?php  } } ?>
                                        </tbody>

                                    </table>
                                    </div>
                              </div></div></div>

                                </div>

                              </div>



     <div class="w-100"></div>
     <div class="row" >
    <div class="col-sm-4" style="margin-top:28px;margin-bottom: 20px;">
      <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>
    </div>
</div>
    <?php ActiveForm::end(); ?>
</div>
</div>
<script type="text/javascript">
	 function getcate(id){
        console.log(id);
       
            $.ajax({
            type: "POST",
            url: "<?= Yii::$app->urlManager->createUrl('brand/getcategory'); ?>",
            data: {id: id},
            dataType: 'html',
            cache: false,
            success: function (data) {
                var categorydetails = JSON.parse(data);
               // console.log(studentdetails,'category');
                   var subcategorylist = '';
                if(categorydetails != ''){
                    
                 $.each( categorydetails, function( key, value ) {
                  
 var input = '<input type = "checkbox" id = "subcategory'+ value.category_id +'" name ="category" onclick="return addcategory(\'' + value.category_id + ',' + value.name + '\')" >';  
                 subcategorylist +="<tr class='active'><td style='text-align:left;padding-left:25px;' >"+input+" &nbsp "+value.name+"</td></tr>";
             
              }); 
               }else{
                  subcategorylist +="<tr class='active'><td  style='text-alignc:center;'>No Record Found !</td></tr>"
               }
                // console.log(subcategorylist,'subcategorylist'); 
              $("#subcategorylistview").html(subcategorylist); 
            },
        });
    }
 

    function addcategory(id){ 
        var chars = id.split(',');
        var id = chars[0];
        var name = chars[1];
        var chekvalue = $("#selectedcategory"+id).val();

     if(chekvalue != undefined){  
    if($('#subcategory'+id).is(':checked')){
      alert(name+' Already Added'); 
      // document.getElementById("subcategory"+id).checked = true;
    }else{
      $('#selectedcategory'+id).remove();
   
     
    }

     }else{  

    if($('#subcategory'+id).is(':checked')){
  
   selectedsubcate ="<tr class='active' id = selectedcategory" + id + " ><td  style='text-align:left;padding-left:25px;border-right:none;'>" + name + "</td><td style='text-align:left;padding-left:25px;border-left:none;text-align:right;' ><a onclick='removeitem("+ id +")' class='fas fa-times-circle' style='color: red; font-size: 22px;' ></a><input type='hidden' name='subcategory[]' value='"+ id +"' ></td></tr>"
    $("#selectedsubcate").append(selectedsubcate); 
   
   }else{
      $('#selectedcategory'+id).remove();
   }

   }
  


    }

    function removeitem(id){
      
      $('#selectedcategory'+id).remove();
      document.getElementById("subcategory"+id).checked = false;
      
    }


 
</script>