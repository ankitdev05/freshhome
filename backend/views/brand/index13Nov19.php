<?php

use yii\helpers\Html;
use yii\grid\GridView;
use himiklab\thumbnail\EasyThumbnailImage;
use common\models\BrandDescription;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\BrandSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Brands';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    img{
        width: 60px;
    }
</style>
<div class="brand-index"> 

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Brand', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'brand_id',
            [
                'attribute'=>'brand_names',
                'format'=>'raw',
                'value'=>function($model){
                   $descriptions = BrandDescription::findAll(['brand_id'=>$model->brand_id]);
                    $html = '';
                    foreach($descriptions as $description){
                        $language   = $description->language;
                        $html .= '<span class="lang_'.$language->language_id.'">'.Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' '.$description->brand_name.'</span><br/>';
                    }
                    return $html; 
                }
            ],

            // 'name',
            // 'image',
             [ 
                'attribute' => 'image',
                'format' => 'image', 
                'value' => function ($model){
                  
                      $src='';
                if(!empty($model->image)){
                  
                         $path1 = Url::base();  
                $path = str_replace("administrator","",$path1);
                $src = $path."frontend/web/uploads/". $model->image ;  

           return $src;
    
          } 

                }
            ],

         


            // 'maincategoryid',

            // ['class' => 'yii\grid\ActionColumn'],
            [
                // 'class' => 'yii\grid\FaActionColumn',
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view} {update}',
                'buttons'=>[
                    'phone_numbers'=>function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'fas fa-phone']), ['/driver-company-numbers','id' => $model->brand_id], ['data-pjax' => 0,'class'=>'btn btn-secondary','title'=>'Phone Numbers']);
                    },
                ]
            ],
        ],
    ]); ?>
</div>
