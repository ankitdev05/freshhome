<?php
use common\models\BrandDescription;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Brands');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-index">
  
    <?php Pjax::begin(); ?>
    <p>
        <?= Html::a(Yii::t('app', 'Create Brand'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="table-responsive">
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        "summary"=>false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'brand_id',
            [
                'attribute'=>'brand_names',
                'format'=>'raw',
                'value'=>function($model){
                   $descriptions = BrandDescription::findAll(['brand_id'=>$model->brand_id]);
                   // echo "<pre>";
                   // print_r($descriptions);die; 
                    $html = '';
                    foreach($descriptions as $description){
                        $language   = $description->language;
                        $html .= '<span class="lang_'.$language->language_id.'">'.Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' '.$description->brand_name.'</span><br/>';
                    }
                    return $html; 
                }
            ],

            // 'name',
            // 'image',
           
             [
                'attribute' => 'image',
                'format' => 'image',
                'value' => function ($model){
                    if(!empty($model->image)){
                        return EasyThumbnailImage::thumbnailFileUrl(
                            "../../frontend/web/uploads/".$model->image,100,100,EasyThumbnailImage::THUMBNAIL_INSET
                        );
                    }
                }
            ],

         


            // 'maincategoryid',

            // ['class' => 'yii\grid\ActionColumn'],
            [
                // 'class' => 'yii\grid\FaActionColumn',
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view} {update}',
                'buttons'=>[
                    'phone_numbers'=>function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'fas fa-phone']), ['/driver-company-numbers','id' => $model->brand_id], ['data-pjax' => 0,'class'=>'btn btn-secondary','title'=>'Phone Numbers']);
                    },
                ]
            ],
        ],
    ]); ?>
</div>  
    <?php Pjax::end(); ?>
</div>


