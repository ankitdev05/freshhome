<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\models\OrderStatus;
$all_status = ArrayHelper::map(OrderStatus::find()->all(), 'order_status_id', 'name');
?>
<h1><?= Html::encode('Customer Orders Report') ?></h1>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'user_id',
            'filter'=>false,
            'value'=>function($data){
                if(!empty($data->user->userDetail) && !empty($data->user->userDetail['name']))
                    return $data->user->userDetail['name'];
                return $data->user->name;
            }
        ],
        [
            'label'=>'Email address',
            'filter'=>false,
            'value'=>function($data){
                return $data->user->email;
            }
        ],
        [
            'label'=>'No of orders',
            'value'=>function($data){
                return $data->totalOrders;
            }

        ],
        [
            'label'=>'No. Products',
            'value'=>function($data){
                return $data->totalOrderProducts;
            }
        ],
        [
            'label'=>'Total',
            'value'=>function($data){
                return $data->currency_code.' '.Yii::$app->formatter->format($data->totalPrice,['decimal',2]);
            }
        ],


    ],
    'pager' => [
        'pageCssClass' => 'page-item',
        'disabledPageCssClass' => 'page-link disabled',
        'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
        'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
        'linkOptions' => ['class' => 'page-link'],
    ]
]); ?>
<?php Pjax::end(); ?>
