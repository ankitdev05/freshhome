<?php
use yii\helpers\Url;
use yii\bootstrap4\Html;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\OrderStatus;
$all_status = ArrayHelper::map(OrderStatus::find()->all(), 'order_status_id', 'name');

$start_date = isset($_GET['filters']['start_date']) ? trim($_GET['filters']['start_date']) : date('m/01/Y');
$end_date = isset($_GET['filters']['end_date']) ? trim($_GET['filters']['end_date']) : date('m/d/Y');
$group_by = isset($_GET['filters']['group_by']) ? trim($_GET['filters']['group_by']) : 'week';
$status = isset($_GET['filters']['status']) ? trim($_GET['filters']['status']) : '';

$_GET['filters']['start_date']  = $start_date;
$_GET['filters']['end_date']    = $end_date;
$_GET['filters']['group_by']    = $group_by;
$_GET['filters']['status']      = $status;
?>
<h1><?= Html::encode('Sales Report') ?></h1>
<div class="sales-report">
    <?php $form = ActiveForm::begin(['method' => 'get','action'=>Url::to(['reports/index','type'=>'sales_report'],true)]); ?>
    <div class="row mb-3">
        <div class="col-sm-3">
            <?= '<label class="control-label" for="start_date">Start Date</label>' ?>
            <?= DatePicker::widget([
                'name' => 'filters[start_date]',
                'id' => 'start_date',
                'value'=>$start_date,
                'options' => ['placeholder' => 'Start date ...'],
                'pluginOptions' => [
                    'autoclose'=>true
                ]
            ]); ?>
        </div>
        <div class="col-sm-3">
            <?= '<label class="control-label" for="end_date">End Date</label>' ?>
            <?= DatePicker::widget([
                'name' => 'filters[end_date]',
                'id' => 'end_date',
                'value'=>$end_date,
                'options' => ['placeholder' => 'End date ...'],
                'pluginOptions' => [
                    'autoclose'=>true
                ]
            ]); ?>
        </div>
        <div class="col-sm-2">
            <?= '<label class="control-label" for="group_by">Group By</label>' ?>
            <?=  Select2::widget([
                'name' => 'filters[group_by]',
                'id' => 'group_by',
                'value'=>$group_by,
                'data' => ['years'=>'Years','month'=>'Months','week'=>'Weeks','day'=>'Days'],

            ]); ?>
        </div>
        <div class="col-sm-2">
            <?= '<label class="control-label" for="status">Status</label>' ?>
            <?=  Select2::widget([
                'name' => 'filters[status]',
                'id' => 'status',
                'value'=>$status,
                'options' => ['placeholder' => 'All Statuses'],
                'data' => $all_status,
                'pluginOptions' => [
                    'allowClear'=>true,
                ]
            ]); ?>
        </div>
        <div class="col-sm-2 mt-4 pt-2">
            <?= Html::submitButton(Yii::t('app', 'Filter'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>