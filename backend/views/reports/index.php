<?php
use yii\helpers\Url;
use yii\bootstrap4\Html;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;

$this->title = Yii::t('app', 'Reports');
$this->params['breadcrumbs'][] = $this->title;
$data['customer_order'] = 'Customer orders report';
$data['sales_report']   = 'Sales report';
?>
<div class="reports-index">



    <h3><?= Html::encode($this->title) ?></h3>
    <?php $form = ActiveForm::begin(['method' => 'get','action'=>Url::to(['reports/index'],true)]); ?>
    <div class="row">
    <div class="col-sm-4">
        <?=  Select2::widget([
            'name' => 'type',
            'data' => $data,
            'value' => isset($_GET['type']) ? $_GET['type'] : 'customer_order',
        ]); ?>
    </div>
        <div class="col-sm-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="w-100 mt-3"></div>
        <div class="col-sm-12">
        <?= $html ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
