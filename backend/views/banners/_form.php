<?php

use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use himiklab\thumbnail\EasyThumbnailImage;
use kartik\file\FileInput;
use common\models\MainCategories;

/* @var $this yii\web\View */
/* @var $model common\models\Banners */
/* @var $form yii\widgets\ActiveForm */
$imageSrc= '';
$delUrl= '';
if(!empty($model->image)){
    $image=EasyThumbnailImage::thumbnailFileUrl(
        "../../frontend/web/uploads/".$model->image,200,200,EasyThumbnailImage::THUMBNAIL_INSET
    );
    $imageSrc	= [Html::img($image, ['class'=>'file-preview-image', 'alt'=>$model->name, 'title'=>$model->name])];
    $delUrl=[['caption'=>$model->name]];
}
$categories = ArrayHelper::map(MainCategories::find()->asArray()->all(), 'id', 'name');

?>

<div class="banners-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($model, 'image')->widget(FileInput::classname(),
            [
                'options'=>['accept'=>'image/*'],
                'pluginOptions'=>
                    [
                        'allowedFileExtensions'=>['jpg','gif','png'],
                        'overwriteInitial'=>true,
                        'initialPreview'=>$imageSrc,
                        'initialPreviewConfig'=>$delUrl
                    ],
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'type')->widget(Select2::classname(), [
                'data' => ['user' => 'User', 'supplier' => 'Supplier']])
            ?>
        </div>
        <div class="col-sm-6" id="categories" style="display:none;">
            <?= $form->field($model, 'categories')->widget(Select2::classname(), [
                'data' => $categories,
                'options' => ['placeholder' => 'Please select...']
                ])
            ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => ['active' => 'Active', 'inactive' => 'Inactive'],])
            ?>
        </div>

    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs('
    if($("#banners-type").val()=="supplier")
        $("#categories").show()
     $("#banners-type").on("change",function(){
        var value = $(this).val();
        if(value=="supplier")
            $("#categories").show()
        if(value=="user"){
            $("#categories").hide()
            $("#banners-categories").val("").change();
        }
     })   
');