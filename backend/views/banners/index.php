<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use himiklab\thumbnail\EasyThumbnailImage;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BannersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Banners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banners-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Banners'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
               ['attribute' => 'image',
                   'format' => 'raw',
                   'value' => function($model){
                       $image=EasyThumbnailImage::thumbnailFileUrl(
                           "../../frontend/web/uploads/".$model->image,150,150,EasyThumbnailImage::THUMBNAIL_INSET
                       );
                       return Html::img($image, ['class'=>'file-preview-image', 'alt'=>$model->name, 'title'=>$model->name]);
                   }
               ],
                [
                    'attribute'=>'status',
                    'filter'=>Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'status',
                        'data' => ['active' => 'Active', 'inactive' => 'Inactive'],
                        'options' => [
                            'placeholder' => 'Please select ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]),
                ],
                'created_at:datetime',
                //'updated_at',

                ['class' => 'yii\grid\FaActionColumn','template' => '{update}'],
            ],
            'pager' => [
                'pageCssClass' => 'page-item',
                'disabledPageCssClass' => 'page-link disabled',
                'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
                'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
                'linkOptions' => ['class' => 'page-link'],
            ]
        ]); ?>
    </div>
    <?php Pjax::end(); ?>

</div>
