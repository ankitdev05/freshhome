<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use common\models\Language;
use common\models\Countries;
use common\models\CityDescription;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;

$all_countries = ArrayHelper::map(Countries::find()->all(), 'country_id', 'name');
$languages	    = Language::find()->where(['status'=>1])->all();
if(!empty($model->id)){
	$descriptions	= CityDescription::find()->where(['city_id'=>$model->id])->all();
	foreach($descriptions as $description){
		$model->city_name[$description->language_id] = $description->city_name;
	}
}
?>

<div class="cuisine-form">
    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'country_id')->textInput()->widget(Select2::classname(), [
                'data' => $all_countries,
                'options' => ['placeholder' => 'Please select ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])
            ?>
        </div>
        <div class="col-sm-4">
        <?= $form->field($model, 'state_id')->widget(DepDrop::classname(), [
            'type'		=> DepDrop::TYPE_SELECT2,
            'pluginOptions'=>[
                'initialize'  => (!empty($model->state_id)) ? true : false,
                'depends'=>['city-country_id'],
                'placeholder' => 'Select...',
                 'url'=>(!empty($model->state_id)) ? Url::to(['/countries/state','sub_cat'=>$model->state_id]) : Url::to(['/countries/state'])
            ]
        ]); ?>
        </div>
        <div class="w-100"></div>
		<?php foreach($languages as $language){	?>
			<div class="col-sm-4">
				<?= $form->field($model, "city_name[{$language->language_id}]")->textInput(['maxlength' => true])->label(Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' (city name)') ?>
			</div>
		<?php } ?>	
        <div class="w-100"></div>
		<div class="col-sm-2">
			<?= $form->field($model, 'status')->widget(Select2::classname(), [
					'data' => ['active' => 'Active', 'inactive' => 'Inactive'],
					
				])
			?>
		</div>
		
		<div class="col-sm-12">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
		</div>
	</div>
	
    <?php ActiveForm::end(); ?>
</div>
<div class="clearfix"></div>
<hr/>