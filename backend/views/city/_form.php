<?php
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;

?>

<div class="cuisine-form">
    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-sm-4">
			<?= $form->field($model, 'city_name')->textInput(['maxlength' => true]) ?>
		</div>
		
		<div class="col-sm-4">
			<?= $form->field($model, 'status')->widget(Select2::classname(), [
					'data' => ['active' => 'Active', 'inactive' => 'Inactive'],
					
				])
			?>
		</div>
		
		<div class="col-sm-4" style="margin-top:28px;">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
		</div>
	</div>
	
    <?php ActiveForm::end(); ?>
</div>
<div class="clearfix"></div>
<hr/>