<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'All Suppliers'), 'url' => ['/user/supplier']];
$this->title = $user_info->name.'\'s Schedules';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="viewschedule-index table-responsive">
	<?php Pjax::begin(); ?>
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			'schedule_date',
			'start_time',
            'end_time',
            'choices',
			[
				'attribute'=>'supplier_id',
				'filter'=>false,
				'header'=>'items',
				'format'=>'raw',
				'value'=>function($data){
					return $data->menuitems;
				}
			],
            
        ],
    ]); ?>
	<?php Pjax::end(); ?>
</div>
