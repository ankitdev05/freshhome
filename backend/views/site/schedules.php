<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\models\City;

$this->title = 'Schedules';
$this->params['breadcrumbs'][] = $this->title;

$all_cities = ArrayHelper::map(City::find()->where(['status'=>'active'])->all(), 'id', 'city_name');
?>

<div class="schedules-index">
	<h3>All <?= Html::encode($this->title) ?></h3>
	<?php Pjax::begin(); ?>
<div class="table-responsive">	
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			'share_id',
			'name',
            'email:email',
			[
				'attribute'=>'status',
				'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => [10=>'Active',0=>'In active'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
					'pluginOptions' => [
                        'allowClear' => true
                    ],
                    
                ]),
				'value'=>function($data){
					return $data->status === 10 ? 'Active' : 'In active';
				}
			],
            [
				'attribute'=>'city',
				'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'city',
                    'data' => $all_cities,
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
					'pluginOptions' => [
                        'allowClear' => true
                    ],
                    
                ]),
				'value'=>function($data){
					return !empty($data->city) ? $data->cityInfo->city_name : null;
				}
			],
            [
				'class' => 'yii\grid\FaActionColumn',
				'template'=>'{schedule}',
				'buttons'=>[
					'schedule'=>function($url, $model, $key){
						return Html::a(Html::tag('i','',['class'=>'fas fa-calendar-alt']), ['site/viewschedule','id' => $model->id], ['data-pjax' => 0,'title'=>'View schedule','class'=>'btn btn-primary']);
					}
				]
				
			],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>
</div>	
	<?php Pjax::end(); ?>
</div>
