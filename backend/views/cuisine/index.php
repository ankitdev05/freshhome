<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use himiklab\thumbnail\EasyThumbnailImage;

$this->title = Yii::t('app', 'Cuisine');
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="cuisine-index">

    <h3>Create <?= Html::encode($this->title) ?></h3>
    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'attribute'=>'cuisine_name',
				'format'=>'raw',
				'value'=>function($data){
					$descriptions	= $data->cuisineDescription;
					$html = '';
					foreach($descriptions as $description){
						$language	= $description->language;
						$html .= '<span class="lang_'.$language->language_id.'">'.Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' '.$description->cuisine_name.'</span><br/>';
					}
					return $html;
				}
			],
            [
                'attribute' => 'image',
                'format' => 'image',
                'value' => function ($data){
                    if(!empty($data->image)){
                        return EasyThumbnailImage::thumbnailFileUrl(
                            "../../frontend/web/uploads/".$data->image,100,100,EasyThumbnailImage::THUMBNAIL_INSET
                        );
                    }
                }
            ],
			[
				'attribute'=>'status',
				'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => ['active' => 'Active', 'inactive' => 'Inactive'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
			],
            [
				'class' => 'yii\grid\FaActionColumn',
				'template'=>'{update}'
			],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>
</div>	
    <?php Pjax::end(); ?>
</div>
