<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cuisine */

$this->title = Yii::t('app', 'Update Cuisine:', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuisines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cuisine-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_multi_lang_form', [
        'model' => $model,
    ]) ?>

</div>
