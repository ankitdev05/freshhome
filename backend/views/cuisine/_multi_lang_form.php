<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

use kartik\select2\Select2;
use common\models\Language;
use common\models\CuisineDescription;
use kartik\file\FileInput;
use himiklab\thumbnail\EasyThumbnailImage;

$languages	    = Language::find()->where(['status'=>1])->all();
if(!empty($model->id)){
	$descriptions	= CuisineDescription::find()->where(['cuisine_id'=>$model->id])->all();
	foreach($descriptions as $description){
		$model->cuisine_name[$description->language_id] = $description->cuisine_name;
	}
}
$imageSrc = '';
$delUrl = '';
if(!empty($model->image)){
    $image= EasyThumbnailImage::thumbnailFileUrl(
        "../../frontend/web/uploads/".$model->image,200,200,EasyThumbnailImage::THUMBNAIL_INSET
    );
    $imageSrc	= [Html::img($image, ['class'=>'file-preview-image', 'alt'=>'', 'title'=>''])];
    $delUrl=[['caption'=> '','url'=>yii::$app->request->baseUrl.'/cuisine/delete-image?id='.$model->id]];
}
?>

<div class="cuisine-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	<div class="row">
	<?php foreach($languages as $language){	?>
		<div class="col-sm-4">
			<?= $form->field($model, "cuisine_name[{$language->language_id}]")->textInput(['maxlength' => true])->label(Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' (cuisine name)') ?>
		</div>
	<?php } ?>

        <div class="col-sm-12">
            <?= $form->field($model, 'image')->widget(FileInput::classname(),
                [
                    'options'=>['accept'=>'image/*'],
                    'pluginOptions'=>
                        [
                            'allowedFileExtensions'=>['jpg','gif','png'],
                            'overwriteInitial'=>true,
                            'initialPreview'=>$imageSrc,
                            'initialPreviewConfig'=>$delUrl
                        ],
                ]) ?>
        </div>
		<div class="col-sm-2">
			<?= $form->field($model, 'status')->widget(Select2::classname(), [
					'data' => ['active' => 'Active', 'inactive' => 'Inactive']
				])
			?>
		</div>
		
		<div class="col-sm-12">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
		</div>
	</div>
	
    <?php ActiveForm::end(); ?>
</div>
<div class="clearfix"></div>
<hr/>