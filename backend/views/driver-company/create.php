<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DriverCompany */

$this->title = Yii::t('app', 'Create Driver Company');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Driver Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
