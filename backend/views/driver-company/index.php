<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DriverCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Driver Companies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-company-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Driver Company'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'company_name',
            'company_email:email',
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class' => 'yii\grid\FaActionColumn',
                'template'=>'{view} {update} {phone_numbers}',
                'buttons'=>[
                    'phone_numbers'=>function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'fas fa-phone']), ['/driver-company-numbers','id' => $model->company_id], ['data-pjax' => 0,'class'=>'btn btn-secondary','title'=>'Phone Numbers']);
                    },
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
