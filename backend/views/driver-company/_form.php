<?php

use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\DriverCompany */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driver-company-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'company_email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'password')->passwordInput() ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($model, 'file')->widget(FileInput::classname(),
                [
                    'pluginOptions'=>
                        [
                            'showCaption' => true,
                            'allowedFileExtensions'=>['xlsx'],
                            'overwriteInitial'=>true
                        ],
                ])->hint("(".Html::a('Download sample file',Url::to(Yii::$app->request->baseUrl.'/sample.xlsx',true),['target'=>'_blank']).")") ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
