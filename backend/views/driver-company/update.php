<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DriverCompany */

$this->title = Yii::t('app', 'Update Driver Company: {name}', [
    'name' => $model->company_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Driver Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->company_name, 'url' => ['view', 'id' => $model->company_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="driver-company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
