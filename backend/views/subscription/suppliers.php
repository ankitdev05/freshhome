<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\models\MainCategories;
use kartik\bs4dropdown\Dropdown;
use kartik\export\ExportMenu;

$this->title = Yii::t('app', 'Suppliers Subscription');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        $gridColumns = [
            'name',
            'username',
            'email',
            'status',
            'supplier_type',
            'share_id',
            'subscription_start',
            'subscription_end'
        ];

        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns'=>$gridColumns
        ]);
    ?>

    <?php Pjax::begin(); ?>
    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'username',
                'email:email',
                [
                    'attribute'=>'status',
                    'filter'=>Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'status',
                        'data' => [10=>'Active',0=>'In active'],
                        'options' => [
                            'placeholder' => 'Please select ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],

                    ]),
                    'value'=>function($data){
                        return $data->status === 10 ? 'Active' : 'In active';
                    }
                ],
                [
                    'attribute'=>'supplier_type',
                    'filter'=>Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'supplier_type',
                        'data' => ArrayHelper::map(MainCategories::find()->where(['status' => 1])->asArray()->all(), 'id', 'name'),
                        'options' => [
                            'placeholder' => 'Please select ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],

                    ]),
                    'value'=>function($data){
                        return $data->suppType->name;
                    }
                ],

                'share_id',
                'subscription_start:datetime',
                'subscription_end:datetime',

                [
                    'class' => 'yii\grid\FaActionColumn',
                    'template'=>'{block}',
                    'buttons' =>
                        [
                            'block' => function($url, $model, $key){
                                if($model->status==10)
                                    return Html::a(Html::tag('i',' Block',['class'=>'fas fa-ban']), Url::to(['subscription/block','id' => $model->id]), ['class'=>'btn btn-sm btn-danger','data-pjax' => 0,'data-method' => 'POST','data-confirm' => 'Are you sure want to block this user ?']);
                                if($model->status==9)
                                    return Html::a(Html::tag('i',' Unblock',['class'=>'fas fa-unlock']), Url::to(['subscription/unblock','id' => $model->id]), ['class'=>'btn btn-sm btn-success','data-pjax' => 0,'data-method' => 'POST','data-confirm' => 'Are you sure want to unblock this user ?']);
                        }
                    ]
                ],
            ],
            'pager' => [
                'pageCssClass' => 'page-item',
                'disabledPageCssClass' => 'page-link disabled',
                'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
                'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
                'linkOptions' => ['class' => 'page-link'],
            ]
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>


