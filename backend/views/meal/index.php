<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use himiklab\thumbnail\EasyThumbnailImage;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Meal');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meal-index">

    <h3>Create <?= Html::encode($this->title) ?></h3>
    <?php Pjax::begin(); ?>
    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<div class="table-responsive">
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				[
					'attribute'=>'meal_name',
					'format'=>'raw',
					'value'=>function($data){
						$descriptions	= $data->mealDescription;
						$html = '';
						foreach($descriptions as $description){
							$language	= $description->language;
							$html .= '<span class="lang_'.$language->language_id.'">'.Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' '.$description->meal_name.'</span><br/>';
						}
						return $html;
					}
				],
                [
                    'attribute' => 'image',
                    'format' => 'image',
                    'value' => function ($data){
                        if(!empty($data->image)){
                            return EasyThumbnailImage::thumbnailFileUrl(
                                "../../frontend/web/uploads/".$data->image,100,100,EasyThumbnailImage::THUMBNAIL_INSET
                            );
                        }
                    }
                ],
				[
					'attribute'=>'status',
					'filter'=>Select2::widget([
						'model' => $searchModel,
						'attribute' => 'status',
						'data' => ['active' => 'Active', 'inactive' => 'Inactive'],
						'options' => [
							'placeholder' => 'Please select ...',
						],
						'pluginOptions' => [
							'allowClear' => true
						],
					]),
				],
				'created_at:datetime',
				[
					'class' => 'yii\grid\FaActionColumn',
					'template'=>'{update}',
				],
			],
            'pager' => [
                'pageCssClass' => 'page-item',
                'disabledPageCssClass' => 'page-link disabled',
                'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
                'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
                'linkOptions' => ['class' => 'page-link'],
            ]
		]); ?>
	</div>
	<?php Pjax::end(); ?>
</div>
