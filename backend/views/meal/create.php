<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Meal */

$this->title = Yii::t('app', 'Create Meal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Meals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_multi_lang_form', [
        'model' => $model,
    ]) ?>

</div>
