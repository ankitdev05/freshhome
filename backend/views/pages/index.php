<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Pages'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
               'attribute' => 'title',
                'format'=>'raw',
                'value'=>function($data){
                    $descriptions	= $data->pageDescriptions;
                    $html = '';
                    foreach($descriptions as $description){
                        $language	= $description->language;
                        $html .= '<span class="lang_'.$language->language_id.'">'.Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' '.$description->title.'</span><br/>';
                    }
                    return $html;
                }
            ],
            [
                'attribute'=>'status',
                'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => [0=>'inactive','1'=>'Active'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'value'=>function($data){
                    return $data->status == 1 ? 'Active' : 'Inactive';
                }
            ],
            //'meta_description:ntext',
            //'meta_keyword:ntext',
            //'slug',
            //'page_order',
            //'display_menu',
            //'status',
            //'created_by',
            //'last_updated_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\FaActionColumn','template' => '{update}'],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>
    <?php Pjax::end(); ?>
</div>
