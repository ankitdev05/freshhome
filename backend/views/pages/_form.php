<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Tabs;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;

use kartik\select2\Select2;
use common\models\Pages;
use common\models\PagesDescription;
use common\models\Language;

$items = [];
$languages	    = Language::find()->where(['status'=>1])->asArray()->all();

if(!empty($model->page_id)){
    $descriptions	= PagesDescription::find()->where(['page_id'=>$model->page_id])->asArray()->all();
    foreach($descriptions as $description){

        $model->title[$description['language_id']] = $description['title'];
        $model->description[$description['language_id']] = $description['description'];
        $model->meta_title[$description['language_id']] = $description['meta_title'];
        $model->meta_description[$description['language_id']] = $description['meta_description'];
        $model->meta_keyword[$description['language_id']] = $description['meta_keyword'];
    }
}

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */

$all_pages 	= ArrayHelper::map(Pages::find()->joinWith('pageDescription as pd')->where(['is','parent_id',null])->all(), 'page_id', function($data){
    return $data->pageDescription->title;
});
if(empty($model->page_order))
    $model->page_order = 1;
?>

<div class="pages-form">
    <?php
        $form = ActiveForm::begin();
        foreach($languages as $k => $language){
            $content = $this->render('_lang_form',['model' => $model,'form' => $form,'language' => $language]);

            $items[] = ['label' => Html::img(Yii::$app->request->baseUrl.'/../images/'.$language['image'],['width' => 25]).' '.$language['name'],'content' => $content];
        }
    ?>

<?php
    echo Tabs::widget([
            'encodeLabels' => false,
        'items' => $items
    ]);
?><div class="row mt-3">
    <div class="col-sm-4">
        <?= $form->field($model, 'parent_id')->textInput()->widget(Select2::classname(), [
            'data' => $all_pages,
            'options' => ['placeholder' => 'Please select ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-4">
        <?= $form->field($model, 'page_type')->widget(Select2::classname(), [
            'data' => [ 0 => 'Normal Page',1 => 'Module'],
        ])
        ?>
    </div>
    <div class="w-100"></div>
    <div class="col-sm-4">
        <?= $form->field($model, 'show_on_footer')->checkbox() ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'show_on_header')->checkbox() ?>
    </div>
    <div class="w-100"></div>
    <div class="col-sm-4">
        <?= $form->field($model, 'status')->widget(Select2::classname(), [
            'data' => [1 => 'Active', 0 => 'Inactive'],
        ])
        ?>
    </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'page_order')->textInput(['type' => 'number']) ?>
        </div>
</div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs('
    $("#pages-title-1").on("keyup change",function(){
        var Text = $(this).val();
        Text = Text.toLowerCase();
        Text = Text.replace(/[^a-zA-Z0-9]+/g,\'-\');
        $("#pages-slug").val(Text);        
    });
');