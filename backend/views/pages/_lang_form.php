<?php
use dosamigos\tinymce\TinyMce;
?>
<div class="row">
    <div class="col-sm-4">
        <?= $form->field($model, "title[{$language['language_id']}]")->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($model, "description[{$language['language_id']}]")->widget(TinyMce::className(), [
            'options' => ['rows' => 12],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ],
                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            ]
        ]);?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, "meta_title[{$language['language_id']}]")->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, "meta_description[{$language['language_id']}]")->textarea(['rows' => 3]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, "meta_keyword[{$language['language_id']}]")->textarea(['rows' => 3]) ?>
    </div>
</div>