<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use common\models\Modules;
use yii\helpers\ArrayHelper;
$menus	= Modules::find()->where(['and',['status'=>1]])->orderBy('sort_order ASC')->all();

$all_menus = ArrayHelper::map(Modules::find()->where(['and',['is','parent_id',null],['status'=>1]])->orderBy('sort_order ASC')->all(), 'id', 'config_name');
if(!empty($model->permission)){
    $all_permissions = unserialize($model->permission);
$model->permission = $all_permissions;
}
?>

<div class="user-role-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-6">
           <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-12">
         <?= $form->field($model, 'permission')->checkboxList($all_menus) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    <div class="mb-3">
        <a href="javascript:void(0)" onClick='$(".custom-control-input").prop("checked", true);' class="btn btn-warning">Select All</a>&nbsp;
        <a href="javascript:void(0)" onClick='$(".custom-control-input").prop("checked", false);' class="btn btn-warning">Deselect All</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
