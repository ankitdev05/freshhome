<?php
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use common\models\Category;
use common\models\CategoryDescription;
use common\models\Language;
use kartik\file\FileInput;
use himiklab\thumbnail\EasyThumbnailImage;

$languages      = Language::find()->where(['status'=>1])->all();

$all_cats = [];
$categories = Category::getAllCategories($cat_id);
foreach ($categories as $category) {
    $all_cats[$category['category_id']] = strip_tags(html_entity_decode($category['name'], ENT_QUOTES, 'UTF-8'));
}


$imageSrc = '';
$delUrl = '';
if(!empty($model->image)){
    $image= EasyThumbnailImage::thumbnailFileUrl(
        "../../frontend/web/uploads/".$model->image,200,200,EasyThumbnailImage::THUMBNAIL_INSET
    );
    
    $imageSrc   = [Html::img($image, ['class'=>'file-preview-image', 'alt'=>$model->name, 'title'=>$model->name])];
    $delUrl=[['caption'=> $model->name,'url'=>yii::$app->request->baseUrl.'/category/delete-image?id='.$model->category_id]];
}



if(!empty($model->category_id)){
  $descriptions = CategoryDescription::find()->where(['category_id'=>$model->category_id])->all(); 
  foreach($descriptions as $description){

    $model->name[$description->language_id] = $description->category_name;
  // echo "<pre>";
  //   print_r($model);die;
  }  
} 

// echo "<pre>";
// print_r($model);die;
?>
<style type="text/css">

</style>
<div class="cuisine-form">
    <?php  $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">

           <div class="col-sm-6">
  <?= $form->field($model, 'home_category_id')->textInput()->widget(Select2::classname(), [
                'data' =>$all_home_category,
                'options' => ['placeholder' => 'Please select ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ], 
            ])->label('Select Home Category'); ?>
           </div>

            <div class="col-sm-6">
            
     <ul>

<?php if(!$model->isNewRecord){
 $CategoryDescription = CategoryDescription::find()->where(['category_id'=>$model['category_id']])->andWhere(['language_id'=>1])->one();

 ?>
    <li>
    <button type="button" class="btn btn-success"  onclick="openform(<?= $model['category_id'] ?>)" ><?= $CategoryDescription['category_name'] ?>   <span class="glyphicon glyphicon-plus"></span></button>
        <ul>
        <?php
     
       $parent_id =$model['category_id'];
        $submenudata = Category::find()->where(['parent_id'=>$parent_id])->all();
      


        foreach ($submenudata as $key=>$value) {
           $category_name1 = CategoryDescription::find()->select('category_name')->where(['category_id'=>$value['category_id']])->andWhere(['language_id'=>1])->one();
        
           // print_r($category_name1['category_name']);
          ?>
          <li style="margin-top: 5px;" >
          <button type="button" class="btn btn-primary" onclick="openform(<?= $value['category_id'] ?>)"><?= $category_name1['category_name'] ?>  <span class="glyphicon glyphicon-plus"></span></button>
          </li> 
        

    <?php   
        $submenus = Category::find()->where(['parent_id'=>$value['category_id']])->all();
       

      foreach ($submenus as $key1=>$value1) {  
         $category_name2 = CategoryDescription::find()->select('category_name')->where(['category_id'=>$value1['category_id']])->andWhere(['language_id'=>1])->one();
        
         // echo "<pre>";
         // print_r($category_name2);
       ?>
              <ul>
              <li style="margin-top: 5px;" >
          <button type="button" class="btn btn-info" onclick="openform(<?= $value1['category_id'] ?>)"><?= $category_name2['category_name'] ?>  <span class="glyphicon glyphicon-plus"></span></button>
          </li> 




     <?php    
        $submenu3 = Category::find()->where(['parent_id'=>$value1['category_id']])->all();
      foreach ($submenu3 as $key2=>$value2) {
    
      $category_name3 = CategoryDescription::find()->select('category_name')->where(['category_id'=>$value2['category_id']])->andWhere(['language_id'=>1])->one();

       ?>
                <ul>
                   <li style="margin-top: 5px;" >
     

             <button type="button" class="btn btn-warning" onclick="openform(<?= $value2['category_id'] ?>)"><?= $category_name3['category_name'] ?>   <span class="glyphicon glyphicon-plus"></span></button> 


          </li> 
               
 
        
             <?php    
        $submenu4 = Category::find()->where(['parent_id'=>$value2['category_id']])->all();
      
      foreach ($submenu4 as $key3=>$value3) { 

        $category_name4 = CategoryDescription::find()->select('category_name')->where(['category_id'=>$value3['category_id']])->andWhere(['language_id'=>1])->one();


       ?>
        
              <ul>
                <li style="margin-top: 5px;" >
          <button type="button" class="btn btn-danger" ><?= $category_name4['category_name'] ?> </button> 
          </li> 
            
            </ul>
           <?php } ?>
 
                </ul>
          <?php } ?>


          </ul>
         <?php } ?>
      
         <?php } ?>
        </ul>

      </li>
<?php } ?>

     

    </ul>

           </div>

    </div>

      <div class="row">


  <?php foreach($languages as $language){ ?>   
    <div class="col-sm-6">
      <?= $form->field($model, "name[{$language->language_id}]")->textInput(['maxlength' => true])->label(Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' (Name)') ?>
    </div>
     <div class="col-sm-6">
        </div>
  <?php } ?>



    </div>
    
    <div class="row">
    
    <div class="col-sm-6">
            <?= $form->field($model, 'active')->widget(Select2::classname(), [
                    'data' => [1 => 'Active', 0 => 'Inactive'],
                    
                ])
            ?>
        </div>
         <div class="col-sm-6">
         </div>


    </div>

    <div class="row">
    
      <div class="col-sm-6">
            <?= $form->field($model, 'image')->widget(FileInput::classname(),
                [
                    'options'=>['accept'=>'image/*'],
                    'pluginOptions'=>
                        [
                            'allowedFileExtensions'=>['jpg','gif','png'],
                            'overwriteInitial'=>true,
                            'initialPreview'=>$imageSrc,
                            'initialPreviewConfig'=>$delUrl
                        ],
                ]) ?>
        </div>   
      <div class="col-sm-6">
      
      </div>

    
    </div>

 

    <div class="row">

        <div class="w-100"></div>
        <div class="col-sm-4" style="margin-top:28px;">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add parent category') : Yii::t('app', 'Update parent category'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
  
   <?php ActiveForm::end(); ?>
  

 <?php  $form = ActiveForm::begin(['action' => ['category/saveform'], 'options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="cuisine-form">
   
<?php if(!$model->isNewRecord){  ?>


  <div style="display: none;" id="subcategoryform">
   <input type="hidden" name="parentid" id="parentid" value="<?= $model['category_id'] ?>">
   <input type="hidden" name="sub_parentid" id="sub_parentid" value=""> 
   <input type="hidden" name="home_category_id" id="home_category_id" value=""> 
   

   <input type="hidden" name="cat_id" id="cat_id" value="<?= $model['cat_id'] ?>">
     <div class="row" style="margin-top:28px;">
         <?php foreach($languages as $language){ ?>
    <div class="col-sm-6">
      <?= $form->field($model, "name1[{$language->language_id}]")->textInput(['maxlength' => true])->label(Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' (Name)') ?>
    </div>
      
  <?php } ?>  
     </div>
  
     <div class="row">
        <div class="col-sm-6"> 
            <?= $form->field($model, 'active1')->widget(Select2::classname(), [
                    'data' => [1 => 'Active', 0 => 'Inactive'],
                    
                ])->label('Status');
            ?>
        </div>
     </div>


        <div class="row">
    
      <div class="col-sm-12">
            <?= $form->field($model, 'image1')->widget(FileInput::classname(),
                [
                    'options'=>['accept'=>'image/*'],
                    'pluginOptions'=>
                        [
                            'allowedFileExtensions'=>['jpg','gif','png'],
                            'overwriteInitial'=>true,
                            // 'initialPreview'=>$imageSrc,
                            // 'initialPreviewConfig'=>$delUrl
                        ],
                ])->label('Image'); ?>
        </div>   
    

    
    </div>


      <div class="row">

        <div class="w-100"></div>
        <div class="col-sm-4" style="margin-top:28px;">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add parent category') : Yii::t('app', 'Create'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
  </div>
   <?php } ?>

    <?php ActiveForm::end(); ?>
  </div>
</div>   
<div class="clearfix"></div>
<hr/>


<script type="text/javascript">
 function openform(val){
 var home_category_id = $("#category-home_category_id").val();
   $("#subcategoryform").show();
   $("#sub_parentid").val(val); 
   $("#home_category_id").val(home_category_id);

     $('html, body').animate({
    scrollTop: $("#subcategoryform").offset().top
   },1000);
     
  }

</script>