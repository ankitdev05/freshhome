<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\CategoryDescription;  

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['/main-categories']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $main_cat->name), 'url' => ['index','cat_id' => $cat_id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->category_id,'cat_id' => $cat_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->category_id,'cat_id' => $cat_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'category_id',
            [
                'attribute'=>'name',
                'format'=>'raw',
                'value'=>function($model){
                   $descriptions = CategoryDescription::findAll(['category_id'=>$model->category_id]);
                   // echo "<pre>";
                   // print_r($descriptions);die;  
                    $html = '';
                    foreach($descriptions as $description){
                        $language   = $description->language;
                        $html .= '<span class="lang_'.$language->language_id.'">'.Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' '.$description->category_name.'</span><br/>';
                    }
                    return $html; 
                }
            ],
            [
                'attribute' => 'active',
                'value' => function($data){
                    return $data->active==1 ? 'Active' : 'Inactive';
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
