<?php
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use common\models\Category;
use kartik\file\FileInput;
use himiklab\thumbnail\EasyThumbnailImage;

$all_cats = [];
$categories = Category::getAllCategories($cat_id);
foreach ($categories as $category) {
    $all_cats[$category['category_id']] = strip_tags(html_entity_decode($category['name'], ENT_QUOTES, 'UTF-8'));
}
$imageSrc = '';
$delUrl = '';
if(!empty($model->image)){
    $image= EasyThumbnailImage::thumbnailFileUrl(
        "../../frontend/web/uploads/".$model->image,200,200,EasyThumbnailImage::THUMBNAIL_INSET
    );
    $imageSrc	= [Html::img($image, ['class'=>'file-preview-image', 'alt'=>$model->name, 'title'=>$model->name])];
    $delUrl=[['caption'=> $model->name,'url'=>yii::$app->request->baseUrl.'/category/delete-image?id='.$model->category_id]];
}
?>

<div class="cuisine-form">
    <?php  $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	<div class="row">
		<div class="col-sm-4">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>
        <div class="col-sm-12">
            <?= $form->field($model, 'image')->widget(FileInput::classname(),
                [
                    'options'=>['accept'=>'image/*'],
                    'pluginOptions'=>
                        [
                            'allowedFileExtensions'=>['jpg','gif','png'],
                            'overwriteInitial'=>true,
                            'initialPreview'=>$imageSrc,
                            'initialPreviewConfig'=>$delUrl
                        ],
                ]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'parent_id')->textInput()->widget(Select2::classname(), [
                'data' =>$all_cats,
                'options' => ['placeholder' => 'Please select ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>

		<div class="col-sm-4">
			<?= $form->field($model, 'active')->widget(Select2::classname(), [
					'data' => [1 => 'Active', 0 => 'Inactive'],
					
				])
			?>
		</div>
        <div class="w-100"></div>
		<div class="col-sm-4" style="margin-top:28px;">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
		</div>
	</div>
	
    <?php ActiveForm::end(); ?>
</div>
<div class="clearfix"></div>
<hr/>