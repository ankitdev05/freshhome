<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\models\Category;
use himiklab\thumbnail\EasyThumbnailImage;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');

$this->params['breadcrumbs'][] = ['label' => $this->title,'url' => ['/main-categories']];
$this->params['breadcrumbs'][] = $main_cat->name;

$all_cats = [];
$categories = Category::getAllCategories($cat_id);
foreach ($categories as $category) {
    $all_cats[$category['category_id']] = strip_tags(html_entity_decode($category['name'], ENT_QUOTES, 'UTF-8'));
}
?>
<div class="category-index">
    <p>
        <?= Html::a(Yii::t('app', 'Create Category'), ['create','cat_id' => $cat_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'category_id',
//            [
//                'format' => 'raw',
//                'attribute' => 'parent_id',
//                'value' => function($data){
//                    $parent = $data->parent;
//                    if(!empty($parent))
//                        return $parent->name;
//                },
//                'filter'=>Select2::widget([
//                    'model' => $searchModel,
//                    'attribute' => 'parent_id',
//                    'data' => $all_cats,
//                    'options' => [
//                        'placeholder' => 'Please select ...',
//                    ],
//                    'pluginOptions' => [
//                        'allowClear' => true
//                    ],
//
//                ])
//            ],

            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($data){
                    $category = Category::getCategoryName($data->category_id);
                    return $category['name'];
                }
            ],
            [
                'attribute' => 'image',
                'format' => 'image',
                'value' => function ($data){
                    if(!empty($data->image)){
                        return EasyThumbnailImage::thumbnailFileUrl(
                            "../../frontend/web/uploads/".$data->image,100,100,EasyThumbnailImage::THUMBNAIL_INSET
                        );
                    }
                }
            ],
            [
                'attribute' => 'active',
                'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'active',
                    'data' => [1 => 'Active',0 => 'Inactive'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'value' => function($data){
                    return $data->active==1 ? 'Active' : 'Inactive';
                }
            ],
            //'created_by',
            //'created_at',
            //'updated_at',

            [
                'template'=>'{view} {update} {featured}',
                'class' => 'yii\grid\FaActionColumn',
                'buttons'=>[
                    'update'=>function($url, $model, $key) use ($cat_id){
                        return Html::a(Html::tag('i','',['class'=>'far fa-edit']), ['category/update','id' => $model->category_id,'cat_id'=>$cat_id], ['data-pjax' => 0,'class'=>'btn btn-secondary']);
                    },
                    'view'=>function($url, $model, $key) use ($cat_id){
                        return Html::a(Html::tag('i','',['class'=>'far fa-eye']), ['category/view','id' => $model->category_id,'cat_id'=>$cat_id], ['data-pjax' => 0,'class'=>'btn btn-secondary']);
                    },
                    'featured'=>function($url, $model, $key) use ($cat_id){
                        $title = 'Add to featured list';
                        $icon   = 'far';
                        $update = 1;
                        if($model->featured==1) {
                            $update = 0;
                            $icon   = 'fas text-danger';
                            $title = 'Remove from featured list';
                        }
                        return Html::a(Html::tag('i','',['class'=>$icon.' fa-star']),Url::to(['index','update'=>$update,'id'=>$model->category_id,'cat_id'=>$cat_id]), ['data-pjax' => 0,'alt'=>$title,'title'=>$title,'class'=>'btn btn-secondary']);
                    }
                ]
            ],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>

    <?php Pjax::end(); ?>

</div>
