<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = Yii::t('app', 'Create Category'); 
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['/main-categories']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $main_cat->name), 'url' => ['index','cat_id'=>$cat_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cat_id' => $cat_id,
        'all_home_category' => $all_home_category,
    ]) ?>

</div>
