<?php
use yii\helpers\Html;
use kartik\select2\Select2;
use common\models\Option;
use common\models\Language;
use common\models\CategoryDescription;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Products;
use unclead\multipleinput\MultipleInput;

$language_id = 1;
//$all_cats = ArrayHelper::map(Product::find()->joinWith('categoryName as on')->where(['on.language_id'=>$language_id])->andWhere(['and',['is','parent_id',null],['cat_id'=>$main_cat->id]])->all(), 'id', 'categoryName.category_name');
$all_cats = ArrayHelper::map(Product::find()->joinWith('categoryName as on')->where(['on.language_id'=>$language_id])->andWhere(['and',['cat_id'=>$main_cat->id]])->all(), 'id', 'categoryName.category_name');
$languages	    = Language::find()->where(['status'=>1])->all();

if(!empty($model->id)){
	$descriptions	= CategoryDescription::find()->where(['category_id'=>$model->id])->all();
	foreach($descriptions as $description){
		$model->category_name[$description->language_id] = $description->category_name;
	}
}
$all_options = ArrayHelper::map(Option::find()->joinWith('optionDescription as on')->where(['on.language_id'=>$language_id])->andWhere(['and',['status'=> 1]])->all(), 'option_id', 'optionDescription.name');

$category_options = $model->categoryToOptions;
if(!empty($category_options)){
    foreach($category_options as $category_option)
        $data[]= ['option_id'=>$category_option->option_id,'is_required'=>$category_option->is_required];
    $model->schedule= $data;
}
?>

<div class="cuisine-form">
    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'parent_id')->textInput()->widget(Select2::classname(), [
                'data' =>$all_cats,
                'options' => ['placeholder' => 'Please select ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>

        <div class="w-100"></div>

		<?php foreach($languages as $language){	?>
			<div class="col-sm-4">
				<?= $form->field($model, "category_name[{$language->language_id}]")->textInput(['maxlength' => true])->label(Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image).' (category name)') ?>
			</div>
		<?php } ?>


        <div class="w-100"></div>
        <div class="col-sm-2">
            <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => ['active' => 'Active', 'inactive' => 'Inactive'],
            ])
            ?>
        </div>
        <div class="w-100"></div>
        <div class="col-sm-12">
            <?= $form->field($model, 'schedule')->widget(MultipleInput::className(), [
                'max' => 4,
                'columns' => [
                    [
                        'name'  => 'option_id',
                        'type'  => Select2::className(),
                        'title' => 'Option',
                        'options' => [
                            'options' => ['placeholder' => 'Please select ...'],
                            'data'=>$all_options,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]
                    ],
                    [
                        'name'  => 'is_required',
                        'type'  => Select2::className(),
                        'title' => 'Is required?',
                        'options' => [
                            'data'=>['no'=>'No','yes'=>'Yes'],
                            'pluginOptions' => [
                            ],
                        ]
                    ],
                ]
            ])->label(false);
            ?>
        </div>
		<div class="col-sm-12 mb-3">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
		</div>

	</div>
	
    <?php ActiveForm::end(); ?>
</div>
