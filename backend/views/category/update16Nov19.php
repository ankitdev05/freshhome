<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = Yii::t('app', 'Update Product', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['/main-categories']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $main_cat->name), 'url' => ['index','cat_id'=>$cat_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update '.$model->name);
?>
<div class="category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'main_cat' => $main_cat,
        'cat_id' => $cat_id,
    ]) ?>

</div>
