<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = Yii::t('app', 'Update Category', [
    'name' => '',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['/main-categories']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $main_cat->name), 'url' => ['index','cat_id'=>$cat_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update ');
?>
<div class="category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model1' => $model1,  
        'main_cat' => $main_cat,
        'cat_id' => $cat_id,
        'all_home_category' => $all_home_category,
     

    ]) ?>

</div>
