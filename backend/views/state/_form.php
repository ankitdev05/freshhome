<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\Countries;

$all_countries = ArrayHelper::map(Countries::find()->all(), 'country_id', 'name');
/* @var $this yii\web\View */
/* @var $model common\models\State */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="state-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-sm-4">

        <?= $form->field($model, 'country_id')->textInput()->widget(Select2::classname(), [
            'data' => $all_countries,
            'options' => ['placeholder' => 'Please select ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])
        ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'status')->widget(Select2::classname(), [
            'data' => [1 => 'Active',0 => 'Inactive'],

        ])
        ?>
    </div>
    <div class="form-group col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
