<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\State */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'States'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="state-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->state_id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'country_id',
                'value'=>function($data){
                    return $data->country->name;
                }
            ],
            'name',
            'code',
            [
                'attribute'=>'status',
                'value'=>function($data){
                    return $data->status==1 ? "Active" : "Inactive";
                }
            ]
        ],
    ]) ?>

</div>
