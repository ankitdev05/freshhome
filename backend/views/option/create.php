<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Option */

$this->title = Yii::t('app', 'Create Attribute');

$this->params['breadcrumbs'][] = ['label' => 'Attribute Group','url' => ['/attribute-group']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Attributes'), 'url' => ['index','attr_id' => $attr_id]];
$this->params['breadcrumbs'][] = $attribute->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="option-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider
    ]) ?>

</div>
