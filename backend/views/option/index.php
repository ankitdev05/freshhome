<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Attributes');
$this->params['breadcrumbs'][] = ['label' => 'Attribute Group','url' => ['/attribute-group']];
$this->params['breadcrumbs'][] = $attribute->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="option-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Attribute'), ['create','attr_id' => $attr_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'option_name',
                'format'=>'raw',
                'value'=>function($data){
                    $descriptions	= $data->optionDescriptions;
                    $html = '';
                    foreach($descriptions as $description){
                        $language	= $description->language;
                        $html .= '<span class="lang_'.$language->language_id.'">'.Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' '.$description->name.'</span><br/>';
                    }
                    return $html;
                }
            ],
            [
                'attribute'=>'type',
                'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'type',
                    'data' => $searchModel->types,
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])
            ],
            'sort_order',
            [
                'attribute'=>'status',
                'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => [1 => 'Active', 0 => 'Inactive'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'value'=>function($data){
                    return $data->status==1 ? "Active" : "In active";
                }
            ],
            'created_at:datetime',
            //'updated_at',

            ['class' => 'yii\grid\FaActionColumn',
                'template'=>'{update} {option_values}',
                'buttons' => [
                    'option_values'=>function($url, $model, $key) use ($attr_id){
                        return Html::a(Html::tag('i','',['class'=>'fas fa-filter']), ['/option-value','option_id' => $model->option_id,'attr_id' => $attr_id], ['title'=>'Options Values','data-pjax' => 0,'class'=>'btn btn-secondary']);
                    },
                    'update'=>function($url, $model, $key) use ($attr_id){
                        return Html::a(Html::tag('i','',['class'=>'fas fa-edit']), ['/option/update','attr_id' => $attr_id,'id' => $model->option_id], ['title'=>'Update','data-pjax' => 0,'class'=>'btn btn-secondary']);
                    },
                ]
            ],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>

    <?php Pjax::end(); ?>

</div>
