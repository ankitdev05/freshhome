<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

use common\models\Language;
use unclead\multipleinput\MultipleInput;
use yii\web\View;


/* @var $this yii\web\View */
/* @var $model common\models\Option */
/* @var $form yii\widgets\ActiveForm */
$language_id = 1;
$languages = Language::find()->where(['status'=>1])->all();
if(empty($model->sort_order))
    $model->sort_order = 1;

if(!empty($model->option_id)){
    $descriptions	= $model->optionDescriptions;
    foreach($descriptions as $description){
        $model->option_name[$description->language_id] = $description->name;
    }
}

?>

<div class="option-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="row">

    <?php foreach($languages as $language){	?>
        <div class="col-sm-4">
            <?= $form->field($model, "option_name[{$language->language_id}]")->textInput(['maxlength' => true])->label(Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' (Option name)') ?>
        </div>
    <?php } ?>
    <div class="w-100"></div>
    <div class="col-sm-12"><hr/></div>

    <div class="col-sm-4" >

        <?= $form->field($model, 'type')->widget(Select2::classname(), [
            'data' => $model->types
        ]); ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'sort_order')->textInput() ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'status')->widget(Select2::classname(), [
         'data' => [1 => 'Active', 0 => 'Inactive'],
        ]); ?>
    </div>
    <div class="w-100"></div>
        <div class="col-sm-12 d-none" id="option_values">
            <hr/>
            <h5>Option Values</h5>
        <?= $form->field($model, 'schedule')->widget(MultipleInput::className(), [

            'columns' => [
                [
                    'name'  => 'option_value',
                    'title' => 'Option Value Name',
                    'enableError' => true,
                    'options' => [
                        'class' => 'input-priority'
                    ]
                ],
//                [
//                    'name'  => 'image',
//                    'type'  => FileInput::className(),
//                    'title' => 'Image',
//                    'options'=>[
//                        'options'=>['accept'=>'image/*'],
//                        'pluginOptions'=>[
//                            'allowedFileExtensions'=>['jpg','gif','png'],
//                            'overwriteInitial'=>true,
//                        ]
//                    ]
//                ],
                [
                    'name'  => 'sort_order',
                    'title' => 'Sort Order',
                    'enableError' => true,
                    'options' => [
                        'class' => 'input-priority'
                    ]
                ]
            ]
        ])->label(false);
        ?>
    </div>

</div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs('
    getTypeVal();
    $(\'#option-type\').on(\'change\',function(){

		getTypeVal();
	});
');
$this->registerJs('
	function getTypeVal(){
		var value = $(\'#option-type\').val();

		if(value=="select" || value=="checkbox" || value=="radio"){
			$(\'#option_values\').show();
		}else
			$(\'#option_values\').hide();
	}
',View::POS_HEAD);