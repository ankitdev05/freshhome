<?php

use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use common\models\Country;
/* @var $this yii\web\View */
/* @var $model common\models\DriverCompanyNumbers */
/* @var $form yii\widgets\ActiveForm */

$all_nationality = ArrayHelper::map(Country::find()->orderBy('nationality')->all(), 'nationality', 'nationality');
?>

<div class="driver-company-numbers-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="row">
    <?= $form->field($model, 'company_id')->hiddenInput()->label(false) ?>
    <div class="col-sm-3">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'age')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-3">
        <?=
        $form->field($model, 'nationality')->widget(Select2::classname(), [
            'data' => $all_nationality,
        ])
        ?>
    </div>
    <?= $form->field($model, 'token')->hiddenInput()->label(false) ?>
</div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
