<?php

use yii\helpers\Html;
use common\models\DriverCompany;

/* @var $this yii\web\View */
/* @var $model common\models\DriverCompanyNumbers */
$comp_info = DriverCompany::findOne($id);

$this->title = Yii::t('app', 'Create Number');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $comp_info->company_name), 'url' => ['/driver-company/view','id'=>$id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $comp_info->company_name.'\'s Numbers'), 'url' => ['index','id'=>$comp_info->company_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-company-numbers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
