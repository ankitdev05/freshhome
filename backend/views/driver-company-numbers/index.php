<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\DriverCompany;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DriverCompanyNumbersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$comp_info = DriverCompany::findOne($id);
$this->title = Yii::t('app', 'Drivers Numbers');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $comp_info->company_name), 'url' => ['/driver-company/view','id'=>$id]];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="driver-company-numbers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create  Number'), ['create','id'=>$id], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'age',
            'nationality',
            'phone_number',
            [
                'class' => 'yii\grid\FaActionColumn','template'=>'{update} {orders}',
                'buttons'=>[
                    'orders'=>function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'fas fa-shopping-cart']), ['/driver-company-numbers/orders','id' => $model->company_id], ['data-pjax' => 0,'class'=>'btn btn-secondary','title'=>'Orders']);
                    },
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
