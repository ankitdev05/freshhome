<?php

use yii\helpers\Html;
use common\models\DriverCompany;
/* @var $this yii\web\View */
/* @var $model common\models\DriverCompanyNumbers */
$comp_info = DriverCompany::findOne($model->company_id);

$this->title = Yii::t('app', 'Update Numbers: {name}', [
    'name' => $model->phone_number,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $comp_info->company_name), 'url' => ['/driver-company/view','id'=>$model->company_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $comp_info->company_name.'\'s Numbers'), 'url' => ['index','id'=>$comp_info->company_id]];

$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="driver-company-numbers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
