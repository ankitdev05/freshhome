<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use common\models\Category;
use common\models\MainCategories;

/* @var $this yii\web\View */
/* @var $model common\models\AttributeGroup */
/* @var $form yii\widgets\ActiveForm */

$categories= $model->categories;
if(!empty($categories)){
    foreach($categories as $cat)
        $cats[] = $cat->category_id;
    $model->category = $cats;
}
$catId = [];
$main_categories = MainCategories::find()->where(['>','id','1'])->asArray()->all();
foreach ($main_categories as $main_category)
    $catId[] = $main_category['id'];
$all_cats = [];
$categories = Category::getAllCategories($catId);
foreach ($categories as $category) {
    $all_cats[$category['category_id']] = strip_tags(html_entity_decode($category['name'], ENT_QUOTES, 'UTF-8'));
}

?>

<div class="attribute-group-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, 'status')->widget(Select2::classname(), [
            'data' => [1 => 'Active', 0 => 'Inactive']])
        ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($model, 'category')->widget(Select2::classname(), [
            'data' => $all_cats,
            'options' => ['placeholder' => 'Please select ...','multiple' => true],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        ?>
    </div>
    <div class="form-group col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
