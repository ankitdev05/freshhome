<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use common\models\City;
use common\models\ContactMessage;
use himiklab\thumbnail\EasyThumbnailImage;


$this->title = Yii::t('app', $title);
$this->params['breadcrumbs'][] = $this->title;

$all_cities = ArrayHelper::map(City::find()->where(['status'=>'active'])->all(), 'id', 'city_name');

?>
<style type="text/css">
  a{
    color: #fff;
  }
</style>
 
<div class="user-index">
<div class="container">
	<div class="row">
		<?= Html::a(Html::tag('i',' Accept All pending images',['class'=>'fas fa-check']),$url_format.'?id=all&role='.$role,['class'=>'btn btn-warning btn-sm float-right mr-3','data-confirm'=>'Are you sure want to accept all pending images?']) ?>
		<?= Html::a(Html::tag('i',' Accept selected images',['class'=>'fas fa-check']),'javascript:void(0)',['class'=>'btn btn-primary btn-sm float-right acc_sel']) ?>
		
	</div>
</div>	
<p class="w-100"></p>	
<hr/>
    <?php Pjax::begin(); ?>
	<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
			['class' => 'yii\grid\CheckboxColumn'],
            ['class' => 'yii\grid\SerialColumn'],
			// 'name',  
   //          'username',
   //          'email:email',
			[
				'attribute'=>'national_pic',
				'filter'=>false,
				'format'=>'raw',
				'value'=>function($model){  // return $model->national_pic; 
					$image = EasyThumbnailImage::thumbnailFileUrl(
						"../../frontend/web/uploads/".$model->national_pic,100,100,EasyThumbnailImage::THUMBNAIL_INSET
					);
					$Limage = EasyThumbnailImage::thumbnailFileUrl(
						"../../frontend/web/uploads/".$model->national_pic,400,400,EasyThumbnailImage::THUMBNAIL_INSET
					); 
					return Html::a(Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Profile Picture', 'title'=>'Profile Picture']),'javascript:void(0)',['class'=>'popImg','data-src'=>$Limage]);      
				
				}
			],
	        'username',
            'email:email',  
            
            [
				'class' => 'yii\grid\ActionColumn',
				'template'=>'{approve} {reject} {viewalldetails}',
				'buttons'=>[
					'approve'=>function($url, $model, $key) use ($url_format){
						return Html::a(Html::tag('button',Html::tag('i',' Accept',['class'=>'fas fa-check']),['class'=>'btn btn-sm btn-primary']), $url_format.'?id='.$model->id.'&role='.$model->role, ['data-pjax' => 0,'data-confirm'=>'Are you sure want to accept this image?']);
					},
					'reject'=>function($url, $model, $key) use ($url_format){
						return Html::a(Html::tag('i',' Reject',['class'=>'fas fa-times']), 'javascript:void(0)', ['class'=>'btn btn-sm btn-danger send_reply','data-pjax' => 0,'data-toggle'=>'modal','data-target'=>'#reply-user','u-email'=>$model->email,'u-subject'=>config_name.' - Your profile picture has been rejected.','u-user_id'=>$model->id]);
					},
					  'viewalldetails'=>function($url, $model, $key) use ($url_format){
                    	// echo "<pre>";
                    	// print_r($model);die;       
	                    $supplier_info = $model->id;    
                        return Html::a(Html::tag('button',Html::tag('i',' View All Details ',['class'=>'fas fa-eye']),['class'=>'btn btn-sm btn-success','onclick'=>"openmode('".$model->id."')"]), 'javascript:void(0)', ['class'=>'send_reply','data-pjax' => 0,'data-toggle'=>'modal']);
                    }     
				]	
			],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>
	</div>
    <?php Pjax::end(); 
	
	$c_model->send_message = 1;
	$c_model->message = "Dear [user_name],\n\n\nThanks,\nTeam ".config_name;
	?>
</div>

<script type="text/javascript">
	   

	function openmode(id){
		console.log(id,'user id');  
		  $.ajax({       
            type: "POST",
            url: "<?= Yii::$app->urlManager->createUrl('user/getuserdetails'); ?>",
            data: {id: id}, 
            dataType: 'html',
            cache: false,
            success: function (data) { 
            	 var viewalldetails = JSON.parse(data);   
   
               console.log(viewalldetails,'viewalldetails ');
            $("#national_pic").text('');    
            $("#profile_pic").text('');    
            $("#myDiv").text('');      
   
            $("#name").text(viewalldetails['userdetails'].name);    
            $("#email").text(viewalldetails['userdetails'].email);    
            $("#username").text(viewalldetails['userdetails'].username);    
            $("#date_of_birth").text(viewalldetails['userdetails'].dob);    
            $("#phone_number").text(viewalldetails['userdetails'].phone_number);    
            $("#nationality").text(viewalldetails['userdetails'].nationality);    
            $("#occupation").text(viewalldetails['occupation']);  
            $("#city").text(viewalldetails['userdetails'].city_name); 

        if(viewalldetails['wallet_amt'] == null){
            $("#wallet_amt").text('0.00');
        }else{
            $("#wallet_amt").text(viewalldetails['wallet_amt']);
        }

            $("#unique_code").text(viewalldetails['userdetails'].share_id);
            $("#description").text(viewalldetails['userdetails'].description);

       if(viewalldetails['userdetails'].is_driver == 'yes'){
            $("#user_type").text('Driver');  
       }else  if(viewalldetails['userdetails'].is_supplier == 'yes'){
            $("#user_type").text('Supplier');  
       }else  if(viewalldetails['userdetails'].is_sales_person == 'yes'){
            $("#user_type").text('Sales Person');  
       }else{
       	  $("#user_type").text('User');  
       }   
  
 
    
            var mydiv = document.getElementById("myDiv");
var aTag = document.createElement('a');
aTag.setAttribute('href',viewalldetails.url+'?id='+viewalldetails['userdetails'].id+'&role='+viewalldetails['userdetails'].role);    
aTag.innerText = "Accept"; 
mydiv.appendChild(aTag);

   	console.log(viewalldetails['userdetails'].national_pic,'national_pic');   

   
   if(viewalldetails['userdetails'].national_pic == null){
 $('.national_pic').hide();
   }else{
 $('.national_pic').show();
 console.log(viewalldetails['userdetails'].national_pic,'national_pic');   
         var impurl = viewalldetails['imgurl'];   
var imageurl = impurl+'/'+viewalldetails['userdetails'].national_pic;
var elem = document.createElement("img");
elem.setAttribute("src", imageurl );
elem.setAttribute("height", "100");
elem.setAttribute("width", "100");
elem.setAttribute("alt", "Flower");
document.getElementById("national_pic").appendChild(elem);
   }     
   if(viewalldetails['userdetails'].profile_pic == null){
 $('.profile_pic').hide();
  }else{ 
 $('.profile_pic').show();

   	var imageurl = impurl+'/'+viewalldetails['userdetails'].profile_pic;
var elem = document.createElement("img");
elem.setAttribute("src", imageurl );
elem.setAttribute("height", "100");
elem.setAttribute("width", "100");
elem.setAttribute("alt", "Flower");  
document.getElementById("profile_pic").appendChild(elem);
 


  }   

    

               },
        });  
$('#viewalldetails').modal('show');
	}

</script>
<div id="reply-user" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h3>Send message to <span id="email-reply"></span></h3>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<?php $form = ActiveForm::begin(); ?>
					<?= $form->field($c_model, 'user_id')->hiddenInput()->label(false) ?>
					<?= $form->field($c_model, 'role')->hiddenInput()->label(false) ?>
					<?= $form->field($c_model, 'email')->textInput(['maxlength' => true]) ?>
					<?= $form->field($c_model, 'subject')->textInput(['maxlength' => true]) ?>
					
					<?= $form->field($c_model, 'message')->textArea(['rows' => 4])->hint('[user_name] will auto populate user\'s name.') ?>
					<?= $form->field($c_model, 'send_message')->checkBox(['maxlength' => true])->label("Send notification to user") ?>
					<?= Html::submitButton(Yii::t('app', 'Send') , ['class' => 'btn btn-warning']) ?>
				<?php ActiveForm::end(); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-body text-center">
        <img src="" id="imagepreview" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



   <div id="viewalldetails" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>User Details <span id="email-reply"></span></h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <div class="col-lg-12">
<div class="row" >
	 <div class="col-lg-6" style="float: left;" >
<p>Name</p>
</div>  
 <div class="col-lg-6">
<span id="name"></span>      
</div> 

</div>


<div class="row" >
   <div class="col-lg-6" style="float: left;" >
<p>Email</p>
</div>    
 <div class="col-lg-6">
<span id="email" ></span>
</div>
</div>  

<div class="row" >
	 <div class="col-lg-6" style="float: left;" >
<p>Username </p>
</div>    
 <div class="col-lg-6">
<span id="username" ></span>
</div>
</div>  



<div class="row" >
   <div class="col-lg-6" style="float: left;" >
<p>User Type</p>
</div>       
 <div class="col-lg-6">
<span id="user_type" ></span>
</div>
</div>  


<div class="row" >
   <div class="col-lg-6" style="float: left;" >
<p>Unique Code</p>
</div>  
 <div class="col-lg-6">
<span id="unique_code" ></span>
</div>

</div> 


<div class="row" >
	 <div class="col-lg-6" style="float: left;" >
<p>Date of Birth</p>
</div>  
 <div class="col-lg-6">
 <span id="date_of_birth" ></span>
</div>

</div>   

<div class="row" >
	 <div class="col-lg-6" style="float: left;" >
<p>Phone Number</p>
</div>  
 <div class="col-lg-6">
 	<span id="phone_number" ></span>
</div>

</div>

<div class="row" >
<div class="col-lg-6" style="float: left;" >
<p>About Me</p>
</div>  
<div class="col-lg-6">
<span id="description" ></span>
</div>  
</div>

<div class="row" >
<div class="col-lg-6" style="float: left;" >
<p>Nationality</p>
</div>  
<div class="col-lg-6">
<span id="nationality" ></span>
</div>
</div> 

<div class="row" >
<div class="col-lg-6" style="float: left;" >
<p>Occupation</p>
</div>  
<div class="col-lg-6">
<span id="occupation" ></span>
</div>
</div> 

<div class="row" >
<div class="col-lg-6" style="float: left;" >
<p>City</p>
</div>  
<div class="col-lg-6">
<span id="city" ></span>
</div>
</div>

<div class="row" >
<div class="col-lg-6" style="float: left;" >
<p>Wallet Amount</p>
</div>    
<div class="col-lg-6">
<span id="wallet_amt" ></span>
</div>
</div>
 
<div class="row profile_pic" >
	 <div class="col-lg-6" style="float: left;" >
<p>Profile Pic</p>
</div>  
 <div class="col-lg-6" id="profile_pic" > 
</div>
 </div> 

 <div class="row national_pic" style="margin-top: 5px;" >
   <div class="col-lg-6" style="float: left;" >
<p>National Pic</p>      
</div>     
 <div class="col-lg-6"   >
 <span id="national_pic"></span> 
</div>
 </div> 


</div>  
                </div>
                <div class="modal-footer">
                   <span>
                  <button type="button" class="btn btn-info" id="myDiv"  >
                
                  </button>

            
                   </span>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>



<?php
$this->registerJs('
	$(".send_reply").on("click",function(){
		var email = $(this).attr("u-email");
		var subject = $(this).attr("u-subject");
		var user_id = $(this).attr("u-user_id");
		$("#email-reply").html(email);
		$("#contactmessage-email").val(email);
		$("#contactmessage-role").val("admin");
		$("#contactmessage-user_id").val(user_id);
		$("#contactmessage-subject").val(subject);
		
	})  
');

$this->registerJs('
	$(\'.popImg\').on(\'click\',function(){
		$(\'#imagepreview\').attr(\'src\', $(this).attr(\'data-src\')); 
		 $(\'#imagemodal\').modal(\'show\');
	});
	$(\'.acc_sel\').on(\'click\',function(){
		var sel_val = [];
		$.each($("input[name=\'selection[]\']:checked"), function(){
			sel_val.push($(this).val());
		});
		var ids = sel_val.join(",");
		if(ids==""){
			alert("Please select the images");
			return false;
		}
		if(ids!="" && confirm("Are you sure want to accept selected images?"))
			window.location.href = "'.$url_format.'?id="+ids+"&role='.$role.'";
	})
');
