<?php
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\OrderStatus;
use common\models\User;

$all_status = ArrayHelper::map(OrderStatus::find()->all(), 'order_status_id', 'name');
?>

<?php Pjax::begin(); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'order_id',
        [
            'attribute'=>'user_id',
            // 'filter'=>false,
            'format' => 'raw',
            'value'=>function($data){
                $user_data = User::findOne($data->user_id);
                $name = '';
                if(!empty($user_data)){
                    $name = $user_data->name;
                }
                return $name.'<br/>('.$user_data->share_id.')';

            }
        ],
        [
            'attribute'=>'supplier_id',
            //'filter' => false,
            'format' => 'raw',
            'value' => function($data){
                $supplier = $data->supplier;
                return $supplier->name.'<br/>('.$supplier->share_id.')';
            }
        ],
        [
            'attribute'=>'total',
            'value'=>function($data){
                return $data->currency_code.' '.Yii::$app->formatter->format($data->total,['decimal',2]);
            }
        ],
        [
            'attribute'=>'order_status_id',
            'filter'=>Select2::widget([
                'model' => $searchModel,
                'attribute' => 'order_status_id',
                'data' => $all_status,
                'options' => [
                    'placeholder' => 'Please select ...',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],

            ]),
            'value'=>function($data){
                return $data->orderStatus->name;
            }
        ],
        //'payment_code',
        'payment_method',
        'created_at:datetime',
        //'updated_at',

        ['class' => 'yii\grid\FaActionColumn','template'=>'{view}'],
    ],
    'pager' => [
        'pageCssClass' => 'page-item',
        'disabledPageCssClass' => 'page-link disabled',
        'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
        'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
        'linkOptions' => ['class' => 'page-link'],
    ]
]); ?>
<?php Pjax::end(); ?>
