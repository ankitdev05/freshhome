<?php
if(!empty($model)){
?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Email</th>
            <th>Bank name</th>
            <th>Account number</th>
            <th>Iban</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?= $model->first_name ?></td>
            <td><?= $model->last_name ?></td>
            <td><?= $model->email_address ?></td>
            <td><?= $model->bank_name ?></td>
            <td><?= $model->account_number ?></td>
            <td><?= $model->iban ?></td>
        </tr>

        </tbody>
    </table>
</div>
<?php
}else
    echo '<p>No detail found.</p>';