<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\models\City;
use common\models\UserRole;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Drivers');
$this->params['breadcrumbs'][] = $this->title;

$all_cities = ArrayHelper::map(City::find()->where(['status'=>'active'])->all(), 'id', 'city_name');
$all_roles = ArrayHelper::map(UserRole::find()->all(), 'role_id', 'name');
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="float-right">
        <a href="<?= Yii::$app->request->baseUrl ?>/user/driver?accept_doc=true" data-confirm="Are you sure want to accept all documents?" class="btn btn-sm btn-warning">Accept All documents</a>
    </div>
    <?php Pjax::begin(); ?>

    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'name',
                [
                   'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function($data){
                        $count = $data->driverDocStatus;
                        $red = '';
                        if($count > 0)
                            $red = ' <i class="fas fa-circle" style="color:red"></i>';
                        return $data->name.$red;
                    }
                ],
                'username',
                'email:email',
                [
                    'attribute'=>'status',
                    'filter'=>Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'status',
                        'data' => [10=>'Active',0=>'In active'],
                        'options' => [
                            'placeholder' => 'Please select ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],

                    ]),
                    'value'=>function($data){
                        return $data->status === 10 ? 'Active' : 'In active';
                    }
                ],
                [
                    'attribute'=>'city',
                    'filter'=>Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'city',
                        'data' => $all_cities,
                        'options' => [
                            'placeholder' => 'Please select ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],

                    ]),
                    'value'=>function($data){
                        return !empty($data->cityInfo) ? $data->cityInfo->city_name : null;
                    }
                ],
                [
                    'attribute'=>'image_approval',
                    'filter'=>Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'image_approval',
                        'data' => [1=>'Active',0=>'In active'],
                        'options' => [
                            'placeholder' => 'Please select ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],

                    ]),
                    'value'=>function($data){
                        return $data->status === 1 ? 'Active' : 'In active';
                    }
                ],
                'share_id',
                [
                    'attribute' => 'wallet_amount',
                    'value' => function($data){
                        return $data->balance;
                    }
                ],
                [
                    'class' => 'yii\grid\FaActionColumn',
                    'template'=>'{view} {delete_btn}',
                    'buttons' => [
                        'view' =>function($url, $model, $key){
                            return Html::a(Html::tag('i','',['class'=>'fas fa-eye']), ['user/driverview','id' => $model->id], ['data-pjax' => 0,'title'=>'View','class'=>'btn btn-secondary']);
                        },
                        'delete_btn' => function($url, $model, $key){
                            return Html::a(Html::tag('i','',['class'=>'far fa-trash-alt']), Url::to(['user/delete','id'=>$model->id]), [ 'data-pjax' => 0,'title'=>'Delete','class'=>'btn btn-secondary','data-method'=>'POST','data-confirm' =>'Are you sure want to delete this user? Action can\'t be undone']);
                        },
                    ]
                ],
            ],
            'pager' => [
                'pageCssClass' => 'page-item',
                'disabledPageCssClass' => 'page-link disabled',
                'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
                'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
                'linkOptions' => ['class' => 'page-link'],
            ]
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>

