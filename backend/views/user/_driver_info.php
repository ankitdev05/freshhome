<?php
use yii\widgets\DetailView;
?>
<div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'email:email',
            [
                'attribute' => 'status',
                'value'=>function($data){
                    return $data->status === 10 ? 'Active' : 'In active';
                }
            ],
            [
                'attribute'=>'city',
                'value'=>function($data){
                    return !empty($data->cityInfo) ? $data->cityInfo->city_name : null;
                }
            ],
            'phone_number',
            'created_at:datetime',
        ],
    ]) ?>
</div>