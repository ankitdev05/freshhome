<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\models\City;
$language_id = 1;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;

$all_cities = ArrayHelper::map(City::find()->joinWith('cityName as cn')->where(['cn.language_id'=>$language_id])->all(), 'id', 'cityName.city_name');
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create','type'=>1], ['class' => 'btn btn-success']) ?>
    </p>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			'name',
            'username',
            'email:email',
			[
				'attribute'=>'status',
				'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => [10=>'Active',0=>'In active'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
					'pluginOptions' => [
                        'allowClear' => true
                    ],

                ]),
				'value'=>function($data){
					return $data->status === 10 ? 'Active' : 'In active';
				}
			],


            //'phone_number',
            //'dob',
            //'profile_pic',
            //'image_approval',
            //'building_no',
            //'flat_no',
            //'floor_no',
            //'landmark',

            //'location',
            //'latitude',
            //'longitude',
            //'nationality',
            //'availability',
            //'description:ntext',
            [
				'attribute'=>'city',
				'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'city',
                    'data' => $all_cities,
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
					'pluginOptions' => [
                        'allowClear' => true
                    ],

                ]),
				'value'=>function($data){
					return !empty($data->city) ? $data->cityInfo->city_name : null;
				}
			],

            'share_id',
            //'password_hash',
            //'password_reset_token',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn','template'=>'{update}'],
        ],
    ]); ?>
</div>
    <?php Pjax::end(); ?>
</div>
