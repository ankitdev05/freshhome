<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\models\City;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$language_id = 1;
$this->title = Yii::t('app', 'Suppliers');
$this->params['breadcrumbs'][] = $this->title;

$all_cities = ArrayHelper::map(City::find()->joinWith('cityName as cn')->where(['cn.language_id'=>$language_id])->all(), 'id', 'cityName.city_name');
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create supplier'), ['create','type'=>2], ['class' => 'btn btn-success']) ?>
    </p>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
			    'attribute' => 'name',
                'format' => 'raw',
                'value' => function($data){
                    $icon = Html::tag('i','',['class' => 'fas fa-utensils']);
                    if($data->supplier_type > 1)
                        $icon = Html::tag('i','',['class' => 'fas fa-shopping-bag']);
                    return $data->name;
                }
            ],

            'username',
            'email:email',
            [
                'attribute' => 'wallet_amount',
                'value' => function($data){
                    return $data->balance;
                }
            ],
			[
				'attribute'=>'status',
				'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => [10=>'Active',0=>'In active'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
					'pluginOptions' => [
                        'allowClear' => true
                    ],
                    
                ]),
				'value'=>function($data){
					return $data->status === 10 ? 'Active' : 'In active';
				}
			],
          
            
            //'phone_number',
            //'dob',
            //'profile_pic',
            //'image_approval',
            //'building_no',
            //'flat_no',
            //'floor_no',
            //'landmark',
            
            //'location',
            //'latitude',
            //'longitude',
            //'nationality',
            //'availability',
            //'description:ntext',
//            [
//				'attribute'=>'city',
//				'filter'=>Select2::widget([
//                    'model' => $searchModel,
//                    'attribute' => 'city',
//                    'data' => $all_cities,
//                    'options' => [
//                        'placeholder' => 'Please select ...',
//                    ],
//					'pluginOptions' => [
//                        'allowClear' => true
//                    ],
//
//                ]),
//				'value'=>function($data){
//					return !empty($data->city) ? $data->cityInfo->city_name : null;
//				}
//			],
            
            'share_id',
            //'password_hash',
            //'password_reset_token',
            //'created_at',
            //'updated_at',

            [
				'class' => 'yii\grid\FaActionColumn',
				'template'=>'{update} &nbsp;&nbsp;{add_menu}&nbsp;&nbsp;{user_account} {schedule} {delete_btn}',
				'buttons'=>[
                    'update' =>    function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'far fa-edit']), ['/user/update','id' => $model->id,'type' => 2], ['data-pjax' => 0,'title'=>'Edit Supplier','class'=>'btn btn-secondary']);
                    },
				    'schedule' => function($url, $model, $key){
                        if($model->supplier_type==1)
                            return Html::a(Html::tag('i','',['class'=>'fas fa-calendar-alt']), ['/site/viewschedule','id' => $model->id,'type' => 2], ['data-pjax' => 0,'title'=>'Schedules','class'=>'btn btn-secondary']);
                    },
				    'user_account' => function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'fas fa-university']), 'javascript:void(0)', ['data-attr' => $model->id, 'data-pjax' => 0,'title'=>'Bank Detail','class'=>'btn btn-secondary user_account']);
				    },
					'add_menu'=>function($url, $model, $key){
                        $icon = Html::tag('i','',['class' => 'fas fa-utensils']);
                        if($model->supplier_type > 1)
                            $icon = Html::tag('i','',['class' => 'fas fa-shopping-bag']);
						return Html::a($icon, ['menu/index','id' => $model->id,'type' => 2], ['data-pjax' => 0,'title'=>'Add menu','class'=>'btn btn-secondary']);
					},
                    'delete_btn' => function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'far fa-trash-alt']), Url::to(['user/delete','id'=>$model->id]), [ 'data-pjax' => 0,'title'=>'Delete','class'=>'btn btn-secondary','data-method'=>'POST','data-confirm' =>'Are you sure want to delete this user? Action can\'t be undone']);
                    },
				]
				
			],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>
</div>	
    <?php Pjax::end(); ?>
</div>
<?php
    $this->registerJs('
        $(".user_account").on("click",function(){
            var id = $(this).attr("data-attr");
            $.ajax({
                url : "'.yii::$app->request->baseUrl.'/user/bank-details",
                type : "POST",
                dataType : "json",
                data : "id="+id,
                success : function(json){
                    $(".modal-body").html(json["html"]);
                    $("#exampleModal").modal();
                }
            
            })
        })
    ');

?>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Bank Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>