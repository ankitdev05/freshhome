<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\date\DatePicker;

use common\models\Country;

use himiklab\thumbnail\EasyThumbnailImage;

$all_nationality 	= ArrayHelper::map(Country::find()->all(), 'code', 'nationality');

$imageSrc = $delUrl = '';
if(!empty($model->profile_pic)){
	$image=EasyThumbnailImage::thumbnailFileUrl(
		"../../frontend/web/uploads/".$model->profile_pic,200,200,EasyThumbnailImage::THUMBNAIL_INSET
	);
	$imageSrc	= [Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Profile Picture', 'title'=>'Profile Picture'])];
	$delUrl=[['caption'=>'Profile Picture','url'=>yii::$app->request->baseUrl.'/user/deleteimage?id='.$model->id]];
}

?>

<div class="row">
	<div class="col-sm-4">
		<?= $form->field($model, 'dob')->widget(DatePicker::classname(), [
			'options' => ['placeholder' => 'Enter birth date ...'],
			'pluginOptions' => [
				'autoclose'=>true,
				'format' => 'yyyy-mm-dd'
			]
		]); ?>
	</div>
	<div class="col-sm-4">
		<?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-4">
		<?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-4">
		<?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-4">
	<?= $form->field($model, 'nationality')->textInput()->widget(Select2::classname(), [
		'data' => $all_nationality,
		'options' => ['placeholder' => 'Please select ...'],
		'pluginOptions' => [
			'allowClear' => true,
		],
	]) ?>
	</div>
	<div class="col-sm-4">
		
		<?= $form->field($model, 'availability')->textInput()->widget(Select2::classname(), [
			'data' => ['online'=>'Online','offline'=>'Offline'],
		]) ?>
	</div>
	<p class="w-100"></p>
	
	<div class="col-sm-4">
		<?= $form->field($model, 'description')->textArea(['rows' => 3]) ?>
	</div>
	<p class="w-100"></p>
	
	<div class="col-sm-6">
		<?= $form->field($model, 'profile_pic')->widget(FileInput::classname(), 
		[
			'options'=>['accept'=>'image/*'],
			'pluginOptions'=>
			[
				'allowedFileExtensions'=>['jpg','gif','png'],
				'overwriteInitial'=>true,
				'initialPreview'=>$imageSrc,
				'initialPreviewConfig'=>$delUrl
			],
		]) ?>
		
	</div>
</div>