<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

$this->registerJs('
	$("#user-role").on(\'change\',function(data){
		$("#user-share_id").val("");
	})
	
	$("#gen_code").on(\'click\',function(data){
		var role = 3;

		$.ajax({
			url		: "'.Yii::$app->request->baseUrl.'/user/code",
			data	: "role="+role,
			dataType: "json",
			success : function(json){
				if(json["code"])
					$("#user-share_id").val(json["code"]);
			}
		})
	})
');
?>
<div class="user-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	<div class="row">
		<div class="col-sm-4">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-sm-4">
			<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-sm-4">
			<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
		</div>

		<div class="col-sm-3">
			<?= $form->field($model, 'share_id')->textInput(['maxlength' => true]) ?><?= Html::a(Html::tag('i','',['class'=>'fas fa-sync-alt']),'javascript:void(0)',['title'=>'Generate Code','id'=>'gen_code']) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => true]) ?>
		</div>

		<div class="col-sm-3">
			<?= $form->field($model, 'status')->widget(Select2::classname(), [
				'data' => ['10' => 'Active', '0' => 'Inactive'],
			])
			?>
		</div>
	</div>
	
    <div class="form-group mt-3">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
