<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
$language_id = 1;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Admin Users');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create  Admin User'), ['admincreate'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			'name',
            'username',
            'email:email',
			[
				'attribute'=>'status',
				'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => [10=>'Active',0=>'In active'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
					'pluginOptions' => [
                        'allowClear' => true
                    ],

                ]),
				'value'=>function($data){
					return $data->status === 10 ? 'Active' : 'In active';
				}
			],
            //'password_hash',
            //'password_reset_token',
            'created_at:datetime',
            //'updated_at',

            [
                'class' => 'yii\grid\FaActionColumn',
                'template'=>'{update}',
                'buttons'=>[
                    'update'=>function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'far fa-edit']),Url::to(['user/adminupdate','id'=>$model->id]), ['data-pjax' => 0,'alt'=>'Edit','title'=>'Edit','class'=>'btn btn-secondary']);
                    }
                ]
            ],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>
</div>	
    <?php Pjax::end(); ?>
</div>
