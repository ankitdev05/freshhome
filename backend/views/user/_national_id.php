<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap4\ActiveForm;
use himiklab\thumbnail\EasyThumbnailImage;

$this->title = Yii::t('app', 'National IDs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <div class="container">
        <div class="row">
            <?= Html::a(Html::tag('i',' Accept All pending documents',['class'=>'fas fa-check']),$url_format.'?id=all',['class'=>'btn btn-warning btn-sm float-right mr-3','data-confirm'=>'Are you sure want to accept all pending images?']) ?>
            <?= Html::a(Html::tag('i',' Accept selected documents',['class'=>'fas fa-check']),'javascript:void(0)',['class'=>'btn btn-primary btn-sm float-right acc_sel']) ?>

        </div>
    </div>
    <p class="w-100"></p>
    <hr/>
    <?php Pjax::begin(); ?>
    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\CheckboxColumn'],
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'username',
                'email:email',
                [
                    'attribute'=>'national_pic',
                    'filter'=>false,
                    'format'=>'raw',
                    'value'=>function($model){
                       return Html::a('View Document',"../../frontend/web/uploads/".$model->national_pic,['target' => '_blank','data-pjax' => 0]);
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{approve} {reject}',
                    'buttons'=>[
                        'approve'=>function($url, $model, $key) use ($url_format){
                            return Html::a(Html::tag('button',Html::tag('i',' Accept',['class'=>'fas fa-check']),['class'=>'btn btn-sm btn-primary']), $url_format.'?id='.$model->id, ['data-pjax' => 0,'data-confirm'=>'Are you sure want to accept this image?']);
                        },
                        'reject'=>function($url, $model, $key) use ($url_format){
                            return Html::a(Html::tag('i',' Reject',['class'=>'fas fa-times']), 'javascript:void(0)', ['class'=>'btn btn-sm btn-danger send_reply','data-pjax' => 0,'data-toggle'=>'modal','data-target'=>'#reply-user','u-email'=>$model->email,'u-subject'=>config_name.' - Your National ID has been rejected.','u-user_id'=>$model->id]);
                        }
                    ]
                ],
            ],
            'pager' => [
                'pageCssClass' => 'page-item',
                'disabledPageCssClass' => 'page-link disabled',
                'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
                'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
                'linkOptions' => ['class' => 'page-link'],
            ]
        ]); ?>
    </div>
    <?php Pjax::end();
    $c_model->send_message = 1;
    $c_model->message = "Dear [user_name],\n\n\nThanks,\nTeam ".config_name;
    ?>
</div>
<div id="reply-user" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Send message to <span id="email-reply"></span></h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($c_model, 'user_id')->hiddenInput()->label(false) ?>
                <?= $form->field($c_model, 'role')->hiddenInput()->label(false) ?>
                <?= $form->field($c_model, 'email')->textInput(['maxlength' => true]) ?>
                <?= $form->field($c_model, 'subject')->textInput(['maxlength' => true]) ?>

                <?= $form->field($c_model, 'message')->textArea(['rows' => 4])->hint('[user_name] will auto populate user\'s name.') ?>
                <?= $form->field($c_model, 'send_message')->checkBox(['maxlength' => true])->label("Send notification to user") ?>
                <?= Html::submitButton(Yii::t('app', 'Send') , ['class' => 'btn btn-warning']) ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs('
	$(".send_reply").on("click",function(){
		var email = $(this).attr("u-email");
		var subject = $(this).attr("u-subject");
		var user_id = $(this).attr("u-user_id");
		$("#email-reply").html(email);
		$("#contactmessage-email").val(email);
		$("#contactmessage-role").val("admin");
		$("#contactmessage-user_id").val(user_id);
		$("#contactmessage-subject").val(subject);
		
	})
');

$this->registerJs('

	$(\'.acc_sel\').on(\'click\',function(){
		var sel_val = [];
		$.each($("input[name=\'selection[]\']:checked"), function(){
			sel_val.push($(this).val());
		});
		var ids = sel_val.join(",");
		if(ids==""){
			alert("Please select the images");
			return false;
		}
		if(ids!="" && confirm("Are you sure want to accept selected images?"))
			window.location.href = "'.$url_format.'?id="+ids;
	})
');
