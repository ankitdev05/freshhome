<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\models\City;
$language_id = 1;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;

$all_cities = ArrayHelper::map(City::find()->joinWith('cityName as cn')->where(['cn.language_id'=>$language_id])->all(), 'id', 'cityName.city_name');
?>
<div class="user-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php Pjax::begin(); ?>
    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['usercreate'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'name',
                'filter'=>false,
                'value'=>function($data){
                    $user_info = $data->userDetail;
                    return $user_info['name'];
                }
            ],
            'username',
            'email:email',
            [
               'attribute' => 'wallet_amount',
               'value' => function($data){
                    return $data->balance;
               }
            ],
			[
				'attribute'=>'status',
				'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => [10=>'Active',0=>'In active'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
					'pluginOptions' => [
                        'allowClear' => true
                    ],

                ]),
				'value'=>function($data){
					return $data->status === 10 ? 'Active' : 'In active';
				}
			],
            
            'share_id',
            //'password_hash',
            //'password_reset_token',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\FaActionColumn',
                'template'=>'{update} {delete_btn}',
                'buttons'=>[
                    'update'=>function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'far fa-edit']),Url::to(['user/userupdate','id'=>$model->id]), ['data-pjax' => 0,'alt'=>'Edit','title'=>'Edit','class'=>'btn btn-secondary']);
                    },
                    'delete_btn' => function($url, $model, $key){
                        return Html::a(Html::tag('i','',['class'=>'far fa-trash-alt']), Url::to(['user/delete','id'=>$model->id]), [ 'data-pjax' => 0,'title'=>'Delete','class'=>'btn btn-secondary','data-method'=>'POST','data-confirm' =>'Are you sure want to delete this user? Action can\'t be undone']);
                    },
                ]
            ],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>
</div>	
    <?php Pjax::end(); ?>
</div>
