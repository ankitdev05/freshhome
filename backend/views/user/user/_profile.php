<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\date\DatePicker;

use common\models\Country;
use common\models\Occupation;

use himiklab\thumbnail\EasyThumbnailImage;

$all_nationality 	= ArrayHelper::map(Country::find()->all(), 'code', 'nationality');

$language_id = 1;
$all_occupations = ArrayHelper::map(Occupation::find()->joinWith('occupationName as on')->where(['on.language_id'=>$language_id])->all(), 'id', 'occupationName.occupation_name');

$imageSrc = $delUrl = '';
if(!empty($user_data->profile_pic)){
	$image=EasyThumbnailImage::thumbnailFileUrl(
		"../../frontend/web/uploads/".$user_data->profile_pic,200,200,EasyThumbnailImage::THUMBNAIL_INSET
	);
	$imageSrc	= [Html::img($image, ['class'=>'file-preview-image', 'alt'=>'Profile Picture', 'title'=>'Profile Picture'])];
	$delUrl=[['caption'=>'Profile Picture','url'=>yii::$app->request->baseUrl.'/user/deleteimage?id='.$model->id]];
}

?>

<div class="row">
	<div class="col-sm-4">
		<?= $form->field($user_data, 'dob')->widget(DatePicker::classname(), [
			'options' => ['placeholder' => 'Enter birth date ...'],
			'pluginOptions' => [
				'autoclose'=>true,
				'format' => 'yyyy-mm-dd'
			]
		]); ?>
	</div>

	<div class="col-sm-4">
	<?= $form->field($user_data, 'nationality')->textInput()->widget(Select2::classname(), [
		'data' => $all_nationality,
		'options' => ['placeholder' => 'Please select ...'],
		'pluginOptions' => [
			'allowClear' => true,
		],
	]) ?>
	</div>
    <div class="col-sm-4">
        <?= $form->field($user_data, 'gender')->textInput()->widget(Select2::classname(), [
            'data' => ['male'=>'Male','female'=>'Female'],
            'options' => ['placeholder' => 'Please select ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]) ?>
    </div>

	<p class="w-100"></p>
    <div class="col-sm-4">
        <?= $form->field($user_data, 'description')->textArea(['rows' => 3]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($user_data, 'occupation_id')->textInput()->widget(Select2::classname(), [
            'data' =>$all_occupations,
            'options' => ['placeholder' => 'Please select ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]) ?>
    </div>

    <p class="w-100"></p>
	<div class="col-sm-6">
		<?= $form->field($user_data, 'profile_pic')->widget(FileInput::classname(),
		[
			'options'=>['accept'=>'image/*'],
			'pluginOptions'=>
			[
				'allowedFileExtensions'=>['jpg','gif','png'],
				'overwriteInitial'=>true,
				'initialPreview'=>$imageSrc,
				'initialPreviewConfig'=>$delUrl
			],
		]) ?>
		
	</div>
</div>