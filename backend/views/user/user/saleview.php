<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */
$user_info = $model->salesDetail;
$this->title = $user_info['name'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sales person'), 'url' => ['sales-person']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'email:email',
            'status',
            [
                'attribute'=>'name',
                'filter'=>false,
                'value'=>function($data) use ($user_info){
                    return $user_info['name'];
                }
            ],
            [
                'attribute'=>'gender',
                'filter'=>false,
                'value'=>function($data) use ($user_info){
                    return $user_info['gender'];
                }
            ],
            [
                'attribute'=>'city',
                'filter'=>false,
                'value'=>function($data) use ($user_info){
                    return $user_info['city_name'];
                }
            ],
            'phone_number',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
