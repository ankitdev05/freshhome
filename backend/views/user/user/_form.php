<?php

use yii\helpers\Html;
use yii\bootstrap4\Tabs;
use yii\bootstrap4\ActiveForm;
if(isset($_GET['type']))
	$model->role= $_GET['type'];
?>
<div class="user-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	<?=
		Tabs::widget([
			'items' => [
				[
					'label'=>yii::t('yii','General'),
					'content' =>'<br/>'.$this->render("_general",['form'=>$form,'model'=>$model,'user_data'=>$user_data])
				],
				[
					'label'=>yii::t('yii','Profile data'),
					'content' =>'<br/>'.$this->render("_profile",['form'=>$form,'model'=>$model,'user_data'=>$user_data])
				]
			]
		]);
	?>
	<div class="clearfix"></div>
	
    <div class="form-group mt-3">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
