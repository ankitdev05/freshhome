<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use common\models\City;
use common\models\UserRole;
use yii\helpers\ArrayHelper;

$all_cities = ArrayHelper::map(City::find()->where(['status'=>'active'])->all(), 'id', 'city_name');
$all_roles = ArrayHelper::map(UserRole::find()->all(), 'role_id', 'name');

$this->registerJs('
	$("#user-role").on(\'change\',function(data){
		$("#user-share_id").val("");
	})
	
	$("#gen_code").on(\'click\',function(data){
		var role = 2;

		$.ajax({
			url		: "'.Yii::$app->request->baseUrl.'/user/code",
			data	: "role="+role,
			dataType: "json",
			success : function(json){
				if(json["code"])
					$("#user-share_id").val(json["code"]);
			}
		})
	})
');
if($model->status!=10)
    $model->status= 0;
?>
<div class="row">
	<div class="col-sm-4">
		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-4">
		<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-4">
		<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
	</div>

	<div class="col-sm-3">
		<?= $form->field($model, 'share_id')->textInput(['maxlength' => true]) ?><?= Html::a(Html::tag('i','',['class'=>'fas fa-sync-alt']),'javascript:void(0)',['title'=>'Generate Code','id'=>'gen_code']) ?>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'password')->passwordInput(['maxlength' => true,'autocomplete' => 'off']) ?>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => true,'autocomplete' => 'off']) ?>
	</div>
	<p class="w-100"></p>
	<div class="col-sm-3">
		<?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'building_no')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'flat_no')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'floor_no')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'landmark')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'city')->textInput()->widget(Select2::classname(), [
				'data' => $all_cities,
				'options' => ['placeholder' => 'Please select ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			])
		?>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'status')->widget(Select2::classname(), [
			'data' => [10 => 'Active', 0 => 'Inactive'],
		])
		?>
	</div>
</div>