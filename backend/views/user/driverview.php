<?php

use yii\helpers\Html;

use common\models\DriverDocuments;
use yii\bootstrap4\Tabs;
/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drivers'), 'url' => ['driver']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$all_documents = DriverDocuments::find()->where(['is_delete'=>0,'user_id' => $model->id ])->orderBY('id desc')->all();
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=
    Tabs::widget([
        'items' => [
            [
                'label' =>yii::t('yii','General Info.'),
                'content' =>'<br/>'.$this->render('_driver_info',['model'=>$model])
            ],
            [
                'label' =>yii::t('yii','Wallet'),
                'content' =>'<br/>'.$this->render('_wallet',['dataProvider'=>$dataProvider,'model' => $model])
            ],
            [
                'label' =>yii::t('yii','Orders'),
                'content' =>'<br/>'.$this->render('_orders',['searchModel'=>$searchModel,'dataProvider'=>$order_dataProvider,'model' => $model])
            ]
        ]
    ]);
    ?>

    <div class="row">
        <div class="col-sm-12">
            <hr/>
        <?php if(!empty($all_documents)){ ?>
            <h3>Documents</h3>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Document</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($all_documents as $all_document){ ?>
                        <tr>
                            <td><a target="_blank" href="<?= Yii::$app->request->baseUrl.'/../uploads/'.$all_document->document ?>">Document</a></td>
                            <td><?= $all_document->status==0 ? 'Inactive' : 'Active' ?></td>
                            <td>
                                <?php if($all_document->status==0){ ?>
                                <a data-confirm="Are you sure want to accept this document?" class="btn btn-success" href="<?= Yii::$app->request->baseUrl.'/user/driverview?id='.$all_document->user_id.'&doc_id='.$all_document->id.'&status=1' ?>">Accept</a>
                                <?php } ?>
                                <a data-confirm="Are you sure want to reject this document?"  class="btn btn-danger" href="<?= Yii::$app->request->baseUrl.'/user/driverview?id='.$all_document->user_id.'&doc_id='.$all_document->id.'&status=0' ?>">Reject</a></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>
        </div>
    </div>
</div>
