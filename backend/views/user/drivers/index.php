<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\models\DriverCompany;

$language_id = 1;
$this->title = Yii::t('app', 'Drivers');
$this->params['breadcrumbs'][] = $this->title;

$all_cities = ArrayHelper::map(DriverCompany::find()->all(), 'company_id', 'company_name');
?>
<div class="driver-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'age',
                'nationality',
                [
                    'attribute'=>'is_available',
                    'filter'=>Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'is_available',
                        'data' => [1=>'Yes',0=>'No'],
                        'options' => [
                            'placeholder' => 'Please select ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],

                    ]),
                    'value'=>function($data){
                        return $data->is_available === 1 ? 'Yes' : 'No';
                    }
                ],
                [
                    'class' => 'yii\grid\FaActionColumn',
                    'template'=>'{update} &nbsp;&nbsp;{add_menu}',
                    'buttons'=>[
                         'update'=>function($url, $model, $key){
                            return Html::a(Html::tag('i','',['class'=>'far fa-edit']), ['user/driverupdate','id' => $model->id], ['data-pjax' => 0,'title'=>'Update','class'=>'btn btn-secondary']);
                        },
                        'add_menu'=>function($url, $model, $key){
                            return Html::a(Html::tag('i','',['class'=>'fas fa-shopping-cart']), ['order/driver','id' => $model->id], ['data-pjax' => 0,'title'=>'Orders','class'=>'btn btn-secondary']);
                        }
                    ]

                ],
            ]
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>