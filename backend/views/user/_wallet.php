<?php
use yii\widgets\ListView;
?>
<h3 class="text-danger">Total Balance: <?= $model->balance ?></h3>
<hr/>
<?=  ListView::widget([
    'layout' => "<div class='col-sm-12'>{summary}</div>\n {items}\n {pager}",
    'dataProvider' => $dataProvider,
    'itemView' => '_wallet_info',
    'summary' => false,
    'options' => [
        'tag' => 'div',
        'class' => '',
        'id' => 'list-wrapper',
    ],
    'itemOptions' => [
        'tag' => false
    ],
    'pager' => [
        'options' => ['class'=>'pagination justify-content-end'],
        'prevPageLabel' =>'Previous',
        'nextPageLabel' =>'Next',
        'pageCssClass' => 'page-item',
        'disabledPageCssClass' => 'page-link disabled',
        'linkOptions' => ['class' => 'page-link'],
    ]
]); ?>
