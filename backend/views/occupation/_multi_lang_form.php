<?php
use yii\helpers\Html;
use kartik\select2\Select2;
use common\models\Language;
use common\models\OccupationDescription;
use yii\bootstrap4\ActiveForm;


$languages	    = Language::find()->where(['status'=>1])->all();
if(!empty($model->id)){
	$descriptions	= OccupationDescription::find()->where(['occupation_id'=>$model->id])->all();
	foreach($descriptions as $description){
		$model->occupation_name[$description->language_id] = $description->occupation_name;
	}
}
?>

<div class="cuisine-form">
    <?php $form = ActiveForm::begin(); ?>
	<div class="row">

		<?php foreach($languages as $language){	?>
			<div class="col-sm-4">
				<?= $form->field($model, "occupation_name[{$language->language_id}]")->textInput(['maxlength' => true])->label(Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' (occupation name)') ?>
			</div>
		<?php } ?>	
		
		<div class="col-sm-2">
			<?= $form->field($model, 'status')->widget(Select2::classname(), [
					'data' => ['active' => 'Active', 'inactive' => 'Inactive'],
					
				])
			?>
		</div>
		
		<div class="col-sm-2" style="margin-top:28px;">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
		</div>
	</div>
	
    <?php ActiveForm::end(); ?>
</div>
<div class="clearfix"></div>
<hr/>