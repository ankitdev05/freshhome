<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OccupationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Occupation');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="occupation-index">

    <h3>Create <?= Html::encode($this->title) ?></h3>
    <?php Pjax::begin(); ?>
    <?= $this->render('_multi_lang_form', ['model' => $model]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'attribute'=>'occupation_name',
				'format'=>'raw',
				'value'=>function($data){
					$descriptions	= $data->occupationDescription;
					$html = '';
					foreach($descriptions as $description){
						$language	= $description->language;
						$html .= '<span class="lang_'.$language->language_id.'">'.Html::img(Yii::$app->request->baseUrl.'/../images/'.$language->image,['width' => 25]).' '.$description->occupation_name.'</span><br/>';
					}
					return $html;
				}
			],
            [
				'attribute'=>'status',
				'filter'=>Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => ['active' => 'Active', 'inactive' => 'Inactive'],
                    'options' => [
                        'placeholder' => 'Please select ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
			],
            'created_at:datetime',

            [
				'class' => 'yii\grid\FaActionColumn',
				'template'=>'{update}',
				'buttons'=>[
					'update'=>function($url, $model, $key){
						return Html::a(Html::tag('i','',['class'=>'far fa-edit']), ['occupation/index','id' => $model->id], ['data-pjax' => 0,'class'=>'btn btn-primary']);
					}
				]
			],
        ],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-link disabled',
            'prevPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'linkOptions' => ['class' => 'page-link'],
        ]
    ]); ?>
    <?php Pjax::end(); ?>
</div>
