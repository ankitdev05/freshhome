<?php

use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use common\models\Currency;

$all_currency = ArrayHelper::map(Currency::find()->orderBy('name')->all(), 'code', 'name');
/* @var $this yii\web\View */
/* @var $model common\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="countries-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'iso_code_2')->textInput(['maxlength' => true])->hint("( <a target='_blank' href='https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes'> 2 digit ISO code refer list</a> )") ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'iso_code_3')->textInput(['maxlength' => true])->hint("( <a target='_blank' href='https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes'> 3 digit ISO code refer list</a> )") ?>
        </div>

        <div class="col-sm-4">
            <?=
            $form->field($model, 'currency_code')->widget(Select2::classname(), [
                'data' => $all_currency,
            ])
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'symbol_left')->textInput(['maxlength' => true])->hint("( <a target='_blank' href='https://en.wikipedia.org/wiki/Currency_symbol'>  Currency Symbol</a> )") ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'symbol_right')->textInput(['maxlength' => true])->hint("( <a target='_blank' href='https://en.wikipedia.org/wiki/Currency_symbol'>  Currency Symbol</a> )") ?>
        </div>

        <div class="w-100"></div>

        <div class="col-sm-4">
            <?= $form->field($model, 'tax_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'tax')->textInput(['maxlength' => true])->label("Tax (%)") ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'delivery_charges')->textInput(['type' => 'number','step' => '0.01','min' => 1]) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => [1 => 'Active',0 => 'Inactive'],

            ])
            ?>
        </div>
        <div class="w-100"></div>
        <div class="form-group col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>