<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Plans */

$this->title = Yii::t('app', 'Update Plans: {name}', [
    'name' => $model->plan_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->plan_name, 'url' => ['view', 'id' => $model->plan_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="plans-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
