<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\Plans */
/* @var $form yii\widgets\ActiveForm */

$all_months = array_combine(range(1,12), range(1,12));
?>

<div class="plans-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-sm-4">
        <?= $form->field($model, 'plan_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'months')->widget(Select2::classname(), [
            'data' => $all_months,
        ]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-12">
        <?= $form->field($model, 'plan_description')->widget(TinyMce::className(), [
            'options' => ['rows' => 12],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ],
                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            ]
        ]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'btree_plan_id')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'type')->widget(Select2::classname(), [
            'data' => ['supplier'=>'Supplier Plan','delivery'=>'Delivery Plan'],
        ]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'status')->widget(Select2::classname(), [
            'data' => ['active'=>'Active','inactive'=>'Inactive'],
        ]) ?>
    </div>
    <div class="w-100"></div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
