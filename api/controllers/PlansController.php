<?php
namespace api\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use	yii\filters\ContentNegotiator;

use common\models\Plans;
use common\models\User;
use common\models\CreditCards;
use common\models\Braintree;
class PlansController extends ActiveController
{
    public $statusCode = 200;
    public $modelClass = 'common\models';
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']	= [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['error','index','all-plans'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        return $behaviors;
    }
    public function actions(){
        $actions			= parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }
    protected function verbs(){
        return [
            'home'	=> ['GET'],
            'all-plans'	=> ['GET'],
            'subscribe'	=> ['POST'],
        ];
    }
    public function actionIndex()
    {
        $json['message'] = 'Welcome to plan page.';
        return $json;
    }
    public function actionAllPlans($type='supplier'){
        $user_model		= new User();
        $headers		= getallheaders();
        $token			= '';
        if(isset($headers['Authorization']))
            $token	= str_replace("Bearer ","",$headers['Authorization']);
        $user_info		= $user_model->findIdentityByAccessToken($token);

        $model = new Plans();
        $results = $model->find()->select(["plan_id","plan_name","plan_description","months","price",'btree_plan_id'])->where(['status'=>'active','type' => $type])->asArray()->all();
        if(!empty($results)){
            $json['data'] = $results;
        }else{
            $json['error']['msg']	= 'No plan found.';
            $this->statusCode		= 401;
        }
        if(!empty($user_info))
        $json['subscription_data'] = $user_info->subscriptionData;
        $json['code']			= $this->statusCode;
        return $json;
    }

    public function actionSubscribe(){
        $plan_id = $_POST['plan_id'] ?? 0;
        $card_id = $_POST['card_id'] ?? 0;
        $user_info = Yii::$app->user->identity;

        $model = new Plans();
        $plan_info = $model->findOne($plan_id);

        $credit_card_model = new CreditCards();
        $cc_info = $credit_card_model->find()->where(['id' => $card_id,'user_id' => $user_info->id])->one();
        if(!$plan_info){
            $json['error']['msg']	= 'No plan found.';
            $this->statusCode		= 401;
        }elseif(!$cc_info){
            $json['error']['msg'] = 'Please select valid card.';
            $this->statusCode		= 401;
        }else{
            $braintree_model = new Braintree();
            $time_stamp  = time();
            if($user_info->subscription_end>$time_stamp)
                $time_stamp = $user_info->subscription_end;
            $response = $braintree_model->subscriptionCreate($cc_info->token,$plan_info->btree_plan_id,$time_stamp,$user_info->id,$plan_info);
            if($response['error']==false)
                $json['success']['msg']	= "Thanks for the subscription.";
            if($response['response']['subscription']['status']){
                $json['success']['msg']	= "Thanks for the subscription. Your subscription status is ".$response['response']['subscription']['status'];
            }
            else{
                $json['error']['msg'] = $response['message'];
                $this->statusCode		= 401;
            }
        }


        $json['code']			= $this->statusCode;
        return $json;
    }
    public function actionCancel(){
        $user_info = Yii::$app->user->identity;
        $braintree_model = new Braintree();
        if(!empty($user_info->subscription_id)){
            $braintree_model->subscriptionCancel($user_info->subscription_id);
            $json['success']['msg']	= "Your subscription has been cancelled.";
            $json['code']			= $this->statusCode;
        }else{
            $json['error']['msg'] = 'Your subscription is already cancelled.';
            $this->statusCode		= 401;
        }

        return $json;
    }
}