<?php
namespace api\controllers;

use Codeception\Module\Db;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use	yii\filters\ContentNegotiator;

use common\models\User;
use common\models\Order;
use common\models\Apiuser;
use common\models\Library;
use common\models\ContactMessage;
use common\models\DriverCompany;
use common\models\Notifications;
use common\models\DriverCompanyNumbers;
use common\models\DriverOrderStatus;
use common\models\UserToRoles;
use common\models\OrderTotalSupplier;
use common\models\OrderTotal;
use common\models\Wallet;
use api\models\Cart;
use yii\db\Expression;

class DriverController extends ActiveController
{
    public $statusCode = 200;
    public $modelClass = 'common\models';
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']	= [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['error','index','companies','login','check-number'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        return $behaviors;
    }
    public function actions(){
        $actions			= parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }
    protected function verbs(){
        return [
            'home'	=> ['GET'],
            'index'	=> ['GET'],
            'profile'	=> ['GET'],
            'individual-profile' => ['GET'],
            'order'=> ['GET'],
            'orders'=> ['GET'],
            'companies'=> ['GET'],
            'check-number'=> ['POST'],
            'update'=> ['POST'],
            'contact'=> ['POST'],
            'accept'=> ['POST'],
            'deliver'=> ['POST'],
            'orderupdate'=> ['POST'],
            'get-orders'=> ['GET'],
            'complete'=> ['POST'],
        ];
    }
    public function actionIndex()
    {
        $json['message'] = 'Welcome to driver page.';
        return $json;
    }
    public function actionCompanies(){
        $companies              = DriverCompany::find()->all();
        if(!empty($companies)){
            $data = [];
            foreach($companies as $company)
                $data[] = ['company_id'=>$company['company_id'],'company_name'=>$company['company_name']];
            $json['data'] = $data;
        }else{
            $json['error']['msg']	= 'No company found.';
            $this->statusCode		= 401;
        }

        $json['code']			= $this->statusCode;
        return $json;
    }
    public function actionCheckNumber(){
        $model = new DriverCompanyNumbers();
        $post_data['DriverCompanyNumbers'] 		= Yii::$app->request->post();
        $post_data['DriverCompanyNumbers']['token'] = 'aa';
        if ($model->load($post_data) && $model->validate()) {
            $d_model = new DriverCompanyNumbers();
          $result = $d_model->find()->where(['company_id'=>$model->company_id,'phone_number'=>$model->phone_number])->one();
          if(!empty($result)){
              $user_info = User::find()->where(['phone_number'=>$model->phone_number])->one();
              if(empty($user_info))
                  $user_data = $this->registration($result);
              else
                  $user_data = $user_info;
              if(isset($post_data['DriverCompanyNumbers']['device_token'])){
                  $user_info->device_token = $post_data['DriverCompanyNumbers']['device_token'];
                  $user_info->save();
              }


              $json['data']       = $result->profile;
              $json['data']['access_token'] = $user_data->auth_key;
              $json['data']['driver_id']    = $user_data->id;
          }else{
              $json['error']['msg']	= 'Invalid phone number.';
              $this->statusCode		= 401;
          }
        }else{
            $all_errors				= [];
            $errors					= $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode			= 401;
        }

        $json['code']					= $this->statusCode;
        return $json;
    }

    public function actionGetOrders($latitude,$longitude){
        $user_data 			= Yii::$app->user->identity;
        $driver_id          = $user_data->driver_id;
        $result             = DriverCompanyNumbers::findOne($driver_id);
        if(!empty($result)){
          $orders = Order::find()->select(new Expression("*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $latitude - supp.latitude) *  pi()/180 / 2), 2) +COS( $longitude * pi()/180) * COS(supp.latitude * pi()/180) * POWER(SIN(( $longitude - supp.longitude) * pi()/180 / 2), 2) ))) as distance"))->joinWith('supplier as supp')->where(['and',
                ['order_status_id' => 4],
                ['order_category' => 1],
                ['supp.status' => 10],
                ['supp.availability' => 'online'],
                ['is','order_accepted',null]
            ])->having("distance <= 30")->orderBy("distance")->limit(30)->all();
          if(!empty($orders)){
            $data = [];
            foreach($orders  as $order)
                $data[] = $order->driverOrderInfo;
              $json['data'] = $data;
          }else{
              $json['error']['msg']	= 'No order found.';
              $this->statusCode			= 401;
          }
        }else{
            $json['error']['msg']	= 'You\'re not authorized to access this page';
            $this->statusCode			= 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }

    public function actionProfile(){
        $user_data 			= Yii::$app->user->identity;
        $driver_id          = $user_data->driver_id;
        $result             = DriverCompanyNumbers::findOne($driver_id);

        if(!empty($result)){
            $profile = $result->profile;
        }else
            $profile = $user_data->driverInfo;

        $json['data']       = $profile;
        $json['data']['access_token'] = $user_data->auth_key;
        $json['data']['driver_id']    = $user_data->id;
        $json['code']					= $this->statusCode;
        return $json;

    }
    public function actionIndividualProfile(){
        $user_data = Yii::$app->user->identity;
        $driver_info = $user_data->driverInfo;
        $json['data'] = $driver_info;
        $json['code'] = $this->statusCode;
        return $json;
    }

    public function actionUpdate(){
        $post_data['DriverCompanyNumbers'] 	= Yii::$app->request->post();
        $user_data 			= Yii::$app->user->identity;
        $driver_id          = $user_data->driver_id;
        if(isset($post_data['DriverCompanyNumbers']['device_token']) && !empty($post_data['DriverCompanyNumbers']['device_token'])){
            $user_data->device_token = $post_data['DriverCompanyNumbers']['device_token'];
            $user_data->save();
        }

        $model              = new DriverCompanyNumbers();

        $result             = $model::findOne($driver_id);
        $result->scenario = 'update';
        $file_data				= [];
        if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic'])){
            foreach($_FILES['profile_pic'] as $key=>$pic_data){
                $file_data[$key]	= ['profile_pic'=>$pic_data];
            }
        }
        if(!empty($file_data))
            $_FILES['DriverCompanyNumbers'] = $file_data;
        if ($result->load($post_data) && $result->validate()) {
            $profile_pic 		= UploadedFile::getInstance($result, 'profile_pic');
            if(!empty($profile_pic)){
                $library   			 = new Library();
                $result->profile_pic  = $library->saveFile($profile_pic,'users');
                $result->image_approval = 0;
            }
            $result->save();
            $json['data']       = $result->profile;
            $json['data']['access_token'] = $user_data->auth_key;
            $json['data']['driver_id']    = $user_data->id;

        }else{
            $all_errors				= [];
            $errors					= $result->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    protected function registration($driver){
        $model		            = new Apiuser();
        $model->password         = 'driverFresh45';
        $pass_hash 			    = $model->password;
        $model->setPassword($pass_hash);
        $model->generateAuthKey();
        $model->phone_number    = $driver->phone_number;
        $model->username    = $driver->token;
        $model->email       = $driver->token.'@gmail.com';
        $model->email_verify    = 1;
        $model->status		    = 10;
        $model->is_user		    = 'yes';
        $model->is_supplier	    = 'no';
        $model->role            = 4;
        $model->driver_id       = $driver->id;
        $model->ip_address		= Yii::$app->getRequest()->getUserIP();
        $model->last_login		= time();
        $model->last_login_ip	= Yii::$app->getRequest()->getUserIP();
        if($model->save()){
            $model->updateUserData;
            $user_to_role	= new UserToRoles();
            $user_to_role->user_id	= $model->id;
            $user_to_role->role_id	= $model->role;
            $user_to_role->save();
            return $model;
        }
        return null;
    }
    public function actionContact(){
        $model		= new ContactMessage();
        $user_id	= Yii::$app->user->identity->id;
        $post_data['ContactMessage'] = Yii::$app->request->post();
        $model->user_id	= $user_id;
        $post_data['ContactMessage']['role'] ='Driver';
        if ($model->load($post_data) && $model->validate()) {
            if($model->save()){
                $model->sendEmail;
                $json['success']['msg']	= "Thank you for contacting us. We will respond to you as soon as possible.";
            }
        }else{
            $all_errors				= [];
            $errors					= $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode		= 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionDeliver(){
        $post_data = Yii::$app->request->post();
        $user_id	= Yii::$app->user->identity->id;
        $order_id   = isset($post_data['order_id']) ? $post_data['order_id'] : 0;
        $order_info = Order::find()->where(['and',['driver_id'=>$user_id],['order_accepted'=>1],['order_id'=>$order_id]])->one();
        if($order_info){
            $order_info->order_status_id =5;
            $order_info->save();

            $library = new Library();
            $order_info1['user_id']        = $order_info->user_id;
            $order_info1['role']            = 1;

            $order_info1['from_user']      = $user_id;
            $order_info1['type']           = 'order';
            $order_info1['config_name']    ='Your order has been completed.';
            $order_info1['config_value']['order_id']= $order_id;
            $order_info1['config_value']['message']=  $order_info1['config_name'];

            $library->saveNotification($order_info1);
            $data['driver_id'] = $user_id;
            $data['order_id']   = $order_id;
            $data['order_status'] = 'Completed';
            $library->updateDriverHistory($data);

            $json['success']['msg']	= "Your order has been completed successfully..";


        }else{
            $json['error']['msg']	= 'Something went wrong.';
            $this->statusCode		= 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    public function actionAccept(){
        $post_data = Yii::$app->request->post();
        $user_id	= Yii::$app->user->identity->id;
        $order_id   = isset($post_data['order_id']) ? $post_data['order_id'] : 0;
        $order_info = Order::find()->where(['and',['order_status_id'=>4],['is','order_accepted',null],['order_id'=>$order_id]])->one();
        if($order_info){
            $order_info->driver_id = $user_id;
            $order_info->order_accepted = 1;
            $order_info->save();

            $library = new Library();
            $order_info1['user_id']        = $user_id;
            $order_info1['role']            = 4;
            $order_info1['status']          = 0;
            $order_info1['from_user']      = $user_id;
            $order_info1['type']           = 'order';
            $order_info1['config_name']    ='Driver accepted the order.';
            $order_info1['config_value']['order_id']= $order_id;
            $order_info1['config_value']['message']=  $order_info1['config_name'];

            //send notification to supplier when order accepted by driver
            $library = new Library();
            $order_info1['user_id']        = $order_info->supplier_id;
            $order_info1['role']            = 2;
            $order_info1['from_user']      = $user_id;
            $order_info1['type']           = 'order';
            $order_info1['config_name']    ='Driver accepted you order#'.$order_id;
            $order_info1['config_value']['order_id']= $order_id;
            $order_info1['config_value']['message']=  $order_info1['config_name'];

            $library->saveNotification($order_info1);
            $data['driver_id'] = $user_id;
            $data['order_id']   = $order_id;
            $data['order_status'] = 'Accepted';
            $library->updateDriverHistory($data);

            $json['success']['msg']	= "Your order has been accepted successfully..";
        }else{
            $json['error']['msg']	= 'Something went wrong.';
            $this->statusCode		= 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    public function actionReject(){
        $post_data = Yii::$app->request->post();
        $user_id	= Yii::$app->user->identity->id;
        $order_id   = isset($post_data['order_id']) ? $post_data['order_id'] : 0;
        $order_info = Order::find()->where(['and',['driver_id'=>$user_id],['is','order_accepted',null],['order_id'=>$order_id]])->one();
        if($order_info){
            $order_info->driver_id = null;
            $order_info->order_accepted = null;
            $order_info->save();


            $library = new Library();
            $order_info1['user_id']        = $user_id;
            $order_info1['role']            = 4;
            $order_info1['status']          = 0;
            $order_info1['from_user']      = $user_id;
            $order_info1['type']           = 'order';
            $order_info1['config_name']    ='Driver rejected the order.';
            $order_info1['config_value']['order_id']= $order_id;
            $order_info1['config_value']['message']=  $order_info1['config_name'];

            $library->saveNotification($order_info1);
            $data['driver_id'] = $user_id;
            $data['order_id']   = $order_id;
            $data['order_status'] = 'Rejected';
            $library->updateDriverHistory($data);
           //send notification to another driver
            $order_info->driver;

            $json['success']['msg']	= "Your order has been rejected successfully..";
        }else{
            $json['error']['msg']	= 'Something went wrong.';
            $this->statusCode		= 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    public function actionOrders(){
        $json 		= [];
        $page_size	= 30;
        $user_info = Yii::$app->user->identity;
        $user_id	= $user_info->id;
        $result             = DriverCompanyNumbers::findOne($user_info->driver_id);
        if(!empty($result)){
            $profile = $result->profile;
        }else
            $profile = $user_info->driverInfo;
        $query		= Order::find()->where(['driver_id'=>$user_id,'order_accepted'=>1]);
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC,
                ]
            ],
        ]);
        $count			= $dataProvider->getTotalCount();
        if($count>0){
            $all_orders = [];
            $orders	= $dataProvider->getModels();
            foreach($orders as $order)
                $all_orders[]	= $order->driverOrderInfo;

            $json['data']		= $all_orders;
            $json['driver_data']		= $profile;
        }else{
            $json['error']['msg']	= 'No order found.';
            $json['driver_data']		= $profile;
            $this->statusCode		= 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    public function actionOrder($order_id){
        $user_id			= Yii::$app->user->identity->id;
        $o_status           = DriverOrderStatus::find()->where(['driver_id'=>$user_id,'order_id'=>$order_id,'order_status'=>'Rejected'])->one();
        if(!empty($o_status))
            $result				= Order::findOne(['order_id'=>$order_id]);
        else
            $result				= Order::findOne(['order_id'=>$order_id]);
            //$result				= Order::findOne(['order_id'=>$order_id,'driver_id'=>$user_id]);

        if(empty($result)){
            $json['error']['msg']	= 'No order found.';
            $this->statusCode = 401;
        }else{
            $json['order_info'] = $result->driverOrderInfo;
            $json['kitchen_location']	= $result->kitchenAddress;
        }


        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionComplete(){
        $post_data =  Yii::$app->request->post();
        $user_info = Yii::$app->user->identity;
        $user_id = $user_info->id;
        $library = new Library();
        $order_id = $post_data['order_id'] ?? 0 ;

        $result             = DriverCompanyNumbers::findOne($user_info->driver_id);
        $is_comp_driver = false;
        if(!empty($result)){
            $profile = $result->profile;
            $is_comp_driver = true;
        }else
            $profile = $user_info->driverInfo;

        $result = Order::findOne(['order_status_id'=>10,'order_id'=>$order_id,'driver_id'=>$user_id,'order_accepted'=>1]);
        if(!empty($result)){
            $result->scenario = 'order_complete';
            $result->order_status_id = 5;
            $file_data				= [];
            if(isset($_FILES['order_image']) && !empty($_FILES['order_image'])){
                foreach($_FILES['order_image'] as $key=>$pic_data){
                    $file_data[$key]	= ['order_image'=>$pic_data];
                }
            }
            if(!empty($file_data))
                $_FILES['Order'] = $file_data;
            $order_image = UploadedFile::getInstance($result, 'order_image');
            if(!empty($order_image)){
                $library   			 = new Library();
                $result->order_image  = $library->saveFile($order_image,'orders');
            }

            if($result->validate() && $result->save()){
                // Start send notification to supplier when order is delivered
                $order_info['user_id']        = $result->supplier_id;
                $order_info['role']           = 2;
                $order_info['from_user']      = $user_id;
                $order_info['type']           = 'order';

                $order_info['config_name']    = 'Your order #'.$order_id.' has been delivered.';
                $order_info['config_value']['order_id']= $order_id;
                $order_info['config_value']['message']= 'Your order #'.$order_id.' has been delivered.';
               $library->saveNotification($order_info);
                // End send notification to supplier when order is delivered

                // Start send notification to user when order is delivered
                $order_info['user_id']        = $result->user_id;
                $order_info['role']           = 1;
                $order_info['from_user']      = $user_id;
                $order_info['type']           = 'order';

                $order_info['config_name']    = 'Your order #'.$order_id.' has been delivered.';
                $order_info['config_value']['order_id']= $order_id;
                $order_info['config_value']['message']= 'Your order #'.$order_id.' has been delivered.';
                $library->saveNotification($order_info);
                // End send notification to user when order is delivered

                // Start supplier wallet
                $supplier_total = OrderTotalSupplier::find()->where(['code' => 'total_earning' ,'order_id' => $order_id])->count();
                if($supplier_total==0){
                    $OrderTotal = OrderTotal::find()->where(['order_id' => $order_id, 'code' => 'sub_total'])->one();
                    $cart  = new Cart();
                    $totals = $cart->getSupplierCartTotal($OrderTotal->value);
                    foreach($totals as $k=>$total_amount){
                        $order_total = new OrderTotalSupplier();
                        $order_total->order_id		= $order_id;
                        $order_total->code			= $total_amount['code'];
                        $order_total->title			= $total_amount['label'];
                        $order_total->value			= $total_amount['total_value'];
                        $order_total->sort_order	= ($k+1);
                        $order_total->save();
                    }

                }
                $supplier_total = OrderTotalSupplier::find()->where(['code' => 'total_earning' ,'order_id' => $order_id])->one();
                $supplier_total_amount = $supplier_total->value;

                $model = new Wallet();
                $model->user_id = $result->supplier_id;
                $model->currency = config_default_currency;
                $model->amount = $supplier_total_amount;
                $model->type = 'received';
                $model->user_role = 2;
                $model->message = 'Received from order #'.$order_id;
                $model->ip_address = Yii::$app->getRequest()->getUserIP();
                if($model->save()){
                    $user_info->updateBalance($model);
                }
                // End supplier wallet

                // Start driver wallet
                $total = OrderTotal::find()->where(['code' => 'delivery_charges' ,'order_id' => $order_id])->one();
                if(!empty($total) && $is_comp_driver==false){
                    $model = new Wallet();
                    $model->user_id = $user_id;
                    $model->currency = config_default_currency;
                    $model->amount = $total->value;
                    $model->type = 'received';
                    $model->user_role = 4;
                    $model->message = 'Received from order #'.$order_id;
                    $model->ip_address = Yii::$app->getRequest()->getUserIP();
                    if($model->save()){
                        $user_info->updateBalance($model);
                    }
                }
                // Start driver wallet
                $json['success']		= 'Your order has been delivered successfully.';
            }else{
                $all_errors = [];
                $errors	 = $result->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }
        }else{
            $json['error']['msg']	= 'No order found.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;

    }
    public function actionOrderupdate(){
        $post_data =  Yii::$app->request->post();
        $order_id = $post_data['order_id'] ?? 0;
        $status_id = $post_data['status_id'] ?? 0;
        $library = new Library();
        $user_info			= Yii::$app->user->identity;
        $result             = DriverCompanyNumbers::findOne($user_info->driver_id);
        if(!empty($result)){
            $profile = $result->profile;
        }else
            $profile = $user_info->driverInfo;

        $user_id = $user_info->id;
        $result				= Order::findOne(['order_id'=>$order_id,'driver_id'=>$user_id,'order_accepted'=>1]);
        if(!empty($result)){
            $result->order_status_id = $status_id;
            if(isset($_FILES['parcel_image']) && !empty($_FILES['parcel_image'])){
                foreach($_FILES['parcel_image'] as $key=>$pic_data){
                    $file_data[$key]	= ['parcel_image'=>$pic_data];
                }
            }
            if(!empty($file_data))
                $_FILES['Order'] = $file_data;
            $parcel_image = UploadedFile::getInstance($result, 'parcel_image');
            if(empty($parcel_image)){
                $json['error']['msg']	= 'Please select parcel image.';
                $this->statusCode = 401;
                $json['code']	= $this->statusCode;
                return $json;
            }
            if(!empty($parcel_image)){
                $result->parcel_image  = $library->saveFile($parcel_image,'parcel');
            }


//            if($result->order_status_id==5){
//                // Start send notification to supplier when order is delivered
//                $order_info['user_id']        = $result->supplier_id;
//                $order_info['role']           = 2;
//                $order_info['from_user']      = $user_id;
//                $order_info['type']           = 'order';
//
//                $order_info['config_name']    = 'Driver delivered your order.';
//                $order_info['config_value']['order_id']= $order_id;
//                $order_info['config_value']['message']= 'Driver delivered your order.';
//                $library->saveNotification($order_info);
//                // End send notification to supplier when order is delivered
//
//                // Start send notification to user when order is delivered
//                $order_info['user_id']        = $result->user_id;
//                $order_info['role']           = 1;
//                $order_info['from_user']      = $user_id;
//                $order_info['type']           = 'order';
//
//                $order_info['config_name']    = 'Your order has been delivered.';
//                $order_info['config_value']['order_id']= $order_id;
//                $order_info['config_value']['message']= 'Your order has been delivered.';
//                $library->saveNotification($order_info);
//                // End send notification to user when order is delivered
//            }
            if($result->order_status_id==10){
                // Start send notification to user when order is out for delivery
                $order_info['user_id']        = $result->user_id;
                $order_info['role']           = 1;
                $order_info['from_user']      = $user_id;
                $order_info['type']           = 'order';

                $order_info['config_name']    = 'Your order is out for delivery.';
                $order_info['config_value']['order_id']= $order_id;
                $order_info['config_value']['message']= 'Your order is out for delivery.';
                $library->saveNotification($order_info);

                $order_info = [];
                $order_info['user_id']        = $result->supplier_id;
                $order_info['role']           = 2;
                $order_info['from_user']      = $user_id;
                $order_info['type']           = 'order';

                $order_info['config_name']    = $profile['name']. ' picked the order from your store.';
                $order_info['config_value']['order_id']= $order_id;
                $order_info['config_value']['message']= $profile['name']. ' picked the order from your store.';
                $library->saveNotification($order_info);


                // End send notification to user when order is out for delivery
            }
            if($result->save()){
                $json['success']		= 'Your order has been updated successfully.';
            }
        }else{
            $json['error']['msg']	= 'No order found.';
            $this->statusCode = 401;
        }

        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionHome(){
        $user_info =Yii::$app->user->identity;
        $user_id	= $user_info->id;
        $result     = DriverCompanyNumbers::findOne($user_info->driver_id);
        if(!empty($result)){
            $profile = $result->profile;
        }else
            $profile = $user_info->driverInfo;


        $order_info	= Order::find()->where(['and',['driver_id'=>$user_id],['!=','order_status_id',5],['order_accepted'=>1]])->all();
        if(!empty($order_info)){
            $json['unread_notifications']   = Notifications::find()->where(['status'=>1,'user_id'=>$user_id,'role'=>4,'is_read'=>0])->count();
            foreach($order_info as $k=>$order)
                $json['data'][] = $order->driverOrderInfo;


            $json['driver_data']	= $profile;

        }else{
            $json['driver_data']	= $profile;
            $json['error']['msg']	= 'No order found.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    public function actionNearMeKitchens($lat,$long){
        $user_info =Yii::$app->user->identity;
        $user_id	= $user_info->id;
        $query      = User::find()->select(new Expression("*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(latitude * pi()/180) * POWER(SIN(( $long - longitude) * pi()/180 / 2), 2) ))) as distance  "))->where(['and',['is_supplier'=>'yes'],['!=','id',$user_id],['!=','longitude',''],['!=','latitude','']])->having("distance <= 15")->orderBy("distance")->limit("15");
        $result = $query->all();
        if(!empty($result)){
            $as_array = $query->asArray()->all();
            foreach($result as $k=>$kitchen){
                $json['data'][$k]	= $kitchen->kitchenAddress;
                $json['data'][$k]['distance'] = $as_array[$k]['distance'];
            }

        }else{
            $json['error']['msg']	= 'No kitchen  found.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;


    }

}