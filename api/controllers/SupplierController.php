<?php
namespace api\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use	yii\filters\ContentNegotiator;

use common\models\Order;
use common\models\Library;
use common\models\OrderCancel;
use common\models\OrderHistory;

class SupplierController extends ActiveController
{
    public $statusCode = 200;
    public $page_size	= 20;
    public $modelClass = 'common\models';
    public $language_id;
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']	= [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['error','index'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        return $behaviors;
    }
    public function actions(){
        $this->language_id = isset($_REQUEST['language_id']) ? (int)$_REQUEST['language_id'] : '1';
        $actions			= parent::actions();

        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }
    protected function verbs(){
        return [
            'index'				=> ['GET'],
            'orders'			=> ['GET'],
            'order'				=> ['GET'],
            'cancel-order' 		=> ['POST'],
        ];
    }
    public function actionIndex()
    {
        $json['message'] = 'Welcome to supplier module';
        return $json;
    }
    public function actionOrders($type=1){
        $user_info		= Yii::$app->user->identity;
        $supplier_id	= $user_info->id;
        if($type==1){
//            $p_query		= Order::find()->where(['supplier_id'=>$supplier_id])
//                ->andWhere(['and',['date'=>date('Y-m-d')],['NOT IN','order_status_id',[2,5]]])
//                ->orWhere(['and',['is','date',null],['NOT IN','order_status_id',[2,5]],['supplier_id'=>$supplier_id],['=',new Expression("FROM_UNIXTIME(created_at,'%Y-%m-%d')"),new Expression('CURDATE()')]])
//                ->orWhere(['and',['NOT IN','order_status_id',[2,5]],['supplier_id'=>$supplier_id]]);
            //$pre_query		= Order::find()->where(['and',['supplier_id'=>$supplier_id],['>','date',new Expression('CURDATE()')]]);
            $p_query = $user_info->getFoodSupplierPendingOrders();
            $c_query = $user_info->getFoodSupplierCompleteOrder();


            $pre_query = $user_info->getFoodSupplierPreOrder();

            $p_dataProvider	=  new ActiveDataProvider([
                'query' => $p_query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'order_id' => SORT_DESC,
                    ]
                ],
            ]);
            $c_dataProvider	=  new ActiveDataProvider([
                'query' => $c_query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'order_id' => SORT_DESC,
                    ]
                ],
            ]);
            $pre_dataProvider	=  new ActiveDataProvider([
                'query' => $pre_query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'order_id' => SORT_DESC,
                    ]
                ],
            ]);

            $p_dataProvider->pagination->pageParam = 'p_page';
            $c_dataProvider->pagination->pageParam = 'c_page';
            $pre_dataProvider->pagination->pageParam = 'pre_page';
            $p_count			= $p_dataProvider->getTotalCount();
            $c_count			= $c_dataProvider->getTotalCount();
            $pre_count			= $pre_dataProvider->getTotalCount();

            $total_p_pages		= ceil($p_count/$this->page_size);
            $total_c_pages		= ceil($c_count/$this->page_size);
            $total_pre_pages	= ceil($pre_count/$this->page_size);

            $pending_orders		= [];
            $completed_orders	= [];
            $pre_orders	        = [];
            if($p_count>0){
                $pending_orders_	= $p_dataProvider->getModels();
                foreach($pending_orders_ as $p_orders){
                    $pending_orders[]	= $p_orders->orderInfo;
                }
            }
            if($c_count>0){
                $completed_orders_	= $c_dataProvider->getModels();
                foreach($completed_orders_ as $c_orders){
                    $completed_orders[]	= $c_orders->orderInfo;
                }
            }
            if($pre_count>0){
                $pre_orders_	= $pre_dataProvider->getModels();
                foreach($pre_orders_ as $pre_o){
                    $pre_orders[]	= $pre_o->orderInfo;
                }
            }

            $json['pre_orders']	                    = $pre_orders;
            $json['pending_orders']		            = $pending_orders;
            $json['completed_orders']	            = $completed_orders;
            $json['total_pending_orders_pages']		= $total_p_pages;
            $json['total_completed_orders_pages']	= $total_c_pages;
            $json['total_pre_orders_pages']		    = $total_pre_pages;


            $json['code']				= $this->statusCode;
            return $json;
        }else{
            $p_query		= Order::find()->where(['and',
                    ['supplier_id'=>$supplier_id],
                    ['NOT IN','order_status_id',[2,5]]
                ]);
            $p_dataProvider	=  new ActiveDataProvider([
                'query' => $p_query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'order_id' => SORT_DESC,
                    ]
                ],
            ]);

            $c_query = Order::find()->where(['supplier_id'=>$supplier_id,'order_status_id'=>5]);
            $c_dataProvider	=  new ActiveDataProvider([
                'query' => $c_query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'order_id' => SORT_DESC,
                    ]
                ],
            ]);
            $c_dataProvider->pagination->pageParam = 'c_page';
            $p_dataProvider->pagination->pageParam = 'p_page';

            $c_count = $c_dataProvider->getTotalCount();
            $p_count = $p_dataProvider->getTotalCount();

            $pending_orders		= [];
            $completed_orders	= [];

            if($c_count>0){
                $completed_orders_	= $c_dataProvider->getModels();
                foreach($completed_orders_ as $c_orders){
                    $completed_orders[]	= $c_orders->orderInfo;
                }
            }
            if($p_count>0){
                $pending_orders_	= $p_dataProvider->getModels();
                foreach($pending_orders_ as $p_orders){
                    $pending_orders[]	= $p_orders->orderInfo;
                }
            }

            $json['pending_orders']		            = $pending_orders;
            $json['completed_orders']	            = $completed_orders;

            $json['code']				= $this->statusCode;
            return $json;
        }
    }
    public function actionOrder($order_id){
        $user_id			= Yii::$app->user->identity->id;
        $result				= Order::findOne(['order_id'=>$order_id,'supplier_id'=>$user_id]);

        if(empty($result)){
            $json['error']['msg']	= 'No order found.';
            $this->statusCode = 401;
        }else
            $json['order_info'] = $result->orderInfo;

        $json['code']		= $this->statusCode;
        return $json;

    }

    public function actionCancelOrder(){
        $user_info	= Yii::$app->user->identity;
        $supplier_id = $user_info->id;
        $post_data['OrderCancel'] = Yii::$app->request->post();

        $model = new OrderCancel();
        $model->cancelled_by = $supplier_id;
        $model->by_supplier = 'yes';
        $model->by_user = 'no';
        if ($model->load($post_data) && $model->validate()) {
            $order_info = Order::find()->where(['AND',['NOT IN','order_status_id',[2,3,7,5,10]],['supplier_id' => $supplier_id],['order_id' => $model->order_id]])->one();
            if(!empty($order_info)){
                $order_info->order_status_id = 2;
                if($order_info->save()) {
                    $model->save();
                    $order_history = new OrderHistory();
                    $order_history->order_id = $model->order_id;
                    $order_history->order_status_id = 2;
                    $order_history->notify = 1;
                    $order_history->comment = 'Your order has been cancelled.';
                    $order_history->save();
                    $json['success'] = 'Your order has been cancelled.';

                    $library = new Library();
                    $order_info1 = [];
                    $order_info1['user_id']  = $order_info->user_id;
                    $order_info1['role'] = 1;
                    $order_info1['from_user']  = $order_info->supplier_id;
                    $order_info1['type'] = 'order';
                    $order_info1['config_name'] = 'Order # '.$order_info->order_id.' has been cancelled by supplier';
                    $order_info1['config_value']['order_id']= $order_info->order_id;
                    $order_info1['config_value']['message']= 'Order # '.$order_info->order_id.' has been cancelled by supplier';
                    $library->saveNotification($order_info1);
                }
            }else{
                $json['error']['msg']	= 'You don\'t have permission to perform this action.' ;
                $this->statusCode = 401;
            }
        }else{
            $all_errors	 = [];
            $errors	 = $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
}