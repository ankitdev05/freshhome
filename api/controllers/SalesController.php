<?php
namespace api\controllers;

use Yii;
use yii\helpers\Url;
use yii\db\Expression;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use	yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

use common\models\User;
use common\models\Videos;
use common\models\Library;
use common\models\Apiuser;
use common\models\UserToRoles;
use common\models\HelpRequests;
use common\models\SalesPerson;
use common\models\OrderStatus;
use common\models\Notifications;
use common\models\MainCategories;
use common\models\VideosWatchlist;
use common\models\SalesPersonRating;
use common\models\CancelledHelpRequests;

class SalesController extends ActiveController{
    public $statusCode = 200;
    public $page_size = 30;
    public $modelClass = 'common\models';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']	= [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['error','index','register','qrcode','help-text','sales-person-rating','sales-person-rating','cancel-request-by-supplier'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        return $behaviors;
    }
    /**
     * {@inheritdoc}
     */
    public function actions(){
        $actions			= parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }
    /**
     * {@inheritdoc}
     */
    protected function verbs(){
        return [
            'jobsdone'=> ['GET'],
            'complete-request'=> ['GET'],
            'accept-request'=> ['GET'],
            'decline-request'=> ['GET'],
            'reject-request'=> ['GET'],
            'myrequests'	=> ['GET'],
            'videos'	    => ['GET'],
            'index'		    => ['GET'],
            'help-text'		=> ['GET'],
            'register'	    => ['POST'],
            'profile'	    => ['GET'],
            'update'	    => ['POST'],
            'video-watch'	=> ['POST'],
            'scan-code'	=> ['POST'],
            'cancel-request-by-supplier'	=> ['POST'],
            'cancel-request'	=> ['POST'],
            'sales-person-rating'	=> ['POST'],
        ];
    }
    public function actionIndex()
    {
        $json['message'] = 'Welcome to '.config_name;
        return $json;
    }
    /*
        Register new user as sales person
        @Data method POST
        @Auth bearer token required
        @Required params full_name, gender, city,dob,username,password,email,phone_number
        @Response array
    */
    public function actionRegister(){
        $model				  = new Apiuser();
        $model1				  = new SalesPerson();
        $model->scenario	  = 'sales_reg';
        $post_data['Apiuser'] = Yii::$app->request->post();
        $post_data1['SalesPerson'] = Yii::$app->request->post();
        
        if ($model->load($post_data) && $model->validate()) {
        
            if ($model1->load($post_data1) && $model1->validate()) {
                $pass_hash 			= $model->password;
                $model->setPassword($pass_hash);

                $model->generateAuthKey();
				$model->generateEmailVerificationToken();
                $model->email_verify = 0;
                $model->role		 = 5;
                //$model->status		 = 0;
                $model->availability = 'offline';
                $model->is_user		= 'yes';
                $model->is_supplier	= 'no';
                $model->is_sales_person	= 'yes';
                $model->ip_address		= Yii::$app->getRequest()->getUserIP();
                $model->last_login		= time();
                $model->last_login_ip	= Yii::$app->getRequest()->getUserIP();
                
                if(isset($post_data['Apiuser']['device_token']))
                    $model->device_token = $post_data['Apiuser']['device_token'];
                $model->name = $model1->full_name;
                
                if($model->save()){
					$model->sendVerifyEmail($model);
					$model->copyTestItems($model->id);
                    $model1->user_id= $model->id;
                    
                    if($model1->save()){
                        $user_to_role	= new UserToRoles();
                        $user_to_role->user_id	= $model->id;
                        $user_to_role->role_id	= $model->role;
                        $user_to_role->save();

                        $json['success']['name']	= $model1->full_name;
                        $json['success']['role']	= strtolower($model->userRole->name);
                        $json['success']['msg']	= 'An email has been sent. Please verify your account.';
                       
                    }
                }
            }else{
                $all_errors				= [];
                $errors					= $model1->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }
        }else{
            $all_errors				= [];
            $errors					= $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    /*
        Get Sales person profile
        @Data method POST
        @Auth bearer token required
        @Response array
    */
    public function actionProfile(){
        $user_info 			= Yii::$app->user->identity;
        $sales_data         = SalesPerson::find()->where(['user_id'=>$user_info->id])->one();
        if(!empty($sales_data))
            $json['success']	= $user_info->salesDetail;
        else{
            $json['error']['msg']	= 'No sales profile found';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    /*
        Update sales person profile
        @Data method POST
        @Auth bearer token required
        @Required params full_name, gender, city,dob,username,password,email,phone_number
        @Response array
    */
    public function actionUpdate(){
        $user_info 	    = Yii::$app->user->identity;
        $model         = SalesPerson::find()->where(['user_id'=>$user_info->id])->one();
        if(!$model){
            $json['error']['msg']	= 'No sales profile found';
            $this->statusCode = 401;
        }else{
            $post_data['SalesPerson'] 	= Yii::$app->request->post();
            $file_data				= [];
            if(isset($_FILES['image_url']) && !empty($_FILES['image_url'])){
                foreach($_FILES['image_url'] as $key=>$pic_data){
                    $file_data[$key]	= ['image_url'=>$pic_data];
                }
            }
            if(!empty($file_data))
                $_FILES['SalesPerson'] = $file_data;
            if ($model->load($post_data) && $model->validate()) {
                $image_url 		= UploadedFile::getInstance($model, 'image_url');
                if(!empty($image_url)){
                    $library   			 = new Library();
                    $model->image_url  = $library->saveFile($image_url,'users');
                }

                if($model->save()){
                    $json['success']	= $user_info->salesDetail;
                    $json['success']['msg']		= 'Your profile has been updated successfully.';
                }
            }else{
                $all_errors				= [];
                $errors					= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    /*
        Get all demo videos
        @Data method GET
        @Auth bearer token required
        @Response array
    */
    public function actionVideos(){
        $user_info 		= Yii::$app->user->identity;
        $user_id        = $user_info->id;
        $results = Videos::find()->select(['video_id','title','link','description'])->where(['status'=>'active'])->asArray()->all();
        if(!empty($results)){
            foreach($results  as $result){

                $is_watched = VideosWatchlist::find()->where(['video_id'=>$result['video_id'],"user_id"=>$user_id])->count();
                $output[] = ['video_id'=>$result['video_id'],'title'=>$result['title'],'link'=>$result['link'],'description'=>$result['description'],'is_watched'=>$is_watched];
            }
            $json['data']   = $output;

        }else{
            $json['error']['msg']	= 'No video found.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    /*
        Update video if user watched
        @Data method POST
        @Auth bearer token required
        @Required parameter $video_id
        @Response array
    */
    public function actionVideoWatch(){
        $user_info 		= Yii::$app->user->identity;
        $user_id        = $user_info->id;
        $post_data['VideosWatchlist']      = Yii::$app->request->post();
        if(!isset($post_data['VideosWatchlist']['video_id'])){
            $json['error']['msg']	='Please select video';
            $this->statusCode = 401;
            $json['code']		= $this->statusCode;
            return $json;
        }
        $is_watched     = VideosWatchlist::find()->where(['video_id'=>$post_data['VideosWatchlist']['video_id'],"user_id"=>$user_id])->count();
        if($is_watched==0){
            $model = new VideosWatchlist();
            $model->user_id = $user_id;
            if ($model->load($post_data) && $model->validate()) {
                if($model->save())
                    $json['success']['msg']		= 'Your data has been saved.';
            }else{
                $all_errors				= [];
                $errors					= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }
        }else
            $json['success']['msg']		= 'Your data has been saved.';
        $json['code']		= $this->statusCode;
        return $json;
    }
    /*
        View pending and accepted requests
        @Data method GET
        @Auth bearer token required
        @Response array
    */
    public function actionMyrequests(){
        $user_info 		= Yii::$app->user->identity;

        $user_id        = $user_info->id;
        $udata = User::findOne($user_id);
        $menu_cats      = MainCategories::find()->where(['status'=>1])->all();
        $data           = [];

        foreach($menu_cats as $menu_cat){
            $model          = new Notifications();
            $query          = $model->find()->where(['AND',['status'=>1],['user_id'=>$user_id],['role'=>5],['request_type'=>$menu_cat->id],['in','request_status',['pending','accepted']]])->orderBy(new Expression("request_status='accepted' desc,notification_id desc"));
            ${$menu_cat->code}	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => 1000,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'notification_id' => SORT_DESC,
                    ]
                ],
            ]);
            $count = $model->find()->where(['AND',['status'=>1],['user_id'=>$user_id],['role'=>5],['request_type'=>$menu_cat->id],['in','request_status',['pending']]])->count();

            ${$menu_cat->code}->pagination->pageParam = $menu_cat->code.'_page';
            $p_count			= ${$menu_cat->code}->getTotalCount();
            $total_p_pages		= ceil($p_count/$this->page_size);
            $results            = ${$menu_cat->code}->getModels();
          //  $data[$menu_cat->code]['total_pages'] = $total_p_pages;
            //$data[$menu_cat->code]['total_record'] = $p_count;
            $data[$menu_cat->code]['count'] = $count;
            if(!empty($results)){
                foreach($results as $k=>$result){
                    $help_req = HelpRequests::findOne($result->request_id);

                    if(!empty($help_req)){
                        $user_info = [];
                        $user_info['name'] = $help_req->name;
                        $user_info['location'] = $help_req->location;
                        $user_info['phonenumber'] = $help_req->phonenumber;
                        $user_info['lat'] = $help_req->lat;
                        $user_info['lng'] = $help_req->lng;
                        $user_info['rating_complete'] = $help_req->rating_complete;
                        $user_info['device_token'] = $help_req->device_token;
                        $user_info['time']= Yii::$app->formatter->format($help_req->created_at, 'relativeTime');

                        $data[$menu_cat->code]['results'][$k] =  $result->salesnotifications;
                        $data[$menu_cat->code]['results'][$k]['salesperson'] =  [];
                        if(!empty($help_req->accepted_by)){
                            $sales_info = User::findOne($help_req->accepted_by);
                            $data[$menu_cat->code]['results'][$k]['salesperson'] =  $sales_info->salesDetail;
                        }
                       // $data[$menu_cat->code]['results'][$k]['supplier_data'] =  $user_info;
                    }
                }
            }else
                $data[$menu_cat->code]['results'] = [];
        }
        $json['sales_person'] = $udata->salesDetail;
        $json['data'] = $data;
        $json['is_watched'] = $udata->videosinfo;
        $json['code'] = $this->statusCode;
        return $json;
    }

    /*
        Cancel request
        @Data method POST
        @Auth bearer token required
        @Required parameter $request_id
        @Response array
    */
    public function actionCancelRequest(){
        $can_model      = new CancelledHelpRequests();
        $can_model->scenario = 'by_supplier';
        $post_data      = Yii::$app->request->post();
        $user_info 		= Yii::$app->user->identity;
        $user_id        = $user_info->id;
        $request_id     = isset($post_data['request_id']) ? $post_data['request_id'] : 0;
        $help_req       = HelpRequests::find()->where(['request_id'=>$request_id,'accepted_by'=>$user_id,'request_status'=>13])->one();
        if(!empty($help_req)){
            $post_data['CancelledHelpRequests'] = Yii::$app->request->post();
            $can_model->request_id = $request_id;
            $can_model->by_owner = 'no';
            $can_model->by_sales_person = 'yes';
            $can_model->rejected_by = $user_id;
            if ($can_model->load($post_data) && $can_model->validate()) {
                if($can_model->save()){
                    $help_req->request_status = 2;
                    if($help_req->save()){
                        $model = new Notifications();
                        $res = $model->find()->where(['request_id'=>$help_req->request_id,'request_status'=>'accepted','user_id'=>$user_id])->one();
                        $res->request_status = 'canceled';
                        if($res->save()){
                            $json['success']['msg']		= 'Your request has been cancelled.';
                        }
                    }
                }
            }else{
                $all_errors				= [];
                $errors					= $can_model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }

        }else{
            $json['error']['msg']	= 'You\'re not allowed to perform this action.';
            $this->statusCode = 401;
        }
        $json['code']	= $this->statusCode;
        return $json;
    }
    /*
        Accept pending request
        @Data method POST
        @Auth bearer token required
        @Required parameter $notification_id
        @Response array
    */
    public function actionAcceptRequest($notification_id){
        $user_info 		= Yii::$app->user->identity;
        $user_id        = $user_info->id;
        $model          = new Notifications();
        $query          = $model->find()->where(['status'=>1,'user_id'=>$user_id,'role'=>5,'request_status'=>'accepted']);
        $count          = $query->count();
        if($count>3){
            $json['error']['msg']	= 'You can not accept more than 3 requests.';
            $this->statusCode = 401;
        }else{
            $result      = $model->find()->where(['notification_id'=>$notification_id,'status'=>1,'user_id'=>$user_id,'role'=>5,'request_status'=>'pending'])->one();
            if(!empty($result)){
                $result->request_status = 'accepted';
                if($result->save()){
                    $help_req = HelpRequests::findOne($result->request_id);
                    $help_req->request_status = 13;
                    $help_req->accepted_by = $user_id;
                    if($help_req->save()){
                        $user_detail = $user_info->userInfo;
                        $library = new Library();
                        $order_info['user_id']        = 1;
                        $order_info['role']           = 2;
                        $order_info['from_user']      = $user_id;
                        $order_info['type']           = 'help_request';
                        $order_info['request_id']     = $help_req->request_id;
                       // $gender = $user_detail['gender'] =='male' ? 'He' : 'She';
                        $order_info['config_name']    = $user_detail['name'].' accepted your request.Our sales representative will contact you shortly. ';
                        $order_info['config_value']['request_id']= $help_req->request_id;
                        $order_info['config_value']['device_token']= $help_req->device_token;
                        $order_info['config_value']['message']= $user_detail['name'].' accepted your request.Our sales representative will contact you shortly. ';

                        $help_req->sendEmail($order_info['config_name']);
                        $library->saveNotification($order_info,true);
                    }

                    $results  = $model->find()->where(['and',['request_id'=>$result->request_id,'status'=>1,'request_id'=>$help_req->request_id,'role'=>5,'request_status'=>'pending'],['!=','notification_id',$notification_id]])->all();
                    if(!empty($results)){
                        foreach($results as $result1){
                            $result1->request_status = 'canceled';
                            $result1->save();
                        }
                    }
                    // Start notifications count
                    $model = new Notifications();
                    $count = $model->find()->where(['AND',['status'=>1],['user_id'=>$user_id],['role'=>5],['in','request_status',['pending']]])->count();
                    $json['data']['count'] = $count;
                    // End notifications count
                    $json['success']['msg']		= 'Your request has been accepted.';
                }
            }else{
                $json['error']['msg']	= 'You\'re not allowed to perform this action.';
                $this->statusCode = 401;
            }
        }

        $json['code']	= $this->statusCode;
        return $json;
    }
    /*
        Reject request
        @Data method POST
        @Auth bearer token required
        @Required parameter $notification_id
        @Response array
    */
    public function actionRejectRequest($notification_id){
        $user_info 		= Yii::$app->user->identity;
        $user_id        = $user_info->id;
        $model          = new Notifications();
        $result          = $model->find()->where(['notification_id'=>$notification_id,'status'=>1,'user_id'=>$user_id,'role'=>5,'request_status'=>'pending'])->one();
        if(!empty($result)){
            $result->request_status = 'rejected';
            $result->save();
            $json['success']['msg']		= 'Your request has been rejected.';
            // Start notifications count
            $model = new Notifications();
            $count = $model->find()->where(['AND',['status'=>1],['user_id'=>$user_id],['role'=>5],['in','request_status',['pending']]])->count();
            $json['data']['count'] = $count;
            // End notifications count
        }else{
            $json['error']['msg']	= 'You\'re not allowed to access this action.';
            $this->statusCode = 401;
        }
        $json['code']	= $this->statusCode;
        return $json;
    }
    /*
       Complete request
       @Data method POST
       @Auth bearer token required
       @Required parameter $request_id
       @Response array
   */
    public function actionCompleteRequest($request_id){
        $user_info = Yii::$app->user->identity;
        $user_detail = $user_info->salesDetail;
        $email = $user_info->email;
        $user_id = $user_info->id;
        $help_req = HelpRequests::find()->where(['request_id'=>$request_id,'email'=>$email,'request_status'=>13])->one();
        if(!empty($help_req)){
            $help_req->request_status = 5;
            $help_req->save();
            $model   = new Notifications();
            $result  = $model->find()->where(['request_id'=>$help_req->request_id,'status'=>1,'role'=>5,'request_status'=>'accepted'])->one();
            if(!empty($result)){
                $result->request_status = 'completed';
                $result->save();
                $json['success']['msg']		= $user_detail['name'].' has been completed your request.';

                // delete old notifications
                Notifications::deleteAll(['type'=>'help_request','user_id'=>$help_req->accepted_by,'request_id'=>$help_req->request_id]);
                $library = new Library();
                $order_info['user_id']        = $help_req->accepted_by;
                $order_info['role']           = 5;
                $order_info['from_user']      = $user_id;
                $order_info['type']           = 'help_request';
                $order_info['request_status'] = 'completed';
                $order_info['request_type'] = $result->request_type;
                $order_info['request_id']     = $help_req->request_id;

                $order_info['config_name']    = $user_detail['name'].' has been completed your request.';
                $order_info['config_value']['request_id']= $help_req->request_id;
                $order_info['config_value']['device_token']= $help_req->device_token;
                $order_info['config_value']['message']= $user_detail['name'].' has been completed your request.';

                $library->saveNotification($order_info);
            }

        }else{
            $json['error']['msg']	= 'You\'re not allowed to perform this action.';
            $this->statusCode = 401;
        }
        $json['code']	= $this->statusCode;
        return $json;
    }

    /*
      Reject request
      @Data method POST
      @Auth bearer token required
      @Required parameter $request_id
      @Response array
  */
    public function actionDeclineRequest($request_id){
        $user_info = Yii::$app->user->identity;
        $user_detail = $user_info->salesDetail;
        $email = $user_info->email;
        $user_id = $user_info->id;
        $help_req = HelpRequests::find()->where(['request_id'=>$request_id,'email'=>$email,'request_status'=>13])->one();
        if(!empty($help_req)){
            $help_req->request_status = 7;
            $help_req->save();
            $model   = new Notifications();
            $result  = $model->find()->where(['request_id'=>$help_req->request_id,'status'=>1,'role'=>5,'request_status'=>'accepted'])->one();
            if(!empty($result)){
                $result->request_status = 'rejected';
                $result->save();
                $json['success']['msg']		= $user_detail['name'].' has been rejected your request.';

                $can_model =  new CancelledHelpRequests();
                $can_model->request_id = $help_req->request_id;
                $can_model->by_owner = 'yes';
                $can_model->by_sales_person = 'no';
                $can_model->message = 'Request has been rejected by owner.';
                $can_model->save();
                // delete old notifications
                Notifications::deleteAll(['type'=>'help_request','user_id'=>$help_req->accepted_by,'request_id'=>$help_req->request_id]);
                $library = new Library();
                $order_info['user_id']        = $help_req->accepted_by;
                $order_info['role']           = 5;
                $order_info['from_user']      = $user_id;
                $order_info['type']           = 'help_request';
                $order_info['request_id']     = $help_req->request_id;

                $order_info['config_name']    = $user_detail['name'].' has been rejected your request.';
                $order_info['config_value']['request_id']= $help_req->request_id;
                $order_info['config_value']['device_token']= $help_req->device_token;
                $order_info['config_value']['message'] = $user_detail['name'].' has been rejected your request.';

                $library->saveNotification($order_info);
            }

        }else{
            $json['error']['msg']	= 'You\'re not allowed to perform this action.';
            $this->statusCode = 401;
        }
        $json['code']	= $this->statusCode;
        return $json;
    }
    /*
       Total jobs done
       @Data method POST
       @Auth bearer token required
       @Response array
   */
    public function actionJobsdone(){
        $user_info 		= Yii::$app->user->identity;
        $user_id        = $user_info->id;
        $menu_cats      = MainCategories::find()->where(['status'=>1])->all();
        $data           = [];
        $final_total = HelpRequests::find()->where(['request_status'=>5,'accepted_by'=>$user_id])->count();
        foreach($menu_cats as $menu_cat){
            $data[$menu_cat->code] =HelpRequests::find()->where(['request_type'=>$menu_cat->id,'request_status'=>5,'accepted_by'=>$user_id])->count();
        }
        $json['data']   = $data;
        $json['data']['total']   = $final_total;
        $json['code']	= $this->statusCode;


        $menu_cats      = MainCategories::find()->where(['status'=>1])->all();
        $data           = [];
        foreach($menu_cats as $menu_cat){
            $model          = new Notifications();
            $query          = $model->find()->where(['AND',['status'=>1],['user_id'=>$user_id],['role'=>5],['request_type'=>$menu_cat->id],['request_status'=>'completed']])->orderBy(new Expression("request_status='accepted' desc,notification_id desc"));
            ${$menu_cat->code}	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'notification_id' => SORT_DESC,
                    ]
                ],
            ]);
            ${$menu_cat->code}->pagination->pageParam = $menu_cat->code.'_page';
            $p_count			= ${$menu_cat->code}->getTotalCount();
            $total_p_pages		= ceil($p_count/$this->page_size);
            $results            = ${$menu_cat->code}->getModels();
            $data[$menu_cat->code]['total_pages'] = $total_p_pages;
            $data[$menu_cat->code]['total_record'] = $p_count;
            if(!empty($results)){
                foreach($results as $k=>$result){
                    $help_req = HelpRequests::findOne($result->request_id);
                    if(!empty($help_req)){
                        $user_info = [];
                        $user_info['name'] = $help_req->name;
                        $user_info['location'] = $help_req->location;
                        $user_info['phonenumber'] = $help_req->phonenumber;
                        $user_info['lat'] = $help_req->lat;
                        $user_info['lng'] = $help_req->lng;
                        $user_info['device_token'] = $help_req->device_token;

                        $data[$menu_cat->code]['results'][$k] =  $result->salesnotifications;
                        $data[$menu_cat->code]['results'][$k]['supplier_data'] =  $user_info;
                    }
                }

            }else
                $data[$menu_cat->code]['results'] = [];
        }
        $json['completed_data']   = $data;
        return $json;
    }
    /*
       Get supplier information
       @Data method POST
       @Auth bearer token required
       @Required parameter $request_id
       @Response array
   */
    public function actionViewdetail(){
        $post_data = Yii::$app->request->post();
        $user_info  = Yii::$app->user->identity;
        $user_id    = $user_info->id;
        $model      = new HelpRequests();
        $request_id = isset($post_data['request_id']) ? $post_data['request_id'] : 0;
        $result     = $model->find()->where(['request_id'=>$request_id,'accepted_by'=>$user_id,'request_status'=>5])->one();
        if(!empty($result)){
            $user_info = User::findOne($result->from_user);
            $json['supplier_data']		= $user_info->userInfo;
        }else{
            $json['error']['msg']	= 'You\'re not allowed to perform this action.';
            $this->statusCode = 401;
        }
        $json['code']	= $this->statusCode;
        return $json;
    }
    /*
       Sales person rating
       @Data method POST
       @Required parameter $request_id,$rating
       @Response array
   */
    public function actionSalesPersonRating(){
        $post_data['SalesPersonRating'] = Yii::$app->request->post();
        $model      = new HelpRequests();
        $request_id = isset($post_data['SalesPersonRating']['request_id']) ? $post_data['SalesPersonRating']['request_id'] : 0;
        $result     = $model->find()->where(['request_id'=>$request_id,'request_status'=>5,'rating_complete'=>'no'])->one();
        if(!empty($result)){
            $model = new SalesPersonRating();
            $model->user_id = $result->accepted_by;
            if ($model->load($post_data) && $model->validate()) {
                $result->rating_complete = 'yes';
                $result->save();
                $model->save();
                $json['success']['msg']		= 'Thanks for the rating.';

            }else{
                $all_errors				= [];
                $errors					= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }
        }else{
            $json['error']['msg']	= 'You\'re not allowed to perform this action.';
            $this->statusCode = 401;
        }
        $json['code']	= $this->statusCode;
        return $json;
    }
    /*
           Get Ask for help text
           @Data method GET
           @Response array
       */
    public function actionHelpText($device_token=null){
        $json['data']		= config_ask_for_help_text;
        $json['code']	= $this->statusCode;
        $user_model		= new User();
        $model          = new HelpRequests();
        $headers		= getallheaders();
        $token			= '';
        if(isset($headers['Authorization']))
            $token	= str_replace("Bearer ","",$headers['Authorization']);
        $user_info		= $user_model->findIdentityByAccessToken($token);
        if(!empty($user_info)){
            $help_req     = $model->find()->where(['email'=>$user_info->email])->andWhere(['or',['request_status'=>1],['request_status'=>11],['request_status'=>13]])->orderBy("request_id desc")->one();
            if(!empty($help_req)){
                $user_info = [];
                $user_info['request_id'] = $help_req->request_id;
                $user_info['name'] = $help_req->name;
                $user_info['location'] = $help_req->location;
                $user_info['phonenumber'] = $help_req->phonenumber;
                $user_info['lat'] = $help_req->lat;
                $user_info['lng'] = $help_req->lng;
                $user_info['rating_complete'] = $help_req->rating_complete;
                $user_info['device_token'] = $help_req->device_token;
                $user_info['request_status'] = OrderStatus::findOne($help_req->request_status)->name;
                $user_info['time']= Yii::$app->formatter->format($help_req->created_at, 'relativeTime');

                $json['help_request'] = $user_info;
                $json['help_request']['salesperson'] =  [];
                if(!empty($help_req->accepted_by)){
                    $sales_info = User::findOne($help_req->accepted_by);
                    $json['help_request']['salesperson'] =  $sales_info->salesDetail;
                }else
                    $json['help_request']['salesperson'] =  (object)[];
            }else
                $json['help_request'] = (object)[];

        }


        $json['code']	= $this->statusCode;
        return $json;
    }
    public function actionCancelRequestBySupplier(){
        $can_model      = new CancelledHelpRequests();
        $model      = new HelpRequests();
        $post_data = Yii::$app->request->post();
        $request_id = isset($post_data['request_id']) ? $post_data['request_id'] : 0;
        $device_token = isset($post_data['device_token']) ? $post_data['device_token'] : null;


        //$help_req     = $model->find()->where(['device_token'=>$device_token,'request_id'=>$request_id])->andWhere(['!=','request_status',2])->orderBy("request_id desc")->one();
        $help_req     = $model->find()->where(['request_id'=>$request_id])->andWhere(['!=','request_status',2])->orderBy("request_id desc")->one();
        if(!empty($help_req)){
            $post_data['CancelledHelpRequests'] = Yii::$app->request->post();
            $can_model->request_id = $request_id;
            $can_model->by_owner = 'yes';
            $can_model->by_sales_person = 'no';
            $can_model->rejected_by = null;
            if ($can_model->load($post_data) && $can_model->validate()) {
                if($can_model->save()){
                    $help_req->request_status = 2;
                    if($help_req->save()){
                        if(!empty($help_req->accepted_by)){
                            $library = new Library();
                            $order_info['user_id']        = $help_req->accepted_by;
                            $order_info['role']           = 5;
                            $order_info['from_user']      = 1;
                            $order_info['type']           = 'help_request';
                            $order_info['request_id']     = $help_req->request_id;

                            $order_info['config_name']    = $help_req->name.' cancelled the request.';
                            $order_info['config_value']['request_id']= $help_req->request_id;
                            $order_info['config_value']['device_token']= $help_req->device_token;
                            $order_info['config_value']['message']= $help_req->name.' cancelled the request.';

                            $library->saveNotification($order_info);
                        }
                        $model = new Notifications();
                        $res = $model->find()->where(['request_id'=>$help_req->request_id])->all();
                        foreach($res as $data){
                            $data->request_status = 'canceled';
                            $data->save();
                        }
                        $json['success']['msg']		= 'Your request has been cancelled.';
                    }
                }
            }else{
                $all_errors				= [];
                $errors					= $can_model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }

        }else{
            $json['error']['msg']	= 'You\'re not allowed to perform this action.';
            $this->statusCode = 401;
        }
        $json['code']	= $this->statusCode;
        return $json;
    }

    public function actionScanCode(){
        $post_data = Yii::$app->request->post();
        $user_info  = Yii::$app->user->identity;
        $user_id    = $user_info->id;
        $device_token = $post_data['device_token'] ?? "";
        $email  = $post_data['email'] ?? "";
        $help_req = HelpRequests::find()->where(['email'=>$email,'accepted_by'=>$user_id])->andWhere(['or',['request_status'=>1],['request_status'=>11],['request_status'=>13]])->one();
        if(!empty($help_req)){
            $user_detail = $user_info->salesDetail;
            $user_info = User::find()->where(['email'=>$email])->one();
            if(!empty($user_info)){

                $library = new Library();
                $order_info['user_id']        = $user_info->id;
                $order_info['role']           = 2;
                $order_info['from_user']      = $user_id;
                $order_info['type']           = 'help_request';
                $order_info['request_id']     = $help_req->request_id;
                // $gender = $user_detail['gender'] =='male' ? 'He' : 'She';
                $order_info['config_name']    = $user_detail['name'].' scanned your QR code.Please complete the request.';
                $order_info['config_value']['request_id']= $help_req->request_id;
                $order_info['config_value']['device_token']= $help_req->device_token;
                $order_info['config_value']['message']= $user_detail['name'].' scanned your QR code.Please complete the request.';
                $order_info['config_value']['salesperson_data']= $user_detail;
                $library->saveNotification($order_info,true);

                $json['success']['msg']		= 'QR code has been scanned successfully.';
            }else{
                $json['error']['msg']	= 'Invalid user email address.';
                $this->statusCode = 401;
            }

        }else{
            $json['error']['msg']	= 'Invalid request.';
            $this->statusCode = 401;
        }
        $json['code']	= $this->statusCode;
        return $json;
    }
}