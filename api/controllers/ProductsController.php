<?php
namespace api\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use	yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Menu;
use common\models\Library;
use common\models\Category;
use common\models\MenuRating;
use common\models\MenuCategories;
use common\models\OptionDescription;
use common\models\CategoryToOptions;
use common\models\MenuToImages;
use common\models\OptionValue;
use common\models\Brand;
use common\models\CategoryDescription;
// use common\models\Category;


class ProductsController extends ActiveController
{
    public $statusCode = 200;
    public $page_size = 30;
    public $language_id = 1;
    public $modelClass = 'common\models';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']	= [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['error','index','product','get-menu-rating'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        return $behaviors;
    }
    /**
     * {@inheritdoc}
     */
    public function actions(){
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }
    /**
     * {@inheritdoc}
     */
    protected function verbs(){
        return [
            'index' => ['GET'],
            'images' => ['GET'],
            'add-product' => ['POST'],
            'delete-image' => ['POST'],
            'get-categories' => ['POST'],
            'get-variations' => ['GET'],
            'my-products' => ['GET'],
            'product' => ['GET'],
            'upload-image' => ['POST'],
            'update-image' => ['POST'],
            'edit-product' => ['POST'],
            'delete-product' => ['GET'],
            'get-menu-rating' => ['GET'],
        ];
    }
    public function actionIndex(){
        $json['message'] = 'Welcome to '.config_name;
        return $json;
    }
    public function actionGetVariations($category_id){
        $user_info = Yii::$app->user->identity;
        $has_permission = $this->checkPermission($user_info);
        if(!$has_permission){
            $this->statusCode	 = 401;
            $json['error']['msg']	= "You're not authorized to perform this action.";
        }else{
            $options = [];
            $categories = explode(',',$category_id);
            foreach($categories as $category){
                $cat_to_options = CategoryToOptions::find()->where(['category_id'=>$category])->asArray()->all();
                if(!empty($cat_to_options)){
                    foreach($cat_to_options as $k=>$cat_to_option){
                        $option_to_values = OptionValue::find()->where(['option_id' =>$cat_to_option['option_id']])->asArray()->all();
                        $option_value = [];
                        if(!empty($option_to_values)){
                            foreach($option_to_values as $option_to_value){
                                $option_value[] = ['option_value_id' => $option_to_value['option_value_id'],'name' => $option_to_value['name']];
                            }
                        }
                        $option_name = OptionDescription::find()->where(['option_id'=>$cat_to_option['option_id'],'language_id'=>$this->language_id])->asArray()->one();
                        $options[$category][$k] = ['option_id'=>$cat_to_option['option_id'],'option_name'=>$option_name['name'],'is_required'=>$cat_to_option['is_required'],'option_value'=>$option_value];
                    }
                }
            }
            if(!empty($options)){
                $json['data'] = $options;
            }else{
                $this->statusCode	 = 401;
                $json['error']['msg']	= "No variation found.";
            }

            $json['code']	= $this->statusCode;
            return $json;

        }
    }
    /*
        View pending and accepted requests
        @Data method POST
        @Auth bearer token required
        @Response array
    */
    public function actionGetCategories(){
        $user_info = Yii::$app->user->identity;
        $has_permission = $this->checkPermission($user_info);
        $post_data = Yii::$app->request->post();
        $language_id = $_REQUEST['language_id'] ?? $this->language_id;
        if(!$has_permission){
            $this->statusCode	 = 401;
            $json['error']['msg']	= "You're not authorized to perform this action.";
        }else{
            $parent_id = $post_data['parent_id'] ?? null;
            $model = Category::find()->select('*')->where(['active'=>1]);
            if($parent_id==null)
                $model->andWhere(['and',['is','parent_id',null],['cat_id'=>$user_info->supplier_type]]);
            else
                $model->andWhere(['and',['parent_id'=>$parent_id],['cat_id'=>$user_info->supplier_type]]);
            $results = $model->asArray()->all();
            if(!empty($results)){
                foreach($results as $category){
                    $count_parents = Category::find()->where(['and',['parent_id'=>$category['category_id']],['active'=>1]])->count();
                    $has_parents = $count_parents>0 ? 'yes' : 'no';

                    if($parent_id==null)
                        $data[] = ['main_category'=>$category['category_id'],'category_name'=>$category['name'],'has_parents'=>$has_parents];
                    else{
                        $menuItems = $this->menuItems($category);
                        $data[] = ['sub_category'=>$category['category_id'],'category_name'=>$category['name'],'has_parents'=>$has_parents,'sub_categories'=>$menuItems];
                    }

                }
            $json['data'] = $data;
            }else{
                $this->statusCode	 = 401;
                $json['error']['msg']	= "No category found.";
            }

        }
        $json['code']	= $this->statusCode;
        return $json;
    }
    protected function menuItems($category){

        $items = [];
        $categoryModel = Category::findOne(['name'=>$category['name'],'active'=>1]);
        $results =Category::find()->where(['parent_id' => $category['category_id']])->all();
        if(empty($results))
            return $items;
        $i = 0;
        foreach($results as $result){
            $childItems	= $this->menuItems($result);
            $childItems	= [];
            $count_parents = Category::find()->where(['and',['parent_id'=>$category['category_id']],['active'=>1]])->count();
            $has_parents = $count_parents>0 ? 'yes' : 'no';
            $items[$i] 	=  ['sub_category'=>$result['category_id'],'category_name'=>$result['name'],'has_parents'=>$has_parents];
            foreach( $childItems as $key => $value)
                $items[$i]['items'][]	= $value;
            $i++;
        }
        return $items;
        $model = Category::find()->select('*')->joinWith('categoryName as cn')->where(['status'=>'active','cn.language_id'=>$language_id]);
        $model->andWhere(['and',['parent_id'=>$parent_id],['cat_id'=>$user_info->supplier_type]]);
        $results = $model->orderBy('cn.category_name asc')->asArray()->all();
        $items = [];
        if(empty($results))
            return $items;
        $i = 0;
        foreach($results as $result){

            $childItems		= $this->menuItems($result['category_id'],$language_id,$user_info);
            $count_parents = Product::find()->where(['parent_id'=>$result['category_id'],'status'=>'active'])->count();
            $has_parents = $count_parents>0 ? 'yes' : 'no';
            $items[$i] 	=  ['sub_category'=>$result['category_id'],'category_name'=>$result['category_name'],'has_parents'=>$has_parents];
            foreach( $childItems as $key => $value)
                $items[$i]['items'][]	= $value;
            $i++;
        }
        return $items;
    }
    public function actionEditProduct(){
        $user_info = Yii::$app->user->identity;
        $user_subscription = $user_info->subscriptionData;
        if($user_subscription['status']=='inactive'){
            $json['error']['msg']	= 'Please subscribe the plan.' ;
            $this->statusCode = 401;
            $json['code']		= $this->statusCode;
            return $json;
        }

        $supplier_id = $user_info->id;
        $post_data['Menu'] = Yii::$app->request->post();
        if(isset($post_data['Menu']['product_id']) && !empty($post_data['Menu']['product_id'])){
            $model	= Menu::find()->where(['supplier_id'=>$supplier_id,'id'=>$post_data['Menu']['product_id']])->one();
            if(!$model){
                $this->statusCode = 401;
                $json['error']['msg']	= 'Wrong selection';
            }else{
                $model->scenario = 'other_items';

                if(isset($post_data['Menu']['product_name'])){

                    $file_data = [];
                    if(isset($_FILES['product_image']) && !empty($_FILES['product_image'])){
                        foreach($_FILES['product_image'] as $key=>$pic_data){
                            $file_data[$key]	= ['dish_image'=>$pic_data];
                        }
                    }

                    if(!empty($file_data))
                        $_FILES['Menu'] = $file_data;
                    if ($model->load($post_data) && $model->validate()) {
                        $model->dish_name = $model->product_name;
                        $model->dish_description = $model->product_description;
                        $model->dish_price = $model->product_price;
                        $model->dist_qty = $model->product_qty;
                        $model->status = $model->status;
                        $dish_image	= UploadedFile::getInstance($model, 'dish_image');
                        if(!empty($dish_image)){
                            $library   			 	= new Library();
                            $model->dish_image  	= $library->saveFile($dish_image,'menu');
                            $model->image_approval 	= 0;
                        }

                        if($model->save()){
                            $json['success']['msg']			= "Your Product has been updated successfully.";
                        }
                    }else{
                        $all_errors	= [];
                        $errors = $model->getErrors();
                        foreach($errors as $key=>$error){
                            $all_errors[]	= $error[0];
                        }
                        $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                        $this->statusCode = 401;
                    }
                }else{
                    $this->statusCode = 401;
                    $json['error']['msg']	= 'Product name can\'t be blank';
                }
            }

        }else{
            $this->statusCode = 401;
            $json['error']['msg']	= 'Wrong selection';
        }

        $json['code']		= $this->statusCode;
        return $json;
    }
    public function actionAddProduct(){
        $user_info	= Yii::$app->user->identity;
        $user_subscription = $user_info->subscriptionData;
        if($user_subscription['status']=='inactive'){
            $json['error']['msg']	= 'Please subscribe the plan.' ;
            $this->statusCode = 401;
            $json['code']		= $this->statusCode;
            return $json;
        }

        $model		= new Menu();
        $model->scenario = 'other_items';
        $post_data['Menu'] 		= Yii::$app->request->post();
        if(isset($_FILES['product_image']) && !empty($_FILES['product_image'])){
            foreach($_FILES['product_image'] as $key=>$pic_data){
                $file_data[$key]	= ['dish_image'=>$pic_data];
            }
        }
        if(!empty($file_data))
            $_FILES['Menu'] = $file_data;
        $model->supplier_id	= $user_info->id;
        $model->dist_qty	= !empty($model->product_qty) ? $model->product_qty : 0;
        if ($model->load($post_data) && $model->validate()) {
   //start new colom added
            $model->brand_id = $user_info->brand_id;      
   //end new colom added
    
            $model->dish_name = $model->product_name;

            $model->dish_description = $model->product_description;
            $model->dish_price = $model->product_price;
            $model->dist_qty = $model->product_qty;
            $model->status = 'inactive';

            $model->city_id = $user_info->city;
            $model->product_type = $user_info->supplier_type;
            $dish_image	= UploadedFile::getInstance($model, 'dish_image');
            $more_text = '';
            if(!empty($dish_image)){
                $library   			 	= new Library();
                $model->dish_image  	= $library->saveFile($dish_image,'menu');
                $model->image_approval 	= 0;
                $more_text	.= ' Please wait till image approval from Admin.';
            }
            if($model->save()){
                if(isset($post_data['Menu']['main_category']) && !empty($post_data['Menu']['main_category'])){
                    $categories = explode(',',$post_data['Menu']['main_category']);
                    foreach($categories as $category){
                        $cat_model = new MenuCategories();
                        $cat_model->menu_id	= $model->id;
                        $cat_model->category_id	= $category;
                        $cat_model->main_category	= 1;
                        $cat_model->save();
                    }
                }
                if(isset($post_data['Menu']['sub_category']) && !empty($post_data['Menu']['sub_category'])){
                    $categories = explode(',',$post_data['Menu']['sub_category']);
                    foreach($categories as $category){
                        $cat_model = new MenuCategories();
                        $cat_model->menu_id	= $model->id;
                        $cat_model->category_id	= $category;
                        $cat_model->main_category	= 0;
                        $cat_model->save();
                    }
                }
                $json['success']['msg']			= "Your Product has been added successfully.".$more_text;
            }
        }else{
            $all_errors = [];
            $errors = $model->getErrors();

            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;

    }
    protected function checkPermission($user_info){

        $supplier_type = $user_info->supplier_type;
        if($supplier_type>1)
            return true;
        return false;
    }

    public function actionUploadImage(){
        $library    = new Library();
        $user_info = Yii::$app->user->identity;
        $model = new MenuToImages();
        $menu_id = $_POST['product_id'] ?? 0;
        $file_data =[];
        $post_data['MenuToImages'] 	= Yii::$app->request->post();
        if(isset($_FILES['image']) && !empty($_FILES['image'])){
            foreach($_FILES['image'] as $key=>$pic_data){
                $file_data[$key] = ['image_name'=>$pic_data];
            }
        }
        if(!empty($file_data))
            $_FILES['MenuToImages'] = $file_data;
        $image_name	= UploadedFile::getInstance($model, 'image_name');
        if(!empty($image_name)){
            $library   			 	= new Library();
            $model->image_name  	= $library->saveFile($image_name,'menu');
        }
        $post_data['MenuToImages']['menu_id'] = $menu_id;
        $model->user_id = $user_info->id;
        $model->file_info  = $library->json_encodeArray($image_name);
        if ($model->load($post_data) && $model->validate()) {
            if($model->save()){
                $json['data'] = ['image_id' => $model->image_id,'product_id' => $model->menu_id,'image' => Yii::$app->params['website_path'].'/uploads/'.$model->image_name];
                $json['success']['msg']	= "Your image has been uploaded successfully.";
            }
        }else{
            $all_errors = [];
            $errors = $model->getErrors();

            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionImages($product_id){
        $user_info = Yii::$app->user->identity;
        $user_id = $user_info->id;

        $results = MenuToImages::find()->where(['menu_id' => $product_id,'user_id' => $user_id])->asArray()->all();
        if(!empty($results)){
            foreach($results as $result)
                $data[] = ['image_id' => $result['image_id'],'product_id' => $result['menu_id'],'image' => Yii::$app->params['website_path'].'/uploads/'.$result['image_name']];
            $json['data'] = $data;
        }else{
            $json['error']['msg']	= 'No image found.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionDeleteImage(){
        $user_info = Yii::$app->user->identity;
        $user_id = $user_info->id;
        $image_id = $_POST['image_id'] ?? 0;
        $result = MenuToImages::find()->where(['image_id' => $image_id,'user_id' => $user_id])->one();
        if(!empty($result)){
            $fullurl 		= Yii::getAlias('@frontend').'/web/uploads/';
            unlink($fullurl.$result->image_name);
            $result->delete();
            $json['success']['msg']	= "Your image has been deleted successfully.";
        }else{
            $json['error']['msg']	= 'You\'re not authorized to perform this action.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionUpdateImage(){
        $user_info = Yii::$app->user->identity;
        $user_id = $user_info->id;
        $image_id = $_POST['image_id'] ?? 0;
        $file_data = [];
        $model =  MenuToImages::find()->where(['image_id' => $image_id,'user_id' => $user_id])->one();
        if(!empty($model)){
            if(isset($_FILES['image']) && !empty($_FILES['image'])){
                foreach($_FILES['image'] as $key=>$pic_data){
                    $file_data[$key] = ['image_name'=>$pic_data];
                }
            }
            if(!empty($file_data))
                $_FILES['MenuToImages'] = $file_data;
            $image_name	= UploadedFile::getInstance($model, 'image_name');
            if(!empty($image_name)){
                $fullurl 		= Yii::getAlias('@frontend').'/web/uploads/';
                unlink($fullurl.$model->image_name);
                $library   			 	= new Library();
                $model->image_name  	= $library->saveFile($image_name,'menu');
                $model->file_info  = $library->json_encodeArray($image_name);
                $model->save();
                $json['success']['msg']	= "Your image has been updated successfully.";
            }else{
                $json['error']['msg']	= 'Please upload image.';
                $this->statusCode = 401;
            }
        }else{
            $json['error']['msg']	= 'You\'re not authorized to perform this action.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionMyProducts($screen_id){
        $user_info = Yii::$app->user->identity;
        $supplier_id = $user_info->id;
        $query = Menu::find()->where(['supplier_id'=>$supplier_id,'is_delete'=>0,'product_type' => $screen_id]);
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'dish_name' => SORT_ASC,
                ]
            ],
        ]);
        $results = $dataProvider->getModels();
        if(!empty($results)){
            foreach($results as $k=>$result)
                $json['success'][$k] = $result->productInfo;
        }else{
            $this->statusCode = 401;
            $json['error']['msg'] = 'No product found.';
        }
        $json['code'] = $this->statusCode;
        return $json;
    }

    public function actionProduct($product_id){
        $result = Menu::find()->where(['is_delete'=>0,'id' => $product_id])->one();
        

        if(!empty($result)){
            $json['success']['data'] = $result->productInfo;
            $result->updateCounters(['view' => 1]);
            $user_image			= '';
            $supplierInfo	=  $result->supplierInfo1;
            if(!empty($supplierInfo->profile_pic) && $supplierInfo->image_approval==1)
                $user_image	= Url::to($supplierInfo->getUserImage($supplierInfo->profile_pic,600,400),true);

            $json['success']['supplier']['supplier_id']		= $supplierInfo->id;
            $json['success']['supplier']['name']			= $supplierInfo->name;
            $json['success']['supplier']['location']		= $supplierInfo->location;
            $json['success']['supplier']['rating']			= 4.5;
            $json['success']['supplier']['profile_pic']		= $user_image;

         
          
         $categoryid = $json['success']['data']['product_id'];
          // echo "<pre>";          
          //  print_r($result['product_type']);die;   
        $categoryid = 0;  
         if(!empty($result['product_type'])){
            $categoryid = $result['product_type'];
         }
         $sub_category_info = [];  
         $sub_category = [];  

         $categories = Category::find()
         ->where(['cat_id' => $categoryid ])
         ->andWhere(['active'=>1])  
         ->limit(10)        
         ->all(); 
        

        if(!empty($categories)){ 
            foreach ($categories as $category ){

                

                 $categorydiscription = CategoryDescription::find()->where(['category_id'=>$category->category_id])->andWhere(['language_id'=>1])->one();
                 $image = '';
               if(!empty($category['image'])){

                  $path1 = Url::base();         
                $path = str_replace("/api","",$path1); 
                $image = "https://freshhomee.com".$path."/frontend/web/uploads/". $category['image'] ;  
                
                }   

                $sub_category_info = ['name'=> $categorydiscription['category_name'] ,'sub_category_id' => $category->category_id, 'image' => $image];
                $sub_category[] = $sub_category_info; 

            }
        }

        // $json['code']    = $this->statusCode;

            $json['success']['sub_category']     = $sub_category;
        


 
         

         $brand_info = []; 
         $branddetails = [];     
  
            $brandsql ="SELECT tbl_brand_description.brand_id,tbl_brand_description.brand_name,tbl_brand_category.category_id FROM tbl_brand_category 
            LEFT JOIN tbl_brand_description ON tbl_brand_description.brand_id=tbl_brand_category.brand_id 
            AND tbl_brand_description.language_id=1  
            WHERE tbl_brand_category.category_id IN (SELECT category_id FROM tbl_category WHERE cat_id=$categoryid) 
            GROUP BY tbl_brand_category.brand_id ";     
        $brandlist = Yii::$app->db->createCommand($brandsql)->queryAll(); 
         
          if(!empty($brandlist)){ 
            foreach ($brandlist as $value ){
                 $branddata = Brand::find()->where(['brand_id'=>$value['brand_id']])->one();
                 $image = '';

               if(!empty($branddata['image'])){

                  $path1 = Url::base();         
                $path = str_replace("/api","",$path1); 
                $image = "https://freshhomee.com".$path."/frontend/web/uploads/". $branddata['image'] ; 
                }   
                $brand_info = ['name'=> $value['brand_name'] ,'brand_id' => $value['brand_id'], 'image' => $image];
                $branddetails[] = $brand_info; 
   
            }
        } 

        // $json['code']    = $this->statusCode;
   
            $json['success']['brand_list']     = $branddetails;


        }else{
            $this->statusCode = 401;
            $json['error']['msg']	 = 'No product found.';
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionDeleteProduct($product_id){
        $user_info		= Yii::$app->user->identity;
        $supplier_id	= $user_info->id;
        $result         = Menu::find()->where(['supplier_id'=>$supplier_id,'id'=>$product_id])->one();
        if(!empty($result)){
            $result->is_delete = 1;
            if($result->save())
                $json['success']['message'] = 'Your product has been deleted successfully.';
        }else{
            $json['error']['msg']	= 'Something went wrong! Try again later.';
            $this->statusCode	= 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionGetMenuRating($product_id){
        $page_size		= 30;
        $result   = Menu::findOne($product_id);
        if(!empty($result)){
            $json['avg_reviews'] = $result->avgReviews;
            $query			= MenuRating::find()->where(['!=','reviews',''])->andWhere(['menu_id'=>$product_id]);
            $dataProvider	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => $page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_DESC,
                    ]
                ],
            ]);
            $results			= $dataProvider->getModels();
            $count				= $dataProvider->getTotalCount();
            $total_pages		= ceil($count/$page_size);
            $json['total_pages']    = $total_pages;
            $reviews = [];
            foreach($results as $k=>$result)
                $reviews[$k]			= $result->ratingInfo;
            $json['reviews']    = $reviews;
        }
        else{
            $json['error']['msg']	= 'No rating found.';
            $this->statusCode	= 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
}