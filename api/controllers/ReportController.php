<?php
namespace api\controllers;
use common\models\Notifications;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\db\Expression;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use	yii\filters\ContentNegotiator;

use common\models\Order;

class ReportController extends ActiveController
{
    public $statusCode = 200;
    public $page_size	= 20;
    public $modelClass = 'common\models';
    public $language_id;
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']	= [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['error'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        return $behaviors;
    }
    public function actions(){
        $this->language_id = isset($_REQUEST['language_id']) ? (int)$_REQUEST['language_id'] : '1';
        $actions			= parent::actions();

        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }
    protected function verbs(){
        return [
            'index'				=> ['GET']
        ];
    }
    public function actionIndex($start_date,$end_date,$type=null)
    {
        $user_info		= Yii::$app->user->identity;
        $supplier_id	= $user_info->id;
        $start_date_ex  = explode('/',$start_date);
        $end_date_ex    = explode('/',$end_date);
        $start_date     = mktime(0,0,0,$start_date_ex[1],$start_date_ex[0],$start_date_ex[2]);
        $end_date       = mktime(0,0,0,$end_date_ex[1],$end_date_ex[0],$end_date_ex[2]);
        $datediff       = $end_date - $start_date;
        $days           = round($datediff / (60 * 60 * 24));
        if($end_date>$start_date){
            if($type===null){
                if($days<=7)
                    $type = 'days';
                else if($days>7 && $days<=30)
                    $type = 'weeks';
                else
                    $type = 'month';
            }
            $final_results = [];
            $order_ststus_id = 5;
            if($type=='days'){
                for ($i=$start_date; $i<=$end_date; $i+=86400) {
                    $start_date1 = date('Y-m-d',$i);
                    $end_date1   = date('Y-m-d',$i);
                    $title = date("d M Y",$i);
                    $reports    = $this->reports($supplier_id,$start_date1,$end_date1,$type);

                    $order_total        = 0;
                    $order_total_products = 0;
                    $order_total_orders = 0;
                    $order_currency     = config_default_currency;
                    if(!empty($reports)){
                        $order_total            = $reports['total'];
                        $order_total_orders     = $reports['total_orders'];
                        $order_total_products   = $reports['products'];
                    }
                    $ios_start_date = date('d/m/Y',strtotime($start_date1));
                    $ios_end_date = date('d/m/Y',strtotime($end_date1));

                    $final_results[] = ['title'=>$title,'total'=>$order_total,'total_orders'=>$order_total_orders,'total_products_sale'=>$order_total_products,'currency'=>$order_currency,'start_date'=>$start_date1,'end_date'=>$end_date1,'ios_start_date'=>$ios_start_date,'ios_end_date'=>$ios_end_date];
                }
            }
            if($type=='weeks'){
                $periods = new \DatePeriod(
                    new \DateTime(date('Y-m-d',$start_date)),
                    new \DateInterval('P1W'),
                    new \DateTime(date('Y-m-d',$end_date))
                );
                foreach($periods as $k=>$period){
                    $week_name  =  $period->format("W");
                    $start_date1 = $period->format("Y-m-d");
                    $end_date11 = strtotime($start_date1)+(6*86400);

                    if($end_date11>$end_date){
                        $end_date1  = date('Y-m-d',$end_date);
                    }else
                        $end_date1  = date('Y-m-d',$end_date11);

                    $order_total        = 0;
                    $order_total_products = 0;
                    $order_total_orders = 0;
                    $order_currency     = config_default_currency;
                    $reports    = $this->reports($supplier_id,$start_date1,$end_date1,$type);
                    if(!empty($reports)){
                        $order_total            = $reports['total'];
                        $order_total_orders     = $reports['total_orders'];
                        $order_total_products   = $reports['products'];
                    }
                    $ios_start_date = date('d/m/Y',strtotime($start_date1));
                    $ios_end_date = date('d/m/Y',strtotime($end_date1));
                    $final_results[] = ['title'=>'Week '.$week_name,'total'=>$order_total,'total_orders'=>$order_total_orders,'total_products_sale'=>$order_total_products,'currency'=>$order_currency,'start_date'=>$start_date1,'end_date'=>$end_date1,'ios_start_date'=>$ios_start_date,'ios_end_date'=>$ios_end_date];
                }
            }

            if($type=='month'){
                $k  = 0;
                $all_years = [];
                for ($i=$start_date; $i<=$end_date; $i+=86400) {
                    if($k==0){
                        $e_date = date('Y-m-d',$i);
                        $s_date = date('Y-m-d',mktime(0,0,0,date('m',$start_date),date('d',$start_date),date("Y",$start_date)));
                    }
                    else{
                        $e_date = date('Y-m-d',mktime(23,59,59,date('m',$i),date('d',$i),date("Y",$i)));
                        $s_date = date('Y-m-d',mktime(0,0,0,date('m',$i),1,date("Y",$i)));
                    }
                    $all_years[date('M,Y',$i)] = ['start_date'=>$s_date,'end_date'=>$e_date];
                    $k++;
                }
                $l = 0;
                foreach($all_years as $k=>$all_year){
                    if($l==0)
                        $start_date1 = date('Y-m-d',$start_date);
                    else
                        $start_date1 = $all_year['start_date'];
                    $end_date1  = $all_year['end_date'];

                    $order_total        = 0;
                    $order_total_products = 0;
                    $order_total_orders = 0;
                    $order_currency     = config_default_currency;
                    $reports    = $this->reports($supplier_id,$start_date1,$end_date1,$type);
                    if(!empty($reports)){
                        $order_total            = $reports['total'];
                        $order_total_orders     = $reports['total_orders'];
                        $order_total_products   = $reports['products'];
                    }
                    $ios_start_date = date('d/m/Y',strtotime($start_date1));
                    $ios_end_date = date('d/m/Y',strtotime($end_date1));
                    $final_results[] = ['title'=>$k,'total'=>$order_total,'total_orders'=>$order_total_orders,'total_products_sale'=>$order_total_products,'currency'=>$order_currency,'start_date'=>$start_date1,'end_date'=>$end_date1,'ios_start_date'=>$ios_start_date,'ios_end_date'=>$ios_end_date];
                    $l++;
                }
            }

            if(!empty($final_results)){
                $json['results']= $final_results;
                $start_date1 = date("Y-m-d",$start_date);
                $end_date1 = date("Y-m-d",$end_date);
                $total_data = $this->totalData($supplier_id,$start_date1,$end_date1);
                $json['total_data'] = $total_data;
            }

            else{
                $json['error']['msg']	= 'No result found.' ;
                $this->statusCode		= 401;
            }
        }else{
            $json['error']['msg']	= 'Please select valid date.' ;
            $this->statusCode		= 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    protected function reports($supplier_id,$start_date,$end_date,$type ){
        $model = (new \yii\db\Query());
        $model->from('tbl_order as o');
        $model->select(new Expression('o.order_id,unix_timestamp(MIN(o.date_added)) as min_ts,unix_timestamp(MAX(o.date_added)) as max_ts,MIN(o.date_added) AS date_start, MAX(o.date_added) AS date_end, COUNT(*) AS `total_orders`,SUM((SELECT SUM(op.quantity) FROM  tbl_order_products as op WHERE op.order_id = o.order_id GROUP BY op.order_id)) AS products,SUM((SELECT SUM(ot.value) FROM tbl_order_total as ot WHERE ot.order_id = o.order_id AND ot.code = "vat" GROUP BY ot.order_id)) AS tax, SUM(o.total) AS `total`'));
        $model->where(['and',['o.supplier_id'=>$supplier_id],['>=',new Expression('DATE(o.date_added)'),$start_date],['<=',new Expression('DATE(o.date_added)'),$end_date]]);
//        if($type=='days'){
//            $model->groupBy(new Expression('YEAR(o.date_added), MONTH(o.date_added), DAY(o.date_added)'));
//        }elseif($type==='weeks'){
//            $model->groupBy(new Expression('YEAR(o.date_added), WEEK(o.date_added)'));
//        }elseif($type==='month'){
//            $model->groupBy(new Expression('YEAR(o.date_added), MONTH(o.date_added)'));
//        }
//        elseif($type==='year'){
//            $model->groupBy(new Expression('YEAR(o.date_added)'));
//        }
        $model->orderBy('o.date_added DESC');

        return $results =  $model->one();
    }
    protected function totalData($supplier_id,$start_date,$end_date){
        $model = (new \yii\db\Query());
        $query = $model->from('tbl_order as o');
        $condition = ['and',['o.supplier_id'=>$supplier_id],['>=',new Expression('DATE(o.date_added)'),$start_date],['<=',new Expression('DATE(o.date_added)'),$end_date]];

        $query->select(new Expression("SUM(o.total) as total,SUM((SELECT SUM(op.quantity) FROM  tbl_order_products as op WHERE op.order_id = o.order_id GROUP BY op.order_id)) as quantity"));
        $query->where($condition);

        $total_res = $query->one();
        $total = 0;
        if(!empty($total_res))
            $total   = $total_res['total'];

        $json['default_currency'] = config_default_currency;
        $json['total_revenue'] = $total;
        $json['my_plan_fee'] = 10;
        $json['my_kitchen_revenue'] = $json['total_revenue']- $json['my_plan_fee'];
        $json['total_meals_sold'] = $total_res['quantity'];

        $query = $model->from('tbl_order as o');
        $query->select(new Expression('total,date_added'));
        $query->where($condition);
        $query->orderBy('total desc');
        $total_res = $query->one();
        $best_selling_day = '';
        if(!empty($total_res))
            $best_selling_day   = date("l (d F,Y)",strtotime($total_res['date_added']));
        $json['best_selling_day'] = $best_selling_day;

        $model = (new \yii\db\Query());
        $query = $model->from('tbl_order_products as o');
        $query->select(new Expression("name,count(product_id) as tp"));
        $query->where(new Expression('`order_id` in ((select order_id from  `tbl_order` `o` WHERE (`o`.`supplier_id`=26) AND (DATE(o.date_added) >= "'.$start_date.'") AND (DATE(o.date_added) <= "'.$end_date.'")))'));
        $query->groupBy('product_id');
        $query->orderBy('tp desc');
        $total_pros = $query->one();
        $json['best_selling_product'] = '';
        if(!empty($total_pros))
            $json['best_selling_product'] =$total_pros['name'];
         return $json;
    }
}