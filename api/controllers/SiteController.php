<?php
namespace api\controllers;

use Yii;
use yii\helpers\Url;
use yii\db\Expression;
use yii\web\Response;
use yii\rest\ActiveController;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use	yii\filters\ContentNegotiator;

use common\models\User;
use common\models\Order;
use common\models\Banners;
use common\models\Country;
use common\models\Library;
use common\models\HelpRequests;
use common\models\DriverOrderStatus;
use common\models\ReferData;
use common\models\CancelReasons;
use common\models\ContactMessage;
use common\models\MainCategory;
use frontend\models\PasswordResetRequestForm;

/**
 * Site controller
 */
class SiteController extends ActiveController
{
	public $time_to_wait = 300;
	public $extra_time = 1200;
	public $statusCode = 200;
	public $modelClass = 'common\models';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
		$behaviors['contentNegotiator']	= [
			'class' => ContentNegotiator::className(),
			'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
		];
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'except' => ['error','index','getcountry','refer','deviceinfo','settime','cancelreasons','reset'],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		$behaviors['corsFilter'] = [
			'class' => \yii\filters\Cors::className(),
			'cors' => [
				'Origin' => ['*'],
				'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
				'Access-Control-Request-Headers' => ['*'],
				'Access-Control-Max-Age' => 3600,
				'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
			],
		];
		return $behaviors;
    }

	public function actions(){
		$actions			= parent::actions();

		unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);

        return $actions;
	}
	protected function verbs(){
        return [
            'index'	=> ['GET'],
            'getcountry' => ['GET'],
            'cancel-reasons' => ['GET'],
            'refer' => ['GET'],
            'banners' => ['GET'],
            'contact' => ['POST'],
            'askforhelp' => ['POST'],
            'reset' => ['POST'],
        ];
    }

    public function actionIndex()
    {
		$json['message'] = 'Welcome to homepage';
        return $json;
    }
    
	public function actionError(){
		
		$json['error']	= 'The requested page does not exist.';
		return $json;
	}

    /**
     *  Get all countries
     *  @Data method GET
     *  @Response array
     */
	public function actionGetcountry(){
		$results	= Country::find()->orderBy('countryname asc')->asArray()->all();
		foreach($results as $k=>$result){
			$json['success'][$k]['code']			= $result['code'];
			$json['success'][$k]['country_name']	= $result['countryname'];
			$json['success'][$k]['nationality']		= $result['nationality'];
			$json['success'][$k]['flag']			= Url::to(Yii::$app->params['website_path'].'/uploads/flags/'.strtolower($result['code']).'.png',true);
		}
		$json['code']		= $this->statusCode;
		return $json;
	}

    /**
     *  Send contact message
     *  @Data method POST
     *  @Auth bearer token required
     *  @Required params user_id, subject, email,message,role
     *  @Response array
     */
	public function actionContact(){
		$model = new ContactMessage();
		$user_id	= Yii::$app->user->identity->id;
		$post_data['ContactMessage'] = Yii::$app->request->post();
		$model->user_id	= $user_id;
		if ($model->load($post_data) && $model->validate()) {
			if($model->save()){
				$model->sendEmail;
				$json['success']['msg']	= "Thank you for contacting us. We will respond to you as soon as possible.";
			}
		}else{
			$all_errors				= [];
			$errors					= $model->getErrors();
			foreach($errors as $key=>$error){
				$all_errors[]	= $error[0];
			}
			$json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
			$this->statusCode		= 401;
		}
		$json['code']		= $this->statusCode;
		return $json;
	}

    /**
        Insert refer url
        @Data method GET
        @Required params code
        @Response redirect to app URL
    */
	public function actionRefer($code){
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_HTML;

        $user_info = User::find()->where(['share_id'=>$code])->one();
        if(!empty($user_info)){
            if(\Yii::$app->mobileDetect->isMobile()){
                if(\Yii::$app->mobileDetect->isiOS()){
                    $this->ReferIns($code,$user_info);
                    return $this->redirect('https://itunes.apple.com/app/id568839295?mt=8&pt=com.blinkslabs.Blinkist');
                }
                if(\Yii::$app->mobileDetect->isAndroidOS()){
                    $this->ReferIns($code,$user_info);
                    return $this->redirect('https://play.google.com/store/apps/details?id=com.blinkslabs.blinkist.android');
                }
            }else
                echo '<h3 style="text-align:center;">Something went wrong!!</h3>';
        }else
            echo '<h3 style="text-align:center;">Invalid Link!</h3>';
    }
    protected function ReferIns($code,$user_info){
        $model = new ReferData();
        $model->ip_address = Yii::$app->getRequest()->getUserIP();
        $model->code = $code;
        $model->user_id = $user_info->id;
        $model->user_agent  = Yii::$app->getRequest()->getUserAgent();
        $model->server_info =json_encode($_SERVER);
        $model->created_at = time();
        $model->save();
    }

    public function actionGetDriver(){
        $library = new Library();
        $all_orders = Order::find()->where(['and',['is','order_accepted',null],['not IN','order_status_id',[5,2]]])->all();
        if(!empty($all_orders)){
            foreach($all_orders as $order){
            $current_time   = time();
              $d = new \DateTime($order->date_added);
                $date_added = $d->getTimestamp();
            $time = $date_added+$order->dish_prep_time+$this->extra_time;
             $time_remaining = $time-$current_time;

                if($time_remaining<=0){
                    $order_id = $order->order_id;
                    $order->order_status_id = 2;
                    $order->driver_id = null;
                    $order->order_accepted = null;
                    $order->save();


                    $order_info['user_id']        = $order->user_id;
                    $order_info['role']           = 1;
                    $order_info['from_user']      = $order->user_id;
                    $order_info['type']           = 'order';
                    $order_info['reason']         = 'Driver was not available';
                    $order_info['config_name']    = 'Your order has been cancelled';
                    $order_info['config_value']['order_id']= $order_id;
                    $order_info['config_value']['message']= 'Your order has been cancelled';
                    $library->saveNotification($order_info);
                }else{
                    $time_left = ($date_added+$order->dish_prep_time)-time();
                    if($time_left<=$this->extra_time){
                        if($order->driver_id==null){
                            $order->driver;
                        }else{
                            $order_id = $order->order_id;
                            $driver_id = $order->driver_id;

                            $time = time();
                            $driver_order_status = DriverOrderStatus::find()->where(['order_id'=>$order_id,'driver_id'=>$driver_id,'order_status'=>'Pending'])->one();
                            if(!empty($driver_order_status)){
                                $remaining_time = $time-$driver_order_status->created_at;
                                if($remaining_time>=$this->time_to_wait){
                                    $data['driver_id'] = $driver_id;
                                    $data['order_id']   = $order_id;
                                    $data['order_status'] = 'Cancelled';
                                    $library->updateDriverHistory($data);

                                    $order_info['user_id']       = $driver_id;
                                    $order_info['role']          = 4;
                                    $order_info['from_user']     = $driver_id;
                                    $order_info['type']          = 'order';
                                    $order_info['config_name']    = 'Your order has been cancelled.';
                                    $order_info['config_value']['order_id']= $this->order_id;
                                    $order_info['config_value']['message']= 'Your order has been cancelled.';
                                    $library->saveNotification($order_info);

                                    $order->driver_id = null;
                                    $order->order_accepted = null;
                                    $order->save();

                                    $order->driver;

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function actionAskforhelp(){
        $post_data['HelpRequests'] = Yii::$app->request->post();
        $model = new HelpRequests();
        $latitude = ($post_data['HelpRequests']['lat']) ?? '';
        $longitude = ($post_data['HelpRequests']['lng']) ??  '';
        if($model->load($post_data) && $model->validate()){
//            $query      = User::find()->select(new Expression("*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $latitude - latitude) *  pi()/180 / 2), 2) +COS( $latitude * pi()/180) * COS(latitude * pi()/180) * POWER(SIN(( $longitude - longitude) * pi()/180 / 2), 2) ))) as distance  "))->where(['and',['is_sales_person'=>'yes'],['status'=>10],['!=','longitude',''],['!=','latitude','']]);
//            $query->having("distance <= 20")->orderBy("distance");
//
//            $query->having("distance <= 20")->orderBy("distance");
            $user_model		= new User();
            $headers		= getallheaders();
            $token			= '';
            if(isset($headers['Authorization']))
                $token	= str_replace("Bearer ","",$headers['Authorization']);
            $user_info		= $user_model->findIdentityByAccessToken($token);

            $results = $model->getSalesperson($user_info);
            if(!empty($results)){
                $model->request_status = 1;
                if($model->save()){
                    $library = new Library();
                    foreach($results as $result){
                        $order_info['user_id']        = $result->id;
                        $order_info['role']           = 5;
                        $order_info['from_user']      = 1;
                        $order_info['type']           = 'help_request';
                        $order_info['request_id']     = $model->request_id;
                        $order_info['request_status']  = 'pending';
                        $order_info['request_type']  = $model->request_type;
                        $order_info['config_name']    = $model->name.' is looking for a sales representative';
                        $order_info['config_value']['request_id']= $model->request_id;
                        $order_info['config_value']['message']= $model->name.' is looking for a sales representative';
                        $order_info['config_value']['name'] = $model->name;
                        $order_info['config_value']['location'] = $model->location;
                        $order_info['config_value']['phonenumber']= $model->phonenumber;
                        $order_info['config_value']['lat']= $model->lat;
                        $order_info['config_value']['lng']= $model->lng;
                        $order_info['config_value']['device_token']= $model->device_token;
                        $library->saveNotification($order_info);

                    }
                    $json['success']['msg']		= 'Your request has been sent';
                }
            }else{
                $json['error']['msg']	= 'Sorry no sales person found in your location.';
                $this->statusCode = 401;
            }

        }else{
            $all_errors				= [];
            $errors					= $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;

    }
    public function actionAskforhelp11(){
        $user_info			= Yii::$app->user->identity;

        $user_detail = $user_info->loggedinInfo;
        $user_id = $user_info->id;
        $post_data = Yii::$app->request->post();
        $latitude = isset($post_data['latitude']) ? ($post_data['latitude']) : '';
        $longitude = isset($post_data['longitude']) ? ($post_data['longitude']) : '';
        if(!empty($longitude) && !empty($latitude)){
            $query      = User::find()->select(new Expression("*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $latitude - latitude) *  pi()/180 / 2), 2) +COS( $latitude * pi()/180) * COS(latitude * pi()/180) * POWER(SIN(( $longitude - longitude) * pi()/180 / 2), 2) ))) as distance  "))->where(['and',['is_sales_person'=>'yes'],['status'=>10],['!=','id',$user_id],['!=','longitude',''],['!=','latitude','']]);
            $query->having("distance <= 20")->orderBy("distance");
             $results = $query->all();
             if(!empty($results)){
                 $post_data['HelpRequests'] = Yii::$app->request->post();
                $model = new HelpRequests();
                $model->from_user   = $user_id;
                $model->request_status = 1;
                if($model->load($post_data) && $model->save()){
                    $library = new Library();
                    foreach($results as $result){
                        $order_info['user_id']        = $result->id;
                        $order_info['role']           = 5;
                        $order_info['from_user']      = $user_id;
                        $order_info['type']           = 'help_request';
                        $order_info['request_id']     = $model->request_id;
                        $order_info['request_status']  = 'pending';
                        $order_info['request_type']  = $model->request_type;
                        $order_info['config_name']    = $user_detail['name'].' is looking for a sales representative';
                        $order_info['config_value']['request_id']= $model->request_id;
                        $order_info['config_value']['message']= $user_detail['name'].' is looking for a sales representative';
                        $library->saveNotification($order_info);
                    }
                    $json['success']['msg']		= 'Your request has been sent';
                }else{
                    $all_errors				= [];
                    $errors					= $model->getErrors();
                    foreach($errors as $key=>$error){
                        $all_errors[]	= $error[0];
                    }
                    $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                    $this->statusCode = 401;
                }
             }else{
                 $json['error']['msg']	= 'Sorry no sales person found in your location.';
                 $this->statusCode = 401;
             }
        }else{
            $json['error']['msg']	= 'Please enter latitude and longitude';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    public function actionCancelreasons(){
         $json['data'] = CancelReasons::find()->select(["reason_id","name"])->where(['status'=>1])->asArray()->all();
        $json['code']		= $this->statusCode;
         return $json;
    }

    public function actionBanners($type='user'){
	    if($type=='user')
            $results = Banners::find()->where(['status'=>'active','type'=>$type])->asArray()->all();
	    if($type=='supplier'){
            $user_info = Yii::$app->user->identity;
            $results = Banners::find()->where(['status'=>'active','type'=>$type,'categories' => $user_info->supplier_type])->asArray()->all();
        }

        if(!empty($results)){
            foreach($results as $result)
                $data[] = ['banner_id' => $result['banner_id'],'name' => $result['name'],'image' => Yii::$app->params['website_path'].'/uploads/'.$result['image']];

            $json['data'] = $data;
        }else{
            $json['error']['msg']	= 'No banner found.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionReset(){
        $model = new PasswordResetRequestForm();
        $post_data['PasswordResetRequestForm'] = Yii::$app->request->post();
        if ($model->load($post_data) && $model->validate()) {
            if ($model->sendEmail()) {
                $json['success']['msg']		= 'Please check your email for further instructions.';
            }
        }else{
            $all_errors				= [];
            $errors					= $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
}