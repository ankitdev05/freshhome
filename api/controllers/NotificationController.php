<?php
namespace api\controllers;
use common\models\Order;
use common\models\Notifications;
use common\models\MainCategories;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use	yii\filters\ContentNegotiator;

class NotificationController extends ActiveController
{
    public $statusCode = 200;
    public $page_size	= 20;
    public $modelClass = 'common\models';
    public $language_id;
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']	= [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['error'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        return $behaviors;
    }
    public function actions(){
        $this->language_id = isset($_REQUEST['language_id']) ? (int)$_REQUEST['language_id'] : '1';
        $actions			= parent::actions();

        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }
    protected function verbs(){
        return [
            'index'				=> ['GET'],
            'read'				=> ['POST'],
        ];
    }
    public function actionIndex($role)
    {
        $user_info		= Yii::$app->user->identity;
        $user_id	= $user_info->id;

        $query		= Notifications::find()->where(['status'=>1,'user_id'=>$user_id,'role'=>$role]);
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'notification_id' => SORT_DESC,
                ]
            ],
        ]);
        $count			= $dataProvider->getTotalCount();
        $total_pages	= ceil($count/$this->page_size);
        $data = [];
        if($count>0){
            $results	= $dataProvider->getModels();
            $json['total_pages']		    = $total_pages;
            $json['unread_notifications']   = Notifications::find()->where(['status'=>1,'user_id'=>$user_id,'role'=>$role,'is_read'=>0])->count();
            foreach($results as $k=>$result){
                if($role ==5){
                    $cat_name = MainCategories::findOne($result->request_type);
                    //$data[$cat_name->code][] = $result->salesnotifications;
                    $data[] = $result->salesnotifications;
                }
                else{
                    $not = $result->getNormalNotification($role);
                    if(!empty($not))
                    $data[] =  $not;
                }

                //$result->is_read = 1;
                //$result->save();
            }
            $json['data'] = (array)$data;

        }else{
            $json['error']['msg']	= 'No notification found.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionRead(){
        $user_info		= Yii::$app->user->identity;
        $user_id	    = $user_info->id;
        $post_data 	= Yii::$app->request->post();
        if(isset($post_data['notification_id'])){
            $result		= Notifications::find()->where(['status'=>1,'user_id'=>$user_id,'notification_id'=>$post_data['notification_id']])->one();
            if(!empty($result)){
                $json['success']['msg']		= 'Data updated successfully.';
                $result->is_read = 1;
                $result->save();
            }else{
                $json['error']['msg']	= 'Something went wrong.';
                $this->statusCode = 401;
            }
        }else{
            $json['error']['msg']	= 'Please send notification id.';
            $this->statusCode = 401;
        }

        $json['code']		= $this->statusCode;
        return $json;
    }
}