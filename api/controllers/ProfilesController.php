<?php
namespace api\controllers;
use Yii;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

use	yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\User;
use common\models\Order;
use common\models\CreditCards;
use common\models\Library;
use common\models\Braintree;
use common\models\HoldPayments;
use common\models\HoldTransactions;

class ProfilesController extends ActiveController
{
    public $statusCode = 200;
    public $page_size = 30;
    public $language_id = 1;
    public $modelClass = 'common\models';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']	= [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['error','index'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function actions(){
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }

    /**
     * {@inheritdoc}
     */
    protected function verbs(){
        return [
            'index' => ['GET'],
            'home' => ['GET'],
            'profile' => ['GET'],
            'suppliers' => ['GET'],
            'accept-order' => ['POST'],
            'update' => ['POST'],
        ];
    }

    public function actionIndex(){
        $json['message'] = 'Welcome to '.config_name;
        return $json;
    }

    public function actionHome(){
        $user_info =Yii::$app->user->identity;
        $user_id	= $user_info->id;
        $profile = $user_info->driverInfo;


        $order_info	= Order::find()->where(['and',['driver_id'=>$user_id],['!=','order_status_id',5],['order_accepted'=>1]])->all();
        if(!empty($order_info)){
            $json['unread_notifications']   = Notifications::find()->where(['status'=>1,'user_id'=>$user_id,'role'=>4,'is_read'=>0])->count();
            foreach($order_info as $k=>$order)
                $json['data'][] = $order->driverOrderInfo;


            $json['driver_data']	= $profile;

        }else{
            $json['driver_data']	= $profile;
            $json['error']['msg']	= 'No order found.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionProfile(){
        $user_data 			= Yii::$app->user->identity;
        $profile = $user_data->driverInfo;

        $json['data']       = $profile;
        $json['data']['access_token'] = $user_data->auth_key;
        $json['data']['driver_id']    = $user_data->id;
        $json['code']					= $this->statusCode;
        return $json;

    }
    public function actionUpdate(){
        $post_data['User'] 	= Yii::$app->request->post();
        $user_data = Yii::$app->user->identity;
        $user_id = $user_data->id;


        $file_data				= [];
        if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic'])){
            foreach($_FILES['profile_pic'] as $key=>$pic_data){
                $file_data[$key]	= ['profile_pic'=>$pic_data];
            }
        }
        if(!empty($file_data))
            $_FILES['User'] = $file_data;
        if ($user_data->load($post_data) && $user_data->validate()) {
            $profile_pic 		= UploadedFile::getInstance($user_data, 'profile_pic');
            if(!empty($profile_pic)){
                $library   			 = new Library();
                $user_data->profile_pic  = $library->saveFile($profile_pic,'users');
                $user_data->image_approval = 0;
            }
            if($user_data->save()){
                $json['data']       = $user_data->driverInfo;
                $json['data']['access_token'] = $user_data->auth_key;
                $json['data']['driver_id']    = $user_data->id;
            }else{
                $all_errors				= [];
                $errors					= $user_data->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }


        }else{
            $all_errors				= [];
            $errors					= $user_data->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionSuppliers($latitude,$longitude){
        $user_information = Yii::$app->user->identity;
        if($user_information->is_driver=='yes' || $user_information->driver_id!=""){

            $food_delivery = User::find()->select(new Expression("*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $latitude - latitude) *  pi()/180 / 2), 2) +COS( $longitude * pi()/180) * COS(latitude * pi()/180) * POWER(SIN(( $longitude - longitude) * pi()/180 / 2), 2) ))) as distance  "))->where(['and',['availability' => 'online','supplier_type'=>1],['status'=>10],['!=','id',$this->id],['!=','longitude',''],['!=','latitude','']])->having("distance <= 30")->orderBy("distance")->limit(30)->all();

            $other_delivery = User::find()->select(new Expression("*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $latitude - latitude) *  pi()/180 / 2), 2) +COS( $longitude * pi()/180) * COS(latitude * pi()/180) * POWER(SIN(( $longitude - longitude) * pi()/180 / 2), 2) ))) as distance"))->where(['and',['!=','supplier_type',1],['status'=>10,'availability' => 'online'],['!=','id',$this->id],['!=','longitude',''],['!=','latitude','']])->having("distance <= 30")->orderBy("distance")->limit(30)->all();
            $food_suppliers = [];
            $others_suppliers = [];
            if(!empty($food_delivery)){
                foreach($food_delivery as $food){
                    $user_info = $food->userInfo;
                    $pending_orders = $food->pendingOrders;
                    $food_suppliers[] = [
                        'name' => $user_info['name'],
                        'phone_number' => $user_info['phone_number'],
                        'building_no' => $user_info['building_no'],
                        'flat_no' => $user_info['flat_no'],
                        'floor_no' => $user_info['floor_no'],
                        'location' => $user_info['location'],
                        'landmark' => $user_info['landmark'],
                        'availability' => $user_info['availability'],
                        'profile_pic' => $user_info['profile_pic'],
                        'city_name' => $user_info['city_name'],
                        'latitude' => $user_info['latitude'],
                        'longitude' => $user_info['longitude'],
                        'supplier_type' => $food->supplier_type,
                        'pending_orders' => $pending_orders['count'],
                        'orders_data' => $pending_orders['orders_data'],
                    ];
                }
            }

            if(!empty($other_delivery)){
                foreach($other_delivery as $food){
                    $pending_order = $food->pendingOrders;
                    $user_info = $food->userInfo;
                    $others_suppliers[] = [
                        'name' => $user_info['name'],
                        'phone_number' => $user_info['phone_number'],
                        'building_no' => $user_info['building_no'],
                        'flat_no' => $user_info['flat_no'],
                        'floor_no' => $user_info['floor_no'],
                        'location' => $user_info['location'],
                        'landmark' => $user_info['landmark'],
                        'availability' => $user_info['availability'],
                        'profile_pic' => $user_info['profile_pic'],
                        'city_name' => $user_info['city_name'],
                        'latitude' => $user_info['latitude'],
                        'longitude' => $user_info['longitude'],
                        'supplier_type' => $food->supplier_type,
                        'pending_orders' => $pending_order['count'],
                        'orders_data' => $pending_order['orders_data'],
                    ];
                }
            }

            $json['data']['food_suppliers'] = $food_suppliers;
            $json['data']['others_suppliers'] = $others_suppliers;

            $driver_info['name'] = $user_information->name;
            $driver_info['email'] = $user_information->email;
            $driver_info['availability'] = $user_information->availability;
            $driver_info['is_supplier'] = $user_information->is_supplier;
            $driver_info['is_user'] = $user_information->is_user;
            $driver_info['is_driver'] = $user_information->is_driver;
            $driver_info['subscription_status'] = $user_information->subscriptionData;
            $driver_info['wallet_amount'] = $user_information->balance;

            $json['data']['driver_info'] = $driver_info;
        }else{
            $this->statusCode	 = 401;
            $json['error']['msg']	= "Please subscribe the driver package.";
        }
        $json['code']	= $this->statusCode;
        return $json;
    }

    public function actionAcceptOrder(){
        $orderIds = $_POST['order_ids'] ?? 0;
        $credit_card_id = $_POST['credit_card'] ?? 0;

        $user_info = Yii::$app->user->identity;
        $allow = false;
        $user_id  = $user_info->id;
        $order_ids = explode(',', $orderIds);
        $old_order_count  = $user_info->countDriverAcceptedOrders+(count($order_ids));
        $message = 'Please subscribe the driver package.';
        $driver_doc = $user_info->driverDocActiveStatus;
        $credit_card = CreditCards::find()->where(['id' => $credit_card_id,'user_id' => $user_id])->one();
        if($user_info->is_driver=='yes'){
            $allow = true;
            $time = time();
            if($user_info->subscription_end < $time)
                $allow = false;

        }if ($driver_doc==0){
            $allow = false;
            $message	= "Your document verification is pending.";
        }
        if($old_order_count>5){
            $allow = false;
            $message	= "You can't accept more than 5 orders.";
        }

        if(empty($credit_card)){
            $allow = false;
            $message	= "Please select valid credit card.";
        }
        if(empty($orderIds)){
            $allow = false;
            $message	= "Please select some orders.";
        }
        if($allow==true){
            $acc_orders = [];
            $reject_orders = [];
            foreach ($order_ids as $order_id){
                $order_info = Order::find()->where(['and',['order_status_id' => 4],['is','order_accepted',null],['order_id'=>$order_id]])->one();
                    if(!empty($order_info)){
                        $order_info->driver_id = $user_id;
                        $order_info->order_accepted = 1;
                        if($order_info->save()){
                            $library = new Library();
                            $data['driver_id'] = $user_id;
                            $data['order_id']   = $order_id;
                            $data['order_status'] = 'Accepted';
                            $library->updateDriverHistory($data);
                            $holdPayment = $this->holdPayment($order_info,$credit_card);
                            if($holdPayment==true)
                                $acc_orders[] = $order_id;
                            else{
                                $order_info->driver_id = null;
                                $order_info->order_accepted = null;
                                $order_info->save();
                                $reject_orders[] = $order_id;
                            }
                        }


                }else{
                   $reject_orders[] = $order_id;
                }
            }
            if(!empty($acc_orders)){
                $json['success']['msg']	= "Order #".implode(',',$acc_orders)." order has been accepted successfully.";
            }
            if(!empty($reject_orders)){
                $json['error']['msg']	= "You can't accept "."Order #".implode(',',$reject_orders);
            }
        }else{
            $this->statusCode	 = 401;
            $json['error']['msg']	= $message;
        }
        $json['code']	= $this->statusCode;
        return $json;
    }
    protected function holdPayment($order_info,$credit_card){
        $result =true;
        $hold_model = new HoldPayments();
        $hold_model->order_id = $order_info->order_id;
        $hold_model->status = 'hold';
        $hold_model->amount = $order_info->total;
        $hold_model->currency_code = $order_info->currency_code;
        if($hold_model->save()){
            $braintree_model = new Braintree();
            $response = $braintree_model->authPayment($order_info->total,$credit_card->token,'hold_'.$hold_model->hold_id);
            if($response['error']==true){
                $hold_model->delete();
                $result =false;
            }else{
                $this->orderTransaction($response['response'],$hold_model->hold_id,$order_info->order_id);
            }
        }

        return $result;
    }

    protected function orderTransaction($response,$hold_id,$order_id){

        $model = new HoldTransactions();
        $model->transaction_id = $response['id'];
        $model->status = $response['status'];
        $model->type = $response['type'];
        $model->currency_iso_code = $response['currencyIsoCode'];
        $model->amount = $response['amount'];
        $model->merchantAccountId = $response['merchantAccountId'];
        $model->order_id = $order_id;
        $model->hold_id = $hold_id;
        $model->full_response = serialize($response);
        $model->payment_type = 'main';
        $model->payment_status = 'on_hold';
        $model->payment_method = 'braintree';
        $model->save();
    }
}