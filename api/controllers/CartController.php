<?php
namespace api\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\rest\ActiveController;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use	yii\filters\ContentNegotiator;

use common\models\User;
use common\models\Menu;
use common\models\Order;
use common\models\Library;
use common\models\Currency;
use common\models\OrderTotal;
use common\models\OrderRatings;
use common\models\OrderHistory;
use common\models\OrderProducts;
use common\models\MenuRating;
use api\models\Cart;
/**
 * Cart controller
 */
class CartController extends ActiveController{
	public $statusCode = 200;
	public $modelClass = 'common\models';
	/**
     * {@inheritdoc}
     */
	public function behaviors()
    {
        $behaviors = parent::behaviors();
		$behaviors['contentNegotiator']	= [
			'class' => ContentNegotiator::className(),
			'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
		];
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'except' => ['error','index'],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		$behaviors['corsFilter'] = [
			'class' => \yii\filters\Cors::className(),
			'cors' => [
				'Origin' => ['*'],
				'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
				'Access-Control-Request-Headers' => ['*'],
				'Access-Control-Max-Age' => 3600,
				'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
			],
		];
		return $behaviors;
    }
	
	public function actions(){
		$actions			= parent::actions();

		unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
	}
	protected function verbs(){
        return [
            'index'		=> ['GET'],
            'viewCart'	=> ['GET'],
            'add' 		=> ['POST'],
            'checkout'	=> ['POST'],
            'rating'	=> ['POST'],
            'menurating'	=> ['POST'],
        ];
    }
	
	public function actionIndex()
    {
		$json['message'] = 'Welcome to '.config_name;
        return $json;
    }
	
	public function actionAdd(){

		$confirm			= 0;
		$model				= new Cart();
		$library			= new Library();
		
		$user_info			= Yii::$app->user->identity;
		$user_id			= $user_info->id;
		$post_data['Cart']	= Yii::$app->request->post();
		
		if(isset($post_data['Cart']['confirm']) && $post_data['Cart']['confirm']==1)
			$confirm = 1;


//$message = print_r($post_data,true);
// Yii::$app->mailer->compose()
//                   ->setHtmlBody($message)
//                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
//                    ->setTo('rahuloberai@icloud.com')
//                    ->setSubject('Notification')
//                    ->send();
// die;
		$cart	= $library->json_decodeArray($user_info->cart);
		if($model->load($post_data) && $model->validate()) {
			$menu_id 	= $model->menu_id;
			$quantity 	= (int)$model->quantity;
			$result		= Menu::find()->where(['id'=>$menu_id])->one();
			if(!empty($result)){
                if( isset($cart[$result->supplier_id][$menu_id]['product_attributes'])){
                    $product_attributes = $cart[$result->supplier_id][$menu_id]['product_attributes'];
                    $post_data['Cart']['product_attributes'] = $product_attributes;
                }

                $validate_cart = $result->validateCart($post_data['Cart']);
                if(!empty($validate_cart)){
                    if(isset($validate_cart['error'])){
                        $json['error']['msg'] = $validate_cart['error'];
                        $this->statusCode = 401;
                        $json['code']	= $this->statusCode;
                        return $json;
                    }
                }
				if($quantity==0){
					$json['success']		= 'Your product has been removed successfully.';
					unset($cart[$result->supplier_id][$menu_id]);
					if(empty($cart[$result->supplier_id]))
						unset($cart[$result->supplier_id]);
					if(!empty($cart))
						$cart_val = $library->json_encodeArray($cart);
					else
						$cart_val = null;
					$user_info->cart = $cart_val;
					$user_info->save();
				}else{
					if(!empty($cart)){
						if(!isset($cart[$result->supplier_id])){
							$supplier_info = user::findOne($result->supplier_id);
							if($confirm==0){
								$json['is_allow'] 		= 0;
								$json['confirm']		= 'Your cart contains items from '.$supplier_info->name.' - '.$supplier_info->location.'. Do you wish to clear your cart and start a new order here?';
								$json['code']	= $this->statusCode;

                                if($this->statusCode==200){
                                    $cart_items = 0;
                                    $cart_total = (object)[];
                                    $cart = new Cart();
                                    $total_items = $cart->getTotalCartItems($user_info->id);
                                    if(isset($total_items['total_items'])){
                                        $cart_items = $total_items['total_items'];
                                        $cart_total = $total_items['total_price'];
                                    }
                                    $json['cart_items'] = $cart_items;
                                    $json['cart_total'] = $cart_total;
                                }
								return $json;
							}
							if($confirm==1)
								$cart = [];
							
						}
					}

                    $product_attributes = $post_data['Cart']['product_attributes'] ?? [];

                    $cart[$result->supplier_id][$menu_id]	= ['menu_id'=>(int)$menu_id,'quantity'=>(int)$quantity,'product_attributes' => $product_attributes];

					$cart_val = $library->json_encodeArray($cart);
					$user_info->cart = $cart_val;
					$user_info->save();
					$json['is_allow'] 		= 1;
					$json['success']		= 'Your cart has been updated successfully.';
				}
				
			}else{
				$json['error']['msg']	= 'Sorry this product does not exist or currently not active.';
				$this->statusCode		= 401;
			}
			
			
		}else{
			$all_errors				= [];
			$errors					= $model->getErrors();
			foreach($errors as $key=>$error){
				$all_errors[]	= $error[0];
			}
			$json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
			$this->statusCode		= 401;
		}
		$json['code']	= $this->statusCode;
		if($this->statusCode==200){
		    $cart_items = 0;
		    $cart_total = (object)[];
            $cart = new Cart();
            $total_items = $cart->getTotalCartItems($user_info->id);
            if(isset($total_items['total_items'])){
                $cart_items = $total_items['total_items'];
                $cart_total = $total_items['total_price'];
            }
            $json['cart_items'] = $cart_items;
            $json['cart_total'] = $cart_total;
        }
		return $json;
	}
	
	public function actionViewCart(){
		
		$library			= new Library();
		$user_info			= Yii::$app->user->identity;
		$cart_data			= $library->json_decodeArray($user_info->cart);
		if(!empty($cart_data)){
			$cart_model		= new Cart();
			$output 		= $cart_model->cartData($user_info->id,'all');

			$json['supplier_info'] 	= ($output['cart']['supplier_info']);
			$json['cart_total'] 	= array_values($output['total_amounts']);
		}else{
			$json['error']['msg']	= 'Your cart is empty' ;
			$this->statusCode		= 401;
		}
		$json['code']	= $this->statusCode;
		return $json;
	}
    public function actionCheckout(){
        $post_data['Order']	= Yii::$app->request->post();
        $user_info			= Yii::$app->user->identity;
        $model = new Order();
        $json = $model->addToCart($user_info,$post_data);
        if(isset($json['error']['msg']))
            $this->statusCode = 401;
        $json['code']	= $this->statusCode;
        return $json;
    }
	public function actionCheckout1(){
		$post_data['Order']	= Yii::$app->request->post();
		$library			= new Library();
		$user_info			= Yii::$app->user->identity;
		$user_detail        = User::findOne($user_info->id);
		$user_detail        = $user_detail->userDetail;
		$cart_data			= $library->json_decodeArray($user_info->cart);

		if(!empty($cart_data)){
			$cart_model		= new Cart();
			$output 		= $cart_model->cartData($user_info->id);
			$total_price 	= end($output['total_amounts']);
			$currency_info	= Currency::findOne(['code'=>config_default_currency]);
			$order_type     = 'online';
			$date           = null;
			$time           = null;

			if(isset($post_data['Order']['order_type']))
                $order_type = $post_data['Order']['order_type'];

            if(isset($post_data['Order']['collection_time']))
                $time = $post_data['Order']['collection_time'];

            if(isset($post_data['Order']['collection_date']))
                $date = $post_data['Order']['collection_date'];
            if($order_type=='online'){
                $alow_order = true;
                $menu_items = [];
                foreach($output['cart']['supplier_info']['cart_data'] as $menu){
                    $dish_id = $menu['menu_id'];
                    $dish_info = Menu::findOne($dish_id);
                    if(!empty($dish_info) && $dish_info->status=='inactive'){
                        $alow_order = false;
                        $menu_items[] = $dish_info->dish_name;
                    }
                }
                if($alow_order==false){
                    $json['error']['msg']	='Your cart contains some in active items. Please remove '.implode(',',$menu_items).' and try again.';
                    $this->statusCode = 401;
                    $json['code']	= $this->statusCode;
                    return $json;
                }
            }

			$model					= new Order();
			$model->order_type		= $order_type;
			$model->date		    = $date;
			$model->time		    = $time;
			$model->user_id			= $user_info->id;
			$model->supplier_id		= $output['cart']['supplier_info']['supplier_id'];
			
			$model->currency_id		= $currency_info->id;
			$model->currency_code	= config_default_currency;
			$model->ip				= Yii::$app->getRequest()->getUserIP();
			$model->total			= $total_price['total_value'];
			$model->order_status_id	= 1;
			$model->estimate_deliver_time	= 3600;
			if(empty($model->date))
                $model->order_status_id	= 11;

			$model->payment_code	= 'cod';
			$model->payment_method	= 'Cash on delivery';
			if($model->load($post_data) && $model->validate()) {
				if($model->save()){
                    if(empty($model->date))
                        $datetime = date("Y-m-d G:i:s",$model->created_at);
                    else{
                        $strtotime = ($model->date.' '.date("G:i",strtotime($model->time)));
                        $date_time = explode(' ',$strtotime);
                        $date = $date_time[0];
                        $time = $date_time[1];

                        $date_exp = explode('/',$date);
                        $time_exp = explode(':',$time);

                        $time_stamp = mktime($time_exp[0],$time_exp[1],0,$date_exp[1],$date_exp[0],$date_exp[2]);
                        $datetime = date("Y-m-d G:i:s",$time_stamp);

                    }
                    $model->date_added = $datetime;
                    $model->save();

					$order_history	= new OrderHistory();
					$order_history->order_id			= $model->order_id;
					$order_history->order_status_id		= $model->order_status_id;
					$order_history->notify				= 1;
					$order_history->comment				= 'Your order is under processing.';
					$order_history->save();
					$product_ids = [];
					foreach($output['cart']['supplier_info']['cart_data'] as $m=>$menu){
					    if($order_type =="online" && $menu['status']=="active"){
                            $order_products			= new OrderProducts();
                            $order_products->order_id	= $model->order_id;
                            $order_products->product_id	= $menu['menu_id'];
                            $order_products->menu_id	= $menu['menu_id'];
                            $order_products->name		= $menu['dish_name'];
                            $order_products->quantity	= $menu['quantity'];
                            $order_products->price		= $menu['dish_price'];
                            $order_products->total		= $menu['total_value'];
                            $order_products->save();
                            $product_ids[] = $menu['menu_id'];
                            unset($cart_data[$model->supplier_id][$menu['menu_id']]);
                            $cart_val = $library->json_encodeArray($cart_data);
                            $user_info->cart = $cart_val;
                            $user_info->save();
                        }
                        if($order_type =="pre-order" && $menu['status']=="inactive"){
                            $order_products			= new OrderProducts();
                            $order_products->order_id	= $model->order_id;
                            $order_products->product_id	= $menu['menu_id'];
                            $order_products->menu_id	= $menu['menu_id'];
                            $order_products->name		= $menu['dish_name'];
                            $order_products->quantity	= $menu['quantity'];
                            $order_products->price		= $menu['dish_price'];
                            $order_products->total		= $menu['total_value'];
                            $order_products->save();
                            $product_ids[] = $menu['menu_id'];
                            unset($cart_data[$model->supplier_id][$menu['menu_id']]);
                            $cart_val = $library->json_encodeArray($cart_data);
                            $user_info->cart = $cart_val;
                            $user_info->save();
                        }

					}
					if(!empty($product_ids)){
					    $dish_prep_time = Menu::find()->where(['in','id',$product_ids])->max('dish_prep_time');
					    $model->dish_prep_time = $dish_prep_time;
					    $model->save();
                    }
					foreach($output['total_amounts'] as $k=>$total_amount){
						$order_total = new OrderTotal();
						$order_total->order_id		= $model->order_id;
						$order_total->code			= $total_amount['code'];
						$order_total->title			= $total_amount['label'];
						$order_total->value			= $total_amount['total_value'];
						$order_total->sort_order	= ($k+1);
						$order_total->save();
					}
					$library = new Library();
					$order_info['user_id']        = $model->supplier_id;
                    $order_info['role']            = 2;
					$order_info['from_user']        = $model->user_id;
					$order_info['type']           = 'order';
					$order_info['config_name']    = 'You\'ve received a new order from '.$user_detail['name'];
					$order_info['config_value']['order_id']= $model->order_id;
					$order_info['config_value']['message']= 'You\'ve received a new order from '.$user_detail['name'];
					$library->saveNotification($order_info);
					$model->sendEmail;
					if(isset($cart_data[$model->supplier_id]) && count($cart_data[$model->supplier_id])==0)
    					$user_info->cart = null;
					$user_info->save();
					$json['success']['msg']		= 'Congratulations!! Your order has been placed successfully.';
					if($model->dish_prep_time<=1200)
					    $model->driver;
				}
			}else{
                $all_errors				= [];
                $errors					= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }
		}else{
			$json['error']['msg']	= 'Your cart is empty' ;
			$this->statusCode		= 401;
		}
		$json['code']	= $this->statusCode;
		return $json;
	}
	public function actionRating(){
        $user_id			= Yii::$app->user->identity->id;
        $post_data	        = Yii::$app->request->post();
        if(isset($post_data['order_id']) && !empty($post_data['order_id'])){
            $order_id       = $post_data['order_id'];
            $order_info     = Order::find()->where(['order_id'=>$order_id,'user_id'=>$user_id])->one();
            if(!empty($order_info)){
                $res = OrderRatings::find()->where(['order_id'=>$order_id,'user_id'=>$user_id])->one();
                if(!empty($res))
                    $res->delete();
                $supplier_id = $order_info->supplier_id;
                $taste_rating          = isset($post_data['taste_rating']) ? $post_data['taste_rating'] : null;
                $presentation_rating   = isset($post_data['presentation_rating']) ? $post_data['presentation_rating'] : null;
                $packing_rating        = isset($post_data['packing_rating']) ? $post_data['packing_rating'] : null;
                $overall_rating        = isset($post_data['overall_rating']) ? $post_data['overall_rating'] : null;
                $review                = isset($post_data['review']) ? $post_data['review'] : null;

                $order_rating                      = new OrderRatings();
                $order_rating->order_id            = $order_id;
                $order_rating->user_id             = $user_id;
                $order_rating->supplier_id         = $supplier_id;
                $order_rating->taste_rating        = $taste_rating;
                $order_rating->presentation_rating = $presentation_rating;
                $order_rating->packing_rating      = $packing_rating;
                $order_rating->overall_rating      = $overall_rating;
                $order_rating->review              = $review;
                $order_rating->is_active           = 1;
                if($order_rating->save()){
                    $user_detail        = User::findOne($user_id);
                    $user_detail        = $user_detail->userDetail;

                    $library = new Library();
                    $order_info1['user_id']        = $order_info->supplier_id;
                    $order_info1['from_user']        = $order_info->user_id;
                    $order_info1['role']            = 2;
                    $order_info1['type']           = 'order_rating';
                    $order_info1['config_name']    = 'You\'ve received a new review from '.$user_detail['name'];
                    $order_info1['config_value']['order_id']= $order_info->order_id;
                    $order_info1['config_value']['message']= 'You\'ve received a new order from '.$user_detail['name'];
                    $library->saveNotification($order_info1);

                    $json['success']['msg']		= 'Thanks! for your rating and valuable review.';
                }else{
                    $all_errors				= [];
                    $errors					= $order_rating->getErrors();
                    foreach($errors as $key=>$error){
                        $all_errors[]	= $error[0];
                    }
                    $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                    $this->statusCode = 401;
                }
            }else{
                $json['error']['msg']	= 'No order found.' ;
                $this->statusCode		= 401;
            }
        }else{
            $json['error']['msg']	= 'Something went wrong!!' ;
            $this->statusCode		= 401;
        }
        $json['code']	= $this->statusCode;
        return $json;
    }
    public function actionMenurating(){
        $user_id			= Yii::$app->user->identity->id;
        $post_data	        = Yii::$app->request->post();
        if(isset($post_data['order_id']) && !empty($post_data['order_id']) && isset($post_data['menu_id']) && !empty($post_data['menu_id'])){
            $order_id       = $post_data['order_id'];
            $menu_id       = $post_data['menu_id'];
            $order_info     = Order::find()->where(['order_id'=>$order_id,'user_id'=>$user_id])->one();
            if(!empty($order_info)){
                MenuRating::deleteAll(['order_id'=>$order_id,'user_id'=>$user_id,'menu_id'=>$menu_id]);

                $supplier_id = $order_info->supplier_id;
                $rating        = isset($post_data['rating']) ? $post_data['rating'] : 0;
                $review                = isset($post_data['reviews']) ? $post_data['reviews'] : null;

                $menu_rating          = new MenuRating();
                $menu_rating->order_id  = $order_id;
                $menu_rating->user_id   = $user_id;
                $menu_rating->supplier_id= $supplier_id;
                $menu_rating->menu_id      =  $menu_id;
                $menu_rating->rating        = $rating;
                $menu_rating->reviews       = $review;
                if($menu_rating->save()){
                    $dish_info 		= Menu::findOne($menu_id);
                    $user_detail        = User::findOne($user_id);
                    $user_detail        = $user_detail->userDetail;

                    $library = new Library();
                    $order_info1['user_id']        = $order_info->supplier_id;
                    $order_info1['from_user']        = $user_id;
                    $order_info1['role']            = 2;
                    $order_info1['type']           = 'dish_rating';
                    $order_info1['config_name']    = $user_detail['name']. ' sent you a new review for your dish '.$dish_info->dish_name;
                    $order_info1['config_value']['order_id']= $order_info->order_id;
                    $order_info1['config_value']['message']=  $user_detail['name']. ' sent you a new review for your dish '.$dish_info->dish_name;
                    $library->saveNotification($order_info1);
                    $json['success']['msg']		= 'Thanks! for your rating and valuable review.';
                }else{
                    $all_errors				= [];
                    $errors					= $menu_rating->getErrors();
                    foreach($errors as $key=>$error){
                        $all_errors[]	= $error[0];
                    }
                    $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                    $this->statusCode = 401;
                }
            }else{
                $json['error']['msg']	= 'No order found.' ;
                $this->statusCode		= 401;
            }
        }else{
            $json['error']['msg']	= 'Something went wrong!!' ;
            $this->statusCode		= 401;
        }
        $json['code']	= $this->statusCode;
        return $json;
    }
}