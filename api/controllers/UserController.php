<?php
namespace api\controllers;

use common\models\Wallet;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use	yii\filters\ContentNegotiator;

use common\models\User;
use common\models\Order;
use common\models\Apiuser;
use common\models\Library;
use common\models\Braintree;
use common\models\CreditCards;
use common\models\UserAuth;
use common\models\UserRole;
use common\models\UserData;
use common\models\ReferData;
use common\models\UserAddress;
use common\models\UserFavMenu;
use common\models\UserToRoles;
use common\models\ApiLoginForm;
use common\models\OrderHistory;
use common\models\OrderCancel;
use common\models\DriverDocuments;
use common\models\LoginActivity;
use common\models\OrderRatings;
use common\models\UserFavSupplier;
use common\models\UserBankAccount;

/**
 * Site controller
 */
class UserController extends ActiveController
{
	public $statusCode = 200;
	public $page_size = 30;
	public $modelClass = 'common\models';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
		$behaviors['contentNegotiator']	= [
			'class' => ContentNegotiator::className(),
			'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
		];
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'except' => ['error','index','checknumber','login','register','verifyaccount','gmaillogin','supplierinfo','supplier-rating'],      
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			], 
		];
		$behaviors['corsFilter'] = [
			'class' => \yii\filters\Cors::className(),
			'cors' => [
				'Origin' => ['*'],
				'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
				'Access-Control-Request-Headers' => ['*'],
				'Access-Control-Max-Age' => 3600,
				'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
			],
		];
		return $behaviors;
    }

	public function actions(){
		$actions			= parent::actions();

		unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
	}
	protected function verbs(){
        return [
            'index'				=> ['GET'],
            'myfav'				=> ['GET'],
			'login' 			=> ['POST'],
            'profile' 			=> ['POST'],
            'register'			=> ['POST'],
            'checknumber'		=> ['POST'],
			'verifyaccount'		=> ['GET'],
			'mywallet'		    => ['GET'],
			'changepassword' 	=> ['POST'],
			'updateprofile' 	=> ['POST'],
			'updateuserprofile'	=> ['POST'],
			'gmaillogin' 		=> ['POST'],
			'repeat-order' 		=> ['POST'],
			'cancel-order' 		=> ['POST'],
			'add-bank' 		    => ['POST'],
			'update-bank' 		=> ['POST'],
			'view-bank' 		=> ['GET'],
			'favsupplier' 		=> ['GET','POST','DELETE'],
			'favmenu' 			=> ['GET','POST','DELETE'],
			'address' 			=> ['GET','POST','DELETE'],
			'supplierinfo'		=> ['GET'],
			'orders'			=> ['GET'],
			'order'				=> ['GET'],
			'supplier-rating'	=> ['GET'],
			'updatelocation'	=> ['POST'],
			'forgot-password'	=> ['POST'],
			'switch'	        => ['POST'],
			'change-email'	    => ['POST'],
			'driver-update'	    => ['POST'],
			'change-document'	=> ['POST'],
			'switch-to-driver'	=> ['POST'],

        ];
    }

    public function actionIndex()
    {
		$json['message'] = 'Welcome to user module';
        return $json;
    }
    
	public function actionError(){
		
		$json['error']	= 'The requested page does not exist.';
		return $json;
	}
	
	public function actionChecknumber(){
		$model							= new Apiuser();
		$model->scenario				= 'checknumber';
		$post_data['Apiuser']			= Yii::$app->request->post();
		
		if ($model->load($post_data) && $model->validate()) {
			if($model->findByPhonenumber($model->phone_number)){
				$json['error']['msg'] 	= 'Phone number is already in use';
				$this->statusCode 		= 401;
			}else{
				$json['success']['msg'] = 'Otp sent successfully';
			}
		}else{
			$all_errors				= [];
			$errors					= $model->getErrors();
			foreach($errors as $key=>$error){
				$all_errors[]	= $error[0];
			}
			$json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
			$this->statusCode 			= 401;
		}
		$json['code']					= $this->statusCode;
		return $json;
	}
	
	public function actionLogin(){
		$json							= [];
		$model							= new ApiLoginForm();
		$post_data['ApiLoginForm'] 		= Yii::$app->request->post();
		if ($model->load($post_data) && $model->login()) {
			$user_info = Yii::$app->user->identity;
			if($user_info->status === 10){
			    if($model->role==4){
			        $count = DriverDocuments::find()->where(['user_id'=>$user_info->id,'status'=>1])->orderBy('id desc')->count();
			        if($count==0){
                        $json['error']['msg']	= 'Your document verification is pending.';
                        $this->statusCode = 401;
                        $json['code'] = $this->statusCode;
                        return $json;
                    }
                }
				$user					= User::findOne($user_info->id);
				$user->last_login		= time();
				$user->last_login_ip	= Yii::$app->getRequest()->getUserIP();
				$user->save();
				$login_act				= new LoginActivity();
				$login_act->user_id		= $user_info->id;
				$login_act->ip_address	= Yii::$app->getRequest()->getUserIP();

				$login_act->save();
				if(isset($post_data['ApiLoginForm']['device_token']) && !empty($post_data['ApiLoginForm']['device_token'])){
                    $device_token = $post_data['ApiLoginForm']['device_token'];
                    $user->device_token = $device_token;
                    $user->save();
                }
				if($post_data['ApiLoginForm']['role']==1)
	    			$json['success']		= $user->userDetail;
                if($post_data['ApiLoginForm']['role']==2)
                    $json['success']		= $user->userInfo;
                if($post_data['ApiLoginForm']['role']==5)
                    $json['success']		= $user->salesDetail;
                if($post_data['ApiLoginForm']['role']==4)
                    $json['success']		= $user->userDetail;
                $user->last_logged_in_as    = $post_data['ApiLoginForm']['role'];
                $user->save();
			}else{
				$json['error']['msg']	= 'Please verify your email address.';
				$this->statusCode		= 401;
			}
		}else{
			$all_errors				= [];
			$errors					= $model->getErrors();
			foreach($errors as $key=>$error){
				$all_errors[]	= $error[0];
			}
			$json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
			$this->statusCode			= 401;
		}
		$json['code']					= $this->statusCode;
		return $json;
	}
	
	public function actionRegister(){
        $post_data['Apiuser'] = Yii::$app->request->post();
		$model = new Apiuser();
		if(isset($post_data['Apiuser']['role']) && $post_data['Apiuser']['role']==4)
            $model->scenario = 'driver_register';
		else
    	    $model->scenario = 'register';

		if(isset($post_data['Apiuser']["DOB"]) && !empty($post_data['Apiuser']["DOB"])){
			$post_data['Apiuser']["dob"] = $post_data['Apiuser']["DOB"];
			unset($post_data['Apiuser']["DOB"]);
		}
        $post_data['Apiuser']['city'] = isset($post_data['Apiuser']['city']) ? (int)$post_data['Apiuser']['city'] : null;
		if( $post_data['Apiuser']['city']==0)
            $post_data['Apiuser']['city'] = 1;

        $file_data = [];
        if(isset($_FILES['document']) && !empty($_FILES['document'])){
            foreach($_FILES['document'] as $key=>$pic_data){
                $file_data[$key]	= ['document'=>$pic_data];
            }
        }
        if(!empty($file_data))
            $_FILES['Apiuser'] = $file_data;

        $document  = UploadedFile::getInstance($model, 'document');
        $model->document = $document;
		if ($model->load($post_data) && $model->validate()) {
		    $document_name = '';
            if(!empty($document) ){
                $library   			 = new Library();
                $document_name = $library->saveFile($document,'driver_docs');
            }else $model->document = null;

			$pass_hash 			= $model->password;
			$model->setPassword($pass_hash);
			$model->generateAuthKey();
			$model->generateEmailVerificationToken();
			$model->email_verify = 0;
			//$model->role		 = 1;
			//$model->status		 = 0;
			$model->availability = 'offline';
			$model->is_user		= 'yes';
			$model->is_supplier	= 'no';
			
			if($model->role==1)
				$model->is_user = 'yes';
			elseif($model->role==2)
				$model->is_supplier = 'yes';
            elseif($model->role==4)
                $model->is_driver = 'yes';
			
			$model->ip_address		= Yii::$app->getRequest()->getUserIP();
			$model->last_login		= time();
			$model->last_login_ip	= Yii::$app->getRequest()->getUserIP();
            if(isset($post_data['Apiuser']['device_token']))
                $model->device_token = $post_data['Apiuser']['device_token'];

			if($model->save()){

			    if(!empty($document)){
                    $driver_docs = new DriverDocuments();
                    $driver_docs->user_id = $model->id;
                    $driver_docs->status = 0;
                    $driver_docs->document = $document_name;
                    $driver_docs->file_info = serialize($document);
                    $driver_docs->save();
                }
				if($model->is_user == 'yes')
					$model->updateUserData;
                if($model->is_supplier == 'yes')
                    $model->updateSuppData;

				$user_to_role	= new UserToRoles();
				$user_to_role->user_id	= $model->id;
				$user_to_role->role_id	= $model->role;
				$user_to_role->save();

				$json['success']['name']	= $model->name;
				$json['success']['role']	= strtolower($model->userRole->name);
				$json['success']['msg']	= 'An email has been sent. Please verify your account.';
				$model->sendVerifyEmail($model);
                $this->referIns($model);
			
			}else{
                $all_errors				= [];
                $errors					= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }
		}else{
			$all_errors				= [];
			$errors					= $model->getErrors();
			foreach($errors as $key=>$error){
				$all_errors[]	= $error[0];
			}
			$json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
			$this->statusCode = 401;
		}
		$json['code']		= $this->statusCode;
		return $json;
		
	}
	
	public function actionGmaillogin(){
		$model					= new Apiuser();
		$model->scenario		= 'gmaillogin';
		$post_data['Apiuser'] 	= Yii::$app->request->post();
		$email					= isset($post_data['Apiuser']['email']) ? $post_data['Apiuser']['email'] : '';
		$user_exist				= $model->findByUsername($email);
		
		
		if ($model->load($post_data) && $model->validate() && !$user_exist) {
			$umodel		= new User();
			
			$code		= $umodel->genrateCode('u');
				
			$password 			= Yii::$app->security->generateRandomString(6);
			$e_explode			= explode('@',$email);
			$model->ip_address		= Yii::$app->getRequest()->getUserIP();
			$model->last_login		= time();
			$model->last_login_ip	= Yii::$app->getRequest()->getUserIP();
			
			$model->username	= $e_explode[0];
			$model->setPassword($password);
			$model->generateAuthKey();
			$model->email_verify = 1;
			$model->role		 = 1;
			$model->status		 = 10;
			$model->availability = 'offline';
			$model->share_id		= $code;
			$model->temp_password	= 1;
			$model->is_user			= 'yes';
			$model->is_supplier		= 'no';
            if(isset($post_data['Apiuser']['device_token']))
                $model->device_token = $post_data['Apiuser']['device_token'];

			$model->save();
			$model->updateUserData;
			
			$user_to_role			= new UserToRoles();
			$user_to_role->user_id	= $model->id;
			$user_to_role->role_id	= $model->role;
			$user_to_role->save();
			$model->sendGmailEmail($email,$password,$model);
            $this->referIns($model);
			
			$user_auth		= new UserAuth();
			$user_auth->user_id = $model->id;
			$user_auth->source = 'gmail';
			$user_auth->source_id = isset($post_data['Apiuser']['source_id']) ? $post_data['Apiuser']['source_id'] : '';
			$user_auth->save();
		
			$json['success']		= User::findOne($model->id)->userInfo;
		}else{
			if($user_exist && $user_exist->status==10){
				$json['success']		= User::findOne($user_exist->id)->userInfo;
				
				$check_auth = UserAuth::find()->where(['user_id'=>$user_exist->id])->one();
				if(!$check_auth){
					$user_auth		= new UserAuth();
					$user_auth->user_id = $user_exist->id;
					$user_auth->source = 'gmail';
					$user_auth->source_id = isset($post_data['Apiuser']['source_id']) ? $post_data['Apiuser']['source_id'] : '';
					$user_auth->save();
				}
				
			}else{
				$json['error']['msg']	= 'Something went wrong!!. Please contact with site owner.';
				$this->statusCode = 401;
			}
		}
		
		$json['code']		= $this->statusCode;
		return $json;
	}
	protected function referIns($user_info){
        $ip_address = $user_info->ip_address;
        $refer = ReferData::find()->where(['ip_address'=>$ip_address])->OrderBy('id desc')->one();
        if(!empty($refer)){
            $user_info->refer_by = $refer->user_id;
            $user_info->save();
        }

    }
	public function actionVerifyaccount($token){
		$response = Yii::$app->response;
		$response->format = \yii\web\Response::FORMAT_HTML;
		$model			= new User();
		$result			= $model->findIdentityByAccessToken($token);
		if(!empty($result) && $result->email_verify===0){
			$role_info	= UserRole::findOne($result->role);
			$code		= '';
			if(!empty($role_info)){
				$umodel		= new User();
				$prefix		= strtolower($role_info->code);
				$code		= $umodel->genrateCode($prefix);
			}
			
			$result->share_id		= $code;
			$result->status 	 	= 10;
			$result->email_verify 	= 1;
			$result->save();
			echo '<h1 style="text-align:center;">Thanks! Your email has been verified.</h1>';
		}else
			echo '<h3 style="text-align:center;">Invalid Link!</h3>';
	}
	
	public function actionProfile($is_user=null){
		$user_info 			= Yii::$app->user->identity;
		if($is_user==1)
			$json['success']	= $user_info->userDetail;
		else
			$json['success']	= $user_info->userInfo;
		$json['code']		= $this->statusCode;
		return $json;
	}
	
	public function actionChangepassword(){
		$model					= new Apiuser();
		$model->scenario		= 'changepassword';
		$post_data['Apiuser'] 	= Yii::$app->request->post();
		if ($model->load($post_data) && $model->validate()) {
			$result					= Apiuser::findOne(Yii::$app->user->id);
			$result->password_hash	= Yii::$app->security->generatePasswordHash($model->new_password);
			$result->temp_password	= 0;
			$result->save();
			$json['success']['id']	= $result->id;
			$json['success']['msg']	= 'Your password has been changed successfully.';
		}else{
			$all_errors				= [];
			$errors					= $model->getErrors();
			foreach($errors as $key=>$error){
				$all_errors[]	= $error[0];
			}
			$json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
			$this->statusCode = 401;
		}
		$json['code']		= $this->statusCode;
		return $json;
	}
	
	public function actionUpdateuserprofile(){
		$user_id	= Yii::$app->user->identity->id;
		$user_ = User::findOne($user_id);
		$model		= UserData::findOne(['user_id'=>$user_id]);
		if(empty($model)){
			$model = new UserData();
			$model->user_id	 = $user_id;
		}
		$model->scenario		= 'updateprofile';	
		
		$post_data['UserData'] 	= Yii::$app->request->post();
		if(isset($post_data['UserData']['header_image'])){
            $header_image = $post_data['UserData']['header_image'];
            $user_->header_image = $header_image;
            $user_->save();
        }

		$file_data				= [];
		if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic'])){
			foreach($_FILES['profile_pic'] as $key=>$pic_data){
				$file_data[$key]	= ['profile_pic'=>$pic_data];
			}
		}
		if(!empty($file_data))
			$_FILES['UserData'] = $file_data;
		
		if ($model->load($post_data) && $model->validate()) {
			
			if(isset($post_data['UserData']['DOB']))
				$model->dob = $post_data['UserData']['DOB'];
			
			$profile_pic 		= UploadedFile::getInstance($model, 'profile_pic');
			if(!empty($profile_pic)){
				$library   			 = new Library();
				$model->profile_pic  = $library->saveFile($profile_pic,'users');
				$model->image_approval = 1;
			}
			$model->save();
			$json['success']			= User::findOne($model->user_id)->userDetail;
			$json['success']['msg']		= 'Your profile has been updated successfully.';
		}else{
			$all_errors				= [];
			$errors					= $model->getErrors();
			foreach($errors as $key=>$error){
				$all_errors[]	= $error[0];
			}
			$json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
			$this->statusCode = 401;
		}
		$json['code']		= $this->statusCode;
		return $json;
	}
	
	public function actionUpdateprofile(){
		$user_info 				= Yii::$app->user->identity;
		$model					= Apiuser::findOne($user_info->id);
		$model->scenario		= 'updateprofile';
		
		$post_data['Apiuser'] 	= Yii::$app->request->post();
		$file_data				= [];
		if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic'])){
			foreach($_FILES['profile_pic'] as $key=>$pic_data){
				$file_data[$key]	= ['profile_pic'=>$pic_data];
			}
		}
		if(!empty($file_data))
			$_FILES['Apiuser'] = $file_data;
		$more_text		= '';
		if ($model->load($post_data) && $model->validate()) {
			if(isset($post_data['Apiuser']['DOB']))
				$model->dob = $post_data['Apiuser']['DOB'];
			$profile_pic 		= UploadedFile::getInstance($model, 'profile_pic');
			if(!empty($profile_pic)){
				$library   			 = new Library();
				$model->profile_pic  = $library->saveFile($profile_pic,'users');
				$model->image_approval = 0;
				$more_text	= ' Please wait till image approval from Admin.';
			}
            if(isset($post_data['Apiuser']['device_token']) && !empty($post_data['Apiuser']['device_token']))
                $model->device_token = $post_data['Apiuser']['device_token'];

			$model->save();
			$json['success']			= User::findOne($model->id)->userInfo;
			$json['success']['msg']		= 'Your profile has been updated successfully.'.$more_text;
		}else{
			$all_errors				= [];
			$errors					= $model->getErrors();
			foreach($errors as $key=>$error){
				$all_errors[]	= $error[0];
			}
			$json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
			$this->statusCode = 401;
		}
		$json['code']		= $this->statusCode;
		return $json;
	}
	
	public function actionMyfav(){
		$user_info		= Yii::$app->user->identity;
		$menus			= $user_info->userFavMenus;
		$suppliers		= $user_info->userFavSuppliers;
		if(empty($menus) && empty($suppliers)){
			$json['error']	= 'No data found';
			$this->statusCode = 401;
		}else{
			$json['success']['suppliers']	= $suppliers;
			$json['success']['menus']		= $menus;
		}
		$json['code']	= $this->statusCode;
		return $json;
	}
	
	public function actionFavsupplier($supplier_id=null){
		$user_id	= Yii::$app->user->identity->id;
		$model		= new UserFavSupplier();
		$post_data['UserFavSupplier']	= Yii::$app->request->post();
		
		if(Yii::$app->request->isPost){
			$model->user_id	= $user_id;
			if ($model->load($post_data) && $model->validate()) {
				$result	= UserFavSupplier::find()->where(['user_id'=>$user_id,'supplier_id'=>$model->supplier_id])->one();
				if(!empty($result))
					$result->delete();
				$model->save();
				$json['success']['msg']	= 'Successfully added as favorite.';
			}else{
				$all_errors				= [];
				$errors					= $model->getErrors();
				foreach($errors as $key=>$error){
					$all_errors[]	= $error[0];
				}
				$json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
				$this->statusCode = 401;
			}
			
		}elseif(Yii::$app->request->isGet){
			$results	= UserFavSupplier::find()->where(['user_id'=>$user_id])->all();
			if(!empty($results)){
				$user_data	= [];
				foreach($results as $result)
					$user_data[]	= $result->supplier->userInfo;
				$json['success']	= $user_data;		
			}else{
				$json['error']['msg']	= 'Favorite list is empty';
				$this->statusCode = 401;
			}
		}
		elseif(Yii::$app->request->isDelete){
			$result	= UserFavSupplier::find()->where(['user_id'=>$user_id,'supplier_id'=>$supplier_id])->one();
			if(!empty($result)){
				$result->delete();
				$json['success']['msg']	= 'Successfully deleted from your favorite.';
			}else{
				$json['error']['msg']	= 'Something went wrong!!. Please try again';
				$this->statusCode = 401;
			}
		}
		$json['code']		= $this->statusCode;
		return $json;
	}
	
	public function actionFavmenu($dish_id=null){
		$user_id	= Yii::$app->user->identity->id;
		$model		= new UserFavMenu();
		$post_data['UserFavMenu']	= Yii::$app->request->post();
		
		if(Yii::$app->request->isPost){
			$model->user_id	= $user_id;
			if ($model->load($post_data) && $model->validate()) {
				$result	= UserFavMenu::find()->where(['user_id'=>$user_id,'menu_id'=>$model->menu_id])->one();
				if(!empty($result))
					$result->delete();
				$model->save();
				$json['success']['msg']	= 'Successfully added as favorite.';
			}else{
				$all_errors				= [];
				$errors					= $model->getErrors();
				foreach($errors as $key=>$error){
					$all_errors[]	= $error[0];
				}
				$json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
				$this->statusCode = 401;
			}
			
		}elseif(Yii::$app->request->isGet){
			$results	= UserFavMenu::find()->where(['user_id'=>$user_id])->all();
			if(!empty($results)){
				$user_data	= [];
				foreach($results as $result)
					$user_data[]	= $result->menu->dishInfo;
				$json['success']	= $user_data;		
			}else{
				$json['error']['msg']	= 'Favorite list is empty';
				$this->statusCode = 401;
			}
		}elseif(Yii::$app->request->isDelete){
			$result	= UserFavMenu::find()->where(['user_id'=>$user_id,'menu_id'=>$dish_id])->one();
			if(!empty($result)){
				$result->delete();
				$json['success']['msg']	= 'Successfully deleted from your favorite.';
			}else{
				$json['error']['msg']	= 'Something went wrong!!. Please try again';
				$this->statusCode = 401;
			}
		}
		$json['code']		= $this->statusCode;
		return $json;
	}
	public function actionAddress($address_id=null){
		$user_id					= Yii::$app->user->identity->id;
		$model						= new UserAddress();
		$post_data['UserAddress']	= Yii::$app->request->post();

		if(Yii::$app->request->isPost){
			$model->user_id = $user_id;
			if(isset($post_data['UserAddress']['address_id'])){
				$model	= UserAddress::find()->where(['user_id'=>$user_id,'address_id'=>$post_data['UserAddress']['address_id']])->one();
				if(!$model){
					$model			= new UserAddress();
					$model->user_id = $user_id;
				}
			}

			if ($model->load($post_data) && $model->validate()) {
				if($model->isNewRecord)
					$message = 'Your address has been added successfully.';
				else
					$message = 'Your address has been updated successfully.';

				if(isset($post_data['UserAddress']['is_default']) && $post_data['UserAddress']['is_default']=='yes'){
					UserAddress::updateAll(['is_default'=>'no'],'user_id='.$user_id);
					$model->is_default = 'yes';
				}
				if($model->save())
					$json['success']['msg']	= $message;
			}else{
				$all_errors				= [];
				$errors					= $model->getErrors();
				foreach($errors as $key=>$error){
					$all_errors[]	= $error[0];
				}
				$json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
				$this->statusCode = 401;
			}
		}elseif(Yii::$app->request->isDelete){
			$result	= UserAddress::find()->where(['user_id'=>$user_id,'address_id'=>$address_id])->one();
			if(!empty($result)){
				$result->is_delete = 1;
				$result->save();
				$json['success']['msg']	= 'Your address has been deleted successfully.';
			}else{
				$json['error']['msg']	= 'Something went wrong!!. Please try again';
				$this->statusCode = 401;
			}
		}elseif(Yii::$app->request->isGet){
			if(!empty($address_id)){
				$result	= UserAddress::find()->where(['user_id'=>$user_id,'address_id'=>$address_id])->one();
				if(!empty($result))
					$json['success']	= $result->addressData;
				else{
					$json['error']['msg']	= 'Something went wrong!!. Please try again';
					$this->statusCode = 401;
				}
			}else{
				$results	= UserAddress::find()->where(['user_id'=>$user_id,'is_delete'=>0])->all();
				if(!empty($results)){
					$address_data	= [];
					foreach($results as $result)
						$address_data[]	= $result->addressData;
					$json['success']['data']	= $address_data;		
				}else{
					$json['error']['msg']	= 'Address list is empty';
					$this->statusCode = 401;
				}
			}
		}
		$json['code']	= $this->statusCode;
		return $json;
	}
	public function actionSupplierinfo($supplier_id){
		$supplier_info 				= User::findOne($supplier_id);
		if(!empty($supplier_info)){
            $supplier_info->updateCounters(['views' => 1]);

			$json['success']['details']			= $supplier_info->userInfo;
			//unset($json['success']['details']['supplier_reviews']);


			//$json['success']['fav_menus']		= $supplier_info->userFavMenus;
			//$json['success']['fav_suppliers']	= $supplier_info->userFavSuppliers;
			$json['success']['menu_schedules']	= $supplier_info->userMenuSchedules;
			$json['success']['reviews']	= $supplier_info->supplierReviews;
            unset($json['success']['reviews']['user_info']['supplier_reviews']);
		}
		else{
			$json['error']['msg']	= 'No supplier found.';
			$this->statusCode = 401;
		}
		$json['code']	= $this->statusCode;
		return $json;
	}
	
	public function actionOrders(){
		$json 		= [];
		$page_size	= 30;
		$user_id	= Yii::$app->user->identity->id;
		$p_query		= Order::find()->where(['and',['user_id'=>$user_id],['NOT in','order_status_id',[5,2]]]);
		$c_query		= Order::find()->where(['user_id'=>$user_id,'order_status_id'=>5]);
		
		$p_dataProvider	=  new ActiveDataProvider([
            'query' => $p_query,
			'pagination' => [
				'defaultPageSize' => $page_size,
			],
			'sort' => [
				'defaultOrder' => [
					'order_id' => SORT_DESC,
				]
			],
        ]);
		$c_dataProvider	=  new ActiveDataProvider([
            'query' => $c_query,
			'pagination' => [
				'defaultPageSize' => $page_size,
			],
			'sort' => [
				'defaultOrder' => [
					'order_id' => SORT_DESC,
				]
			],
        ]);
		$p_count			= $p_dataProvider->getTotalCount();
		$c_count			= $c_dataProvider->getTotalCount();
		$pending_orders		= [];
		$completed_orders	= [];
		if($p_count>0){
			$pending_orders_	= $p_dataProvider->getModels();
			foreach($pending_orders_ as $p_orders){
				$pending_orders[]	= $p_orders->orderInfo;
			}
		}
		if($c_count>0){
			$completed_orders_	= $c_dataProvider->getModels();
			foreach($completed_orders_ as $c_orders){
				$completed_orders[]	= $c_orders->orderInfo;
			}
		}
		$json['pending_orders']		= $pending_orders;
		$json['completed_orders']	= $completed_orders;
		$json['code']				= $this->statusCode;
		return $json;
	}
	
	public function actionOrder($order_id){
		$user_id			= Yii::$app->user->identity->id;
		$result				= Order::findOne(['order_id'=>$order_id,'user_id'=>$user_id]);
		
		if(empty($result)){
			$json['error']['msg']	= 'No order found.';
			$this->statusCode = 401;
		}else
			$json['order_info'] = $result->orderInfo;
		
		$json['code']		= $this->statusCode;
		return $json;
		
	}
	public function actionRepeatOrder(){
        $library			= new Library();
        $user_info          = Yii::$app->user->identity;
        $user_id			= $user_info->id;
        $post_data		    = Yii::$app->request->post();
        $order_id           = isset($post_data['order_id']) ? $post_data['order_id'] : 0;
        $result             = Order::findOne(['user_id'=>$user_id,'order_id'=>$order_id]);
        if(!empty($result)){
            $user_info->cart = null;
            $user_info->save();
            $order_products = $result->orderProducts;
            $cart_products  = [];
            foreach($order_products as $order_product){
                $cart_products[$result->supplier_id][$order_product->menu_id] = ['menu_id'=>(int)$order_product->menu_id,'quantity'=>(int)$order_product->quantity];
            }
            $cart_val = $library->json_encodeArray($cart_products);
            $user_info->cart = $cart_val;
            $user_info->save();
            $json['is_allow'] 		= 1;
            $json['success']		= 'Your cart has been updated successfully.';
        }else{
            $json['error']['msg']	= 'Something went wrong!! Please try again.';
            $this->statusCode = 401;
        }

        $json['code']		= $this->statusCode;
        return $json;
    }
    public function actionSupplierRating($supplier_id){
        $page_size	= 30;
        $user_info = User::findOne($supplier_id);
        if(!empty($user_info)){
            $json['taste_rating'] = $user_info->avgTaste ;
            $json['presentation_rating'] = $user_info->avgPresentation;
            $json['packing_rating']      = $user_info->avgPacking;
            $json['overall_rating']      = $user_info->avgOverall;
            $reviews = [];
            $query			= OrderRatings::find()->where(['and',['!=','review',''],['is_active'=>1],['supplier_id'=>$supplier_id]]);
            $dataProvider	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => $page_size,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_DESC,
                    ]
                ],
            ]);
            $results			= $dataProvider->getModels();
            $count				= $dataProvider->getTotalCount();
            $total_pages		= ceil($count/$page_size);
            $json['total_pages']    = $total_pages;
            foreach($results as $k=>$result){
                $reviews[$k]			= $result->ratingInfo;
            }
            $json['reviews']    = $reviews;

        }else{
            $json['error']['msg']	= 'No supplier found.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    public function actionUpdatelocation(){
        $user_info          = Yii::$app->user->identity;
        $post_data			= Yii::$app->request->post();
        if(isset($post_data['latitude']) && isset($post_data['longitude'])){
            $user_info->latitude = $post_data['latitude'];
            $user_info->longitude = $post_data['longitude'];
            if($user_info->save()){
                $json['success']		= 'Your account has been updated successfully.';
            }
        }else{
            $json['error']['msg']	= 'Please enter latitude & longitude.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionSwitch(){
        $user_id = Yii::$app->user->identity->id;
        $user_info = User::findOne($user_id);
        $post_data = Yii::$app->request->post();
        $sale = $post_data['sale'] ?? null;
        if($sale!=null){
            if($user_info->is_sales_person=='yes'){
                $json['data'] = $user_info->salesDetail;
            }else{
                $user_info->is_sales_person = 'yes';
                $user_info->save();

                $u_data = UserData::findOne(['user_id'=>$user_id]);
                if(!empty($u_data)) $user_info->switchToSales($u_data,$user_id);
                else $user_info->switchToSales($u_data,$user_id);
                $json['data'] = $user_info->salesDetail;
            }
        }else{
            $json['error']['msg']	= 'Please enter information.';
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    public function actionChangeEmail(){
        $json = [];
        $user_model =  User::findOne(Yii::$app->user->identity->id);

        $post_data['User'] = Yii::$app->request->post();
        if(isset($post_data['User']['type'])){
            if($post_data['User']['type']=='email'){
                $user_model->scenario = 'change_email';
                if ($user_model->load($post_data) && $user_model->validate()) {
                    $user_model->email = $user_model->email;
                    $user_model->status = 9;
                    $user_model->email_verify = 0;
                    $user_model->generateEmailVerificationToken();
                    if($user_model->save()){
                        $user_model->sendEmailUpdate($user_model);
                        $json['success'] = 'Please check your inbox for verification email.';
                    }
                }else{
                    $all_errors = [];
                    $errors	 = $user_model->getErrors();
                    foreach($errors as $key=>$error){
                        $all_errors[]	= $error[0];
                    }
                    $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                    $this->statusCode = 401;
                }
            }elseif($post_data['User']['type']=='phone_number'){
                $user_model->scenario = 'checknumber';
                $user_model->phone_number = '';
                if ($user_model->load($post_data) && $user_model->validate()) {
                    if($user_model->save()){
                        $json['success'] = 'Your phone number has been update successfully.';
                    }
                }else{
                    $all_errors = [];
                    $errors	 = $user_model->getErrors();
                    foreach($errors as $key=>$error){
                        $all_errors[]	= $error[0];
                    }
                    $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                    $this->statusCode = 401;
                }
            }
        }
        else{
            $json['error']['msg']	= 'Please send request type';
            $this->statusCode = 401;
        }

        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionDriverUpdate(){
        $user_info	= Yii::$app->user->identity;
        $model = Apiuser::findOne($user_info->id);
        $model->scenario = 'driverupdate';
        $post_data['Apiuser'] = Yii::$app->request->post();

        $file_data = [];
        if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic'])){
            foreach($_FILES['profile_pic'] as $key=>$pic_data){
                $file_data[$key]	= ['profile_pic'=>$pic_data];
            }
        }
        if(!empty($file_data))
            $_FILES['Apiuser'] = $file_data;

        if ($model->load($post_data) && $model->validate()) {

            $profile_pic 		= UploadedFile::getInstance($model, 'profile_pic');
            if(!empty($profile_pic)){
                $library   			 = new Library();
                $model->profile_pic  = $library->saveFile($profile_pic,'users');
                $model->image_approval = 1;
            }
            if($model->save()){
                $json['success']			= User::findOne($model->id)->userInfo;
                $json['success']['msg']		= 'Your profile has been updated successfully.';
            }

        }else{
            $all_errors	 = [];
            $errors	 = $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    public function actionChangeDocument(){
        $user_info	= Yii::$app->user->identity;
        $user_id = $user_info->id;

        $file_data = [];
        if(isset($_FILES['document']) && !empty($_FILES['document'])){
            foreach($_FILES['document'] as $key=>$pic_data){
                $file_data[$key]	= ['document'=>$pic_data];
            }
        }

        $model = New DriverDocuments();
        $model->user_id = $user_id;
        $model->status = 0;
        if(!empty($file_data))
            $_FILES['DriverDocuments'] = $file_data;
        $document  = UploadedFile::getInstance($model, 'document');
        $model->document = $document;
        $model->file_info = serialize($document);
       if(!empty($document) && $model->validate()){

           $library  = new Library();
           $document_name = $library->saveFile($document,'driver_docs');
           DriverDocuments::updateAll(['is_delete' => 1],['user_id'=>$user_id]);

           if($model->save()){

               $model->document = $document_name;
               $model->is_delete = 0;
               $model->save();
               $json['success'] = 'Your document has been submitted for verification.';
           }else{
               $all_errors	 = [];
               $errors	 = $model->getErrors();
               foreach($errors as $key=>$error){
                   $all_errors[]	= $error[0];
               }
               $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
               $this->statusCode = 401;
           }
       }else{
           $all_errors	 = [];
           $errors	 = $model->getErrors();
           foreach($errors as $key=>$error){
               $all_errors[]	= $error[0];
           }
           $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
           $this->statusCode = 401;
       }
        $json['code']		= $this->statusCode;
        return $json;
    }

    public function actionCancelOrder(){
        $user_info	= Yii::$app->user->identity;
        $user_id = $user_info->id;
        $post_data['OrderCancel'] = Yii::$app->request->post();

        $model = new OrderCancel();
        $model->cancelled_by = $user_id;
        $model->by_supplier = 'no';
        $model->by_user = 'yes';
        if ($model->load($post_data) && $model->validate()) {
            $order_info = Order::find()->where(['AND',['NOT IN','order_status_id',[2,3,7,5,10]],['user_id' => $user_id],['order_id' => $model->order_id]])->one();
            if(!empty($order_info)){
                $order_info->order_status_id = 2;
                if($order_info->save()) {
                    $model->save();
                    $order_history = new OrderHistory();
                    $order_history->order_id = $model->order_id;
                    $order_history->order_status_id = 2;
                    $order_history->notify = 1;
                    $order_history->comment = 'Your order has been cancelled.';
                    $order_history->save();
                    $json['success'] = 'Your order has been cancelled.';


                }
            }else{
                $json['error']['msg']	= 'You don\'t have permission to perform this action.' ;
                $this->statusCode = 401;
            }
        }else{
            $all_errors	 = [];
            $errors	 = $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }

    // public function actionMywallet(){
    //     $this->page_size =100;
    //     $user_info	= Yii::$app->user->identity;
    //     $model = new Wallet();
    //     $query = $model->find()->where(['user_id' => $user_info->id]);
    //     $dataProvider	=  new ActiveDataProvider([
    //         'query' => $query,
    //         'pagination' => [
    //             'defaultPageSize' => $this->page_size,
    //         ],
    //         'sort' => [
    //             'defaultOrder' => [
    //                 'wallet_id' => SORT_DESC,
    //             ]
    //         ],
    //     ]);

    //     $results = $dataProvider->getModels();
    //   // start check user validation for trationation data ( 11 dec 2019  )  by  catotech  

        

    //       $amountsql ="SELECT SUM(amount) as pendingforapproval FROM tbl_wallet WHERE user_id=".$user_info->id;
    //       $totalamt = Yii::$app->db->createCommand($amountsql)->queryAll();
    //       $json['pendingforapproval']    = $totalamt;
    //       $json['cashout']    = 0; 
 
       
    //     $uservalidation = User::findOne()->where(['id' => $user_info->id]);
    //     if(!empty($uservalidation)){
    //     	    if($uservalidation['national_pic_approval']==0){
          
    //         $this->statusCode = 401;
    //         $json['error']['msg']	 = 'Please Upload National Id';
    //     	   return $json;

    //     	}
    //     		if($uservalidation['national_pic_approval']==1){
           
    //          $this->statusCode = 401;
    //         $json['error']['msg']	 = 'Your National Id is Pending For approval';
    //           return $json;
    //     	}

    //     		if($uservalidation['national_pic_approval']==3){
          
    //         $this->statusCode = 401;
    //          $json['error']['msg'] = 'Your National Id is Rejected';
    //     	  return $json;
    //     	}

    //     }

    //   // end check user validation for trationation


    //     if(!empty($results)){
    //         foreach($results as $k=>$result)
    //             $json['success'][$k]	= ['wallet_id' => $result->wallet_id,'amount' => $result->amount,'currency' => $result->currency,'message' => $result->message,'time' => Yii::$app->formatter->format($result->created_at, 'relativeTime')];
    //         $count				= $dataProvider->getTotalCount();
    //         $total_pages		= ceil($count/$this->page_size);
    //         $json['total_pages']    = $total_pages;
    //         $json['total_balance']    = $user_info->balance;
    //         $json['currency']    = config_default_currency;
    //     }else{
    //         $this->statusCode = 401;
    //         $json['error']['msg']	 = 'No data found.';
    //     }
    //     $json['code'] = $this->statusCode;
    //     $json['bank_details'] = $user_info->bankDetails;
    //     return $json;

    // }

     
      public function actionMywallet(){
               
        $this->page_size =100;
        $user_info	= Yii::$app->user->identity;
        // $user_info->id = 84 ;

         
          $uservalidation = User::find()->where(['id' => $user_info->id])->one(); 
    
    
        if(!empty($uservalidation)){
        	    if($uservalidation['national_pic_approval']==0){
          
            $json['code'] = 401 ;
            $json['error']['msg']	 = 'Please Upload National Id';
       
        	}
        		if($uservalidation['national_pic_approval']==1){
           
            // $this->statusCode = 401;
            $json['code'] = 401 ;
            $json['error']['msg']	 = 'Your National Id is Pending For approval';
          
        	}
        		if($uservalidation['national_pic_approval']==3){
          
            // $this->statusCode = 401;  
             $json['code'] = 401 ;
             $json['error']['msg'] = 'Your National Id is Rejected';
        
        	}
 
  
        	if($uservalidation['national_pic_approval']==2){
             

        $model = new Wallet();
        $query = $model->find()->where(['user_id' => $user_info->id ]);
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'wallet_id' => SORT_DESC,
                ]
            ],
        ]);
        $results = $dataProvider->getModels();
      // start check user validation for trationation data ( 11 dec 2019  )  by  catotech  
          $amountsql ="SELECT SUM(amount) as pendingforapproval FROM tbl_wallet WHERE user_id=".$user_info->id;
          $totalamt = Yii::$app->db->createCommand($amountsql)->queryAll();
             // print_r($totalamt);die;
          $json['pendingforapproval']    = $totalamt[0]['pendingforapproval'];
          $json['cashout']    = 0; 
  
      
  
       $totalbalance = 0;
      // end check user validation for trationation
        if(!empty($results)){
            foreach($results as $k=>$result)
            {

                $json['success'][$k]	= ['wallet_id' => $result->wallet_id,'amount' => $result->amount,'currency' => $result->currency,'message' => $result->message,'time' => Yii::$app->formatter->format($result->created_at, 'relativeTime')];
            $totalbalance = $totalbalance + $result->amount;

            }

    
    // print_r($totalbalance);die;    
            $count				= $dataProvider->getTotalCount();
            $total_pages		= ceil($count/$this->page_size);

            $json['total_pages']    = $total_pages;
          
            $json['total_balance']    = $totalbalance; 

            $json['currency']    = config_default_currency;
        }else{     
            $json['code'] = $this->statusCode;
            $json['error']['msg']	 = 'No data found.';
        }   
         
        $json['code'] = $this->statusCode;

        $tbluserbankaccount =UserBankAccount::find()->where(['user_id'=>$user_info->id])->one();

        $json['bank_details'] = ['first_name'=>$tbluserbankaccount['first_name'],'bank_name'=>$tbluserbankaccount['bank_name'],'account_number'=>$tbluserbankaccount['account_number'],'iban'=>$tbluserbankaccount['iban']];

        	}
        }


    


        return $json;          

    }

    public function actionAddBank(){
        $user_info = Yii::$app->user->identity;
        $model = new UserBankAccount();
        $model->user_id = $user_info->id;
        $post_data['UserBankAccount'] = Yii::$app->request->post();
        if ($model->load($post_data) && $model->validate()) {
            if($model->save()){
                $json['success'] = 'Your data has been saved.';
            }
        }else{
            $all_errors				= [];
            $errors					= $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode 			= 401;
        }
        $json['code']					= $this->statusCode;
        return $json;
    }
    public function actionUpdateBank(){
        $user_info = Yii::$app->user->identity;
        $model =  UserBankAccount::find()->where(['user_id' => $user_info->id])->one();
        if(!empty($model)){
            $post_data['UserBankAccount'] = Yii::$app->request->post();
            if ($model->load($post_data) && $model->validate()) {
                if($model->save()){
                    $json['success'] = 'Your data has been saved.';
                }
            }else{
                $all_errors				= [];
                $errors					= $model->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode 			= 401;
            }
            $json['code']					= $this->statusCode;
            return $json;
        }

    }
    public function actionViewBank(){
        $user_info = Yii::$app->user->identity;
        $model =  UserBankAccount::find()->where(['user_id' => $user_info->id])->one();
        if(!empty($model)){
            $json['data'] = ['first_name' => $model->first_name,'last_name' => $model->last_name,'email_address' => $model->email_address,'bank_name' => $model->bank_name,'account_number' => $model->account_number,'iban' => $model->iban];
        }else{
            $json['error']['msg']	= 'No data found.' ;
            $this->statusCode 	= 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }

    public function actionAddMoney(){
        $user_info = Yii::$app->user->identity;
        $model = new Wallet();
        $model->user_id = $user_info->id;
        $model->currency = config_default_currency;
        $model->type = 'added';
        $model->user_role = 1;
        $model->message = 'Added to wallet';
        $model->ip_address = Yii::$app->getRequest()->getUserIP();
        $post_data['Wallet'] = Yii::$app->request->post();
        $model->scenario = 'add_money';
        if ($model->load($post_data) ) {
            if($model->validate() && $model->save()){
                $token = '';
                $braintree_model = new Braintree();
                $creditcard = CreditCards::find()->where(['id' => $model->card_id,'user_id' => $user_info->id ])->one();
                if(!empty($creditcard))
                    $token = $creditcard->token;

                $model->amount = number_format($model->amount, 2, '.', '');
                $response = $braintree_model->submitForSettlement($model->amount,$token,'Wallet '.$model->wallet_id);
                if($response['error']==1){
                    Wallet::findOne($model->wallet_id)->delete();
                    $json['error']['msg']	= $response['message'];
                    $this->statusCode = 401;
                }else{
                    $model->orderTransaction($response['response']);
                    $json['success'] = 'Money has been added to your wallet.';
                }
            }else{
                $all_errors = [];
                $errors	= $model->getErrors();
                foreach($errors as $key=>$error) {
                    $all_errors[] = $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }
        }

        $json['code'] = $this->statusCode;
        return $json;
    }

    public function actionInvoice($order_id){
        yii::$app->language = 'en-US';
        $pdf = Yii::$app->pdf;
        $user_id = yii::$app->user->id;
        $model = Order::find()->where(['AND',['order_id' => $order_id]])->one();

        $htmlContent = $this->renderPartial('@frontend/views/profile/_invoice',['model' => $model]);
        $pdf->content = $htmlContent;

        return $pdf->render();
    }

    public function actionSwitchToDriver(){
        $user_id = Yii::$app->user->identity->id;
        $user_info = User::findOne($user_id);
        if($user_info->is_supplier=='yes')
            $json['error']['msg']	= 'You can\'t switch to driver. Please contact with administrator.';
        else{
            $user_info->is_driver = 'yes';
            $user_info->save();
            $json['data'] = $user_info->driverinfo;
        }

        $json['code'] = $this->statusCode;
        return $json;

    }
}
