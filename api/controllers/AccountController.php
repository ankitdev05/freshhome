<?php
namespace api\controllers;
use Yii;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use	yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

use common\models\Braintree;
use common\models\CreditCards;

class AccountController extends ActiveController{
    public $statusCode = 200;
    public $page_size = 30;
    public $modelClass = 'common\models';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']	= [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['error','index'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        return $behaviors;
    }
    /**
     * {@inheritdoc}
     */
    public function actions(){
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }
    /**
     * {@inheritdoc}
     */
    protected function verbs(){
        return [
            'index'		=> ['GET'],
            'addcard'	=> ['POST'],
            'mycards'	=> ['GET'],
            'deletecard'	=> ['POST'],
            'updatecard'	=> ['POST'],
        ];
    }
    public function actionIndex(){
        $json['message'] = 'Welcome to '.config_name;
        return $json;
    }

    /*
        Create user's new account(In braintree) if not available and save their credit/debit card
        @Required params customer data{customer data email,first name,last name}
        @Required params credit card info{card no., CCV, Name, Exp. Date}
        @Response array
    */
    public function actionAddcard(){
        $user_info       = Yii::$app->user->identity;
        $user_id         = $user_info->id;
        $model 			 = new CreditCards();
        $model->scenario = 'step_1';
        $post_data['CreditCards'] 	= Yii::$app->request->post();
        if ($model->load($post_data) && $model->validate()) {
            $btree_model	= new Braintree();
            $btree_user		= $btree_model->createAccount($user_info); // check or create user braintree vault
            if(empty($btree_user)){
                $json['error']['msg']	= 'Invalid request. Please try again';
                $this->statusCode = 401;
            }else{
                $cc_data					= [];
                $cc_data['cardholderName']	= $model->card_name;
                $cc_data['number']			= $model->card_number;
                $cc_data['cvv']				= $model->cvv;
                $cc_data['expirationDate']	= $model->exp_month.'/'.$model->exp_year;
                $cc_add						= $btree_model->addCreditCard($btree_user->id,$cc_data);// Add new credit card.
                if(isset($cc_add['error'])){
                    $json['error']['msg']	= $cc_add['error'];
                    $this->statusCode = 401;
                }else{

                    $model					= new CreditCards();
                    $model->scenario		= 'step_2';
                    $model->name			= $cc_add['creditCard']['cardholderName'];
                    $model->user_id			= $user_id;
                    $model->token			= $cc_add['creditCard']['token'];
                    $model->identifier		= $cc_add['creditCard']['uniqueNumberIdentifier'];
                    $model->card_type		= $cc_add['creditCard']['cardType'];
                    $model->expired			= (int)$cc_add['creditCard']['expired'];
                    $model->image_url		= $cc_add['creditCard']['imageUrl'];
                    $model->exp_date		= $cc_add['creditCard']['expirationDate'];
                    $model->masked_num		= $cc_add['creditCard']['maskedNumber'];
                    $model->cc_verification	= $cc_add['creditCard']['verifications'][0]['status'];
                    $model->type			= Yii::$app->params['btree_environment'];
                    if($model->save()){
                        $json['success']['msg']	= 'Your card has been added successfully.';
                    }else{
                        $all_errors				= [];
                        $errors					= $model->getErrors();
                        foreach($errors as $key=>$error){
                            $all_errors[]	= $error[0];
                        }
                        $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                        $this->statusCode = 401;
                    }
                }
            }
        }else{
            $all_errors				= [];
            $errors					= $model->getErrors();
            foreach($errors as $key=>$error){
                $all_errors[]	= $error[0];
            }
            $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }

    /*
        Update credit/debit card
        @Required params credit card info{card no., CCV, Name, Exp. Date}
        @Response array
    */
    public function actionUpdatecard(){
        $post_data['CreditCards'] 	= Yii::$app->request->post();
        $user_info       = Yii::$app->user->identity;
        $user_id         = $user_info->id;
        $card_id         = isset($post_data['CreditCards']['card_id']) ? $post_data['CreditCards']['card_id'] : 0;
        $model 			 = new CreditCards();
        $model->scenario = 'step_1';
        $result = $model->find()->where(['user_id'=>$user_id,'id'=>$card_id])->one();
        if(!empty($result)){
            if ($model->load($post_data) && $model->validate()) {
                $token = $result->token;
                $cc_data = [];
                $cc_data['cardholderName'] = $model->card_name;
                $cc_data['number'] = $model->card_number;
                $cc_data['cvv'] = $model->cvv;
                $cc_data['expirationDate'] = $model->exp_month . '/' . $model->exp_year;
                $btree_model = new Braintree();
                $cc_add = $btree_model->updateCreditCard($token, $cc_data);// Update credit card on braintree.
                if(isset($cc_add['error'])){
                    $json['error']['msg']	= $cc_add['error'];
                    $this->statusCode = 401;
                }else{
                    $model					= $result;
                    $model->scenario		= 'step_2';
                    $model->name			= $cc_add['creditCard']['cardholderName'];
                    $model->user_id			= $user_id;
                    $model->token			= $cc_add['creditCard']['token'];
                    $model->identifier		= $cc_add['creditCard']['uniqueNumberIdentifier'];
                    $model->card_type		= $cc_add['creditCard']['cardType'];
                    $model->expired			= (int)$cc_add['creditCard']['expired'];
                    $model->image_url		= $cc_add['creditCard']['imageUrl'];
                    $model->exp_date		= $cc_add['creditCard']['expirationDate'];
                    $model->masked_num		= $cc_add['creditCard']['maskedNumber'];
                    $model->cc_verification	= $cc_add['creditCard']['verifications'][0]['status'];
                    $model->type			= Yii::$app->params['btree_environment'];
                    if($model->save()){
                        $json['success']['msg']	= 'Your card has been updated successfully.';
                    }else{
                        $all_errors				= [];
                        $errors					= $model->getErrors();
                        foreach($errors as $key=>$error){
                            $all_errors[]	= $error[0];
                        }
                        $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                        $this->statusCode = 401;
                    }
                }
            }

        }else{
            $json['error']['msg']	= 'You\'re not authorized to perform this action.' ;
            $this->statusCode = 401;
        }
        $json['code']		= $this->statusCode;
        return $json;
    }
    /*
           View user's all saved cards
           @Required params $user_token(auth bearer token)
           @Response array
     */
    public function actionMycards(){
        $user_info       = Yii::$app->user->identity;
        $user_id         = $user_info->id;
        $model 			 = new CreditCards();
        $query  = $model->find()->where(['user_id'=>$user_id]);
        $dataProvider	=  new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->page_size,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        $count	= $dataProvider->getTotalCount();
        $total_pages	= ceil($count/$this->page_size);
        if($count>0){
            $results = $dataProvider->getModels();
            foreach($results as $result)
                $data[] = ['type'=>Yii::$app->params['btree_environment'],'card_id'=>$result->id,'card_type'=>$result->card_type,'name'=>$result->name,'expired'=>$result->expired,'card_image'=>$result->image_url,'exp_date'=>$result->exp_date,'masked_num'=>$result->masked_num];

            $json['data'] = $data;
            $json['total_pages'] = $total_pages;

        }else{
            $json['error']['msg']	= 'No card found.' ;
            $this->statusCode = 401;
        }
        $json['code']				= $this->statusCode;
        return $json;
    }
    /*
           Delete user's card from braintree's account and in our database
           @Required params $user_token(auth bearer token),$card_id
           @Response array
     */
    public function actionDeletecard(){
        $post_data 	= Yii::$app->request->post();
        $user_info       = Yii::$app->user->identity;
        $user_id         = $user_info->id;
        $card_id        = isset($post_data['card_id']) ? $post_data['card_id'] : 0;
        $model 			 = new CreditCards();
        $result          = $model->find()->where(['user_id'=>$user_id,'id'=>$card_id])->one();
        if(!empty($result)){
            $btree_model	= new Braintree();
            $response 		= $btree_model->deleteCard($result->token);
            if(isset($response->success)){
                $result->delete();
                $json['success']['msg']	= 'Your card has been removed.';
            }
        }else{
            $json['error']['msg']	= 'You\'re not authorized to perform this action.' ;
            $this->statusCode = 401;
        }
        $json['code']				= $this->statusCode;
        return $json;
    }
}