<?php
namespace api\controllers;

use common\models\User;
use Yii;
use common\models\Category;
use common\models\Brand;
use common\models\Menu;
use yii\data\ActiveDataProvider;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\Response;

class SearchController extends ActiveController{
    public $statusCode = 200;
    public $language_id = 1;
    public $page_size = 30;
    public $modelClass = 'common\models';
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']	= [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['error','index'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        return $behaviors;
    }
    public function actions(){
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }
    protected function verbs(){
        return [
            'by-brands'	=> ['GET'],
            'by-sub-category' => ['GET'],
            'auto-complete' => ['GET'],
        ];
    }
    public function actionByBrands($brand_id,$language_id = 1){
        $language_id = $language_id;
        $page_id = isset($_GET['page']) ? $_GET['page'] : 1 ;
        $sort_order = ['id' => SORT_DESC];
        $json = [];
        $user_info = Yii::$app->user->identity;
        $supplier_id = $user_info->id;
        $result = Brand::find()->joinWith('brandDescription as bd')->where(['tbl_brand.brand_id' => $brand_id,'bd.language_id' => $language_id,'tbl_brand.status' => 1])->one();
        if(!empty($result)){
            $query = Menu::find()->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0,'tbl_menu.brand_id'=>$brand_id],['>','subscription_end',time()]]);
            $query->andWhere(['!=','supplier_id',$supplier_id]);
            $dataProvider	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => $sort_order
                ],
            ]);
            $results = $dataProvider->getModels();
            $count = $dataProvider->getTotalCount();
            $total_pages = ceil($count/$this->page_size);
            if(!empty($results)){
                $dish_data = [];
                foreach($results as $k=>$result){
                    if($result->product_type==1)
                        $dish_data[$k] = $result->dishInfo;
                    else
                        $dish_data[$k] = $result->productInfo;
                }
                $json['success']['data'] = $dish_data;
                if($total_pages > $page_id){
                    $json['success']['pageAction']['next_page_url'] = Url::to(Yii::$app->request->baseUrl.'/search/by-brands?page='.($page_id+1),true);
                }else
                    $json['success']['pageAction']['next_page_url'] = '';
                $json['success']['pageAction']['current_page'] = Url::to(Yii::$app->request->baseUrl.'/search/by-brands?page='.($page_id),true);
                if($page_id!=1)
                    $json['success']['pageAction']['prev_page_url'] = Url::to(Yii::$app->request->baseUrl.'/search/by-brands?page='.($page_id-1),true);
                else
                    $json['success']['pageAction']['prev_page_url'] = '';
            }else{
                $this->statusCode = 401;
                $json['error']['msg']	 = 'Menu list is empty.';
            }
        }else{
            $json['error']['msg']	= 'Brand not found.';
            $this->statusCode	= 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }

    public function actionBySubCategory($category_id,$language_id = 1){
        $page_id = isset($_GET['page']) ? $_GET['page'] : 1 ;
        $sort_order = ['id' => SORT_DESC];
        $json = [];
        $user_info = Yii::$app->user->identity;
        $supplier_id = $user_info->id;
        $category =  Category::find();
        $query = $category->select('*')->joinWith('categoryDescription as cd')->where(['active' => 1,'cd.language_id' => language_id]);
        $query->andWhere(['tbl_category.category_id' => $category_id]);
        $result = $query->one();
        if($result){
            $query = Menu::find();
            $query->joinWith(['menuCategories','supplierInfo']);
            $query->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],['>','subscription_end',time()]]);
            $query->andWhere(['!=','supplier_id',$supplier_id]);
            $query->andWhere(['in','tbl_menu_categories.category_id',$category_id]);

            $dataProvider	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => $sort_order
                ],
            ]);
            $results = $dataProvider->getModels();
            $count = $dataProvider->getTotalCount();
            $total_pages = ceil($count/$this->page_size);
            if(!empty($results)){
                $dish_data = [];
                foreach($results as $k=>$result){
                    if($result->product_type==1)
                        $dish_data[$k] = $result->dishInfo;
                    else
                        $dish_data[$k] = $result->productInfo;
                }
                $json['success']['data'] = $dish_data;
                if($total_pages > $page_id){
                    $json['success']['pageAction']['next_page_url'] = Url::to(Yii::$app->request->baseUrl.'/search/by-brands?page='.($page_id+1),true);
                }else
                    $json['success']['pageAction']['next_page_url'] = '';
                $json['success']['pageAction']['current_page'] = Url::to(Yii::$app->request->baseUrl.'/search/by-brands?page='.($page_id),true);
                if($page_id!=1)
                    $json['success']['pageAction']['prev_page_url'] = Url::to(Yii::$app->request->baseUrl.'/search/by-brands?page='.($page_id-1),true);
                else
                    $json['success']['pageAction']['prev_page_url'] = '';
            }else{
                $this->statusCode = 401;
                $json['error']['msg']	 = 'Menu list is empty.';
            }
        }else{
            $json['error']['msg']	= 'Category Not found.';
            $this->statusCode	= 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }

    public function actionAutoComplete($string){
        $q = trim(Html::encode($string));
        $json = [];
        $results = [];
        $all_brands = Brand::find()->joinWith('brandDescription as bd')->where(['and',['bd.language_id' => language_id],['like','brand_name',$q],['status' => 1]])->limit(10)->all();

        $all_categories = Category::find()->select('*,cd.category_name as cname')->joinWith('categoryDescription as cd')->where(['and',['active' => 1,'cd.language_id' => language_id],['like','cd.category_name',$q],['NOT IN','cat_id',1]])->limit(10)->all();

        $all_suppliers = Menu::find()->select('tbl_menu.*')->joinWith('supplierInfo')->where(['and',['tbl_menu.status'=>'active','tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0]])->andWhere(['like','tbl_user.name',$q])->groupBy('tbl_user.id')->limit(10)->all();

        $all_menus = Menu::find()->where(['and',['tbl_menu.status'=>'active','tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0]])->andWhere(['like','tbl_menu.dish_name',$q])->limit(10)->all();

        if(!empty($all_brands)){
            foreach ($all_brands as $all_brand)
                $results[] = ['label' => $all_brand->brandDescription->brand_name,'value' => $all_brand->brandDescription->brand_name,'type' => 'brands'];
        }

        if(!empty($all_categories)){
            foreach ($all_categories as $category)
                $results[] = ['label' => $category->categoryDescription->category_name,'value' => $category->categoryDescription->category_name,'type' => 'category'];
        }

        if(!empty($all_suppliers)){
            foreach ($all_suppliers as $supplier)
                $results[] = ['label' => $supplier->supplierInfo->name,'value' =>$supplier->supplierInfo->name,'type' => 'supplier'];
        }

        if(!empty($all_menus)){
            foreach ($all_menus as $menu)
                $results[] = ['label' => $menu->dish_name,'value' =>$menu->dish_name,'type' => 'product'];
        }

        $json['results'] = $results;
        if(empty($results)){
            $json['error']['msg'] = 'No result found.';
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }

    public function actionSearch($label){
        $page_id = isset($_GET['page']) ? $_GET['page'] : 1 ;
        $title = trim(Html::encode($label));
        $sort_order = ['id' => SORT_DESC];
        if(!empty($title)){
            $booking_ids[] = 0;
            $query = Menu::find()->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],['>','subscription_end',time()]]);

            $all_brands = Brand::find()->select('tbl_brand.brand_id')->joinWith('brandDescription as bd')->where(['and',['bd.language_id' => language_id],['like','brand_name',$title],['status' => 1]]);
            $brand_menus = Menu::find()->select('tbl_menu.*')->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],['>','subscription_end',time()],['IN','brand_id',$all_brands]])->asArray()->all();
            if(!empty($brand_menus)){
                foreach ($brand_menus as $brand_menu)
                    $booking_ids[] = $brand_menu['id'];
            }
            $all_categories = Category::find()->select(' tbl_category.category_id')->joinWith('categoryDescription as cd')->where(['and',['active' => 1,'cd.language_id' => language_id],['like','cd.category_name',$title],['NOT IN','cat_id',1]]);
            $category_menus = Menu::find()->select('tbl_menu.*')->joinWith(['supplierInfo','menuCategories as mc'])->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],['>','subscription_end',time()],['IN','mc.category_id',$all_categories]])->asArray()->all();
            if(!empty($category_menus)){
                foreach ($category_menus as $category_menu)
                    $booking_ids[] = $category_menu['id'];
            }
            $all_suppliers = User::find()->select('id')->where(['and',['status' => 10],['>','subscription_end',time()],['like','name',$title],['is_supplier' => 'yes']]);
            $supplier_menus = Menu::find()->select('tbl_menu.*')->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],['>','subscription_end',time()],['IN','supplier_id',$all_suppliers]])->asArray()->all();
            if(!empty($supplier_menus)){
                foreach ($supplier_menus as $supplier_menu)
                    $booking_ids[] = $supplier_menu['id'];
            }

            $all_menus = Menu::find()->select('tbl_menu.*')->joinWith('supplierInfo')->where(['and',['tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0],['>','subscription_end',time()],['like','dish_name',$title]])->asArray()->all();
            if(!empty($all_menus)){
                foreach ($all_menus as $all_menu)
                    $booking_ids[] = $all_menu['id'];
            }
            if(!empty($booking_ids)){
                $query->andWhere(['IN','tbl_menu.id',$booking_ids]);
            }
            if (!Yii::$app->user->isGuest)
                $query->andWhere(['!=','supplier_id',Yii::$app->user->identity->id]);
            $dataProvider	=  new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => $this->page_size,
                ],
                'sort' => [
                    'defaultOrder' => $sort_order
                ],
            ]);
            $results = $dataProvider->getModels();
            $count = $dataProvider->getTotalCount();
            $total_pages = ceil($count/$this->page_size);
            if(!empty($results)){
                $dish_data = [];
                foreach($results as $k=>$result){
                    if($result->product_type==1)
                        $dish_data[$k] = $result->dishInfo;
                    else
                        $dish_data[$k] = $result->productInfo;
                }
                $json['success']['data'] = $dish_data;
                if($total_pages > $page_id){
                    $json['success']['pageAction']['next_page_url'] = Url::to(Yii::$app->request->baseUrl.'/search/by-brands?page='.($page_id+1),true);
                }else
                    $json['success']['pageAction']['next_page_url'] = '';
                $json['success']['pageAction']['current_page'] = Url::to(Yii::$app->request->baseUrl.'/search/by-brands?page='.($page_id),true);
                if($page_id!=1)
                    $json['success']['pageAction']['prev_page_url'] = Url::to(Yii::$app->request->baseUrl.'/search/by-brands?page='.($page_id-1),true);
                else
                    $json['success']['pageAction']['prev_page_url'] = '';
            }else{
                $this->statusCode = 401;
                $json['error']['msg']	 = 'Menu list is empty.';
            }
        }else{
            $json['error']['msg'] = 'Please enter some data.';
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
}