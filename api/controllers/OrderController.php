<?php
namespace api\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\rest\ActiveController;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use	yii\filters\ContentNegotiator;
use common\models\User;
use common\models\Order;
use common\models\Library;
use common\models\OrderHistory;
use common\models\OrderCancel;

class OrderController extends ActiveController
{
    public $statusCode = 200;
    public $modelClass = 'common\models';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']	= [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['error','index'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        return $behaviors;
    }
    public function actions(){
        $actions			= parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }
    protected function verbs(){
        return [
            'index'	=> ['GET'],
            'accept'=> ['POST'],
            'reject'=> ['POST'],
            'process-order'=> ['POST'],
        ];
    }
    public function actionIndex()
    {
        $json['message'] = 'Welcome to order page.';
        return $json;
    }
    public function actionAccept(){
        $supplier_id    = Yii::$app->user->identity->id;
        $post_data      = Yii::$app->request->post();
        $order_id       = isset($post_data['order_id']) ? $post_data['order_id'] : 0;
        $order_info     = Order::find()->where(['supplier_id'=>$supplier_id,'order_id'=>$order_id])->orWhere(['order_status_id'=>6,'order_status_id'=>8])->one();
        if(!empty($order_info)){
            $order_info->order_status_id = 11;
            if($order_info->save()){
                $json['success']['msg']	= 'Your order has been accepted successfully.';

                $user_detail        = User::findOne($supplier_id);
                $user_detail        = $user_detail->userInfo;

                $library = new Library();
                $order_info1['user_id']        = $order_info->user_id;
                $order_info1['role']            = 1;
                $order_info1['from_user']      = $order_info->supplier_id;
                $order_info1['type']           = 'order_accept';
                $order_info1['config_name']    = $user_detail['name'].' has accepted your order.';
                $order_info1['config_value']['order_id']= $order_info->order_id;
                $order_info1['config_value']['message']= $user_detail['name'].' has accepted your order.';
                $library->saveNotification($order_info1);

                $order_history	= new OrderHistory();
                $order_history->order_id			= $order_info->order_id;
                $order_history->order_status_id		= $order_info->order_status_id;
                $order_history->notify				= 1;
                $order_history->comment				= 'Your order has been accepted successfully.';
                $order_history->save();
            }else{
                $all_errors				= [];
                $errors					= $order_info->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
                $this->statusCode = 401;
            }


        }else{
            $json['error']['msg']	= 'Order does not exist';
            $this->statusCode		= 401;
        }
        $json['code']					= $this->statusCode;
        return $json;
    }
    public function actionReject(){
        $supplier_id    = Yii::$app->user->identity->id;
        $post_data      = Yii::$app->request->post();
        $order_id       = isset($post_data['order_id']) ? $post_data['order_id'] : 0;
        $order_info     = Order::find()->where(['supplier_id'=>$supplier_id,'order_id'=>$order_id])->one();
        $reject_type    = isset($post_data['reject_type']) ? trim($post_data['reject_type']) : null;
        $reject_message = isset($post_data['reject_message']) ? trim($post_data['reject_message']) : null;
        if(!empty($order_info)){
                $order_info->order_status_id= 8;
            if($supplier_id==$order_info->supplier_id)
                $order_info->order_status_id= 5;

            $order_info->reject_type = $reject_type;
            $order_info->reject_message = $reject_message;
            if($order_info->save()){
                if($supplier_id==$order_info->supplier_id){
                    $canModel = new OrderCancel();
                    $canModel->cancelled_by = $supplier_id;
                    $canModel->by_supplier = 'yes';
                    $canModel->by_user = 'no';
                    $canModel->reason = $reject_message;
                    $canModel->save();
                }


                $json['success']['msg']	= 'Your order has been rejected successfully.';
                $user_detail        = User::findOne($supplier_id);
                $user_detail        = $user_detail->userInfo;

                $library = new Library();
                $order_info1['user_id']        = $order_info->user_id;
                $order_info1['role']            = 1;
                $order_info1['from_user']      = $order_info->supplier_id;
                $order_info1['type']           = 'order_reject';
                $order_info1['config_name']    = $user_detail['name'].' has rejected your order.';
                $order_info1['config_value']['order_id']= $order_info->order_id;
                $order_info1['config_value']['message']= $reject_message;
                $order_info1['config_value']['reject_type']= $reject_type;
                $library->saveNotification($order_info1);

                $order_history	= new OrderHistory();
                $order_history->order_id			= $order_info->order_id;
                $order_history->order_status_id		= $order_info->order_status_id;
                $order_history->notify				= 1;
                $order_history->comment				= 'Your order has been rejected successfully.';
                $order_history->save();



            }
        }else{
            $json['error']['msg']	= 'Order does not exist';
            $this->statusCode		= 401;
        }

        $json['code']					= $this->statusCode;
        return $json;
    }
    public function actionProcessOrder(){
        $supplier_id    = Yii::$app->user->identity->id;
        $post_data      = Yii::$app->request->post();
        $order_id       = isset($post_data['order_id']) ? $post_data['order_id'] : 0;
        $order_info     = Order::find()->where(['supplier_id'=>$supplier_id,'order_id'=>$order_id])->one();
        if(!empty($order_info)){
            $order_info->order_status_id= 4;
            $order_info->save();

            $order_history	= new OrderHistory();
            $order_history->order_id			= $order_info->order_id;
            $order_history->order_status_id		= $order_info->order_status_id;
            $order_history->notify				= 1;
            $order_history->comment				= 'Your order has been processed.';
            $order_history->save();

            $library = new Library();
            $order_info1['user_id']        = $order_info->user_id;
            $order_info1['role']            = 1;
            $order_info1['from_user']      = $order_info->supplier_id;
            $order_info1['type']           = 'order';
            $order_info1['config_name']    = ' Your order has been processed.';
            $order_info1['config_value']['order_id']= $order_info->order_id;
            $order_info1['config_value']['message']= ' Your order has been processed.';
            $library->saveNotification($order_info1);

            $json['success']['msg']	= 'Your order has been processed successfully.';

        }else{
            $json['error']['msg']	= 'Order does not exist';
            $this->statusCode		= 401;
        }
        $json['code']					= $this->statusCode;
        return $json;
    }

}