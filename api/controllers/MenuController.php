<?php
namespace api\controllers;
use PHPUnit\Util\Log\JSON;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\HeaderCollection;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use common\models\HomeCategory;
use common\models\HomeCategoryDescription;
use common\models\CategoryDescription;
use common\models\Brand;
use common\models\User;
use common\models\City;
use common\models\Meal;
use common\models\Menu;
use common\models\Cuisine;
use common\models\Library;
use common\models\Category;
use common\models\MenuMeals;
use common\models\Occupation;
use common\models\SortMethod;
use common\models\MenuCuisines;
use common\models\MenuSchedules;
use common\models\MenuScheduleItems;
use common\models\MenuCategories;
use common\models\MenuRating;
use common\models\Notifications;
use common\models\Countries;
use common\models\UserAddress;
use api\models\Cart;
use yii\db\Expression;
use himiklab\thumbnail\EasyThumbnailImage;
/**
 * Site controller
 */
class MenuController extends ActiveController {
    public $statusCode = 200;
    public $modelClass = 'common\models';
    public $language_id;
    public $screen_id = 1;
    public $page_size = 30;
    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = ['class' => ContentNegotiator::className(), 'formats' => ['application/json' => Response::FORMAT_JSON, ], ];
        $behaviors['authenticator'] = ['class' => CompositeAuth::className(), 'except' => ['error', 'index', 'getcategory', 'getcuisine', 'getfilter', 'viewdish', 'searchitem', 'finditem', 'menulisting', 'schedulelisting', 'get-menu-rating', 'best-selling-products', 'online-kitchens', 'home-items', 'home-items-list', 'latest-product', 'best-selling-product', 'online-kitchens', 'order-now', 'sub-category', 'view-all-sub-category', 'supplier-by', 'view-all-supplier', 'brand-list', 'all-brand-list', 'category-details', 'product-list', 'subcategory-list', 'hot-deals', 'all-brand-list12', 'homecategory', 'homecategorydetail', 'brand-list-by-sub-category'], 'authMethods' => [HttpBasicAuth::className(), HttpBearerAuth::className(), QueryParamAuth::className(), ], ];
        $behaviors['corsFilter'] = ['class' => \yii\filters\Cors::className(), 'cors' => ['Origin' => ['*'], 'Access-Control-Request-Method' => ['POST', 'GET', 'PUT', 'OPTIONS'], 'Access-Control-Request-Headers' => ['*'], 'Access-Control-Max-Age' => 3600, 'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'], ], ];
        return $behaviors;
    }
    public function actions() {
        $this->language_id = isset($_REQUEST['language_id']) ? (int)$_REQUEST['language_id'] : '1';
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }
    protected function verbs() {
        return ['index' => ['GET'], 'getcategory' => ['GET'], 'getcuisine' => ['GET'], 'getfilter' => ['GET'], 'viewdish' => ['GET'], 'online-kitchens' => ['GET'], 'addmenu' => ['POST'], 'menulist' => ['POST'], 'menulisting' => ['GET'], 'home-items' => ['GET'], 'editdish' => ['POST'], '
            ' => ['POST'], 'national-id' => ['POST'], 'finditem' => ['POST'], 'view-all-sub-category' => ['POST'], 'all-brand-list' => ['POST'], 'homecategory' => ['POST'], 'view-all-supplier' => ['POST'], 'getmenuschedule' => ['GET'], 'get-menu-rating' => ['GET'], 'best-selling-products' => ['GET'], 'updatemenuschedule' => ['POST'], 'addmenuschedule' => ['POST'], 'national-id' => ['POST'], 'deletedish' => ['DELETE'], 'deleteschedule' => ['DELETE'], ];
    }
    public function actionIndex() {
        $json['message'] = 'Welcome to menu module';
        return $json;
    }
    public function actionError() {
        $json['error'] = 'The requested page does not exist.';
        return $json;
    }
    public function actionGetcategory($screen_id = null) {
        if (!empty($screen_id)) $this->screen_id = $screen_id;
        $model = new Category();
        $result = $model->find()->where(['active' => 1, 'cat_id' => $this->screen_id])->orderBy('name asc')->all();
        if (empty($result)) {
            $this->statusCode = 401;
            $json['error']['msg'] = "No category found";
        } else {
            foreach ($result as $data) {
                $json['success'][] = ['image' => $data->categoryImage, 'category_name' => $data->name];
            }
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionGetcuisine() {
        $language_id = $this->language_id;
        $model = new Cuisine();
        $result = $model->find()->select('*')->joinWith('cuisineName as cn')->where(['status' => 'active', 'cn.language_id' => $language_id])->orderBy('cn.cuisine_name asc')->all();
        if (empty($result)) {
            $this->statusCode = 401;
            $json['error']['msg'] = "No cuisine found";
        } else {
            foreach ($result as $data) $json['success'][] = ['cuisine_name' => $data->cuisine_name, 'image' => $data->cuisineImage];
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionGetfilter($screen_id = null, $country_name = null) {
        $country_id = null;
        if (!empty($country_name)) {
            $cntry = Countries::find()->where(['like', 'name', trim($country_name) ])->one();
            if (!empty($cntry)) $country_id = $cntry->country_id;
        }
        if (!empty($screen_id)) $this->screen_id = $screen_id;
        $language_id = $this->language_id;
        $categoryList = $cuisinesList = $cityList = $mealList = $sortmethodList = $occList = [];
        $categories = Category::find()->where(['active' => 1, 'cat_id' => $this->screen_id])->orderBy('name asc')->asArray()->all();
        $cuisines = Cuisine::find()->select('*')->joinWith('cuisineName as cn')->where(['status' => 'active', 'cn.language_id' => $language_id])->orderBy('cn.cuisine_name asc')->asArray()->all();
        if (!empty($country_id)) $cities = City::find()->select('*')->joinWith('cityName as cn')->where(['country_id' => $country_id, 'status' => 'active', 'cn.language_id' => $language_id])->orderBy('cn.city_name asc')->asArray()->all();
        else $cities = City::find()->select('*')->joinWith('cityName as cn')->where(['status' => 'active', 'cn.language_id' => $language_id])->orderBy('cn.city_name asc')->asArray()->all();
        $meals = Meal::find()->select('*')->joinWith('mealName as mn')->where(['status' => 'active', 'mn.language_id' => $language_id])->orderBy('mn.meal_name asc')->asArray()->all();
        $occupations = Occupation::find()->select('*')->joinWith('occupationName as on')->where(['status' => 'active', 'on.language_id' => $language_id])->orderBy('on.occupation_name asc')->asArray()->all();
        $sortmethods = Sortmethod::find()->where(['status' => 'active', 'type' => 1])->asArray()->all();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                if (!empty($category['image'])) $category['image'] = Yii::$app->params['website_path'] . '/uploads/' . $category['image'];
                $getCat = CategoryDescription::find()->where(['category_id' => $category['category_id']])->asArray()->all();
                $categoryList[] = ['image' => $category['image'], 'category_id' => $category['category_id'], 'category_name' => $getCat[0]['category_name']];
            }
        }
        if (!empty($cuisines)) {
            foreach ($cuisines as $cuisine) {
                if (!empty($cuisine['image'])) $cuisine['image'] = Yii::$app->params['website_path'] . '/uploads/' . $cuisine['image'];
                $cuisinesList[] = ['image' => $cuisine['image'], 'cuisine_id' => $cuisine['cuisine_id'], 'cuisine_name' => $cuisine['cuisine_name']];
            }
        }
        if (!empty($cities)) {
            foreach ($cities as $city) $cityList[] = ['city_id' => $city['city_id'], 'city_name' => $city['city_name']];
        }
        if (!empty($meals)) {
            foreach ($meals as $meal) {
                if (!empty($meal['image'])) $meal['image'] = Yii::$app->params['website_path'] . '/uploads/' . $meal['image'];
                $mealList[] = ['image' => $meal['image'], 'meal_id' => $meal['meal_id'], 'meal_name' => $meal['meal_name']];
            }
        }
        if (!empty($occupations)) {
            foreach ($occupations as $occupation) $occList[] = ['occupation_id' => $occupation['occupation_id'], 'occupation_name' => $occupation['occupation_name']];
        }
        if (!empty($sortmethods)) {
            foreach ($sortmethods as $sortmethod) $sortmethodList[] = ['sort_id' => $sortmethod['id'], 'sortmethod_name' => $sortmethod['sort_name']];
        }
        $json['success']['category'] = $categoryList;
        $json['success']['cuisine'] = $cuisinesList;
        $json['success']['city'] = $cityList;
        $json['success']['meal'] = $mealList;
        $json['success']['occupations'] = $occList;
        $json['success']['sortmethod'] = $sortmethodList;
        $json['success']['transactions'] = ['transaction_fee' => config_transaction_fee, 'vat' => config_vat, 'freshhomee_fee' => config_freshhommee_fee];
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionAddmenu() {
        $user_info = Yii::$app->user->identity;
        //        $user_subscription = $user_info->subscriptionData;
        //        if($user_subscription['status']=='inactive'){
        //            $json['error']['msg']	= 'Please subscribe the plan.' ;
        //            $this->statusCode = 401;
        //            $json['code']		= $this->statusCode;
        //            return $json;
        //        }
        //   echo "<pre>";
        // print_r($_POST);die;
        $model = new Menu();
        $model->scenario = 'food_menu';
        $post_data['Menu'] = Yii::$app->request->post();
        $file_data = [];
        if (isset($_FILES['dish_image']) && !empty($_FILES['dish_image'])) {
            foreach ($_FILES['dish_image'] as $key => $pic_data) {
                $file_data[$key] = ['dish_image' => $pic_data];
            }
        }
        if (!empty($file_data)) $_FILES['Menu'] = $file_data;
        $model->supplier_id = $user_info->id;
        $model->dist_qty = !empty($model->dist_qty) ? $model->dist_qty : 0;
        $more_text = '';
        if ($model->load($post_data) && $model->validate()) {
            $model->city_id = $user_info->city;
            //start new colom added
            if (!empty($post_data['menu']['brand_id'])) {
                $model->brand_id = $post_data['menu']['brand_id'];
            }
            //end new colom added
            $model->product_type = $user_info->supplier_type;
            $dish_image = UploadedFile::getInstance($model, 'dish_image');
            if (!empty($dish_image)) {
                $library = new Library();
                $model->dish_image = $library->saveFile($dish_image, 'menu');
                $model->image_approval = 0;
                $more_text = ' Please wait till image approval from Admin.';
            }
            if (!empty($model->dish_since)) {
                $explode = explode(':', $model->dish_since);
                $time = $explode[0] * 60;
                if (isset($explode[1])) $time+= $explode[1];
                $model->dish_prep_time = $time;
            }
            if ($model->save()) {
                if (isset($post_data['Menu']['dish_categories']) && !empty($post_data['Menu']['dish_categories'])) {
                    $categories = explode(',', $post_data['Menu']['dish_categories']);
                    foreach ($categories as $category) {
                        $cat_model = new MenuCategories();
                        $cat_model->menu_id = $model->id;
                        $cat_model->category_id = $category;
                        $cat_model->main_category = 1;
                        $cat_model->save(false);
                    }
                }
                if (isset($post_data['Menu']['dish_meals']) && !empty($post_data['Menu']['dish_meals'])) {
                    $meals = explode(',', $post_data['Menu']['dish_meals']);
                    foreach ($meals as $meal) {
                        $menu_model = new MenuMeals();
                        $menu_model->menu_id = $model->id;
                        $menu_model->meal_id = $meal;
                        $menu_model->save();
                    }
                }
                if (isset($post_data['Menu']['dish_cuisines']) && !empty($post_data['Menu']['dish_cuisines'])) {
                    $cuisines = explode(',', $post_data['Menu']['dish_cuisines']);
                    foreach ($cuisines as $cuisine) {
                        $cus_model = new MenuCuisines();
                        $cus_model->menu_id = $model->id;
                        $cus_model->cuisine_id = $cuisine;
                        $cus_model->save();
                    }
                }
                $json['success']['dish_name'] = $model->dish_name;
                $json['success']['msg'] = "Your Dish has been added successfully." . $more_text;
            }
        } else {
            $all_errors = [];
            $errors = $model->getErrors();
            foreach ($errors as $key => $error) {
                $all_errors[] = $error[0];
            }
            $json['error']['msg'] = !empty($all_errors) ? $all_errors[0] : '';
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionMenulist() {
        $json = [];
        $page_size = 30;
        $user_info = Yii::$app->user->identity;
        $supplier_id = $user_info->id;
        $query = Menu::find()->where(['supplier_id' => $supplier_id, 'is_delete' => 0, 'product_type' => 1]);
        $dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => ['defaultPageSize' => $page_size, ], 'sort' => ['defaultOrder' => ['dish_name' => SORT_ASC, ]], ]);
        $results = $dataProvider->getModels();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                $json['success'][$k] = $result->dishInfo;
            }
        } else {
            $this->statusCode = 401;
            $json['error']['msg'] = 'Menu list is empty';
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionMenulisting($supplier_id) {
        $json = [];
        $page_size = 30;
        $query = Menu::find()->where(['supplier_id' => $supplier_id, 'is_delete' => 0, 'test_data' => 0]);
        $dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => ['defaultPageSize' => $page_size, ], 'sort' => ['defaultOrder' => ['dish_name' => SORT_ASC, ]], ]);
        $results = $dataProvider->getModels();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                $json['success'][$k] = $result->dishInfo;
            }
        } else {
            $this->statusCode = 401;
            $json['error']['msg'] = 'Menu list is empty';
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionViewdish($id) {
        $result = Menu::findOne($id);
        if (empty($result)) {
            $this->statusCode = 401;
            $json['error']['msg'] = 'Wrong selection';
        } else {
            $json['success'] = $result->dishInfo;
            $result->updateCounters(['view' => 1]);
            $user_image = '';
            $supplierInfo = $result->supplierInfo;
            if (!empty($supplierInfo->profile_pic) && $supplierInfo->image_approval == 1) $user_image = Url::to($supplierInfo->getUserImage($supplierInfo->profile_pic, 600, 400), true);
            $json['success']['supplier']['supplier_id'] = $supplierInfo->id;
            $json['success']['supplier']['name'] = $supplierInfo->name;
            $json['success']['supplier']['location'] = $supplierInfo->location;
            $json['success']['supplier']['rating'] = 4.5;
            $json['success']['supplier']['profile_pic'] = $user_image;
            // $json['success']['supplier']['product_type']		= $supplierInfo->product_type;
            
        }
        $categoryid = $json['success']['product_type'];
        $sub_category_info = [];
        $sub_category = [];
        $categories = Category::find()->where(['cat_id' => $categoryid])->andWhere(['active' => 1])->limit(10)->all();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $categorydiscription = CategoryDescription::find()->where(['category_id' => $category->category_id])->andWhere(['language_id' => 1])->one();
                $image = '';
                if (!empty($category['image'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $category['image'];
                }
                $sub_category_info = ['name' => $categorydiscription['category_name'], 'sub_category_id' => $category->category_id, 'image' => $image];
                $sub_category[] = $sub_category_info;
            }
        }
        // $json['code']	= $this->statusCode;
        // $json['sub_category'] = $sub_category;
        $json['success']['sub_category'] = $sub_category;
        $brand_info = [];
        $branddetails = [];
        $brandsql = "SELECT tbl_brand_description.brand_id,tbl_brand_description.brand_name,tbl_brand_category.category_id FROM tbl_brand_category 
            LEFT JOIN tbl_brand_description ON tbl_brand_description.brand_id=tbl_brand_category.brand_id 
            AND tbl_brand_description.language_id=1  
            WHERE tbl_brand_category.category_id IN (SELECT category_id FROM tbl_category WHERE cat_id=$categoryid) 
            GROUP BY tbl_brand_category.brand_id ";
        $brandlist = Yii::$app->db->createCommand($brandsql)->queryAll();
        if (!empty($brandlist)) {
            foreach ($brandlist as $value) {
                $branddata = Brand::find()->where(['brand_id' => $value['brand_id']])->one();
                $image = '';
                if (!empty($branddata['image'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $branddata['image'];
                }
                $brand_info = ['name' => $value['brand_name'], 'brand_id' => $value['brand_id'], 'image' => $image];
                $branddetails[] = $brand_info;
            }
        }
        // $json['code']	= $this->statusCode;
        // $json['brand_list'] = $branddetails;
        $json['success']['brand_list'] = $branddetails;
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionEditdish() {
        $user_info = Yii::$app->user->identity;
        $user_subscription = $user_info->subscriptionData;
        if ($user_subscription['status'] == 'inactive') {
            $json['error']['msg'] = 'Please subscribe the plan.';
            $this->statusCode = 401;
            $json['code'] = $this->statusCode;
            return $json;
        }
        $supplier_id = $user_info->id;
        $post_data['Menu'] = Yii::$app->request->post();
        if (isset($post_data['Menu']['dish_id']) && !empty($post_data['Menu']['dish_id'])) {
            $model = Menu::find()->where(['supplier_id' => $supplier_id, 'id' => $post_data['Menu']['dish_id']])->one();
            if (empty($model)) {
                $json['error']['msg'] = 'You are not authorized to perform this action.';
                $this->statusCode = 401;
                $json['code'] = $this->statusCode;
                return $json;
            }
            $model->scenario = 'food_menu';
            if (!$model) {
                $this->statusCode = 401;
                $json['error']['msg'] = 'Wrong selection';
            } else {
                if (!isset($post_data['Menu']['dish_name'])) {
                    if ($model->load($post_data) && $model->validate()) {
                        $model->city_id = $user_info->city;
                        if (!empty($post_data['menu']['brand_id'])) {
                            $model->brand_id = $post_data['menu']['brand_id'];
                        }
                        if (!empty($model->dish_since)) {
                            $explode = explode(':', $model->dish_since);
                            $time = $explode[0] * 60;
                            if (isset($explode[1])) $time+= $explode[1];
                            $model->dish_prep_time = $time;
                        }
                        if (isset($post_data['Menu']['dist_qty'])) $model->dist_qty;
                        if ($model->save()) {
                            $manudelte = MenuCategories::deleteAll(['menu_id' => $model->id]);
                            if (isset($post_data['Menu']['dish_categories']) && !empty($post_data['Menu']['dish_categories'])) {
                                $categories = explode(',', $post_data['Menu']['dish_categories']);
                                foreach ($categories as $category) {
                                    $cat_model = new MenuCategories();
                                    $cat_model->menu_id = $model->id;
                                    $cat_model->category_id = $category;
                                    $cat_model->main_category = 1;
                                    $cat_model->save(false);
                                }
                            }
                            if (isset($post_data['Menu']['dist_qty']) && $model->status == 'inactive') {
                                $sch = MenuSchedules::find()->where(['supplier_id' => $supplier_id, 'schedule_date' => date("Y-m-d") ])->orderBy('id desc')->one();
                                if (!empty($sch)) {
                                    $items = MenuScheduleItems::find()->where(['menuschedule_id' => $sch->id, 'menu_id' => $model->id])->one();
                                    if (!empty($items)) $sch->delete();
                                }
                            }
                            if (isset($post_data['Menu']['dist_qty']) && $model->status == 'active') {
                                $sch = MenuSchedules::find()->where(['supplier_id' => $supplier_id, 'schedule_date' => date("Y-m-d") ])->orderBy('id desc')->one();
                                if (!empty($sch)) {
                                    $items = MenuScheduleItems::find()->where(['menuschedule_id' => $sch->id, 'menu_id' => $model->id])->one();
                                    if (!empty($items)) $sch->delete();
                                }
                                $sch_model = new MenuSchedules();
                                $sch_model->supplier_id = $supplier_id;
                                $sch_model->schedule_date = date("Y-m-d");
                                $sch_model->start_time = "00:00:01";
                                $sch_model->end_time = "23:59:59";
                                $sch_model->choices = "active";
                                if ($sch_model->save()) {
                                    $menu_sch_items = new MenuScheduleItems();
                                    $menu_sch_items->menuschedule_id = $sch_model->id;
                                    $menu_sch_items->choices = 'active';
                                    $menu_sch_items->quantity = $post_data['Menu']['dist_qty'];
                                    $menu_sch_items->menu_id = $model->id;
                                    $menu_sch_items->save();
                                }
                            }
                        }
                    } else {
                        $all_errors = [];
                        $errors = $model->getErrors();
                        foreach ($errors as $key => $error) {
                            $all_errors[] = $error[0];
                        }
                        $json['error']['msg'] = !empty($all_errors) ? $all_errors[0] : '';
                        $this->statusCode = 401;
                        $json['code'] = $this->statusCode;
                        return $json;
                    }
                    return $this->actionViewdish($post_data['Menu']['dish_id']);
                }
                $file_data = [];
                if (isset($_FILES['dish_image']) && !empty($_FILES['dish_image'])) {
                    foreach ($_FILES['dish_image'] as $key => $pic_data) {
                        $file_data[$key] = ['dish_image' => $pic_data];
                    }
                }
                if (!empty($file_data)) $_FILES['Menu'] = $file_data;
                $model->supplier_id = $user_info->id;
                $model->dist_qty = !empty($model->dist_qty) ? $model->dist_qty : 0;
                $more_text = '';
                if ($model->load($post_data) && $model->validate()) {
                    $model->city_id = $user_info->city;
                    $dish_image = UploadedFile::getInstance($model, 'dish_image');
                    if (!empty($dish_image)) {
                        $library = new Library();
                        $model->dish_image = $library->saveFile($dish_image, 'menu');
                        $model->image_approval = 0;
                        $more_text = ' Please wait till image approval from Admin.';
                    }
                    if (!empty($model->dish_since)) {
                        $explode = explode(':', $model->dish_since);
                        $time = $explode[0] * 60;
                        if (isset($explode[1])) $time+= $explode[1];
                        $model->dish_prep_time = $time;
                    }
                    if ($model->save()) {
                        MenuCategories::deleteAll(['menu_id' => $model->id]);
                        MenuCuisines::deleteAll(['menu_id' => $model->id]);
                        MenuMeals::deleteAll(['menu_id' => $model->id]);
                        if (isset($post_data['Menu']['dish_categories']) && !empty($post_data['Menu']['dish_categories'])) {
                            $categories = explode(',', $post_data['Menu']['dish_categories']);
                            foreach ($categories as $category) {
                                $cat_model = new MenuCategories();
                                $cat_model->menu_id = $model->id;
                                $cat_model->category_id = (int)$category;
                                $cat_model->save();
                            }
                        }
                        if (isset($post_data['Menu']['dish_meals']) && !empty($post_data['Menu']['dish_meals'])) {
                            $meals = explode(',', $post_data['Menu']['dish_meals']);
                            foreach ($meals as $meal) {
                                $menu_model = new MenuMeals();
                                $menu_model->menu_id = $model->id;
                                $menu_model->meal_id = $meal;
                                $menu_model->save();
                            }
                        }
                        if (isset($post_data['Menu']['dish_cuisines']) && !empty($post_data['Menu']['dish_cuisines'])) {
                            $cuisines = explode(',', $post_data['Menu']['dish_cuisines']);
                            foreach ($cuisines as $cuisine) {
                                $cus_model = new MenuCuisines();
                                $cus_model->menu_id = $model->id;
                                $cus_model->cuisine_id = $cuisine;
                                $cus_model->save();
                            }
                        }
                        $json['success'] = $model->dishInfo;
                        $json['success']['msg'] = "Your Dish has been updated successfully." . $more_text;
                    } else {
                        $all_errors = [];
                        $errors = $model->getErrors();
                        foreach ($errors as $key => $error) {
                            $all_errors[] = $error[0];
                        }
                        $json['error']['msg'] = !empty($all_errors) ? $all_errors[0] : '';
                        $this->statusCode = 401;
                    }
                } else {
                    $all_errors = [];
                    $errors = $model->getErrors();
                    foreach ($errors as $key => $error) {
                        $all_errors[] = $error[0];
                    }
                    $json['error']['msg'] = !empty($all_errors) ? $all_errors[0] : '';
                    $this->statusCode = 401;
                }
            }
        } else {
            $this->statusCode = 401;
            $json['error']['msg'] = 'Wrong selection';
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionSearchitem() {
        //        if(!empty($screen_id))
        //            $this->screen_id = $screen_id;
        $user_model = new User();
        $headers = getallheaders();
        $token = '';
        if (isset($headers['Authorization'])) $token = str_replace("Bearer ", "", $headers['Authorization']);
        $supplier = [];
        $post_data = Yii::$app->request->post();
        $this->screen_id = $post_data['screen_id']??$this->screen_id;
        $user_info = $user_model->findIdentityByAccessToken($token);
        if (!empty($user_info)) {
            $user_image = '';
            if (!empty($user_info->profile_pic) && $user_info->image_approval == 1) $user_image = Url::to($user_info->getUserImage($user_info->profile_pic, 600, 400), true);
            $supplier['name'] = $user_info->name;
            $supplier['username'] = $user_info->username;
            $supplier['email'] = $user_info->email;
            $supplier['availability'] = $user_info->availability;
            $supplier['user_image'] = $user_image;
        }
        $page_id = isset($_GET['page']) ? $_GET['page'] : 1;
        $page_size = (isset($post_data['perPage']) && !empty($post_data['perPage'])) ? trim($post_data['perPage']) : 15;
        if (isset($post_data['name'])) {
            $query = Menu::find()->select('tbl_menu.*')->joinWith('supplierInfo')->where(['and', ['tbl_menu.status' => 'active', 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0, 'tbl_menu.product_type' => $this->screen_id]])->andWhere(['or', ['like', 'tbl_menu.dish_name', $post_data['name']], ['like', 'tbl_user.name', $post_data['name']]]);
            if (!empty($user_info)) {
                $query->andWhere(['!=', 'tbl_menu.supplier_id', $user_info->id]);
            }
            $query->orderBy('tbl_menu.dish_name');
            $dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => ['defaultPageSize' => $page_size, ], ]);
            $count = $dataProvider->getTotalCount();
            if ($count > 0) {
                $json['supplier'] = $supplier;
                $dish_data = $this->getDishData($dataProvider, $page_size, $page_id);
                $json['success'] = $dish_data['success'];
            } else {
                $json['error']['msg'] = 'Menu list is empty';
                $this->statusCode = 401;
            }
        } else {
            $json['error']['msg'] = 'Please enter some keywords.';
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionFinditem() {
        $post_data = Yii::$app->request->post();
        $latitude = $post_data['latitude']??'';
        $longitude = $post_data['longitude']??'';
        //        if(!empty($screen_id))
        //            $this->screen_id = $screen_id;
        $this->screen_id = $post_data['screen_id']??$this->screen_id;
        $user_model = new User();
        $headers = getallheaders();
        $token = '';
        if (isset($headers['Authorization'])) $token = str_replace("Bearer ", "", $headers['Authorization']);
        $supplier = [];
        $user_info = $user_model->findIdentityByAccessToken($token);
        $select = '*';
        $default_addr = 'no';
        if (!empty($user_info)) {
            $user_address = UserAddress::find()->where(['user_id' => $user_info->id, 'is_default' => 'yes'])->one();
            if (!empty($user_address)) {
                $lat = $user_address->latitude;
                $long = $user_address->longitude;
                if (!empty($lat) && !empty($long)) {
                    $default_addr = 'yes';
                    $select = (new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance  "));
                }
            }
        }
        $page_id = isset($_GET['page']) ? $_GET['page'] : 1;
        $page_size = (isset($post_data['perPage']) && !empty($post_data['perPage'])) ? trim($post_data['perPage']) : 15;
        if (isset($post_data['city_id']) && !empty($post_data['city_id'])) $query = Menu::find()->select($select)->joinWith('supplierInfo')->where(['and', ['tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0, 'tbl_menu.product_type' => $this->screen_id], ['tbl_menu.city_id' => (int)$post_data['city_id']], ['>', 'subscription_end', time() ]]);
        else $query = Menu::find()->select($select)->joinWith('supplierInfo')->where(['and', ['tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0, 'tbl_menu.product_type' => $this->screen_id], ['>', 'subscription_end', time() ]]);
        if (!empty($user_info)) {
            $query->andWhere(['!=', 'tbl_menu.supplier_id', $user_info->id]);
        }
        if (isset($post_data['cuisines']) && !empty($post_data['cuisines'])) {
            $cuisines = explode(',', $post_data['cuisines']);
            $query->joinWith(['menuCuisines'])->andWhere(['in', 'tbl_menu_cuisines.cuisine_id', $cuisines]);
        }
        if (isset($post_data['category']) && !empty($post_data['category'])) {
            $categories = explode(',', $post_data['category']);
            $query->joinWith(['menuCategories'])->andWhere(['in', 'tbl_menu_categories.category_id', $categories]);
        }
        if (isset($post_data['meal']) && !empty($post_data['meal'])) {
            $meals = explode(',', $post_data['meal']);
            $query->joinWith(['menuMeals'])->andWhere(['in', 'tbl_menu_meals.meal_id', $meals]);
        }
        if (isset($post_data['sort']) && !empty($post_data['sort'])) {
            $sort = $post_data['sort'];
            if ($sort == 1) $query->orderBy('tbl_menu.created_at DESC');
            elseif ($sort == 2) {
                if (!empty($latitude) && !empty($longitude)) {
                    $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $latitude - latitude) *  pi()/180 / 2), 2) +COS( $latitude * pi()/180) * COS(latitude * pi()/180) * POWER(SIN(( $longitude - longitude) * pi()/180 / 2), 2) ))) as distance  "))->orderBy("distance");
                }
            } elseif ($sort == 3) $query->orderBy('tbl_menu.dish_serve DESC');
            elseif ($sort == 4) {
            } elseif ($sort == 5) {
                $query->orderBy(new Expression('CASE WHEN (tbl_menu.status) = "active" THEN pos ELSE 1 END,tbl_menu.created_at DESC'));
            } else $query->orderBy('tbl_menu.created_at DESC');
        } else {
            if ($default_addr == 'yes') $query->orderBy('distance ASC');
        }
        $dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => ['defaultPageSize' => $page_size, ], 'sort' => ['defaultOrder' => ['dish_name' => SORT_ASC, ]], ]);
        $count = $dataProvider->getTotalCount();
        if ($count > 0) {
            $cart_data = [];
            $dish_data = $this->getDishData($dataProvider, $page_size, $page_id);
            $json['success'] = $dish_data['success'];
            $json['unread_notifications'] = 0;
            if (!empty($user_info)) {
                if (isset($post_data['is_user']) && $post_data['is_user'] == 1) {
                    $json['success']['supplier'] = $user_info->userDetail;
                    $cart = new Cart();
                    $total_items = $cart->getTotalCartItems($user_info->id);
                    if (isset($total_items['total_items'])) {
                        $cart_data['cart_items'] = $total_items['total_items'];
                        $cart_data['cart_total'] = $total_items['total_price'];
                    } else {
                        $cart_data['cart_items'] = [];
                        $cart_data['cart_total'] = [];
                    }
                    $json['unread_notifications'] = Notifications::find()->where(['status' => 1, 'user_id' => $user_info->id, 'role' => 1, 'is_read' => 0])->count();
                } else {
                    $json['success']['supplier'] = $user_info->userInfo;
                    $json['unread_notifications'] = Notifications::find()->where(['status' => 1, 'user_id' => $user_info->id, 'role' => 2, 'is_read' => 0])->count();
                }
            } else $json['success']['supplier'] = [];
            $json['success']['cart_data'] = !empty($cart_data) ? $cart_data : (object)[];
        } else {
            $cart_data = [];
            if (isset($post_data['is_user']) && $post_data['is_user'] == 1) {
                if (!empty($user_info)) {
                    $cart = new Cart();
                    $total_items = $cart->getTotalCartItems($user_info->id);
                    if (isset($total_items['total_items'])) {
                        $cart_data['cart_items'] = $total_items['total_items'];
                        $cart_data['cart_total'] = $total_items['total_price'];
                    } else {
                        $cart_data['cart_items'] = [];
                        $cart_data['cart_total'] = [];
                    }
                }
            }
            $json['error']['msg'] = 'Menu list is empty';
            $json['cart_data'] = !empty($cart_data) ? $cart_data : (object)[];
            $this->statusCode = 401;
        }
        $subscription_data = [];
        if (!empty($user_info)) {
            $subscription_data = $user_info->subscriptionData;
        }
        $json['subscription_data'] = $subscription_data;
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionGetmenuschedule() {
        $user_info = Yii::$app->user->identity;
        $supplier_id = $user_info->id;
        $results = $user_info->userMenuSchedules;
        $days_between = ceil(abs(time() - $user_info->created_at) / 86400);
        if (!empty($results)) {
            $json['success']['data'] = $results;
            $json['success']['days'] = $days_between;
            if ($days_between <= 15) $json['success']['message'] = ''; // 'Please add minimum 12 schedules to continue.';
            else $json['success']['message'] = '';
        } else {
            $json['error']['msg'] = 'Schedule list is empty';
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionSchedulelisting($supplier_id) {
        $user_info = User::findOne($supplier_id);
        $results = $user_info->userMenuSchedules;
        if (!empty($results)) $json['success']['data'] = $results;
        else {
            $json['error']['msg'] = 'Schedule list is empty';
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    private function getDishData($dataProvider, $page_size, $page_id) {
        $results = $dataProvider->getModels();
        $count = $dataProvider->getTotalCount();
        $total_pages = ceil($count / $page_size);
        foreach ($results as $k => $result) {
            if ($result->product_type == 1) $dish_data[$k] = $result->dishInfo;
            else $dish_data[$k] = $result->productInfo;
        }
        $json['success']['data'] = $dish_data;
        if ($total_pages > $page_id) {
            $json['success']['pageAction']['next_page_url'] = Url::to(Yii::$app->request->baseUrl . '/searchItem?page=' . ($page_id + 1), true);
        } else $json['success']['pageAction']['next_page_url'] = '';
        $json['success']['pageAction']['current_page'] = Url::to(Yii::$app->request->baseUrl . '/searchItem?page=' . ($page_id), true);
        if ($page_id != 1) $json['success']['pageAction']['prev_page_url'] = Url::to(Yii::$app->request->baseUrl . '/searchItem?page=' . ($page_id - 1), true);
        else $json['success']['pageAction']['prev_page_url'] = '';
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionGetMenuRating($menu_id) {
        $page_size = 30;
        $result = Menu::findOne($menu_id);
        if (!empty($result)) {
            $json['avg_reviews'] = $result->avgReviews;
            $query = MenuRating::find()->where(['!=', 'reviews', ''])->andWhere(['menu_id' => $menu_id]);
            $dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => ['defaultPageSize' => $page_size, ], 'sort' => ['defaultOrder' => ['id' => SORT_DESC, ]], ]);
            $results = $dataProvider->getModels();
            $count = $dataProvider->getTotalCount();
            $total_pages = ceil($count / $page_size);
            $json['total_pages'] = $total_pages;
            $reviews = [];
            foreach ($results as $k => $result) $reviews[$k] = $result->ratingInfo;
            $json['reviews'] = $reviews;
        } else {
            $json['error']['msg'] = 'No rating found.';
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionAddmenuschedule() {
        $raw_data = Yii::$app->request->rawBody;
        $post_data = json_decode($raw_data, true);
        $user_info = Yii::$app->user->identity;
        $user_subscription = $user_info->subscriptionData;
        if ($user_subscription['status'] == 'inactive') {
            $json['error']['msg'] = 'Please subscribe the plan.';
            $this->statusCode = 401;
            $json['code'] = $this->statusCode;
            return $json;
        }
        $supplier_id = $user_info->id;
        $days_between = ceil(abs(time() - $user_info->created_at) / 86400);
        if (!empty($post_data)) {
            $data = $post_data;
            if (isset($data['data'])) {
                $count_days = count($data['data']);
                //                if($days_between>=15 && $count_days<12){
                //                    $json['error']['msg']	= 'Please add minimum 12 schedules to continue.';
                //                }else{
                foreach ($data['data'] as $data) {
                    $date = $data['date'];
                    $start_time = $data['start_time'];
                    $end_time = $data['end_time'];
                    $menu_items = $data['menu_items'];
                    $menu_schedule = new MenuSchedules();
                    $result_sch = $menu_schedule->find()->where(['supplier_id' => $supplier_id, 'schedule_date' => $date])->one();
                    $menu_schedule->supplier_id = $supplier_id;
                    if (!empty($result_sch)) {
                        $menu_schedule = $result_sch;
                    }
                    $menu_schedule->schedule_date = $date;
                    $menu_schedule->start_time = $start_time;
                    $menu_schedule->end_time = $end_time;
                    $menu_schedule->choices = 'active';
                    if ($menu_schedule->save()) {
                        MenuScheduleItems::deleteAll(['menuschedule_id' => $menu_schedule->id]);
                        foreach ($menu_items as $menu_item) {
                            $menu_sch_items = new MenuScheduleItems();
                            $menu_sch_items->menuschedule_id = $menu_schedule->id;
                            $menu_sch_items->choices = 'active';
                            $menu_sch_items->quantity = $menu_item['qty'];
                            $menu_sch_items->menu_id = $menu_item['menu_id'];
                            $menu_sch_items->save();
                        }
                    }
                }
                $json['success']['message'] = 'Your schedule has been added successfully.';
                //               }
                
            }
        } else {
            $json['error']['msg'] = 'Please enter some data.';
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionUpdatemenuschedule() {
        $raw_data = Yii::$app->request->rawBody;
        $post_data = json_decode($raw_data, true);
        $user_info = Yii::$app->user->identity;
        $supplier_id = $user_info->id;
        if (!empty($post_data)) {
            $data = $post_data;
            if (isset($data['data'])) {
                foreach ($data['data'] as $data) {
                    $date = $data['date'];
                    $start_time = $data['start_time'];
                    $end_time = $data['end_time'];
                    $menu_items = $data['menu_items'];
                    $menu_schedule = new MenuSchedules();
                    $result_sch = $menu_schedule->find()->where(['supplier_id' => $supplier_id, 'schedule_date' => $date])->one();
                    $menu_schedule->supplier_id = $supplier_id;
                    if (!empty($result_sch)) {
                        $menu_schedule = $result_sch;
                        $menu_schedule->schedule_date = $date;
                        $menu_schedule->start_time = $start_time;
                        $menu_schedule->end_time = $end_time;
                        $menu_schedule->choices = 'active';
                        if ($menu_schedule->save()) {
                            MenuScheduleItems::deleteAll(['menuschedule_id' => $menu_schedule->id]);
                            foreach ($menu_items as $menu_item) {
                                $menu_sch_items = new MenuScheduleItems();
                                $menu_sch_items->menuschedule_id = $menu_schedule->id;
                                $menu_sch_items->choices = 'active';
                                $menu_sch_items->quantity = $menu_item['qty'];
                                $menu_sch_items->menu_id = $menu_item['menu_id'];
                                $menu_sch_items->save();
                            }
                        }
                    }
                }
                $json['success']['message'] = 'Your schedule has been updated successfully.';
            }
        } else {
            $json['error']['msg'] = 'Please enter some data.';
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionDeletedish($dish_id) {
        $user_info = Yii::$app->user->identity;
        $supplier_id = $user_info->id;
        $result = Menu::find()->where(['supplier_id' => $supplier_id, 'id' => $dish_id])->one();
        MenuScheduleItems::deleteAll(['menu_id' => $dish_id]);
        $all_schedules = MenuSchedules::find()->where(['supplier_id' => $supplier_id])->all();
        foreach ($all_schedules as $all_schedule) {
            $count_items = MenuScheduleItems::find()->where(['menuschedule_id' => $all_schedule->id])->count();
            if ($count_items == 0) $all_schedule->delete();
        }
        if (!empty($result)) {
            $result->is_delete = 1;
            if ($result->save()) $json['success']['message'] = 'Your dish has been deleted successfully.';
        } else {
            $json['error']['msg'] = 'Something went wrong! Try again later.';
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    public function actionDeleteschedule($id) {
        $user_info = Yii::$app->user->identity;
        $supplier_id = $user_info->id;
        $result = MenuSchedules::find()->where(['id' => $id, 'supplier_id' => $supplier_id])->one();
        if (!empty($result)) {
            $result->delete();
            $json['success']['message'] = 'Your schedule has been deleted successfully.';
        } else {
            $json['error']['msg'] = 'Something went wrong! Try again later.';
            $this->statusCode = 401;
        }
        $json['code'] = $this->statusCode;
        return $json;
    }
    /**
     * Get Top homepage data
     * @param integer $screen_id
     * @param integer $limit
     * @return mixed
     */
    public function actionHomeItemsList() {
        $json = [];
        $long = $long = '';
        $user_model = new User();
        $headers = getallheaders();
        $token = '';
        $top_selling_products = $online_kitchens = $user_address = $user_data = $latest_products = $order_now = $top_categories = [];
        if (isset($headers['Authorization'])) $token = str_replace("Bearer ", "", $headers['Authorization']);
        $user_info = $user_model->findIdentityByAccessToken($token);
        if (!empty($user_info)) {
            $user_data = $user_info->userDetail;
            $user_address = UserAddress::find()->where(['user_id' => $user_info->id, 'is_default' => 'yes'])->one();
            if (!empty($user_address)) {
                $lat = $user_address->latitude;
                $long = $user_address->longitude;
            }
        }
        // Start Latest products
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance "))->orderBy('distance');
        } else $query->select('tbl_menu.*');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.status' => 'active', 'tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['NOT IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy('tbl_menu.id DESC')->limit(10)->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                if ($result->product_type == 1) $latest_products[$k] = $result->dishInfo;
                else $latest_products[$k] = $result->productInfo;
            }
        }
        $json['latest_products'] = $latest_products;
        // End Latest products
        // Start best selling products
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance ,SUM(op.quantity) AS TotalQuantity "))->orderBy('distance');
        } else $query->select('tbl_menu.*,SUM(op.quantity) AS TotalQuantity');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['NOT IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy('TotalQuantity desc')->limit(10)->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                if ($result->product_type == 1) $top_selling_products[$k] = $result->dishInfo;
                else $top_selling_products[$k] = $result->productInfo;
                $top_selling_products[$k]['total_quantity_sold'] = $result->TotalQuantity;
            }
        }
        $json['best_selling_products'] = $top_selling_products;
        // End best selling products
        // Start Online kitchen
        $query = User::find();
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_user.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance  "))->orderBy('distance');
        }
        $query->where([
        // 'availability'=>'online',
        'status' => 10, 'is_test_user' => 0, 'is_supplier' => 'yes', 'supplier_type' => 1]);
        $query->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'id', $user_info->id]);
        $results = $query->limit(10)->orderBy(new Expression('CASE WHEN  availability = "online" THEN 0 WHEN availability = "offline" THEN 1 ELSE 2
		END '))->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) $online_kitchens[] = $result->userInfo;
        }
        $json['online_kitchens'] = $online_kitchens;
        $json['user_address'] = $user_address;
        $json['user_data'] = $user_data;
        // End Online kitchen
        // Start online now items
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance ,SUM(op.quantity) AS TotalQuantity "))->orderBy('distance');
        } else $query->select('tbl_menu.*');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy(new Expression('tbl_menu.id DESC,CASE WHEN  tbl_menu.status = "online" THEN 0 WHEN tbl_menu.status = "offline" THEN 1 ELSE 2
		END '))->limit(10)->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                if ($result->product_type == 1) $order_now[$k] = $result->dishInfo;
                else $order_now[$k] = $result->productInfo;
            }
        }
        $json['order_now'] = $order_now;
        // End online now items
        // Start featured categories
        $categories = Category::find()->where(['featured' => 1, 'active' => 1])->orderBy('sort_order')->all();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $category_info = ['type' => 'category', 'category_id' => $category->category_id, 'name' => $category->name, 'image' => $category->categoryImage];
                $top_categories[] = $category_info;
            }
        }
        // End featured categories
        $json['top_categories'] = $top_categories;
        $json['code'] = $this->statusCode;
        return $json;
    }
    // start home category list
    public function actionHomeItems() {
        //  $HomeCategoryArray= [];
        //  $HomeCategory = HomeCategory::find()->where(['status'=>1])->all();
        //  foreach ($HomeCategory as $key => $value) {
        //  	// echo "<pre>";
        //  	// print_r($value);
        //   $HomeCategoryArray[$value['home_category_id']] = $value['home_category_id'];
        //   $HomeCategoryArray[$value['home_category_id']] = $value['image'];
        //   $HomeCategoryArray[$value['home_category_id']] = $value['file_info'];
        //   $HomeCategoryArray[$value['home_category_id']] = $value['status'];
        //   $HomeCategoryArray[$value['home_category_id']] = $value['created_at'];
        //   $HomeCategoryArray[$value['home_category_id']] = $value['updated_at'];
        //  }
        // echo "<pre>";
        // print_r($HomeCategoryArray);die;
        //  die;
        $json = [];
        $long = $long = '';
        $user_model = new User();
        $headers = getallheaders();
        $token = '';
        $top_selling_products = $online_kitchens = $user_address = $user_data = $latest_products = $order_now = $top_categories = [];
        
        if (isset($headers['Authorization'])) $token = str_replace("Bearer ", "", $headers['Authorization']);
        $user_info = $user_model->findIdentityByAccessToken($token);
        if (!empty($user_info)) {
            $user_data = $user_info->userDetail;
            $user_address = UserAddress::find()->where(['user_id' => $user_info->id, 'is_default' => 'yes'])->one();
            if (!empty($user_address)) {
                $lat = $user_address->latitude;
                $long = $user_address->longitude;
            }
        }
        // Start Latest products
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance "))->orderBy('distance');
        } else $query->select('tbl_menu.*');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.status' => 'active', 'tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['NOT IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy('tbl_menu.id DESC')->limit(10)->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                if ($result->product_type == 1) $latest_products[$k] = $result->dishInfo;
                else $latest_products[$k] = $result->productInfo;
            }
        }
        $json['latest_products'] = $latest_products;
        // End Latest products
        // Start best selling products
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance ,SUM(op.quantity) AS TotalQuantity "))->orderBy('distance');
        } else $query->select('tbl_menu.*,SUM(op.quantity) AS TotalQuantity');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['NOT IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy('TotalQuantity desc')->limit(10)->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                if ($result->product_type == 1) $top_selling_products[$k] = $result->dishInfo;
                else $top_selling_products[$k] = $result->productInfo;
                $top_selling_products[$k]['total_quantity_sold'] = $result->TotalQuantity;
            }
        }
        $json['best_selling_products'] = $top_selling_products;
        // End best selling products
        // Start Online kitchen
        $query = User::find();
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_user.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance  "))->orderBy('distance');
        }
        $query->where([
        // 'availability'=>'online',
        'status' => 10, 'is_test_user' => 0, 'is_supplier' => 'yes', 'supplier_type' => 1]);
        $query->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'id', $user_info->id]);
        $results = $query->limit(10)->orderBy(new Expression('CASE WHEN  availability = "online" THEN 0 WHEN availability = "offline" THEN 1 ELSE 2
		END '))->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) $online_kitchens[] = $result->userInfo;
        }
        $json['online_kitchens'] = $online_kitchens;
        $json['user_address'] = $user_address;
        $json['user_data'] = $user_data;
        // End Online kitchen
        // Start online now items
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance ,SUM(op.quantity) AS TotalQuantity "))->orderBy('distance');
        } else $query->select('tbl_menu.*');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy(new Expression('tbl_menu.id DESC,CASE WHEN  tbl_menu.status = "online" THEN 0 WHEN tbl_menu.status = "offline" THEN 1 ELSE 2
		END '))->limit(10)->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                if ($result->product_type == 1) $order_now[$k] = $result->dishInfo;
                else $order_now[$k] = $result->productInfo;
            }
        }
        $json['order_now'] = $order_now;
        // End online now items
        // Start featured categories
        $categories = Category::find()->where(['featured' => 1, 'active' => 1])->orderBy('sort_order')->all();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $category_info = ['type' => 'category', 'category_id' => $category->category_id, 'name' => $category->name, 'image' => $category->categoryImage];
                $top_categories[] = $category_info;
            }
        }
        // End featured categories
        // home category start
        $categories = HomeCategory::find()->where(['status' => 1])->andWhere(['is_home' => 1])->all();
        $home_category_info = [];
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $HomeCategorydiscription = HomeCategoryDescription::find()->where(['home_category_id' => $category->home_category_id])->andWhere(['language_id' => 1])->one();
                $image = '';
                if (!empty($category['image'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $category['image'];
                }
                $home_category_info = ['name' => $HomeCategorydiscription['home_category_name'], 'home_category_id' => $category->home_category_id, 'image' => $image, 'file_info' => $category->file_info, 'status' => $category->status, 'created_at' => $category->created_at, 'updated_at' => $category->updated_at, ];
                $home_category[] = $home_category_info;
            }
        }
        // $home_category_info = ['name'=> 'Deals','home_category_id' => 0, 'image' => '','file_info' => '','status' => 1,'created_at' => '','updated_at' => '',];
        // $home_category[] = $home_category_info;
        // home category end
        $json['code'] = $this->statusCode;
        $json['home_category'] = $home_category;
        $json['top_categories'] = $top_categories;
        $json['code'] = $this->statusCode;
        // echo "<pre>";
        // print_r($json);die;
        return $json;
    }
    // Start Latest products
    public function actionLatestProduct() {
        $pageno = 0;
        $limitid = 5;
        if (!empty($_GET['limit'])) {
            if ($_GET['limit'] > 1) {
                $pageno = $_GET['limit'] * $limitid;
            }
        }
        $json = [];
        $long = $long = '';
        $user_model = new User();
        $headers = getallheaders();
        $token = '';
        $top_selling_products = $online_kitchens = $user_address = $user_data = $latest_products = $order_now = $top_categories = [];
        if (isset($headers['Authorization'])) $token = str_replace("Bearer ", "", $headers['Authorization']);
        $user_info = $user_model->findIdentityByAccessToken($token);
        if (!empty($user_info)) {
            $user_data = $user_info->userDetail;
            $user_address = UserAddress::find()->where(['user_id' => $user_info->id, 'is_default' => 'yes'])->one();
            if (!empty($user_address)) {
                $lat = $user_address->latitude;
                $long = $user_address->longitude;
            }
        }
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance "))->orderBy('distance');
        } else $query->select('tbl_menu.*');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.status' => 'active', 'tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['NOT IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy('tbl_menu.id DESC')->offset($pageno)->limit(10)->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                if ($result->product_type == 1) $latest_products[$k] = $result->dishInfo;
                else $latest_products[$k] = $result->productInfo;
            }
        }
        $json['code'] = $this->statusCode;
        $json['latest_products'] = $latest_products;
        return $json;
    }
    // end Latest products
    // Start best selling products
    public function actionBestSellingProduct() {
        $pageno = 0;
        $limitid = 5;
        if (!empty($_GET['limit'])) {
            if ($_GET['limit'] > 1) {
                $pageno = $_GET['limit'] * $limitid;
            }
        }
        $screen_id = 0;
        if (!empty($_GET['screen_id'])) {
            $screen_id = $_GET['screen_id'];
        }
        $json = [];
        $long = $long = '';
        $user_model = new User();
        $headers = getallheaders();
        $token = '';
        $top_selling_products = $online_kitchens = $user_address = $user_data = $latest_products = $order_now = $top_categories = [];
        if (isset($headers['Authorization'])) $token = str_replace("Bearer ", "", $headers['Authorization']);
        $user_info = $user_model->findIdentityByAccessToken($token);
        if (!empty($user_info)) {
            $user_data = $user_info->userDetail;
            $user_address = UserAddress::find()->where(['user_id' => $user_info->id, 'is_default' => 'yes'])->one();
            if (!empty($user_address)) {
                $lat = $user_address->latitude;
                $long = $user_address->longitude;
            }
        }
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance ,SUM(op.quantity) AS TotalQuantity "))->orderBy('distance');
        } else $query->select('tbl_user.*,tbl_menu.*,SUM(op.quantity) AS TotalQuantity');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['NOT IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->andWhere(['tbl_user.supplier_type' => [2, 3]]);
        // $query->where(['tbl_user.supplier_type'=> $screen_id]);
        // $query->andWhere(['tbl_user.supplier_type'=>0]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy('TotalQuantity desc')->offset($pageno)->limit($limitid)->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                if ($result->product_type == 1) $top_selling_products[$k] = $result->dishInfo;
                else $top_selling_products[$k] = $result->productInfo;
                $top_selling_products[$k]['total_quantity_sold'] = $result->TotalQuantity;
            }
        }
        $json['code'] = $this->statusCode;
        $json['best_selling_products'] = $top_selling_products;
        return $json;
    }
    // end best selling products
    // Start Online kitchen
    public function actionOnlineKitchens() {
        $pageno = 0;
        $limitid = 5;
        if (!empty($_GET['limit'])) {
            if ($_GET['limit'] > 1) {
                $pageno = $_GET['limit'] * $limitid;
            }
        }
        $json = [];
        $long = $long = '';
        $user_model = new User();
        $headers = getallheaders();
        $token = '';
        $top_selling_products = $online_kitchens = $user_address = $user_data = $latest_products = $order_now = $top_categories = [];
        if (isset($headers['Authorization'])) $token = str_replace("Bearer ", "", $headers['Authorization']);
        $user_info = $user_model->findIdentityByAccessToken($token);
        if (!empty($user_info)) {
            $user_data = $user_info->userDetail;
            $user_address = UserAddress::find()->where(['user_id' => $user_info->id, 'is_default' => 'yes'])->one();
            if (!empty($user_address)) {
                $lat = $user_address->latitude;
                $long = $user_address->longitude;
            }
        }
        $query = User::find();
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_user.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance  "))->orderBy('distance');
        }
        $query->where([
        // 'availability'=>'online',
        'status' => 10, 'is_test_user' => 0, 'is_supplier' => 'yes', 'supplier_type' => 1]);
        $query->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'id', $user_info->id]);
        $results = $query->offset($pageno)->limit($limitid)->orderBy(new Expression('CASE WHEN  availability = "online" THEN 0 WHEN availability = "offline" THEN 1 ELSE 2
		END '))->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) $online_kitchens[] = $result->userInfo;
        }
        $json['code'] = $this->statusCode;
        $json['online_kitchens'] = $online_kitchens;
        return $json;
    }
    //end Online Kitchen
    // Start online now items
    public function actionOrderNow() {
        $pageno = 0;
        $limitid = 5;
        if (!empty($_GET['limit'])) {
            if ($_GET['limit'] > 1) {
                $pageno = $_GET['limit'] * $limitid;
            }
        }
        $json = [];
        $long = $long = '';
        $user_model = new User();
        $headers = getallheaders();
        $token = '';
        $top_selling_products = $online_kitchens = $user_address = $user_data = $latest_products = $order_now = $top_categories = [];
        if (isset($headers['Authorization'])) $token = str_replace("Bearer ", "", $headers['Authorization']);
        $user_info = $user_model->findIdentityByAccessToken($token);
        if (!empty($user_info)) {
            $user_data = $user_info->userDetail;
            $user_address = UserAddress::find()->where(['user_id' => $user_info->id, 'is_default' => 'yes'])->one();
            if (!empty($user_address)) {
                $lat = $user_address->latitude;
                $long = $user_address->longitude;
            }
        }
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance ,SUM(op.quantity) AS TotalQuantity "))->orderBy('distance');
        } else $query->select('tbl_menu.*');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy(new Expression('tbl_menu.id DESC,CASE WHEN  tbl_menu.status = "online" THEN 0 WHEN tbl_menu.status = "offline" THEN 1 ELSE 2
		END '))->offset($pageno)->limit($limitid)->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                if ($result->product_type == 1) $order_now[$k] = $result->dishInfo;
                else $order_now[$k] = $result->productInfo;
            }
        }
        $json['code'] = $this->statusCode;
        $json['order_now'] = $order_now;
        return $json;
    }
    // End online now items
    // start sub category api
    // public function actionSubCategory(){
    //    $json = [];
    //     $categoryid = 0;
    //   if(isset($_GET['categoryid'])){
    //      $categoryid = $_GET['categoryid'];
    //     }
    //    $categories = Category::find()
    //    ->where(['home_category_id' => $categoryid ])
    //    ->andWhere(['active'=>1])
    //    ->limit(10)
    //    ->all();
    //    $sub_category_info = [];
    //    $sub_category = [];
    //   if(!empty($categories)){
    //       foreach ($categories as $category ){
    //       	 $categorydiscription = CategoryDescription::find()->where(['category_id'=>$category->category_id])->andWhere(['language_id'=>1])->one();
    //       	 $image = '';
    //          if(!empty($category['image'])){
    //       	  $path1 = Url::base();
    //           $path = str_replace("/api","",$path1);
    //           $image = "https://freshhomee.com".$path."/frontend/web/uploads/". $category['image'] ;
    //           }
    //           $sub_category_info = ['name'=> $categorydiscription['category_name'] ,'sub_category_id' => $category->category_id, 'image' => $image];
    //           $sub_category[] = $sub_category_info;
    //       }
    //   }
    //   $json['sub_category'] = $sub_category;
    //   return $json;
    // }
    // end sub category api
    // start view all sub category
    public function actionViewAllSubCategory() {
        $json = [];
        $pageno = 0;
        $limitid = 10;
        if (!empty($_POST['limit'])) {
            if ($_POST['limit'] > 1) {
                $pageno = $_POST['limit'] * $limitid;
            }
        }
        $homecategoryid = 0;
        if (isset($_POST['homecategoryid'])) {
            $homecategoryid = $_POST['homecategoryid'];
        }
        $categories = Category::find()->where(['home_category_id' => $homecategoryid])->andWhere(['active' => 1])->offset($pageno)->limit($limitid)->all();
        $sub_category_info = [];
        $sub_category = [];
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $categorydiscription = CategoryDescription::find()->where(['category_id' => $category->category_id])->andWhere(['language_id' => 1])->one();
                $image = '';
                if (!empty($category['image'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $category['image'];
                }
                $sub_category_info = ['name' => $categorydiscription['category_name'], 'sub_category_id' => $category->category_id, 'image' => $image];
                $sub_category[] = $sub_category_info;
            }
        }
        $json['code'] = $this->statusCode;
        $json['sub_category'] = $sub_category;
        return $json;
    }
    //end view all sub category
    // start shop by supliery
    //     public function actionSupplierBy(){
    //      $json = [];
    //      $supplier_info = [];
    //      $suppliers = [];
    //       $categoryid = 0;
    //     if(isset($_GET['categoryid'])){
    //        $categoryid = $_GET['categoryid'];
    //       }
    //      $supplier = User::find()
    //      ->where(['supplier_type'=> $categoryid ])
    //      ->limit(10)
    //      ->all();
    //      if(!empty($supplier)){
    //      foreach ($supplier as $value) {
    //      $supplier_info =['id'=>$value['id'],'name'=>$value['username'],'name'=>$value['username'],'image_approval'=>$value['image_approval']];
    //      $suppliers[] = $supplier_info;
    //      }
    //      }
    //     $json['suppliers'] = $suppliers;
    //     return $json;
    // }
    // end shop by supliery
    // start shop by view all supliery
    public function actionViewAllSupplier() {
        $categoryid = 0;
        if (isset($_POST['homecategoryid'])) {
            $categoryid = $_POST['homecategoryid'];
        }
        $pageno = 0;
        $limitid = 10;
        if (!empty($_POST['limit'])) {
            if ($_POST['limit'] > 1) {
                $pageno = $_POST['limit'] * $limitid;
            }
        }
        // print_r($categoryid);die;
        $json = [];
        $supplier_info = [];
        $viewallsuppliers = [];
        $allsupplier = User::find()
        // ->where(['supplier_type'=> $categoryid ])
        ->where(['is_supplier' => 'yes'])->offset($pageno)->limit($limitid)->orderBy(new Expression('CASE WHEN  availability = "online" THEN 0 WHEN availability = "offline" THEN 1 ELSE 2 END '))->all();
        if (!empty($allsupplier)) {
            foreach ($allsupplier as $value) {
                $image1 = '';
                if (!empty($value['profile_pic'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image1 = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $value['profile_pic'];
                }
                $supplier_info = ['id' => $value['id'], 'name' => $value['username'], 'name' => $value['username'], 'image' => $image1, 'availability' => $value['availability']];
                $viewallsuppliers[] = $supplier_info;
            }
        }
        $json['code'] = $this->statusCode;
        $json['suppliers'] = $viewallsuppliers;
        return $json;
    }
    // end shop by view all supliery
    // start home category list 3 dec 2019
    // 	public function actionHomecategory(){
    // 		$json = [];
    // 		 $brandsql ="SELECT tbl_home_category.home_category_id,tbl_home_category_description.home_category_name,tbl_home_category.image FROM tbl_home_category
    //            LEFT JOIN tbl_home_category_description ON tbl_home_category_description.home_category_id=tbl_home_category.home_category_id
    //            AND tbl_home_category_description.language_id=1
    //            GROUP BY tbl_home_category.home_category_id ";
    //        $homecategorylist = Yii::$app->db->createCommand($brandsql)->queryAll();
    // 		$json['code']	= $this->statusCode;
    // 		$json['homecategorylist']	= $homecategorylist;
    // 		return $json;
    // }
    // end home category  list 3 dec 2019
    // start home category list
    public function actionHomecategorydetail1() {
        $json = [];
        $homecategoryid = 0;
        if (isset($_GET['homecategoryid'])) {
            $homecategoryid = $_GET['homecategoryid'];
        }
        $brandsql = "SELECT tbl_home_category.home_category_id,tbl_home_category_description.home_category_name,tbl_home_category.image FROM tbl_home_category 
			            LEFT JOIN tbl_home_category_description ON tbl_home_category_description.home_category_id=tbl_home_category.home_category_id 
			            AND tbl_home_category_description.language_id=1
						WHERE tbl_home_category.home_category_id = $homecategoryid
			            GROUP BY tbl_home_category.home_category_id ";
        $homecategorylist = Yii::$app->db->createCommand($brandsql)->queryAll();
        $homecategorydetail = [];
        $homecategory_info = [];
        if (!empty($homecategorylist)) {
            foreach ($homecategorylist as $value) {
                $homecatid = $value['home_category_id'];
                $catdatasql = "SELECT tbl_category.category_id,tbl_category_description.category_name,tbl_category.image FROM tbl_category LEFT JOIN tbl_category_description ON tbl_category_description.category_id=tbl_category.category_id 
            			AND tbl_category_description.language_id=1 WHERE tbl_category.home_category_id = $homecatid";
                $homecategorydet = Yii::$app->db->createCommand($catdatasql)->queryAll();
                if (!empty($homecategorydet)) {
                    $homecategory_info1 = [];
                    foreach ($homecategorydet as $value1) {
                        $catid = $value1['category_id'];
                        $parentsql = "SELECT tbl_category.category_id,tbl_category_description.category_name,tbl_category.image FROM tbl_category LEFT JOIN tbl_category_description ON tbl_category_description.category_id=tbl_category.category_id 
            					AND tbl_category_description.language_id=1 WHERE tbl_category.parent_id = $catid";
                        $parentdet = Yii::$app->db->createCommand($parentsql)->queryAll();
                        $homecategory_info[] = ['categoryname' => $value1['category_name'], 'category_id' => $value1['category_id'], 'image' => $value1['image']];
                        if (!empty($parentdet)) {
                            $homecategory_info2 = [];
                            foreach ($parentdet as $value2) {
                                $catid1 = $value2['category_id'];
                                $parentsql1 = "SELECT tbl_category.category_id,tbl_category_description.category_name,tbl_category.image FROM tbl_category LEFT JOIN tbl_category_description ON tbl_category_description.category_id=tbl_category.category_id 
            							AND tbl_category_description.language_id=1 WHERE tbl_category.parent_id = $catid1";
                                $parentdet1 = Yii::$app->db->createCommand($parentsql1)->queryAll();
                                $homecategory_info[]['subcategory'] = ['categoryname' => $value2['category_name'], 'category_id' => $value2['category_id'], 'image' => $value2['image']];
                                if (!empty($parentdet1)) {
                                    $homecategory_info2 = [];
                                    foreach ($parentdet1 as $value3) {
                                        //				$catid1 = $value2['category_id'];
                                        //				$parentsql1 = "SELECT tbl_category.category_id,tbl_category_description.category_name,tbl_category.image FROM tbl_category LEFT JOIN tbl_category_description ON tbl_category_description.category_id=tbl_category.category_id
                                        //            AND tbl_category_description.language_id=1 WHERE tbl_category.parent_id = $catid1";
                                        //            	 $parentdet1 = Yii::$app->db->createCommand($parentsql1)->queryAll();
                                        $homecategory_info[]['childsubcategory'] = ['categoryname' => $value3['category_name'], 'category_id' => $value3['category_id'], 'image' => $value3['image']];
                                    }
                                }
                            }
                        }
                    }
                }
                //            	 $image = '';
                //
                //               if(!empty($branddata['image'])){
                //
                //            	  $path1 = Url::base();
                //                $path = str_replace("/api","",$path1);
                //                $image = "https://freshhomee.com".$path."/frontend/web/uploads/". $branddata['image'] ;
                //                }
                //                $brand_info = ['name'=> $value['brand_name'] ,'brand_id' => $value['brand_id'], 'image' => $image];
                //                $homecategorydetail[] = $homecategorydet;
                
            }
            //				   $homecategory_info[] = $homecategory_info1;
            
        }
        $json['code'] = $this->statusCode;
        $json['homecategorydetail'] = $homecategory_info;
        return $json;
    }
    // end home category list
    //   start brand
    public function actionBrandList() {
        $json = [];
        $categoryid = 0;
        if (isset($_GET['categoryid'])) {
            $categoryid = $_GET['categoryid'];
        }
        $brand_info = [];
        $branddetails = [];
        $brandsql = "SELECT tbl_brand_description.brand_id,tbl_brand_description.brand_name,tbl_brand_category.category_id FROM tbl_brand_category 
            LEFT JOIN tbl_brand_description ON tbl_brand_description.brand_id=tbl_brand_category.brand_id 
            AND tbl_brand_description.language_id=1 
            WHERE tbl_brand_category.category_id IN (SELECT category_id FROM tbl_category WHERE cat_id=$categoryid) 
            GROUP BY tbl_brand_category.brand_id ";
        $brandlist = Yii::$app->db->createCommand($brandsql)->queryAll();
        if (!empty($brandlist)) {
            foreach ($brandlist as $value) {
                $branddata = Brand::find()->where(['brand_id' => $value['brand_id']])->one();
                $image = '';
                if (!empty($branddata['image'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $branddata['image'];
                }
                $brand_info = ['name' => $value['brand_name'], 'brand_id' => $value['brand_id'], 'image' => $image];
                $branddetails[] = $brand_info;
            }
        }
        $json['code'] = $this->statusCode;
        $json['brand_list'] = $branddetails;
        return $json;
    }
    //  end brand
    // start all brand list
    public function actionAllBrandList12() {
        $json = [];
        $categoryid = 0;
        if (isset($_GET['categoryid'])) {
            $categoryid = $_GET['categoryid'];
        }
        $categories = Category::find()->where(['cat_id' => $categoryid])->andWhere(['active' => 1])
        // ->limit(10)
        ->all();
        $sub_category_info = [];
        $sub_category = [];
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $categorydiscription = CategoryDescription::find()->where(['category_id' => $category->category_id])->andWhere(['language_id' => 1])->one();
                $image = '';
                if (!empty($category['image'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $category['image'];
                }
                $sub_category_info = ['name' => $categorydiscription['category_name'], 'sub_category_id' => $category->category_id, 'image' => $image];
                $sub_category[] = $sub_category_info;
            }
        }
        $json['code'] = $this->statusCode;
        $json['all_brand_list'] = $sub_category;
        return $json;
    }
    public function actionAllBrandList() {
        $json = [];
        $pageno = 0;
        $limitid = 10;
        if (!empty($_POST['limit'])) {
            if ($_POST['limit'] > 1) {
                $pageno = $_POST['limit'] * $limitid;
            }
        }
        $homecategoryid = 0;
        if (isset($_POST['homecategoryid'])) {
            $homecategoryid = $_POST['homecategoryid'];
        }
        //         $categories = Category::find()
        //         ->where(['cat_id' => $categoryid ])
        //         ->andWhere(['active'=>1])
        //		 ->offset($pageno)
        //        ->limit($limitid)
        //         // ->limit(10)
        //         ->all();
        //         $sub_category_info = [];
        //         $sub_category = [];
        //        if(!empty($categories)){
        //            foreach ($categories as $category ){
        //
        //
        //
        //            	 $categorydiscription = CategoryDescription::find()->where(['category_id'=>$category->category_id])->andWhere(['language_id'=>1])->one();
        //            	 $image = '';
        //               if(!empty($category['image'])){
        //
        //            	  $path1 = Url::base();
        //                $path = str_replace("/api","",$path1);
        //                $image = "https://freshhomee.com".$path."/frontend/web/uploads/". $category['image'] ;
        //
        //                }
        //
        //                $sub_category_info = ['name'=> $categorydiscription['category_name'] ,'sub_category_id' => $category->category_id, 'image' => $image];
        //                $sub_category[] = $sub_category_info;
        //
        //            }
        //        }
        $brand_info = [];
        $branddetails = [];
        $brandsql = "SELECT tbl_brand_description.brand_id,tbl_brand_description.brand_name,tbl_brand_category.category_id FROM tbl_brand_category 
            LEFT JOIN tbl_brand_description ON tbl_brand_description.brand_id=tbl_brand_category.brand_id 
            AND tbl_brand_description.language_id=1 
            WHERE tbl_brand_category.category_id IN (SELECT category_id FROM tbl_category WHERE home_category_id=$homecategoryid)
			GROUP BY tbl_brand_category.brand_id LIMIT $limitid OFFSET $pageno ";
        $brandlist = Yii::$app->db->createCommand($brandsql)->queryAll();
        if (!empty($brandlist)) {
            foreach ($brandlist as $value) {
                $branddata = Brand::find()->where(['brand_id' => $value['brand_id']])->one();
                $image1 = '';
                if (!empty($branddata['image'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image1 = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $branddata['image'];
                }
                $brand_info = ['name' => $value['brand_name'], 'brand_id' => $value['brand_id'], 'image' => $image1];
                $branddetails[] = $brand_info;
            }
        }
        $json['code'] = $this->statusCode;
        $json['brand_list'] = $branddetails;
        //        $json['all_brand_list'] = $sub_category;
        return $json;
    }
    // end all brand list
    // start category details
    public function actionCategoryDetails() {
        $json = [];
        $homecategoryid = 0;
        if (isset($_GET['homecategoryid'])) {
            $homecategoryid = $_GET['homecategoryid'];
        }
        $categories = Category::find()->where(['home_category_id' => $homecategoryid])->andWhere(['active' => 1])->limit(10)->all();
        //  $sub_category_info = [];
        //  $sub_category = [];
        //  // start  brand data
        // if(!empty($categories)){
        //     foreach ($categories as $category ){
        //     	 $categorydiscription = CategoryDescription::find()->where(['category_id'=>$category->category_id])->andWhere(['language_id'=>1])->one();
        //     	 $image = '';
        //        if(!empty($category['image'])){
        //     	  $path1 = Url::base();
        //         $path = str_replace("/api","",$path1);
        //         $image = "https://freshhomee.com".$path."/frontend/web/uploads/". $category['image'] ;
        //         }
        //         $sub_category_info = ['name'=> $categorydiscription['category_name'] ,'sub_category_id' => $category->category_id, 'image' => $image];
        //         $sub_category[] = $sub_category_info;
        //     }
        // }
        // $json['brand_list'] = $sub_category;
        $brand_info = [];
        $branddetails = [];
        $brandsql = "SELECT tbl_brand_description.brand_id,tbl_brand_description.brand_name,tbl_brand_category.category_id FROM tbl_brand_category 
            LEFT JOIN tbl_brand_description ON tbl_brand_description.brand_id=tbl_brand_category.brand_id 
            AND tbl_brand_description.language_id=1 
            WHERE tbl_brand_category.category_id IN (SELECT category_id FROM tbl_category WHERE home_category_id=$homecategoryid) 
            GROUP BY tbl_brand_category.brand_id ";
        $brandlist = Yii::$app->db->createCommand($brandsql)->queryAll();
        if (!empty($brandlist)) {
            foreach ($brandlist as $value) {
                $branddata = Brand::find()->where(['brand_id' => $value['brand_id']])->one();
                $image1 = '';
                if (!empty($branddata['image'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image1 = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $branddata['image'];
                }
                $brand_info = ['name' => $value['brand_name'], 'brand_id' => $value['brand_id'], 'image' => $image1];
                $branddetails[] = $brand_info;
            }
        }
        $json['brand_list'] = $branddetails;
        // end  brand data
        // start  suppliery data
        $supplier_info = [];
        $suppliers = [];
        $supplier = User::find()
        // ->where(['supplier_type'=> $homecategoryid ])
        ->orderBy(new Expression('CASE WHEN  availability = "online" THEN 0 WHEN availability = "offline" THEN 1 ELSE 2
		END '))->limit(5)->all();
        if (!empty($supplier)) {
            foreach ($supplier as $value) {
                $image1 = '';
                if (!empty($value['profile_pic'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image1 = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $value['profile_pic'];
                }
                $supplier_info = ['id' => $value['id'], 'name' => $value['username'], 'name' => $value['username'], 'image' => $image1, 'availability' => $value['availability']];
                $suppliers[] = $supplier_info;
            }
        }
        $json['suppliers'] = $suppliers;
        // end  suppliery data
        // start sub category data
        $sub_category_info = [];
        $sub_category = [];
        $categories = Category::find()->where(['home_category_id' => $homecategoryid])->andWhere(['active' => 1])->andWhere(['parent_id' => null])->limit(10)->all();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $categorydiscription = CategoryDescription::find()->where(['category_id' => $category->category_id])->andWhere(['language_id' => 1])->one();
                $image = '';
                if (!empty($category['image'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $category['image'];
                }
                $sub_category_info = ['name' => $categorydiscription['category_name'], 'sub_category_id' => $category->category_id, 'image' => $image];
                $sub_category[] = $sub_category_info;
            }
        }
        $json['code'] = $this->statusCode;
        $json['sub_category'] = $sub_category;
        // end sub category data
        return $json;
    }
    // end category details
    // start product list
    public function actionProductList() {
        $pageno = 0;
        $limitid = 5;
        $supplier_id = 0;
        if (!empty($_GET['limit'])) {
            if ($_GET['limit'] > 1) {
                $pageno = $_GET['limit'] * $limitid;
            }
        }
        $screen_id = 0;
        if (!empty($_GET['screen_id'])) {
            $screen_id = $_GET['screen_id'];
        }
        if (!empty($_GET['supplier_id'])) {
            $supplier_id = $_GET['supplier_id'];
        }
        $json = [];
        $long = $long = '';
        $user_model = new User();
        $headers = getallheaders();
        $token = '';
        $top_selling_products = $online_kitchens = $user_address = $user_data = $latest_products = $order_now = $top_categories = [];
        if (isset($headers['Authorization'])) $token = str_replace("Bearer ", "", $headers['Authorization']);
        $user_info = $user_model->findIdentityByAccessToken($token);
        if (!empty($user_info)) {
            $user_data = $user_info->userDetail;
            $user_address = UserAddress::find()->where(['user_id' => $user_info->id, 'is_default' => 'yes'])->one();
            if (!empty($user_address)) {
                $lat = $user_address->latitude;
                $long = $user_address->longitude;
            }
        }
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance ,SUM(op.quantity) AS TotalQuantity "))->orderBy('distance');
        } else $query->select('tbl_user.*,tbl_menu.*,SUM(op.quantity) AS TotalQuantity');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['NOT IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->where(['tbl_user.supplier_type' => 1]);
        $query->andWhere(['tbl_menu.supplier_id' => $supplier_id]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy('TotalQuantity desc')
        // ->offset($pageno)
        // ->limit($limitid)
        ->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                if ($result->product_type == 1) $top_selling_products[$k] = $result->dishInfo;
                else $top_selling_products[$k] = $result->productInfo;
                $top_selling_products[$k]['total_quantity_sold'] = $result->TotalQuantity;
            }
        }
        $json['code'] = $this->statusCode;
        $json['ProductList'] = $top_selling_products;
        return $json;
    }
    // end product list
    // start subcategory
    public function actionSubcategoryList() {
        $json = [];
        $categoryid = 0;
        if (isset($_GET['categoryid'])) {
            $categoryid = $_GET['categoryid'];
        }
        $categories = Category::find()->where(['parent_id' => $categoryid])->andWhere(['active' => 1])
        // ->limit(10)
        ->all();
        $sub_category_info = [];
        $sub_category = [];
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $categorydiscription = CategoryDescription::find()->where(['category_id' => $category->category_id])->andWhere(['language_id' => 1])->one();
                $image = '';
                if (!empty($category['image'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $category['image'];
                }
                $sub_category_info = ['name' => $categorydiscription['category_name'], 'sub_category_id' => $category->category_id, 'image' => $image];
                $sub_category[] = $sub_category_info;
            }
        }
        $json['code'] = $this->statusCode;
        $json['subcategory-list'] = $sub_category;
        return $json;
    }
    // end subcategory
    // start hot deals
    public function actionHotDeals() {
        $pageno = 0;
        $limitid = 5;
        if (!empty($_GET['limit'])) {
            if ($_GET['limit'] > 1) {
                $pageno = $_GET['limit'] * $limitid;
            }
        }
        $screen_id = 0;
        if (!empty($_GET['screen_id'])) {
            $screen_id = $_GET['screen_id'];
        }
        $json = [];
        $long = $long = '';
        $user_model = new User();
        $headers = getallheaders();
        $token = '';
        $top_selling_products = $online_kitchens = $user_address = $user_data = $latest_products = $order_now = $top_categories = $hotdeals = [];
        if (isset($headers['Authorization'])) $token = str_replace("Bearer ", "", $headers['Authorization']);
        $user_info = $user_model->findIdentityByAccessToken($token);
        if (!empty($user_info)) {
            $user_data = $user_info->userDetail;
            $user_address = UserAddress::find()->where(['user_id' => $user_info->id, 'is_default' => 'yes'])->one();
            if (!empty($user_address)) {
                $lat = $user_address->latitude;
                $long = $user_address->longitude;
            }
        }
        // Start Latest products
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance "))->orderBy('distance');
        } else $query->select('tbl_menu.*');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.status' => 'active', 'tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['NOT IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->andWhere(['tbl_user.supplier_type' => 1]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy('tbl_menu.id DESC')
        // ->offset($pageno)
        ->limit(10)
        // ->limit(10)
        ->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                // echo "<pre>";
                // print_r($result->dishInfo);die;
                if ($result->product_type == 1) $latest_products[$k] = $result->dishInfo;
                else $latest_products[$k] = $result->productInfo;
            }
        }
        $product_info = [];
        $productdetails = [];
        foreach ($latest_products as $key => $value) {
            if ($value['discount'] > 0) {
                $product_info = ['name' => $value['product_name'], 'image' => $value['main_image'], 'product_id' => $value['product_id'], 'categories' => $value['categories'], 'discount' => $value['discount'], 'type_of_product' => $value['type_of_product'], 'rate_point' => $value['rate_point'], 'product_price' => $value['product_price']];
                $productdetails[] = $product_info;
            }
        }
        $json['code'] = $this->statusCode;
        $json['hot_deals_food'] = $productdetails;
        // Start Latest products
        //       $query = Menu::find()->joinWith('supplierInfo');
        //       if(!empty($lat) && !empty($long) ){
        //           $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance "))->orderBy('distance');
        //       }else
        //           $query->select('tbl_menu.*');
        //       $query->joinWith('orderProducts as op')
        //           ->where(['tbl_menu.status'=>'active','tbl_menu.image_approval'=>1,'tbl_menu.is_delete'=>0,'tbl_menu.test_data'=>0])->andWhere(['NOT IN','tbl_menu.product_type',1])
        //           ->andWhere(['>','subscription_end',time()]);
        //       if(!empty($user_info))
        //           $query->andWhere(['NOT IN','tbl_menu.supplier_id',$user_info->id]);
        //           $query->andWhere(['tbl_user.supplier_type'=> 2]);
        //       $query->groupBy('tbl_menu.id');
        //       $results = $query->orderBy('tbl_menu.id DESC')
        //       // ->offset($pageno)
        //       ->limit(10)
        //       // ->limit(10)
        //       ->all();
        //       if(!empty($results)){
        //           foreach($results as $k=>$result){
        //           	// echo "<pre>";
        //           	// print_r($result->dishInfo);die;
        //               if($result->product_type==1)
        //                   $latest_products[$k]	 = $result->dishInfo;
        //               else
        //                   $latest_products[$k] = $result->productInfo;
        //           }
        //       }
        //        foreach ($latest_products as $key => $value) {
        //          if($value['discount'] > 0){
        //         $hotdeals[$key]=  ['name'=>$value['product_name'],'image'=>$value['main_image'],'product_id'=>$value['product_id'],'categories'=>$value['categories'],'discount'=>$value['discount'],'type_of_product'=>$value['type_of_product'],'rate_point'=>$value['rate_point']];
        //          }
        //        }
        // $json['code']	= $this->statusCode;
        //       $json['hot_deals_products_shops'] = $hotdeals;
        $query = Menu::find()->joinWith('supplierInfo');
        if (!empty($lat) && !empty($long)) {
            $query->select(new Expression("tbl_menu.*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - tbl_user.latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(tbl_user.latitude * pi()/180) * POWER(SIN(( $long - tbl_user.longitude) * pi()/180 / 2), 2) ))) as distance "))->orderBy('distance');
        } else $query->select('tbl_menu.*');
        $query->joinWith('orderProducts as op')->where(['tbl_menu.status' => 'active', 'tbl_menu.image_approval' => 1, 'tbl_menu.is_delete' => 0, 'tbl_menu.test_data' => 0])->andWhere(['NOT IN', 'tbl_menu.product_type', 1])->andWhere(['>', 'subscription_end', time() ]);
        if (!empty($user_info)) $query->andWhere(['NOT IN', 'tbl_menu.supplier_id', $user_info->id]);
        $query->andWhere(['NOT IN', 'tbl_user.supplier_type', 1]);
        $query->groupBy('tbl_menu.id');
        $results = $query->orderBy('tbl_menu.id DESC')
        // ->offset($pageno)
        ->limit(10)
        // ->limit(10)
        ->all();
        if (!empty($results)) {
            foreach ($results as $k => $result) {
                // echo "<pre>";
                // print_r($result->dishInfo);die;
                if ($result->product_type == 1) $latest_products[$k] = $result->dishInfo;
                else $latest_products[$k] = $result->productInfo;
            }
        }
        $product_info = [];
        $productdetails = [];
        foreach ($latest_products as $key => $value) {
            if ($value['discount'] > 0) {
                $product_info = ['name' => $value['product_name'], 'image' => $value['main_image'], 'product_id' => $value['product_id'], 'categories' => $value['categories'], 'discount' => $value['discount'], 'type_of_product' => $value['type_of_product'], 'rate_point' => $value['rate_point'], 'product_price' => $value['product_price']];
                $productdetails[] = $product_info;
            }
        }
        $json['code'] = $this->statusCode;
        $json['hot_deals_product'] = $productdetails;
        return $json;
    }
    // end hot deals
    // start home category list
    public function actionHomecategory() {
        $main_category = 0;
        if (isset($_POST['main_category_id'])) {
            $main_category = $_POST['main_category_id'];
        }
        $data = [];
        $json = [];
        $subcategory_info = [];
        $subcategorydetails = [];
        $subcategory1 = [];
        $subcategory_info1 = [];
        $subcategorydetails1 = [];
        // $home_category_id['how']
        $brandsql = "SELECT tbl_home_category.home_category_id,tbl_home_category_description.home_category_name,tbl_home_category.image FROM tbl_home_category 
            LEFT JOIN tbl_home_category_description ON tbl_home_category_description.home_category_id=tbl_home_category.home_category_id 
            WHERE tbl_home_category_description.language_id=1
            AND FIND_IN_SET($main_category , tbl_home_category.main_category )
            GROUP BY tbl_home_category.home_category_id ";
        $homecategorylist = Yii::$app->db->createCommand($brandsql)->queryAll();
        $json['code'] = $this->statusCode;
        $json['homecategory'] = $homecategorylist;
        return $json;
    }
    // end home category  list
    // start brand list by sub category id
    public function actionBrandListBySubCategory() {
        $json = [];
        $categoryid = 0;
        if (isset($_GET['categoryid'])) {
            $categoryid = $_GET['categoryid'];
        }
        $brand_info = [];
        $branddetails = [];
        $brandsql = "SELECT tbl_brand_description.brand_id,tbl_brand_description.brand_name,tbl_brand_category.category_id FROM tbl_brand_category 
            LEFT JOIN tbl_brand_description ON tbl_brand_description.brand_id=tbl_brand_category.brand_id  
            AND tbl_brand_description.language_id=1 
            WHERE tbl_brand_category.category_id IN (SELECT category_id FROM tbl_category WHERE category_id IN ($categoryid))        
            GROUP BY tbl_brand_category.brand_id ";
        $brandlist = Yii::$app->db->createCommand($brandsql)->queryAll();
        if (!empty($brandlist)) {
            foreach ($brandlist as $value) {
                $branddata = Brand::find()->where(['brand_id' => $value['brand_id']])->one();
                $image = '';
                if (!empty($branddata['image'])) {
                    $path1 = Url::base();
                    $path = str_replace("/api", "", $path1);
                    $image = "https://freshhomee.com" . $path . "/frontend/web/uploads/" . $branddata['image'];
                }
                $brand_info = ['name' => $value['brand_name'], 'brand_id' => $value['brand_id'], 'image' => $image];
                $branddetails[] = $brand_info;
            }
        }
        $json['code'] = $this->statusCode;
        $json['brand_list'] = $branddetails;
        return $json;
    }
    // end brand list by sub category id
    // start home category details
    
    public function actionHomecategorydetail() {
        $json = [];
        $homecategoryid = 0;
    
        if (isset($_GET['homecategoryid'])) {
            $homecategoryid = $_GET['homecategoryid'];
        }
        $data = [];
        $subcategory_info = [];
        $subcategorydetails = [];
        $subcategory1 = [];
        $subcategory_info1 = [];
        $subcategorydetails1 = [];
        // $home_category_id['how']
    
        $brandsql = "SELECT tbl_home_category.home_category_id,tbl_home_category_description.home_category_name,tbl_home_category.image FROM tbl_home_category 
            LEFT JOIN tbl_home_category_description ON tbl_home_category_description.home_category_id=tbl_home_category.home_category_id 
            WHERE tbl_home_category_description.language_id=1
            AND tbl_home_category.home_category_id= $homecategoryid
            GROUP BY tbl_home_category.home_category_id ";
        $homecategorylist = Yii::$app->db->createCommand($brandsql)->queryAll();
        $json['code'] = $this->statusCode;
    
        foreach ($homecategorylist as $key => $value) {
            $home_category_id = $value['home_category_id'];

            $path1 = Url::base();
            $path = str_replace("/api", "", $path1);
            $baseurl = "https://freshhomee.com" . $path . "/frontend/web/uploads/";
            $image1 = $baseurl . $value['image'];
            $subcategory_info = ['cat_name' => $value['home_category_name'], 'cat_id' => $value['home_category_id'], 'image' => $image1];
            $subcategorydetails[] = $subcategory_info;
            
            $parentcategorysql1 = "SELECT tbl_category.category_id,tbl_category_description.category_name,tbl_category.image FROM tbl_category 
            LEFT JOIN tbl_category_description ON tbl_category_description.category_id=tbl_category.category_id 
            WHERE tbl_category.home_category_id = $home_category_id
            AND tbl_category.parent_id IS NULL    
            AND tbl_category_description.language_id=1";
            // GROUP BY tbl_category.category_id ";
            $parentcategory1 = Yii::$app->db->createCommand($parentcategorysql1)->queryAll();
            $data1 = [];
            
            foreach ($parentcategory1 as $key1 => $value1) {
                $parent_id = $value1['category_id'];
                $parentcategorysql2 = "SELECT tbl_category.category_id,tbl_category_description.category_name,tbl_category.image FROM tbl_category LEFT JOIN tbl_category_description ON tbl_category_description.category_id=tbl_category.category_id WHERE tbl_category.parent_id = $parent_id AND tbl_category_description.language_id=1";
                $parentcategory2 = Yii::$app->db->createCommand($parentcategorysql2)->queryAll();
                $data2 = [];
                
                foreach ($parentcategory2 as $key2 => $value2) {
                    $parent_id1 = $value2['category_id'];
                    $parentcategorysql3 = "SELECT tbl_category.category_id,tbl_category_description.category_name,tbl_category.image FROM tbl_category LEFT JOIN tbl_category_description ON tbl_category_description.category_id=tbl_category.category_id WHERE tbl_category.parent_id = $parent_id1 AND tbl_category_description.language_id=1";
                    $parentcategory3 = Yii::$app->db->createCommand($parentcategorysql3)->queryAll();
                    $data3 = [];
                    
                    if($parentcategory3) {
	                    foreach ($parentcategory3 as $key3 => $value3) {
	                        $parent_id2 = $value3['category_id'];
	                        $parentcategorysql4 = "SELECT tbl_category.category_id,tbl_category_description.category_name,tbl_category.image FROM tbl_category LEFT JOIN tbl_category_description ON tbl_category_description.category_id=tbl_category.category_id WHERE tbl_category.parent_id = $parent_id2 AND tbl_category_description.language_id=1";
	                        $parentcategory4 = Yii::$app->db->createCommand($parentcategorysql4)->queryAll();
	                        $data4 = [];

	                        if($parentcategory4) {
		                        foreach ($parentcategory4 as $key4 => $value4) {
		                            $parent_id3 = $value4['category_id'];
		                            $parentcategorysql5 = "SELECT tbl_category.category_id,tbl_category_description.category_name,tbl_category.image FROM tbl_category LEFT JOIN tbl_category_description ON tbl_category_description.category_id=tbl_category.category_id WHERE tbl_category.parent_id = $parent_id3 AND tbl_category_description.language_id=1";
		                            
		                            $parentcategory5 = Yii::$app->db->createCommand($parentcategorysql5)->queryAll();
		                            $data5 = [];

		                            if($parentcategory5) {
			                            foreach ($parentcategory5 as $key5 => $value5) {

			                                $image7 = $baseurl . $value5['image'];
			                                $data5[] = array('category_id' => $value5['category_id'], 'name' => $value5['category_name'], 'image' => $image7, 'sub_category6' => '');
			                            }
			                        }

		                            $image6 = $baseurl . $value4['image'];
		                            $data4[] = array('category_id' => $value4['category_id'], 'name' => $value4['category_name'], 'image' => $image6, 'sub_category5' => $data5);
		                        }
		                    }
	                        $image5 = $baseurl . $value3['image'];
	                        $data3[] = array('category_id' => $value3['category_id'], 'name' => $value3['category_name'], 'image' => $image5, 'sub_category4' => $data4);
	                    }
	                }
                    $image4 = $baseurl . $value2['image'];
                    $data2[] = array('category_id' => $value2['category_id'], 'name' => $value2['category_name'], 'image' => $image4, 'sub_category3' => $data3);
                }
                $image3 = $baseurl . $value1['image'];
                $data1[] = array('category_id' => $value1['category_id'], 'name' => $value1['category_name'], 'image' => $image3, 'sub_category2' => $data2);
            }
            $image2 = $baseurl . $value['image'];
            // $data[]=array('category_id'=>$value['home_category_id'],'name'=>$value['home_category_name'],'image'=>$image2,'sub_category1'=>$data1);
            $data[] = array('sub_category1' => $data1);
        }
        $json['code'] = $this->statusCode;
        $json['homecategorydetail'] = $data;
        return $json;
    }
    // end home category details
    // start national id
    public function actionNationalId() {
        $json = [];
        $user_info = Yii::$app->user->identity;
        // $user_subscription = $user_info->subscriptionData;
        // if($user_subscription['status']=='inactive'){
        //     $json['error']['msg']	= 'Please subscribe the plan.' ;
        //     $this->statusCode = 401;
        //     $json['code']		= $this->statusCode;
        //     return $json;
        // }
        $model = User::findOne($user_info->id);
        // $model->scenario = 'food_menu';
        $post_data['User'] = Yii::$app->request->post();
        $file_data = [];
        if (isset($_FILES['national_pic']) && !empty($_FILES['national_pic'])) {
            foreach ($_FILES['national_pic'] as $key => $pic_data) {
                $file_data[$key] = ['national_pic' => $pic_data];
            }
        }
        if (!empty($file_data)) {
            $_FILES['User'] = $file_data;
            // $model->supplier_id	= $user_info->id;
            // $model->dist_qty	= !empty($model->dist_qty) ? $model->dist_qty : 0;
            // $more_text			= '';
            if ($model->load($post_data) && $model->validate()) {
                $dish_image = UploadedFile::getInstance($model, 'national_pic');
                if (!empty($dish_image)) {
                    $library = new Library();
                    $model->national_pic = $library->saveFile($dish_image, 'nationalID');
                    $model->national_pic_approval = 1;
                }
                $model->save();
                $json['code'] = $this->statusCode;
                $json['success'] = 'National Id Upload successfully';
            } else {
                $json['error']['msg'] = 'Something wrong';
            }
            return $json;
        }
        // $model					= new User();
        // $post_data['User'] 		= Yii::$app->request->post();
        // echo "<pre>";
        // print_r($post_data);die;
        // $file_data				= [];
        //    echo "<pre>";
        //    print_r($_FILES['national_pic']);die;
        // if(isset($_FILES['national_pic']) && !empty($_FILES['national_pic'])){
        // 	foreach($_FILES['national_pic'] as $key=>$pic_data){
        // 		$file_data[$key]	= ['national_pic'=>$pic_data];
        // 	}
        // }
        //       if ($model->load($post_data) && $model->validate()) {
        //           $national_pic 		= UploadedFile::getInstance($model, 'national_pic');
        //           print_r($national_pic);die;
        //           if(!empty($national_pic)){
        //               $library   			 = new Library();
        //               $model->national_pic  = $library->saveFile($national_pic,'users');
        //               $model->national_pic_approval = 1;
        //           }
        //           $model->save();
        //           $json['data']       = $model->national_pic;
        //           $json['data']['access_token'] = $user_data->auth_key;
        //           // $json['data']['driver_id']    = $user_data->id;
        //       }else{
        //           $all_errors				= [];
        //           $errors					= $result->getErrors();
        //           foreach($errors as $key=>$error){
        //               $all_errors[]	= $error[0];
        //           }
        //           $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
        //           $this->statusCode = 401;
        //       }
        //       $json['code']		= $this->statusCode;
        //       return $json;
        
    }
    // end national id
    
}
