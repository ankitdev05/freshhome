<?php

namespace api\models;

use common\models\MenuToAttributeOptions;
use common\models\MenuToAttributes;
use common\models\Option;
use Yii;
use yii\base\Model;
use common\models\User;
use common\models\Menu;
use common\models\Library;
class Cart extends Model
{
    public $menu_id;
    public $quantity;
    public $product_attributes;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id', 'quantity'], 'required'],
            [['menu_id', 'quantity'], 'integer']
        ];
    }
    public function attributeLabels()
    {
        return [
            'menu_id' => 'Dish',
        ];
    }
    public function guestCartData($cart_type=''){
        $session = Yii::$app->session;
        $library = new Library();

        $cart_data = $library->json_decodeArray($session->get('guest_cart'));
        $output = [];
        $cart = [];
        $online_orders = [];
        $pre_orders = [];
        $total = 0;
        if(!empty($cart_data)){

            foreach($cart_data as $supplier_id=>$data){
                $supplier = User::findOne($supplier_id);
                if(!empty($supplier)){
                    $supplier_info 	= $supplier->userInfo;
                    $supplier_info_ = ['availability'=>$supplier_info['availability'],
                        'supplier_id'=>$supplier_id,'name'=>$supplier_info['name'],
                        'email'=>$supplier_info['email'],'username'=>$supplier_info['username'],
                        'phone_number'=>$supplier_info['phone_number'],
                        'supplier_type' => $supplier_info['supplier_type'],
                        'profile_pic'=>$supplier_info['profile_pic']];
                    $menus = [];
                    $pre_orders = [];
                    $online_orders = [];

                    foreach($data as $menu_id=>$dish){
                        $options = [];
                        $totals = [];
                        $product_attributes = $dish['product_attributes'] ?? [];
                        if(!empty($product_attributes)){
                            foreach ($product_attributes as $k=> $product_attribute) {
                                $language_id = language_id;
                                $pro_attr = MenuToAttributes::find()->where(['option_id' => $k ])->one();
                                if(!empty($pro_attr)){
                                    $pro_option = Option::find()->joinWith('optionDescription as od')->where(['tbl_option.option_id' => $k,'od.language_id' => $language_id])->asArray()->one();
                                    if(!empty($pro_option)){
                                        if(is_array($product_attribute)){
                                            foreach ($product_attribute as $item) {
                                                $menu_to_pro_options = MenuToAttributeOptions::find()->where(['menu_attr' => $pro_attr->menu_attr,'name' => $item])->one();
                                                if(!empty($menu_to_pro_options)){
                                                    $options[] = [
                                                        'name' => $pro_option['optionDescription']['name'],
                                                        'type' => $pro_option['type'],
                                                        'option_id' => $pro_attr->option_id,
                                                        'menu_attr' => $pro_attr->menu_attr,
                                                        'value' => $menu_to_pro_options->name,
                                                        'price' => $menu_to_pro_options->price,
                                                        'subtract' => $menu_to_pro_options->subtract,
                                                        'quantity' =>$menu_to_pro_options->quantity
                                                    ];

                                                    $totals[] = [
                                                        'subtract' => $menu_to_pro_options->subtract,
                                                        'price' =>$menu_to_pro_options->price
                                                    ];
                                                }

                                            }
                                        }else{
                                            $menu_to_pro_options = MenuToAttributeOptions::find()->where(['menu_attr' => $pro_attr->menu_attr,'name' => $product_attribute])->one();
                                            if(!empty($menu_to_pro_options)){
                                                $options[] = [
                                                    'name' => $pro_option['optionDescription']['name'],
                                                    'type' => $pro_option['type'],
                                                    'menu_attr' => $pro_attr->menu_attr,
                                                    'option_id' => $pro_attr->option_id,
                                                    'value' => $menu_to_pro_options->name,
                                                    'price' => $menu_to_pro_options->price,
                                                    'subtract' => $menu_to_pro_options->subtract,
                                                    'quantity' =>$menu_to_pro_options->quantity
                                                ];

                                                $totals[] = ['subtract' => $menu_to_pro_options->subtract,'price' =>$menu_to_pro_options->price];
                                            }
                                        }

                                    }
                                }


                            }
                        }
                        $option_prices = 0;
                        if(!empty($totals)){
                            foreach($totals as $total1){
                                if($total1['subtract'] == 0)
                                    $option_prices += $total1['price'];
                                if($total1['subtract'] == 1)
                                    $option_prices -= $total1['price'];
                            }
                        }

                        $dish_org = Menu::findOne($menu_id);



                        if($supplier_info['supplier_type']==1)
                            $dish_info 		= $dish_org->dishInfo;
                        else{
                            $dish_info 		= $dish_org->productInfo;
                            $dish_info['dish_price'] = $dish_info['product_price'];
                            $dish_info['dish_id'] = $dish_info['product_id'];
                            $dish_info['dish_name'] = $dish_info['product_name'];
                            $dish_info['dish_description'] = $dish_info['product_description'];
                            $dish_info['dish_image'] = $dish_info['main_image'];

                        }

                        $final_price = $dish_info['dish_price']+$option_prices;
                        $dish_info['dish_price'] = $final_price;

                        $total_value	= $dish['quantity']*$dish_info['dish_price'];
                        $menu_data = [
                            'product_type' => $supplier_info['supplier_type'],
                            'main_image'=>$dish_org->mainImage,
                            'menu_id'=>$dish_info['dish_id'],
                            'dish_name'=>$dish_info['dish_name'],
                            'dish_description'=>$dish_info['dish_description'],
                            'dish_price'=>$dish_info['dish_price'],
                            'currency'=>$dish_info['currency'],
                            'dish_image'=>$dish_info['dish_image'],
                            'status'=>$dish_info['status'],
                            'quantity'=>$dish['quantity'],
                            'total_price'=>Yii::$app->formatter->format($total_value,['decimal',2]),
                            'total_value'=>$total_value,
                            'options'=>$options,
                            'totals'=>$totals,
                        ];

                      
                        if($cart_type=='all'){
                            $menus[] 		= $menu_data;
                            $total += $dish['quantity']*$dish_info['dish_price'];
                        }
                        elseif($cart_type=='pre-order'){
                            if($supplier_info['supplier_type']==1 && $dish_info['status']=='inactive'){
                                $total += $dish['quantity']*$dish_info['dish_price'];
                                $menus[] 		= $menu_data;
                            }
                        }
                        elseif($dish_info['status']=='active'){
                            $menus[] 		= $menu_data;
                            $total += $dish['quantity']*$dish_info['dish_price'];
                        }


                    }

                    $cart['supplier_info']	= $supplier_info_;
                    $cart['supplier_info']['cart_data']	= $menus;

                }

            }

            $output['cart'] = $cart;
            $output['pre_orders'] = $pre_orders;
            $output['online_orders'] = $online_orders;
            $output['total_amounts'] = $this->getCartTotal($total);
            $output['supplier_total_amounts'] = $this->getSupplierCartTotal($total);
        }
        return $output;
    }

	public function cartData($user_id,$cart_type=''){

		$library			= new Library();
		$user_info			= User::findOne($user_id);
		$cart_data			= $library->json_decodeArray($user_info->cart);
		$output = [];
		$cart = [];
		$online_orders = [];
		$pre_orders = [];
		$total = 0;
		if(!empty($cart_data)){

			foreach($cart_data as $supplier_id=>$data){
				$supplier = User::findOne($supplier_id);
				if(!empty($supplier)){
                    $supplier_info 	= $supplier->cartUserInfo;
                    $supplier_info_ = ['availability'=>$supplier_info['availability'],
                        'supplier_id'=>$supplier_id,'name'=>$supplier_info['name'],
                        'email'=>$supplier_info['email'],'username'=>$supplier_info['username'],
                        'phone_number'=>$supplier_info['phone_number'],
                        'supplier_type' => $supplier_info['supplier_type'],
                        'profile_pic'=>$supplier_info['profile_pic']];
                    $menus = [];
                    $inactive_orders = [];
                    $pre_orders = [];
                    $online_orders = [];

                    foreach($data as $menu_id=>$dish){
                        $options = [];
                        $totals = [];
                        $product_attributes = $dish['product_attributes'] ?? [];
                        if(!empty($product_attributes)){
                            foreach ($product_attributes as $k=> $product_attribute) {
                                $language_id = language_id;
                                $pro_attr = MenuToAttributes::find()->where(['option_id' => $k ])->one();
                                if(!empty($pro_attr)){
                                    $pro_option = Option::find()->joinWith('optionDescription as od')->where(['tbl_option.option_id' => $k,'od.language_id' => $language_id])->asArray()->one();
                                    if(!empty($pro_option)){
                                        if(is_array($product_attribute)){
                                            foreach ($product_attribute as $item) {
                                                $menu_to_pro_options = MenuToAttributeOptions::find()->where(['menu_attr' => $pro_attr->menu_attr,'name' => $item])->one();
                                                if(!empty($menu_to_pro_options)){
                                                    $options[] = [
                                                        'name' => $pro_option['optionDescription']['name'],
                                                        'type' => $pro_option['type'],
                                                        'option_id' => $pro_attr->option_id,
                                                        'menu_attr' => $pro_attr->menu_attr,
                                                        'value' => $menu_to_pro_options->name,
                                                        'price' => $menu_to_pro_options->price,
                                                        'subtract' => $menu_to_pro_options->subtract,
                                                        'quantity' =>$menu_to_pro_options->quantity
                                                    ];

                                                    $totals[] = [
                                                        'subtract' => $menu_to_pro_options->subtract,
                                                        'price' =>$menu_to_pro_options->price
                                                    ];
                                                }

                                            }
                                        }else{
                                            $menu_to_pro_options = MenuToAttributeOptions::find()->where(['menu_attr' => $pro_attr->menu_attr,'name' => $product_attribute])->one();
                                            if(!empty($menu_to_pro_options)){
                                                $options[] = [
                                                    'name' => $pro_option['optionDescription']['name'],
                                                    'type' => $pro_option['type'],
                                                    'menu_attr' => $pro_attr->menu_attr,
                                                    'option_id' => $pro_attr->option_id,
                                                    'value' => $menu_to_pro_options->name,
                                                    'price' => $menu_to_pro_options->price,
                                                    'subtract' => $menu_to_pro_options->subtract,
                                                    'quantity' =>$menu_to_pro_options->quantity
                                                ];

                                                $totals[] = ['subtract' => $menu_to_pro_options->subtract,'price' =>$menu_to_pro_options->price];
                                            }
                                        }

                                    }
                                }


                            }
                        }
                        $option_prices = 0;
                        if(!empty($totals)){
                            foreach($totals as $total1){
                                if($total1['subtract'] == 0)
                                    $option_prices += $total1['price'];
                                if($total1['subtract'] == 1)
                                    $option_prices -= $total1['price'];
                            }
                        }

                        $dish_org = Menu::findOne($menu_id);
                        if($supplier_info['supplier_type']==1)
                            $dish_info 		= $dish_org->dishInfo1;
                        else{
                            $dish_info 		= $dish_org->productInfo1;
                            $dish_info['dish_price'] = $dish_info['product_price'];
                            $dish_info['dish_id'] = $dish_info['product_id'];
                            $dish_info['dish_name'] = $dish_info['product_name'];
                            $dish_info['dish_description'] = $dish_info['product_description'];
                            $dish_info['dish_image'] = $dish_info['main_image'];

                        }


                        $final_price = $dish_info['dish_price']+$option_prices;
                        $dish_info['dish_price'] = $final_price;

                        $total_value	= $dish['quantity']*$dish_info['dish_price'];
                        $menu_data = [
                            'product_type' => $supplier_info['supplier_type'],
                            'main_image'=>$dish_org->mainImage,
                            'menu_id'=>$dish_info['menu_id'],
                            'dish_name'=>$dish_info['dish_name'],
                            'dish_description'=>$dish_info['dish_description'],
                            'dish_price'=>$dish_info['dish_price'],
                            'currency'=>$dish_info['currency'],
                            'dish_image'=>$dish_info['dish_image'],
                            'status'=>$dish_info['status'],
                            'quantity'=>$dish['quantity'],
                            'dist_qty'=>$dish_info['dist_qty'],
                            'total_price'=>Yii::$app->formatter->format($total_value,['decimal',2]),
                            'total_value'=>$total_value,
                            'options'=>$options,
                            'totals'=>$totals,
                        ];
                        if($cart_type=='all'){
                            $menus[] 		= $menu_data;
                            $total += $dish['quantity']*$dish_info['dish_price'];
                        }
                        elseif($cart_type=='pre-order'){
                            if($supplier_info['supplier_type']==1 && $dish_info['status']=='inactive'){
                                $total += $dish['quantity']*$dish_info['dish_price'];
                                $menus[] 		= $menu_data;
                            }
                        }
                        elseif($dish_info['status']=='active'){
                            $menus[] 		= $menu_data;
                            $total += $dish['quantity']*$dish_info['dish_price'];
                        }

                        if($dish_info['product_type']!=1 && $dish_info['status']!='active' && empty($cart_type)){
                            $inactive_orders[] = $menu_data;
                        }


                    }

                    $cart['supplier_info']	= $supplier_info_;
                    $cart['supplier_info']['cart_data']	= $menus;
                    $cart['supplier_info']['inactive_orders']	= $inactive_orders;
                }

			}
			$output['cart'] = $cart;
			$output['pre_orders'] = $pre_orders;
			$output['online_orders'] = $online_orders;
			$output['total_amounts'] = $this->getCartTotal($total);
			$output['final_amount'] = $this->getCartTotal($total,true);
			$output['supplier_total_amounts'] = $this->getSupplierCartTotal($total);
		}
		return $output;
	}
    public function getSupplierCartTotal($total){
        $freshhommee_fee = config_freshhommee_fee;
        $transaction_fee_ = config_transaction_fee;
        $country_data = country_data;
        $vat		 = config_vat;
        $currency	 = config_default_currency;
        $sub_total	= Yii::$app->formatter->format($total,['decimal',2]);

        $fee = 0;

        if(($freshhommee_fee) && !empty($freshhommee_fee)){
            $fee	= -($total*$freshhommee_fee)/100;
            $fee_value	= Yii::$app->formatter->format($fee,['decimal',2]);
        }

        $transaction_fee = 0;
        if(($transaction_fee_) && !empty($transaction_fee_)){
            $transaction_fee	= -(($total*$transaction_fee_)/100)+0.30;
            $trans_fee_value	= Yii::$app->formatter->format($transaction_fee,['decimal',2]);
        }

        $output[] = ['code'=>'sub_total','label'=>'Sub Total','total'=>$sub_total,'currency'=>$currency,'total_value'=>$total];
        if(!empty($fee)){
            $output[] = ['code'=>'freshhomee_fee','label'=>'Freshhomee Fee '.$freshhommee_fee.'%','total'=>$fee_value,'currency'=>$currency,'total_value'=>$fee];
        }

        if(!empty($transaction_fee)){
            $output[] = ['code'=>'transaction_fee','label'=>'Transaction Fee '.config_transaction_fee.'%','total'=>$trans_fee_value,'currency'=>$currency,'total_value'=>$transaction_fee];
        }
        $total_amount = $total+$transaction_fee+$fee;
        $total_price = Yii::$app->formatter->format($total_amount,['decimal',2]);

        $output[] = ['code'=>'total_earning','label'=>'Total earning','total'=>$total_price,'currency'=>$currency,'total_value'=>$total_amount];

        return $output;

    }
	public function getCartTotal($total,$final_amount=false){
        $country_data = country_data;
		$vat		 = config_vat;
		$currency	 = config_default_currency;
		$sub_total	= Yii::$app->formatter->format($total,['decimal',2]);
		
		$vat_amount	= 0;

        if(!empty($country_data["tax"])){
            $vat_amount	= ($total*$country_data["tax"])/100;
            $vat_price	= Yii::$app->formatter->format($vat_amount,['decimal',2]);
        }

        $fee = 0;

        if((config_freshhommee_fee) && !empty(config_freshhommee_fee)){
           // $fee	= ($total*config_freshhommee_fee)/100;
           // $fee_value	= Yii::$app->formatter->format($fee,['decimal',2]);
        }

        $transaction_fee = 0;
        if((config_transaction_fee) && !empty(config_transaction_fee)){
            //$transaction_fee	= (($total*config_transaction_fee)/100)+0.30;
            //$trans_fee_value	= Yii::$app->formatter->format($transaction_fee,['decimal',2]);
        }

		$delivery_charges = 4.68;
        if(!empty($country_data)){
           if(isset($country_data['delivery_charges']))
               $delivery_charges = $country_data['delivery_charges'];
        }
		
		$total_amount = $total+$vat_amount+$delivery_charges+$transaction_fee+$fee;
        if($final_amount==true)
            return number_format($total_amount, 2, '.', '');

		$total_price = Yii::$app->formatter->format($total_amount,['decimal',2]);
		
		$output[] = ['code'=>'sub_total','label'=>'Sub Total','total'=>$sub_total,'currency'=>$currency,'total_value'=>$total];

        if(!empty($country_data["tax"])){
            $output[] = ['code'=>'vat','label'=>$country_data['tax_name'].' '.$country_data["tax"].'%','total'=>$vat_price,'currency'=>$currency,'total_value'=>$vat_amount];
        }

        if(!empty($fee)){
            //$output[] = ['code'=>'freshhomee_fee','label'=>'Freshhomee Fee'.config_freshhommee_fee.'%','total'=>$fee_value,'currency'=>$currency,'total_value'=>$fee];
        }

        if(!empty($transaction_fee)){
            //$output[] = ['code'=>'transaction_fee','label'=>'Transaction Fee'.config_transaction_fee.'%','total'=>$trans_fee_value,'currency'=>$currency,'total_value'=>$transaction_fee];
        }

		$output[] = ['code'=>'delivery_charges','label'=>'Delivery Charges','total'=>$delivery_charges,'currency'=>$currency,'total_value'=>$delivery_charges];

        if (!Yii::$app->user->isGuest){
            $user_info = yii::$app->user->identity;
            $balance = $user_info->balance;
            //$balance = 0;
            if($balance > 0){
                if($balance>$total_amount){

                    $wallet_value	= Yii::$app->formatter->format($total_amount,['decimal',2]);
                    $output[] = ['code'=>'wallet','label'=> 'Wallet amount','total'=>'-'.$wallet_value,'currency'=>$currency,'total_value'=>-$total_amount];
                    $total_amount = 0;
                    $total_price = Yii::$app->formatter->format($total_amount,['decimal',2]);
                }else{
                    $wallet_value	= Yii::$app->formatter->format($balance,['decimal',2]);
                    $output[] = ['code'=>'wallet','label'=> 'Wallet amount','total'=>'-'.$wallet_value,'currency'=>$currency,'total_value'=>-$balance];
                    $total_amount = $total_amount-$balance;
                    $total_price = Yii::$app->formatter->format($total_amount,['decimal',2]);
                }
            }

        }
		$output[] = ['code'=>'total','label'=>'Total','total'=>$total_price,'currency'=>$currency,'total_value'=>$total_amount];

		return $output;
	}
    public function getTotalCartItems($user_id){
        $library			= new Library();
        $user_info			= User::findOne($user_id);
        $cart_data			= $library->json_decodeArray($user_info->cart);

        $output = [];
        $total = 0;
        $price = 0;
        if(!empty($cart_data)){
            foreach($cart_data as $supplier_id=>$data){

                $total = count($data);
                $output['total_items'] = $total;
                foreach($data as $menu_id=>$dish){
                    $dish_info 		= Menu::findOne($menu_id) ?? [];
                    if(!empty($dish_info))
                        $price += $dish['quantity']*$dish_info->dish_price;
                }
                $output['total_price']['total'] = Yii::$app->formatter->format($price,['decimal',2]);
                $output['total_price']['currency'] = config_default_currency;
            }
        }
        return $output;
    }

    public function getGuestTotalCartItems(){
        $session = Yii::$app->session;
        $library = new Library();
        $cart_data = $library->json_decodeArray($session->get('guest_cart'));
        $output = [];
        $total = 0;
        $price = 0;
        if(!empty($cart_data)){
            foreach($cart_data as $supplier_id=>$data){

                $total = count($data);
                $output['total_items'] = $total;
                foreach($data as $menu_id=>$dish){
                    $dish_info 		= Menu::findOne($menu_id)->dishInfo ?? [];
                    if(!empty($dish_info))
                        $price += $dish['quantity']*$dish_info['dish_price'];
                }
                $output['total_price']['total'] = Yii::$app->formatter->format($price,['decimal',2]);
                $output['total_price']['currency'] = config_default_currency;
            }
        }
        return $output;
    }

}
