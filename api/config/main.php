<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

use \yii\web\Request;
use common\models\Settings;
use common\models\Countries;
use kartik\mpdf\Pdf;
$baseUrl = str_replace('/api/web', '/api', (new Request)->getBaseUrl());
return [
    'id' => 'app-frontend',
    'name' => 'Fresh Home',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'api\controllers',
	 'on beforeAction' => function ($event) {
        $country = "United Arab Emirates";
        $country = Countries::find()->where(['name'=>$country])->asArray()->one();
        @define("config_default_currency",$country['currency_code']);
        define("country_data",$country);
        $Settings	= Settings::find()->all();
        foreach($Settings as $Setting)
            @define($Setting->settings_key,$Setting->settings_value);
         $language_id	= 1;
         define("language_id",$language_id);
	},
    'components' => [
        'pdf' => [
            'class' => Pdf::classname(),
            'format' => Pdf::FORMAT_A3,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
        ],
        'mobileDetect' => [
            'class' => '\skeeks\yii2\mobiledetect\MobileDetect'
        ],
        'request' => [
            'csrfParam' => '_csrf-api',
			'baseUrl' => $baseUrl,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
			
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
			'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
				
				'contact' 			=> 'site/contact',
				'getCountry' 		=> 'site/getcountry',
                'refer'             => 'site/refer',
                'askforhelp'        => 'site/askforhelp',
                'cancel-reasons'    => 'site/cancelreasons',
                'banners' => 'site/banners',
                'reset' => 'site/reset',

				'addMenu' 			=> 'menu/addmenu',
				'viewDish/<id:\d+>' => 'menu/viewdish',
				'findItem' 			=> 'menu/finditem',
				'editDish' 			=> 'menu/editdish',
				'menuList' 			=> 'menu/menulist',
				'menulisting' 		=> 'menu/menulisting',
				'getFilter' 		=> 'menu/getfilter',
				'getCuisine' 		=> 'menu/getcuisine',
				'searchItem' 		=> 'menu/searchitem',
				'getCategory' 		=> 'menu/getcategory',
				'getmenuschedule'	=> 'menu/getmenuschedule',
				'schedulelisting'	=> 'menu/schedulelisting',
				'addmenuschedule'	=> 'menu/addmenuschedule',
				'updatemenuschedule'=> 'menu/updatemenuschedule',
                'deleteschedule'       =>'menu/deleteschedule',

				'login' 			=> 'user/login',
				'profile' 			=> 'user/profile',
				'register' 			=> 'user/register',
				'gmailLogin' 		=> 'user/gmaillogin',
				'checkNumber' 		=> 'user/checknumber',
				'verify-account' 	=> 'user/verifyaccount',
				'updateProfile' 	=> 'user/updateprofile',
				'updateUserProfile'	=> 'user/updateuserprofile',
				'changePassword' 	=> 'user/changepassword',
				'favSupplier' 		=> 'user/favsupplier',
				'favMenu' 			=> 'user/favmenu',
				'myfav' 			=> 'user/myfav',
				'userAddress'		=> 'user/address',
				'supplierinfo'		=> 'user/supplierinfo',
				'user/order/<order_id:\d+>' => 'user/order',
				'user/invoice/<order_id:\d+>' => 'user/invoice',

				[
					'class' => 'yii\rest\UrlRule',
					'pluralize' => false,
					'controller' => 'user',
					'extraPatterns' => [
						'POST checknumber' => 'checknumber',
					],
				],
				[
					'class' => 'yii\rest\UrlRule',
					'pluralize' => false,
					'controller' => ['site'],
				],
            ],
        ],
    ],
    'params' => $params,
];
