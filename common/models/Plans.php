<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%plans}}".
 *
 * @property int $plan_id
 * @property string $plan_name
 * @property string $plan_description
 * @property int $months
 * @property string $price
 * @property int $created_at
 * @property int $updated_at
 */
class Plans extends \yii\db\ActiveRecord
{
    public $select_plan;
    public $select_card;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%plans}}';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['select_plan', 'select_card'], 'required','on' => 'subscription'],
            [['select_plan', 'select_card'], 'safe'],
            [['plan_name', 'plan_description', 'months', 'price','type','btree_plan_id'], 'required'],
            [['plan_name', 'plan_description', 'months', 'price','type','btree_plan_id'], 'trim'],
            [['plan_description','status'], 'string'],
            [['months', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            [['plan_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => Yii::t('app', 'Plan ID'),
            'plan_name' => Yii::t('app', 'Plan Name'),
            'plan_description' => Yii::t('app', 'Plan Description'),
            'months' => Yii::t('app', 'Months'),
            'price' => Yii::t('app', 'Price'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'btree_plan_id' => Yii::t('app', 'Braintree Plan ID'),
        ];
    }
}
