<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%sales_person}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $full_name
 * @property string $gender
 * @property int $city
 * @property string $scan_code
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class SalesPerson extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sales_person}}';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'full_name', 'gender', 'city','dob'], 'required'],
            [['user_id', 'city', 'created_at', 'updated_at'], 'integer'],
            [['gender','image_url'], 'string'],
            [['full_name', 'scan_code'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'full_name' => Yii::t('app', 'Full Name'),
            'gender' => Yii::t('app', 'Gender'),
            'city' => Yii::t('app', 'City'),
            'scan_code' => Yii::t('app', 'Scan Code'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'dob' => Yii::t('app', 'Date of birth'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
