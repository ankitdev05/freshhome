<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%cuisine_description}}".
 *
 * @property int $id
 * @property int $cuisine_id
 * @property int $language_id
 * @property string $cuisine_name
 *
 * @property Cuisine $cuisine
 * @property Language $language
 */
class BrandDescription extends \yii\db\ActiveRecord
{

   
 
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%brand_description}}';
    }
      public function behaviors()
    {
        return [ 
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id', 'language_id'], 'required'],
            [['brand_id', 'language_id'], 'integer'], 
            [['brand_name'], 'string', 'max' => 255],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cuisine::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'language_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'brand_id' => Yii::t('app', 'Brand ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'brand_name' => Yii::t('app', 'Brand Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand() 
    { 
        return $this->hasOne(brand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['language_id' => 'language_id']);
    }
}
