<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\behaviors\TimestampBehavior;
use himiklab\thumbnail\EasyThumbnailImage;
/**
 * This is the model class for table "{{%menu_schedules}}".
 *
 * @property int $id
 * @property int $supplier_id
 * @property string $schedule_date
 * @property string $start_time
 * @property string $end_time
 * @property string $choices
 * @property int $created_at
 * @property int $updated_at
 *
 * @property MenuScheduleItems[] $menuScheduleItems
 * @property User $supplier
 */
class MenuSchedules extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_schedules}}';
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplier_id', 'schedule_date', 'start_time', 'end_time', 'choices'], 'required'],
            [['supplier_id', 'created_at', 'updated_at','end_timestamp','start_timestamp'], 'integer'],
            [['schedule_date', 'start_time', 'end_time'], 'safe'],
            [['choices'], 'string'],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['supplier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'supplier_id' => Yii::t('app', 'Supplier ID'),
            'schedule_date' => Yii::t('app', 'Schedule Date'),
            'start_time' => Yii::t('app', 'Start Time'),
            'end_time' => Yii::t('app', 'End Time'),
            'choices' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    public function getMenuScheduleItem()
    {
        return $this->hasOne(MenuScheduleItems::className(), ['menuschedule_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuScheduleItems()
    {
        return $this->hasMany(MenuScheduleItems::className(), ['menuschedule_id' => 'id']);
    }
    public function getCountMenuScheduleItems()
    {
        return $this->hasMany(MenuScheduleItems::className(), ['menuschedule_id' => 'id'])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(User::className(), ['id' => 'supplier_id']);
    }
	public function getMenuitems()
    {
        $menu_items = $this->menuScheduleItems;
		if(!empty($menu_items)){
			$items = [];
			foreach($menu_items as $menu_item){
				$image = '';
				if(!empty($menu_item->menu->dish_image)){
					$image = EasyThumbnailImage::thumbnailFileUrl(
						"../../frontend/web/uploads/".$menu_item->menu->dish_image,100,100,EasyThumbnailImage::THUMBNAIL_INSET
					);
					$image = Html::img($image, ['class'=>'file-preview-image']);
				}
				$items[] = Html::a($image.' '.$menu_item->menu->dish_name,Yii::$app->request->baseUrl.'/menu/view?id='.$menu_item->menu->id.'&s_id='.$this->supplier_id,['target'=>'_blank','data-pjax'=>'0']);
			}
			return implode("<br/>",$items);
		}
		return false;
    }
    public function beforeSave($insert){
        $this->start_timestamp = strtotime($this->schedule_date.' '.$this->start_time);
        $this->end_timestamp = strtotime($this->schedule_date.' '.$this->end_time);
        return parent::beforeSave($insert);
    }
}
