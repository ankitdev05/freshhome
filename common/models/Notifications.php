<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%notifications}}".
 *
 * @property int $notification_id
 * @property int $user_id
 * @property string $type
 * @property string $config_name
 * @property string $config_value
 * @property int $is_read
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%notifications}}';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'config_name', 'config_value'], 'required'],
            [['user_id', 'from_user', 'is_read', 'created_at', 'updated_at','role','status','request_type'], 'integer'],
            [['type', 'config_name'], 'string', 'max' => 255],
            [['config_value','reason','request_id','request_status'],  'safe' ],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'notification_id' => Yii::t('app', 'Notofication ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'type' => Yii::t('app', 'Type'),
            'config_name' => Yii::t('app', 'Config Name'),
            'config_value' => Yii::t('app', 'Config Value'),
            'is_read' => Yii::t('app', 'Is Read'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getNormalNotification($role){

        $data['notification_id'] = $this->notification_id;
        $data['type'] = $this->type;
        $data['role'] = $this->role;
        $data['user_type'] = $this->role;

        $data['information'] = json_decode($this->config_value,true);
        if($this->type=='help_request'){
            $help_status = Notifications::find()->where(['from_user'=>1,'request_id'=>$data['information']['request_id']])->one();
            if(!empty($help_status))
                $data['request_status'] = $help_status->request_status;
            $help_rating = 0;
            $rating = SalesPersonRating::find()->where(['request_id'=>$data['information']['request_id']])->asArray()->one();
            if(!empty($rating))
                $help_rating = $rating['rating'];
            $data['rating'] = $help_rating;
        }


        $data['is_read'] = ($this->is_read);
        $data['time'] = Yii::$app->formatter->format($this->created_at, 'relativeTime');
        if($this->type=='order'){
            $order = Order::findOne($data['information']['order_id']);
            if(!empty($order)){
                if($role==4)
                    $order_info = $order->driverOrderInfo;
                else
                    $order_info = $order->orderInfo;
                $data['information']['order_status'] = $order_info['order_status'];
                $data['information']['address']= $order_info['user_info']['delivery_address'];
                $data['information']['order_items']= $order_info['order_items'];
                $data['information']['order_total']= $order_info['order_total'];
                $data['information']['order_ratings']= $order_info['order_ratings'];
            }else
                return [];
        }
        if($this->type=='order_accept' || $this->type=='order_reject'){
            $order = Order::findOne($data['information']['order_id']);
            if(!empty($order)){
                $order_info = $order->orderInfo;
                $data['information']['order_status'] = $order_info['order_status'];
                $data['information']['address']= $order_info['user_info']['delivery_address'];
                $data['information']['order_items']= $order_info['order_items'];
                $data['information']['order_total']= $order_info['order_total'];
                $data['information']['order_ratings']= $order_info['order_ratings'];
            }
        }
        if($this->type=='order_rating'){
            $order_rating = OrderRatings::find()->where(['order_id'=>$data['information']['order_id']])->one();
            if(!empty($order_rating)){
                $data['information']['taste_rating'] = $order_rating->taste_rating;
                $data['information']['presentation_rating'] = $order_rating->presentation_rating;
                $data['information']['packing_rating'] = $order_rating->packing_rating;
                $data['information']['overall_rating'] = $order_rating->overall_rating;
                $data['information']['review'] = $order_rating->review;
            }
        }
        if($this->type=='dish_rating'){
            $dish_rating = MenuRating::find()->where(['order_id'=>$data['information']['order_id']])->one();
            if(!empty($dish_rating)){
                $data['information']['rating'] = $dish_rating->rating;
                $data['information']['reviews'] = $dish_rating->reviews;
                $data['information']['menu_id'] = $dish_rating->menu_id;
                $data['information']['supplier_id'] = $dish_rating->supplier_id;
            }
        }
        if($this->type=='order'){

        }
        return $data;
    }
    public function getSalesnotifications(){
        $data['notification_id'] = $this->notification_id;
        $data['user_type'] = $this->role;
        $data['type'] = $this->type;
        $data['role'] = $this->role;
        $data['request_type'] = $this->request_type;
        $data['request_status'] = $this->request_status;
        $data['information'] = json_decode($this->config_value,true);
        $data['is_read'] = ($this->is_read);
        $data['time'] = Yii::$app->formatter->format($this->created_at, 'relativeTime');
        $help_rating = 0;
        $rating = SalesPersonRating::find()->where(['request_id'=>$data['information']['request_id']])->asArray()->one();
        if(!empty($rating))
            $help_rating = $rating['rating'];
        $data['rating'] = $help_rating;
        if(!empty($this->request_id)){
            $help_request = HelpRequests::findOne($this->request_id);
            if(!empty($help_request)){
                $data['information']['name'] = $help_request->name;
                $data['information']['location'] = $help_request->location;
                $data['information']['phonenumber'] = $help_request->phonenumber;
                $data['information']['email'] = $help_request->email;
                $data['information']['lat'] = $help_request->lat;
                $data['information']['lng'] = $help_request->lng;
            }
        }
        return $data;
    }
}
