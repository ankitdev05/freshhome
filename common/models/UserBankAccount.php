<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%user_bank_account}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email_address
 * @property string $bank_name
 * @property string $account_number
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class UserBankAccount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_bank_account}}';
    }
    public function behaviors(){
        return [
            TimestampBehavior::className()
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'first_name', 'last_name', 'email_address', 'bank_name', 'account_number','iban'], 'required'],
            [['email_address'], 'email'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'last_name', 'email_address', 'bank_name', 'account_number','iban'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email_address' => Yii::t('app', 'Email Address'),
            'bank_name' => Yii::t('app', 'Bank Name'),
            'account_number' => Yii::t('app', 'Account Number'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'iban' => Yii::t('app', 'IBAN Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
