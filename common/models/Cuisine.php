<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%cuisine}}".
 *
 * @property int $id
 * @property string $cuisine_name
 * @property string $status
 * @property string $image
 * @property int $created_at
 * @property int $updated_at
 */
class Cuisine extends \yii\db\ActiveRecord
{
	public $cuisine_name;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cuisine}}';
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cuisine_name', 'status'], 'required'],
            [['cuisine_name'], 'trim'],
            [['status','image'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['cuisine_name'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cuisine_name' => Yii::t('app', 'Cuisine Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
	public function getCuisineDescription()
    {
        return $this->hasMany(CuisineDescription::className(), ['cuisine_id' => 'id']);
    }
	
	public function getCuisineName()
    {
        return $this->hasOne(CuisineDescription::className(), ['cuisine_id' => 'id']);
    }
}
