<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%country}}".
 *
 * @property string $countrycode
 * @property string $countryname
 * @property string $code
 * @property string $nationality
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countrycode', 'countryname'], 'required'],
            [['countrycode'], 'string', 'max' => 3],
            [['countryname', 'nationality'], 'string', 'max' => 200],
            [['code'], 'string', 'max' => 2],
            [['countrycode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'countrycode' => Yii::t('app', 'Countrycode'),
            'countryname' => Yii::t('app', 'Countryname'),
            'code' => Yii::t('app', 'Code'),
            'nationality' => Yii::t('app', 'Nationality'),
        ];
    }
}
