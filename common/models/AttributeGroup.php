<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%attribute_group}}".
 *
 * @property int $attribute_id
 * @property string $name
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class AttributeGroup extends \yii\db\ActiveRecord
{
    public $category;
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%attribute_group}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['name', 'status'], 'trim'],
            [['category'], 'safe'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'attribute_id' => Yii::t('app', 'Attribute ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getCategories()
    {
        return $this->hasMany(AttributeGroupCategory::className(), ['attribute_id' => 'attribute_id']);
    }
}
