<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%menu_cuisines}}".
 *
 * @property int $id
 * @property int $menu_id
 * @property int $cuisine_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Menu $menu
 * @property Cuisine $cuisine
 */
class MenuCuisines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_cuisines}}';
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id', 'cuisine_id'], 'required'],
            [['menu_id', 'cuisine_id', 'created_at', 'updated_at'], 'integer'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['cuisine_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cuisine::className(), 'targetAttribute' => ['cuisine_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'menu_id' => Yii::t('app', 'Menu ID'),
            'cuisine_id' => Yii::t('app', 'Cuisine ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuisine()
    {
		$language_id = 1;
		if(isset($_REQUEST['language_id']))
			$language_id = (int)$_REQUEST['language_id'];
        return $this->hasOne(Cuisine::className(), ['id' => 'cuisine_id'])->select('*')->where(['cd.language_id'=>$language_id])->join('INNER JOIN','tbl_cuisine_description as cd','cd.cuisine_id=tbl_cuisine.id');
    }
}
