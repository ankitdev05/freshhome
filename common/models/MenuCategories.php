<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%menu_categories}}".
 *
 * @property int $id
 * @property int $menu_id
 * @property int $category_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Menu $menu
 * @property Product $category
 */
class MenuCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_categories}}';
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id', 'category_id'], 'required'],
            [['menu_id', 'category_id', 'created_at', 'updated_at','main_category'], 'integer'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'category_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'menu_id' => Yii::t('app', 'Menu ID'),
            'category_id' => Yii::t('app', 'Product ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
		$language_id = 1;
		if(isset($_REQUEST['language_id']))
			$language_id = (int)$_REQUEST['language_id'];
        return $this->hasOne(Category::className(), ['id' => 'category_id'])->select('*')->where(['cd.language_id'=>$language_id])->join('LEFT JOIN','tbl_category_description as cd','cd.category_id=tbl_category.id');
    }
}
