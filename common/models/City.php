<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%city}}".
 *
 * @property int $id
 * @property string $city_name
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 */
class City extends \yii\db\ActiveRecord
{
	public $city_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_name', 'status','country_id','state_id'], 'required'],
			[['city_name'], 'trim'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['city_name'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
        ];
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_name' => Yii::t('app', 'City Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'country_id' => Yii::t('app', 'Country'),
            'state_id' => Yii::t('app', 'State'),
        ];
    }
	public function getCityDescription()
    {
        return $this->hasMany(CityDescription::className(), ['city_id' => 'id']);
    }
	
	public function getCityName()
    {
        return $this->hasOne(CityDescription::className(), ['city_id' => 'id']);
    }
}
