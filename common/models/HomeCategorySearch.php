<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
// use common\models\MainCategories;
use common\models\HomeCategory; 

/**
 * MainCategoriesSearch represents the model behind the search form of `common\models\MainCategories`.
 */
class HomeCategorySearch extends HomeCategory
{ 
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['home_category_id', 'status'], 'integer'],
            // [['name', 'code'], 'safe'],
            // [[ 'image'], 'safe'], 
            [['image','file_info'], 'string', 'max' => 255],

            // [['image'], 'string', 'max' => 255],


        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HomeCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'file_info', $this->file_info]);

        return $dataProvider;
    }
}
