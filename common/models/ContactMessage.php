<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%contact_message}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $subject
 * @property string $email
 * @property string $message
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class ContactMessage extends \yii\db\ActiveRecord
{
	public $send_message;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%contact_message}}';
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'subject', 'email', 'message','role'], 'required'],
            [['user_id', 'subject', 'email', 'message','role'], 'trim'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['send_message'], 'safe'],
            [['email'], 'email'],
            [['message'], 'string'],
            [['subject'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User Name'),
            'subject' => Yii::t('app', 'Subject'),
            'email' => Yii::t('app', 'Email'),
            'message' => Yii::t('app', 'Message'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
	public function getSendEmail(){
		$user_name	= User::findOne($this->user_id);
		$message	= '<p>Hi Admin,</p>';
		$message .= '<p> A user has contacted from '.config_name.' details are as follows:-</p>';
		$message .= '<p> Name: <b>'.$user_name->name.'</b></p>';
		$message .= '<p> Email: <b>'.$this->email.'</b></p>';
		$message .= '<p> Subject: <b>'.$this->subject.'</b></p>';
		$message .= '<p> Message: '.nl2br($this->message).'</p>';
		
		Yii::$app->mailer->compose()
			->setHtmlBody($message)
			->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
			->setTo(config_email)
			->setSubject(config_name.' Contact Us')
			->send();
	}
}
