<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_setting".
 *
 * @property integer $id
 * @property string $settings_key
 * @property string $settings_value
 */
class Settings extends \yii\db\ActiveRecord
{
	public $config_name;
	public $config_owner;
	public $config_address;
	public $config_logo;
	public $config_email;
	public $config_telephone;
	public $config_title;
	public $config_meta_description;
	public $config_meta_keywords;
	public $config_footer;
	public $config_analytic;
	public $config_website_url;
	public $config_paypal_user_name;
	public $config_paypal_password;
	public $config_paypal_signature;
	public $config_paypal_sandbox;
	public $config_tax;
	public $config_last_update;
	public $withdrawal_days;
	public $config_vat;
	public $config_default_currency;
	public $config_ask_for_help_text;
	public $config_freshhommee_fee;
	public $config_transaction_fee;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
       
		return '{{%settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			
            [['settings_key'], 'required'],
			[['config_website_url'],'url'],
			[['config_email'],'email'],  
			[['config_tax','withdrawal_days'],'number'],
			[['withdrawal_days'],'integer'],    
            [['config_email','config_name','config_website_url'], 'string'],
            [['settings_key'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'settings_key' => 'Settings Key',
            'settings_value' => 'Settings Value',
			'config_name' => yii::t('yii','Website Name'),
			'config_footer' => yii::t('yii','Website Footer text'),
			'config_owner' => yii::t('yii','Website Owner name'),
			'config_logo' => yii::t('yii','Website Logo'),
			'config_address' => yii::t('yii','Website Address'),
			'config_email' => yii::t('yii','Admin Email'),
			'config_telephone' => yii::t('yii','Website Telephone'),
			'config_title' => yii::t('yii','Website Meta Title'),
			'config_meta_description' => yii::t('yii','Website Meta Description'),
			'config_meta_keywords' => yii::t('yii','Website Meta Keyword'),
			'config_analytic'=>yii::t('yii','Google Analytics Code'),
			'config_website_url'=>yii::t('yii','Website url'),
			'config_paypal_user_name'=>yii::t('yii','API Username'),
			'config_paypal_password'=>yii::t('yii','API password'),
			'config_paypal_signature'=>yii::t('yii','API Signature'),
			'config_paypal_sandbox'=>yii::t('yii','Test (Sandbox) Mode'),
			'config_tax'=>yii::t('yii','Tax Rate (%)'),
			'config_apikey'=>yii::t('yii','API key'),
			'config_default_currency'=>yii::t('yii','Default Currency'),
			'config_vat'=>yii::t('yii','VAT (%)'),
			'config_ask_for_help_text'=>yii::t('yii','Ask For help text'),
			'config_freshhommee_fee'=>yii::t('yii','Freshhommee Fee (%)'),
			'config_transaction_fee'=>yii::t('yii','Transaction Fee (%)'),
        ];
    }
}
