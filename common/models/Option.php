<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%option}}".
 *
 * @property int $option_id
 * @property string $type
 * @property int $sort_order
 * @property int $status
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $createdBy
 * @property OptionDescription[] $optionDescriptions
 * @property OptionValue[] $optionValues
 */
class Option extends \yii\db\ActiveRecord
{
    public $option_name;
    public $schedule;

    public $types = ['Choose'=>['select'=>'Select','radio'=>'Radio','checkbox'=>'Checkbox'],'Input'=>['input'=>'Text Box','textarea'=>'Large Text Box'],'Date'=>['date'=>'Date','time'=>'Time','date_time'=>'Date & Time']];
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%option}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'sort_order', 'status','option_name','attribute_id'], 'required'],
            [['schedule'], 'safe'],
            [['type', 'sort_order', 'status','option_name'], 'trim'],
            [['sort_order', 'status', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['type'], 'string', 'max' => 32],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['option_name'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'option_id' => Yii::t('app', 'Option ID'),
            'type' => Yii::t('app', 'Type'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptionDescriptions()
    {
        return $this->hasMany(OptionDescription::className(), ['option_id' => 'option_id']);
    }
    public function getOptionDescription()
    {
        return $this->hasOne(OptionDescription::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptionValues()
    {
        return $this->hasMany(OptionValue::className(), ['option_id' => 'option_id']);
    }

    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->id;
        }
        return parent::beforeSave($insert);
    }
}
