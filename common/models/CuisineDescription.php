<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cuisine_description}}".
 *
 * @property int $id
 * @property int $cuisine_id
 * @property int $language_id
 * @property string $cuisine_name
 *
 * @property Cuisine $cuisine
 * @property Language $language
 */
class CuisineDescription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cuisine_description}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cuisine_id', 'language_id', 'cuisine_name'], 'required'],
            [['cuisine_id', 'language_id'], 'integer'],
            [['cuisine_name'], 'string', 'max' => 255],
            [['cuisine_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cuisine::className(), 'targetAttribute' => ['cuisine_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'language_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cuisine_id' => Yii::t('app', 'Cuisine ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'cuisine_name' => Yii::t('app', 'Cuisine Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuisine()
    {
        return $this->hasOne(Cuisine::className(), ['id' => 'cuisine_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['language_id' => 'language_id']);
    }
}
