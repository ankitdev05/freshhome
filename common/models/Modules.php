<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%modules}}".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $config_name
 * @property string $config_value
 * @property int $sort_order
 * @property string $class_name
 * @property int $status
 *
 * @property Modules $parent
 * @property Modules[] $modules
 */
class Modules extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%modules}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort_order', 'status'], 'integer'],
            [['config_name', 'config_value', 'sort_order', 'class_name'], 'required'],
            [['config_name', 'config_value', 'class_name','badge_class'], 'string'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modules::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'config_name' => Yii::t('app', 'Config Name'),
            'config_value' => Yii::t('app', 'Config Value'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'class_name' => Yii::t('app', 'Class Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Modules::className(), ['id' => 'parent_id'])->orderBy('sort_order ASC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModules()
    {
        return $this->hasMany(Modules::className(), ['parent_id' => 'id'])->orderBy('sort_order ASC');
    }
}
