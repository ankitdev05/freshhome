<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%driver_documents}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $status
 * @property string $document
 * @property string $file_info
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class DriverDocuments extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%driver_documents}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'file_info'], 'required'],
            [['user_id', 'status', 'created_at', 'updated_at','is_delete'], 'integer'],
            [['file_info'], 'string'],
           // [['document'], 'string', 'max' => 255],
            [['document'], 'file','skipOnEmpty' => true, 'extensions' => ['jpeg','jpg', 'png', 'pdf', 'doc','docx'],'checkExtensionByMimeType'=>false],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'document' => 'Document',
            'file_info' => 'File Info',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function afterSave($insert,$changedAttributes)
    {
        $subject = 'Driver registration for '.config_name;
        $message	= '<p>Hi Admin</p>';
        $message	.= '<h3>'.$this->user->name.' uploaded documents for verification.</h3>';
        $message	.= '<p>Please '.Html::a('Click Here',Url::to(Yii::$app->params['admin_website_path'].'/user/driverview?id='.$this->user_id,true)).' to view the documents.</p>';
        $message	.= '<p>Thanks,<br/>'.config_name.' Team</p>';
        $email = config_email;
        Yii::$app->mailer->compose()
            ->setHtmlBody($message)
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($email)
            ->setSubject($subject)
            ->send();

        return parent::afterSave($insert,$changedAttributes);
    }

    public function getSendApproveEmail(){
        $user = $this->user;
        $subject = 'Document verification for '.config_name;
        $message	= '<p>Hi '.$user->name.',</p>';
        if($this->status==0)
            $message .= 'Your document has been rejected.';
        elseif($this->status==1)
            $message .= 'Your document has been approved.';
        $message	.= '<p>Thanks,<br/>'.config_name.' Team</p>';
        $email = $user->email;
        return Yii::$app->mailer->compose()
            ->setHtmlBody($message)
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($email)
            ->setSubject($subject)
            ->send();
    }
}
