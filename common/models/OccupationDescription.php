<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%occupation_description}}".
 *
 * @property int $id
 * @property int $occupation_id
 * @property int $language_id
 * @property string $occupation_name
 *
 * @property Language $language
 * @property Occupation $occupation
 */
class OccupationDescription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%occupation_description}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['occupation_id', 'language_id', 'occupation_name'], 'required'],
            [['occupation_id', 'language_id'], 'integer'],
            [['occupation_name'], 'string', 'max' => 255],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'language_id']],
            [['occupation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Occupation::className(), 'targetAttribute' => ['occupation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'occupation_id' => Yii::t('app', 'Occupation ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'occupation_name' => Yii::t('app', 'Occupation Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['language_id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOccupation()
    {
        return $this->hasOne(Occupation::className(), ['id' => 'occupation_id']);
    }
}
