<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use himiklab\thumbnail\EasyThumbnailImage;
use Da\QrCode\QrCode;
use Da\QrCode\Format\MeCardFormat;
use yii\db\Expression;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $verification_token
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;
	public $password;
	public $password_repeat;
	public $old_password;
	public $github;

	public $old_email_address;
	public $verification_code;
	public $wallet_amount;
	public $distance;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subscription_id','subscription_status','subscription_start','subscription_end','subscription_months','subscription_type'],'safe'],
            [['location','latitude','longitude'],'required','on'=>'supp_update_location'],
            [['flat_no'],'required','on'=>'supp_update_flat'],
            [['floor_no'],'required','on'=>'supp_update_floor'],
            [['building_no'],'required','on'=>'supp_update_building'],
            [['city'],'required','on'=>'supp_update_city'],
            [['nationality'],'required','on'=>'supp_update_nationality'],
            [['profile_pic'],'required','on'=>'supp_update_image'],
            [['name'],'required','on'=>'supp_update_name'],
            [['dob'],'required','on'=>'supp_update_dob'],
            [['description'],'required','on'=>'supp_update_description'],
            [['phone_number'],'required','on'=>'supp_update_phone_number'],
            [['email','old_email_address'],'required','on'=>'supp_update_email'],

            [['name','dob','nationality','location','building_no','flat_no','floor_no','landmark','city','latitude','longitude'],'required','on'=>'switch_to_supplier'],

            [['name','dob','nationality','location','building_no','flat_no','floor_no','landmark','city','latitude','longitude','availability'],'required','on'=>'supplier_update'],

            [['ip_address','last_login','last_login_ip','driver_id','order_accepted','last_logged_in_as','supplier_type','is_sales_person','is_test_user','supplier_header_image','occupation_id','is_driver','is_available','profile_pic','distance'],'safe'],

            [['email','username'],'required','on'=>'user_create'],
            [['email','username'],'required','on'=>'user_update'],
            [['name','email','username'],'required','on'=>'admin_create'],
            [['name','email','username'],'required','on'=>'admin_update'],
            [['old_password','password_repeat','password'],'required','on'=>'change_password'],
            [['email','old_email_address'],'required','on'=>'change_email'],
            [['password','password_repeat','share_id'],'required','on'=>'user_create'],
            [['password','password_repeat','share_id'],'required','on'=>'admin_create'],

			[['name','email','username','phone_number','building_no','flat_no','floor_no','landmark','city'],'required','on'=>'create'],
			[['password','password_repeat','share_id'],'required','on'=>'create'],
			[['email','username'],'unique'],
			[['email','old_email_address'],'email'],
			[['password_repeat','password','is_user','is_supplier','cart','gender','views','refer_by','device_token','header_image'],'safe'],
			[['role','email_verify','temp_password','refer_by','driver_id','subscription_start','subscription_end','subscription_months','plan_id'],'integer'],
			[['name','email','username','phone_number','building_no','flat_no','floor_no','landmark','city','location','latitude','longitude','nationality','availability','description','dob','old_email_address','header_image','wallet_amount'],'trim'],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
			['password', 'compare','on'=>'create'],
			['password', 'compare', 'compareAttribute'=>'password_repeat','on'=>'create'],
			['password', 'compare', 'compareAttribute'=>'password_repeat','on'=>'user_create'],
			['password', 'compare', 'compareAttribute'=>'password_repeat','on'=>'user_update'],
			['password', 'compare', 'compareAttribute'=>'password_repeat','on'=>'admin_create'],
			['password', 'compare', 'compareAttribute'=>'password_repeat','on'=>'admin_update'],
			['password', 'compare', 'compareAttribute'=>'password_repeat','on'=>'change_password'],
            ['password_repeat','findPasswords','on'=>'change_password'],
            ['old_email_address','findEmail','on'=>'change_email'],
			[['phone_number'],'required','on'=>'checknumber'],
            [['phone_number'],'checkPhone'],
			[['verification_code'],'required','on'=>'checknumber1'],
        ];
    }
    public function checkPhone($attribute, $params){
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->role==3){
            return;
        }
        if(!empty($this->phone_number)){
            if (Yii::$app->user->isGuest){
                if(!empty($this->id))
                    $checkPhone = User::find()->where(['and',['phone_number'=>$this->phone_number],['status'=>10],['NOT IN','id',$this->id]])->count();
                else
                    $checkPhone = User::find()->where(['and',['phone_number'=>$this->phone_number],['status'=>10]])->count();
                if($checkPhone > 0){
                    $this->addError($attribute, Yii::t('app', 'This phone number has already been taken.'));
                }
            }else{
                $checkPhone = User::find()->where(['and',['phone_number'=>$this->phone_number],['status'=>10],['NOT IN','id',Yii::$app->user->id]])->count();
                if($checkPhone > 0){
                    $this->addError($attribute, Yii::t('app', 'This phone number has already been taken.'));
                }
            }
        }


    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    public function findEmail($attribute, $params){
        if(Yii::$app->user->identity->email!=$this->old_email_address)
            $this->addError($attribute, 'Old email is incorrect.');
    }
    //matching the old password with your existing password.
    public function findPasswords($attribute, $params){
        if(!Yii::$app->security->validatePassword($this->old_password,Yii::$app->user->identity->password_hash))
            $this->addError($attribute, 'Old password is incorrect.');
    }
	public static function findByPhonenumber($phone_number)
    {
        return static::findOne(['phone_number' => $phone_number, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        if(!empty($token))
            return static::findOne(['auth_key' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username,$type=null)
    {
		if($type===null)
			return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
		else
			return static::findOne(['username' => $username,'role'=>$type, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
	
	/**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password_hash' => Yii::t('app','Password'),
            'wallet_amount' => Yii::t('app','Balance Amount'),
            'password_repeat' => Yii::t('app','Confirm Password'),
            'building_no' => Yii::t('app','Building No.'),
            'flat_no' => Yii::t('app','Flat No.'),
            'floor_no' => Yii::t('app','Floor No.'),
            'dob' => Yii::t('app','Date of birth'),
            'profile_pic' => Yii::t('app','Profile image'),
            'share_id' => Yii::t('app','Unique code'),
           
        ];
    }
	/**
     * @return Unique code for users
     */
	public function genrateCode($prefix){
		$code	= $prefix.'-'.$this->random_string(6);
		$result	= User::find()->where(['share_id'=>$code])->one();
		if(!$result)
			return $code;
		$this->genrateCode($prefix);
	}
	function random_string($length) {
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));

		for ($i = 0; $i < $length; $i++) {
			$key .= $keys[array_rand($keys)];
		}

		return $key;
	}
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRole()
    {
        return $this->hasOne(UserRole::className(), ['role_id' => 'role']);
    }
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getCityInfo()
    {
		$language_id = 1;
		if(isset($_REQUEST['language_id']))
			$language_id = (int)$_REQUEST['language_id'];
        return $this->hasOne(City::className(), ['id' => 'city'])->select('*,cd.city_id as id')->where(['cd.language_id'=>$language_id])->join('LEFT JOIN','tbl_city_description as cd','cd.city_id=tbl_city.id');
        
    }
	public function getOccInfo()
    {
		$language_id = 1;
		if(isset($_REQUEST['language_id']))
			$language_id = (int)$_REQUEST['language_id'];
		
        return $this->hasOne(Occupation::className(), ['id' => 'occupation_id'])->select('*,od.occupation_id as id')->where(['od.language_id'=>$language_id])->join('LEFT JOIN','tbl_occupation_description as od','od.occupation_id=tbl_occupation.id');
        
    }
	 public function getCountryInfo()
    {
        return $this->hasOne(Country::className(), ['code' => 'nationality']);
    }
	public function getUserImage($profile_pic,$sizex,$sizey){
		return Yii::$app->params['website_path'].'/uploads/'.$profile_pic;
		$image          = '../../frontend/web/uploads/'.$profile_pic;
		$p_image    = EasyThumbnailImage::thumbnailFileUrl(
				$image,$sizex,$sizey,EasyThumbnailImage::THUMBNAIL_INSET
			);
		return $p_image;
	}
    public function getSupplierImage($profile_pic,$sizeX,$sizeY){
        if(!empty($profile_pic)){
            $image          = '../../frontend/web/uploads/'.$profile_pic;
            $p_image    = EasyThumbnailImage::thumbnailFileUrl(
                $image,$sizeX,$sizeY,EasyThumbnailImage::THUMBNAIL_INSET
            );
            return $p_image;
        }
        return;
    }
    public function getSuppImage($sizeX=100,$sizeY=100){
        if(empty($this->profile_pic))
            $this->image = 'no_image.png';
        $image          = '../../frontend/web/uploads/'.$this->profile_pic;
        $p_image    = EasyThumbnailImage::thumbnailFileUrl(
            $image,$sizeX,$sizeY,EasyThumbnailImage::THUMBNAIL_INSET
        );
        return $p_image;
    }
	public function getSalesDetail(){
        $name = $gender = $dob = $profile_pic = $description = $nationality = $occupation_id = $occupation_name = null;
        $country_info = $this->countryInfo;
        $nationality = !empty($country_info) ? $country_info->nationality : '';
        $sales_data             = SalesPerson::find()->where(['user_id'=>$this->id])->one();
        if(!empty($sales_data)){
            $this->city             = $sales_data->city;
            $city_info			    = $this->cityInfo;
            $json['user_type'] 		= !empty($this->last_logged_in_as) ? $this->last_logged_in_as : $this->role;
            $user_role			    = UserRole::findOne($json['user_type']);
            $json['token']			= $this->auth_key;
            $json['share_id']		= $this->share_id;
            $json['id']				= (int)$this->id;
            $json['email']			= $this->email;
            $json['username']		= $this->username;
            $json['phone_number']	= $this->phone_number;
            $json['role'] 			= strtolower($user_role->name);

            $json['is_supplier']	= $this->is_supplier;
            $json['is_user']		= $this->is_user;
            $json['is_sale_person']	= $this->is_sales_person;
            $json['is_driver']	= $this->is_driver;
            $json['supplier_type']	= $this->supplier_type;
            $json['name']		    = $sales_data->full_name;
            $json['gender']		    = $sales_data->gender;
            $json['dob']		    = $sales_data->dob;
            $json['city']			= !empty($city_info) ? $city_info->id : '';
            $json['city_name']		= !empty($city_info) ? $city_info->city_name : '';
            $json['scan_code']		= $this->qrcode;
            $json['rating']		    = $this->salesPersonRating;
            $json['is_watched']		= $this->videosinfo;
            $json['header_image']	= $this->userBanner;
            $json['banner_id']		= $this->header_image;
            $json['supplier_header_image'] = $this->supplier_header_image;
            $json['supplier_header_image_path']		= $this->supplierBanner;
            $json['subscription_data']		= $this->subscriptionData;
            $json['description']		= $this->description;
            $json['nationality'] = !empty($country_info) ? $country_info->nationality : '';

            $json['flag'] = !empty($nationality) ? Url::to(Yii::$app->params['website_path'].'/uploads/flags/'.strtolower($nationality).'.png',true)  : "";
            $json['occupation_id']	= (int)$this->occupation_id;
            $json['occupation_name'] = '';
            $json['location'] = $this->location;
            $json['landmark'] = $this->landmark;
            $json['floor_no'] = $this->floor_no;
            $json['flat_no'] = $this->flat_no;
            $json['building_no'] = $this->building_no;
            $json['availability']	= $this->availability;
            $json['reviews']	= 0;
            $json['views']	= 0;
            $json['delivered_order']	= 0;
            $json['pending_order']	= 0;
            $json['is_fav']	= 0;
            $json['taste_rating']	= 0;
            $json['presentation_rating']	= 0;
            $json['packing_rating']	= 0;
            $json['overall_rating']	= 0;
            $json['supplier_reviews']	= [];
            $json['distance']	= $this->distance;
            $json['national_pic']	= $this->nationalId;
            $profile_pic= '';
            if(!empty($sales_data->image_url) )
                $profile_pic	= Url::to($this->getUserImage($sales_data->image_url,100,100),true);
            $json['profile_pic']		= $profile_pic;

            return $json;
        }else
            return [];

    }
    public function getBankDetails(){
        $json = [];
        $bankDetails = UserBankAccount::find()->where(['user_id' => $this->id])->one();
        if(!empty($bankDetails)){
            $json['first_name'] = $bankDetails->first_name;
            $json['last_name'] = $bankDetails->last_name;
            $json['email_address'] = $bankDetails->email_address;
            $json['bank_name'] = $bankDetails->bank_name;
            $json['account_number'] = $bankDetails->account_number;
            $json['iban'] = $bankDetails->iban;
        }else $json = (object)[];
        return $json;
    }

	public function getUserDetail(){ //normal user
        $json['user_type'] 		= !empty($this->last_logged_in_as) ? $this->last_logged_in_as : $this->role;
        $user_role			= UserRole::findOne($json['user_type']);
		$name = $gender = $dob = $profile_pic = $description = $nationality = $occupation_id = $occupation_name = null;
		$u_data			= UserData::findOne(['user_id'=>$this->id]);
		if(!empty($u_data)){
			$country_info		= $u_data->countryInfo;
			$occInfo_info	= $u_data->occupation;
			$occupation_name =  !empty($occInfo_info) ? $occInfo_info->occupation_name : null;
			$name 			= $u_data->name;
			$gender 		= $u_data->gender;
			$dob 			= $u_data->dob;
			$description 	= $u_data->description;
			$nationality 	= !empty($country_info) ? $country_info->nationality : '';
			$occupation_id 	= $u_data->occupation_id;
			if(!empty($u_data->profile_pic) && $u_data->image_approval==1)
				$profile_pic	= Url::to($this->getUserImage($u_data->profile_pic,100,100),true);
			else
                $profile_pic	= Url::to($this->getUserImage('users/no_image.png',100,100),true);
		}
		$json['token']			= $this->auth_key;
		$json['share_id']		= $this->share_id;
		$json['id']				= (int)$this->id;
		$json['email']			= $this->email;
		$json['username']		= $this->username;
		$json['phone_number']	= $this->phone_number;
		$json['name']			= $name;
		$json['gender']			= $gender;
		$json['DOB']			= $dob;
		$json['profile_pic']	= $profile_pic;
		$json['description']	= $description;
		$json['nationality']	= $nationality;
		$json['flag']			= !empty($nationality) ? Url::to(Yii::$app->params['website_path'].'/uploads/flags/'.strtolower($nationality).'.png',true)  : "";
		$json['occupation_id']	= (int)$occupation_id;
		$json['occupation_name'] = $occupation_name;

        $json['location']		= $this->location;
        $json['landmark']		= $this->landmark;
        $json['floor_no']		= $this->floor_no;
        $json['flat_no']		= $this->flat_no;
        $json['building_no']	= $this->building_no;
        $json['description']	= $this->description;
        $json['availability']	= $this->availability;

        $json['role'] 			= strtolower($user_role->name);

        $json['rating']			= 0;
        $json['reviews']		= 0;
        $json['views']			= 0;
        $json['delivered_order']= 0;
        $json['pending_order']	= 0;
        $json['temp_password']	= $this->temp_password;
        $json['is_supplier']	= $this->is_supplier;
        $json['is_user']		= $this->is_user;
        $json['is_fav']			= $this->myfavSupp;
       // $json['occupation_id']	= (int)$this->occupation_id;
        $json['taste_rating']        = $this->avgTaste;
        $json['presentation_rating'] = $this->avgPresentation;
        $json['packing_rating']      = $this->avgPacking;
        $json['overall_rating']      = $this->avgOverall;
        $json['supplier_reviews']    = [];

        $sales_data = SalesPerson::find()->where(['user_id'=>$this->id])->one();
        if(!empty($sales_data))
            $json['is_sale_person'] = 'yes';
        $json['supplier_type'] = $this->supplier_type;
        $json['scan_code'] = $this->qrcode;
        $json['header_image'] = $this->userBanner;
        $json['banner_id'] = $this->header_image;
        $json['is_watched'] = $this->videosinfo;
        $json['supplier_header_image'] = $this->supplier_header_image;
        $json['supplier_header_image_path'] = $this->supplierBanner;
        $json['subscription_data'] = $this->subscriptionData;
        $json['is_driver'] = $this->is_driver;
        $json['is_sale_person'] = $this->is_sales_person;
        $json['distance']	= $this->distance;
        $json['national_pic']	= $this->nationalId;
//        if($this->is_driver=='yes')
//            $json['is_sale_person']	    = 'no';
		return $json;
	}
	public function getDriverInfo(){
        $user_image			= '';
        $city_info			= $this->cityInfo;
        if(!empty($this->profile_pic) && $this->image_approval==1)
            $user_image	= Url::to($this->getUserImage($this->profile_pic,100,100),true);
        $json['token']			= $this->auth_key;
        $json['id']				= (int)$this->id;
        $json['name']			= $this->name;
        $json['email']			= $this->email;
        $json['username']		= $this->username;
        $json['phone_number']	= $this->phone_number;
        $json['location']		= $this->location;
        $json['DOB']			= $this->dob;
        $json['share_id']		= $this->share_id;
        $json['is_available']	= $this->is_available;
        $json['profile_pic']	= $user_image;

        $json['city']			= !empty($city_info) ? $city_info->id : '';
        $json['latitude']		= $this->latitude;
        $json['longitude']		= $this->longitude;
        $json['document']		= $this->driverDocument;
        $json['city_name']		= !empty($city_info) ? $city_info->city_name : '';
        $json['rating']		= 0;

        $json['supplier_type'] = $this->supplier_type;
        $json['scan_code'] = $this->qrcode;
        $json['header_image'] = $this->userBanner;
        $json['banner_id'] = $this->header_image;
        $json['is_watched'] = $this->videosinfo;
        $json['supplier_header_image'] = $this->supplier_header_image;
        $json['supplier_header_image_path'] = $this->supplierBanner;

        $json['subscription_data']	= $this->subscriptionData;
        $json['is_supplier']	= $this->is_supplier;
        $json['is_user']		= $this->is_user;
        $json['is_driver'] = $this->is_driver;
        $json['is_sale_person']	  =  $this->is_sales_person;
        $json['distance']	= $this->distance;
        $json['national_pic']	= $this->nationalId;
        return $json;
    }
    public function getCartUserInfo(){
        $user_image			= '';
        if(!empty($this->profile_pic) && $this->image_approval==1)
            $user_image	= Url::to($this->getUserImage($this->profile_pic,100,100),true);
        $json['availability']	= $this->availability;
        $json['name']			= $this->name;
        $json['email']			= $this->email;
        $json['username']		= $this->username;
        $json['phone_number']	= $this->phone_number;
        $json['supplier_type']	= $this->supplier_type;
        $json['profile_pic']	= $user_image;
        return $json;
    }

	public function getUserInfo(){ //supplier info
        $json['user_type'] 		= !empty($this->last_logged_in_as) ? $this->last_logged_in_as : $this->role;
		$user_role			= UserRole::findOne($json['user_type']);
		$city_info			= $this->cityInfo;
		$country_info		= $this->countryInfo;
		$occInfo_info		= $this->occInfo;
		$user_image			= '';
		$menu_count			= Menu::find()->where(['supplier_id'=>$this->id])->count();
		if(!empty($this->profile_pic) && $this->image_approval==1)
			$user_image	= Url::to($this->getUserImage($this->profile_pic,100,100),true);
		
		$json['token']			= $this->auth_key;
		$json['id']				= (int)$this->id;
		$json['name']			= $this->name;
		$json['email']			= $this->email;
		$json['username']		= $this->username;
		$json['phone_number']	= $this->phone_number;
		$json['location']		= $this->location;
		$json['landmark']		= $this->landmark;
		$json['floor_no']		= $this->floor_no;
		$json['flat_no']		= $this->flat_no;
		$json['building_no']	= $this->building_no;
		$json['description']	= $this->description;
		$json['availability']	= $this->availability;
		$json['DOB']			= $this->dob;
		$json['share_id']		= $this->share_id;
      //  $json['gender']		    = $this->gender;
		$json['menu']			= (int)$menu_count;
		$json['profile_pic']	= $user_image;
		$json['nationality']	= !empty($country_info) ? $country_info->nationality : '';

		$json['city']			= !empty($city_info) ? $city_info->id : '';
		$json['city_name']		= !empty($city_info) ? $city_info->city_name : '';
		$json['flag']			= !empty($this->nationality) ? Url::to(Yii::$app->params['website_path'].'/uploads/flags/'.strtolower($this->nationality).'.png',true)  : "";
		$json['role'] 			= strtolower($user_role->name);

		$json['rating']			= 4.5;
		$json['reviews']		= (int)$this->totalreviews;
		$json['views']			= $this->views;
		$json['delivered_order']= 23;
		$json['pending_order']	= 4;
		$json['temp_password']	= $this->temp_password;
		$json['is_supplier']	= $this->is_supplier;
		$json['is_user']		= $this->is_user;
		$json['is_fav']			= $this->myfavSupp;
		$json['occupation_id']	= (int)$this->occupation_id;
		$json['occupation_name'] = !empty($occInfo_info) ? $occInfo_info->occupation_name : null;
		$json['taste_rating']        = $this->avgTaste;
		$json['presentation_rating'] = $this->avgPresentation;
		$json['packing_rating']      = $this->avgPacking;
		$json['overall_rating']      = $this->avgOverall;
		$json['is_available']      = $this->is_available;
		$review = $this->supplierReviews;
		if(!empty($review))
    		$json['supplier_reviews']    = $this->supplierReviews;
		else
            $json['supplier_reviews'] = (object)[];
		$json['total_orders']        = 0;
		$json['plan_id']            = $this->plan_id;
		$json['subscription_ends']  = !empty($this->subscription_end) ? date("F d,Y",$this->subscription_end) : '';
		$time = time();
		$json['subscription_expire']  = ($time > $this->subscription_end) ? "yes" : "no";
        $token			= '';
        $user_model		= new User();
        $headers		= getallheaders();
        if(isset($headers['Authorization']))
            $token	= str_replace("Bearer ","",$headers['Authorization']);
        $user_info		= $user_model->findIdentityByAccessToken($token);
        if (!empty($user_info)) {
            $user_id =$user_info->id;
            $count = Order::find()->where(['user_id'=>$user_id,'order_status_id'=>5])->count();
            $json['total_orders']        = (int)$count;
        }

        $json['is_sale_person']	    = 'no';
        $sales_data             = SalesPerson::find()->where(['user_id'=>$this->id])->one();
        if(!empty($sales_data))
            $json['is_sale_person']	    = 'yes';
        $json['supplier_type']	= $this->supplier_type;
        $json['scan_code']		= $this->qrcode;
        $json['latitude']		= $this->latitude;
        $json['longitude']		= $this->longitude;
        $json['header_image']	= $this->userBanner;
        $json['banner_id']		= $this->header_image;
        $json['is_watched']		= $this->videosinfo;
        $json['supplier_header_image']		= $this->supplier_header_image;
        $json['supplier_header_image_path']		= $this->supplierBanner;
        $json['subscription_data']		= $this->subscriptionData;
        $json['is_driver']		= $this->is_driver;
        $json['distance']	= $this->distance;
        $json['national_pic']	= $this->nationalId;
		return $json;
	}
	public function getDriverDocument(){
        $model = new DriverDocuments();
        $result = $model->find()->where(['user_id' => $this->id])->orderBy('id desc')->one();
        if(!empty($result))
            return Yii::$app->params['website_path'].'/uploads/'.$result->document;
        return '';
    }
	public function getTotalreviews(){
        return OrderRatings::find()->where(['supplier_id'=>$this->id,'is_active'=>1])->count();
    }
    public function getSalesPersonRating(){
        $total =  SalesPersonRating::find()->select('avg(rating)')->where(['user_id'=>$this->id])->one();
        if(!empty($total))
            return round($total['rating'],2);
        return 0;
    }

	public function getAvgTaste(){
        $total		= OrderRatings::find()->select('avg(taste_rating) as rating')->where(['supplier_id'=>$this->id])->one();
        if(!empty($total))
            return round($total['rating'],2);
        return 0;
    }

    public function getAvgPresentation(){
        $total		= OrderRatings::find()->select('avg(presentation_rating) as rating')->where(['supplier_id'=>$this->id])->one();
        if(!empty($total))
            return round($total->rating,2);
        return 0;
    }

    public function getAvgPacking(){
        $total		= OrderRatings::find()->select('avg(packing_rating) as rating')->where(['supplier_id'=>$this->id])->one();
        if(!empty($total))
            return round($total->rating,2);
        return 0;
    }

    public function getAvgOverall(){
        $total		= OrderRatings::find()->select('avg(overall_rating) as rating')->where(['supplier_id'=>$this->id])->one();
        if(!empty($total))
            return round($total->rating,2);
        return 0;
    }
    public function getFavSupp(){ // for website
        $result		= 0;
        if (!Yii::$app->user->isGuest){
            $count		= UserFavSupplier::find()->where(['supplier_id'=>$this->id,'user_id' => Yii::$app->user->identity->id])->count();
            if($count>0)
                $result = 1;
        }
        return $result;
    }
    public function getFavSupps()
    {
        return $this->hasOne(UserFavSupplier::className(), ['user_id' => 'id']);
    }
    public function getSuppType()
    {
        return $this->hasOne(MainCategories::className(), ['id' => 'supplier_type']);
    }
	public function getMyfavSupp(){
		$result		= 0;
		$user_model		= new User();
		$headers		= getallheaders();
		$token			= '';
		if(isset($headers['Authorization']))
			$token	= str_replace("Bearer ","",$headers['Authorization']);
		
		$supplier 		= [];
		$post_data 		= Yii::$app->request->post();
		
		$user_info		= $user_model->findIdentityByAccessToken($token);
		if(!empty($user_info)){
			$count		= UserFavSupplier::find()->where(['supplier_id'=>$this->id,'user_id'=>$user_info->id])->count();
			if($count>0)
				$result = 1;
		}
			
		return $result;
	}
	
	public function getUserFavMenus(){
		$menus_data		= [];
		$menus			= UserFavMenu::find()->where(['user_id'=>$this->id])->all();
		if(!empty($menus)){
			foreach($menus as $menu)
				$menus_data[]	=  $menu->menu->dishInfo;
		}
		return $menus_data;
	}
	public function getUserFavSuppliers(){
		$suppliers_data				= [];
		$suppliers					= UserFavSupplier::find()->where(['user_id'=>$this->id])->all();
		if(!empty($suppliers)){
			foreach($suppliers as $supplier)
				$suppliers_data[]	=  $supplier->supplier->userInfo;
		}
		return $suppliers_data;
	}
	public function getUserMenuSchedules(){
		$searchModel 				= new MenuSchedulesSearch();
		$searchModel->supplier_id 	= $this->id;
		$dataProvider 				= $searchModel->searchSch(Yii::$app->request->queryParams);
		$results	= $dataProvider->getModels();
		$output		= [];
		if(!empty($results)){
			
			foreach($results as $k=>$result){
				$qty = 0;
				$output[$k]['id'] 				= $result->id;
				//$output[$k]['supplier_id'] 		= $result->supplier_id;
				$output[$k]['schedule_date'] 	= $result->schedule_date;
				$output[$k]['start_time'] 		= $result->start_time;
				$output[$k]['end_time'] 		= $result->end_time;
				$output[$k]['choices'] 			= $result->choices;

				$items			= [];
				$menu_items		= $result->menuScheduleItems;
				if(!empty($menu_items)){
					foreach($menu_items as $m=>$menu_item){
					    $dish_info = $menu_item->menu->dishInfo;

						$items[$m] = ['dish_name'=>$dish_info['dish_name'],'dish_image'=>$dish_info['dish_image'],'menu_id'=>$dish_info['dish_id'],'qty'=>$menu_item->quantity];

					}
				}

				$output[$k]['menu_items']		= $items;
			}
		}
		return $output;
	}

    public function getSupplierReviews()
    {
        $all_reviews	= [];
        $order_reviews  = OrderRatings::find()->where(['supplier_id'=>$this->id,'is_active'=>1])->orderBy("id desc")->one();
        if(!empty($order_reviews)){
                $info = $order_reviews->user->userDetail;
                $all_reviews['review']  = $order_reviews->review;
                $all_reviews['user_info']['name']  = $info['name'];
                $all_reviews['user_info']['profile_pic']  = $info['profile_pic'];
                $all_reviews['time']	= Yii::$app->formatter->format($order_reviews->created_at, 'relativeTime');

        }
        return $all_reviews;
    }

    public function getSupplierReview()
    {
        $all_reviews	= [];
        $order_reviews  = OrderRatings::find()->where(['supplier_id'=>$this->id,'is_active'=>1])->orderBy("id desc")->one();
        if(!empty($order_reviews)){
            $info = $order_reviews->user->userDetail;
            $all_reviews['review']  = $order_reviews->review;
            $all_reviews['user_info']['name']  = $info['name'];
            $all_reviews['user_info']['profile_pic']  = $info['profile_pic'];
            $all_reviews['time']	= Yii::$app->formatter->format($order_reviews->created_at, 'relativeTime');

        }
        return $all_reviews;
    }
    public function getKitchenAddress(){
        $data = [];

        $city_info			= $this->cityInfo;
        $data['name'] = $this->name;
        $data['phone_number'] = $this->phone_number;
        $data['building_no'] = $this->building_no;
        $data['flat_no'] = $this->flat_no;
        $data['floor_no'] = $this->floor_no;
        $data['landmark'] = $this->landmark;
        $data['location'] = $this->location;
        $data['latitude'] = $this->latitude;
        $data['longitude'] = $this->longitude;
        $data['city'] =!empty($city_info) ? $city_info->city_name : '';

        return $data;
    }
    public function getQrcode(){
        $data['phone'] = "{$this->phone_number}";
        $data['email'] = "{$this->email}";
        $data['unique_code'] = "{$this->share_id}";
        $data['device_token'] = "{$this->device_token}";
        $qrCode = new QrCode(json_encode($data));
        $fullurl 		= Yii::getAlias('@frontend').'/web/uploads/';
        $qrCode->writeFile($fullurl . 'qrcodes/'.$this->id.'.png');
        return Url::to(Yii::$app->params['website_path'].'/uploads/qrcodes/'.$this->id.'.png',true);
    }
    public function getLoggedinInfo(){
        if($this->last_logged_in_as==1)
            $user_data		= $this->userDetail;
        if($this->last_logged_in_as==2)
            $user_data		= $this->userInfo;
        if($this->last_logged_in_as==5)
            $user_data		= $this->salesDetail;

        return $user_data;
    }
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }
    public function loginActivity($user){
        $user->last_login		= time();
        $user->last_login_ip	= Yii::$app->getRequest()->getUserIP();
        $user->save();

        $login_act				= new LoginActivity();
        $login_act->user_id		= $user->id;
        $login_act->ip_address	= Yii::$app->getRequest()->getUserIP();
        $login_act->save();
    }

    protected function getVideosinfo(){
        $total_videos  = Videos::find()->where(['status'=>'active'])->count();
        $watched_videos = VideosWatchlist::find()->where(["user_id"=>$this->id])->count();
        if($total_videos<=$watched_videos)
            return 1;
        return 0;
    }

    protected function getUserBanner(){
        if(!empty($this->header_image)){
            $result = Banners::find()->where(['status'=>'active','banner_id'=>$this->header_image])->asArray()->one();
            if(!empty($result)){
                return Yii::$app->params['website_path'].'/uploads/'.$result['image'];
            }
            return null;
        }
    }

    protected function getNationalId(){
        if(!empty($this->national_pic)){
            return Yii::$app->params['website_path'].'/uploads/'.$this->national_pic;

        }
        return null;
    }
    protected function getSupplierBanner(){
        if(!empty($this->supplier_header_image)){
            $result = Banners::find()->where(['status'=>'active','banner_id'=>$this->supplier_header_image])->asArray()->one();
            if(!empty($result)){
                return Yii::$app->params['website_path'].'/uploads/'.$result['image'];
            }
            return null;
        }
    }
    public function getCheckNationalId(){
        if(!empty($this->national_pic) && $this->national_pic_approval==2){
            return Yii::$app->params['website_path'].'/uploads/'.$this->national_pic;

        }
        return null;
    }

    public function switchToSales($user_info,$user_id){
        $gender = $user_info->gender ?? 'male';
        if(empty($gender)) $gender = 'male';

        if(empty($this->city)) $this->city = 1;
        if(empty($this->dob)) $this->dob = '1970-01-01';

        $model = new SalesPerson();
        $model->user_id = $user_id;
        $model->full_name = $user_info->name ?? $this->name;
        $model->gender = $gender;
        $model->city = $this->city;
        $model->dob = $this->dob;
        if(!$model->save()) print_r($model->getErrors());
    }

    public function getProfileUrl(){
        return Url::to(Yii::$app->request->baseUrl.'/user/'.$this->username,true);
    }
    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    public function sendEmailUpdate($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject('Email verification at ' . Yii::$app->name)
            ->send();
    }

    public function getTotalAvailableItems(){
        return Menu::find()->where(['and',['supplier_id' => $this->id],['status'=>'active'],['!=','dist_qty',0]])->count();
    }
    public function getTotalDeliveredItems(){
        $query = Order::find()->select('order_id')->where(['supplier_id' => $this->id,'order_status_id'=>5]);
        return OrderProducts::find()->where(['in','order_id',$query])->count();
    }

    public function getAddresses(){
        return $this->hasMany(UserAddress::className(), ['user_id' => 'id']);
    }

    public function getBalance(){
        $balance = 0;
        $model = UserTotal::find()->where(['user_id' => $this->id])->one();
        if(!empty($model))
            $balance = $model->balance;

        return $balance;
    }

    public function getSubscriptionStatus(){
        $time = time();
        if($this->subscription_end > $time)
            return 'active';
        return 'inactive';
    }
    public function getSubscriptionData(){
        $json = [];
        $json['status'] = 'inactive';
        $json['subscription_status'] = '';
        $json['subscription_start'] ='';
        $json['subscription_start_format'] = '';
        $json['subscription_end'] ='';
        $json['subscription_end_format'] = '';
        if($this->subscription_type==Yii::$app->params['btree_environment']){
            $time = time();
            if($this->subscription_end > $time)
                $json['status'] = 'active';

            $json['subscription_status'] = $this->subscription_status;
            $json['subscription_start'] = $this->subscription_start;
            $json['subscription_start_format'] = date('d F Y',$this->subscription_start);
            $json['subscription_end'] = $this->subscription_end;
            $json['subscription_end_format'] = date('d F Y',$this->subscription_end);
        }
        return $json;
    }

    public function getPendingOrders(){
        $query = Order::find()->where(['supplier_id' => $this->id,'order_status_id' => 4]);
        $count = $query->count();
        $json['count'] = $count;
        if($count > 0 ){
            $orders = $query->all();
            foreach ($orders as $order)
                $order_data[] = $order->driverOrderInfo;
            $json['orders_data'] = $order_data;
        }else $json['orders_data'] = [];
        return $json;
    }
    public function getDriverDocStatus(){
        return DriverDocuments::find()->where(['user_id' => $this->id,'status' => 0])->count();
    }

    public function getDriverDocActiveStatus(){
        return DriverDocuments::find()->where(['user_id' => $this->id,'status' => 1])->count();
    }

    public function getFoodSupplierPendingOrders(){
        return  Order::find()
            ->where(['supplier_id'=>$this->id])
            ->andWhere(['and',['date'=>date('j/m/Y')],['NOT IN','order_status_id',[2,5]]])
            ->orWhere(['and',
                ['is','date',null],
                ['NOT IN','order_status_id',[2,5]],
                ['supplier_id'=>$this->id],
                ['=',new Expression("FROM_UNIXTIME(created_at,'%Y-%m-%d')"),new Expression('CURDATE()')]
            ]);
    }

    public function getFoodSupplierCompleteOrder(){
        return Order::find()->where(['supplier_id'=>$this->id,'order_status_id'=>5]);
    }

    public function getFoodSupplierPreOrder(){
        return Order::find()->where(['and',
           // ['>','date',date('j/n/Y')],
            ['supplier_id'=>$this->id]
        ])->andWhere(['and',['=','order_type','pre-order'],['in','order_status_id',[11,1]]]);
    }

    public function getCountDriverAcceptedOrders(){
        return $count = Order::find()->where(['driver_id' => $this->id, 'order_status_id' => 4])->count();
    }

    public function updateBalance($wallet){
        $total = $wallet->amount;
        $model = new UserTotal();
        $result = $model->find()->where(['user_id'=>$this->id])->one();
        if(!empty($result)){
            $result->updateCounters(['balance' => $total]);
            $total = $result->balance;
        }else{
            $model->user_id = $this->id;
            $model->balance = $total;
            $model->save();
        }
        $wallet->close_balance = $total;
        $wallet->save();
    }
}
