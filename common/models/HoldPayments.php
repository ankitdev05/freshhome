<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%hold_payments}}".
 *
 * @property int $hold_id
 * @property int $order_id
 * @property string $status
 * @property string $amount
 * @property string $currency_code
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Order $order
 * @property HoldTransaction[] $holdTransactions
 */
class HoldPayments extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%hold_payments}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'status', 'amount', 'currency_code'], 'required'],
            [['order_id', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
            [['status', 'currency_code'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'order_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hold_id' => Yii::t('app', 'Hold ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'status' => Yii::t('app', 'Status'),
            'amount' => Yii::t('app', 'Amount'),
            'currency_code' => Yii::t('app', 'Currency Code'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHoldTransactions()
    {
        return $this->hasMany(HoldTransaction::className(), ['hold_id' => 'hold_id']);
    }
}
