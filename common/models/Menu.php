<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use himiklab\thumbnail\EasyThumbnailImage;
use common\models\Library;
use common\models\CategoryDescription;
/**
 * This is the model class for table "{{%menu}}".
 *
 * @property int $id
 * @property int $supplier_id
 * @property string $dish_name
 * @property string $dish_description
 * @property string $dish_image
 * @property int $image_approval '0'=>Not approve, '1'=>approve
 * @property string $dish_price
 * @property string $dish_weight
 * @property string $dish_since
 * @property int $city_id
 * @property string $status
 * @property int $dist_qty
 * @property int $dish_serve
 * @property int $created_at
 * @property int $updated_at
 *
 * @property MenuCategories[] $menuCategories
 * @property MenuCuisines[] $menuCuisines
 * @property MenuMeals[] $menuMeals
 */
class Menu extends \yii\db\ActiveRecord
{
	public $distance;
	public $meal;
	public $cuisine;
	public $category;

	public $product_name;
	public $product_description;
	public $product_price;
	public $product_image;
	public $product_qty;
	public $product_images;
	public $product_attributes;
	public $TotalQuantity;
	public $products_type = ['hand' => 'Hand made','factory' => 'Factory type'];
	public $discount_product;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu}}';
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'dish_name',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplier_id', 'image_approval', 'dish_serve', 'created_at', 'updated_at','view','is_delete','dish_prep_time','dist_qty','product_type','test_data','attribute_id','brand_id'], 'integer'],
            [['product_name','product_description','product_price','product_qty','collected_amount','type_of_product'],'required','on'=>'other_items'],
            [['product_name','product_description','product_price','product_qty'],'trim'],
            [['product_name','product_description','product_price','product_image','product_qty','product_images','product_attributes','home_category','category'],'safe'],
            [['dish_name','dish_description','dish_price','dish_serve','collected_amount'], 'required','on'=>'food_menu'],
            [['dish_name','dish_description','dish_price','dish_weight','dist_qty','dish_since'], 'trim'],
            [['dish_description', 'status','slug'], 'string'],
            [['dish_price','product_price','discount','collected_amount'], 'number'],
            [['meal','cuisine','category','city_id','meal','cuisine','category','type_of_product','TotalQuantity'], 'safe'],
            [['dish_name', 'dish_image'], 'string', 'max' => 255],
            [['dish_weight', 'dish_since'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'supplier_id' => Yii::t('app', 'Supplier ID'),
            'dish_name' => Yii::t('app', 'Name'),
            'dish_description' => Yii::t('app', 'Description'),
            'dish_image' => Yii::t('app', 'Image'),
            'dish_price' => Yii::t('app', 'Price'),
            'dish_weight' => Yii::t('app', 'Weight'),
            'dish_since' => Yii::t('app', 'Preparation time'),
            'city_id' => Yii::t('app', 'City'),
            'status' => Yii::t('app', 'Status'),
            'dist_qty' => Yii::t('app', 'Quantity'),
            'dish_serve' => Yii::t('app', 'Serving for(per person)'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'cuisine' => Yii::t('app', 'Cuisines'),
            'category' => Yii::t('app', 'Categories'),
            'meal' => Yii::t('app', 'Type of meal'),
            'product_qty' => Yii::t('app', 'Product Quantity'),
            'attribute_id' => Yii::t('app', 'Product Attribute'),
            'brand_id' => Yii::t('app', 'Brand'),
            'discount_product' => Yii::t('app', 'Would you like to give some discount on this item?'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuCategories()
    {
        return $this->hasMany(MenuCategories::className(), ['menu_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuCuisines()
    {
        return $this->hasMany(MenuCuisines::className(), ['menu_id' => 'id']);
    }
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProducts::className(), ['product_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuMeals()
    {
        return $this->hasMany(MenuMeals::className(), ['menu_id' => 'id']);
    }
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['brand_id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavMenu()
    {
        return $this->hasOne(UserFavMenu::className(), ['menu_id' => 'id']);
    }
    public function getImages()
    {
        return $this->hasMany(MenuToImages::className(), ['menu_id' => 'id']);
    }
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getsupplierInfo()
    {
        return $this->hasOne(User::className(), ['id' => 'supplier_id'])->andWhere(['tbl_user.status' => 10]);
    }

    public function getSupplierInfo1()
    {
        return $this->hasOne(User::className(), ['id' => 'supplier_id']);
    }
	public function getMenuImage($dish_image,$sizex,$sizey){
		return Yii::$app->params['website_path'].'/uploads/'.$dish_image;
		$image          = '../../frontend/web/uploads/'.$dish_image;
		$p_image    = EasyThumbnailImage::thumbnailFileUrl(
				$image,$sizex,$sizey,EasyThumbnailImage::THUMBNAIL_INSET
			);
		return $p_image;
	}
    public function getProductImage(){
        if(!empty($this->dish_image) && $this->image_approval==1)
            return Yii::$app->params['website_path'].'/uploads/'.$this->dish_image;
        return;
        $image          = '../../frontend/web/uploads/'.$dish_image;
        $p_image    = EasyThumbnailImage::thumbnailFileUrl(
            $image,$sizex,$sizey,EasyThumbnailImage::THUMBNAIL_INSET
        );
        return $p_image;
    }

	public function getMyfavDish(){
		$result		= 0;
		$user_model		= new User();
		$headers		= getallheaders();
		$user_info		= [];
		$token			= '';
		if(isset($headers['Authorization']))
			$token	= str_replace("Bearer ","",$headers['Authorization']);
        if(!empty($token))
    		$user_info		= $user_model->findIdentityByAccessToken($token);
		if(!empty($user_info)){
			$count		= UserFavMenu::find()->where(['menu_id'=>$this->id,'user_id'=>$user_info->id])->count();
			if($count>0)
				$result = 1;
		}
			
		return $result;
	}
	public function getMyCartDish(){
		$result			= 0;
		$user_model		= new User();
		$headers		= getallheaders();
		$token			= '';
		if(isset($headers['Authorization']))
			$token	= str_replace("Bearer ","",$headers['Authorization']);
		
		$user_info 		= [];
		$supplier 		= [];
		$post_data 		= Yii::$app->request->post();
		if(!empty($token))
		    $user_info		= $user_model->findIdentityByAccessToken($token);
		if(!empty($user_info)){
			$library			= new Library();
			$cart_data			= $library->json_decodeArray($user_info->cart);
			if(!empty($cart_data)){
				if(isset($cart_data[$this->supplier_id][$this->id]))
					$result = $cart_data[$this->supplier_id][$this->id]['quantity'];
			}
		}
			
		return $result;
	}
    public function getMyCart(){
        $result			= 0;
        if(!yii::$app->user->isGuest){
            $user_info = yii::$app->user->identity;
            if(!empty($user_info)){
                $library = new Library();
                $cart_data = $library->json_decodeArray($user_info->cart);
                if(!empty($cart_data)){
                    if(isset($cart_data[$this->supplier_id][$this->id]))
                        $result = $cart_data[$this->supplier_id][$this->id]['quantity'];
                }
            }
        }
        return $result;
    }

	public function getDishData(){
        $json['dish_id'] 			= $this->id;
        $json['dish_status'] 		= $this->status;
        $json['dish_name'] 			= $this->dish_name;
        $json['dish_description'] 	= $this->dish_description;
        $json['dish_price']			= $this->dish_price;
        $json['currency']			= config_default_currency;
        $json['dish_weight'] 		= $this->dish_weight;
        $json['total_review'] 		= $this->avgReviews;

        return $json;
    }
    public function getProductInfo1(){
        $dish_image	= '';
        if(!empty($this->dish_image) && $this->image_approval==1)
            $dish_image	= Url::to($this->getMenuImage($this->dish_image,600,400),true);

        $json['supplier_id'] = $this->supplier_id;
        $json['product_type'] = $this->product_type;
        $json['product_name'] = $this->dish_name;
        $json['product_description'] = $this->dish_description;
        $json['main_image'] = $dish_image;
        $json['currency'] = config_default_currency;
        $json['item_available'] = $this->dist_qty;
        $json['dist_qty'] = $this->dist_qty;
        $json['status'] = $this->status;
        $json['product_price'] = $this->dish_price;
        $json['product_id'] = 'P-'.$this->id;
        $json['menu_id'] = $this->id;
        return $json;
    }

    public function getDishInfo1(){
        $this->status               = 'inactive';
        $this->dist_qty               = 0;

        $time    = time();
        $check_today_schedule = MenuSchedules::find()->joinWith('menuScheduleItem as msc')->where(['supplier_id'=>$this->supplier_id,'schedule_date'=>date("Y-m-d")])->andWhere(['and',['<=','start_timestamp',$time],['>=','end_timestamp',$time],['msc.choices' => 'active','msc.menu_id'=>$this->id]])->one();
        if(!empty($check_today_schedule)){
            $this->status               = 'active';
            $this->dist_qty             = $check_today_schedule->menuScheduleItem->quantity;
        }

        $json['product_id'] 	    = 'P-'.$this->id;
        $json['supplier_id'] 		= $this->supplier_id;
        $json['product_type'] 		= $this->product_type;
        $json['dish_id'] 			= $this->id;
        $json['dish_status'] 		= $this->status;
        $json['dish_name'] 			= $this->dish_name;
        $json['dish_description'] 	= $this->dish_description;
        $json['dish_price']			= $this->dish_price;
        $json['currency']			= config_default_currency;

        $dish_image	= '';
        if(!empty($this->dish_image) && $this->image_approval==1)
            $dish_image	= Url::to($this->getMenuImage($this->dish_image,600,400),true);
        $json['dish_image']			= $dish_image;

        $json['item_available']		= $this->dist_qty;
        $json['dist_qty']			= $this->dist_qty;
        $json['status']				= $this->status;
        $json['menu_id'] 	    = $this->id;
        return $json;
    }
	public function getDishInfo(){
		$dish_image	= '';
		if(!empty($this->dish_image) && $this->image_approval==1)
			$dish_image	= Url::to($this->getMenuImage($this->dish_image,600,400),true);
		$fav_count					= UserFavMenu::find()->where(['menu_id'=>$this->id])->count();
		$myCartDish					= $this->myCartDish;

		$this->status               = 'inactive';
		$this->dist_qty               = 0;
		$time    = time();

//        $check_today_schedule = MenuSchedules::find()->joinWith('menuScheduleItems as msc')->where(['supplier_id'=>$this->supplier_id,'schedule_date'=>date("Y-m-d")])->andWhere(['and',['<=','start_timestamp',$time],['>=','end_timestamp',$time],['msc.choices' => 'active','msc.menu_id'=>$this->id]])->one();
//        if(!empty($check_today_schedule)){
//            $this->status               = 'active';
//            $this->dist_qty             = $check_today_schedule->menuScheduleItems->quantity;
//        }

        $check_today_schedule = MenuSchedules::find()->joinWith('menuScheduleItem as msc')->where(['supplier_id'=>$this->supplier_id,'schedule_date'=>date("Y-m-d")])->andWhere(['and',['<=','start_timestamp',$time],['>=','end_timestamp',$time],['msc.choices' => 'active','msc.menu_id'=>$this->id]])->one();
        if(!empty($check_today_schedule)){
            $this->status               = 'active';
            $this->dist_qty             = $check_today_schedule->menuScheduleItem->quantity;
        }
		$json['product_id'] 	    = 'P-'.$this->id;
        $json['product_type'] 		= $this->product_type;
		$json['supplier_id'] 		= $this->supplier_id;
		$json['dish_id'] 			= $this->id;
		$json['dish_status'] 		= $this->status;
		$json['dish_name'] 			= $this->dish_name;
		$json['dish_description'] 	= $this->dish_description;
		$json['dish_price']			= $this->dish_price;
		$json['currency']			= config_default_currency;
		$json['dish_weight'] 		= $this->dish_weight;
		$json['dish_since']			= $this->dish_since;
		$json['item_available']		= $this->dist_qty;
		$json['dist_qty']			= $this->dist_qty;
		$json['user_view'] 			= $this->view;
		$json['customer_view']		= $this->view;
		$json['dish_serve']			= $this->dish_serve;
		$json['is_fav']				= $this->myfavDish;
		$json['distance']				= $this->distance;

		$json['pending_order'] 		= 100;
		$json['delivered_order']	= 45;
		$json['rate_point']			= $this->avgReviews;
		//$json['total_review']		= $this->avgReviews;
		$json['status']				= $this->status;
		$json['dish_image']			= $dish_image;
		$json['fav_count']			= $fav_count;
		$json['cart_qty']			= $myCartDish;
        $json['last_review']		= $this->lastReview;
		$categories					= $this->menuCategories;
		$cuisines					= $this->menuCuisines;
		$meals						= $this->menuMeals;
        $json['edit_link']		= $this->editLink;
        $json['collected_amount'] = $this->collected_amount;
        $json['type_of_product'] = $this->type_of_product;
        $json['discount'] = $this->discount;

		if(!empty($categories)){
			foreach($categories as $category){
				$cat_desc		= $category->category;
				if(!empty($cat_desc))
                    $getCat = CategoryDescription::find()->where(['category_id'=>$category->category_id])->one();

                UserFavMenu::find()->where(['menu_id'=>$this->id])->count();

					$json['categories'][] = ['menu_id'=>$category->menu_id,'category_id'=>$category->category_id,'detail'=>['id'=>$category->category_id,'category_name'=>$getCat->category_name]];
			}
		}else
			$json['categories']		= [];
		
		if(!empty($cuisines)){
			foreach($cuisines as $cuisine){
				$cuisine_desc		= $cuisine->cuisine;
				if(!empty($cuisine_desc))
					$json['cuisines'][] = ['menu_id'=>$cuisine->menu_id,'cuisine_id'=>$cuisine->cuisine_id,'cuisine_detail'=>['id'=>$cuisine->cuisine_id,'cuisine_name'=>$cuisine_desc->cuisine_name]];
			}
		}else
			$json['cuisines']		= [];
		
		if(!empty($meals)){
			foreach($meals as $meal){
				$meal_desc		= $meal->meal;
				if(!empty($meal_desc))
					$json['meals'][] = ['menu_id'=>$meal->menu_id,'meal_id'=>$meal->meal_id,'meal_detail'=>['id'=>$meal->meal_id,'meal_name'=>$meal_desc->meal_name]];
			}
		}else
			$json['meals']		= [];
        $json['total_orders']        = 0;

        $user_info		= [];
        $token			= '';
        $user_model		= new User();
        $headers		= getallheaders();
        if(isset($headers['Authorization']))
            $token	= str_replace("Bearer ","",$headers['Authorization']);
        if(!empty($token))
            $user_info		= $user_model->findIdentityByAccessToken($token);

        if (!empty($user_info)) {
            $user_id = $user_info->id;
            $count = Order::find()->join('LEFT JOIN','tbl_order_products as op','op.order_id=tbl_order.order_id')->where(['and',['user_id'=>$user_id],['order_status_id'=>5,'op.menu_id'=>$this->id]])->count();
            $json['total_orders']        = (int)$count;
        }
		return $json;
	}
    public function getAvgReviews(){
        $total		= MenuRating::find()->select('avg(rating) as rating')->where(['menu_id'=>$this->id])->one();
        if(!empty($total))
            return round($total->rating,2);
        return 0;
    }
    public function beforeSave($insert){
        if($this->isNewRecord){
            $user_info = Yii::$app->user->identity;
            $this->city_id = $user_info->city;
            $this->product_type = $user_info->supplier_type;
            $this->test_data = $user_info->is_test_user;
            $this->supplier_id = $user_info->id;
        }
        $discount = 0;
        if(!empty($this->discount))
            $discount = $this->discount;
        $price = $this->dish_price;
        $percentage = config_transaction_fee+config_freshhommee_fee+config_vat;
        if(empty($discount)){
            $price = $price - ($price*$discount/100);
        }
        $price = $price - ($price*$percentage/100);
        $this->collected_amount = $price;

        return parent::beforeSave($insert);
    }

    public function getMainImage($sizeX=200,$sizeY=200){
        if(!empty($this->dish_image))
            $image          = '../../frontend/web/uploads/'.$this->dish_image;
        else
            $image          = '../../frontend/web/uploads/no_image.png';
        $p_image    = EasyThumbnailImage::thumbnailFileUrl(
            $image,$sizeX,$sizeY,EasyThumbnailImage::THUMBNAIL_INSET
        );
        return $p_image;
    }
    public function getImage($path,$sizeX=200,$sizeY=200){
        $image          = '../../frontend/web/uploads/'.$path;
        $p_image    = EasyThumbnailImage::thumbnailFileUrl(
            $image,$sizeX,$sizeY,EasyThumbnailImage::THUMBNAIL_INSET
        );
        return $p_image;
    }

    public function getProUrl(){
        return Url::to(Yii::$app->request->baseUrl.'/product/'.$this->id.'-'.$this->slug);
    }

    public function getFavDish(){ // for website
        $result		= 0;
        if (!Yii::$app->user->isGuest){
            $count		= UserFavMenu::find()->where(['menu_id'=>$this->id,'user_id' => Yii::$app->user->identity->id])->count();
            if($count>0)
                $result = 1;
        }
        return $result;
    }

    public function getMenuImages(){
        return $this->hasMany(MenuToImages::className(), ['menu_id' => 'id']);
    }

    public function getProductInfo(){
        $product_images = $this->menuImages;
        $other_images = [];
        if(!empty($product_images)){
            foreach($product_images as $product_image)
                $other_images[] = Url::to($this->getMenuImage($product_image->image_name,600,400),true);
        }
        $dish_image	= '';
        if(!empty($this->dish_image) && $this->image_approval==1)
            $dish_image	= Url::to($this->getMenuImage($this->dish_image,600,400),true);
        $fav_count = UserFavMenu::find()->where(['menu_id'=>$this->id])->count();
        $myCartDish	= $this->myCartDish;
        $json['supplier_id'] 		= $this->supplier_id;
        $json['product_name'] = $this->dish_name;
        $json['product_description'] = $this->dish_description;
        $json['main_image'] = $dish_image;
        $json['product_price'] = $this->dish_price;
        $json['currency'] = config_default_currency;
        $json['user_view'] = $this->view;
        $json['customer_view']	= $this->view;
        $json['is_fav']	= $this->myfavDish;
        $json['total_review'] = $this->avgReviews;
        $json['status'] = $this->status;
        //$json['dish_image'] = $dish_image;
        $json['fav_count'] = $fav_count;
        $json['cart_qty'] = $myCartDish;
        $json['other_images'] = $other_images;
        $json['quantity'] = $this->dist_qty;
        $json['product_id'] = $this->id;
        $json['pending_order'] 		= 100;
        $categories					= $this->menuCategories;
        $json['delivered_order']	= 45;
        $json['rate_point']			= $this->avgReviews;
        $json['last_review']		= $this->lastReview;
        $json['edit_link']		= $this->editLink;
        $json['product_attributes']		= $this->productAttributes;
        $json['collected_amount']		= $this->collected_amount;
        $json['type_of_product']		= $this->type_of_product;
        $json['discount']		= $this->discount;

        if(!empty($categories)){
            foreach($categories as $category){
                $cat_desc		= $category->category;
                if(!empty($cat_desc))
                    $json['categories'][] = ['menu_id'=>$category->menu_id,'category_id'=>$category->category_id,'detail'=>['id'=>$category->category_id,'category_name'=>$cat_desc->name]];
            }
        }else
            $json['categories']		= [];

        return $json;
    }

    public function getPendingOrders(){
        return 10;
    }

    public function getLastReview(){
        $menu_rating = MenuRating::find()->where(['menu_id'=>$this->id])->orderBy('id desc')->one();
        $rating = null;
        $review = null;
        if(!empty($menu_rating)){
            $rating = $menu_rating->rating;
            $review = $menu_rating->reviews;
            $output['rating'] = $rating;
            $output['review'] = $review;
            $output['time'] = Yii::$app->formatter->format($menu_rating->created_at, 'relativeTime');
            $output['user_info'] = $menu_rating->user->userDetail;

            return $output;
        }
        return (object)[];
    }

    protected function getEditLink(){
        $user_model		= new User();
        $headers		= getallheaders();
        $user_info		= [];
        $token			= '';
        if(isset($headers['Authorization']))
            $token	= str_replace("Bearer ","",$headers['Authorization']);
        if(!empty($token))
            $user_info		= $user_model->findIdentityByAccessToken($token);

        if (!empty($user_info)) {

            $product = Menu::find()->where(['id' => $this->id,'supplier_id' => $user_info->id])->one();
            if(!empty($product)){
                return Yii::$app->params['website_path'].'/auth-user?token='.$user_info->auth_key.'&product_id='.$this->id;
            }
        }
        return;
    }

    public function getProductAttributes()
    {
        $all_attributes = $this->hasMany(MenuToAttributes::className(), ['menu_id' => 'id'])->all();
        $options = [];
        if(!empty($all_attributes)){
            $language_id = language_id;
            foreach($all_attributes as $all_attribute){
                $option = Option::find()->joinWith('optionDescription as od')->where(['tbl_option.status' => 1,'tbl_option.option_id' => $all_attribute->option_id,'od.language_id' => $language_id])->asArray()->one();
                if(!empty($option)){
                    $option_data = [];
                    $attr_options = MenuToAttributeOptions::find()->where(['menu_id' => $this->id,'menu_attr'=>$all_attribute->menu_attr])->asArray()->all();
                    if(!empty($attr_options)){
                        foreach($attr_options as $attr_option){
                            $option_data[] = ['name' => $attr_option['name'],'quantity' => $attr_option['quantity'],'subtract' => $attr_option['subtract'],'price'=>$attr_option['price'],'price_prefix'=>$attr_option['subtract']==0 ? '+' : "-"];
                        }
                        $options[] = ['name'=>$option['optionDescription']['name'],'option_id'=>$option['option_id'],'type' => $option['type'],'attribute_id' => $all_attribute->menu_id,'is_required' => $all_attribute->is_required,'menu_attr' => $all_attribute->menu_attr,'option_data' => $option_data];
                    }

                }

            }
        }
       return $options;
    }

    public function getProductPrice(){
        $library = new Library();
        $price = $library->currencyFormat($this->dish_price,config_default_currency);
        $prices['price']['amount'] = $price;
        $prices['price']['value'] = $this->dish_price;
        return $prices;
    }

    public function validateCart($post_data=null){

        if($this->product_type==1 ){
            $post_data = null;
        }
        $json = [];
        if($post_data==null)
            return $json;
        $quantity = $post_data['quantity'];
        if($quantity>$this->dist_qty){
            $qty = ' quantity is ';
            if($this->dist_qty>1)
                $qty = ' quantities are ';
            $json['error'] = 'Only "'.$this->dist_qty.'" '.$qty.'  are left.';
            return $json;
        }
        if($this->dist_qty==0){
            $json['error'] = 'Sorry this product is not available.';
            return $json;
        }
        $menu_attributes = MenuToAttributes::find()->where(['menu_id' => $this->id,'is_required' => 'yes'])->all();
        $is_error = false;
        if(!empty($menu_attributes)){
            foreach($menu_attributes as $menu_attribute){
                $option_id = $menu_attribute->option_id;
                $value = $post_data['product_attributes'][$option_id] ?? '';
                if(empty($value)){
                    $is_error = true;
                }else{
                    $menu_to_opt = MenuToAttributeOptions::find()->where(['name' => $value,'menu_id' => $this->id])->one();
                    if(empty($menu_to_opt)){
                        $is_error = true;
                    }else{
                        if($menu_to_opt->quantity==0 || $menu_to_opt->quantity<$quantity){
                            $language_id = language_id;
                            $option_data = Option::find()->select('*')->joinWith('optionDescription as od')->where(['od.language_id' => $language_id,'tbl_option.option_id' => $option_id])->asArray()->one();
                            if($quantity > 1)
                                $json['error'] = 'Sorry '.$quantity.' quantities are not available for '.$menu_to_opt->name.' '.$option_data['optionDescription']['name'];
                            else
                                $json['error'] = 'Sorry '.$quantity.' quantity is not available for '.$menu_to_opt->name.' '.$option_data['optionDescription']['name'];
                            return $json;
                        }
                    }
                }
                if($is_error==true){
                    $language_id = language_id;
                    $option_data = Option::find()->select('*')->joinWith('optionDescription as od')->where(['od.language_id' => $language_id,'tbl_option.option_id' => $option_id])->asArray()->one();
                    $json['error'] = 'Please select '.$option_data['optionDescription']['name'];

                    return $json;
                }
            }
        }
        return $json;
    }

    public function getFinalPrice(){
        $format = yii::$app->formatter;

        $data['discount_price'] = 0;
        $data['discount_price_label'] = '';

        $data['discount'] = $this->discount+0;

        $data['product_price'] = $this->dish_price+0;
        $data['product_price_label'] = $format->asCurrency($this->dish_price,config_default_currency);

        $data['real_price'] = $this->dish_price+0;
        $data['real_price_label'] = $format->asCurrency($this->dish_price,config_default_currency);

        if($this->discount > 0){
            $discount_price = $this->calculatePercentage($this->dish_price,$this->discount);
            $data['discount_price'] = round($discount_price,2)+0;
            $data['discount_price_label'] = $format->asCurrency($data['discount_price'],config_default_currency);

            $data['real_price'] = $data['discount_price'];
            $data['real_price_label'] = $data['discount_price_label'];
        }
        return $data;
    }
    protected function calculatePercentage($price,$percent,$value=false){
        if($value==false)
            return $price - (($percent/100)*$price);
        return ($percent/100)*$price;
    }
}
