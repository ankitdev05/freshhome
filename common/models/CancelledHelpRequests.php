<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%cancelled_help_requests}}".
 *
 * @property int $id
 * @property int $request_id
 * @property string $by_owner
 * @property string $by_sales_person
 * @property int $reason_id
 * @property string $message
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CancelReason $request
 */
class CancelledHelpRequests extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cancelled_help_requests}}';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'by_owner', 'by_sales_person', 'message'], 'required'],
            [['reason_id'], 'required','on'=>'by_supplier'],
            [['request_id', 'reason_id', 'created_at', 'updated_at'], 'integer'],
            [['by_owner', 'by_sales_person', 'message'], 'string'],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => HelpRequests::className(), 'targetAttribute' => ['request_id' => 'request_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'request_id' => Yii::t('app', 'Request ID'),
            'by_owner' => Yii::t('app', 'By Owner'),
            'by_sales_person' => Yii::t('app', 'By Sales Person'),
            'reason_id' => Yii::t('app', 'Reason ID'),
            'message' => Yii::t('app', 'Message'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(CancelReason::className(), ['reason_id' => 'request_id']);
    }
}
