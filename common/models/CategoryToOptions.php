<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%category_to_options}}".
 *
 * @property int $id
 * @property int $category_id
 * @property int $option_id
 * @property string $is_required
 *
 * @property Category $category
 * @property Option $option
 */
class CategoryToOptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category_to_options}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'option_id', 'is_required'], 'required'],
            [['category_id', 'option_id'], 'integer'],
            [['is_required'], 'string'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['option_id'], 'exist', 'skipOnError' => true, 'targetClass' => Option::className(), 'targetAttribute' => ['option_id' => 'option_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'option_id' => Yii::t('app', 'Option ID'),
            'is_required' => Yii::t('app', 'Is Required'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(Option::className(), ['option_id' => 'option_id']);
    }
}
