<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\base\Model;
use common\models\Notifications;
use common\models\DriverOrderStatus;

class Library extends Model{
	
    public function curl_response($url){
        $ch 			= curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        $result=curl_exec($ch);
        curl_close($ch);
        
        return json_decode($result, true);
    }
	public function json_encodeArray($array){
	    return json_encode($array);
	}
	
	public function json_decodeArray($array){
	    return (array)json_decode($array,true);
	}
	public function saveFile($file_obj, $destination_folder = null) {
		if(!empty($file_obj)){
			$fullurl 		= Yii::getAlias('@frontend').'/web/uploads/';
			
			if(!file_exists($fullurl)) {
				mkdir($fullurl, 0755);
			}
			
			$fullurl .= $destination_folder . "/";
			
			if($destination_folder && !file_exists($fullurl)) {
				mkdir($fullurl, 0755);
			}
			$explode	= explode('.',$file_obj->name);
			$file_ext	= end($explode);
			$fileName	= md5(rand().time().$file_obj->name).'.'.$file_ext;
			
			$file_obj->saveAs($fullurl.$fileName);
			return $destination_folder . "/" . $fileName;
		}
		return null;
	}

	public function saveNotification($data,$opn_device=false){
        $model              = new Notifications();
        $model->user_id     = $data['user_id'];
        $model->role        = $data['role'];
        $model->from_user   = $data['from_user'];
        $model->type        = $data['type'];
        $model->config_name = $data['config_name'];
        $model->config_value    =json_encode($data['config_value']);
        $model->is_read         = 0;
        $model->status = isset($data['status']) ? $data['status'] : 1;
        $model->reason = isset($data['reason']) ? $data['reason'] : "";
        $model->request_id = isset($data['request_id']) ? $data['request_id'] : null;
        $model->request_status = isset($data['request_status']) ? $data['request_status'] : null;
        $model->request_type = isset($data['request_type']) ? $data['request_type'] : null;
        $model->save();
        if($model->status==1){
            if( $opn_device==true){
                $device_id = $data['config_value']['device_token'];
            }else{
                $user_info = User::findOne($model->user_id);
                if($user_info->device_token!=""){
                    $device_id = $user_info->device_token;
                }
            }
            if(!empty($device_id)){
                $serverKey = Yii::$app->params['fcm_server_key'];
                $senderId  = Yii::$app->params['fcm_sender_id'];

                $client = new \Fcm\FcmClient($serverKey, $senderId);
                $notification = new \Fcm\Push\Notification();

                $data1['type'] = $model->type;
                $data1['data'] = $data['config_value'];
                $notification
                    ->addRecipient($device_id)
                    ->setTitle(' ')
                    ->setBody($data['config_name'])
                    ->addData('request_type',$data1['type']);
                foreach($data1['data'] as $k=>$data){
                    $notification->addData($k, $data);
                }
                $notification->addData('user_type',$model->role);
                $notification->addData('request_status',$model->request_status);
                $response = $client->send($notification);


//                $notification = $client->pushData($data1, $device_id);
//                $response = $client->send($notification);
//                $message = print_r($response,true);
//                Yii::$app->mailer->compose()
//                    ->setHtmlBody($message)
//                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
//                    ->setTo('rahuloberai@icloud.com')
//                    ->setSubject('Notification')
//                    ->send();
            }
        }
    }
    public function updateDriverHistory($data){
        $model = new DriverOrderStatus();
        foreach($data as $key=>$result)
            $model->{$key} = $result;
        $model->save();
    }

    public function getCCyears(){
        $years	= [];
        $current_date	= date('Y');
        for($i=date('y');$i<=30;$i++){
            $years[$i] = $current_date;
            $current_date++;
        }
        return $years;
    }
    public function getCCmonths(){
        $months	= [];

        for($i=1;$i<=12;$i++){
            $month			= date('m',mktime(0,0,0,$i,'1','1970'));
            $month_name 	= date('F',mktime(0,0,0,$i,'1','1970'));
            $months[$month] = $month.' - '.$month_name;
        }
        return $months;
    }

    public function currencyFormat($price,$currency){
        $formatter = \Yii::$app->formatter;
        return $formatter->asCurrency($price,$currency);
    }
    public function weeks($month, $year){
        $firstday = date("w", mktime(0, 0, 0, $month, 1, $year));
        $lastday = date("t", mktime(0, 0, 0, $month, 1, $year));
        if ($firstday!=0) $count_weeks = 1 + ceil(($lastday-8+$firstday)/7);
        else $count_weeks = 1 + ceil(($lastday-1)/7);
        return $count_weeks;
    }
    function getStartAndEndDate($week, $year) {
        $dto = new \DateTime();
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('Y-m-d');
        $dto->modify('+6 days');
        $ret['week_end'] = $dto->format('Y-m-d');
        return $ret;
    }

    public function convertMonthNumberToName($monthNum){
        $dateObj   = \DateTime::createFromFormat('!m', $monthNum);
        return $dateObj->format('F');
    }
}