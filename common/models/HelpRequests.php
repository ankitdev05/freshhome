<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%help_requests}}".
 *
 * @property int $request_id
 * @property string $name
 * @property string $location
 * @property string $phonenumber
 * @property string $lat
 * @property string $lng
 * @property string $device_token
 * @property int $accepted_by
 * @property int $request_status
 * @property int $request_type
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $acceptedBy
 * @property OrderStatus $requestStatus
 * @property MainCategory $requestType
 */
class HelpRequests extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%help_requests}}';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'location', 'phonenumber', 'lat', 'lng',  'request_type','email'], 'required'],
            [['email'], 'email'],
            [['location', 'lat', 'lng', 'device_token','rating_complete','source_type'], 'string'],
            [['accepted_by', 'request_status', 'request_type', 'created_at', 'updated_at'], 'integer'],
            [['name', 'phonenumber'], 'string', 'max' => 255],
            [['accepted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['accepted_by' => 'id']],
            [['request_status'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['request_status' => 'order_status_id']],
            [['request_type'], 'exist', 'skipOnError' => true, 'targetClass' => MainCategories::className(), 'targetAttribute' => ['request_type' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_id' => Yii::t('app', 'Request ID'),
            'name' => Yii::t('app', 'Name'),
            'location' => Yii::t('app', 'Location'),
            'phonenumber' => Yii::t('app', 'Phone number'),
            'lat' => Yii::t('app', 'Latitude'),
            'lng' => Yii::t('app', 'Longitude'),
            'device_token' => Yii::t('app', 'Device Token'),
            'accepted_by' => Yii::t('app', 'Accepted By'),
            'request_status' => Yii::t('app', 'Request Status'),
            'request_type' => Yii::t('app', 'Request Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'accepted_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['order_status_id' => 'request_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestType()
    {
        return $this->hasOne(MainCategory::className(), ['id' => 'request_type']);
    }

    public function getSalesperson($user=[]){
        $latitude = $this->lat;
        $longitude = $this->lng;
        $query      = User::find()->select(new Expression("*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $latitude - latitude) *  pi()/180 / 2), 2) +COS( $latitude * pi()/180) * COS(latitude * pi()/180) * POWER(SIN(( $longitude - longitude) * pi()/180 / 2), 2) ))) as distance  "))->where(['and',['is_sales_person'=>'yes'],['status'=>10],['!=','longitude',''],['!=','latitude','']]);
        if(!empty($user))
            $query->andWhere(['!=','id',$user->id]);
        $query->having("distance <= 200")->orderBy("distance");
        $results = $query->all();
        return $results;
    }

    public function sendEmail($message){

        $subject = 'Help request update | '.config_name;
        $message1 = '<p>Hi '.$this->name.',</p>';
        $message1 .= '<p>'.$message.'</p>';
        $message1 .= '<p>Thanks,</p>';
        $message1 .= '<p>'.config_name.'</p>';
        return  Yii::$app->mailer->compose()
            ->setHtmlBody($message1)
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject($subject)
            ->send();
    }
}
