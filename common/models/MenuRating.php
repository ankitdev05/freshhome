<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%menu_rating}}".
 *
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property int $supplier_id
 * @property int $menu_id
 * @property string $rating
 * @property int $reviews
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Menu $menu
 * @property User $user
 * @property User $supplier
 * @property Order $order
 */
class MenuRating extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_rating}}';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id','menu_id'], 'required'],
            [['order_id', 'user_id', 'supplier_id', 'menu_id', 'created_at', 'updated_at'], 'integer'],
            [['rating'], 'number'],
            [['reviews'], 'string'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['supplier_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'order_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'supplier_id' => Yii::t('app', 'Supplier ID'),
            'menu_id' => Yii::t('app', 'Menu ID'),
            'rating' => Yii::t('app', 'Rating'),
            'reviews' => Yii::t('app', 'Reviews'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(User::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
    public function getRatingInfo(){
        $json['review'] = $this->reviews;
        $json['time'] = Yii::$app->formatter->format($this->created_at, 'relativeTime');
        $json['uploaded_by'] = $this->user->userDetail;
        return $json;
    }
}
