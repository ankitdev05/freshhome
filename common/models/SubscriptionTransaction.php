<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%subscription_transaction}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $payment_token
 * @property string $subscription_id
 * @property string $transaction_id
 * @property string $transaction_status
 * @property string $transaction_currency
 * @property string $transaction_amount
 * @property string $full_response
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class SubscriptionTransaction extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%subscription_transaction}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['transaction_amount'], 'number'],
            [['full_response','type'], 'string'],
            [['payment_token', 'subscription_id', 'transaction_id', 'transaction_status', 'transaction_currency'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'payment_token' => Yii::t('app', 'Payment Token'),
            'subscription_id' => Yii::t('app', 'Subscription ID'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'transaction_status' => Yii::t('app', 'Transaction Status'),
            'transaction_currency' => Yii::t('app', 'Transaction Currency'),
            'transaction_amount' => Yii::t('app', 'Transaction Amount'),
            'full_response' => Yii::t('app', 'Full Response'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
