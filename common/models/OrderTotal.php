<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_total}}".
 *
 * @property int $order_total_id
 * @property int $order_id
 * @property string $code
 * @property string $title
 * @property string $value
 * @property int $sort_order
 *
 * @property Order $order
 */
class OrderTotal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_total}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'code', 'title', 'value', 'sort_order'], 'required'],
            [['order_id', 'sort_order'], 'integer'],
            [['value'], 'number'],
            [['code'], 'string', 'max' => 32],
            [['title'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'order_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_total_id' => Yii::t('app', 'Order Total ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'code' => Yii::t('app', 'Code'),
            'title' => Yii::t('app', 'Title'),
            'value' => Yii::t('app', 'Value'),
            'sort_order' => Yii::t('app', 'Sort Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
