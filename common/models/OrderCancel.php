<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%order_cancel}}".
 *
 * @property int $id
 * @property int $order_id
 * @property int $cancelled_by
 * @property string $by_supplier
 * @property string $by_user
 * @property string $reason
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $cancelledBy
 * @property Order $order
 */
class OrderCancel extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_cancel}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'cancelled_by', 'by_supplier', 'by_user', 'reason'], 'required'],
            [['order_id', 'cancelled_by', 'created_at', 'updated_at'], 'integer'],
            [['by_supplier', 'by_user', 'reason'], 'string'],
            [['cancelled_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['cancelled_by' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'order_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'cancelled_by' => Yii::t('app', 'Cancelled By'),
            'by_supplier' => Yii::t('app', 'By Supplier'),
            'by_user' => Yii::t('app', 'By User'),
            'reason' => Yii::t('app', 'Reason'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCancelledBy()
    {
        return $this->hasOne(User::className(), ['id' => 'cancelled_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
