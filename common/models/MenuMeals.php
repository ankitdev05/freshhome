<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%menu_meals}}".
 *
 * @property int $id
 * @property int $menu_id
 * @property int $meal_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Meal $meal
 * @property Menu $menu
 */
class MenuMeals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_meals}}';
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id', 'meal_id'], 'required'],
            [['menu_id', 'meal_id', 'created_at', 'updated_at'], 'integer'],
            [['meal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meal::className(), 'targetAttribute' => ['meal_id' => 'id']],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'menu_id' => Yii::t('app', 'Menu ID'),
            'meal_id' => Yii::t('app', 'Meal ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeal()
    {
		$language_id = 1;
		if(isset($_REQUEST['language_id']))
			$language_id = (int)$_REQUEST['language_id'];
        return $this->hasOne(Meal::className(), ['id' => 'meal_id'])->select('*')->where(['md.language_id'=>$language_id])->join('INNER JOIN','tbl_meal_description as md','md.meal_id=tbl_meal.id');;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }
}
