<?php

namespace common\models;
use yii\base\Model;

class Filters extends Model{

    public $meals;
    public $title;
    public $cuisines;
    public $categories;
    public $city;
    public $sort;
    public $screen_id;
    public $start_date;
    public $end_date;
    public $report_type;
    public function rules()
    {
        return [
            [['categories', 'cuisines', 'meals', 'title','city','sort','screen_id','start_date','end_date','report_type'], 'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            [['categories', 'cuisines', 'meals', 'title','city','sort','screen_id','start_date','end_date','report_type'], 'safe'],
            [['categories', 'cuisines', 'meals', 'title','city','sort','screen_id','start_date','end_date','report_type'], 'trim']
        ];

    }
}
