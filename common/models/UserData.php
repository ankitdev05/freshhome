<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%user_data}}".
 *
 * @property int $user_data_id
 * @property int $user_id
 * @property string $name
 * @property string $phone_number
 * @property string $dob
 * @property string $profile_pic
 * @property int $image_approval
 * @property string $description
 * @property int $occupation_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 * @property Occupation $occupation
 */
class UserData extends \yii\db\ActiveRecord
{
    public $verification_code;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_data}}';
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'gender','dob','occupation_id'], 'trim'],
            [['user_id', 'name', 'gender','dob','occupation_id'], 'required','on'=>'updateprofile'],
            [['user_id', 'name', 'gender','dob','occupation_id','nationality'], 'required','on'=>'update_profile'],
            [['user_id', 'name', 'gender','dob','occupation_id','gender'], 'required','on'=>'user_create'],
            [['user_id', 'image_approval', 'occupation_id', 'created_at', 'updated_at'], 'integer'],
            [['dob','gender'], 'safe'],
            [['description','nationality','gender'], 'string'],
            [['name', 'phone_number', 'profile_pic'], 'string', 'max' => 255],
            [['phone_number'],'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['occupation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Occupation::className(), 'targetAttribute' => ['occupation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_data_id' => Yii::t('app', 'User Data ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'dob' => Yii::t('app', 'Dob'),
            'profile_pic' => Yii::t('app', 'Profile Pic'),
            'image_approval' => Yii::t('app', 'Image Approval'),
            'description' => Yii::t('app', 'About Me'),
            'occupation_id' => Yii::t('app', 'Occupation'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOccupation()
    {
        $language_id = 1;
		if(isset($_REQUEST['language_id']))
			$language_id = (int)$_REQUEST['language_id'];
		
        return $this->hasOne(Occupation::className(), ['id' => 'occupation_id'])->select('*')->where(['od.language_id'=>$language_id])->join('LEFT JOIN','tbl_occupation_description as od','od.occupation_id=tbl_occupation.id');
    }
	public function getCountryInfo()
    {
        return $this->hasOne(Country::className(), ['code' => 'nationality']);
    }
}
