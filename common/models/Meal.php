<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%meal}}".
 *
 * @property int $id
 * @property string $meal_name
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property MenuMeals[] $menuMeals
 */
class Meal extends \yii\db\ActiveRecord
{
	public $meal_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%meal}}';
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meal_name', 'status'], 'required'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['meal_name'], 'trim'],
			[['meal_name'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'meal_name' => Yii::t('app', 'Meal Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuMeals()
    {
        return $this->hasMany(MenuMeals::className(), ['meal_id' => 'id']);
    }
	public function getMealDescription()
    {
        return $this->hasMany(MealDescription::className(), ['meal_id' => 'id']);
    }
	
	public function getMealName()
    {
        return $this->hasOne(MealDescription::className(), ['meal_id' => 'id']);
    }
    public function getMealImage(){
        if(!empty($this->image))
            return Yii::$app->params['website_path'].'/uploads/'.$this->image;
    }
}
