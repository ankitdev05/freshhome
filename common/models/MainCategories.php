<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%main_categories}}".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $status
 */
class MainCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%main_categories}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'status'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getCategoryDescription()
    {
        return $this->hasOne(MainCategoriesDescription::className(), ['category_id' => 'id']);
    }
}
