<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use himiklab\thumbnail\EasyThumbnailImage;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class Apiuser extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;
	public $password;
	public $gender;
	public $new_password;
	public $document;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

			[['username','name','phone_number','email','password','latitude','longitude','location','dob','building_no','flat_no','floor_no','landmark','city','role'],'required','on'=>'register'],

			[['username','name','phone_number','email','password','latitude','longitude','location','dob','city','role','document'],'required','on'=>'driver_register'],

            [['username','password','email','phone_number'],'required','on'=>'sales_reg'],

			[['username','name','phone_number','email','password','latitude','longitude','location','dob','building_no','flat_no','floor_no','landmark','description','availability','nationality','city','temp_password','role','is_user','is_supplier','ip_address','last_login','last_login_ip','occupation_id','gender','cart','refer_by','device_token','supplier_type','last_logged_in_as','is_test_user','is_driver','document','profile_pic','is_available'],'safe'],

        //    [['document'], 'file','skipOnEmpty' => false, 'extensions' => ['jpeg','jpg', 'png', 'pdf', 'doc','docx'],'checkExtensionByMimeType'=>false,'on'=>'driver_register'],

            [['email'],'email'],

            [['password_hash','header_image','supplier_header_image','is_available'],'safe'],

            [['email','username'],'unique'],

            ['status', 'default', 'value' => self::STATUS_INACTIVE],

            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],

            [['phone_number'],'required','on'=>'checknumber'],
            [['phone_number'],'checkPhone'],

            [['password'], 'check_pass','on'=>'changepassword'],

            [['password','new_password'],'required','on'=>'changepassword'],

            [['name','phone_number','availability','username','name','email','building_no','flat_no','floor_no','landmark','city'],'required','on'=>'updateprofile'],

            [['name','city','dob','location','latitude','longitude','is_available'],'required','on'=>'driverupdate'],

            [['email','name'],'required','on'=>'gmaillogin'],
        ];
    }
    public function checkPhone($attribute, $params){
        if (Yii::$app->user->isGuest){
            $checkPhone = User::find()->where(['phone_number'=>$this->phone_number,'status'=>10])->count();
            if($checkPhone > 0){
                $this->addError($attribute, Yii::t('app', 'This phone number has already been taken.'));
            }
        }else{
            $checkPhone = User::find()->where(['and',['phone_number'=>$this->phone_number],['status'=>10],['NOT IN','id',Yii::$app->user->id]])->count();
            if($checkPhone > 0){
                $this->addError($attribute, Yii::t('app', 'This phone number has already been taken.'));
            }
        }
    }
	public function check_pass($attribute, $params){
		if(!Yii::$app->security->validatePassword($this->password,Yii::$app->user->identity->password_hash))
			$this->addError($attribute,'Please enter correct password.');
	}
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
	public static function findByPhonenumber($phone_number)
    {
        return static::findOne(['phone_number' => $phone_number,'status' => 10]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username,$type=null)
    {
		if(filter_var($username, FILTER_VALIDATE_EMAIL)) 
			return static::findOne(['email' => $username]);
		return static::findOne(['username' => $username]);
		if($type===null)
			return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
		else
			return static::findOne(['username' => $username,'role'=>$type, 'status' => self::STATUS_ACTIVE]);
    }
	public static function findByUsernameByRole($username,$type=null)
    {		
		if(filter_var($username, FILTER_VALIDATE_EMAIL)){
			if($type==1)
				return static::findOne(['email' => $username,'status' => self::STATUS_ACTIVE,'is_user'=>'yes']);
			if($type==2)
				return static::findOne(['email' => $username,'status' => self::STATUS_ACTIVE,'is_supplier'=>'yes']);
            if($type==4)
                return static::findOne(['email' => $username,'status' => self::STATUS_ACTIVE,'is_driver'=>'yes']);
            if($type==5){
                $sale = new SalesPerson();
                $u_data =  static::findOne(['email' => $username,'status' => self::STATUS_ACTIVE]);
                if(!empty($u_data)){
                    $sale_data = $sale::findOne($u_data->id);
                    if(!$sale_data)
                        return;
                }
                return $u_data;
            }

		}
		if($type==1)
			return static::findOne(['username' => $username,'status' => self::STATUS_ACTIVE,'is_user'=>'yes']);
		if($type==2)
			return static::findOne(['username' => $username,'status' => self::STATUS_ACTIVE,'is_supplier'=>'yes']);
        if($type==4)
            return static::findOne(['username' => $username,'status' => self::STATUS_ACTIVE,'is_driver'=>'yes']);
        if($type==5){
            $sale = new SalesPerson();
            $u_data =  static::findOne(['username' => $username,'status' => self::STATUS_ACTIVE]);
            if(!empty($u_data)){
                $sale_data = $sale->findOne(['user_id'=>$u_data->id]);
                if(!$sale_data)
                    return;
            }
            return $u_data;
        }
		if($type===null)
			return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
		else{
			echo $type;
			if($type==1)
				return static::findOne(['username' => $username,'is_user'=>'yes', 'status' => self::STATUS_ACTIVE]);
			if($type==2)
				return static::findOne(['username' => $username,'is_supplier'=>'yes', 'status' => self::STATUS_ACTIVE]);
            if($type==4)
                return static::findOne(['username' => $username,'status' => self::STATUS_ACTIVE,'is_driver'=>'yes']);

		}
			
    }
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRole()
    {
        return $this->hasOne(UserRole::className(), ['role_id' => 'role']);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {

        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
	public function sendVerifyEmail($model){
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $model]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
	}
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getCityInfo()
    {
        return $this->hasOne(City::className(), ['id' => 'city']);
    }
	 public function getCountryInfo()
    {
        return $this->hasOne(Country::className(), ['code' => 'nationality']);
    }
	public function getUserImage($profile_pic,$sizex,$sizey){
		
		$image          = '../../frontend/web/uploads/'.$profile_pic;
		$p_image    = EasyThumbnailImage::thumbnailFileUrl(
				$image,$sizex,$sizey,EasyThumbnailImage::THUMBNAIL_INSET
			);
		return $p_image;
	}
	public function sendGmailEmail($email,$password,$user){
		$subject = 'New account registration for '.config_name;
		$message	= '<p>Dear '.$user->username.'</p>';
		$message	.= '<h3>Welcome to '.config_name.'</h3>';
		$message	.= '<p>Your access Details</p>';
		$message	.= '<p>Username: '.$user->username.'</p>';
		$message	.= '<p>Password: '.$password.'</p>';
		$message	.= '<p>Thanks again for joining the '.config_name.' and feel free to contact us at any time if you have any questions or issues on our website.</p>';
		$message	.= '<p>Thanks,<br/>'.config_name.' Team</p>';
		
		
		Yii::$app->mailer->compose()
			->setHtmlBody($message)
			->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
			->setTo($email)
			->setSubject($subject)
			->send();
	}
	public function getUpdateUserData(){
		$name			= $this->name;
		$phone_number	= $this->phone_number;
		$dob			= $this->dob;
		$profile_pic	= $this->profile_pic;
		$image_approval	= $this->image_approval;
		$description	= $this->description;
		$occupation_id	= $this->occupation_id;
		
		$u_data					 = new UserData();
		$u_data->user_id		= $this->id;
		$u_data->name			 = $name;
		$u_data->phone_number	 = $phone_number;
		$u_data->dob			 = $dob;
		$u_data->profile_pic	 = $profile_pic;
		$u_data->image_approval	 = $image_approval;
		$u_data->description	 = $description;
		$u_data->occupation_id	 = $occupation_id;
		if($u_data->save()){
//			$u_info = Apiuser::findOne($this->id);
//			$u_info->name				= null;
//			//$u_info->phone_number		= null;
//			$u_info->dob				= null;
//			$u_info->profile_pic		= null;
//			$u_info->image_approval		= 0;
//			$u_info->description		= null;
//			$u_info->occupation_id		= null;
//			$u_info->save();

		}
		
	}
    public function getUpdateSuppData(){
        $name			= $this->name;
        $phone_number	= $this->phone_number;
        $dob			= $this->dob;
        $profile_pic	= $this->profile_pic;
        $image_approval	= $this->image_approval;
        $description	= $this->description;
        $occupation_id	= $this->occupation_id;

        $u_data					 = new UserData();
        $u_data->user_id		= $this->id;
        $u_data->name			 = $name;
        $u_data->phone_number	 = $phone_number;
        $u_data->dob			 = $dob;
        $u_data->profile_pic	 = $profile_pic;
        $u_data->image_approval	 = $image_approval;
        $u_data->description	 = $description;
        $u_data->occupation_id	 = $occupation_id;
        $u_data->save();

    }
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    public function copyTestItems($user_id){

    }
}
