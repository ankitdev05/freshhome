<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%cancel_reasons}}".
 *
 * @property int $reason_id
 * @property string $name
 * @property string $type
 * @property int $created_at
 * @property int $updated_at
 */
class CancelReasons extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cancel_reasons}}';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type','status'], 'required'],
            [['name'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['type'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'reason_id' => Yii::t('app', 'Reason ID'),
            'name' => Yii::t('app', 'Reason Name'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
