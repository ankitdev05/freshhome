<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%menu_to_images}}".
 *
 * @property int $image_id
 * @property int $menu_id
 * @property string $image_name
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Menu $menu
 */
class MenuToImages extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_to_images}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id', 'image_name'], 'required'],
            [['menu_id', 'created_at', 'updated_at'], 'integer'],
            [['image_name'], 'string', 'max' => 255],
            [['file_info'], 'safe'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'image_id' => Yii::t('app', 'Image ID'),
            'menu_id' => Yii::t('app', 'Product ID'),
            'image_name' => Yii::t('app', 'Image Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }
}
