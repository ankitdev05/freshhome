<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Menu;

/**
 * MenuSearch represents the model behind the search form of `common\models\Menu`.
 */
class MenuSearch extends Menu
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'supplier_id', 'image_approval', 'city_id', 'dist_qty', 'dish_serve', 'created_at', 'updated_at','is_delete','product_type'], 'integer'],
            [['dish_name', 'dish_description', 'dish_image', 'dish_weight', 'dish_since', 'status'], 'safe'],
            [['dish_price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Menu::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'supplier_id' => $this->supplier_id,
            'image_approval' => $this->image_approval,
            'dish_price' => $this->dish_price,
            'city_id' => $this->city_id,
            'dist_qty' => $this->dist_qty,
            'dish_serve' => $this->dish_serve,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_delete' => $this->is_delete,
            'product_type' => $this->product_type,
        ]);

        $query->andFilterWhere(['like', 'dish_name', $this->dish_name])
            ->andFilterWhere(['like', 'dish_description', $this->dish_description])
            ->andFilterWhere(['like', 'dish_image', $this->dish_image])
            ->andFilterWhere(['like', 'dish_weight', $this->dish_weight])
            ->andFilterWhere(['like', 'dish_since', $this->dish_since])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
	
	public function imageSearch($params)
    {
        $query = Menu::find()->where(['and',['!=','dish_image',''],['image_approval'=>0]]);;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'supplier_id' => $this->supplier_id,
            'image_approval' => $this->image_approval,
            'dish_price' => $this->dish_price,
            'city_id' => $this->city_id,
            'dist_qty' => $this->dist_qty,
            'dish_serve' => $this->dish_serve,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'dish_name', $this->dish_name])
            ->andFilterWhere(['like', 'dish_description', $this->dish_description])
            ->andFilterWhere(['like', 'dish_image', $this->dish_image])
            ->andFilterWhere(['like', 'dish_weight', $this->dish_weight])
            ->andFilterWhere(['like', 'dish_since', $this->dish_since])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
