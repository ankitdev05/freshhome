<?php

namespace backend\models;
use Yii;


namespace common\models;
use himiklab\thumbnail\EasyThumbnailImage;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "brand".
 *
 * @property int $brand_id
 * @property string $name   
 * @property string $image
 */
class Brand extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

     public function behaviors() 
    { 
        return [
            TimestampBehavior::className(),
        ];
    }

     public $brand_names;
     public $maincategoryid;
     

    public static function tableName()
    {
        return '{{%brand}}'; 
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_names'], 'required'],
            // [['brand_id'], 'integer'],
            // [['name'], 'string', 'max' => 55],
            [['image'], 'string', 'max' => 250],
            // [['brand_id'], 'unique'],
            [['created_at', 'updated_at'], 'integer'],

        ]; 
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            // 'brand_id' => 'Brand ID',
            'name' => 'Name',
            'image' => 'Image',
            'brand_id' => 'Brand',
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getBrandDescription()
    {
        return $this->hasOne(BrandDescription::className(), ['brand_id' => 'brand_id']);
    }
    public function getBrandCategories()
    {
        return $this->hasMany(BrandCategory::className(), ['brand_id' => 'brand_id']);
    }

    public function getBrandImage($sizeX=100,$sizeY=100){
        if(empty($this->image))
            $this->image = 'no_image.png';
        $image          = '../../frontend/web/uploads/'.$this->image;
        $p_image    = EasyThumbnailImage::thumbnailFileUrl(
            $image,$sizeX,$sizeY,EasyThumbnailImage::THUMBNAIL_INSET
        );
        return $p_image;
    }
}
