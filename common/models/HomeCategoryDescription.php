<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%cuisine_description}}".
 *
 * @property int $id
 * @property int $cuisine_id
 * @property int $language_id
 * @property string $cuisine_name
 *
 * @property Cuisine $cuisine
 * @property Language $language 
 */
class HomeCategoryDescription extends \yii\db\ActiveRecord
{
 
   
 
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    { 
        return '{{%home_category_description}}';
    }
      public function behaviors()
    {
        return [ 
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['home_category_id', 'language_id'], 'required'],
            [['home_category_id', 'language_id'], 'integer'], 
            [['home_category_name'], 'string', 'max' => 255],
            [['home_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cuisine::className(), 'targetAttribute' => ['home_category_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'language_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'home_category_id' => Yii::t('app', 'home_category'),
            'language_id' => Yii::t('app', 'Language ID'),
            'home_category_name' => Yii::t('app', 'Home Category Came'), 
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand() 
    { 
        return $this->hasOne(brand::className(), ['id' => 'home_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['language_id' => 'language_id']);
    }
}
