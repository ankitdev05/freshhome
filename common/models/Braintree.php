<?php

namespace common\models;
use Yii;
use yii\base\Model;
use Braintree_Gateway;

class Braintree extends Model{
    public $gateway;
    /*
        Initialize braintree Token
    */
    public function init(){
        $this->gateway = new Braintree_Gateway([
            'environment' => Yii::$app->params['btree_environment'],
            'merchantId' => Yii::$app->params['btree_merchant_id'],
            'publicKey' => Yii::$app->params['btree_public_key'],
            'privateKey' => Yii::$app->params['btree_private_key']
        ]);
        parent::init();
    }
    /*
        Create user's new account
        @Required params $data{customer data email,first name,last name}
        @Response array
    */
    function createAccount($data){
        $user_detail= [];
        if($data->last_logged_in_as==1)
            $user_detail		= $data->userDetail;
        if($data->last_logged_in_as==2)
            $user_detail		= $data->userInfo;
        if($data->last_logged_in_as==5)
            $user_detail		= $data->salesDetail;

        $id = 'fresh_'.$data->id;
        $check_exist	= $this->checkExistingUser($id);
        if(!empty($check_exist))
            return $check_exist;
        $firstName 	= $user_detail['name'] ?? $data->name;
        $lastName 	= '';
        $company 	= '';
        $email 		= $user_detail['email'] ?? $data->email;
        $phone 		= $user_detail['phone_number'] ?? $data->phone_number;
        $this->gateway->customer()->create([
            'id' => $id,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'company' => $company,
            'email' => $email,
            'phone' => $phone,
        ]);
        return $this->checkExistingUser($id);
    }
    /*
        Check account if already created
        @Required params $id {account id}
        @Response array
    */
    protected function checkExistingUser($id){
        try{
            return $this->gateway->customer()->find($id);
        }
        catch(\Exception $e) {
            return;
        }
    }
    /*
        Add User's credit card
        @Required params $user_id,$cc_data {credit card details}
        @Response array
    */
    public function addCreditCard($user_id,$cc_data){
        $number 		= $cc_data['number'];
        $expirationDate = $cc_data['expirationDate'];
        $cvv 			= $cc_data['cvv'];
        $cardholderName	= $cc_data['cardholderName'];
        $result			= $this->gateway->creditCard()->create([
            'customerId' => $user_id,
            'cardholderName' => $cardholderName,
            'number' => $number,
            'expirationDate' => $expirationDate,
            'options'=>['verifyCard'=> 1,'failOnDuplicatePaymentMethod'=>0],
            'cvv' => $cvv
        ]);
        $response 	= json_decode(json_encode($result), true);

        if($result->success==1){
            return $response;
        }else{
            $errors		= $result->errors->deepAll();
            if(!empty($errors))
                return ['error'=>$errors[0]->message];
            elseif($result->message){
                return ['error'=>$result->message];
            }
        }
    }
    /*
        Update User's credit card
        @Required params $user_id,$cc_data {credit card details},$token
        @Response array
    */
    public function updateCreditCard($token,$cc_data){
        $number 		= $cc_data['number'];
        $expirationDate = $cc_data['expirationDate'];
        $cvv 			= $cc_data['cvv'];
        $cardholderName	= $cc_data['cardholderName'];

        $result			= $this->gateway->creditCard()->update($token,[
            'cardholderName' => $cardholderName,
            'number' => $number,
            'expirationDate' => $expirationDate,
            'options'=>['verifyCard'=> 1,'failOnDuplicatePaymentMethod'=>0],
            'cvv' => $cvv
        ]);
        $response 	= json_decode(json_encode($result), true);

        if($result->success==1){
            return $response;
        }else{
            $errors		= $result->errors->deepAll();
            if(!empty($errors))
                return ['error'=>$errors[0]->message];
            elseif($result->message){
                return ['error'=>$result->message];
            }
        }
    }

    /*
        Delete User's credit card
        @Required params $token
        @Response array
    */
    public function deleteCard($token){
        try{
            return $this->gateway->paymentMethod()->delete($token);
        }
        catch(\Exception $e) {
            return;
        }
    }

    public function submitForSettlement($amount,$token,$orderId){
        $json = [];
        $result = $this->gateway->transaction()->sale([
            'orderId' => $orderId,
            'amount' => $amount,
            'paymentMethodToken'  => $token,
            'options' => [
                'submitForSettlement' => True,
                //'holdInEscrow' => True,

            ],
        ]);

        if ($result->success) {
            $response = json_decode(json_encode($result->transaction),true);
            $json['response'] = $response;
            $json['error'] = false;
        }else{
            $json['message'] = $result->message;
            $json['error'] = true;
        }
        return $json;
    }

    public function authPayment($amount,$token,$orderId){
        $json = [];
        $result = $this->gateway->transaction()->sale([
            'orderId' => $orderId,
            'amount' => $amount,
            'paymentMethodToken'  => $token,
            'options' => [
                'submitForSettlement' => false,
                //'holdInEscrow' => True,

            ],
        ]);

        if ($result->success) {
            $response = json_decode(json_encode($result->transaction),true);
            $json['response'] = $response;
            $json['error'] = false;
        }else{
            $json['message'] = $result->message;
            $json['error'] = true;
        }
        return $json;
    }

    public function capturedAmount($id){
        $json = [];
        $result =  $this->gateway->transaction()->submitForSettlement($id);
        if ($result->success) {
            $response = json_decode(json_encode($result->transaction),true);
            $json['response'] = $response;
            $json['error'] = false;
        }else{
            $json['message'] = $result->message;
            $json['error'] = true;
        }
        return $json;
    }
    public function voided($id){
        $json = [];
        $result =  $this->gateway->transaction()->void($id);
        if ($result->success) {
            $response = json_decode(json_encode($result->transaction),true);
            $json['response'] = $response;
            $json['error'] = false;
        }else{
            $json['message'] = $result->message;
            $json['error'] = true;
        }
        return $json;
    }
    public function subscriptionCreate($token,$plan_id,$time_stamp,$user_id,$plan){
        $date = new \DateTime();
        $start_date = $date->setTimestamp($time_stamp);
        $json = [];
        $result = $this->gateway->subscription()->create([
            'paymentMethodToken' => $token,
            'planId' => $plan_id,
            'firstBillingDate' => $start_date
        ]);
        if ($result->success) {
            $response = json_decode(json_encode($result),true);
            $json['response'] = $response;
            $json['error'] = false;
            $user_model = User::findOne($user_id);
            if(!empty($user_model)){
                $user_model->subscription_status = $response['subscription']['status'];
                $user_model->subscription_id = $response['subscription']['id'];
                if($user_model->subscription_status!='Pending'){
                    $user_model->subscription_end  = strtotime($response['subscription']['billingPeriodEndDate']['date']);
                }

                if(empty($user_model->subscription_start)){
                    $user_model->subscription_start = $time_stamp;
                    $help_request = HelpRequests::find()->where(['and',['email' => $user_model->email],['request_status' => 5],['!=','accepted_by','']])->one();
                    if(!empty($help_request)){
                        $sales_person = User::findOne($help_request->accepted_by);
                        if(!empty($sales_person)){
                            $percentage = 10;
                            $total_amount = ($percentage/100)*$response['subscription']['transactions']['amount'];

                            $wallet_model = new Wallet();
                            $wallet_model->user_id = $sales_person->id;
                            $wallet_model->currency = $response['subscription']['transactions']['currencyIsoCode'];
                            $wallet_model->amount = $total_amount;
                            $wallet_model->type = 'received';
                            $wallet_model->user_role = 5;
                            $wallet_model->message = 'Received from Suppler\'s Subscription Request ID#'.$help_request->request_id;
                            $wallet_model->ip_address = Yii::$app->getRequest()->getUserIP();
                            if($wallet_model->save()){
                                $sales_person->updateBalance($wallet_model);
                            }
                        }
                    }
                }



                $user_model->subscription_months = $plan->months;
                $user_model->plan_id = $plan->plan_id;
                $user_model->subscription_type = Yii::$app->params['btree_environment'];
                $user_model->save();
            }

        }else{
            $json['message'] = $result->message;
            $json['error'] = true;
        }
        return $json;
    }

    public function subscriptionCancel($subscription_id){
        $result = $this->gateway->subscription()->cancel($subscription_id);
    }

    public function webHookResponse($bt_signature,$bt_payload){
         $webhookNotification = $this->gateway->webhookNotification()->parse($bt_signature, $bt_payload);
        $response = json_decode(json_encode($webhookNotification),true);
        $json['response'] = $response;
        return $json;
    }

}