<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "{{%main_categories}}".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $status
 */
class HomeCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */


      public function behaviors() 
    { 
        return [
            TimestampBehavior::className(),
        ];
    }

     public $home_category_names;
 

    public static function tableName()
    {
        return '{{%home_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['home_category_names'], 'required'], 
            [['status'], 'integer'], 
            [['image','file_info'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'integer'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            // 'id' => Yii::t('app', 'ID'),
            'home_category_names' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),  
            'image' => Yii::t('app', 'image'), 
        ];
    } 

    /**
     * {@inheritdoc}
     */
    public function getHomeCategory()
    { 
        return $this->hasOne(HomeCategoryDescription::className(), ['id' => 'home_category_id']);
    }

    public function getHomeCategoryDescription(){
        return $this->hasOne(HomeCategoryDescription::className(), ['home_category_id' => 'home_category_id']);
    }
}
