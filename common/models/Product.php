<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use creocoder\nestedsets\NestedSetsBehavior;

class Product extends \kartik\tree\models\Tree
{
    public static $treeQueryClass = 'kartik\tree\models\TreeQuery';
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'root',
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'lvl',
            ],
        ];
    }
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['cat_id', 'safe'];
        return $rules;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

}