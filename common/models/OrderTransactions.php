<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%order_transactions}}".
 *
 * @property int $id
 * @property string $transaction_id
 * @property string $status
 * @property string $type
 * @property string $currency_iso_code
 * @property string $amount
 * @property string $merchantAccountId
 * @property string $order_id
 * @property string $full_response
 * @property string $payment_type
 * @property string $payment_status
 * @property string $captured_amount
 * @property int $created_at
 * @property int $updated_at
 */
class OrderTransactions extends \yii\db\ActiveRecord
{
	public function behaviors()
    {
        return [

            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_transactions}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transaction_id', 'status', 'type', 'currency_iso_code', 'amount', 'order_id', 'full_response', 'payment_type', 'payment_status'], 'required'],
            [['payment_method','transaction_id', 'amount', 'full_response', 'payment_type', 'payment_status','captured_response'], 'string'],
            [['captured_amount'], 'number'],
            [['payment_method'], 'safe'],
            [['created_at', 'updated_at'], 'integer'],
            [['status', 'type', 'currency_iso_code', 'merchantAccountId'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'status' => Yii::t('app', 'Status'),
            'type' => Yii::t('app', 'Type'),
            'currency_iso_code' => Yii::t('app', 'Currency Iso Code'),
            'amount' => Yii::t('app', 'Amount'),
            'merchantAccountId' => Yii::t('app', 'Merchant Account ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'full_response' => Yii::t('app', 'Full Response'),
            'payment_type' => Yii::t('app', 'Payment Type'),
            'payment_status' => Yii::t('app', 'Payment Status'),
            'captured_amount' => Yii::t('app', 'Captured Amount'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
	public function beforeAction($action){
	  $this->payment_method = Yii::$app->params['default_gateway'];
	  return parent::beforeAction($action);
	}
}
