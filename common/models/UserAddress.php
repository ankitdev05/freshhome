<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%user_address}}".
 *
 * @property int $address_id
 * @property int $user_id
 * @property string $title
 * @property string $location
 * @property string $city
 * @property string $building_name
 * @property string $flat_no
 * @property string $floor_no
 * @property string $landmark
 * @property string $latitude
 * @property string $longitude
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class UserAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_address}}';
    }
	
	/**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'location', 'city', 'building_name', 'flat_no', 'floor_no', 'landmark', 'latitude', 'longitude'], 'required'],
			//['is_default', 'default', 'value' => 'no'],
            [['user_id', 'created_at', 'updated_at','city','is_delete'], 'integer'],
            [['landmark','is_default'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['title', 'location', 'city', 'building_name'], 'string', 'max' => 255],
            [['flat_no', 'floor_no'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'address_id' => Yii::t('app', 'Address ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'title' => Yii::t('app', 'Title'),
            'location' => Yii::t('app', 'Location'),
            'city' => Yii::t('app', 'City'),
            'building_name' => Yii::t('app', 'Building Name'),
            'flat_no' => Yii::t('app', 'Flat No.'),
            'floor_no' => Yii::t('app', 'Floor No.'),
            'landmark' => Yii::t('app', 'Landmark'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getCityInfo()
    {
        return $this->hasOne(City::className(), ['id' => 'city']);
    }
	public function getAddressData(){
		$json['address_id']			= (int)$this->address_id;
		$json['title']				= $this->title;
		$json['location']			= $this->location;
		$json['city']				= $this->city;
		$json['building_name']		= $this->building_name;
		$json['flat_no']			= $this->flat_no;
		$json['floor_no']			= $this->floor_no;
		$json['landmark']			= $this->landmark;
		$json['latitude']			= $this->latitude;
		$json['longitude']			= $this->longitude;
		$json['is_default']			= $this->is_default;
		
		return $json;
	}
}
