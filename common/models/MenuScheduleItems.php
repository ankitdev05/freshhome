<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%menu_schedule_items}}".
 *
 * @property int $id
 * @property int $menuschedule_id
 * @property int $menu_id
 * @property string $choices
 * @property int $created_at
 * @property int $updated_at
 *
 * @property MenuSchedules $menuschedule
 * @property Menu $menu
 */
class MenuScheduleItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_schedule_items}}';
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menuschedule_id', 'menu_id', 'choices'], 'required'],
            [['menuschedule_id', 'menu_id', 'created_at', 'updated_at','quantity'], 'integer'],
            [['choices'], 'string'],
            [['menuschedule_id'], 'exist', 'skipOnError' => true, 'targetClass' => MenuSchedules::className(), 'targetAttribute' => ['menuschedule_id' => 'id']],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'menuschedule_id' => Yii::t('app', 'Menuschedule ID'),
            'menu_id' => Yii::t('app', 'Menu ID'),
            'choices' => Yii::t('app', 'Choices'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuschedule()
    {
        return $this->hasOne(MenuSchedules::className(), ['id' => 'menuschedule_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }
}
