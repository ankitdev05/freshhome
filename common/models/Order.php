<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use api\models\Cart;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property int $order_id
 * @property int $user_id
 * @property int $supplier_id
 * @property int $address_id
 * @property int $currency_id
 * @property string $currency_code
 * @property string $ip
 * @property string $total
 * @property int $order_status_id
 * @property string $payment_code
 * @property string $payment_method
 * @property int $created_at
 * @property int $updated_at
 *
 * @property UserAddress $address
 * @property Currency $currency
 * @property OrderStatus $orderStatus
 * @property User $supplier
 * @property OrderHistory[] $orderHistories
 * @property OrderProducts[] $orderProducts
 * @property OrderTotal[] $orderTotals
 */
class Order extends \yii\db\ActiveRecord
{

    public $payment_options_values = ['cod' => 'Cash on delivery','card' => 'Pay via card','wallet' =>'Wallet'];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order}}';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_image'],'required','on' => 'order_complete'],
            [['payment_code'],'required','on' => 'card_add'],
            [['payment_code','card_id'],'required','on' => 'card_add1'],
            [['payment_code','card_id','date','time'],'required','on' => 'pre_order_with_card'],
            [['date','time'],'required','on' => 'pre_order'],
            ['payment_code', 'in', 'range' => ['cod','card','wallet']],
            [['reject_type','reject_message','order_id'],'required','on' => 'reject_order'],
            [['user_id', 'supplier_id', 'address_id', 'currency_id', 'ip', 'total', 'order_status_id', 'payment_code', 'payment_method'], 'required'],
            [['dish_prep_time','user_id', 'supplier_id', 'address_id', 'currency_id', 'order_status_id', 'created_at', 'updated_at','estimate_deliver_time','card_id'], 'integer'],
            [['total','order_accepted'], 'number'],
            ['address_id', 'checkAddress'],
            ['card_id', 'checkCard'],
            [['currency_code'], 'string', 'max' => 3],
            [['notes','phone_number','date','time','reject_type','reject_message','order_type','parcel_image'], 'string'],
            [['ip'], 'string', 'max' => 40],
            [['payment_code'], 'string', 'max' => 10],
            [['payment_method','order_image'], 'string', 'max' => 255],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserAddress::className(), 'targetAttribute' => ['address_id' => 'address_id']],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
            [['order_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['order_status_id' => 'order_status_id']],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['supplier_id' => 'id']],
        ];
    }
    public function checkAddress($attribute, $params){
        if (!Yii::$app->user->isGuest) {
            $user_id	=  Yii::$app->user->identity->id;
            if($user_id==$this->user_id){
                $checkAddress = UserAddress::find()->where(['address_id'=>$this->address_id,'user_id'=>$this->user_id])->count();
                if($checkAddress==0)
                    $this->addError($attribute, Yii::t('app', 'You select an invalid address.'));
            }

        }else
            $this->addError($attribute, Yii::t('app', 'You select an invalid address.'));
    }
    public function checkCard($attribute, $params){
        if (!Yii::$app->user->isGuest) {
            $user_id	=  Yii::$app->user->identity->id;
            $checkCard = CreditCards::find()->where(['id'=>$this->card_id,'user_id'=>$this->user_id])->count();
            if($checkCard==0)
                $this->addError($attribute, Yii::t('app', 'You select an invalid card.'));
        }else
            $this->addError($attribute, Yii::t('app', 'You select an invalid card.'));
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('app', 'Order ID'),
            'user_id' => Yii::t('app', 'User Name'),
            'supplier_id' => Yii::t('app', 'Supplier name'),
            'address_id' => Yii::t('app', 'Address'),
            'currency_id' => Yii::t('app', 'Currency'),
            'currency_code' => Yii::t('app', 'Currency Code'),
            'ip' => Yii::t('app', 'Ip'),
            'total' => Yii::t('app', 'Total'),
            'order_status_id' => Yii::t('app', 'Order Status'),
            'payment_code' => Yii::t('app', 'Payment Method'),
            'payment_method' => Yii::t('app', 'Payment Method'),
            'created_at' => Yii::t('app', 'Date Added'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'card_id' => Yii::t('app', 'Card'),
            'date' => Yii::t('app', 'Delivery date'),
            'time' => Yii::t('app', 'Delivery time'),
            'order_image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(UserAddress::className(), ['address_id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['order_status_id' => 'order_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(User::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories()
    {
        return $this->hasMany(OrderHistory::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderRatings()
    {
        return $this->hasOne(OrderRatings::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProducts::className(), ['order_id' => 'order_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountOrderProducts()
    {
        $items = 0;
        $products =  $this->hasMany(OrderProducts::className(), ['order_id' => 'order_id'])->all();
        if(!empty($products)){
            foreach ($products as $product) {
                $items +=$product->quantity;
            }
        }
        return $items;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderTotals()
    {
        return $this->hasMany(OrderTotal::className(), ['order_id' => 'order_id']);
    }
    public function getSupplierOrderTotals()
    {
        return $this->hasMany(OrderTotalSupplier::className(), ['order_id' => 'order_id']);
    }

    public function getSendEmail(){
        $suupplier_info = User::findOne($this->supplier_id);
        $user_info 		= User::findOne($this->user_id);
        $subject		= 'Order #'.$this->order_id.' -  Your '.config_name.' order from '.$suupplier_info->name;
        $message		= '<p>Dear <b>'.$user_info->name.'</b>,</p>';
        $message		.= '<p>Thanks for ordering online on '.config_name.'</p>';
        $message		.= '<p>We hope you enjoyed your meal from <b>'.$suupplier_info->name.'</b></p>';
        $message		.= '<p>If you have any feedback for '.config_name.' or about your ordering experience, we\'d love to hear from you - simply reply to this email and we\'ll be in touch.</p>';
        $message		.= '<h3>Your payment history</h3>';
        $message		.= '<p>Order ID: <b>'.$this->order_id.'</b></p>';
        $message		.= '<p>Date: <b>'.date('d M,Y',$this->created_at).'</b></p>';
        foreach($this->orderTotals as $order_total){
            $message  .= '<p>'.$order_total->title.': <b>'.$this->currency_code.' '.Yii::$app->formatter->format($order_total->value,['decimal',2]).'</b></p>';
        }

        $message	.= '<p>Thanks,<br/>Team '.Yii::$app->name.'</p>';
        Yii::$app->mailer->compose()
            ->setHtmlBody($message)
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($user_info->email)
            ->setSubject($subject)
            ->send();
    }
    public function getDriver(){
        return;
        $order_info = $this->DriverorderInfo();
        /* $model = new DriverOrderStatus();
        $result = $model->find()->where(['order_id'=>$this->order_id])->orderBy("id desc")->one();
        if(!empty($result)){
            $order_info['order_status'] = $result->order_status;
        }
        return $order_info; */
    }
    public function getOrderInfo(){
        $address_info		= $this->address;
        $user_info			= $this->user->userDetail;
        $supplier_info      = $this->supplier;
        if(!empty($supplier_info) && $supplier_info->supplier_type ==1 && empty($this->date)){
            $estimateOrderTime = $this->estimateOrderTime;
            $this->date = $estimateOrderTime['date'];
            $this->time = $estimateOrderTime['time'];
        }
        if(!empty($this->time)){
            $explode = explode(' ',$this->time);
            if(isset($explode[1]))
                $this->time = $explode[0].':00 '.$explode[1];
        }
        $order_products		= $this->orderProducts;
        if(Yii::$app->user->id==$this->supplier_id)
            $order_totals		= $this->supplierOrderTotals;
        else
            $order_totals		= $this->orderTotals;
        $order_items		= [];
        $order_total		= [];
        foreach($order_products as $order_product){
            $menuImage 		= Menu::findOne($order_product->menu_id)->productImage;
            $order_items[] = [
                'name'=>$order_product->name,
                'quantity'=>$order_product->quantity,
                'dish_image'=>$menuImage,
                'dish_id'=>$order_product->menu_id,
                'price'=>Yii::$app->formatter->format($order_product->price,['decimal',2]),
                'total'=>Yii::$app->formatter->format($order_product->total,['decimal',2]),
                'dish_reviews'=>$this->getOrderMenuRating($this->order_id,$order_product->menu_id)
            ];
        }
        foreach($order_totals as $order_total_){
            if (!Yii::$app->user->isGuest) {
                $order_total[]	= [
                    'code'=>$order_total_->code,
                    'title'=>$order_total_->title,
                    'value'=>Yii::$app->formatter->format($order_total_->value,['decimal',2])
                ];
            }

        }
        $order_info	= [
            'order_id'=>$this->order_id,
            'order_type'=>$this->order_category,
            'order_status'=>$this->orderStatus->name,
            'currency_code'=>$this->currency_code,
            'total'=>Yii::$app->formatter->format($this->total,['decimal',2]),
            'order_date'=>date("d M,Y",$this->created_at),
            'payment_method'=>$this->payment_method,
            'total_items'=>count($order_products),
            'estimate_deliver_time'=>round($this->estimate_deliver_time/60),
            'date'=>$this->date,
            'time'=>$this->time,
            'additional_phone_number'=>$this->phone_number,
            'additional_notes'=>$this->notes,
            'invoice_url'=>$this->invoicePdf,
            'driver_earning'=>$this->driverEarnings,
            'parcel_image'=>$this->parcelImage,
            'user_info'=>[
                'email'=>$user_info['email'],
                'phone_number'=>$user_info['phone_number'],
                'name'=>$user_info['name'],
                'profile_pic'=>$user_info['profile_pic'],
                'delivery_address'=>[
                    'title'=>$address_info->title,
                    'location'=>$address_info->location,
                    'city'=>$address_info->city,
                    'building_name'=>$address_info->building_name,
                    'building_name'=>$address_info->building_name,
                    'flat_no'=>$address_info->flat_no,
                    'floor_no'=>$address_info->floor_no,
                    'landmark'=>$address_info->landmark,
                ],
            ],
            'order_items'=>$order_items,
            'order_total'=>$order_total,
            'refund_text' => 'User will be notify soon',
            'allow_cancel' => $this->allowCancel,
            'order_ratings'=>$this->orderRating,
            'driver_info'=>$this->driverInfo
        ];

        return $order_info;
    }
    public function getDriverOrderInfo(){
        $address_info		= $this->address;
        $city_info          = CityDescription::find()->where(['city_id' => $address_info->city])->one();
        $user_info			= UserData::find()->where(['user_id' => $this->user_id])->one();
        $order_products		= $this->orderProducts;
        $order_info	= [
            'order_id'=>$this->order_id,
            'order_type'=>$this->order_category,
            'order_status'=>$this->orderStatus->name,
            'currency_code'=>$this->currency_code,
            'total'=>Yii::$app->formatter->format($this->total,['decimal',2]),
            'order_date'=>date("d M,Y",$this->created_at),
            'payment_method'=>$this->payment_method,
            'total_items'=>count($order_products),
            'estimate_deliver_time'=>round($this->estimate_deliver_time/60),
            'date'=>$this->date,
            'time'=>$this->time,
            'additional_phone_number'=>$this->phone_number,
            'additional_notes'=>$this->notes,
            'kitchen_location'=>$this->kitchenAddress,
            'driver_earning'=>$this->driverEarnings,
            'parcel_image'=>$this->parcelImage,
            'user_info'=>[
                'phone_number'=>$user_info['phone_number'],
                'name'=>$user_info['name'],
                'profile_pic'=>$user_info['profile_pic'],
                'delivery_address'=>[
                    'title'=>$address_info->title,
                    'location'=>$address_info->location,
                    'city'=>$address_info->city,
                    'city_name'=> $city_info['city_name'],
                    'building_name'=>$address_info->building_name,
                    'flat_no'=>$address_info->flat_no,
                    'floor_no'=>$address_info->floor_no,
                    'landmark'=>$address_info->landmark,
                    'latitude'=>$address_info->latitude,
                    'longitude'=>$address_info->longitude,
                ],
            ],
        ];

        return $order_info;
    }
    public function getOrderRating(){
        $user_info			= $this->user;
        $order_rating =  OrderRatings::find()->where(['order_id'=>$this->order_id,'user_id'=>$user_info->id])->asArray()->one();
        if(!empty($order_rating)){
            $output['taste_rating'] = $order_rating['taste_rating'];
            $output['presentation_rating'] = $order_rating['presentation_rating'];
            $output['packing_rating'] = $order_rating['packing_rating'];
            $output['overall_rating'] = $order_rating['overall_rating'];
            $output['review'] = $order_rating['review'];
        }else{
            $output['taste_rating'] = null;
            $output['presentation_rating'] = null;
            $output['packing_rating'] =  null;
            $output['overall_rating'] =  null;
            $output['review'] =  null;
        }

        return $output;
    }
    public function getOrderMenuRating($order_id,$menu_id){
        $menu_rating = MenuRating::find()->where(['order_id'=>$order_id,'menu_id'=>$menu_id])->one();
        $rating = null;
        $review = null;
        if(!empty($menu_rating)){
            $rating = $menu_rating->rating;
            $review = $menu_rating->reviews;
        }
        $output['rating'] = $rating;
        $output['review'] = $review;
        return $output;
    }
    public function getTotalOrders(){
        $model = new Sale();
        return $model->TotalOrders($this->user_id);
    }
    public function getTotalOrderProducts(){
        $model = new Sale();
        return $model->TotalOrderProducts($this->user_id);
    }
    public function getTotalPrice(){
        $model = new Sale();
        return $model->TotalPrice($this->user_id);
    }
    public function getKitchenAddress(){
        $data = [];
        $supplier_info = User::findOne($this->supplier_id);
        if(!empty($supplier_info)){
            $city_info			= $supplier_info->cityInfo;
            $data['username'] = $supplier_info->name;
            $data['phone_number'] = $supplier_info->phone_number;
            $data['building_no'] = $supplier_info->building_no;
            $data['flat_no'] = $supplier_info->flat_no;
            $data['floor_no'] = $supplier_info->floor_no;
            $data['landmark'] = $supplier_info->landmark;
            $data['location'] = $supplier_info->location;
            $data['latitude'] = $supplier_info->latitude;
            $data['longitude'] = $supplier_info->longitude;
            $data['city'] =!empty($city_info) ? $city_info->city_name : '';
        }
        return $data;
    }

    public function DriverorderInfo(){
        $supplierinfo = User::findOne($this->supplier_id);

        $long = $supplierinfo->longitude;
        $lat = $supplierinfo->latitude;


        $rejected_drivers = DriverOrderStatus::find()->select("driver_id")->where(['order_id'=>$this->order_id])->andWhere(['or',['order_status'=>'Rejected'],['order_status'=>'Cancelled']])->asArray()->all();

        $query      = User::find()->select(new Expression("*,(6353  * 2 * ASIN(SQRT( POWER(SIN(( $lat - latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(latitude * pi()/180) * POWER(SIN(( $long - longitude) * pi()/180 / 2), 2) ))) as distance  "))->where(['and',['role'=>4],['status'=>10],['!=','id',$this->supplier_id],['!=','longitude',''],['!=','latitude','']]);
        if(!empty($rejected_drivers))
            $query->andWhere(['!=','id',$rejected_drivers]);
        $query->having("distance <= 10")->orderBy("distance");

        $result = $query->one();

        if(!empty($result)){
            $this->driver_id = $result->id;
            if($this->save()){

                $library = new Library();
                $order_info['user_id']       = $this->driver_id;
                $order_info['role']          = 4;
                $order_info['from_user']     = $this->supplier_id;
                $order_info['type']          = 'order';
                $order_info['config_name']    = 'You\'ve received a new order from '.$supplierinfo->name;
                $order_info['config_value']['order_id']= $this->order_id;
                $order_info['config_value']['message']= 'You\'ve received a new order from '.$supplierinfo->name;
                $library->saveNotification($order_info);

                $data['driver_id'] = $this->driver_id;
                $data['order_id']   = $this->order_id;
                $data['order_status'] = 'Pending';
                $library->updateDriverHistory($data);
                return true;
            }
        }
        return;
    }

    public function addToCart($user_info,$post_data=[],$cart_type='all'){
        $session = Yii::$app->session;

        $user_detail = $user_info->userDetail;
        $json = [];
        $library = new Library();
        $cart_data1 = $library->json_decodeArray($user_info->cart);

        if(!empty($cart_data1)){
            $cart_model	= new Cart();
            $output = $cart_model->cartData($user_info->id,$cart_type);

            if($output!=1){
                $cart_data = $output['cart']['supplier_info']['cart_data'];
                if(isset($cart_data) && !empty($cart_data)){
                    foreach($cart_data as $cart_){
                        $post_data1['Cart']['quantity'] = $cart_['quantity'];
                        $options = $cart_['options'];
                        if(!empty($options)){
                            foreach($options as $option){
                                if($option['type']=='checkbox')
                                    $post_data1['Cart']['product_attributes'][$option['option_id']][] = $option['value'];
                                else
                                    $post_data1['Cart']['product_attributes'][$option['option_id']] = $option['value'];
                            }
                            $result = Menu::findOne($cart_['menu_id']);
                            $validate_cart = $result->validateCart($post_data1['Cart']);
                            if(!empty($validate_cart)){
                                if(isset($validate_cart['error'])){
                                    $json['error']['msg'] = $validate_cart['error'];
                                    return $json;
                                }
                            }
                        }

                    }
                }
            }

            $order_type     = 'online';
            $date           = null;
            $time           = null;

            if(isset($post_data['Order']['order_type']))
                $order_type = $post_data['Order']['order_type'];

            if(isset($post_data['Order']['collection_time']))
                $time = $post_data['Order']['collection_time'];

            if(isset($post_data['Order']['collection_date']))
                $date = $post_data['Order']['collection_date'];
            if($order_type=='online'){
                $alow_order = true;
                $menu_items = [];
                foreach($output['cart']['supplier_info']['cart_data'] as $menu){
                    $dish_id = $menu['menu_id'];
                    $dish_info = Menu::findOne($dish_id);
                    if(!empty($dish_info) && $dish_info->status=='inactive' && $dish_info->product_type!=1){
                        $alow_order = false;
                        $menu_items[] = $dish_info->dish_name;
                    }
                }
                if($alow_order==false){
                    $json['error']['msg']	='Your cart contains some in active items. Please remove '.implode(',',$menu_items).' and try again.';
                    return $json;
                }
            }
            $currency_info	= Currency::findOne(['code'=>config_default_currency]);
            $total_price 	= end($output['total_amounts']);
            $final_amount = $output['final_amount'];

            $this->order_type		= $order_type;
            $this->date		    = $date;
            $this->time		    = $time;
            $this->user_id			= $user_info->id;
            $this->supplier_id		= $output['cart']['supplier_info']['supplier_id'];
            $this->currency_id		= $currency_info->id;
            $this->currency_code	= config_default_currency;
            $this->ip				= Yii::$app->getRequest()->getUserIP();
            $this->total			= $total_price['total_value'];
            $this->order_status_id	= 1;
            $this->estimate_deliver_time	= 3600;
            if(empty($this->date))
                $this->order_status_id	= 11;
            if(isset($post_data['Order']['payment_code'])){
                $this->payment_code	= $post_data['Order']['payment_code'];
                $this->payment_method	= $this->payment_options_values[$this->payment_code];
            }else{
                $this->payment_code	= '';
                $this->payment_method	= '';
            }
            if($this->payment_code=='card')
                $this->scenario = 'card_add1';
            else
                $this->scenario = 'card_add';

            if($this->load($post_data) && $this->validate()) {

                if($this->save()){
                    if($this->payment_code=='card'){
                        $braintree_model = new Braintree();
                        $token = '';
                        $creditcard = CreditCards::find()->where(['id' => $this->card_id,'user_id' => $this->user_id ])->one();
                        if(!empty($creditcard))
                            $token = $creditcard->token;
                        $this->total = number_format($this->total, 2, '.', '');
                        $response = $braintree_model->submitForSettlement($this->total,$token,$this->order_id);
                        if($response['error']==1){
                            //$json['error']['msg'] = $response['message'];
                            $json['error']['msg'] = 'There is some problem for placing order, please try again later.';
                            Order::findOne($this->order_id)->delete();
                            return $json;
                        }else{
                            $this->orderTransaction($response['response']);
                        }

                    }
                    if(empty($this->date))
                        $datetime = date("Y-m-d G:i:s",$this->created_at);
                    else{
                        $strtotime = ($this->date.' '.date("G:i",strtotime($this->time)));
                        $date_time = explode(' ',$strtotime);
                        $date = $date_time[0];
                        $time = $date_time[1];

                        $date_exp = explode('/',$date);
                        $time_exp = explode(':',$time);

                        $time_stamp = mktime($time_exp[0],$time_exp[1],0,$date_exp[1],$date_exp[0],$date_exp[2]);
                        $datetime = date("Y-m-d G:i:s",$time_stamp);

                    }
                    $this->date_added = $datetime;
                    $this->total = $final_amount;
                    $this->save();

                    $order_history	= new OrderHistory();
                    $order_history->order_id			= $this->order_id;
                    $order_history->order_status_id		= $this->order_status_id;
                    $order_history->notify				= 1;
                    $order_history->comment				= 'Your order is under processing.';
                    $order_history->save();
                    $product_ids = [];
                    foreach($output['cart']['supplier_info']['cart_data'] as $m=>$menu){
                        if(isset($menu['options']) && !empty($menu['options'])){
                            foreach($menu['options'] as $optns){
                                $model_order_options = new OrderOptions();
                                $model_order_options->order_id = $this->order_id;
                                $model_order_options->order_product_id = $menu['menu_id'];
                                $model_order_options->name = $optns['name'];
                                $model_order_options->type = $optns['type'];
                                $model_order_options->value = $optns['value'];
                                $model_order_options->price = $optns['price'];
                                $model_order_options->subtract = $optns['subtract'];
                                if($model_order_options->save()){
                                    $menu_attr_optns =  MenuToAttributeOptions::find()->where(['menu_id' => $menu['menu_id'],'menu_attr' => $optns['menu_attr'],'name' => $optns['value']])->one();
                                    if(!empty($menu_attr_optns)){
                                        $qty = $menu_attr_optns->quantity;
                                        $quantity = $qty-$menu['quantity'];
                                        if($quantity<0)
                                            $quantity = 0;
                                        $menu_attr_optns->quantity = $quantity;
                                        $menu_attr_optns->save();
                                    }

                                }
                            }
                        }
                        //if($order_type =="online" && $menu['status']=="active"){

                        if($order_type =="online" ){

                            $order_products			= new OrderProducts();
                            $order_products->order_id	= $this->order_id;
                            $order_products->product_id	= $menu['menu_id'];
                            $order_products->menu_id	= $menu['menu_id'];
                            $order_products->name		= $menu['dish_name'];
                            $order_products->quantity	= $menu['quantity'];
                            $order_products->price		= $menu['dish_price'];
                            $order_products->total		= $menu['total_value'];
                            $order_products->save();
                            $product_ids[] = $menu['menu_id'];
                            unset($cart_data1[$this->supplier_id][$menu['menu_id']]);
                            $cart_val = $library->json_encodeArray($cart_data1);
                            $user_info->cart = $cart_val;
                            $user_info->save();
                            $menu1 = Menu::findOne($menu['menu_id']);
                            if(!empty($menu1)){
                                $menu1->dist_qty = $menu1->dist_qty - $menu['quantity'];
                                $menu1->save();
                            }
                        }
                        if($order_type =="pre-order" && $menu['status']=="inactive"){
                            $order_products			= new OrderProducts();
                            $order_products->order_id	= $this->order_id;
                            $order_products->product_id	= $menu['menu_id'];
                            $order_products->menu_id	= $menu['menu_id'];
                            $order_products->name		= $menu['dish_name'];
                            $order_products->quantity	= $menu['quantity'];
                            $order_products->price		= $menu['dish_price'];
                            $order_products->total		= $menu['total_value'];
                            $order_products->save();
                            $product_ids[] = $menu['menu_id'];
                            unset($cart_data1[$this->supplier_id][$menu['menu_id']]);
                            $cart_val = $library->json_encodeArray($cart_data1);
                            $user_info->cart = $cart_val;
                            $user_info->save();

                            $menu1 = Menu::findOne($menu['menu_id']);
                            $menu1->dist_qty = $menu1->dist_qty - $menu['quantity'];
                            $menu1->save();
                        }
                        if(!empty($product_ids)){
                            $dish_prep_time = Menu::find()->where(['in','id',$product_ids])->max('dish_prep_time');
                            $this->dish_prep_time = $dish_prep_time;
                            $this->save();
                        }



                    }
                    foreach($output['total_amounts'] as $k=>$total_amount){
                        $order_total = new OrderTotal();
                        $order_total->order_id		= $this->order_id;
                        $order_total->code			= $total_amount['code'];
                        $order_total->title			= $total_amount['label'];
                        $order_total->value			= $total_amount['total_value'];
                        $order_total->sort_order	= ($k+1);
                        $order_total->save();
                        if($total_amount['code']=='wallet'){
                            $model = new Wallet();
                            $model->user_id = $user_info->id;
                            $model->currency = $total_amount['currency'];
                            $model->amount = $total_amount['total_value'];
                            $model->type = 'paid';
                            $model->user_role = 4;
                            $model->message = 'Paid for order #'.$this->order_id;
                            $model->ip_address = Yii::$app->getRequest()->getUserIP();
                            if($model->save()){
                                $user_info->updateBalance($model);
                            }
                        }
                    }
                    foreach($output['supplier_total_amounts'] as $k=>$total_amount){
                        $order_total = new OrderTotalSupplier();
                        $order_total->order_id		= $this->order_id;
                        $order_total->code			= $total_amount['code'];
                        $order_total->title			= $total_amount['label'];
                        $order_total->value			= $total_amount['total_value'];
                        $order_total->sort_order	= ($k+1);
                        $order_total->save();
                    }
                    $this->sendEmail;
                    $session->set('phone_number',null);
                    $session->set('notes',null);
                   // $user_info->cart = null;
                   // $user_info->save();

                    $library = new Library();
                    $order_info['user_id']        = $this->supplier_id;
                    $order_info['role']            = 2;
                    $order_info['from_user']        = $this->user_id;
                    $order_info['type']           = 'order';
                    $order_info['config_name']    = 'You\'ve received a new order from '.$user_detail['name'];
                    $order_info['config_value']['order_id']= $this->order_id;
                    $order_info['config_value']['message']= 'You\'ve received a new order from '.$user_detail['name'];
                    $library->saveNotification($order_info);

                    $json['success']['msg']		= 'Congratulations!! Your order has been placed successfully.';
                    if($this->dish_prep_time<=1200)
                        $this->driver;
                }
            }
            else{
                $all_errors				= [];
                $errors					= $this->getErrors();
                foreach($errors as $key=>$error){
                    $all_errors[]	= $error[0];
                }
                $json['error']['msg']	= !empty($all_errors) ? $all_errors[0] : '' ;
            }
        }
        return $json;
    }

    public function getOrderOptions(){
        return $this->hasMany(OrderOptions::className(), ['order_id' => 'order_id']);
    }

    function getDrivingDistance()
    {
        $address= $this->address;
        if(!empty($address)){
            $user = $this->supplier;
            $lat1 = $address->latitude;
            $long1 = $address->longitude;

            $lat2 = $user->latitude;
            $long2 = $user->longitude;


            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&key=".Yii::$app->params['google_api_key'];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response_a = json_decode($response, true);
            $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

            return array('distance' => $dist, 'time' => $time);
        }
        return [];
    }

    public function getOrderTime(){
        $time_stamp = 0;

        $supplier_info      = $this->supplier;
        if($supplier_info->supplier_type ==1 && empty($this->date)){
            $estimateOrderTime = $this->estimateOrderTime;
            $this->date = $estimateOrderTime['date'];
            $this->time = $estimateOrderTime['time'];
        }

        if(!empty($this->date)){
            $explode = explode('/',$this->date);
         $this->date = str_pad($explode[0], 2,0,STR_PAD_LEFT).'-'.str_pad($explode[1], 2,0,STR_PAD_LEFT).'-'.$explode[2];

         $this->date.' '.$this->time;
         return $time_stamp = strtotime($this->date.' '.$this->time);
        }
        return $time_stamp;
    }

    public function getEstimateOrderTime(){
        $time_stamp = $this->created_at+$this->dish_prep_time;
        $delivery_time = 10*60; //20 minutes for delivery time
        $driver_time = 15*60;
        $total_time = $time_stamp+$delivery_time+$driver_time;

        return ['date'=>date('d/m/Y',$total_time),'time' => date('h:i A',$total_time)];

    }

    public function getInvoicePdf(){
        return Yii::$app->params['api_path'].'/user/invoice/'.$this->order_id;
    }

    public function orderTransaction($response){
        $model = new OrderTransactions();
        $model->transaction_id = $response['id'];
        $model->status = $response['status'];
        $model->type = $response['type'];
        $model->currency_iso_code = $response['currencyIsoCode'];
        $model->amount = $response['amount'];
        $model->merchantAccountId = $response['merchantAccountId'];
        $model->order_id = $response['orderId'];
        $model->full_response = serialize($response);
        $model->payment_type = 'main';
        $model->payment_status = 'completed';
        $model->payment_method = 'braintree';
        $model->save();
    }

    public function getAllowCancel(){
        $supplier_info = $this->supplier;
        $allow = true;
        $status_array = [2,5,10];
        if($supplier_info->supplier_type==1){
            if($this->order_type=='pre-order'){
                if(in_array($this->order_status_id,$status_array))
                    $allow = false;
                else{
                    $order_time = $this->orderTime;
                    $remaining_time = $order_time-time();

                    if($remaining_time<10800)
                        $allow = false;
                }
            }
        }else{

            if(in_array($this->order_status_id,$status_array))
                $allow = false;
        }
        return $allow;
    }

    public function getDriverInfo(){
        $json = [];
        if(!empty($this->driver_id)){
            $user = User::findOne($this->driver_id);
            $driver_id          = $user->driver_id;
            $result             = DriverCompanyNumbers::findOne($driver_id);

            if(!empty($result)){
                $json = $result->profile;
            }else
                $json = $user->driverInfo;

        }
        return $json;
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $supplier_info = User::findOne($this->supplier_id);
            $this->order_category = $supplier_info->supplier_type;

        }
        return parent::beforeSave($insert);
    }

    protected function getDriverEarnings(){
        $json = [];

        $total = OrderTotal::find()->where(['order_id' => $this->order_id,'code' => 'delivery_charges'])->one();
        if(!empty($total)){
            $json['currency'] = $this->currency_code;
            $json['total'] = $total->value;
        }
        return $json;
    }

    public function getParcelImage(){
        if(!empty($this->parcel_image))
            return Yii::$app->params['website_path'].'/uploads/'.$this->parcel_image;
        return;
    }
}