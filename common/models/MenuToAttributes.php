<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%menu_to_attributes}}".
 *
 * @property int $menu_attr
 * @property int $menu_id
 * @property int $option_id
 * @property string $is_required
 *
 * @property MenuToAttributeOption[] $menuToAttributeOptions
 * @property Menu $menu
 * @property Option $option
 */
class MenuToAttributes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_to_attributes}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id', 'option_id', 'is_required'], 'required'],
            [['menu_id', 'option_id'], 'integer'],
            [['is_required'], 'string'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['option_id'], 'exist', 'skipOnError' => true, 'targetClass' => Option::className(), 'targetAttribute' => ['option_id' => 'option_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'menu_attr' => 'Menu Attr',
            'menu_id' => 'Menu ID',
            'option_id' => 'Option ID',
            'is_required' => 'Is Required',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuToAttributeOptions()
    {
        return $this->hasMany(MenuToAttributeOption::className(), ['menu_attr' => 'menu_attr']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(Option::className(), ['option_id' => 'option_id']);
    }
}
