<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%wallet}}".
 *
 * @property int $wallet_id
 * @property int $user_id
 * @property int $card_id
 * @property string $amount
 * @property string $currency
 * @property string $type
 * @property int $user_role
 * @property string $message
 * @property string $ip_address
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 * @property UserRole $userRole
 * @property CreditCard $card
 * @property WalletTransaction[] $walletTransactions
 */
class Wallet extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%wallet}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['card_id'],'required','on'=>'add_money'],
            [['user_id', 'amount', 'currency', 'type', 'user_role', 'message', 'ip_address'], 'required'],
            [['user_id', 'card_id', 'user_role', 'created_at', 'updated_at'], 'integer'],
            [['amount','close_balance'], 'number'],
            [['message'], 'string'],
            ['card_id', 'checkCard','on'=>'add_money'],
            [['currency'], 'string', 'max' => 10],
            [['type', 'ip_address'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['user_role'], 'exist', 'skipOnError' => true, 'targetClass' => UserRole::className(), 'targetAttribute' => ['user_role' => 'role_id']],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => CreditCards::className(), 'targetAttribute' => ['card_id' => 'id']],
        ];
    }
    public function checkCard($attribute, $params){
        if (!Yii::$app->user->isGuest) {
            $user_id	=  Yii::$app->user->identity->id;
            $checkCard = CreditCards::find()->where(['id'=>$this->card_id,'user_id'=>$this->user_id])->count();
            if($checkCard==0)
                $this->addError($attribute, Yii::t('app', 'You select an invalid card.'));
        }else
            $this->addError($attribute, Yii::t('app', 'You select an invalid card.'));
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'wallet_id' => Yii::t('app', 'Wallet ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'card_id' => Yii::t('app', 'Card'),
            'amount' => Yii::t('app', 'Amount'),
            'currency' => Yii::t('app', 'Currency'),
            'type' => Yii::t('app', 'Type'),
            'user_role' => Yii::t('app', 'User Role'),
            'message' => Yii::t('app', 'Message'),
            'ip_address' => Yii::t('app', 'Ip Address'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRole()
    {
        return $this->hasOne(UserRole::className(), ['role_id' => 'user_role']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCard()
    {
        return $this->hasOne(CreditCards::className(), ['id' => 'card_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWalletTransactions()
    {
        return $this->hasMany(WalletTransactions::className(), ['wallet_id' => 'wallet_id']);
    }

    public function orderTransaction($response){
        $model = new WalletTransactions();
        $model->transaction_id = $response['id'];
        $model->status = $response['status'];
        $model->type = $response['type'];
        $model->currency_iso_code = $response['currencyIsoCode'];
        $model->amount = $response['amount'];
        $model->merchantAccountId = $response['merchantAccountId'];
        $model->wallet_id = str_replace('Wallet ','',$response['orderId']);
        $model->full_response = serialize($response);
        $model->payment_method = 'braintree';
        $model->save();
    }
}
