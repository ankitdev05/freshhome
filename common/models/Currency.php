<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%currency}}".
 *
 * @property int $id
 * @property string $code
 * @property string $symbol
 * @property string $name
 * @property string $symbol_native
 * @property int $decimal_digits
 * @property int $rounding
 * @property string $name_plural
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'symbol', 'name', 'symbol_native', 'decimal_digits', 'rounding', 'name_plural'], 'required'],
            [['decimal_digits', 'rounding'], 'integer'],
            [['code'], 'string', 'max' => 3],
            [['symbol'], 'string', 'max' => 5],
            [['name', 'name_plural'], 'string', 'max' => 255],
            [['symbol_native'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'symbol' => Yii::t('app', 'Symbol'),
            'name' => Yii::t('app', 'Name'),
            'symbol_native' => Yii::t('app', 'Symbol Native'),
            'decimal_digits' => Yii::t('app', 'Decimal Digits'),
            'rounding' => Yii::t('app', 'Rounding'),
            'name_plural' => Yii::t('app', 'Name Plural'),
        ];
    }
}
