<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%refer_data}}".
 *
 * @property int $id
 * @property string $ip_address
 * @property int $user_id
 * @property string $user_agent
 * @property string $code
 * @property string $server_info
 * @property int $created_at
 *
 * @property User $user
 */
class ReferData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%refer_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ip_address', 'user_id', 'user_agent', 'server_info'], 'required'],
            [['user_id', 'created_at'], 'integer'],
            [['user_agent', 'server_info','code'], 'string'],
            [['ip_address'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ip_address' => Yii::t('app', 'Ip Address'),
            'user_id' => Yii::t('app', 'User ID'),
            'user_agent' => Yii::t('app', 'User Agent'),
            'server_info' => Yii::t('app', 'Server Info'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
