<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%driver_company}}".
 *
 * @property int $company_id
 * @property string $company_name
 * @property string $company_email
 * @property int $created_at
 * @property int $updated_at
 */
class DriverCompany extends \yii\db\ActiveRecord
{
    public $file;
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%driver_company}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_name', 'company_email'], 'required'],
            [['password'], 'required','on'=>'create'],
            [['company_name', 'company_email'], 'trim'],
            [['company_email'], 'email'],
            [['created_at', 'updated_at'], 'integer'],
            [['company_name', 'company_email','password'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'Company ID'),
            'company_name' => Yii::t('app', 'Name'),
            'company_email' => Yii::t('app', 'Email'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
