<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%countries}}".
 *
 * @property int $country_id
 * @property string $name
 * @property string $iso_code_2
 * @property string $iso_code_3
 * @property string $address_format
 * @property int $postcode_required
 * @property int $status
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%countries}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'iso_code_2', 'iso_code_3', 'currency_code'], 'required'],
            [['address_format','tax_name','symbol_right','symbol_left'], 'string'],
            [['postcode_required', 'status'], 'integer'],
            [['tax','delivery_charges'], 'number'],
            [['name','flag'], 'string', 'max' => 128],
            [['iso_code_2'], 'string', 'max' => 2],
            [['iso_code_3'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('app', 'Country ID'),
            'name' => Yii::t('app', 'Name'),
            'iso_code_2' => Yii::t('app', 'Iso Code 2'),
            'iso_code_3' => Yii::t('app', 'Iso Code 3'),
            'address_format' => Yii::t('app', 'Address Format'),
            'postcode_required' => Yii::t('app', 'Postcode Required'),
            'status' => Yii::t('app', 'Status'),
            'currency_code' => Yii::t('app', 'Currency'),
        ];
    }
}
