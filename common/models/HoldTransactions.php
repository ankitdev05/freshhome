<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%hold_transactions}}".
 *
 * @property int $id
 * @property string $transaction_id
 * @property string $status
 * @property string $type
 * @property string $currency_iso_code
 * @property string $amount
 * @property string $merchantAccountId
 * @property int $order_id
 * @property int $hold_id
 * @property string $full_response
 * @property string $payment_type
 * @property string $payment_status
 * @property string $captured_amount
 * @property string $captured_response
 * @property string $payment_method
 * @property int $created_at
 * @property int $updated_at
 *
 * @property HoldPayment $hold
 * @property Order $order
 */
class HoldTransactions extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%hold_transactions}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transaction_id', 'status', 'type', 'currency_iso_code', 'amount', 'order_id', 'hold_id', 'full_response', 'payment_type', 'payment_status'], 'required'],
            [['transaction_id', 'amount', 'full_response', 'payment_type', 'payment_status', 'captured_response'], 'string'],
            [['order_id', 'hold_id', 'created_at', 'updated_at'], 'integer'],
            [['captured_amount'], 'number'],
            [['status', 'type', 'currency_iso_code', 'merchantAccountId', 'payment_method'], 'string', 'max' => 255],
            [['hold_id'], 'exist', 'skipOnError' => true, 'targetClass' => HoldPayments::className(), 'targetAttribute' => ['hold_id' => 'hold_id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'order_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'status' => Yii::t('app', 'Status'),
            'type' => Yii::t('app', 'Type'),
            'currency_iso_code' => Yii::t('app', 'Currency Iso Code'),
            'amount' => Yii::t('app', 'Amount'),
            'merchantAccountId' => Yii::t('app', 'Merchant Account ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'hold_id' => Yii::t('app', 'Hold ID'),
            'full_response' => Yii::t('app', 'Full Response'),
            'payment_type' => Yii::t('app', 'Payment Type'),
            'payment_status' => Yii::t('app', 'Payment Status'),
            'captured_amount' => Yii::t('app', 'Captured Amount'),
            'captured_response' => Yii::t('app', 'Captured Response'),
            'payment_method' => Yii::t('app', 'Payment Method'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHold()
    {
        return $this->hasOne(HoldPayment::className(), ['hold_id' => 'hold_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
