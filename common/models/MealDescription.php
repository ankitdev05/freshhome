<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%meal_description}}".
 *
 * @property int $id
 * @property int $meal_id
 * @property int $language_id
 * @property string $meal_name
 *
 * @property Language $language
 * @property Meal $meal
 */
class MealDescription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%meal_description}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meal_id', 'language_id', 'meal_name'], 'required'],
            [['meal_id', 'language_id'], 'integer'],
            [['meal_name'], 'string', 'max' => 255],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'language_id']],
            [['meal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meal::className(), 'targetAttribute' => ['meal_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'meal_id' => Yii::t('app', 'Meal ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'meal_name' => Yii::t('app', 'Meal Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['language_id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeal()
    {
        return $this->hasOne(Meal::className(), ['id' => 'meal_id']);
    }
}
