<?php

namespace common\models;

use himiklab\thumbnail\EasyThumbnailImage;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property int $category_id
 * @property int $parent_id
 * @property int $cat_id
 * @property string $name
 * @property string $image
 * @property int $active
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 * @property int $sort_order
 *
 * @property AttributeGroupCategory[] $attributeGroupCategories
 * @property User $createdBy
 * @property MainCategory $cat
 * @property Category $parent
 * @property Category[] $categories
 * @property MenuCategory[] $menuCategories
 */
class Category extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }
   
     public $name;
     public $name1;
     public $image1;
     public $active1;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['home_category_id','parent_id', 'cat_id', 'active','active1', 'created_by','sort_order', 'created_at', 'updated_at'], 'integer'],
            [['home_category_id','cat_id','slug'], 'required'],
            [['cat_id'], 'trim'], 
            [['name'], 'required'],
            [['image','image1'], 'string', 'max' => 255],
            [['created_by','image1'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => MainCategories::className(), 'targetAttribute' => ['cat_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['parent_id' => 'category_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_id' => Yii::t('app', 'Category ID'),
            'parent_id' => Yii::t('app', 'Parent Category'),
            'cat_id' => Yii::t('app', 'Cat ID'),
            // 'name' => Yii::t('app', 'Name'),
            'home_category_id' => Yii::t('app', 'Home Category'), 
              
            'active' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeGroupCategories()
    {
        return $this->hasMany(AttributeGroupCategory::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(MainCategory::className(), ['id' => 'cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'category_id']);
    }

    public function getParentCategory()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'category_id'])->joinWith('categoryDescription as cd')->where(['and',['active' => 1],['cd.language_id' => language_id]]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuCategories()
    {
        return $this->hasMany(MainCategories::className(), ['category_id' => 'category_id']);
    }

    public function saveCategoryPath(){
        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;
        $results = CategoryPath::find()->where(['category_id' => $this->parent_id])->orderBy("level ASC")->all();
        if(!empty($results)){
            foreach ($results as $result){
                $model = new CategoryPath();
                $model->category_id = $this->category_id;
                $model->path_id = $result->path_id;
                $model->level = $level;
                $model->save();
                $level++;
            }
        }
        $model = new CategoryPath();
        $model->category_id = $this->category_id;
        $model->path_id = $this->category_id;
        $model->level = $level;
        $model->save();
    }

    public function updateCategoryPath(){
        // MySQL Hierarchical Data Closure Table Pattern
        $results = CategoryPath::find()->where(['path_id' => $this->category_id])->orderBy("level ASC")->all();
        if(!empty($results)){
            foreach ($results as $result) {
                // Delete the path below the current one
                CategoryPath::deleteAll(['and',['category_id' => $result->category_id],['<','level',$result->level]]);
                $path = [];

                // Get the nodes new parents
                $category_paths = CategoryPath::find()->where(['category_id' => $this->parent_id])->orderBy("level ASC")->all();
                if(!empty($category_paths)){
                    foreach ($category_paths as $category_path)
                        $path[] = $category_path->path_id;
                }

                // Get whats left of the nodes current path
                $category_paths = CategoryPath::find()->where(['category_id' => $result->category_id])->orderBy("level ASC")->all();
                if(!empty($category_paths)){
                    foreach ($category_paths as $category_path)
                        $path[] = $category_path->path_id;
                }

                // Combine the paths with a new level
                $level = 0;
                foreach ($path as $path_id) {
                    $model = CategoryPath::find()->where(['path_id' => $path_id,'category_id' => $result->category_id])->one();
                    if(!$model) $model = new CategoryPath;
                    $model->category_id = $result->category_id;
                    $model->path_id = $path_id;
                    $model->level = $level;
                    $model->save();
                    $level++;
                }
            }
        }else{
            // Delete the path below the current one
            CategoryPath::deleteAll(['category_id' => $this->category_id]);

            // Fix for records with no paths
            $level = 0;

            $results = CategoryPath::find()->where(['category_id' => $this->parent_id])->orderBy("level ASC")->all();
            if(!empty($results)){
                foreach ($results as $result){
                    $model = new CategoryPath();
                    $model->category_id = $this->category_id;
                    $model->path_id = $result->path_id;
                    $model->level = $level;
                    $model->save();
                    $level++;
                }
            }

            $model = CategoryPath::find()->where(['category_id' => $this->category_id])->one();
            if(!$model) $model = new CategoryPath;
            $model->category_id = $this->category_id;
            $model->path_id = $this->category_id;
            $model->level = $level;
            $model->save();
        }
    }

    static public function getAllCategories($cat_id,$condition=[]){
        $query =  CategoryPath::find()
            ->select('cp.category_id AS category_id, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR \'&nbsp;&nbsp;&gt;&nbsp;&nbsp;\') AS name')
            ->alias('cp')
            ->leftJoin('tbl_category as c1 ON (cp.category_id = c1.category_id)')
            ->leftJoin('tbl_category as c2 ON (cp.path_id = c2.category_id)')
            ->where(['in','c1.cat_id', $cat_id]);
        if(!empty($condition))
            $query->andWhere($condition);

        $query->groupBy('cp.category_id')->orderBy('c1.name');
        $result = $query->asArray()->all();
        return $result;
    }
    static public function getCategoryName($category_id){
        return CategoryPath::find()
            ->select('cp.category_id AS category_id, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR \'&nbsp;&nbsp;&gt;&nbsp;&nbsp;\') AS name')
            ->alias('cp')
            ->leftJoin('tbl_category as c1 ON (cp.category_id = c1.category_id)')
            ->leftJoin('tbl_category as c2 ON (cp.path_id = c2.category_id)')
            ->where(['c1.category_id' => $category_id])

            ->asArray()
            ->one();
    }

    public function getCategoryImage(){
        if(!empty($this->image))
            return Yii::$app->params['website_path'].'/uploads/'.$this->image;
    }

    public function getCategoryDescription()
    {
        return $this->hasOne(CategoryDescription::className(), ['category_id' => 'category_id']);
    }

    public function getCategoryUrl(){
        if(!empty($this->slug))
            return Url::to(Yii::$app->request->baseUrl.'/category/'.$this->slug,true);
        return Url::to(Yii::$app->request->baseUrl.'/category?category_id='.$this->category_id,true);
    }

    public function getParentCategories($category_id){
        $category = Category::findOne($category_id);

        if(empty($category))
            return $categories ?? [];
        if(!empty($category)){
            $categories[] = $category->category_id;
            if(!empty($category->parent_id)){
                $child_items =  $this->getParentCategories($category->parent_id);
                if(!empty($child_items))
                    foreach ($child_items as $child_item)
                        $categories[] = $child_item;


            }
        }
        return $categories ?? [];
    }

    public function getProductImage($sizeX=100,$sizeY=100){
        if(empty($this->image))
            $this->image = 'no_image.png';
        $image          = '../../frontend/web/uploads/'.$this->image;
        $p_image    = EasyThumbnailImage::thumbnailFileUrl(
            $image,$sizeX,$sizeY,EasyThumbnailImage::THUMBNAIL_INSET
        );
        return $p_image;
    }
}
