<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_options}}".
 *
 * @property int $order_option_id
 * @property int $order_id
 * @property int $order_product_id
 * @property string $name
 * @property string $type
 * @property string $value
 * @property string $price
 * @property int $subtract
 *
 * @property Order $order
 * @property Menu $orderProduct
 */
class OrderOptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_options}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'order_product_id', 'name', 'type', 'value', 'price', 'subtract'], 'required'],
            [['order_id', 'order_product_id', 'subtract'], 'integer'],
            [['value'], 'string'],
            [['price'], 'number'],
            [['name', 'type'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'order_id']],
            [['order_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['order_product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_option_id' => Yii::t('app', 'Order Option ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'order_product_id' => Yii::t('app', 'Order Product ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'value' => Yii::t('app', 'Value'),
            'price' => Yii::t('app', 'Price'),
            'subtract' => Yii::t('app', 'Subtract'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProduct()
    {
        return $this->hasOne(Menu::className(), ['id' => 'order_product_id']);
    }
}
