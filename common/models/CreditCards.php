<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%credit_cards}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property string $identifier
 * @property string $card_type
 * @property string $name
 * @property int $expired
 * @property string $image_url
 * @property string $exp_date
 * @property string $masked_num
 * @property string $cc_verification
 * @property string $type
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class CreditCards extends \yii\db\ActiveRecord
{
    public $card_name;
    public $card_number;
    public $exp_month;
    public $exp_year;
    public $redirect_to;
    public $cvv;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%credit_cards}}';
    }
    public function behaviors(){
        return [
            TimestampBehavior::className()
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['card_number','exp_month','exp_year','cvv','card_name'],'trim'],
            [['card_number','exp_month','exp_year','cvv','card_name'],'required','on'=>'step_1'],
            [['user_id',  'token', 'identifier', 'card_type', 'masked_num'], 'required','on'=>'step_2'],
            [['user_id', 'expired', 'created_at', 'updated_at','default_card'], 'integer'],
            [['identifier', 'image_url'], 'string'],
            [['redirect_to'], 'safe'],
            [['token', 'name', 'exp_date', 'masked_num', 'type'], 'string', 'max' => 255],
            [['card_type'], 'string', 'max' => 25],
            [['cc_verification'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'token' => Yii::t('app', 'Token'),
            'identifier' => Yii::t('app', 'Identifier'),
            'card_type' => Yii::t('app', 'Card Type'),
            'name' => Yii::t('app', 'Name'),
            'expired' => Yii::t('app', 'Expired'),
            'image_url' => Yii::t('app', 'Image Url'),
            'exp_date' => Yii::t('app', 'Exp Date'),
            'masked_num' => Yii::t('app', 'Masked Num'),
            'cc_verification' => Yii::t('app', 'Cc Verification'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'card_name' => Yii::t('app', 'Card Holder Name'),
            'exp_month' => Yii::t('app', 'Month'),
            'exp_year' => Yii::t('app', 'Year'),
            'cvv' => Yii::t('app', 'CVV'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
