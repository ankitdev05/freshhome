<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%order_ratings}}".
 *
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property int $supplier_id
 * @property string $taste_rating
 * @property string $presentation_rating
 * @property string $packing_rating
 * @property string $overall_rating
 * @property string $review
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Order $order
 * @property User $user
 * @property User $supplier
 */
class OrderRatings extends \yii\db\ActiveRecord
{
    public $rating;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_ratings}}';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'user_id', 'supplier_id'], 'required'],
            [['order_id', 'user_id', 'supplier_id', 'created_at', 'updated_at','is_active'], 'integer'],
            [['taste_rating', 'presentation_rating', 'packing_rating', 'overall_rating'], 'number'],
            [['review'], 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'order_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['supplier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'user_id' => Yii::t('app', 'User Name'),
            'supplier_id' => Yii::t('app', 'Supplier name'),
            'taste_rating' => Yii::t('app', 'Taste Rating'),
            'presentation_rating' => Yii::t('app', 'Presentation Rating'),
            'packing_rating' => Yii::t('app', 'Packing Rating'),
            'overall_rating' => Yii::t('app', 'Overall Rating'),
            'review' => Yii::t('app', 'Review'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(User::className(), ['id' => 'supplier_id']);
    }
    public function getRatingInfo(){
        $json['review'] = $this->review;
        $json['time'] = Yii::$app->formatter->format($this->created_at, 'relativeTime');
        $user_detail= $this->user->userDetail;
        $json['uploaded_by']['name'] = $user_detail['name'];
        $json['uploaded_by']['profile_pic'] = $user_detail['profile_pic'];
        $json['uploaded_by']['email'] = $user_detail['email'];
        return $json;
    }
}
