<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%driver_company_numbers}}".
 *
 * @property int $id
 * @property int $company_id
 * @property string $phone_number
 * @property string $token
 *
 * @property DriverCompany $company
 */
class DriverCompanyNumbers extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%driver_company_numbers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'phone_number', 'token'], 'required'],
            [['is_available', 'name', 'age','nationality'], 'required','on'=>'update'],
            [['phone_number'], 'trim'],
            [['phone_number'], 'unique','on'=>'create'],
            [['company_id','is_available','age'], 'integer'],
            [['token','name','nationality','profile_pic'], 'string'],
            [['phone_number'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => DriverCompany::className(), 'targetAttribute' => ['company_id' => 'company_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company Name'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'token' => Yii::t('app', 'Token'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(DriverCompany::className(), ['company_id' => 'company_id']);
    }
    public function getGenrateCode(){
        $code	= $this->random_string(8);
        $result	= DriverCompanyNumbers::find()->where(['token'=>$code])->one();
        if(!$result)
            return $code;
        $this->genrateCode();
    }
    function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }
    public function getProfile(){
        $model = new User();
        $json['is_available'] = $this->is_available;
        $json['username']         = $this->name;
        $json['name']         = $this->name;
        $json['age']         = $this->age;
        $json['is_driver']  = 'yes';
        $json['nationality']  = $this->nationality;
        $json['profile_pic'] = !empty($this->profile_pic) ? Url::to($model->getUserImage($this->profile_pic,100,100) ,true): null ;
        return $json;
    }
}
