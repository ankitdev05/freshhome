<?php
namespace common\models;
use Yii;
use yii\helpers\Url;
use yii\base\Model;
use common\models\Order;
class Sale extends Model{
    public function TotalOrders($user_id){
        return Order::find()->where(['user_id'=>$user_id])->count();
    }
    public function TotalOrderProducts($user_id){
        return Order::find()->joinWith('orderProducts')->where(['user_id'=>$user_id])->count();
    }
    public function TotalPrice($user_id){
        return Order::find()->joinWith('orderTotals as ot')->where(['user_id'=>$user_id,'ot.code'=>'total'])->sum('ot.value');
    }

}