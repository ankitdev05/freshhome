<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form of `common\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_accepted','order_id', 'address_id', 'currency_id', 'order_status_id', 'created_at', 'updated_at'], 'integer'],
            [['currency_code', 'ip', 'payment_code', 'payment_method','order_accepted','user_id', 'supplier_id'], 'safe'],
            [['total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find()->joinWith('supplier as sup')->joinWith('user as u');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['order_id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if(empty($this->order_accepted)){
           // $query->andWhere(['IS','order_accepted',null]);
        }else
            $query->andFilterWhere(['order_accepted'=>$this->order_accepted]);
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            //'user_id' => $this->user_id,
           // 'supplier_id' => $this->supplier_id,
            'address_id' => $this->address_id,
            'currency_id' => $this->currency_id,
            'total' => $this->total,
            'tbl_order.driver_id' => $this->driver_id,
            'order_status_id' => $this->order_status_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'currency_code', $this->currency_code])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'payment_code', $this->payment_code])
            ->andFilterWhere(['like', 'payment_method', $this->payment_method])
            ->andFilterWhere(['OR',['like', 'sup.name', $this->supplier_id],['like', 'sup.share_id', $this->supplier_id]])
            ->andFilterWhere(['OR',['like', 'u.name', $this->user_id],['like', 'u.share_id', $this->user_id]]);

        return $dataProvider;
    }
}
