<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%subscriptions}}".
 *
 * @property int $id
 * @property string $email
 * @property string $ip_address
 * @property int $status
 * @property string $user_agent
 * @property int $created_at
 * @property int $updated_at
 */
class Subscriptions extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%subscriptions}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'ip_address', 'status', 'user_agent'], 'required'],
            [['email'], 'email'],
            [['email'], 'trim'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['user_agent'], 'string'],
            [['email'], 'string', 'max' => 255],
            [['ip_address'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'ip_address' => Yii::t('app', 'Ip Address'),
            'status' => Yii::t('app', 'Status'),
            'user_agent' => Yii::t('app', 'User Agent'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
