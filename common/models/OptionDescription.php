<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%option_description}}".
 *
 * @property int $id
 * @property int $option_id
 * @property int $language_id
 * @property string $name
 *
 * @property Option $option
 * @property Language $language
 */
class OptionDescription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%option_description}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['option_id', 'language_id', 'name'], 'required'],
            [['option_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['option_id'], 'exist', 'skipOnError' => true, 'targetClass' => Option::className(), 'targetAttribute' => ['option_id' => 'option_id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'language_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'option_id' => Yii::t('app', 'Option ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(Option::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['language_id' => 'language_id']);
    }
}
