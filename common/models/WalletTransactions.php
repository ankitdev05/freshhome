<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%wallet_transactions}}".
 *
 * @property int $id
 * @property int $wallet_id
 * @property int $transaction_id
 * @property string $status
 * @property string $type
 * @property string $currency_iso_code
 * @property string $amount
 * @property string $merchantAccountId
 * @property string $full_response
 * @property string $payment_method
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Wallet $wallet
 */
class WalletTransactions extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%wallet_transactions}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['wallet_id', 'transaction_id', 'status', 'type', 'currency_iso_code', 'amount', 'merchantAccountId', 'full_response', 'payment_method'], 'required'],
            [['wallet_id', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
            [['full_response','transaction_id'], 'string'],
            [['status', 'type', 'currency_iso_code', 'merchantAccountId', 'payment_method'], 'string', 'max' => 255],
            [['wallet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wallet::className(), 'targetAttribute' => ['wallet_id' => 'wallet_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'wallet_id' => Yii::t('app', 'Wallet ID'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'status' => Yii::t('app', 'Status'),
            'type' => Yii::t('app', 'Type'),
            'currency_iso_code' => Yii::t('app', 'Currency Iso Code'),
            'amount' => Yii::t('app', 'Amount'),
            'merchantAccountId' => Yii::t('app', 'Merchant Account ID'),
            'full_response' => Yii::t('app', 'Full Response'),
            'payment_method' => Yii::t('app', 'Payment Method'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWallet()
    {
        return $this->hasOne(Wallet::className(), ['wallet_id' => 'wallet_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $user_info = Yii::$app->user->identity;
        $total = $this->amount;
        $model = new UserTotal();
        $result = $model->find()->where(['user_id'=>$user_info->id])->one();
        if(!empty($result)){
            $result->updateCounters(['balance' => $total]);
            $total = $result->balance;
        }else{
            $model->user_id = $user_info->id;
            $model->balance = $total;
            $model->save();
        }
        $wallet = Wallet::findOne($this->wallet_id);
        $wallet->close_balance = $total;
        $wallet->save();
        return parent::afterSave($insert, $changedAttributes);
    }
}
