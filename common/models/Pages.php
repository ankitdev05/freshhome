<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%pages}}".
 *
 * @property int $page_id
 * @property int $parent_id
 * @property string $title
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $slug
 * @property int $page_order
 * @property int $display_menu
 * @property int $status
 * @property int $created_by
 * @property int $last_updated_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Pages $parent
 * @property Pages[] $pages
 */
class Pages extends \yii\db\ActiveRecord
{
    public $title;
    public $description;
    public $meta_description;
    public $meta_title;
    public $meta_keyword;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%pages}}';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'page_order', 'display_menu', 'status', 'created_by', 'last_updated_by', 'created_at', 'updated_at','page_type','show_on_footer','show_on_header'], 'integer'],
            [['title', 'description', 'slug', 'page_order','meta_title'], 'trim'],
            [['title', 'description', 'slug', 'page_order', 'display_menu', 'status'], 'required'],

            [['slug'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pages::className(), 'targetAttribute' => ['parent_id' => 'page_id']],
            [['title'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            [['description'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            [['meta_description'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            [['meta_title'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            [['meta_keyword'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'page_id' => Yii::t('app', 'Page ID'),
            'parent_id' => Yii::t('app', 'Parent Page'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'meta_keyword' => Yii::t('app', 'Meta Keyword'),
            'slug' => Yii::t('app', 'Slug'),
            'page_order' => Yii::t('app', 'Page Order'),
            'display_menu' => Yii::t('app', 'Display On'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'last_updated_by' => Yii::t('app', 'Last Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'page_type' => Yii::t('app', 'Page Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Pages::className(), ['page_id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Pages::className(), ['parent_id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageDescriptions()
    {
        return $this->hasMany(PagesDescription::className(), ['page_id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageDescription()
    {
        return $this->hasOne(PagesDescription::className(), ['page_id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'last_updated_by']);
    }
    public function beforeSave($insert){
        if ($this->isNewRecord){
            $this->created_by 		    = Yii::$app->user->identity->id;
            $this->last_updated_by 		= Yii::$app->user->identity->id;
        }
        else
            $this->last_updated_by 		= Yii::$app->user->identity->id;
        return parent::beforeSave($insert);
    }
    public function afterSave($insert,$changedAttributes){

        $explode			= explode("\\", get_class($this));
        $class_name			= strtolower(end($explode));
        if ($insert)
            $path			= \Yii::getAlias('@webroot').'/updated_files/'.$class_name.'/'.$this->page_id.'/main';
        else
            $path			= \Yii::getAlias('@webroot').'/updated_files/'.$class_name.'/'.$this->page_id.'/update';
        @mkdir($path, 0755, true);
        $file_name			= time().'_'.Yii::$app->user->identity->id.'.json';
        $fp 				= fopen($path.'/'.$file_name, 'w');
        $result				= Pages::find()->where(['page_id'=>$this->page_id])->asArray()->one();
        if(!empty($result)){
            fwrite($fp, json_encode($result));
            fclose($fp);
        }

        return parent::afterSave($insert,$changedAttributes);
    }
    public function getPageUrl(){
        if($this->page_type===0)
            return Url::to(Yii::$app->request->baseUrl.'/pages/'.$this->slug,true);
        return Url::to(Yii::$app->request->baseUrl.'/'.$this->slug,true);
    }
}
