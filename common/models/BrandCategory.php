<?php

namespace common\models;
 
use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%brand_category}}" 
 *
 * @property int $reason_id
 * @property string $name
 * @property string $type
 * @property int $created_at
 * @property int $updated_at
 */
class BrandCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%brand_category}}';
    }
    public function behaviors()
    {
        return [ 
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id', 'category_id'], 'required'],
            // [['brand_id'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            // [['type'], 'string'],
        ];  
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    { 
        return [
            'brand_id' => Yii::t('app', 'brand Id'),
            'category_id' => Yii::t('app', 'category id '),
            // 'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['brand_id' => 'brand_id']);
    }
}
