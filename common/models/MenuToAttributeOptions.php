<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%menu_to_attribute_options}}".
 *
 * @property int $id
 * @property int $menu_id
 * @property int $menu_attr
 * @property string $name
 * @property int $quantity
 * @property int $subtract
 * @property string $price
 *
 * @property Menu $menu
 * @property MenuToAttribute $menuAttr
 */
class MenuToAttributeOptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_to_attribute_options}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id', 'menu_attr', 'name'], 'required'],
            [['menu_id', 'menu_attr', 'name'], 'trim'],
            [['menu_id', 'menu_attr', 'quantity', 'subtract'], 'integer'],
            [['name'], 'string'],
            [['price'], 'number'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['menu_attr'], 'exist', 'skipOnError' => true, 'targetClass' => MenuToAttributes::className(), 'targetAttribute' => ['menu_attr' => 'menu_attr']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'menu_attr' => 'Menu Attr',
            'name' => 'Name',
            'quantity' => 'Quantity',
            'subtract' => 'Subtract',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuAttr()
    {
        return $this->hasOne(MenuToAttributes::className(), ['menu_attr' => 'menu_attr']);
    }
}
