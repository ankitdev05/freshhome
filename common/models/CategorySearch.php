<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Category;

/**
 * CategorySearch represents the model behind the search form of `common\models\Category`.
 */
class CategorySearch extends Category
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [ 
            [['category_id','home_category_id', 'parent_id', 'cat_id', 'active', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['name','name1','image1'], 'safe'], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Category::find()->select('*')
        ->where(['parent_id' => null]);
         // ->Where(['not', ['parent_id' => null]]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['category_id' => SORT_DESC ]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'home_category_id'=> $this->home_category_id,
            'name'=> $this->name,
            'category_id' => $this->category_id,
            'parent_id' => $this->parent_id,
            'cat_id' => $this->cat_id,
            'active' => $this->active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
       
        $query->andFilterWhere(['like', 'name', $this->name,
            'like', 'home_category_id', $this->home_category_id,
    ]);

        return $dataProvider;
    }
}
