<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%cuisine_description}}".
 *
 * @property int $id
 * @property int $cuisine_id
 * @property int $language_id
 * @property string $cuisine_name
 *
 * @property Cuisine $cuisine
 * @property Language $language
 */
class CategoryDescription extends \yii\db\ActiveRecord
{

   
 
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category_description}}';
    }
      public function behaviors()
    {
        return [ 
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'language_id'], 'required'],
            [['category_id', 'language_id'], 'integer'], 
            [['category_name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cuisine::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'language_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'category ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'category_name' => Yii::t('app', 'category Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()  
    { 
        return $this->hasOne(category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['language_id' => 'language_id']);
    }
}
