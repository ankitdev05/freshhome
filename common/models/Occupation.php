<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%occupation}}".
 *
 * @property int $id
 * @property string $occupation_name
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property OccupationDescription[] $occupationDescriptions
 */
class Occupation extends \yii\db\ActiveRecord
{
	public $occupation_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%occupation}}';
    }
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['occupation_name', 'status'], 'required'],
            [['occupation_name'], 'trim'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
			[['occupation_name'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'occupation_name' => Yii::t('app', 'Occupation Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOccupationDescription()
    {
        return $this->hasMany(OccupationDescription::className(), ['occupation_id' => 'id']);
    }
	public function getOccupationName()
    {
        return $this->hasOne(OccupationDescription::className(), ['occupation_id' => 'id']);
    }
}
