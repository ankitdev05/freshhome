<?php
return [
    'bsVersion' => '4.x',
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'btree_environment' => 'sandbox',
    'btree_merchant_id' => 'yz3kmv2rkf22fqgy',
    'btree_public_key' => 'w2tp3cgdyx6j65td',
    'btree_private_key' => 'bc97e11f61e03637a26a9e14ad94782a',
    //'api_path'=>'http://localhost/MP/api',
    'api_path'=>'https://freshhomee.com/web/api',
    //'website_path'=>'http://localhost/MP',
    'website_path'=>'https://freshhomee.com/web',
    'admin_website_path'=>'https://freshhomee.com/web/administrator',
];
